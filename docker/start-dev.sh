#!/usr/bin/env sh

if [ "$FRONTEND_LIVE_RELOAD" = "1" ]; then
    echo "start server with live reload"
    cd /app
    yarn install
    yarn start
else
    echo "start server"
    cd /app
    yarn install
    yarn build:run
fi