# Pleio frontend
This repository is the base of the new responsive Pleio subsite theme. It contains a frontend codebase using React.js and Apollo client and also contains a GraphQL API to query the backend services.

## Install
Run `yarn` or `yarn install`

## Build
Run `yarn build`

- To test the build locally, run `yarn build` and then `yarn build:run`.
- Make sure to set `export PUBLIC_PATH='http://localhost:9001/'` in your frontend `.envrc` file to make sure the frontend can find the built chunks. 
- After changing the `.envrc` file, run `direnv allow` to apply the changes.

## Watch
Run `yarn start`

## Typechecking
Run `yarn typecheck`

## Unit testing
Run `yarn test` to run all unit tests.

To run a specific test, run `yarn test -t 'name of test'`.

When using Visual Studio Code, the official `Jest` extension enables you to run tests by simply clicking them.

## Translate
Create, edit or delete keys in src/js/i18n/messages/en.json and use `yarn translate` to automatically translate other languages. This requires you to have an environment variable $DEEPL_TOKEN set. You can add this locally for this directory using [direnv](https://direnv.net/): rename `.envrc.example` to `.envrc`, insert your token and run `direnv allow`. More info and options on for translation options van be found at https://github.com/leolabs/json-autotranslate.

## Search
When you want to use search and mentioning (@[username]) be sure to have ElasticSearch activated in the back-end: https://gitlab.com/pleio/backend2#elasticsearch

## Storybook
We use Storybook to develop and test our components in an isolated state. It also helps us showcasing these components.

Run it with `yarn storybook`.

## Tooling

### Gitlab automation

#### Create local branch

It's possible to automatically create a local branch and update the Kanban board when you start working on an issue with the following command:

```bash
yarn branch <issuenr>
```

For this to work you have to setup Gitlab CLI Tools:

1. Install Gitlab CLI Tools with `brew install glab`
2. Create a new access token at https://gitlab.com/-/profile/personal_access_tokens and make sure "api" and "write_repository" permissions are checked
3. Authorize glab with `glab auth login`

(The script that is executed is `scripts/branch.mjs`.)

#### Update Kanban board for review

To create a draft MR for the current branch, make sure your branch is published and run:

```bash
yarn mr
```

To create a published MR for the current branch, run:

```bash
yarn mr --p
```

(The script that is executed is `scripts/mr.mjs`.)

#### Update Kanban board for review

To put the current issue on the Kanban board in review, run:

```bash
yarn review
```
