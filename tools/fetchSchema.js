import fs from 'fs'
import fetch from 'node-fetch'

fetch(`http://test1.pleio.local:8000/graphql`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
        variables: {},
        query: `
      {
        __schema {
          types {
            kind
            name
            possibleTypes {
              name
            }
          }
        }
      }
    `,
    }),
})
    .then((result) => result.json())
    .then((result) => {
        const possibleTypes = {}

        result.data.__schema.types.forEach((supertype) => {
            if (supertype.possibleTypes) {
                possibleTypes[supertype.name] = supertype.possibleTypes.map(
                    (subtype) => subtype.name,
                )
            }
        })

        fs.writeFile(
            './src/possibleTypes.json',
            JSON.stringify(possibleTypes),
            (err) => {
                if (err) {
                    console.error('Error writing possibleTypes.json', err)
                }
            },
        )
    })
