// Run `yarn review` from command line to update the Kanban board for the current issue. It moves the issue to the "In Review" column and updates the tags.
// Make sure you've GitLab CLI Tools installed, as described in ./branch.mjs.

import chalk from 'chalk'
import { exec } from 'child_process'

const repoName = 'beheer'

// Create branch from issue title
// Update Kanban board by updating labels and assignee
export default async function review() {
    exec(`git branch --show-current`, (err, stdout) => {
        if (err) {
            console.log(chalk.red(`Cannot find current branch`))
        } else {
            console.log(chalk.green(`Updating branch: ${stdout}`))
            const branchName = stdout.trim()
            const issueId = branchName.split('#')[1].split('-')[0]
            console.log(chalk.gray(`Extracted issue id: ${issueId}`))

            updateIssue(issueId)
        }
    })
}

function updateIssue(issueId) {
    exec(
        `glab issue update ${issueId} --repo https://gitlab.com/pleio/${repoName} --unlabel 'env::Mee bezig' --label 'env::Review' --unassign`,
        (err, stdout, stderr) => {
            if (!err) {
                console.log(
                    chalk.green(
                        `Issue ${issueId} put in review and assigned to Unassigned`,
                    ),
                )
            } else {
                console.log(chalk.red(`Error: ${stderr}`))
            }
        },
    )
}

review()
