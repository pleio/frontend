// Run `yarn branch <issuenr>` from command line to create a new branch with the issue number as name; while Kanban board is updated as well.
// 1. Install Gitlab CLI Tools with `brew install glab`
// 2. Create new access token in https://gitlab.com/-/profile/personal_access_tokens and make sure "api" and "write_repository" permissions are checked
// 3. Authorize glab with `glab auth login`

import chalk from 'chalk'
import { exec } from 'child_process'

const repoName = 'beheer'
const frontendProjectId = '8391332' // Change to project_id of other repo when you want to create a branch from another repo

// Create branch from issue title
// Update Kanban board by updating labels and assignee
export default async function branch() {
    const args = process.argv
    const issueId = args.slice(2)[0]

    if (issueId) {
        exec(`git checkout dev`, (err) => {
            if (err) {
                console.log(
                    chalk.red(
                        'Could not checkout dev. Is your current branch clean?',
                    ),
                )
            } else {
                exec(
                    `glab api https://gitlab.com/api/v4/projects/${frontendProjectId}/issues/${issueId}`,
                    (err, stdout, stderr) => {
                        if (!err) {
                            // Parse ticket title to branch title
                            const json = JSON.parse(stdout)
                            const branchTitle = getBranchTitle(
                                issueId,
                                json.title,
                            )

                            // Create local branch
                            createBranch(branchTitle)

                            // Update issue in Gitlab
                            updateIssueLabels(issueId)
                            updateIssueAssignee(issueId)
                        }
                    },
                )
            }
        })
        console.log(
            `Issue: https://gitlab.com/pleio/${repoName}/-/issues/${issueId}`,
        )
    } else {
        console.log(chalk.red('No issue id provided'))
    }
}

async function createBranch(branchTitle) {
    await exec(`git pull && git checkout -b ${branchTitle}`)
    console.log(chalk.green(`Branch ${branchTitle} created`))
}

// Generate branch title from issue title
function getBranchTitle(issueId, issueTitle) {
    const title = issueTitle
        .replace(/ /g, '-')
        .toLowerCase()
        .replace(/[^a-z0-9-_]/gi, '')
        .trim()
    return `${repoName}#${issueId}-${title}`.slice(0, 60) // Prevent branch name from being too long
}

function updateIssueLabels(issueId) {
    exec(
        `glab issue update ${issueId} --repo https://gitlab.com/pleio/${repoName} --label 'env::Mee bezig' --label 'pleio::kanban' --unlabel 'Refinen'`,
        (err, stdout, stderr) => {
            if (!err) {
                console.log(chalk.green(`Issue ${issueId} updated`))
            } else {
                console.log(chalk.red(`Error: ${stderr}`))
            }
        },
    )
}

async function updateIssueAssignee(issueId) {
    exec(
        `glab issue update ${issueId} --repo https://gitlab.com/pleio/${repoName} --assignee @me`,
        (err, stdout, stderr) => {
            if (!err) {
                console.log(
                    chalk.green(`Issue ${issueId} assigned to yourself`),
                )
            } else {
                console.log(chalk.red(`Error: ${stderr}`))
            }
        },
    )
}

branch()
