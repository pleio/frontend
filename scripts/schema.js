import { getSchema } from '@tiptap/core'
import * as fs from 'fs'

import getExtensions from '../src/js/components/Tiptap/getExtensions'

// create schema.json file with help from https://github.com/ueberdosis/tiptap/issues/1718

const { spec } = getSchema(
    getExtensions({
        placeholder: '',
        textStyle: true,
        textLink: true,
        textAnchor: true,
        textCSSClass: true,
        textQuote: true,
        textList: true,
        insertMedia: true,
        insertAccordion: true,
        insertTable: true,
        insertMention: true,
        insertButton: true,
        textLanguage: true,
    }),
)

const marks = {}
const nodes = {}

spec.marks?.forEach((key, value) => {
    marks[key] = value
})

spec.nodes?.forEach((key, value) => {
    nodes[key] = value
})

const schema = JSON.stringify(
    {
        topNode: spec.topNode,
        marks,
        nodes,
    },
    null,
    1,
)

fs.writeFile('./public/schema.json', schema, (err) => {
    if (err) {
        console.error('Error writing schema.json', err)
    }
})
