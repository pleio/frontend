// Run `yarn mr` to create an MR for your published branch. It grabs the issue ID and name of the corresponding Beheer issue and uses it to create the MR title.
// By default the MR is in Draft, using the `--p` parameter you can instantly publish it.
// Make sure you've GitLab CLI Tools installed, as described in ./branch.mjs.

import chalk from 'chalk'
import { exec } from 'child_process'

const repoName = 'beheer'
const frontendProjectId = '8391332'

// Create MR from issue id and title
export default async function mr() {
    const args = process.argv

    const publish = args.includes('--p')

    exec(`git branch --show-current`, (err, stdout) => {
        if (err) {
            console.log(chalk.red(`Cannot find current branch`))
        } else {
            console.log(chalk.green(`Creating MR for branch: ${stdout}`))
            const branchName = stdout.trim()
            const issueId = branchName.split('#')[1].split('-')[0]
            console.log(chalk.gray(`Extracted issue id: ${issueId}`))

            getIssueTitle(issueId, (issueTitle) => {
                console.log(chalk.gray(`Extracted issue title: ${issueTitle}`))

                createMR(issueId, issueTitle, publish)
            })
        }
    })
}

function getIssueTitle(issueId, cb) {
    exec(
        `glab api https://gitlab.com/api/v4/projects/${frontendProjectId}/issues/${issueId}`,
        (err, stdout, stderr) => {
            if (!err) {
                // Parse ticket title to branch title
                const json = JSON.parse(stdout)
                cb(json.title)
            } else {
                console.log(chalk.red(`Error: ${stderr}`))
            }
        },
    )
}

function createMR(issueId, issueTitle, publish) {
    const draft = !publish ? '--draft' : ''

    exec(
        `glab mr create --fill --title "feat: ${issueTitle} (beheer#${issueId})" ${draft} --yes --remove-source-branch --squash-before-merge`,
        (err, stdout, stderr) => {
            if (!err) {
                console.log(chalk.green(`Created MR: ${stdout}`))
            } else {
                console.log(
                    chalk.red(
                        `Error: You probably did not yet publish this branch, or the MR already exists.`,
                    ),
                )
                console.log(chalk.red(`Error: ${stderr}`))
            }
        },
    )
}

mr()
