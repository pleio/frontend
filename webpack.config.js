/* eslint @typescript-eslint/no-var-requires: "off" */
const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin')

const { join, resolve } = path

const devMode = process.env.NODE_ENV !== 'production'

const publicPath = devMode
    ? 'http://localhost:9001/'
    : process.env.PUBLIC_PATH
      ? process.env.PUBLIC_PATH
      : '/static/frontend/'

const config = {
    mode: devMode ? 'development' : 'production',

    entry: {
        web: ['./src/js/Web.jsx'],
    },

    output: {
        path: join(__dirname, 'build'),
        publicPath,
        chunkFilename: '[name].[id].js',
    },

    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendor',
                    filename: '[name].js',
                    chunks: 'all',
                },
                styles: {
                    idHint: 'styles',
                    test: /\.css$/,
                    chunks: 'all',
                    enforce: true,
                },
            },
        },
    },

    devServer: devMode
        ? {
              webSocketServer: 'ws',
              static: {
                  directory: resolve(__dirname, 'public'),
              },
              hot: true,
              port: 9001,
              host: '0.0.0.0',
              headers: {
                  'Access-Control-Allow-Origin': '*',
                  'Access-Control-Allow-Methods':
                      'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                  'Access-Control-Allow-Headers':
                      'X-Requested-With, content-type, Authorization',
              },
              historyApiFallback: {
                  index: 'index.html',
              },
              allowedHosts: 'all',
              client: {
                  overlay: {
                      runtimeErrors: (error) => {
                          // Hide error overlay when resizing window with useMeasure
                          if (
                              error.message ===
                              'ResizeObserver loop completed with undelivered notifications.'
                          ) {
                              return false
                          }

                          return true
                      },
                  },
              },
          }
        : undefined,

    devtool: devMode ? 'eval-cheap-module-source-map' : false,

    module: {
        rules: [
            { test: /\.tsx?$/, loader: 'ts-loader' },
            {
                test: /\.(js|jsx)$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        plugins: [
                            devMode && require.resolve('react-refresh/babel'),
                        ].filter(Boolean),
                    },
                },
                exclude: /node_modules/,
            },
            {
                test: /\.less$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    { loader: 'css-loader' },
                    {
                        loader: 'postcss-loader',
                        options: {
                            postcssOptions: {
                                plugins: [['postcss-preset-env']],
                            },
                        },
                    },
                    'less-loader',
                ],
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                include: [resolve(__dirname, 'src/images')],
                loader: 'file-loader',
                options: {
                    name: 'public/images/[name].[ext]',
                },
            },
            {
                test: /.svg$/,
                include: [resolve(__dirname, 'src/icons')],
                use: [
                    {
                        loader: '@svgr/webpack',
                        options: {
                            replaceAttrValues: {
                                '#000': 'currentColor',
                                '#000000': 'currentColor',
                            },
                            svgProps: {
                                display: 'block',
                                focusable: 'false',
                                role: 'img',
                                'aria-hidden': 'true',
                            },
                            svgoConfig: {
                                plugins: [
                                    {
                                        name: 'preset-default',
                                        params: {
                                            overrides: {
                                                removeViewBox: false,
                                                cleanupIds: false,
                                            },
                                        },
                                    },
                                ],
                            },
                        },
                    },
                ],
            },
        ],
    },

    plugins: [
        new MiniCssExtractPlugin({
            filename: '[name].css',
        }),
        devMode && new ReactRefreshWebpackPlugin(),
        new CopyPlugin({
            patterns: [
                {
                    from: './public/schema.json',
                    to: '[path][name].json',
                },
            ],
        }),
    ].filter(Boolean),

    resolve: {
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
        alias: {
            js: join(__dirname, 'src/js'),
            images: join(__dirname, 'src/images'),
            icons: join(__dirname, 'src/icons'),
            helpers: join(__dirname, 'src/js/lib/helpers'),
            'date-fns$': resolve(__dirname, 'node_modules/date-fns'), // fixes tree-shaking of date-fns functions (not locales)
            i18next: resolve(
                __dirname,
                'node_modules/i18next/dist/esm/i18next.js',
            ), // Do not bundle cjs/i18next.js

            // Dynamically generated ProseMirror aliases
            ...Object.fromEntries(
                ['keymap', 'model', 'state', 'transform', 'view'].map(
                    (module) => [
                        `prosemirror-${module}$`,
                        resolve(
                            __dirname,
                            `node_modules/prosemirror-${module}/dist/index.js`,
                        ),
                    ],
                ),
            ), // Do not bundle cjs files
        },
    },
}

module.exports = config
