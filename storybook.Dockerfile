FROM node:18-alpine AS build

RUN mkdir /app

# First copy only package.json and yarn.lock to make the dependency fetching step optional.
COPY ./package.json \
     ./yarn.lock \
     /app/

WORKDIR /app
RUN yarn install

# Now copy the whole directory for the build step.
COPY . /app
RUN yarn build-storybook

# Copy static files to nginx container.
FROM nginx:1.27.3-alpine-slim

RUN apk update && apk upgrade --available

# Copy nginx configuration
COPY ./docker/storybook.conf /etc/nginx/conf.d/default.conf

# Copy static files to nginx container
COPY --from=build /app/storybook-static /usr/share/nginx/html

RUN mkdir -p /var/cache/nginx

RUN sed -i 's|pid[[:space:]]*/var/run/nginx.pid;|pid /tmp/nginx.pid;|' /etc/nginx/nginx.conf

# Adjust permissions to ensure the nginx user can access the files
RUN chown -R nginx:nginx /usr/share/nginx/html /var/cache/nginx /etc/nginx

# Run as nginx user (UID 101)
USER nginx

EXPOSE 8080

# Start nginx server
CMD ["nginx", "-g", "daemon off;"]