FROM node:18-alpine

RUN mkdir /app

# First copy only package.json and yarn.lock to make the dependency fetching step optional.
COPY ./package.json \
     ./yarn.lock \
     /app/

WORKDIR /app

RUN yarn install

COPY ./docker/start-dev.sh /
RUN chmod +x /start-dev.sh

# Now copy the whole directory for the build step.
COPY . /app

RUN yarn build

EXPOSE 9001

CMD [ "/start-dev.sh" ]