import React from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import NotFound from 'js/core/NotFound'
import { editFragment } from 'js/magazine-issue/lib/fragments'

import AddEditForm from './AddEditForm/AddEditForm'

const Edit = () => {
    const { t } = useTranslation()
    const { guid } = useParams()

    const { loading, data } = useQuery(GET_ISSUE_EDIT, {
        variables: {
            guid,
        },
    })

    if (loading) return null
    if (!data || !data.entity) return <NotFound />

    const { entity } = data

    return (
        <AddEditForm title={t('entity-magazine.edit-issue')} entity={entity} />
    )
}

const GET_ISSUE_EDIT = gql`
    query MagazineIssueEdit($guid: String!) {
        entity(guid: $guid) {
            guid
            ${editFragment}
        }
    }
`

export default Edit
