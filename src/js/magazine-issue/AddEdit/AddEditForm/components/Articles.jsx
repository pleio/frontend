import React from 'react'
import { DragDropContext, Droppable } from 'react-beautiful-dnd'
import { useFieldArray, useFormContext } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import Setting from 'js/components/Setting'

import Article from './Article'

const Articles = () => {
    const { t } = useTranslation()
    const { control } = useFormContext()

    const { fields, move } = useFieldArray({
        control,
        name: 'articles',
    })

    const onDragEnd = (result) => {
        if (!result.destination) return
        move(result.source.index, result.destination.index)
    }

    return (
        <>
            <Setting helper={t('entity-magazine.articles-helper')} />
            <DragDropContext onDragEnd={onDragEnd}>
                <Droppable droppableId="articles">
                    {(provided) => (
                        <ul
                            {...provided.droppableProps}
                            ref={provided.innerRef}
                            style={{ margin: '8px 0' }}
                        >
                            {fields.map(({ id }, index) => {
                                return (
                                    <Article key={id} id={id} index={index} />
                                )
                            })}
                            {provided.placeholder}
                        </ul>
                    )}
                </Droppable>
            </DragDropContext>
        </>
    )
}

export default Articles
