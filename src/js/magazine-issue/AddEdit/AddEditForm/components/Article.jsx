import React from 'react'
import { Draggable } from 'react-beautiful-dnd'
import { useFormContext, useWatch } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import styled, { css } from 'styled-components'

import IconButton from 'js/components/IconButton/IconButton'

import CoverPageIcon from 'icons/cover-page.svg'
import MoveIcon from 'icons/move-vertical.svg'

const Wrapper = styled.li`
    background-color: white;
    padding: 8px 0;

    ${(p) =>
        p.$isDragging &&
        css`
            border-radius: ${(p) => p.theme.radius.normal};
            box-shadow: ${p.theme.shadow.soft.center};
        `};

    .article-label {
        flex-grow: 1;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        font-weight: ${(p) => p.theme.font.weight.semibold};
    }
`

const Article = ({ id, index }) => {
    const { t } = useTranslation()

    const { control } = useFormContext()

    const watchArticles = useWatch({
        name: 'articles',
        control,
    })

    const title = watchArticles?.[index]?.title ?? ''

    const buttonProps = {
        type: 'button',
        size: 'large',
        variant: 'secondary',
        hoverSize: 'normal',
        radiusStyle: 'rounded',
    }

    const label = `${index + 1}. ${title}`

    const featured = [0, 1, 2].includes(index)

    return (
        <Draggable draggableId={id} index={index}>
            {(provided, snapshot) => (
                <Wrapper
                    ref={provided.innerRef}
                    $isDragging={snapshot.isDragging}
                    {...provided.draggableProps}
                >
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        <IconButton
                            {...buttonProps}
                            as="div"
                            tooltip={t('action.move')}
                            placement="left"
                            aria-label={t('aria.move-field', {
                                label,
                            })}
                            {...provided.dragHandleProps}
                        >
                            <MoveIcon />
                        </IconButton>

                        <div className="article-label">{label}</div>

                        {featured && (
                            <IconButton
                                {...buttonProps}
                                as="div"
                                tooltip={t('entity-magazine.featured-on-cover')}
                                placement="right"
                                aria-label={t(
                                    'entity-magazine.featured-on-cover-aria',
                                    {
                                        label,
                                    },
                                )}
                            >
                                <CoverPageIcon />
                            </IconButton>
                        )}
                    </div>
                </Wrapper>
            )}
        </Draggable>
    )
}

export default Article
