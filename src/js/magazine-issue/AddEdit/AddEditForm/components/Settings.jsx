import React from 'react'
import { useFormContext } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import AccessField from 'js/components/Form/AccessField'
import { Col, Row } from 'js/components/Grid/Grid'

const Settings = () => {
    const { t } = useTranslation()

    const { control } = useFormContext()

    return (
        <Row $spacing={20} $gutter={30}>
            <Col mobileLandscapeUp={1 / 2}>
                <AccessField
                    control={control}
                    name="accessId"
                    label={t('form.visible')}
                />
            </Col>
        </Row>
    )
}

export default Settings
