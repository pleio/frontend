import React, { useState } from 'react'
import { FormProvider, useFieldArray, useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useLocation, useNavigate } from 'react-router-dom'
import { gql, useMutation } from '@apollo/client'
import { formatISO } from 'date-fns'
import styled from 'styled-components'

import ActionContainer from 'js/components/ActionContainer/ActionContainer'
import DeleteModal from 'js/components/EntityActions/DeleteModal'
import ScheduleFields from 'js/components/EntityActions/Schedule/components/ScheduleFields'
import FormItem from 'js/components/Form/FormItem'
import { Col, Container, Row } from 'js/components/Grid/Grid'
import HideVisually from 'js/components/HideVisually/HideVisually'
import Setting from 'js/components/Setting'
import Spacer from 'js/components/Spacer/Spacer'
import Sticky from 'js/components/Sticky/Sticky'
import TabMenu from 'js/components/TabMenu'
import TabPage from 'js/components/TabPage'
import getUrls from 'js/magazine/lib/getUrls'
import refetchQueries from 'js/magazine/lib/refetchQueries'

import Articles from './components/Articles'
import Settings from './components/Settings'

const TabMenuWrapper = styled(Sticky)`
    position: relative;

    &[data-sticky='true'] {
        background: white;
        box-shadow: ${(p) => p.theme.shadow.soft.bottom};
        z-index: 1;
    }
`

const AddEditForm = ({ containerGuid, entity, title }) => {
    const { t } = useTranslation()
    const navigate = useNavigate()
    const location = useLocation()

    const urls = getUrls()

    const isPublished = entity?.statusPublished === 'published'

    const [tab, setTab] = useState('general')

    const [timeScheduled, setTimeScheduled] = useState(
        isPublished ? null : entity?.timePublished || null,
    )
    const [scheduleDeleteEntity, setScheduleDeleteEntity] = useState(
        entity?.scheduleDeleteEntity || '',
    )

    const [showDeleteModal, setShowDeleteModal] = useState(false)
    const toggleDeleteModal = () => setShowDeleteModal(!showDeleteModal)

    const afterDelete = () => {
        navigate(urls.list, {
            replace: true,
        })
    }

    const handleClose = () => {
        navigate(
            location?.state?.prevPathname ||
                (entity && entity.url) ||
                urls.list,
        )
    }

    const handleSaveAsDraft = () => {
        setValue('isDraft', true)
        submitForm()
    }

    const handlePublish = () => {
        setValue('isDraft', false)
        submitForm()
    }

    const [addIssue] = useMutation(ADD_ISSUE)
    const [editIssue] = useMutation(EDIT_ISSUE)

    const submit = async ({
        isDraft,
        issueNumber,
        backgroundColor,
        accessId,
        articles,
        colophon,
    }) => {
        const mutate = entity ? editIssue : addIssue

        await mutate({
            variables: {
                input: {
                    ...(entity
                        ? {
                              guid: entity.guid,
                              articles: articles.map(({ guid }) => guid),
                              isAccessRecursive: true,
                          }
                        : {
                              containerGuid,
                              subtype: 'magazine_issue',
                          }),
                    // Schedule or save as draft
                    ...(isDraft
                        ? { timePublished: timeScheduled || null }
                        : {}),
                    ...(!isDraft && entity?.statusPublished === 'draft'
                        ? {
                              timePublished: formatISO(new Date()),
                          }
                        : {}),
                    accessId,
                    scheduleDeleteEntity: scheduleDeleteEntity || null,
                    issueNumber,
                    backgroundColor,
                    colophon: colophon.map(({ key, value }) => ({
                        key,
                        value,
                    })),
                },
            },
            refetchQueries,
        })
            .then(({ data }) => {
                if (entity) {
                    navigate(data.editEntity.entity.url, {
                        replace: true,
                    })
                } else {
                    navigate(data.addEntity.entity.url, {
                        replace: true,
                    })
                }
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    const defaultValues = {
        issueNumber: entity?.issueNumber || 1,
        backgroundColor: entity?.backgroundColor || '',
        accessId: entity?.accessId,
        articles: entity?.articles || [],
        colophon: entity?.colophon || [],
    }

    const formMethods = useForm({
        mode: 'onChange',
        defaultValues,
    })

    const {
        control,
        handleSubmit,
        setValue,
        getValues,
        formState: { isValid, isSubmitting },
    } = formMethods

    const submitForm = handleSubmit(submit)

    const { fields } = useFieldArray({
        control,
        name: 'colophon',
    })

    return (
        <ActionContainer
            title={title}
            timeScheduled={timeScheduled}
            canSaveAsDraft
            saveAsDraft={handleSaveAsDraft}
            publish={handlePublish}
            isValid={isValid}
            isSavingAsDraft={isSubmitting && getValues('isDraft')}
            isPublishing={isSubmitting && !getValues('isDraft')}
            isPublished={isPublished}
            onDelete={entity ? toggleDeleteModal : null}
            onClose={handleClose}
        >
            <FormProvider {...formMethods}>
                <HideVisually aria-hidden>
                    <FormItem
                        control={control}
                        type="switch"
                        name="isDraft"
                        tabIndex="-1"
                    />
                </HideVisually>

                <TabMenuWrapper>
                    <Container size="small">
                        <TabMenu
                            label={t('entity-magazine.issue-options')}
                            options={[
                                {
                                    onClick: () => setTab('general'),
                                    label: t('global.general'),
                                    isActive: tab === 'general',
                                    id: 'tab-general',
                                    ariaControls: 'tabpanel-general',
                                },
                                {
                                    onClick: () => setTab('settings'),
                                    label: t('global.settings'),
                                    isActive: tab === 'settings',
                                },
                                ...(entity?.articles?.length
                                    ? [
                                          {
                                              onClick: () =>
                                                  setTab('table-of-contents'),
                                              label: t(
                                                  'entity-magazine.table-of-contents',
                                              ),
                                              isActive:
                                                  tab === 'table-of-contents',
                                              id: 'tab-table-of-contents',
                                              ariaControls:
                                                  'tabpanel-table-of-contents',
                                          },
                                      ]
                                    : []),
                                {
                                    onClick: () => setTab('schedule'),
                                    label: t('form.schedule'),
                                    isActive: tab === 'schedule',
                                    id: 'tab-schedule',
                                    ariaControls: 'tabpanel-schedule',
                                },
                            ]}
                            showBorder
                            style={{ position: 'static' }}
                        />
                    </Container>
                </TabMenuWrapper>

                <Container
                    size="small"
                    style={{
                        paddingTop: 24,
                    }}
                >
                    <TabPage
                        id={`tabpanel-general`}
                        aria-labelledby={`tab-general`}
                        visible={tab === 'general'}
                    >
                        <Spacer
                            as="form"
                            spacing="small"
                            onSubmit={handleSubmit(submit)}
                        >
                            <FormItem
                                control={control}
                                type="number"
                                name="issueNumber"
                                label={t('entity-magazine.issue-number')}
                                helper={t(
                                    'entity-magazine.issue-number-helper',
                                )}
                                min={1}
                                max={999}
                                required
                                style={{ width: 180 }}
                            />

                            <FormItem
                                control={control}
                                type="color"
                                name="backgroundColor"
                                title={t('global.background-color')}
                                helper={t(
                                    'entity-magazine.background-color-helper',
                                )}
                                isClearable
                            />

                            {fields.length ? (
                                <>
                                    <Setting
                                        subtitle="entity-magazine.colophon"
                                        helper="entity-magazine.colophon-helper"
                                    />
                                    <Row $spacing={20}>
                                        {fields.map(({ label, id }, index) => {
                                            return (
                                                <Col
                                                    key={id}
                                                    mobileLandscapeUp={1 / 2}
                                                >
                                                    <FormItem
                                                        control={control}
                                                        type="text"
                                                        name={`colophon.${index}.value`}
                                                        label={label}
                                                        // className="ColophonFieldLabel"
                                                    />
                                                </Col>
                                            )
                                        })}
                                    </Row>
                                </>
                            ) : null}
                        </Spacer>
                    </TabPage>

                    <TabPage
                        id={`tabpanel-settings`}
                        aria-labelledby={`tab-settings`}
                        visible={tab === 'settings'}
                    >
                        <Settings />
                    </TabPage>

                    <TabPage
                        id={`tabpanel-table-of-contents`}
                        aria-labelledby={`tab-table-of-contents`}
                        visible={tab === 'table-of-contents'}
                    >
                        <Articles />
                    </TabPage>

                    <TabPage
                        id={`tabpanel-schedule`}
                        aria-labelledby={`tab-schedule`}
                        visible={tab === 'schedule'}
                    >
                        <ScheduleFields
                            canSchedulePublish={!isPublished}
                            timeScheduled={timeScheduled}
                            setTimeScheduled={setTimeScheduled}
                            scheduleDeleteEntity={scheduleDeleteEntity}
                            setScheduleDeleteEntity={setScheduleDeleteEntity}
                        />
                    </TabPage>
                </Container>
            </FormProvider>

            <DeleteModal
                isVisible={showDeleteModal}
                onClose={toggleDeleteModal}
                title={t('entity-magazine.delete-issue')}
                entity={entity}
                afterDelete={afterDelete}
                refetchQueries={refetchQueries}
            />
        </ActionContainer>
    )
}

export const ADD_ISSUE = gql`
    mutation addEntity($input: addEntityInput!) {
        addEntity(input: $input) {
            entity {
                guid
                ... on MagazineIssue {
                    title
                    url
                }
            }
        }
    }
`

const EDIT_ISSUE = gql`
    mutation editEntity($input: editEntityInput!) {
        editEntity(input: $input) {
            entity {
                guid
                ... on MagazineIssue {
                    title
                    url
                    accessId
                }
            }
        }
    }
`

export default AddEditForm
