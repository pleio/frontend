import React from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import NotFound from 'js/core/NotFound'

import AddEditForm from './AddEditForm/AddEditForm'

const Add = () => {
    const { t } = useTranslation()
    const { containerGuid } = useParams()

    const { data, loading } = useQuery(GET_ISSUE_ADD, {
        variables: {
            guid: containerGuid,
        },
    })

    if (loading) return null

    const { entity } = data || {}

    if (!entity?.canEdit) return <NotFound />

    return (
        <AddEditForm
            containerGuid={entity.guid}
            title={t('entity-magazine.create-issue')}
        />
    )
}

const GET_ISSUE_ADD = gql`
    query AddIssue($guid: String!) {
        entity(guid: $guid) {
            guid
            ... on Magazine {
                canEdit
            }
        }
    }
`

export default Add
