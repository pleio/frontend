import React from 'react'
import { Route, Routes } from 'react-router-dom'

import NotFound from 'js/core/NotFound'
import Add from 'js/magazine-issue/AddEdit/Add'
import Edit from 'js/magazine-issue/AddEdit/Edit'
import AddArticle from 'js/magazine-issue/AddEditArticle/Add'
import EditArticle from 'js/magazine-issue/AddEditArticle/Edit'
import View from 'js/magazine-issue/View/View'

const MagazineIssueRoutes = () => (
    <Routes>
        {/* Issue */}
        <Route path="/add/:containerGuid" element={<Add />} />
        <Route path="/edit/:guid" element={<Edit />} />

        {/* Article */}
        <Route
            path="/view/:containerGuid/add-article"
            element={<AddArticle />}
        />
        <Route path="/edit/:containerGuid/:guid" element={<EditArticle />} />

        {/* View magazine issue */}
        <Route element={<View />}>
            {/* Pass data via RouterContext? */}
            <Route path="/view/:containerGuid" element={<div />} />
            <Route path="/view/:containerGuid/colophon" element={<div />} />
            <Route path="/view/:containerGuid/:guid" element={<div />} />
            <Route path="/view/:containerGuid/:guid/:slug" element={<div />} />
        </Route>

        <Route element={<NotFound />} />
    </Routes>
)

export default MagazineIssueRoutes
