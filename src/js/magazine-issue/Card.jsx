import React from 'react'
import styled from 'styled-components'

import Card, { CardContent } from 'js/components/Card/Card'
import CoverImage from 'js/components/CoverImage'
import DisplayDate from 'js/components/DisplayDate'
import FeedItem from 'js/components/FeedItem/FeedItem'
import ActionsButton from 'js/magazine-issue/View/components/ActionsButton'
import ThemeProvider from 'js/theme/ThemeProvider'

const Wrapper = styled(Card)`
    flex-grow: 1; // Fill out vertically
    border-radius: 12px;

    .issue-card--featured-image {
        overflow: hidden;
        border-radius: ${(p) => p.theme.radius.large};
        margin-bottom: 16px;
    }

    .issue-card--meta {
        display: flex;
    }

    .issue-card--title {
        flex-grow: 1;
        font-family: ${(p) => p.theme.fontHeading.family};
        font-size: ${(p) => p.theme.fontHeading.size.large};
        line-height: ${(p) => p.theme.fontHeading.lineHeight.large};
        font-weight: ${(p) => p.theme.fontHeading.weight.bold};
    }

    .issue-card--date {
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
    }

    .issue-card--table-of-contents {
        padding-left: 12px;
        margin-top: 12px;
        list-style: disc;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};

        li {
            padding: 4px 0;
        }
    }
`

const IssueCardWrapper = ({
    showTheme,
    themeColor,
    backgroundColor,
    children,
    ...rest
}) => {
    return showTheme ? (
        <ThemeProvider
            theme={themeColor}
            style={{ display: 'flex', flexDirection: 'column' }} // Fill out vertically
        >
            <Wrapper as={Card} $backgroundColor={backgroundColor} {...rest}>
                {children}
            </Wrapper>
        </ThemeProvider>
    ) : (
        <Wrapper as={FeedItem} {...rest}>
            {children}
        </Wrapper>
    )
}

const IssueCard = ({ entity, showTheme, dataFeed }) => {
    const { url, issueNumber, timePublished, articles, container } = entity

    const featuredArticle = articles[0]

    const { featured } = featuredArticle || {}

    const backgroundColor =
        entity.backgroundColor || featuredArticle?.backgroundColor

    const themeColor = backgroundColor || '#FFFFFF'

    const title = showTheme ? issueNumber : `${container.title} ${issueNumber}`

    return (
        <IssueCardWrapper
            showTheme={showTheme}
            themeColor={themeColor}
            backgroundColor={backgroundColor}
            data-feed={dataFeed}
        >
            <CardContent>
                <CoverImage
                    url={url}
                    openUrlInNewWindow
                    hideCaption
                    featured={featured}
                    size="landscape-large"
                    className="issue-card--featured-image"
                />

                <div className="issue-card--meta">
                    <a
                        href={url}
                        target="_blank"
                        rel="noopener noreferrer"
                        className="issue-card--title"
                    >
                        {title}
                    </a>
                    <ActionsButton entity={entity} />
                </div>
                <DisplayDate
                    date={timePublished}
                    type="date"
                    disableTooltip
                    className="issue-card--date"
                />

                {articles.length ? (
                    <ul className="issue-card--table-of-contents">
                        {articles.slice(0, 3).map(({ guid, title }) => (
                            <li key={guid}>{title}</li>
                        ))}
                    </ul>
                ) : null}
            </CardContent>
        </IssueCardWrapper>
    )
}

export default IssueCard
