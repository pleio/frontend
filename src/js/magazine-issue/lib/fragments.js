import { featuredViewFragment } from 'js/lib/fragments/featured'
import { iconViewFragment } from 'js/lib/fragments/icon'

import { viewFragment as articleViewFragment } from './articleFragments'

const sharedFields = `
    url
    canEdit
    statusPublished
    canArchiveAndDelete
    issueNumber
    backgroundColor
`

export const listFragment = `
... on MagazineIssue {
    ${sharedFields}
    timePublished
    timeUpdated
    owner {
        guid
        name
    }
    ${featuredViewFragment}
    container {
        guid
        ... on Magazine {
            title
        }
    }
    articles {
        guid
        ... on Blog {
            title
            url
            backgroundColor
            ${featuredViewFragment}
        }
    }
}
`

export const editFragment = `
... on MagazineIssue {
    ${sharedFields}
    accessId
    timePublished
    scheduleDeleteEntity
    articles {
        __typename @include(if: false)
        guid
        ... on Blog {
            title
        }
    }
    colophon {
        __typename @include(if: false)
        key
        label
        value
    }
}
`

export const viewFragment = `
... on MagazineIssue {
    ${sharedFields}
    timePublished
    container {
        guid
        ... on Magazine {
            title
            url
            subtitle
            ${iconViewFragment}
        }
    }
    colophon {
        key
        value
        label
    }
    articles {
        guid
        ${articleViewFragment}
    }
}
`
