export default function (guid) {
    return {
        ...(guid
            ? {
                  edit: `/magazine-issue/edit/${guid}`,
                  addArticle: `/magazine-issue/view/${guid}/add-article`,
              }
            : {}),
    }
}
