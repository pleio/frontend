import {
    featuredEditFragment,
    featuredViewFragment,
} from 'js/lib/fragments/featured'

const sharedFields = `
    title
    url
    richDescription
    backgroundColor
    canEdit
    statusPublished
    canArchiveAndDelete
    inputLanguage
    isTranslationEnabled
`

export const editFragment = `
... on Blog {
    abstract
    ${sharedFields}
    ${featuredEditFragment}
}
`

export const viewFragment = `
... on Blog {
    ${sharedFields}
    ${featuredViewFragment}
    localTitle
    localRichDescription
    localDescription
    abstract
    owner {
        __typename @include(if: false)
        guid
        username
        name
        icon
        url
    }
}
`
