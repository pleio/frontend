import React from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import NotFound from 'js/core/NotFound'
import { editFragment } from 'js/magazine-issue/lib/articleFragments'

import AddEditForm from './AddEditForm'

const Edit = () => {
    const { t } = useTranslation()
    const { containerGuid, guid } = useParams()

    const { loading, data } = useQuery(GET_ARTICLE_EDIT, {
        variables: {
            containerGuid,
            guid,
        },
    })

    if (loading) return null
    if (!data || !data.entity) return <NotFound />

    const { site, viewer, entity, container } = data

    return (
        <AddEditForm
            title={t('entity-magazine.edit-article')}
            site={site}
            viewer={viewer}
            container={container}
            entity={entity}
        />
    )
}

const GET_ARTICLE_EDIT = gql`
    query MagazineArticleEdit($containerGuid: String!, $guid: String!) {
        site {
            guid
            maxCharactersInAbstract
        }
        viewer {
            guid
            canInsertMedia(subtype: "blog")
        }
        container: entity(guid: $containerGuid) {
            guid
            ... on MagazineIssue {
                canEdit
            }
        }
        entity(guid: $guid) {
            guid
            ${editFragment}
        }
    }
`

export default Edit
