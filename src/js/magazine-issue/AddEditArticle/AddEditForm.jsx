import React, { useRef, useState } from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useLocation, useNavigate } from 'react-router-dom'
import { gql, useMutation } from '@apollo/client'

import ActionContainer from 'js/components/ActionContainer/ActionContainer'
import DeleteModal from 'js/components/EntityActions/DeleteModal'
import General from 'js/components/EntityActions/General/General'
import FormItem from 'js/components/Form/FormItem'
import { Container } from 'js/components/Grid/Grid'
import { MAX_LENGTH } from 'js/lib/constants'

const AddEditForm = ({
    site,
    viewer,
    container,
    entity,
    title,
    subtype = 'blog',
}) => {
    const { t } = useTranslation()
    const navigate = useNavigate()
    const location = useLocation()
    const refFeatured = useRef()

    const isPublished = entity?.statusPublished === 'published'

    const [showDeleteModal, setShowDeleteModal] = useState(false)
    const toggleDeleteModal = () => setShowDeleteModal(!showDeleteModal)

    const afterDelete = () => {
        navigate(container.url, {
            replace: true,
        })
    }

    const handleClose = () => {
        navigate(location?.state?.prevPathname || container.url)
    }

    const [addIssue] = useMutation(ADD_ISSUE)
    const [editIssue] = useMutation(EDIT_ISSUE)

    const submit = async ({
        title,
        abstract,
        richDescription,
        inputLanguage,
        isTranslationEnabled,
        backgroundColor,
    }) => {
        const mutate = entity ? editIssue : addIssue

        await mutate({
            variables: {
                input: {
                    ...(entity
                        ? { guid: entity.guid }
                        : {
                              containerGuid: container.guid,
                              subtype,
                          }),
                    title,
                    abstract,
                    featured:
                        refFeatured.current && refFeatured.current.getValue(),
                    richDescription,
                    backgroundColor,
                    inputLanguage,
                    isTranslationEnabled,
                },
            },
        })
            .then(({ data }) => {
                if (entity) {
                    navigate(data.editEntity.entity.url, {
                        replace: true,
                    })
                } else {
                    navigate(data.addEntity.entity.url, {
                        replace: true,
                    })
                }
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    const defaultValues = {
        title: entity?.title || null,
        abstract: entity?.abstract || null,
        richDescription: entity?.richDescription || null,
        backgroundColor: entity?.backgroundColor || '',
        inputLanguage: entity?.inputLanguage || site?.language,
        isTranslationEnabled: entity ? entity.isTranslationEnabled : true, // Default to true
    }

    const formMethods = useForm({
        mode: 'onChange',
        defaultValues,
    })

    const {
        control,
        handleSubmit,
        formState: { errors, isValid, isSubmitting },
    } = formMethods

    const submitForm = handleSubmit(submit)

    const { canInsertMedia } = viewer

    const canAccess = {
        abstract: true,
        featured: canInsertMedia,
        backgroundColor: true,
    }

    return (
        <ActionContainer
            title={title}
            publish={submitForm}
            isValid={isValid}
            isPublishing={isSubmitting}
            isPublished={isPublished}
            onDelete={entity ? toggleDeleteModal : null}
            onClose={handleClose}
        >
            <FormProvider {...formMethods}>
                <Container size="small" style={{ marginBottom: 24 }}>
                    <FormItem
                        control={control}
                        type="text"
                        name="title"
                        label={t('form.title')}
                        size="large"
                        errors={errors}
                        required
                        maxLength={MAX_LENGTH.title}
                    />
                </Container>

                <General
                    visible
                    ref={refFeatured}
                    canAccess={canAccess}
                    entity={entity}
                    site={site}
                    viewer={viewer}
                    subtype={subtype}
                    featuredFullHeight
                />
            </FormProvider>

            <DeleteModal
                isVisible={showDeleteModal}
                onClose={toggleDeleteModal}
                title={t('entity-magazine.delete-issue')}
                entity={entity}
                afterDelete={afterDelete}
            />
        </ActionContainer>
    )
}

export const ADD_ISSUE = gql`
    mutation addEntity($input: addEntityInput!) {
        addEntity(input: $input) {
            entity {
                guid
                ... on Blog {
                    title
                    url
                }
            }
        }
    }
`

const EDIT_ISSUE = gql`
    mutation editEntity($input: editEntityInput!) {
        editEntity(input: $input) {
            entity {
                guid
                ... on Blog {
                    title
                    url
                }
            }
        }
    }
`

export default AddEditForm
