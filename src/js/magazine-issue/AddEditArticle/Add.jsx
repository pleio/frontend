import React from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import NotFound from 'js/core/NotFound'

import AddEditForm from './AddEditForm'

const Add = () => {
    const { t } = useTranslation()
    const { containerGuid } = useParams()

    const { data, loading } = useQuery(GET_ARTICLE_ADD, {
        variables: {
            containerGuid,
        },
    })

    if (loading) return null

    const { site, viewer, entity } = data || {}

    if (!entity?.canEdit) return <NotFound />

    return (
        <AddEditForm
            site={site}
            viewer={viewer}
            container={entity}
            title={t('entity-magazine.create-article')}
        />
    )
}

const GET_ARTICLE_ADD = gql`
    query AddIssue($containerGuid: String!) {
        site {
            guid
            maxCharactersInAbstract
        }
        viewer {
            guid
            canInsertMedia(subtype: "blog")
        }
        entity(guid: $containerGuid) {
            guid
            ... on MagazineIssue {
                url
                canEdit
            }
        }
    }
`

export default Add
