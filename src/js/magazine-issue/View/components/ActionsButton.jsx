import React from 'react'
import { useTranslation } from 'react-i18next'

import ItemActions from 'js/components/Item/ItemActions/ItemActions'
import getUrls from 'js/magazine-issue/lib/getUrls'
import refetchQueries from 'js/magazine-issue/lib/refetchQueries'

const ActionsButton = ({ entity, smallButton, ...rest }) => {
    const { t } = useTranslation()

    const { guid, canEdit } = entity

    const urls = getUrls(guid)

    return (
        <ItemActions
            entity={entity}
            smallButton={smallButton}
            onEdit={urls.edit}
            onAfterArchive={urls.list}
            onAfterDelete={urls.list}
            refetchQueries={refetchQueries}
            customOptions={[
                [
                    ...(canEdit
                        ? [
                              {
                                  name: t('entity-magazine.create-article'),
                                  to: urls.addArticle,
                                  state: {
                                      prevPathname: location.pathname,
                                  },
                              },
                          ]
                        : []),
                ],
            ]}
            {...rest}
        />
    )
}

export default ActionsButton
