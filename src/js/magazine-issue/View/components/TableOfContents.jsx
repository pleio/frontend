import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import CoverImage from 'js/components/CoverImage'
import Spacer from 'js/components/Spacer/Spacer'
import TiptapView from 'js/components/Tiptap/TiptapView'
import HeaderLogo from 'js/magazine/View/components/HeaderLogo'

import ArticleIcon from 'icons/article.svg'
import CoverPageIcon from 'icons/cover-page.svg'
import InfoIcon from 'icons/info.svg'

const Wrapper = styled.div`
    padding: 24px 20px 20px;
    max-width: 540px;
    max-height: calc(100vh - 88px);
    overflow-y: auto;

    .toc-list {
        margin-top: 20px;
    }

    .toc-item {
        display: flex;
        border-radius: ${(p) => p.theme.radius.large};

        &:hover {
            background-color: ${(p) => p.theme.menu.hover};
        }

        &:active {
            background-color: ${(p) => p.theme.menu.active};
        }
    }

    .toc-item-icon,
    .toc-item-featured {
        overflow: hidden;
        flex-shrink: 0;
        background-color: ${(p) => p.theme.color.grey['10-alpha']};
        width: 60px;
        height: 60px;
        border-radius: ${(p) => p.theme.radius.large};
        margin-right: 16px;
    }

    .toc-item-icon {
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .toc-item-text {
        flex-grow: 1;
        display: flex;
        flex-direction: column;
        justify-content: center; // Center text vertically
    }

    .toc-item-title {
        font-size: ${(p) => p.theme.font.size.normal};
        line-height: ${(p) => p.theme.font.lineHeight.normal};
    }
`

const TableOfContents = ({ entity, ...rest }) => {
    const { t } = useTranslation()

    const { url, issueNumber, timePublished, container, articles, colophon } =
        entity

    const hasColophon = colophon.some((c) => c.value)

    return (
        <Wrapper {...rest}>
            <HeaderLogo
                icon={container.icon}
                title={container.title}
                number={issueNumber}
                subtitle={container.subtitle}
                date={timePublished}
                isSmall
            />
            <Spacer spacing="small" as="ul" className="toc-list">
                <li>
                    <ListItem
                        url={url}
                        title={t('entity-magazine.cover-page')}
                        icon={<CoverPageIcon />}
                    />
                </li>
                {articles.map(({ guid, url, featured, title, abstract }) => (
                    <li key={guid}>
                        <ListItem
                            url={url}
                            title={title}
                            abstract={abstract}
                            featured={featured}
                        />
                    </li>
                ))}
                {hasColophon && (
                    <li>
                        <ListItem
                            url={`${url}colophon`}
                            title={t('entity-magazine.colophon')}
                            icon={<InfoIcon />}
                        />
                    </li>
                )}
            </Spacer>
        </Wrapper>
    )
}

const ListItem = ({ url, title, abstract, icon, featured, ...rest }) => (
    <Link to={url} className="toc-item" {...rest}>
        {featured?.image || featured?.video ? (
            <CoverImage
                featured={{
                    // Exclude caption
                    image: featured.image,
                    alt: featured.alt,
                    video: featured.video,
                    videoTitle: featured.videoTitle,
                    videoThumbnailUrl: featured.videoThumbnailUrl,
                }}
                size="cover"
                videoButtonSize="small"
                className="toc-item-featured"
            />
        ) : (
            <div className="toc-item-icon">{icon || <ArticleIcon />}</div>
        )}

        <div className="toc-item-text">
            <h2 className="toc-item-title">{title}</h2>
            {abstract && <TiptapView content={abstract} textSize="small" />}
        </div>
    </Link>
)

export default TableOfContents
