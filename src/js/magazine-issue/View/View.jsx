import React, { useEffect } from 'react'
import { Link, useMatch, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { t } from 'i18next'
import { transparentize } from 'polished'
import styled from 'styled-components'

import DisplayDate from 'js/components/DisplayDate'
import Document from 'js/components/Document'
import Flexer from 'js/components/Flexer/Flexer'
import { Container } from 'js/components/Grid/Grid'
import IconButton from 'js/components/IconButton/IconButton'
import ItemStatusTag from 'js/components/Item/ItemStatusTag'
import Popover from 'js/components/Popover/Popover'
import NotFound from 'js/core/NotFound'
import { useIsMount } from 'js/lib/helpers'
import HeaderLogo from 'js/magazine/View/components/HeaderLogo'
import { viewFragment } from 'js/magazine-issue/lib/fragments'
import ArticlePage from 'js/magazine-issue/View/views/ArticlePage'
import ColophonPage from 'js/magazine-issue/View/views/ColophonPage'
import CoverPage from 'js/magazine-issue/View/views/CoverPage/CoverPage'
import ThemeProvider from 'js/theme/ThemeProvider'

import ArrowCircleLeftIcon from 'icons/arrow-circle-left-large.svg'
import ArrowCircleRightIcon from 'icons/arrow-circle-right-large.svg'
import TocCircleIcon from 'icons/toc-circle.svg'

import ActionsButton from './components/ActionsButton'
import TableOfContents from './components/TableOfContents'

const Wrapper = styled.div`
    flex-grow: 1;
    display: flex;
    flex-direction: column;

    .layout--theme-provider {
        flex-grow: 1;
        display: flex;
        flex-direction: column;
    }
`

const StyledHeader = styled.header`
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    z-index: 1;
    min-height: 88px;
    background-color: ${(p) => transparentize(0.05, p.$themeColor)};

    .layout-header--container {
        position: relative;
        display: flex;
        align-items: flex-start;
        justify-content: center;
    }

    .layout-header--logo {
        position: absolute;
        top: 24px;
        left: 0;
        right: 0;
        display: flex;
        justify-content: center;
        pointer-events: none;

        > * {
            pointer-events: auto;
        }
    }

    .layout-header--actions-left,
    .layout-header--actions-right {
        position: absolute;
        top: 16px;
    }

    .layout-header--actions-left {
        left: 20px;
        display: flex;
        align-items: center;
    }

    .layout-header--actions-right {
        right: 20px;

        [disabled] {
            visibility: hidden;
        }
    }

    .layout-header--meta-wrapper {
        display: flex;
        align-items: center;
        margin-left: 8px;
    }

    .layout-header--meta {
        a:hover,
        a:active {
            text-decoration: underline;
        }

        > *:not(:first-child):before {
            content: '  •  ';
            white-space: pre;
        }
    }
`

const LayoutWrapper = () => {
    const { containerGuid } = useParams()

    const { loading, data, refetch } = useQuery(GET_MAGAZINE_ISSUE, {
        variables: {
            guid: containerGuid,
        },
    })

    const isMount = useIsMount()
    useEffect(() => {
        if (isMount) {
            // Fetch all articles after creating a new article
            refetch()
        }
    }, [isMount, refetch])

    if (loading) return null
    if (!data || !data.entity) return <NotFound />

    return <Layout entity={data.entity} />
}

const Layout = ({ entity }) => {
    const params = useParams()
    const articleGuid = params.guid

    const {
        url,
        issueNumber,
        container,
        articles,
        timePublished,
        statusPublished,
        backgroundColor,
    } = entity

    const colophon = entity.colophon.filter((field) => field.value)

    const pathIsHome = !!useMatch({ path: url, end: true })

    const colophonUrl = `${url}colophon`
    const pathIsColophon = !!useMatch({ path: colophonUrl, end: true })

    const currentArticle = articles.find((a) => a.guid === articleGuid) || null
    const currentArticleIndex = currentArticle
        ? articles.indexOf(currentArticle)
        : null

    const getPrev = () => {
        const prevArticle =
            articles[
                pathIsColophon ? articles.length - 1 : currentArticleIndex - 1
            ] || null

        return prevArticle
            ? {
                  url: prevArticle.url,
                  title: prevArticle.title,
              }
            : pathIsHome
              ? null
              : {
                    url,
                    title: t('entity-magazine.cover-page'),
                }
    }

    const getNext = () => {
        const currentArticleIndexIsNumber =
            typeof currentArticleIndex === 'number'

        const nextArticle =
            articles[
                pathIsHome
                    ? 0
                    : currentArticleIndexIsNumber
                      ? currentArticleIndex + 1
                      : null
            ] || null

        return nextArticle
            ? {
                  url: nextArticle.url,
                  title: nextArticle.title,
              }
            : pathIsColophon
              ? null
              : colophon.length
                ? {
                      url: colophonUrl,
                      title: t('entity-magazine.colophon'),
                  }
                : null
    }

    const prev = getPrev()
    const next = getNext()

    const themeColor = pathIsHome
        ? backgroundColor || articles[0]?.backgroundColor || '#FFFFFF'
        : currentArticle?.backgroundColor || backgroundColor || '#FFFFFF'

    const buttonProps = {
        size: 'huge',
        variant: 'secondary',
        radiusStyle: 'rounded',
        placement: 'bottom-end',
    }

    return (
        <Wrapper>
            <ThemeProvider
                theme={themeColor}
                bodyColor={themeColor}
                className="layout--theme-provider"
            >
                <Document title={''} />
                <StyledHeader $themeColor={themeColor}>
                    <Container size="huge" className="layout-header--container">
                        <div className="layout-header--actions-left">
                            <Popover
                                content={<TableOfContents entity={entity} />}
                            >
                                <IconButton
                                    {...buttonProps}
                                    tooltip={t(
                                        'entity-magazine.table-of-contents',
                                    )}
                                >
                                    <TocCircleIcon />
                                </IconButton>
                            </Popover>

                            {!pathIsHome && (
                                <div className="layout-header--meta-wrapper">
                                    <div className="layout-header--meta">
                                        <Link to={url}>
                                            {container.title} {issueNumber}
                                        </Link>
                                        {statusPublished === 'published' && (
                                            <span>
                                                <DisplayDate
                                                    date={timePublished}
                                                    type="date"
                                                    disableTooltip
                                                />
                                            </span>
                                        )}
                                    </div>

                                    <ItemStatusTag
                                        entity={entity}
                                        style={{ marginLeft: 8 }}
                                    />

                                    <ActionsButton
                                        entity={entity}
                                        smallButton
                                        style={{ marginLeft: 4 }}
                                    />
                                </div>
                            )}
                        </div>
                        {pathIsHome && (
                            <div className="layout-header--logo">
                                <HeaderLogo
                                    icon={container.icon}
                                    title={container.title}
                                    number={issueNumber}
                                    subtitle={container.subtitle}
                                    date={timePublished}
                                >
                                    <ItemStatusTag
                                        entity={entity}
                                        style={{ marginLeft: 8 }}
                                    />
                                    <ActionsButton
                                        entity={entity}
                                        style={{ marginLeft: 4 }}
                                    />
                                </HeaderLogo>
                            </div>
                        )}
                        <Flexer
                            gutter="none"
                            className="layout-header--actions-right"
                        >
                            <IconButton
                                {...buttonProps}
                                as={prev && Link}
                                to={prev?.url}
                                tooltip={prev?.title}
                                aria-label={`${t('action.previous')}: ${prev?.title}`}
                                disabled={!prev}
                            >
                                <ArrowCircleLeftIcon />
                            </IconButton>
                            <IconButton
                                {...buttonProps}
                                as={next && Link}
                                to={next?.url}
                                tooltip={next?.title}
                                aria-label={`${t('action.next')}: ${next?.title}`}
                                disabled={!next}
                            >
                                <ArrowCircleRightIcon />
                            </IconButton>
                        </Flexer>
                    </Container>
                </StyledHeader>
                {pathIsHome ? (
                    <CoverPage entity={entity} />
                ) : pathIsColophon ? (
                    <ColophonPage colophon={colophon} />
                ) : currentArticle ? (
                    <ArticlePage container={entity} entity={currentArticle} />
                ) : null}
            </ThemeProvider>
        </Wrapper>
    )
}

const GET_MAGAZINE_ISSUE = gql`
    query MagazineIssue(
        $guid: String!
    ) {
        entity(guid: $guid) {
            guid
            ${viewFragment}
        }
    }
`

export default LayoutWrapper
