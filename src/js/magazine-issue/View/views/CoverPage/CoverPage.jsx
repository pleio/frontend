import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import Container from 'js/components/Grid/Container'
import Spacer from 'js/components/Spacer/Spacer'
import Text from 'js/components/Text/Text'
import { media } from 'js/lib/helpers'
import getUrls from 'js/magazine-issue/lib/getUrls'

import CoverArticle from './components/CoverArticle'

const NoArticles = styled(Spacer)`
    flex-grow: 1;
    padding: 140px 20px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`

const Wrapper = styled(Container)`
    .cover-page--grid {
        min-height: 100vh;
        display: grid;
        padding: 108px 40px 40px 40px;
        gap: 40px;

        ${media.mobilePortraitUp`
            /* grid-template-columns: 1fr; */
        `}

        ${media.mobileLandscapeUp`
            grid-template-areas:
                'a b'
                'a c';
            grid-template-rows: 1fr 1fr;
            grid-template-columns: 1fr 1fr;

            > :nth-child(1) {
                grid-area: ${(p) => (p.$articlesCount > 1 ? 'a' : 'a / a / c / b')};
            }

            > :nth-child(2) {
                grid-area: ${(p) => (p.$articlesCount > 2 ? 'b' : 'b / c / c')};
            }

            > :nth-child(3) {
                grid-area: c;
            }
        `}

        ${media.desktopUp`
            grid-template-columns: 2fr 1fr;
        `};
    }
`

const CoverPage = ({ entity }) => {
    const { t } = useTranslation()

    const { guid, articles, backgroundColor } = entity

    const urls = getUrls(guid)

    if (!articles.length) {
        return (
            <NoArticles>
                <Text>{t('entity-magazine.no-articles')}</Text>
                <Button
                    as={Link}
                    to={urls.addArticle}
                    variant="secondary"
                    size="large"
                >
                    {t('entity-magazine.create-article')}
                </Button>
            </NoArticles>
        )
    }

    return (
        <Wrapper size="huge" $noPadding $articlesCount={articles.length}>
            <div className="cover-page--grid">
                {articles.slice(0, 3).map((entity, index) => (
                    <CoverArticle
                        key={entity.guid}
                        index={index}
                        entity={entity}
                        issueBackgroundColor={backgroundColor}
                    />
                ))}
            </div>
        </Wrapper>
    )
}

export default CoverPage
