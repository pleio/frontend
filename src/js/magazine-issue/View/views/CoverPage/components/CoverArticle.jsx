import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import styled, { css } from 'styled-components'

import Button from 'js/components/Button/Button'
import CoverImage from 'js/components/CoverImage'
import { getReadableColor } from 'js/lib/helpers/getContrast'

const StyledCoverImage = styled(CoverImage)`
    height: 100%;
    margin: 0;
    padding: 0;
`

const Wrapper = styled(Link)`
    ${(p) =>
        p.$index === 0 &&
        css`
            .cover-page--article-overlay {
                padding: 40px;
            }

            .cover-page--article-title {
                max-width: 660px;
                font-size: ${(p) => p.theme.fontHeading.size.huge};
                line-height: ${(p) => p.theme.fontHeading.lineHeight.huge};
            }

            .cover-page--article-read-button {
                margin-top: 32px;
            }
        `};

    ${(p) =>
        p.$index > 0 &&
        css`
            .cover-page--article-overlay {
                padding: 32px;
            }

            .cover-page--article-title {
                font-size: ${(p) => p.theme.fontHeading.size.normal};
                line-height: ${(p) => p.theme.fontHeading.lineHeight.normal};
            }

            .cover-page--article-read-button {
                margin-top: 24px;
            }
        `};

    .cover-page--article-title {
        font-family: ${(p) => p.theme.fontHeading.family};
    }

    .cover-page--article-content {
        position: relative;
        width: 100%;
        height: 100%;
        display: flex;
        flex-direction: column;
        justify-content: flex-end;
        border-radius: 16px;
        overflow: hidden;

        .cover-page--article-canvas,
        figure {
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            z-index: -1;
        }
    }

    .cover-page--article-overlay {
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        padding-top: 80px;
        background: linear-gradient(
            180deg,
            rgba(0, 0, 0, 0) 0,
            rgba(0, 0, 0, 0.42) 80px,
            rgba(0, 0, 0, 0.42) 100%
        );
        color: white;
    }

    .cover-page--article-read-button {
        background-color: ${(p) => p.$buttonBackgroundColor};
        color: ${(p) => getReadableColor(p.$buttonBackgroundColor)};
    }
`

const CoverArticle = ({ index, entity, issueBackgroundColor }) => {
    const { t } = useTranslation()

    const { title, url, featured } = entity

    const isSmallCard = index > 0

    return (
        <Wrapper
            to={url}
            $index={index}
            $buttonBackgroundColor={issueBackgroundColor || '#FFFFFF'}
        >
            <div className="cover-page--article-content">
                <StyledCoverImage
                    hideCaption
                    featured={featured}
                    size="cover"
                />
                <div className="cover-page--article-overlay">
                    <h2 className="cover-page--article-title">{title}</h2>
                    <Button
                        variant="primary"
                        size={isSmallCard ? 'normal' : 'large'}
                        as="div"
                        className="cover-page--article-read-button"
                        aria-hidden
                    >
                        {t('entity-magazine.read-article')}
                    </Button>
                </div>
            </div>
        </Wrapper>
    )
}

export default CoverArticle
