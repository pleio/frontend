import React from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import styled from 'styled-components'

import CoverImage from 'js/components/CoverImage'
import { Container } from 'js/components/Grid/Grid'
import { H1 } from 'js/components/Heading'
import ItemActions from 'js/components/Item/ItemActions/ItemActions'
import Author from 'js/components/Item/ItemHeader/components/Author'
import ItemStatusTag from 'js/components/Item/ItemStatusTag'
import Section from 'js/components/Section/Section'
import TiptapView from 'js/components/Tiptap/TiptapView'

const Wrapper = styled(Section)`
    padding: 100px 0;

    .article-header--title {
        max-width: 900px;
        margin: 0 auto;
        font-size: ${(p) => p.theme.fontHeading.size.enormous};
        line-height: ${(p) => p.theme.fontHeading.lineHeight.enormous};
        text-align: center;
        text-wrap: balance;
    }

    .article-header--actions {
        display: inline-block;
        vertical-align: middle;
        margin-left: 8px;
        margin-bottom: 8px;
    }

    .article-header--author {
        margin-top: 24px;
        justify-content: center;
    }

    .article-header--cover-image {
        margin-top: 40px;

        img {
            max-height: 80vh;
        }
    }

    .article-content {
        margin-top: 72px;
    }
`

const ArticlePage = ({ container, entity }) => {
    const params = useParams()
    const navigate = useNavigate()

    const { guid, title, richDescription, featured } = entity

    const onEdit = `/magazine-issue/edit/${params.containerGuid}/${guid}`

    const afterDelete = () => {
        navigate(container.url, {
            replace: true,
        })
    }

    return (
        <Wrapper grow>
            <Container size="huge" className="article-header--container">
                <H1 className="article-header--title">
                    {title}
                    <ItemActions
                        entity={entity}
                        onEdit={onEdit}
                        onAfterDelete={afterDelete}
                        hideArchive
                        className="article-header--actions"
                        refetchQueries={['MagazineIssue', 'MagazineView']}
                    />
                    <ItemStatusTag
                        entity={entity}
                        style={{
                            marginTop: '8px',
                        }}
                    />
                </H1>

                <Author entity={entity} className="article-header--author" />

                <CoverImage
                    featured={featured}
                    size="full"
                    borderRadius="16px"
                    className="article-header--cover-image"
                />
            </Container>

            <Container size="normal">
                <TiptapView
                    content={richDescription}
                    textSize="large"
                    className="article-content"
                    dropCaps
                />
            </Container>
        </Wrapper>
    )
}

export default ArticlePage
