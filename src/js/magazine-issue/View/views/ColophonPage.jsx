import React from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import { Col, Container, Row } from 'js/components/Grid/Grid'
import { H1 } from 'js/components/Heading'
import Section from 'js/components/Section/Section'

const Wrapper = styled(Section)`
    padding: 100px 0;

    .colophon--title {
        font-size: ${(p) => p.theme.fontHeading.size.enormous};
        line-height: ${(p) => p.theme.fontHeading.lineHeight.enormous};
        text-align: center;
        text-wrap: balance;
        margin-bottom: 80px;
    }

    .colophon--label {
        font-size: ${(p) => p.theme.font.size.normal};
        line-height: ${(p) => p.theme.font.lineHeight.normal};
        color: ${(p) => p.theme.color.text.grey};
    }
    .colophon--value {
        font-size: ${(p) => p.theme.font.size.large};
        line-height: ${(p) => p.theme.font.lineHeight.large};
    }
`

const ColophonPage = ({ colophon }) => {
    const { t } = useTranslation()

    return (
        <Wrapper grow>
            <Container size="normal">
                <H1 className="colophon--title">
                    {t('entity-magazine.colophon')}
                </H1>
                <Row as="ul" $gutter={40} $spacing={40}>
                    {colophon.map(({ key, label, value }) => (
                        <Col as="li" key={key} mobileLandscapeUp={1 / 2}>
                            <div className="colophon--label">{label}</div>
                            <div className="colophon--value">{value}</div>
                        </Col>
                    ))}
                </Row>
            </Container>
        </Wrapper>
    )
}

export default ColophonPage
