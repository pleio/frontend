import React from 'react'
import { useTranslation } from 'react-i18next'

import EntityAddEditForm from 'js/components/EntityActions/EntityAddEditForm'

const Add = () => {
    const refetchQueries = ['TasksList']
    const { t } = useTranslation()

    return (
        <EntityAddEditForm
            title={t('entity-task.create')}
            subtype="task"
            refetchQueries={refetchQueries}
        />
    )
}

export default Add
