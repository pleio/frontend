import React from 'react'
import { DragDropContext } from 'react-beautiful-dnd'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import { Row } from 'js/components/Grid/Grid'

import Column from './components/Column'

const Tasks = ({ mutate, data }) => {
    const onDragStart = (start, provided) => {
        const message = t('global.drag-start', {
            item: t('entity-task.content-name'),
            column: columns.find((el) => el.state === start.source.droppableId)
                .title,
            position: start.source.index + 1,
        })

        provided.announce(message)
    }

    const onDragUpdate = (update, provided) => {
        if (update.destination) {
            const message = t('global.drag-update', {
                item: t('entity-task.content-name'),
                column: columns.find(
                    (el) => el.state === update.destination.droppableId,
                ).title,
                position: update.destination.index + 1,
            })

            provided.announce(message)
        }
    }

    const onDragEnd = (result, provided) => {
        const message = result.destination
            ? t('global.drag-update', {
                  item: t('entity-task.content-name'),
                  column: columns.find(
                      (el) => el.state === result.destination.droppableId,
                  ).title,
                  position: result.destination.index + 1,
              })
            : t('global.drag-cancel', {
                  item: t('entity-task.content-name'),
                  column: columns.find(
                      (el) => el.state === result.source.droppableId,
                  ).title,
                  position: result.source.index + 1,
              })

        provided.announce(message)

        if (result.destination) {
            mutate({
                variables: {
                    input: {
                        guid: result.draggableId,
                        state: result.destination.droppableId,
                    },
                },
            })
        }
    }

    const { entities } = data

    const { t } = useTranslation()

    if (!entities) return null

    const columns = [
        { state: 'NEW', title: t('entity-task.status-to-do') },
        { state: 'BUSY', title: t('entity-task.status-doing') },
        { state: 'DONE', title: t('entity-task.status-done') },
    ]

    return (
        <DragDropContext
            onDragStart={onDragStart}
            onDragUpdate={onDragUpdate}
            onDragEnd={onDragEnd}
        >
            <Row $growContent style={{ height: '100%' }}>
                {columns.map((column, index) => (
                    <Column
                        key={index}
                        title={column.title}
                        state={column.state}
                        edges={entities.edges}
                    />
                ))}
            </Row>
        </DragDropContext>
    )
}

const Query = gql`
    query TasksList(
        $subtype: String!
        $containerGuid: String!
        $offset: Int!
        $limit: Int!
    ) {
        entities(
            subtype: $subtype
            containerGuid: $containerGuid
            offset: $offset
            limit: $limit
        ) {
            total
            edges {
                guid
                ... on Task {
                    guid
                    title
                    state
                    richDescription
                    canEdit
                }
            }
        }
    }
`

const Mutation = gql`
    mutation editTask($input: editTaskInput!) {
        editTask(input: $input) {
            entity {
                guid
                state
            }
        }
    }
`

export default graphql(Query, {
    options: {
        fetchPolicy: 'cache-and-network',
    },
})(graphql(Mutation)(Tasks))
