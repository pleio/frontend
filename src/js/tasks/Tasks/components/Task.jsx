import React, { useState } from 'react'
import { Draggable } from 'react-beautiful-dnd'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { getGroupUrl } from 'helpers'
import styled from 'styled-components'

import Card, { CardContent } from 'js/components/Card/Card'
import DropdownButton from 'js/components/DropdownButton/DropdownButton'
import HideVisually from 'js/components/HideVisually/HideVisually'
import IconButton from 'js/components/IconButton/IconButton'
import DeleteForm from 'js/components/Item/ItemActions/components/DeleteForm'
import Modal from 'js/components/Modal/Modal'
import TiptapView from 'js/components/Tiptap/TiptapView'

import OptionsIcon from 'icons/options.svg'

const Wrapper = styled(Card)`
    position: relative;
    margin-bottom: 20px;
    color: ${(p) => p.theme.color.primary.main};

    .DraggableArea {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    .TaskContent {
        display: flex;
        justify-content: space-between;

        button {
            z-index: 1;
        }
    }
`

const Task = ({ index, entity }) => {
    const params = useParams()
    const { guid, title, richDescription, canEdit } = entity

    const { t } = useTranslation()

    const [showDeleteModal, setShowDeleteModal] = useState(false)
    const toggleDeleteModal = () => {
        setShowDeleteModal(!showDeleteModal)
    }

    const [showContentModal, setShowContentModal] = useState(false)
    const toggleContentModal = () => {
        setShowContentModal(!showContentModal)
    }

    const refetchQueries = ['TasksList']

    return (
        <Draggable index={index} draggableId={guid}>
            {(provided) => {
                const id = `${provided.draggableProps['data-rbd-draggable-id']}-description`
                return (
                    <>
                        <HideVisually id={id}>
                            {t('global.draggable-description')}
                        </HideVisually>
                        <Wrapper
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                        >
                            <CardContent className="TaskContent">
                                <button
                                    type="button"
                                    onClick={toggleContentModal}
                                >
                                    {title}
                                </button>
                                <Modal
                                    isVisible={showContentModal}
                                    title={title}
                                    size="normal"
                                    showCloseButton
                                    onClose={toggleContentModal}
                                >
                                    <TiptapView content={richDescription} />
                                </Modal>
                                {canEdit && (
                                    <>
                                        <DropdownButton
                                            options={[
                                                {
                                                    name: t('entity-task.edit'),
                                                    label: t(
                                                        'action.edit-item-label',
                                                        {
                                                            title: entity.title,
                                                        },
                                                    ),
                                                    to: `${getGroupUrl(
                                                        params,
                                                    )}/tasks/edit/${guid}`,
                                                },
                                                {
                                                    name: t(
                                                        'entity-task.delete',
                                                    ),
                                                    label: t(
                                                        'action.delete-item-label',
                                                        {
                                                            title: entity.title,
                                                        },
                                                    ),
                                                    onClick: toggleDeleteModal,
                                                },
                                            ]}
                                        >
                                            <IconButton
                                                variant="secondary"
                                                size="normal"
                                                tooltip={t('global.options')}
                                                aria-label={t(
                                                    'action.show-options',
                                                )}
                                            >
                                                <OptionsIcon />
                                            </IconButton>
                                        </DropdownButton>
                                        <Modal
                                            isVisible={showDeleteModal}
                                            onClose={toggleDeleteModal}
                                            title={t('action.delete-item')}
                                            size="small"
                                        >
                                            <DeleteForm
                                                entity={entity}
                                                refetchQueries={refetchQueries}
                                                onClose={toggleDeleteModal}
                                                afterDelete={toggleDeleteModal}
                                            />
                                        </Modal>
                                    </>
                                )}
                            </CardContent>
                            <div
                                className="DraggableArea"
                                {...provided.dragHandleProps}
                                aria-label={t('action.move-title', {
                                    title,
                                })}
                                aria-roledescription={t(
                                    'global.draggable-role',
                                    {
                                        item: t('entity-task.content-name'),
                                    },
                                )}
                                aria-describedby={id}
                            />
                        </Wrapper>
                        {provided.placeholder}
                    </>
                )
            }}
        </Draggable>
    )
}

export default Task
