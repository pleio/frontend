import React from 'react'
import { Droppable } from 'react-beautiful-dnd'

import { Col } from 'js/components/Grid/Grid'
import { H3 } from 'js/components/Heading'

import Task from './Task'

const Column = ({ edges, title, state }) => {
    const tasks = edges.filter((e) => e.state === state)

    return (
        <Col mobileLandscapeUp={1 / 3}>
            <H3 style={{ flexGrow: 0, marginBottom: '16px' }}>{title}</H3>
            <Droppable droppableId={state} type="TASK">
                {(provided) => (
                    <div ref={provided.innerRef} style={{ flexGrow: 1 }}>
                        {tasks.map((entity, index) => (
                            <Task
                                key={entity.guid}
                                index={index}
                                entity={entity}
                            />
                        ))}
                        {provided.placeholder}
                    </div>
                )}
            </Droppable>
        </Col>
    )
}

export default Column
