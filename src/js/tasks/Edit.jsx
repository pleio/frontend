import React from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { getGroupUrl } from 'helpers'

import EntityAddEditForm from 'js/components/EntityActions/EntityAddEditForm'
import NotFound from 'js/core/NotFound'

const Edit = () => {
    const navigate = useNavigate()
    const { t } = useTranslation()
    const params = useParams()
    const { guid } = params

    const { loading, data } = useQuery(Query, {
        variables: {
            guid,
        },
    })

    if (loading) return null
    if (!data || !data.entity) return <NotFound />

    const { entity } = data

    const returnToTaskOverview = () => {
        navigate(`${getGroupUrl(params)}/tasks`, {
            replace: true,
        })
    }

    const refetchQueries = ['TasksList']

    return (
        <EntityAddEditForm
            title={t('entity-task.edit')}
            deleteTitle={t('entity-task.delete')}
            subtype="task"
            entity={entity}
            afterDelete={returnToTaskOverview}
            onClose={returnToTaskOverview}
            refetchQueries={refetchQueries}
        />
    )
}

const Query = gql`
    query EditTask($guid: String!) {
        entity(guid: $guid) {
            guid
            ... on Task {
                title
                description
                richDescription
                url
                accessId
                timePublished
                statusPublished
                scheduleArchiveEntity
                scheduleDeleteEntity
                canEdit
                canEditAdvanced
                canEditGroup
                tags
                owner {
                    guid
                    name
                }
                group {
                    guid
                    ... on Group {
                        name
                    }
                }
            }
        }
    }
`

export default Edit
