import { entityEditGroupFragment } from 'js/components/EntityActions/fragments/group'
import { ownerFragment } from 'js/lib/fragments/owner'

export default `
    ... on Task {
        title
        description
        richDescription
        url
        accessId
        timePublished
        timeCreated
        statusPublished
        scheduleArchiveEntity
        scheduleDeleteEntity
        canEdit
        canEditAdvanced
        canEditGroup
        writeAccessId
        tags
        tagCategories {
            name
            values
        }
        ${ownerFragment}
        ${entityEditGroupFragment}
    }
`
