import React from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import { Container } from 'js/components/Grid/Grid'

import Document from '../components/Document'

import Tasks from './Tasks/Tasks'

const GroupList = () => {
    const { t } = useTranslation()
    const { groupGuid } = useParams()

    const { data } = useQuery(Query, {
        variables: {
            guid: groupGuid,
        },
    })

    return (
        <>
            <Document
                title={t('entity-task.title-list')}
                containerTitle={data?.entity?.name}
            />

            <Container style={{ flexGrow: 1 }}>
                <Tasks
                    type="object"
                    subtype="task"
                    containerGuid={groupGuid}
                    offset={0}
                    limit={100}
                />
            </Container>
        </>
    )
}

const Query = gql`
    query GroupList($guid: String!) {
        entity(guid: $guid) {
            guid
            ... on Group {
                guid
                name
            }
        }
    }
`

export default GroupList
