import React from 'react'
import { useTranslation } from 'react-i18next'

import { CardContent } from 'js/components/Card/Card'
import FeedItem from 'js/components/FeedItem/FeedItem'
import FeedItemImage from 'js/components/FeedItem/FeedItemImage'
import FeedItemMeta from 'js/components/FeedItem/FeedItemMeta'
import { H3 } from 'js/components/Heading'
import ItemTags from 'js/components/ItemTags'
import Text from 'js/components/Text/Text'
import Truncate from 'js/components/Truncate/Truncate'
import { useSiteStore } from 'js/lib/stores'

import ExternalIcon from 'icons/new-window-small.svg'

const RenderExcerpt = ({ excerptMaxLines, children }) =>
    excerptMaxLines ? (
        <Truncate lines={excerptMaxLines}>{children}</Truncate>
    ) : (
        children
    )

const ExternalContentCard = ({
    entity,
    'data-feed': dataFeed,
    excerptMaxLines,
}) => {
    const { t } = useTranslation()

    const { site } = useSiteStore()
    const { showTagsInFeed, showCustomTagsInFeed } = site

    return (
        <FeedItem data-feed={dataFeed}>
            <CardContent className="FeedItemContent">
                <FeedItemMeta
                    entity={entity}
                    subtype={entity.source?.name}
                    translateSubtype={false}
                    group={entity.group}
                />

                {entity.title && (
                    <H3 className="FeedItemTitle">
                        <a
                            href={entity.url}
                            target="_blank"
                            rel="noopener noreferrer"
                            aria-label={`${entity.title}${t(
                                'global.opens-in-new-window',
                            )}`}
                            style={{
                                display: 'flex',
                                alignItems: 'center',
                                gap: '8px',
                            }}
                        >
                            <Truncate lines={2}>{entity.title}</Truncate>
                            <ExternalIcon />
                        </a>
                    </H3>
                )}

                {entity.description && (
                    <div className="FeedItemDescription">
                        <Text>
                            <div
                                dangerouslySetInnerHTML={{
                                    __html: entity.description,
                                }}
                            />
                        </Text>
                    </div>
                )}
                {entity.excerpt && (
                    <div className="FeedItemExcerpt">
                        <RenderExcerpt excerptMaxLines={excerptMaxLines}>
                            <Text>
                                <div
                                    dangerouslySetInnerHTML={{
                                        __html: entity.excerpt,
                                    }}
                                />
                            </Text>
                        </RenderExcerpt>
                    </div>
                )}

                <ItemTags
                    showCustomTags={showCustomTagsInFeed}
                    showTags={showTagsInFeed}
                    customTags={entity.tags}
                    tagCategories={entity.tagCategories}
                    style={{ marginTop: '12px' }}
                />
            </CardContent>
            <FeedItemImage entity={entity} />
        </FeedItem>
    )
}

export default ExternalContentCard
