import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { useWindowScrollPosition } from 'helpers'
import { DesktopUp, TabletDown } from 'helpers/mediaWrapper'

import Document from 'js/components/Document'
import { Col, Container, Row } from 'js/components/Grid/Grid'
import HideVisually from 'js/components/HideVisually/HideVisually'
import Section from 'js/components/Section/Section'
import UsersOnline from 'js/components/UsersOnline'
import { createCookie, readCookie } from 'js/lib/cookies'
import FeaturedView from 'js/page/Widget/components/Featured/FeaturedView'
import FeedView from 'js/page/Widget/components/Feed/FeedView'

import DirectLinks from './components/DirectLinks'
import Footer from './components/Footer'
import Initiative from './components/Initiative'
import LeraarLead from './components/LeraarLead'
import Poll from './components/Poll'
import Recommended from './components/Recommended'
import Trending from './components/Trending'

const Activity = ({ data }) => {
    const { site, viewer } = data

    const [showLead, setShowLead] = useState(true)

    const { t } = useTranslation()

    useWindowScrollPosition('home')

    if (!viewer || !site) return null

    const closeLead = () => {
        createCookie(`hideLeraarLead-${site.guid}`, '1', 365)
        setShowLead(false)
    }

    const initiative = site ? <Initiative site={site} /> : null

    const numberOfFeaturedItems = parseInt(
        window.__SETTINGS__.numberOfFeaturedItems,
        10,
    )

    const showLeader =
        showLead &&
        site.showLeader &&
        !readCookie(`hideLeraarLead-${site.guid}`)

    const hasFeaturedItems = numberOfFeaturedItems > 0
    const showFeaturedSection = showLeader || hasFeaturedItems

    const settings = {
        showTagFilter: true,
        showTypeFilter: window.__SETTINGS__.showExtraHomepageFilters,
        showGroupFilter: window.__SETTINGS__.showExtraHomepageFilters,
        ...(window.__SETTINGS__.enableFeedSorting
            ? {
                  sortingOptions: ['timePublished', 'lastAction'],
              }
            : {}),
        tagFilter: {},
        typeFilter: [],
        groupFilter: '',
        sortBy: 'lastAction',
        sortDirection: 'desc',
        itemView: 'list',
        itemCount: 5,
    }

    const Sidebar = (
        <Col desktopUp={1 / 3}>
            <Row>
                <Poll />
                <DirectLinks />
                <Recommended />
                <Trending />
                {initiative}
                <Footer />
            </Row>
        </Col>
    )

    return (
        <>
            <Document />

            {!showLeader && (
                <HideVisually as="h1">
                    {site.name || t('global.home-title')}
                </HideVisually>
            )}

            <Section>
                <Container>
                    {showFeaturedSection && (
                        <div style={{ paddingBottom: '16px' }}>
                            {showLeader && (
                                <LeraarLead
                                    data={{ site }}
                                    isLoggedIn={viewer.loggedIn}
                                    onClose={closeLead}
                                />
                            )}

                            <FeaturedView
                                settings={{
                                    title: t('widget-featured.title'),
                                    limit: numberOfFeaturedItems,
                                }}
                                style={{ marginBottom: '24px' }}
                            />
                        </div>
                    )}

                    <UsersOnline />

                    <Row>
                        <TabletDown>{Sidebar}</TabletDown>

                        <Col desktopUp={2 / 3}>
                            <FeedView
                                guid="activityStream"
                                settings={settings}
                                style={{ marginBottom: '24px' }}
                            />
                        </Col>

                        <DesktopUp>{Sidebar}</DesktopUp>
                    </Row>
                </Container>
            </Section>
        </>
    )
}

const Query = gql`
    query ActivityWrapper {
        viewer {
            guid
            loggedIn
        }
        site {
            guid
            showInitiative
            initiativeTitle
            initiativeImage
            initiativeImageAlt
            logo
            logoAlt
            initiativeDescription
            initiatorLink
            showLeader
            name
            showLeaderButtons
            subtitle
            leaderImage
            activityFilter {
                contentTypes {
                    key
                    value
                }
            }
            tagCategories {
                values
            }
        }
    }
`

export default graphql(Query)(Activity)
