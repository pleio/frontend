import React from 'react'
import { gql, useQuery } from '@apollo/client'
import { media } from 'helpers'
import styled from 'styled-components'

import { Col } from 'js/components/Grid/Grid'

const Wrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    margin: 0 -4px;

    ${media.tabletDown`
        display: none;
    `}

    a {
        padding: 4px 8px;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        color: ${(p) => p.theme.color.text.grey};

        &:hover {
            color: ${(p) => p.theme.color.text.black};
        }
    }
`

const Footer = () => {
    const { data } = useQuery(GET_FOOTER_SITE_DATA)

    const footerLinks = data?.site?.footer || []

    return footerLinks.length === 0 ? null : (
        <Col mobileLandscape={1 / 2} tablet={1 / 2}>
            <Wrapper>
                {footerLinks.map((item, i) => (
                    <a key={i} href={item.link} title={item.title}>
                        {item.title}
                    </a>
                ))}
            </Wrapper>
        </Col>
    )
}

const GET_FOOTER_SITE_DATA = gql`
    query GetFooterSiteData {
        site {
            footer {
                title
                link
            }
        }
    }
`

export default Footer
