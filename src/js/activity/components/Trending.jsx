import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import styled from 'styled-components'

import Accordeon from 'js/components/Accordeon'
import { Col } from 'js/components/Grid/Grid'

import ArrowRightIcon from 'icons/chevron-right.svg'

const Wrapper = styled(Link)`
    display: flex;
    align-items: center;
    padding: 8px 0;

    span {
        width: 100%;
        color: ${(p) => p.theme.color.primary.main};
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        padding-right: 8px;
    }

    &:hover span {
        text-decoration: underline;
    }
`

const Trending = (props) => {
    const { t } = useTranslation()

    const { data } = props
    const { trending } = data

    if (!trending || trending.length === 0) return null

    const items = trending.map((item, i) => (
        <Wrapper key={i} to={`/trending/${encodeURIComponent(item.tag)}`}>
            <span>{item.tag}</span>
            <ArrowRightIcon />
        </Wrapper>
    ))

    return (
        <Col
            mobileLandscape={1 / 2}
            tablet={1 / 2}
            className="ActivityStreamTrending"
        >
            <Accordeon title={t('global.trending-title')}>{items}</Accordeon>
        </Col>
    )
}

const Query = gql`
    query Trending {
        trending {
            tag
            likes
        }
    }
`

export default graphql(Query)(Trending)
