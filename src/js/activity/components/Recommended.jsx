import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import styled from 'styled-components'

import Avatar from 'js/components/Avatar/Avatar'
import { Col } from 'js/components/Grid/Grid'

import Accordeon from '../../components/Accordeon'

const Wrapper = styled(Link)`
    display: flex;
    padding: 4px 0;
    font-size: ${(p) => p.theme.font.size.small};
    line-height: ${(p) => p.theme.font.lineHeight.small};

    .LinkItemTitle {
        color: ${(p) => p.theme.color.primary.main};
    }

    &:hover .LinkItemTitle {
        text-decoration: underline;
    }

    .LinkItemOwner {
        color: ${(p) => p.theme.color.text.grey};
    }
`

const Recommended = ({ data }) => {
    const { t } = useTranslation()

    const { recommended } = data

    if (!recommended || recommended.edges.length === 0) {
        return null
    }

    const items = recommended.edges.map((entity, i) => (
        <Wrapper key={i} to={entity.url}>
            <Avatar
                name={entity.owner.name}
                image={entity.owner.icon}
                style={{ marginTop: '4px', marginRight: '12px' }}
            />
            <div>
                <div className="LinkItemTitle">{entity.title}</div>
                {entity.showOwner && (
                    <div className="LinkItemOwner">{entity.owner.name}</div>
                )}
            </div>
        </Wrapper>
    ))

    return (
        <Col
            mobileLandscape={1 / 2}
            tablet={1 / 2}
            className="ActivityStreamRecommended"
        >
            <Accordeon title={t('entity-blog.recommended-blog')}>
                {items}
            </Accordeon>
        </Col>
    )
}

const Query = gql`
    query Recommended {
        recommended(limit: 3) {
            total
            edges {
                guid
                ... on Blog {
                    title
                    subtype
                    url
                    showOwner
                    owner {
                        guid
                        name
                        icon
                    }
                }
            }
        }
    }
`

export default graphql(Query)(Recommended)
