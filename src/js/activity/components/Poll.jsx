import React from 'react'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import { Col } from 'js/components/Grid/Grid'
import PollViewForm from 'js/page/Widget/components/Poll/PollViewForm'

import Accordeon from '../../components/Accordeon'

const Poll = ({ data }) => {
    if (!data.entities || data.entities.edges.length === 0) return null

    return (
        <Col
            mobileLandscape={1 / 2}
            tablet={1 / 2}
            className="ActivityStreamPoll"
        >
            <Accordeon title={data.entities.edges[0].title}>
                <PollViewForm
                    viewer={data.viewer}
                    entity={data.entities.edges[0]}
                    size="small"
                    legend={data.entities.edges[0].title}
                />
            </Accordeon>
        </Col>
    )
}

const Query = gql`
    query Poll {
        viewer {
            guid
            loggedIn
        }
        entities(subtype: "poll", limit: 1) {
            total
            edges {
                guid
                ... on Poll {
                    title
                    url
                    canEdit
                    hasVoted
                    choices {
                        guid
                        text
                        votes
                    }
                }
            }
        }
    }
`

export default graphql(Query)(Poll)
