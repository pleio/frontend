import React, { Fragment } from 'react'

import Card, { CardContent } from 'js/components/Card/Card'
import { Col } from 'js/components/Grid/Grid'
import { H3 } from 'js/components/Heading'

const Initiative = ({ site }) => {
    const {
        showInitiative,
        initiativeTitle,
        initiativeImage,
        initiativeImageAlt,
        logo,
        logoAlt,
        initiativeDescription,
        initiatorLink,
    } = site

    if (!showInitiative) return null

    const image = initiativeImage || logo
    const alt = initiativeImage ? initiativeImageAlt : logoAlt

    return (
        <Col mobileLandscape={1 / 2} tablet={1 / 2}>
            <Card
                as={initiatorLink && 'a'}
                href={initiatorLink || null}
                target={initiatorLink ? '_blank' : null}
                rel={initiatorLink ? 'noopener noreferrer' : null}
                style={{ overflow: 'hidden', marginBottom: '16px' }}
            >
                {initiativeTitle ? (
                    <Fragment>
                        <CardContent style={image ? {} : { paddingBottom: 0 }}>
                            <H3>{initiativeTitle}</H3>
                        </CardContent>
                    </Fragment>
                ) : null}
                {!!image && (
                    <img
                        src={image}
                        alt={alt}
                        style={{ position: 'relative', width: '100%' }}
                    />
                )}
                {initiativeDescription ? (
                    <CardContent>{initiativeDescription}</CardContent>
                ) : null}
            </Card>
        </Col>
    )
}

export default Initiative
