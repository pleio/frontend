import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { getCleanUrl, isExternalUrl } from 'helpers'
import styled from 'styled-components'

import Accordeon from 'js/components/Accordeon'
import { Col } from 'js/components/Grid/Grid'

import ArrowRightIcon from 'icons/chevron-right.svg'
import ExternalIcon from 'icons/new-window-small.svg'

const Wrapper = styled.div`
    display: flex;
    align-items: center;
    padding: 4px 0;

    .LinkItemTitle {
        width: 100%;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        color: ${(p) => p.theme.color.primary.main};
        padding-right: 8px;
    }

    &:hover .LinkItemTitle {
        text-decoration: underline;
    }

    .LinkItemIcon {
        flex-shrink: 0;
    }
`

const DirectLinks = ({ data }) => {
    const { site } = data
    if (!site || !site.directLinks || site.directLinks.length === 0) return null

    const { t } = useTranslation()

    const items = site.directLinks.map((item, index) => {
        const url = getCleanUrl(item.link)
        const isExternal = isExternalUrl(url)
        const LinkElement = isExternal ? 'a' : Link

        return (
            <Wrapper
                key={'item' + index}
                as={LinkElement}
                to={!isExternal ? url : null}
                href={isExternal && url}
                target={isExternal ? '_blank' : null}
                rel={isExternal ? 'noopener noreferrer' : null}
                aria-label={`${item.title}${
                    isExternal ? t('global.opens-in-new-window') : ''
                }`}
            >
                <div className="LinkItemTitle">{item.title}</div>
                <div className="LinkItemIcon">
                    {isExternal ? <ExternalIcon /> : <ArrowRightIcon />}
                </div>
            </Wrapper>
        )
    })

    return (
        <Col mobileLandscape={1 / 2} tablet={1 / 2}>
            <Accordeon title={t('widget-direct-links.title')}>
                {items}
            </Accordeon>
        </Col>
    )
}

const Query = gql`
    query DirectLinks {
        site {
            guid
            directLinks {
                title
                link
            }
        }
    }
`

export default graphql(Query)(DirectLinks)
