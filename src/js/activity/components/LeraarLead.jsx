import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

import HideVisually from 'js/components/HideVisually/HideVisually'
import { CloseLeaderButton } from 'js/page/Widget/components/Leader/LeaderView'

import CrossIcon from 'icons/cross.svg'

/**
 * @deprecated AVOID FURTHER USAGE - should be refactored to be same as other Lead widgets
 */
const LeraarLead = ({ data, isLoggedIn, onClose }) => {
    const { site } = data
    const { t } = useTranslation()

    if (!site) return null

    const style = {
        backgroundImage: `url(${
            site.leaderImage || '/mod/pleio_template/src/images/lead-home2.png'
        })`,
        marginBottom: '24px',
    }

    let buttons
    if (site.showLeaderButtons) {
        buttons = (
            <div className="buttons ___margin-top ___gutter ___center">
                <Link to="/page/view/60168708/over-ons">
                    <div className="button ___large">Over Leraar.nl</div>
                </Link>
                {!isLoggedIn && (
                    <a href="https://account.pleio.nl/register/">
                        <div className="button ___large">Aan de slag</div>
                    </a>
                )}
            </div>
        )
    }

    return (
        <aside style={style} className="lead ___rounded">
            <div className="lead__justify">
                <div className="container">
                    <h1 className="lead__title">{site.name}</h1>
                    <p className="lead__sub-title">{site.subtitle}</p>
                    {buttons}
                </div>
            </div>

            <CloseLeaderButton
                variant="secondary"
                size="large"
                onClick={onClose}
            >
                <CrossIcon />
                <HideVisually>{t('widget-leader.close')}</HideVisually>
            </CloseLeaderButton>
        </aside>
    )
}

LeraarLead.propTypes = {
    data: PropTypes.object.isRequired,
    isLoggedIn: PropTypes.bool,
    onHide: PropTypes.func,
}

export default LeraarLead
