import React from 'react'
import PropTypes from 'prop-types'

import BlogCard from 'js/blog/components/Card'
import DiscussionCard from 'js/discussions/components/Card'
import EventCard from 'js/events/components/Card'
import ExternalContentCard from 'js/externalContent/ExternalContentCard'
import FileFolderCard from 'js/files/components/FileFolderCard'
import StatusUpdateCard from 'js/group/StatusUpdate/Card'
import MagazineIssueCard from 'js/magazine-issue/Card'
import NewsCard from 'js/news/components/Card'
import PageCard from 'js/page/Card'
import EpisodeCard from 'js/podcast/components/EpisodeCard'
import PodcastCard from 'js/podcast/components/PodcastCard'
import QuestionCard from 'js/questions/components/Card'
import WikiCard from 'js/wiki/components/Card'

const Card = ({ entity, viewer, ...rest }) => {
    switch (entity.entity.__typename) {
        case 'Blog':
            return <BlogCard entity={entity.entity} {...rest} />

        case 'Discussion':
            return <DiscussionCard entity={entity.entity} {...rest} />

        case 'Episode': // Podcast in feeds
            return <EpisodeCard entity={entity.entity} {...rest} />

        case 'Event':
            return (
                <EventCard entity={entity.entity} viewer={viewer} {...rest} />
            )

        case 'ExternalContent':
            return <ExternalContentCard entity={entity.entity} {...rest} />

        case 'File':
        case 'Folder':
        case 'Pad':
            return <FileFolderCard entity={entity.entity} {...rest} />

        case 'News':
            return <NewsCard entity={entity.entity} {...rest} />

        case 'Page':
            return <PageCard entity={entity.entity} {...rest} />

        case 'Podcast':
            return <PodcastCard entity={entity.entity} {...rest} />

        case 'Question':
            return <QuestionCard entity={entity.entity} {...rest} />

        case 'StatusUpdate':
            return <StatusUpdateCard entity={entity.entity} {...rest} />

        case 'Wiki':
            return <WikiCard entity={entity.entity} {...rest} />

        case 'MagazineIssue':
            return <MagazineIssueCard entity={entity.entity} {...rest} />

        default:
            console.error('Unknown entity: ', entity)
            return null
    }
}

Card.propTypes = {
    entity: PropTypes.object.isRequired,
}

export default Card
