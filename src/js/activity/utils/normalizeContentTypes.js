export default (contentTypes, omitTypes) =>
    contentTypes
        .filter(({ key }) => key.toLowerCase() && !omitTypes?.includes(key))
        .map(({ key, value }) => ({
            value: key,
            label: value,
        }))
