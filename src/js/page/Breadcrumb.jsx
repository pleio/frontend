import React, { Fragment } from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import RightSmallIcon from 'icons/chevron-right.svg'

const Wrapper = styled.div`
    display: flex;
    align-items: stretch;
    flex-wrap: wrap;
    overflow: hidden;

    .Breadcrumb {
        position: relative;
        min-width: 20px;
        display: flex;
        align-items: center;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;

        &:focus {
            outline: none;
            text-decoration: underline;
        }

        &:first-child {
            flex-shrink: 0;
        }

        &:not([aria-current]) {
            color: ${(p) => p.theme.color.text.grey};

            &:hover {
                color: ${(p) => p.theme.color.text.black};
            }
        }

        &[aria-current] {
            font-weight: ${(p) => p.theme.font.weight.semibold};
            color: ${(p) => p.theme.color.text.black};
        }
    }

    svg {
        align-self: center;
        margin: 1px 6px 0;
        color: ${(p) => p.theme.color.icon.grey};
    }
`

const Breadcrumb = ({ breadcrumb, rootUrl, children, ...rest }) => {
    return (
        <Wrapper {...rest}>
            {children}
            {breadcrumb.map(({ guid, title, url }, i) => {
                const link = rootUrl ? `${rootUrl}/${guid}` : url

                return (
                    <Fragment key={guid}>
                        {i > 0 && <RightSmallIcon />}
                        {breadcrumb.length - 1 === i ? (
                            <div
                                className="Breadcrumb"
                                title={title}
                                aria-current
                            >
                                {title}
                            </div>
                        ) : (
                            <Link
                                to={link}
                                className="Breadcrumb"
                                title={title}
                            >
                                {title}
                            </Link>
                        )}
                    </Fragment>
                )
            })}
        </Wrapper>
    )
}

export default Breadcrumb
