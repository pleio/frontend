export default function (obj) {
    return Object.entries(obj).map(([key, value]) => {
        // If the value is an object, we extract the key (f.e. richDescription, attachment, links) and its value
        if (value === Object(value)) {
            const objectKey = Object.keys(value)[0]
            const objectValue = Object.values(value)[0]

            return {
                key,
                [objectKey]: objectValue,
            }
        }

        // If the value is not an object, we assume it's a key-value pair
        return {
            key,
            value,
        }
    })
}
