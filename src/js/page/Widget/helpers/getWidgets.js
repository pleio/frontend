import React from 'react'
import { useTranslation } from 'react-i18next'
import { sortByProperty } from 'helpers'

import Introduction from 'js/group/components/Introduction'
import MembersCard from 'js/group/components/MembersCard'
import Appointment from 'js/page/Widget/components/Appointment/Appointment'
import Birthdays from 'js/page/Widget/components/Birthdays/Birthdays'
import CallToAction from 'js/page/Widget/components/CallToAction/CallToAction'
import Carousel from 'js/page/Widget/components/Carousel/Carousel'
import Code from 'js/page/Widget/components/Code/Code'
import Create from 'js/page/Widget/components/Create/CreateWidget'
import Events from 'js/page/Widget/components/Events/Events'
import Featured from 'js/page/Widget/components/Featured/Featured'
import Feed from 'js/page/Widget/components/Feed/Feed'
import Footer from 'js/page/Widget/components/Footer/Footer'
import Form from 'js/page/Widget/components/Form/Form'
import ItemList from 'js/page/Widget/components/ItemList/ItemList'
import Lead from 'js/page/Widget/components/Lead/Lead'
import Leader from 'js/page/Widget/components/Leader/Leader'
import LinkList from 'js/page/Widget/components/LinkList/LinkList'
import Podcast from 'js/page/Widget/components/Podcast/Podcast'
import Poll from 'js/page/Widget/components/Poll/Poll'
import Rss from 'js/page/Widget/components/Rss/Rss'
import Search from 'js/page/Widget/components/Search/Search'
import Text from 'js/page/Widget/components/Text/Text'

import AppointmentIcon from 'icons/widget-appointment.svg'
import BirthdaysIcon from 'icons/widget-birthdays.svg'
import CallToActionIcon from 'icons/widget-call-to-action.svg'
import CarouselIcon from 'icons/widget-carousel.svg'
import CodeIcon from 'icons/widget-code.svg'
import CreateIcon from 'icons/widget-create.svg'
import EventsIcon from 'icons/widget-events.svg'
import FeaturedIcon from 'icons/widget-featured.svg'
import FeedIcon from 'icons/widget-feed.svg'
import FooterIcon from 'icons/widget-footer.svg'
import FormIcon from 'icons/widget-form.svg'
import IntroductionIcon from 'icons/widget-introduction.svg'
import ItemListIcon from 'icons/widget-item-list.svg'
import LeadIcon from 'icons/widget-lead.svg'
import LeaderIcon from 'icons/widget-leader.svg'
import LinkListIcon from 'icons/widget-link-list.svg'
import MembersIcon from 'icons/widget-members.svg'
import PodcastIcon from 'icons/widget-podcast.svg'
import PollIcon from 'icons/widget-poll.svg'
import RssIcon from 'icons/widget-rss.svg'
import SearchIcon from 'icons/widget-search.svg'
import TextIcon from 'icons/widget-text.svg'

export default function (sitePlanType, scheduleAppointmentEnabled, inGroup) {
    const { t } = useTranslation()
    const sitePlanIsPro = ['pro', 'pro_plus'].includes(sitePlanType)

    const widgets = [
        {
            value: 'linkList',
            label: t('widget-linklist.title'),
            icon: <LinkListIcon />,
            component: LinkList,
        },
        {
            value: 'objects',
            label: t('widget-objects.title'),
            icon: <ItemListIcon />,
            component: ItemList,
        },
        {
            value: 'poll',
            label: t('widget-poll.title'),
            icon: <PollIcon />,
            component: Poll,
        },
        {
            value: 'events',
            label: t('widget-events.title'),
            icon: <EventsIcon />,
            component: Events,
        },
        {
            value: 'text',
            label: t('widget-text.title'),
            icon: <TextIcon />,
            component: Text,
        },
        {
            value: 'html',
            label: t('widget-code.title'),
            icon: <CodeIcon />,
            component: Code,
        },
        {
            value: 'activity',
            label: t('widget-feed.title'),
            icon: <FeedIcon />,
            component: Feed,
        },
        {
            value: 'birthdays',
            label: t('widget-birthdays.title'),
            icon: <BirthdaysIcon />,
            component: Birthdays,
        },
        {
            value: 'callToAction',
            label: t('widget-call-to-action.title'),
            icon: <CallToActionIcon />,
            component: CallToAction,
        },
        {
            value: 'leader',
            label: t('widget-leader.title'),
            icon: <LeaderIcon />,
            component: Leader,
        },
        {
            value: 'lead',
            label: t('widget-lead.title'),
            icon: <LeadIcon />,
            component: Lead,
        },
        {
            value: 'featured',
            label: t('widget-featured.title'),
            icon: <FeaturedIcon />,
            component: Featured,
        },
        {
            value: 'footer',
            label: t('widget-footer.title'),
            icon: <FooterIcon />,
            component: Footer,
        },
        {
            value: 'rss',
            label: t('widget-rss.title'),
            icon: <RssIcon />,
            component: Rss,
        },
        {
            value: 'search',
            label: t('widget-search.title'),
            icon: <SearchIcon />,
            component: Search,
        },
        {
            value: 'podcast',
            label: t('widget-podcast.title'),
            icon: <PodcastIcon />,
            component: Podcast,
        },
        {
            value: 'create',
            label: t('widget-create.title'),
            icon: <CreateIcon />,
            component: Create,
        },
        ...(sitePlanIsPro
            ? [
                  {
                      value: 'form',
                      label: t('widget-form.title'),
                      icon: <FormIcon />,
                      component: Form,
                  },
                  {
                      value: 'carousel',
                      label: t('widget-carousel.title'),
                      icon: <CarouselIcon />,
                      component: Carousel,
                  },
              ]
            : []),
        ...(inGroup
            ? [
                  {
                      value: 'groupIntroduction',
                      label: t('widget-introduction.title'),
                      icon: <IntroductionIcon />,
                      component: Introduction,
                  },
                  {
                      value: 'groupMembers',
                      label: t('widget-users.title'),
                      icon: <MembersIcon />,
                      component: MembersCard,
                  },
              ]
            : [
                  ...(scheduleAppointmentEnabled
                      ? [
                            {
                                value: 'appointment',
                                label: t('widget-appointment.title'),
                                icon: <AppointmentIcon />,
                                component: Appointment,
                            },
                        ]
                      : []),
              ]),
    ]

    widgets.sort(sortByProperty('label'))

    return widgets
}
