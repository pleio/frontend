import React, { forwardRef } from 'react'
import { useTranslation } from 'react-i18next'

import ErrorBoundary from 'js/components/ErrorBoundary'
import ErrorStyles from 'js/components/ErrorStyles'
import getWidgets from 'js/page/Widget/helpers/getWidgets'

const Widget = React.memo(
    forwardRef(
        (
            {
                guid,
                containerGuid,
                site,
                entity,
                group,
                canEditContainer,
                rowBackgroundColor,
                columnWidth,
                isEditing,
                editMode,
                onSave,
                dispatchWidgetOptions,
            },
            ref,
        ) => {
            const { t } = useTranslation()

            const { sitePlanType, scheduleAppointmentEnabled } = site

            const widgets = getWidgets(
                sitePlanType,
                scheduleAppointmentEnabled,
                !!group,
            )

            const type = entity?.type
            const Component = widgets.find(
                (widget) => widget.value === type,
            )?.component

            if (!Component) {
                console.error(
                    `${t('page.widget-not-found')} ${t(
                        'global.type',
                    )}: ${type}`,
                )

                if (editMode) {
                    return t('page.widget-not-found')
                }
                return null
            }

            const props = {
                ref,
                guid,
                containerGuid,
                site,
                entity,
                group,
                canEditContainer,
                rowBackgroundColor,
                columnWidth,
                editMode,
                isEditing,
                onSave,
                dispatchWidgetOptions,
            }

            return (
                <ErrorBoundary
                    fallback={
                        <ErrorStyles>
                            {t('error.widget-load-error')}
                        </ErrorStyles>
                    }
                >
                    <Component {...props} />
                </ErrorBoundary>
            )
        },
    ),
)

Widget.displayName = 'Widget'

export default Widget
