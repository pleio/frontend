import React, { forwardRef } from 'react'

import useGetSetting from 'js/page/Widget/hooks/useGetSetting'

import PodcastEdit from './PodcastEdit'
import PodcastView from './PodcastView'

const Podcast = forwardRef(({ entity, isEditing, editMode, onSave }, ref) => {
    const getSetting = useGetSetting(entity)

    const guid = getSetting('podcastGuid')

    if (isEditing) {
        return <PodcastEdit ref={ref} podcastGuid={guid} onSave={onSave} />
    }

    return <PodcastView podcastGuid={guid} editMode={editMode} />
})

Podcast.displayName = 'Podcast'

export default Podcast
