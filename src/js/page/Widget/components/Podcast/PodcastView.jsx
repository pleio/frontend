import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'

import Card, { CardContent } from 'js/components/Card/Card'
import { iconViewFragment } from 'js/lib/fragments/icon'
import { ownerFragment } from 'js/lib/fragments/owner'
import EditWidgetPlaceholder from 'js/page/Widget/components/components/EditWidgetPlaceholder'
import PodcastPlayer from 'js/podcast/components/PodcastPlayer/PodcastPlayer'

const PodcastView = ({ editMode, podcastGuid, ...rest }) => {
    const { t } = useTranslation()

    const { loading, data } = useQuery(GET_PODCASTS, {
        variables: {
            podcastGuid,
        },
    })

    if (loading || (!data?.entity && !editMode)) {
        return null
    }

    if (!data?.entity && editMode) {
        return <EditWidgetPlaceholder title={t('widget-podcast.title')} />
    }

    return (
        <Card {...rest}>
            <CardContent>
                <PodcastPlayer podcast={data.entity} />
            </CardContent>
        </Card>
    )
}

const GET_PODCASTS = gql`
    query Podcast($podcastGuid: String!) {
        viewer {
            guid
            loggedIn
        }
        entity(guid: $podcastGuid) {
            guid
            ... on Podcast {
                title
                url
                authors {
                    name
                }
                ${ownerFragment}
                ${iconViewFragment}
                episodes {
                    edges {
                        guid
                        title
                        url
                        excerpt
                        file {
                            guid
                            url
                            ... on File {
                                download
                            }
                        }
                    }
                }
                canEdit
            }
        }
    }
`

export default PodcastView
