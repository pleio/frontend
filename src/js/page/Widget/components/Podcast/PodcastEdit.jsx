import React, { forwardRef, useImperativeHandle, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { Query } from '@apollo/client/react/components'

import Select from 'js/components/Select/Select'
import WidgetViewPortal from 'js/page/Widget/components/components//WidgetViewPortal'
import convertObjectToWidgetSettings from 'js/page/Widget/helpers/convertObjectToWidgetSettings'

import Podcast from './Podcast'

const GET_PODCASTS = gql`
    query PodcastList {
        entities(offset: 0, limit: 99, subtype: "podcast") {
            total
            edges {
                guid
                ... on Podcast {
                    title
                }
            }
        }
    }
`

const PodcastEdit = forwardRef(({ podcastGuid, onSave }, ref) => {
    const { t } = useTranslation()

    const [value, setValue] = useState(podcastGuid)

    useImperativeHandle(ref, () => ({
        onSave() {
            handleSave()
        },
    }))

    const handleChange = (value) => {
        setValue(value)
    }

    const filterValues = () => {
        return convertObjectToWidgetSettings({
            podcastGuid: value,
        })
    }

    const handleSave = () => {
        onSave(filterValues())
    }

    // Have to use Query component, because old widget system uses refs for onSave
    return (
        <>
            <Query query={GET_PODCASTS}>
                {({ loading, data }) => {
                    const options =
                        data?.entities.edges.map((entity) => {
                            return {
                                value: entity.guid,
                                label: entity.title,
                            }
                        }) || []

                    return (
                        <Select
                            isLoading={loading}
                            name="podcastItem"
                            label={t('widget-podcast.choose-a-podcast')}
                            options={options}
                            value={value}
                            onChange={handleChange}
                        />
                    )
                }}
            </Query>

            <WidgetViewPortal Component={Podcast} settings={filterValues()} />
        </>
    )
})

PodcastEdit.displayName = 'PodcastEdit'

export default PodcastEdit
