import React, {
    forwardRef,
    useCallback,
    useEffect,
    useImperativeHandle,
    useState,
} from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql, useLazyQuery, useMutation } from '@apollo/client'

import FormContainer from 'js/components/Form/FormContainer'
import FormItem from 'js/components/Form/FormItem'
import Spacer from 'js/components/Spacer/Spacer'
import { useDebounce } from 'js/lib/helpers'
import WidgetViewPortal from 'js/page/Widget/components/components//WidgetViewPortal'
import convertObjectToWidgetSettings from 'js/page/Widget/helpers/convertObjectToWidgetSettings'

import Rss from './Rss'

const RssEdit = forwardRef(
    ({ guid, settings, group, onSave, dispatchWidgetOptions }, ref) => {
        const { t } = useTranslation()

        const [testRssEndpoint, { loading, data }] =
            useLazyQuery(TEST_RSS_ENDPOINT)

        useEffect(() => {
            dispatchWidgetOptions({
                type: 'setSaveButtonDisabled',
                payload: loading
                    ? true
                    : !data
                      ? settings.url === ''
                      : !data.testRssEndpoint?.isFeed,
            })
        }, [loading, data, settings.url, dispatchWidgetOptions])

        useImperativeHandle(ref, () => ({
            onSave() {
                submitForm()
            },
        }))

        const {
            control,
            handleSubmit,
            watch,
            setValue,
            trigger,
            formState: { errors },
        } = useForm({
            defaultValues: settings,
        })
        const watchValues = watch()
        const url = watchValues?.url

        const filterValues = ({ title, url, initialLimit }) => {
            return convertObjectToWidgetSettings({
                title,
                url,
                initialLimit: initialLimit.toString(),
            })
        }

        const [rssUrl, setRssUrl] = useState(settings.url)

        const debouncedValidate = useCallback(async () => {
            try {
                await testRssEndpoint({
                    variables: {
                        url: rssUrl,
                        groupGuid: group?.guid,
                    },
                })
                trigger('url')
            } catch (error) {
                console.error(error)
            }
        }, [rssUrl, testRssEndpoint, group?.guid, trigger])

        useDebounce(debouncedValidate, 500)

        const handleUrlChange = (e) => {
            setRssUrl(e.target.value)
            setValue('url', e.target.value)
        }

        const [addRssEndpoint, { loading: isAddingRssEndpoint }] =
            useMutation(ADD_RSS_ENDPOINT)

        const onSubmit = () => {
            if (isAddingRssEndpoint) return

            addRssEndpoint({
                variables: {
                    url,
                },
            })
                .then(({ data }) => {
                    if (data?.addRssEndpoint?.success) {
                        onSave(filterValues(watchValues))
                    }
                })
                .catch((errors) => {
                    console.error(errors)
                })
        }

        const submitForm = handleSubmit(onSubmit)

        const maxInitialLimit = 10

        return (
            <>
                <FormContainer isLoading={loading || isAddingRssEndpoint}>
                    <Spacer spacing="small">
                        <FormItem
                            control={control}
                            type="text"
                            name="title"
                            id={`${guid}-title`}
                            title={t('form.title')}
                            helper={t('form.title-helper-empty')}
                        />

                        <FormItem
                            control={control}
                            type="text"
                            name="url"
                            id={`${guid}-url`}
                            title={t('widget-rss.feed-url')}
                            helper={t('widget-rss.feed-url-helper')}
                            required
                            onChange={handleUrlChange}
                            rules={{
                                validate: () => {
                                    if (
                                        !loading &&
                                        (!data || data?.testRssEndpoint?.isFeed)
                                    ) {
                                        return true
                                    }
                                    return t('widget-rss.invalid-feed-url')
                                },
                            }}
                            errors={errors}
                        />

                        <FormItem
                            control={control}
                            type="number"
                            name="initialLimit"
                            id={`${guid}-initialLimit`}
                            title={t('widget-rss.initial-limit')}
                            helper={t('widget-rss.initial-limit-helper', {
                                count: maxInitialLimit,
                            })}
                            min={1}
                            max={maxInitialLimit}
                        />
                    </Spacer>
                </FormContainer>
                <WidgetViewPortal
                    Component={Rss}
                    settings={filterValues(watchValues)}
                />
            </>
        )
    },
)

const TEST_RSS_ENDPOINT = gql`
    query TestRssEndpoint($url: String!) {
        testRssEndpoint(url: $url) {
            isFeed
        }
    }
`

const ADD_RSS_ENDPOINT = gql`
    mutation AddRssEndpoint($url: String!) {
        addRssEndpoint(url: $url) {
            success
        }
    }
`

RssEdit.displayName = 'RssEdit'

export default RssEdit
