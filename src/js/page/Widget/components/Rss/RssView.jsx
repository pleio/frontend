import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'
import { media } from 'helpers'
import styled, { css } from 'styled-components'

import Card from 'js/components/Card/Card'
import FetchMore from 'js/components/FetchMore'
import { H3, H4 } from 'js/components/Heading'
import HideVisually from 'js/components/HideVisually/HideVisually'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Spacer from 'js/components/Spacer/Spacer'
import Text from 'js/components/Text/Text'
import Truncate from 'js/components/Truncate/Truncate'
import EditWidgetPlaceholder from 'js/page/Widget/components/components/EditWidgetPlaceholder'

const Wrapper = styled(Card)`
    .RssViewTitle,
    .RssViewList {
        ${media.mobileLandscapeDown`
            padding-left: ${(p) => p.theme.padding.horizontal.small};
            padding-right: ${(p) => p.theme.padding.horizontal.small};
        `};

        ${media.tabletUp`
            padding-left: ${(p) => p.theme.padding.horizontal.normal};
            padding-right: ${(p) => p.theme.padding.horizontal.normal};
        `};
    }

    .RssViewTitle {
        margin-bottom: 12px;
    }

    .RssViewList {
        margin-bottom: 16px;
    }

    ${(p) =>
        p.$hasTitle
            ? css`
                  .RssViewTitle {
                      ${media.mobileLandscapeDown`
                        padding-top: ${(p) => p.theme.padding.vertical.small};
                    `};

                      ${media.tabletUp`
                        padding-top: ${(p) => p.theme.padding.vertical.normal};
                    `};
                  }
              `
            : css`
                  .RssViewList {
                      ${media.mobileLandscapeDown`
                        padding-top: ${(p) => p.theme.padding.vertical.small};
                    `};

                      ${media.tabletUp`
                        padding-top: ${(p) => p.theme.padding.vertical.normal};
                    `};
                  }
              `};
`

const RssView = ({ guid, settings, editMode }) => {
    const { t } = useTranslation()

    const { title, url, initialLimit } = settings

    const [queryLimit, setQueryLimit] = useState(initialLimit)
    const { loading, data, fetchMore, refetch } = useQuery(FETCH_RSS_ENDPOINT, {
        variables: {
            url,
            offset: 0,
            limit: queryLimit,
        },
    })

    useEffect(() => {
        if (editMode) {
            setQueryLimit(initialLimit)
            setTimeout(() => refetch(), 0)
        }
    }, [initialLimit, editMode, setQueryLimit, refetch])

    const noResults =
        !loading && (!data || data?.fetchRssEndpoint?.edges?.length === 0)

    if (!url || (noResults && !title)) {
        if (editMode)
            return <EditWidgetPlaceholder title={t('widget-rss.title')} />
        return null
    }

    return (
        <Wrapper
            $hasTitle={!!title}
            style={{
                display: 'flex',
                flexDirection: 'column',
            }}
        >
            {title ? (
                <H3 as="h2" className="RssViewTitle">
                    {title}
                </H3>
            ) : (
                <HideVisually as="h2">{t('widget-objects.title')}</HideVisually>
            )}

            {!data || loading ? (
                <LoadingSpinner />
            ) : (
                <FetchMore
                    edges={data.fetchRssEndpoint.edges}
                    getMoreResults={(data) => data.fetchRssEndpoint.edges}
                    fetchMore={fetchMore}
                    fetchType="click"
                    fetchCount={5}
                    setLimit={setQueryLimit}
                    isScrollableBox
                    containHeight
                >
                    <Spacer className="RssViewList" as="ul" spacing="small">
                        {data.fetchRssEndpoint.edges.map(
                            ({ title, link, description }, i) => (
                                <li key={`${guid}-${i}`}>
                                    <H4>
                                        <a
                                            href={link}
                                            target="_blank"
                                            rel="noopener noreferrer"
                                        >
                                            <Truncate lines={1}>
                                                <span
                                                    dangerouslySetInnerHTML={{
                                                        __html: title,
                                                    }}
                                                />
                                                <HideVisually>
                                                    (
                                                    {t(
                                                        'global.opens-in-new-window',
                                                    )}
                                                    )
                                                </HideVisually>
                                            </Truncate>
                                        </a>
                                    </H4>
                                    <Truncate lines={2}>
                                        <Text
                                            size="small"
                                            style={{ marginTop: '2px' }}
                                        >
                                            <span
                                                dangerouslySetInnerHTML={{
                                                    __html: description,
                                                }}
                                            />
                                        </Text>
                                    </Truncate>
                                </li>
                            ),
                        )}
                    </Spacer>
                </FetchMore>
            )}
        </Wrapper>
    )
}

const FETCH_RSS_ENDPOINT = gql`
    query FetchRssEndpoint($url: String!, $offset: Int, $limit: Int) {
        fetchRssEndpoint(url: $url, offset: $offset, limit: $limit) {
            edges {
                guid
                title
                link
                description
            }
        }
    }
`

export default RssView
