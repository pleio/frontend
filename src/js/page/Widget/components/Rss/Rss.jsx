import React, { forwardRef } from 'react'

import useGetSetting from 'js/page/Widget/hooks/useGetSetting'

import RssEdit from './RssEdit'
import RssView from './RssView'

const Rss = forwardRef(
    (
        {
            guid,
            entity,
            group,
            isEditing,
            editMode,
            onSave,
            dispatchWidgetOptions,
        },
        ref,
    ) => {
        const getSetting = useGetSetting(entity)

        const settings = {
            title: getSetting('title'),
            url: getSetting('url'),
            initialLimit: parseInt(getSetting('initialLimit', 5), 10),
        }

        if (isEditing) {
            return (
                <RssEdit
                    ref={ref}
                    guid={guid}
                    settings={settings}
                    group={group}
                    onSave={onSave}
                    dispatchWidgetOptions={dispatchWidgetOptions}
                />
            )
        }

        return <RssView guid={guid} settings={settings} editMode={editMode} />
    },
)

Rss.displayName = 'Rss'

export default Rss
