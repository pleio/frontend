import React, { forwardRef } from 'react'

import useGetSetting from 'js/page/Widget/hooks/useGetSetting'

import CodeEdit from './CodeEdit'
import CodeView from './CodeView'

const Code = forwardRef(({ entity, editMode, isEditing, onSave }, ref) => {
    const getSetting = useGetSetting(entity)
    const description = getSetting('description')

    if (isEditing) {
        return <CodeEdit ref={ref} description={description} onSave={onSave} />
    } else {
        return <CodeView description={description} editMode={editMode} />
    }
})

Code.displayName = 'Code'

export default Code
