import React, { forwardRef, useImperativeHandle, useState } from 'react'
import { useTranslation } from 'react-i18next'

import Textarea from 'js/components/Textarea/Textarea'
import WidgetViewPortal from 'js/page/Widget/components/components//WidgetViewPortal'
import convertObjectToWidgetSettings from 'js/page/Widget/helpers/convertObjectToWidgetSettings'

import Code from './Code'

const CodeEdit = forwardRef(({ description, onSave }, ref) => {
    const [inputValue, setInputValue] = useState(description)
    const onChangeInput = (evt) => {
        setInputValue(evt.target.value)
    }

    const filterValues = () => {
        return convertObjectToWidgetSettings({
            description: inputValue,
        })
    }

    const submitForm = () => {
        onSave(filterValues())
    }

    useImperativeHandle(ref, () => ({
        onSave() {
            submitForm()
        },
    }))

    const { t } = useTranslation()

    return (
        <>
            <Textarea
                name="description"
                placeholder={t('widget-code.code-placeholder')}
                value={inputValue}
                onChange={onChangeInput}
            />

            <WidgetViewPortal Component={Code} settings={filterValues()} />
        </>
    )
})

CodeEdit.displayName = 'CodeEdit'

export default CodeEdit
