import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { loadScript } from 'helpers'

import EditWidgetPlaceholder from '../components/EditWidgetPlaceholder'

const CodeView = ({ description, editMode }) => {
    const { t } = useTranslation()

    useEffect(() => {
        const el = document.createElement('html')
        el.innerHTML = description

        const scripts = []
        const evals = []

        const scriptTags = el.getElementsByTagName('script')
        for (let i = 0; i < scriptTags.length; i++) {
            if (scriptTags[i].src) {
                scripts.push(loadScript(scriptTags[i].src))
            } else {
                evals.push(scriptTags[i].innerText)
            }
        }

        Promise.all(scripts).then(() => {
            evals.forEach((code) => {
                // eslint-disable-next-line no-eval
                window.eval(code)
            })
        })
    })

    if (!description) {
        if (!editMode) return null
        return <EditWidgetPlaceholder title={t('widget-code.title')} />
    }

    return (
        <div
            className="cms-block-html"
            dangerouslySetInnerHTML={{
                __html: description,
            }}
        />
    )
}

export default CodeView
