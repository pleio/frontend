import React, { Fragment, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { addDays, isSameDay, setYear, startOfDay, subDays } from 'date-fns'
import { media, useIsMount } from 'helpers'
import { isSameDayAndMonth } from 'helpers/date/parseDate'
import { getAge, showDateWithoutYear } from 'helpers/date/showDate'
import styled from 'styled-components'

import Avatar from 'js/components/Avatar/Avatar'
import Card, { CardContent } from 'js/components/Card/Card'
import FetchMore from 'js/components/FetchMore'
import { H3 } from 'js/components/Heading'
import HideVisually from 'js/components/HideVisually/HideVisually'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Text from 'js/components/Text/Text'
import EditWidgetPlaceholder from 'js/page/Widget/components/components/EditWidgetPlaceholder'

import BirthdayIcon from 'icons/birthday.svg'

const Wrapper = styled(Card)`
    .BirthdaysDate {
        display: block;
        margin-top: 4px;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        color: ${(p) => p.theme.color.text.grey};

        ${media.mobileLandscapeDown`
            padding: 2px ${(p) => p.theme.padding.horizontal.small};
        `};

        ${media.tabletUp`
            padding: 2px ${(p) => p.theme.padding.horizontal.normal};
        `};
    }

    .BirthdaysItem {
        > a {
            display: flex;
            align-items: center;
            font-size: ${(p) => p.theme.font.size.small};
            line-height: ${(p) => p.theme.font.lineHeight.small};

            ${media.mobileLandscapeDown`
                padding: 0 ${(p) => p.theme.padding.horizontal.small};
            `};

            ${media.tabletUp`
                padding: 0 ${(p) => p.theme.padding.horizontal.normal};
            `};

            &:hover .BirthdaysUser {
                text-decoration: underline;
            }
        }

        &:last-child {
            margin-bottom: 16px;
        }

        + .BirthdaysItem {
            border-top: 1px solid ${(p) => p.theme.color.grey[20]};
        }
    }

    .BirthdaysUser {
        overflow: hidden;
        flex-grow: 1;
        padding: 8px 12px 8px 0;
        display: flex;
        align-items: center;
        color: ${(p) => p.theme.color.primary.main};

        span {
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
        }
    }

    .BirthdaysAge {
        flex-shrink: 0;
        display: flex;
        align-items: center;
        font-weight: ${(p) => p.theme.font.weight.semibold};
    }
`

const useBirthDate = () => {
    const { t } = useTranslation()

    return (date) => {
        const currentYear = new Date().getFullYear()
        const birthDate = startOfDay(setYear(new Date(date), currentYear))
        const birthDateLastYear = startOfDay(
            setYear(new Date(date), currentYear - 1),
        )
        const birthDateNextYear = startOfDay(
            setYear(new Date(date), currentYear + 1),
        )

        const today = startOfDay(new Date())
        const yesterday = subDays(today, 1)
        const tomorrow = addDays(today, 1)

        if (isSameDay(birthDate, today)) {
            return t('date.today')
        } else if (
            isSameDay(birthDate, yesterday) ||
            isSameDay(birthDateLastYear, yesterday)
        ) {
            return t('date.yesterday')
        } else if (
            isSameDay(birthDate, tomorrow) ||
            isSameDay(birthDateNextYear, tomorrow)
        ) {
            return t('date.tomorrow')
        } else {
            return showDateWithoutYear(date)
        }
    }
}

const FUTURE_DAYS = 30

const BirthdaysView = ({ settings, groupGuid, editMode }) => {
    const { title, description, profileFieldGuid, showAge } = settings

    const { t } = useTranslation()
    const getBirthDate = useBirthDate()

    const [queryLimit, setQueryLimit] = useState(5)

    const { loading, data, fetchMore, refetch } = useQuery(GET_BIRTHDAYS_VIEW, {
        variables: {
            groupGuid,
            profileFieldGuid,
            offset: 0,
            futureDays: FUTURE_DAYS,
            limit: queryLimit,
            skip: !profileFieldGuid, // skip on the query itself didn't work
        },
    })

    const isMount = useIsMount()

    useEffect(() => {
        if (isMount) {
            // Prevents the wrong height being set on the widget (because of caching). Use useEffect, not useLayoutEffect.
            refetch()
        }
    }, [])

    if (!profileFieldGuid) {
        if (!editMode) return null
        return <EditWidgetPlaceholder title={t('widget-birthdays.title')} />
    }

    return (
        <Wrapper
            style={{
                display: 'flex',
                flexDirection: 'column',
            }}
        >
            {(!!title || !!description) && (
                <CardContent style={{ paddingBottom: 0 }}>
                    {title ? (
                        <H3 as="h2" style={{ marginBottom: '8px' }}>
                            {title}
                        </H3>
                    ) : (
                        <HideVisually as="h2">
                            {t('widget-objects.title')}
                        </HideVisually>
                    )}
                    {description && (
                        <Text size="small" style={{ marginBottom: '8px' }}>
                            {description}
                        </Text>
                    )}
                </CardContent>
            )}

            {loading && (
                <LoadingSpinner
                    style={{
                        flexGrow: 1,
                    }}
                />
            )}

            {!loading && !!data?.usersByBirthDate && (
                <FetchMore
                    isScrollableBox
                    containHeight
                    edges={data.usersByBirthDate.edges}
                    maxLimit={data.usersByBirthDate.total}
                    getMoreResults={(data) => data.usersByBirthDate.edges}
                    fetchMore={fetchMore}
                    setLimit={setQueryLimit}
                    resultsMessage={t('widget-birthdays.result', {
                        count: data.usersByBirthDate.edges.length,
                    })}
                    noResults={
                        <CardContent
                            style={{
                                paddingTop: 0,
                                height: '100%',
                                display: 'flex',
                                alignItems: 'center',
                            }}
                        >
                            <Text
                                size="small"
                                variant="grey"
                                style={{
                                    width: '100%',
                                    textAlign: 'center',
                                }}
                            >
                                {t('widget-birthdays.no-results')}
                            </Text>
                        </CardContent>
                    }
                    style={{
                        paddingTop: !title && !description && '16px',
                    }}
                >
                    <ul>
                        {data.usersByBirthDate.edges.map((user, i, arr) => {
                            const birthdayValue =
                                user.profile?.find(
                                    (el) => el.guid === profileFieldGuid,
                                ).value ?? null

                            if (!birthdayValue) return null

                            const age = getAge(birthdayValue, FUTURE_DAYS)

                            let sameAsPrevious = false
                            if (i > 0) {
                                const previousBirthday = arr[
                                    i - 1
                                ].profile.find(
                                    (el) => el.guid === profileFieldGuid,
                                ).value

                                sameAsPrevious = isSameDayAndMonth(
                                    previousBirthday,
                                    birthdayValue,
                                )
                            }

                            const birthDate = showDateWithoutYear(birthdayValue)

                            return (
                                <Fragment key={user.guid}>
                                    {!sameAsPrevious && (
                                        <li
                                            className="BirthdaysDate"
                                            aria-hidden
                                        >
                                            {getBirthDate(birthdayValue)}
                                        </li>
                                    )}
                                    <li className="BirthdaysItem">
                                        <Link
                                            data-feed={i}
                                            to={user.url}
                                            aria-label={`${
                                                showAge
                                                    ? t(
                                                          'widget-birthdays.birthday-age',
                                                          {
                                                              user: user.name,
                                                              age,
                                                              date: birthDate,
                                                          },
                                                      )
                                                    : t(
                                                          'widget-birthdays.birthday',
                                                          {
                                                              user: user.name,
                                                              date: birthDate,
                                                          },
                                                      )
                                            } ${t(
                                                'profile.see-profile-action',
                                            )}`}
                                        >
                                            <div className="BirthdaysUser">
                                                <Avatar
                                                    disabled
                                                    image={user.icon}
                                                    style={{
                                                        marginRight: '12px',
                                                    }}
                                                />
                                                <span>{user.name}</span>
                                            </div>
                                            <div className="BirthdaysAge">
                                                {showAge && (
                                                    <span
                                                        style={{
                                                            marginRight: '6px',
                                                        }}
                                                    >
                                                        {age}
                                                    </span>
                                                )}
                                                <BirthdayIcon />
                                            </div>
                                        </Link>
                                    </li>
                                </Fragment>
                            )
                        })}
                    </ul>
                </FetchMore>
            )}
        </Wrapper>
    )
}

const GET_BIRTHDAYS_VIEW = gql`
    query BirthdaysView(
        $groupGuid: String
        $profileFieldGuid: String!
        $futureDays: Int
        $offset: Int
        $limit: Int
        $skip: Boolean!
    ) {
        usersByBirthDate(
            groupGuid: $groupGuid
            profileFieldGuid: $profileFieldGuid
            futureDays: $futureDays
            offset: $offset
            limit: $limit
        ) @skip(if: $skip) {
            total
            edges {
                guid
                name
                icon
                url
                profile {
                    guid
                    value
                }
            }
        }
    }
`

export default BirthdaysView
