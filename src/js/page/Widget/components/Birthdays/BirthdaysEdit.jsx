import React, { forwardRef, useImperativeHandle } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { Query } from '@apollo/client/react/components'

import FormItem from 'js/components/Form/FormItem'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Spacer from 'js/components/Spacer/Spacer'
import WidgetViewPortal from 'js/page/Widget/components/components//WidgetViewPortal'
import convertObjectToWidgetSettings from 'js/page/Widget/helpers/convertObjectToWidgetSettings'

import Birthdays from './Birthdays'

const QUERY = gql`
    query BirthdaysEdit {
        site {
            guid
            profileFields {
                guid
                fieldType
                name
            }
        }
    }
`

const BirthdaysEdit = forwardRef(({ guid, group, settings, onSave }, ref) => {
    const { t } = useTranslation()

    useImperativeHandle(ref, () => ({
        onSave() {
            submitForm()
        },
    }))

    const { control, handleSubmit, watch } = useForm({
        defaultValues: settings,
    })

    const watchValues = watch()

    const filterValues = ({
        title,
        description,
        profileFieldGuid,
        showAge,
    }) => {
        return convertObjectToWidgetSettings({
            title,
            description,
            profileFieldGuid,
            showAge: showAge.toString(),
        })
    }

    const onSubmit = () => {
        onSave(filterValues(watchValues))
    }
    const submitForm = handleSubmit(onSubmit)

    return (
        <>
            <Query query={QUERY}>
                {({ loading, data }) => {
                    if (loading) return <LoadingSpinner />

                    const fieldOptions = data.site.profileFields
                        .filter((field) => field.fieldType === 'dateField')
                        .map((field) => {
                            return {
                                label: field.name,
                                value: field.guid,
                            }
                        })

                    return (
                        <Spacer spacing="small">
                            <FormItem
                                control={control}
                                type="text"
                                name="title"
                                label={t('form.title')}
                            />
                            <FormItem
                                control={control}
                                type="textarea"
                                name="description"
                                label={t('form.description')}
                                size="small"
                            />
                            <FormItem
                                control={control}
                                type="select"
                                name="profileFieldGuid"
                                options={fieldOptions}
                                label={t('widget-birthdays.profile-field')}
                                helper={t(
                                    'widget-birthdays.profile-field-helper',
                                )}
                            />
                            <FormItem
                                control={control}
                                type="switch"
                                name="showAge"
                                label={t('widget-birthdays.show-age')}
                                size="small"
                            />
                        </Spacer>
                    )
                }}
            </Query>

            <WidgetViewPortal
                Component={Birthdays}
                settings={filterValues(watchValues)}
                guid={guid}
                group={group}
            />
        </>
    )
})

BirthdaysEdit.displayName = 'BirthdaysEdit'

export default BirthdaysEdit
