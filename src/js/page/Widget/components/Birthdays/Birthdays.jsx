import React, { forwardRef } from 'react'

import useGetSetting from 'js/page/Widget/hooks/useGetSetting'

import BirthdaysEdit from './BirthdaysEdit'
import BirthdaysView from './BirthdaysView'

const Birthdays = forwardRef(
    ({ entity, group, isEditing, onSave, editMode }, ref) => {
        const getSetting = useGetSetting(entity)

        const settings = {
            title: getSetting('title'),
            description: getSetting('description'),
            profileFieldGuid: getSetting('profileFieldGuid'),
            showAge: getSetting('showAge', 'true') === 'true',
        }

        if (isEditing) {
            return (
                <BirthdaysEdit
                    ref={ref}
                    group={group}
                    settings={settings}
                    onSave={onSave}
                />
            )
        }

        return (
            <BirthdaysView
                settings={settings}
                editMode={editMode}
                groupGuid={group?.guid}
            />
        )
    },
)

Birthdays.displayName = 'Birthdays'

export default Birthdays
