import React, { forwardRef } from 'react'
import PropTypes from 'prop-types'

import useGetSetting from 'js/page/Widget/hooks/useGetSetting'

import LinkListEdit from './LinkListEdit/LinkListEdit'
import LinkListView from './LinkListView/LinkListView'

const LinkList = forwardRef(
    ({ guid, containerGuid, entity, isEditing, editMode, onSave }, ref) => {
        const getSetting = useGetSetting(entity)

        const getLinks = (key, defaultValue = '') => {
            const setting = entity?.settings?.find(
                (setting) => setting.key === key,
            )
            return setting ? setting.links : defaultValue
        }

        const settings = {
            title: getSetting('title', ''),
            layout: getSetting('layout', 'list'),
            template: getSetting('template')?.split(',') || [],
            links: getLinks('links', []),
            configurable: getSetting('configurable') === 'true',
        }

        if (isEditing) {
            return (
                <LinkListEdit
                    ref={ref}
                    guid={guid}
                    settings={settings}
                    onSave={onSave}
                    containerGuid={containerGuid}
                />
            )
        }

        return (
            <LinkListView
                guid={guid}
                containerGuid={containerGuid}
                settings={settings}
                userSettings={entity.userSettings}
                editMode={editMode}
            />
        )
    },
)

LinkList.displayName = 'LinkList'

LinkList.propTypes = {
    entity: PropTypes.object,
    isEditing: PropTypes.bool,
    onSave: PropTypes.func,
}

export default LinkList
