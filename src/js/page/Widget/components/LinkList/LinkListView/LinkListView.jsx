import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import IconButton from 'js/components/IconButton/IconButton'
import Modal from 'js/components/Modal/Modal'
import Slider from 'js/components/Slider/Slider'
import EditWidgetPlaceholder from 'js/page/Widget/components/components/EditWidgetPlaceholder'
import ListWidgetWrapper from 'js/page/Widget/components/components/ListWidgetWrapper'

import EditIcon from 'icons/edit-small.svg'

import ConfigureLinks from './components/ConfigureLinks'
import GridLinks from './components/GridLinks'
import LinkCard from './components/LinkCard'
import ListLinks from './components/ListLinks'

const Wrapper = styled(ListWidgetWrapper)`
    position: relative;

    .LinkListConfigureButton {
        position: absolute;
        top: 4px;
        right: 4px;
        z-index: 1;
        opacity: 0;

        body:not(.mouse-user) &:focus {
            opacity: 1;
        }
    }

    &:hover .LinkListConfigureButton {
        opacity: 1;
    }
`

const LinkListView = ({
    guid,
    containerGuid,
    settings,
    userSettings,
    editMode,
}) => {
    const { title, layout, template, links, configurable } = settings

    const { t } = useTranslation()

    const [showConfigureLinks, setShowConfigureLinks] = useState(false)

    const widgetTitle = t('widget-linklist.title')

    if (
        !links ||
        links?.length === 0 ||
        !['carousel', 'grid', 'list'].includes(layout)
    ) {
        if (!editMode) return null
        return <EditWidgetPlaceholder title={widgetTitle} />
    }

    const userSavedLinksEnabled = !editMode && configurable
    const userSavedLinks = userSavedLinksEnabled
        ? getUserSavedLinks(userSettings, links)
        : []

    // If user saved links are empty, show all links
    const filteredLinks = userSavedLinks.length ? userSavedLinks : links

    return (
        <Wrapper layout={layout} title={title} widgetTitle={widgetTitle}>
            {userSavedLinksEnabled && (
                <>
                    <IconButton
                        variant="secondary"
                        size="normal"
                        radiusStyle="rounded"
                        onClick={() => setShowConfigureLinks(true)}
                        tooltip={t('widget-linklist.configure-links')}
                        className="LinkListConfigureButton"
                    >
                        <EditIcon />
                    </IconButton>
                    <Modal
                        title={t('widget-linklist.configure-links')}
                        size="normal"
                        isVisible={showConfigureLinks}
                        onClose={() => setShowConfigureLinks(false)}
                        showCloseButton
                    >
                        <ConfigureLinks
                            guid={guid}
                            containerGuid={containerGuid}
                            template={template}
                            links={links}
                            userSavedLinks={userSavedLinks}
                            onClose={() => setShowConfigureLinks(false)}
                        />
                    </Modal>
                </>
            )}

            {layout === 'carousel' ? (
                <Slider>
                    {filteredLinks.map(
                        ({ title, url, description, image }, i) => {
                            return (
                                <LinkCard
                                    key={i}
                                    title={title || url}
                                    url={url}
                                    description={
                                        template.includes('description') &&
                                        description
                                    }
                                    image={template.includes('image') && image}
                                />
                            )
                        },
                    )}
                </Slider>
            ) : layout === 'grid' ? (
                <GridLinks template={template} links={filteredLinks} />
            ) : layout === 'list' ? (
                <ListLinks template={template} links={filteredLinks} />
            ) : null}
        </Wrapper>
    )
}

const getUserSavedLinks = (userSettings, links) => {
    // Get user saved link ids
    const userSettingsLinkIds = userSettings?.find(
        (setting) => setting.key === 'linkIds',
    )?.value

    // Parse string value to array
    const userSettingsLinkIdsArray = userSettingsLinkIds
        ? JSON.parse(userSettingsLinkIds)
        : []

    // Filter links following the order of saved ids (if it exists)
    const userSavedLinks = []
    userSettingsLinkIdsArray.forEach((id) => {
        const link = links.find((link) => link.id === id)
        if (link) {
            userSavedLinks.push(link)
        }
    })

    return userSavedLinks
}

export default LinkListView
