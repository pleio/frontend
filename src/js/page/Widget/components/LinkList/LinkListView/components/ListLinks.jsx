import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useLocation } from 'react-router-dom'
import { getCleanUrl, isExternalUrl, media } from 'helpers'
import styled from 'styled-components'

import HideVisually from 'js/components/HideVisually/HideVisually'
import Img from 'js/components/Img/Img'

import ArrowRightIcon from 'icons/chevron-right.svg'
import ExternalIcon from 'icons/new-window-small.svg'

const ListLinks = ({ template, links }) => {
    return (
        <ul style={{ paddingTop: '12px', paddingBottom: '16px' }}>
            {links.map(({ title, url, image }, i) => {
                return (
                    <ListLink
                        key={i}
                        title={title || url}
                        url={url}
                        image={template.includes('image') && image}
                    />
                )
            })}
        </ul>
    )
}

const ListLinkWrapper = styled.li`
    &:not(:last-child) {
        border-bottom: 1px solid ${(p) => p.theme.color.grey[20]};
    }

    > a {
        display: flex;
        align-items: center;

        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        color: ${(p) => p.theme.color.text.black};

        &:not([disabled]) {
            padding-top: 10px;
            padding-bottom: 10px;
        }

        &:hover .LinkItemTitle {
            text-decoration: underline;
        }

        ${media.mobileLandscapeDown`
            padding: 0 ${(p) => p.theme.padding.horizontal.small};
        `};

        ${media.tabletUp`
            padding: 0 ${(p) => p.theme.padding.horizontal.normal};
        `};
    }

    .ConfigureLinkItemImage {
        width: 20px;
        margin-right: 6px;
    }

    .LinkItemTitle {
        min-height: ${(p) => p.theme.font.lineHeight.small};
        margin-right: auto;
        color: ${(p) => p.theme.color.primary.main};
    }

    .LinkItemIcon {
        width: 12px;
        flex-shrink: 0;
        margin-left: 8px;

        svg {
            margin: 0 auto;
        }
    }
`

const ListLink = ({ title, url, image }) => {
    const { t } = useTranslation()
    const location = useLocation()

    const cleanUrl = getCleanUrl(url)
    const isExternal = isExternalUrl(cleanUrl)
    const LinkElement = isExternal ? 'a' : Link

    return (
        <ListLinkWrapper>
            <LinkElement
                href={isExternal ? cleanUrl : null}
                to={!isExternal ? cleanUrl : null}
                state={
                    !isExternal
                        ? {
                              prevPathname: location.pathname,
                          }
                        : null
                }
                target={isExternal ? '_blank' : null}
                rel={isExternal ? 'noopener noreferrer' : null}
            >
                {image && (
                    <Img
                        src={image?.url}
                        align="center"
                        aria-hidden
                        className="ConfigureLinkItemImage"
                    />
                )}
                <div className="LinkItemTitle">
                    {title}
                    {isExternal && (
                        <HideVisually>
                            {t('global.opens-in-new-window')}
                        </HideVisually>
                    )}
                </div>
                <div className="LinkItemIcon">
                    {!isExternal ? <ArrowRightIcon /> : <ExternalIcon />}
                </div>
            </LinkElement>
        </ListLinkWrapper>
    )
}

export default ListLinks
