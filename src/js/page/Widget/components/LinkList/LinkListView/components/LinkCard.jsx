import React from 'react'

import CardContent from 'js/components/Card/CardContent'
import FeedItem from 'js/components/FeedItem/FeedItem'
import FeedItemImage from 'js/components/FeedItem/FeedItemImage'
import { H3 } from 'js/components/Heading'
import LinkItem from 'js/components/LinkItem/LinkItem'
import TiptapView from 'js/components/Tiptap/TiptapView'

const Card = ({ title, url, description, image }) => {
    return (
        <FeedItem>
            <CardContent className="FeedItemContent">
                {title && (
                    <H3 className="FeedItemTitle">
                        <LinkItem url={url}>{title}</LinkItem>
                    </H3>
                )}

                {description && (
                    <div className="FeedItemExcerpt">
                        <TiptapView
                            content={description}
                            contentType={'html'}
                        />
                    </div>
                )}
            </CardContent>
            {image?.url && (
                <FeedItemImage
                    entity={{
                        url,
                        featured: {
                            image: { download: image?.url },
                            alt: title,
                        },
                    }}
                />
            )}
        </FeedItem>
    )
}

export default Card
