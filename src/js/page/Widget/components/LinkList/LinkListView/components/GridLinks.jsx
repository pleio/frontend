import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { getCleanUrl, isExternalUrl } from 'helpers'
import styled from 'styled-components'

import HideVisually from 'js/components/HideVisually/HideVisually'
import Img from 'js/components/Img/Img'

import ExternalIcon from 'icons/new-window-small.svg'

const Wrapper = styled.ul`
    padding: 12px 12px 10px;
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(75px, 1fr));
    grid-gap: 8px;
    overflow-wrap: anywhere;

    > * {
        flex: 1 1 0px;
    }
`

const GridLinks = ({ template, links }) => {
    return (
        <Wrapper>
            {links.map(({ title, url, image }, i) => {
                return (
                    <li key={i}>
                        <GridLink
                            title={title || url}
                            url={url}
                            image={template.includes('image') && image}
                        />
                    </li>
                )
            })}
        </Wrapper>
    )
}

const GridLinkWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;

    &:hover,
    &:active {
        .GridLinkTitle {
            text-decoration: underline;
        }
    }

    .GridLinkTitle {
        margin-top: 4px;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        color: ${(p) => p.theme.color.primary.main};
        text-align: center;
        word-break: normal; // Better wrapping for long titles

        svg {
            display: inline;
            margin-left: 4px;
        }
    }

    .GridLinkImage {
        width: 100%;
        height: 48px;
    }
`

const GridLink = ({ title, url, image }) => {
    const { t } = useTranslation()

    const cleanUrl = getCleanUrl(url)
    const isExternal = isExternalUrl(cleanUrl)
    const LinkElement = isExternal ? 'a' : Link

    return (
        <GridLinkWrapper
            as={LinkElement}
            href={isExternal ? cleanUrl : null}
            to={!isExternal ? cleanUrl : null}
            target={isExternal ? '_blank' : null}
            rel={isExternal ? 'noopener noreferrer' : null}
        >
            {image && (
                <Img
                    src={image?.url}
                    aria-hidden
                    align="center"
                    className="GridLinkImage"
                />
            )}
            {title && (
                <div className="GridLinkTitle">
                    {title}
                    {isExternal && (
                        <>
                            <HideVisually>
                                {t('global.opens-in-new-window')}
                            </HideVisually>
                            <ExternalIcon />
                        </>
                    )}
                </div>
            )}
        </GridLinkWrapper>
    )
}

export default GridLinks
