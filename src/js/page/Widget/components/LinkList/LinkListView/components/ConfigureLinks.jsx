import React, { useState } from 'react'
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { gql, useMutation } from '@apollo/client'
import { produce } from 'immer'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import { Col, Row } from 'js/components/Grid/Grid'
import IconButton from 'js/components/IconButton/IconButton'
import Img from 'js/components/Img/Img'
import Setting from 'js/components/Setting'

import AddIcon from 'icons/add.svg'
import CrossIcon from 'icons/cross.svg'
import MoveIcon from 'icons/move-vertical.svg'

const Wrapper = styled.div`
    .ConfigureLinkItem {
        display: flex;
        background: white;
        border-bottom: 1px solid transparent;

        &:not(:last-child) {
            border-bottom-color: ${(p) => p.theme.color.grey[20]};
        }

        &[aria-current='dragging'] {
            border-bottom-color: transparent;
            box-shadow: ${(p) => p.theme.shadow.soft.center};
            border-radius: ${(p) => p.theme.radius.small};
        }
    }

    .ConfigureLinkItemImage {
        width: 20px;
        margin-right: 6px;
    }

    .ConfigureLinkItemText {
        margin-right: auto;
        padding: 9px 0;
        line-height: ${(p) => p.theme.font.lineHeight.small};
    }

    a.ConfigureLinkItemTitle {
        color: ${(p) => p.theme.color.primary.main};

        &:hover,
        &:focus {
            text-decoration: underline;
        }
    }

    .ConfigureLinkItemTitle {
        margin-right: 4px;
        font-size: ${(p) => p.theme.font.size.small};
    }

    .ConfigureLinkItemMove {
        display: flex;
        align-items: center;
        height: 40px;
        margin-right: 8px;
    }
`

const ConfigureLinks = ({
    guid,
    containerGuid,
    template,
    links,
    userSavedLinks,
    onClose,
}) => {
    const { t } = useTranslation()

    const [configuredLinks, setConfiguredLinks] = useState(userSavedLinks)

    const onDragEnd = (result) => {
        if (!result.destination) return

        const sourcePosition = result.source.index
        const destinationPosition = result.destination.index
        setConfiguredLinks(
            produce((newState) => {
                const [movedItem] = newState.splice(sourcePosition, 1)
                newState.splice(destinationPosition, 0, movedItem)
            }),
        )
    }

    const [updateUserWidgetSettings, { loading }] = useMutation(
        UPDATE_USER_WIDGET_SETTINGS,
    )

    const handleConfirm = async () => {
        await updateUserWidgetSettings({
            variables: {
                input: {
                    guid,
                    containerGuid,
                    settings: [
                        {
                            key: 'linkIds',
                            value: JSON.stringify(
                                configuredLinks.map((link) => link.id),
                            ),
                        },
                    ],
                },
            },
            refetchQueries: ['PageView'],
        })
            .then(() => {
                onClose()
            })
            .catch((e) => {
                console.error(e)
            })
    }

    return (
        <Wrapper>
            <Row $spacing={20} $gutter={30}>
                <Col mobileLandscapeUp={1 / 2}>
                    <Setting
                        subtitle="widget-linklist.available-links"
                        helper="widget-linklist.available-links-helper"
                    />

                    <ul style={{ marginTop: '8px' }}>
                        {links
                            .filter(
                                (link) =>
                                    !configuredLinks.find(
                                        (configuredLink) =>
                                            link.id === configuredLink.id,
                                    ),
                            )
                            .map((link) => {
                                const { id, title, url, image } = link

                                const addSuggested = () => {
                                    setConfiguredLinks([
                                        ...configuredLinks,
                                        link,
                                    ])
                                }

                                const itemTitle = title || url

                                return (
                                    <li key={id} className="ConfigureLinkItem">
                                        {template.includes('image') && (
                                            <Img
                                                src={image?.url}
                                                align="center"
                                                aria-hidden
                                                className="ConfigureLinkItemImage"
                                            />
                                        )}
                                        <div className="ConfigureLinkItemText">
                                            <Link className="ConfigureLinkItemTitle">
                                                {itemTitle}
                                            </Link>
                                        </div>
                                        <IconButton
                                            variant="secondary"
                                            size="large"
                                            hoverSize="normal"
                                            radiusStyle="rounded"
                                            onClick={addSuggested}
                                            aria-label={t(
                                                'form.add-to-suggested',
                                                {
                                                    title: itemTitle,
                                                },
                                            )}
                                        >
                                            <AddIcon />
                                        </IconButton>
                                    </li>
                                )
                            })}
                    </ul>
                </Col>
                <Col mobileLandscapeUp={1 / 2}>
                    <Setting
                        subtitle="widget-linklist.your-links"
                        helper="widget-linklist.your-links-helper"
                    />

                    {configuredLinks.length > 0 ? (
                        <>
                            <DragDropContext onDragEnd={onDragEnd}>
                                <Droppable droppableId="suggested-items">
                                    {(provided) => (
                                        <>
                                            <ul
                                                style={{ marginTop: '8px' }}
                                                ref={provided.innerRef}
                                            >
                                                {configuredLinks.map(
                                                    (link, i) => {
                                                        const {
                                                            id,
                                                            title,
                                                            url,
                                                            image,
                                                        } = link

                                                        const removeSuggested =
                                                            () => {
                                                                setConfiguredLinks(
                                                                    produce(
                                                                        (
                                                                            newState,
                                                                        ) => {
                                                                            newState.splice(
                                                                                i,
                                                                                1,
                                                                            )
                                                                        },
                                                                    ),
                                                                )
                                                            }

                                                        const itemTitle =
                                                            title || url

                                                        return (
                                                            <Draggable
                                                                key={`${id}-draggable`}
                                                                draggableId={id}
                                                                index={i}
                                                            >
                                                                {(
                                                                    provided,
                                                                    snapshot,
                                                                ) => (
                                                                    <li
                                                                        ref={
                                                                            provided.innerRef
                                                                        }
                                                                        className="ConfigureLinkItem"
                                                                        aria-current={
                                                                            snapshot.isDragging
                                                                                ? 'dragging'
                                                                                : null
                                                                        }
                                                                        {...provided.draggableProps}
                                                                        {...provided.dragHandleProps}
                                                                    >
                                                                        <div className="ConfigureLinkItemMove">
                                                                            <MoveIcon />
                                                                        </div>
                                                                        <Img
                                                                            src={
                                                                                image?.url
                                                                            }
                                                                            align="center"
                                                                            aria-hidden
                                                                            className="ConfigureLinkItemImage"
                                                                        />
                                                                        <div className="ConfigureLinkItemText">
                                                                            <span className="ConfigureLinkItemTitle">
                                                                                {
                                                                                    itemTitle
                                                                                }
                                                                            </span>
                                                                        </div>
                                                                        <IconButton
                                                                            className="ConfigureLinkItemRemove"
                                                                            variant="secondary"
                                                                            size="large"
                                                                            hoverSize="normal"
                                                                            radiusStyle="rounded"
                                                                            onClick={
                                                                                removeSuggested
                                                                            }
                                                                            aria-label={t(
                                                                                'form.remove-from-suggested',
                                                                                {
                                                                                    title: itemTitle,
                                                                                },
                                                                            )}
                                                                        >
                                                                            <CrossIcon />
                                                                        </IconButton>
                                                                    </li>
                                                                )}
                                                            </Draggable>
                                                        )
                                                    },
                                                )}
                                            </ul>
                                            {provided.placeholder}
                                        </>
                                    )}
                                </Droppable>
                            </DragDropContext>
                        </>
                    ) : null}
                </Col>
            </Row>
            <Flexer mt>
                <Button
                    size="normal"
                    variant="tertiary"
                    type="button"
                    onClick={onClose}
                >
                    {t('action.cancel')}
                </Button>
                <Button
                    size="normal"
                    variant="primary"
                    loading={loading}
                    onClick={handleConfirm}
                >
                    {t('action.save')}
                </Button>
            </Flexer>
        </Wrapper>
    )
}

const UPDATE_USER_WIDGET_SETTINGS = gql`
    mutation UpdateUserWidgetSettings($input: userWidgetSettingsInput!) {
        updateUserWidgetSettings(input: $input) {
            success
        }
    }
`

export default ConfigureLinks
