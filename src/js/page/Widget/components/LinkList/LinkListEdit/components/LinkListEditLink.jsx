import React, { useState } from 'react'
import { Draggable } from 'react-beautiful-dnd'
import { useTranslation } from 'react-i18next'
import styled, { css } from 'styled-components'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import IconButton from 'js/components/IconButton/IconButton'
import Img from 'js/components/Img/Img'
import Spacer from 'js/components/Spacer/Spacer'
import UploadOrSelectFilesModal from 'js/files/UploadOrSelectFiles/UploadOrSelectFilesModal'

import RemoveIcon from 'icons/delete-small.svg'
import EditIcon from 'icons/edit-small.svg'
import MoveIcon from 'icons/move-vertical.svg'

const Wrapper = styled.li`
    overflow: hidden;
    font-size: ${(p) => p.theme.font.size.small};
    line-height: ${(p) => p.theme.font.lineHeight.small};
    color: ${(p) => p.theme.color.text.black};
    background-color: white;
    border-bottom: 1px solid ${(p) => p.theme.color.grey[20]};

    &:last-child {
        border-bottom-color: transparent;
    }

    ${(p) =>
        p.$isDragging &&
        css`
            box-shadow: ${p.theme.shadow.soft.center};
            border-bottom-color: transparent;
        `};

    .LinkListEditLinkLabel {
        flex-grow: 1;
        align-self: stretch;
        display: flex;
        align-items: center;
        padding: 4px 0;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};

        ${(p) =>
            p.$hasTitle
                ? css`
                      font-weight: ${(p) => p.theme.font.weight.semibold};
                  `
                : css`
                      color: ${(p) => p.theme.color.text.grey};
                  `};
    }

    .LinkListEditLinkProperties {
        padding: 8px 20px 16px;
    }
`

const LinkListEditLink = ({
    id,
    index,
    control,
    setValue,
    watchLinks,
    onRemove,
}) => {
    const { t } = useTranslation()

    const { title, description, image, url, isEditing } =
        watchLinks?.[index] ?? {}

    const toggleIsEditing = () => {
        if (!isEditing) {
            // Hide description field if it's empty, show if it's not
            setShowDescription(!!description)
        }

        setValue(`links.${index}.isEditing`, !isEditing)
    }

    const [showDescription, setShowDescription] = useState(false)
    const [showUploadModal, setShowUploadModal] = useState(false)

    const buttonProps = {
        type: 'button',
        size: 'large',
        variant: 'secondary',
        hoverSize: 'normal',
        radiusStyle: 'rounded',
    }

    const itemTitle = title || url || t('global.new-item')

    return (
        <Draggable draggableId={id} index={index}>
            {(provided, snapshot) => (
                <Wrapper
                    ref={provided.innerRef}
                    $isDragging={snapshot.isDragging}
                    $hasTitle={title || url}
                    {...provided.draggableProps}
                >
                    <div style={{ display: 'flex', alignItems: 'flex-start' }}>
                        <IconButton
                            as="div"
                            size="large"
                            variant="secondary"
                            hoverSize="normal"
                            aria-label={t('aria.move-link', {
                                label: itemTitle,
                            })}
                            {...provided.dragHandleProps}
                        >
                            <MoveIcon />
                        </IconButton>

                        <div
                            className="LinkListEditLinkLabel"
                            aria-label={t('aria.move-link', {
                                label: itemTitle,
                            })}
                            {...provided.dragHandleProps}
                        >
                            {itemTitle}
                        </div>

                        <Flexer gutter="tiny" divider="normal">
                            <IconButton
                                {...buttonProps}
                                onClick={toggleIsEditing}
                                aria-pressed={isEditing}
                                aria-label={t('aria.edit-link', {
                                    label: itemTitle,
                                })}
                                tooltip={t('action.edit')}
                            >
                                <EditIcon />
                            </IconButton>
                            <IconButton
                                {...buttonProps}
                                onClick={onRemove}
                                aria-label={t('aria.remove-link', {
                                    label: itemTitle,
                                })}
                                tooltip={t('action.delete')}
                            >
                                <RemoveIcon />
                            </IconButton>
                        </Flexer>
                    </div>

                    {isEditing && (
                        <Spacer
                            spacing="small"
                            className="LinkListEditLinkProperties"
                        >
                            <FormItem
                                control={control}
                                type="text"
                                name={`links.${index}.title`}
                                label={t('form.title')}
                            />

                            <FormItem
                                control={control}
                                type="text"
                                name={`links.${index}.url`}
                                label={t('form.link')}
                            />

                            {showDescription && (
                                <FormItem
                                    control={control}
                                    type="rich"
                                    name={`links.${index}.description`}
                                    title={t('form.description')}
                                    options={{
                                        textLink: true,
                                    }}
                                    contentType={'html'}
                                    textSize="small"
                                />
                            )}

                            {(!showDescription || !image) && (
                                <Flexer justifyContent="flex-start">
                                    {!showDescription && (
                                        <Button
                                            size="small"
                                            variant="tertiary"
                                            onClick={() => {
                                                setShowDescription(
                                                    (desc) => !desc,
                                                )
                                                setShowUploadModal(false)
                                            }}
                                        >
                                            {t('form.add-description')}
                                        </Button>
                                    )}
                                    {!image && (
                                        <Button
                                            size="small"
                                            variant="tertiary"
                                            onClick={() => {
                                                setShowUploadModal(
                                                    (img) => !img,
                                                )
                                                setShowDescription(false)
                                            }}
                                        >
                                            {t('form.add-image')}
                                        </Button>
                                    )}
                                </Flexer>
                            )}

                            <FormItem
                                type="text"
                                control={control}
                                value={image}
                                name={`links.${index}.image`}
                                style={{ display: 'none' }}
                            />

                            {image && (
                                <>
                                    <Img src={image?.url} aria-hidden />
                                    <Flexer
                                        justifyContent="flex-start"
                                        style={{ marginTop: 12 }}
                                    >
                                        <Button
                                            size="small"
                                            variant="secondary"
                                            onClick={() => {
                                                setShowUploadModal(
                                                    (img) => !img,
                                                )
                                                setShowDescription(false)
                                            }}
                                        >
                                            {t('form.edit-image')}..
                                        </Button>
                                        <Button
                                            size="small"
                                            variant="secondary"
                                            onClick={() => {
                                                setValue(
                                                    `links.${index}.image`,
                                                    '',
                                                )
                                            }}
                                        >
                                            {t('form.remove-image')}
                                        </Button>
                                    </Flexer>
                                </>
                            )}
                        </Spacer>
                    )}

                    <UploadOrSelectFilesModal
                        title={t('action.insert-image')}
                        accept="image"
                        showModal={showUploadModal}
                        setShowModal={setShowUploadModal}
                        onComplete={({ guid, download }) => {
                            setValue(`links.${index}.image`, {
                                id: guid,
                                url: download,
                            })
                        }}
                    />
                </Wrapper>
            )}
        </Draggable>
    )
}

export default LinkListEditLink
