import React from 'react'
import { useTranslation } from 'react-i18next'

import FormItem from 'js/components/Form/FormItem'
import Spacer from 'js/components/Spacer/Spacer'

const General = ({ guid, control, templateOptions }) => {
    const { t } = useTranslation()

    return (
        <Spacer spacing="small">
            <FormItem
                control={control}
                type="text"
                name="title"
                id={`title-${guid}`}
                label={t('form.title')}
                helper={t('form.title-helper-empty')}
            />

            <FormItem
                type="radio"
                control={control}
                name="layout"
                id={`layout-${guid}`}
                size="small"
                title={t('widget.layout')}
                options={[
                    {
                        value: 'list',
                        label: t('widget.layout-list'),
                    },
                    {
                        value: 'grid',
                        label: t('widget.layout-grid'),
                    },
                    {
                        value: 'carousel',
                        label: t('widget.layout-carousel'),
                    },
                ]}
                gridTemplateColumns="1fr 1fr"
            />

            {templateOptions?.length > 0 && (
                <FormItem
                    control={control}
                    type="checkboxes"
                    name="template"
                    id={`template-${guid}`}
                    size="small"
                    options={templateOptions}
                    title={t('widget.template')}
                    gridTemplateColumns="1fr 1fr"
                />
            )}

            <FormItem
                control={control}
                type="switch"
                name="configurable"
                id={`configurable-${guid}`}
                size="small"
                prependField
                title={t('widget-linklist.configurable')}
                helper={t('widget-linklist.configurable-helper')}
            />
        </Spacer>
    )
}

export default General
