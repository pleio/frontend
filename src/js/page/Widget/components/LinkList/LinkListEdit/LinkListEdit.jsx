import React, { forwardRef, useImperativeHandle, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import Spacer from 'js/components/Spacer/Spacer'
import TabMenu from 'js/components/TabMenu'
import TabPage from 'js/components/TabPage'
import WidgetViewPortal from 'js/page/Widget/components/components//WidgetViewPortal'
import convertObjectToWidgetSettings from 'js/page/Widget/helpers/convertObjectToWidgetSettings'

import LinkList from '../LinkList'

import Content from './components/Content'
import General from './components/General'

const LinkListEdit = forwardRef(
    ({ guid, containerGuid, settings, onSave }, ref) => {
        const { t } = useTranslation()

        useImperativeHandle(ref, () => ({
            onSave() {
                submitForm()
            },
        }))

        const [tab, setTab] = useState('general')

        const { title, layout, template, links, configurable } = settings

        const { control, handleSubmit, watch, setValue } = useForm({
            defaultValues: {
                title,
                layout,
                template,
                links,
                configurable,
            },
        })
        const watchValues = watch()

        const filterValues = ({
            title,
            layout,
            template,
            links,
            configurable,
        }) => {
            // Prevent saving more template options than the layout allows (would result in bigger query than needed)
            const filteredTemplate = template.filter((t) =>
                templateOptions.find((o) => o.value === t),
            )

            const filteredLinks = links
                .filter(({ title, url }) => title || url)
                .map(({ id, title, url, description, image }) => ({
                    id,
                    title,
                    url,
                    description,
                    image,
                }))

            return convertObjectToWidgetSettings({
                title,
                layout,
                template: filteredTemplate.join(','),
                links: {
                    links: filteredLinks,
                },
                configurable: configurable.toString(),
            })
        }

        const onSubmit = () => {
            onSave(filterValues(watchValues))
        }
        const submitForm = handleSubmit(onSubmit)

        const layoutIsCarousel = watch('layout') === 'carousel'

        const templateOptions = [
            {
                value: 'image',
                label: t('global.image'),
            },
            ...(layoutIsCarousel
                ? [
                      {
                          value: 'description',
                          label: t('form.description'),
                      },
                  ]
                : []),
        ]

        return (
            <>
                <Spacer spacing="small">
                    <TabMenu
                        label={t('global.settings')}
                        options={[
                            {
                                onClick: () => setTab('general'),
                                label: t('global.general'),
                                isActive: tab === 'general',
                                id: 'tab-general',
                                ariaControls: 'tabpanel-general',
                            },
                            {
                                onClick: () => setTab('content'),
                                label: t('global.content'),
                                isActive: tab === 'content',
                                id: 'tab-content',
                                ariaControls: 'tabpanel-content',
                            },
                        ]}
                        edgePadding
                        edgeMargin
                        showBorder
                    />

                    <TabPage
                        id={`tabpanel-${tab}`}
                        ariaLabelledby={`tab-${tab}`}
                        visible
                    >
                        {tab === 'general' && (
                            <General
                                guid={guid}
                                control={control}
                                templateOptions={templateOptions}
                            />
                        )}

                        {tab === 'content' && (
                            <Content
                                guid={guid}
                                control={control}
                                setValue={setValue}
                            />
                        )}
                    </TabPage>
                </Spacer>

                <WidgetViewPortal
                    Component={LinkList}
                    settings={filterValues(watchValues)}
                    guid={guid}
                    containerGuid={containerGuid}
                />
            </>
        )
    },
)

LinkListEdit.displayName = 'LinkListEdit'

export default LinkListEdit
