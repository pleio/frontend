import React, { forwardRef, useImperativeHandle, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { Query } from '@apollo/client/react/components'

import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Select from 'js/components/Select/Select'
import WidgetViewPortal from 'js/page/Widget/components/components//WidgetViewPortal'
import convertObjectToWidgetSettings from 'js/page/Widget/helpers/convertObjectToWidgetSettings'

import Poll from './Poll'

const pollWidgetQuery = gql`
    query PollsList {
        entities(offset: 0, limit: 99, subtype: "poll") {
            total
            edges {
                guid
                ... on Poll {
                    title
                }
            }
        }
    }
`

const PollEdit = forwardRef(({ pollGuid, onSave }, ref) => {
    const { t } = useTranslation()

    const [value, setValue] = useState(pollGuid)

    useImperativeHandle(ref, () => ({
        onSave() {
            handleSave()
        },
    }))

    const handleChange = (value) => {
        setValue(value)
    }

    const filterValues = () => {
        return convertObjectToWidgetSettings({
            pollGuid: value,
        })
    }

    const handleSave = () => {
        onSave(filterValues())
    }

    // Have to use Query component, because old widget system uses refs for onSave
    return (
        <>
            <Query query={pollWidgetQuery}>
                {({ loading, data }) => {
                    if (loading) return <LoadingSpinner />

                    const options = data.entities.edges.map((entity) => {
                        return {
                            value: entity.guid,
                            label: entity.title,
                        }
                    })

                    return (
                        <Select
                            name="pollItem"
                            label={t('widget-poll.choose-a-poll')}
                            options={options}
                            value={value}
                            onChange={handleChange}
                        />
                    )
                }}
            </Query>

            <WidgetViewPortal Component={Poll} settings={filterValues()} />
        </>
    )
})

PollEdit.displayName = 'PollEdit'

export default PollEdit
