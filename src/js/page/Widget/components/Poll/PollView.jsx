import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import Card, { CardContent } from 'js/components/Card/Card'
import { H3 } from 'js/components/Heading'
import Tiptap from 'js/components/Tiptap/TiptapView'
import EditWidgetPlaceholder from 'js/page/Widget/components/components/EditWidgetPlaceholder'

import PollViewForm from './PollViewForm'

const PollView = ({ data, editMode, ...rest }) => {
    const { t } = useTranslation()

    if (!data.entity && !editMode) return null

    if (!data.entity && editMode) {
        return <EditWidgetPlaceholder title={t('widget-poll.title')} />
    }

    const { viewer, entity } = data
    const { title, richDescription } = entity

    return (
        <Card {...rest}>
            <CardContent>
                <H3 as="h2" style={{ marginBottom: '8px' }}>
                    {title}
                </H3>
                {richDescription && (
                    <div style={{ marginBottom: '8px' }}>
                        <Tiptap content={richDescription} />
                    </div>
                )}
                <PollViewForm
                    entity={entity}
                    viewer={viewer}
                    size="small"
                    legend={title}
                />
            </CardContent>
        </Card>
    )
}

const Query = gql`
    query Poll($guid: String!) {
        viewer {
            guid
            loggedIn
        }
        entity(guid: $guid) {
            guid
            ... on Poll {
                title
                richDescription
                url
                hasVoted
                canEdit
                choices {
                    guid
                    text
                    votes
                }
            }
        }
    }
`

export default graphql(Query)(PollView)
