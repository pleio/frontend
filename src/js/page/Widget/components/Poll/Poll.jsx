import React, { forwardRef } from 'react'

import useGetSetting from 'js/page/Widget/hooks/useGetSetting'

import PollEdit from './PollEdit'
import PollView from './PollView'

const Poll = forwardRef(({ entity, isEditing, editMode, onSave }, ref) => {
    const getSetting = useGetSetting(entity)

    const pollGuid = getSetting('pollGuid')

    if (isEditing) {
        return <PollEdit ref={ref} pollGuid={pollGuid} onSave={onSave} />
    } else {
        return <PollView guid={pollGuid} editMode={editMode} />
    }
})

Poll.displayName = 'Poll'

export default Poll
