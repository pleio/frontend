import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

import { AriaLiveMessage } from 'js/components/AriaLive'
import Button from 'js/components/Button/Button'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import RadioFields from 'js/components/RadioField/RadioFields'
import Text from 'js/components/Text/Text'

const Choice = styled.div`
    padding: 4px 0;

    ${(p) =>
        p.size === 'small' &&
        css`
            font-size: ${(p) => p.theme.font.size.small};
            line-height: ${(p) =>
                p.theme.font.lineHeight
                    .small}; /* aligns percentage with text */

            .ChoicePercentage {
                width: 34px;
                font-size: ${(p) => p.theme.font.size.tiny};
            }
        `};

    ${(p) =>
        p.size === 'normal' &&
        css`
            font-size: ${(p) => p.theme.font.size.normal};
            line-height: ${(p) =>
                p.theme.font.lineHeight
                    .normal}; /* aligns percentage with text */

            .ChoicePercentage {
                width: 48px;
                font-size: ${(p) => p.theme.font.size.small};
            }
        `};

    .ChoicePercentage {
        flex-shrink: 0;
        font-weight: ${(p) => p.theme.font.weight.semibold};
    }

    .ChoiceBar {
        margin-top: 6px;
        height: 4px;
        border-radius: 2px;
        background-color: ${(p) => p.theme.color.secondary.main};
    }
`

const PollViewForm = ({ mutate, viewer, entity, size = 'normal', legend }) => {
    const { t } = useTranslation()
    const { guid, choices, hasVoted, canEdit } = entity

    const [value, setValue] = useState(null)
    const [errors, setErrors] = useState([])
    const [showVotes, setShowVotes] = useState(!viewer?.loggedIn || hasVoted)

    const toggleShowVotes = () => {
        setShowVotes(!showVotes)
    }

    const handleChange = (evt) => {
        setValue(evt.target.value)
    }

    const handleSubmit = () => {
        mutate({
            variables: {
                input: {
                    guid,
                    response: choices.find((el) => el.guid === value).text,
                },
            },
        })
            .then(() => {
                toggleShowVotes()
            })
            .catch((errors) => {
                setErrors(errors)
            })
    }

    let votes = 0
    choices.forEach((choice) => {
        votes += choice.votes
    })

    if (showVotes) {
        return (
            <>
                <AriaLiveMessage
                    message={t('entity-poll.vote', { count: votes })}
                />
                {choices.map((choice, index) => {
                    const percentage =
                        Math.round((choice.votes / votes) * 100) || 0

                    return (
                        <Choice key={index} size={size}>
                            <div style={{ display: 'flex' }}>
                                <div className="ChoicePercentage">
                                    {percentage}%
                                </div>
                                {choice.text}
                            </div>
                            {percentage ? (
                                <div
                                    className="ChoiceBar"
                                    style={{ width: `${percentage}%` }}
                                />
                            ) : null}
                        </Choice>
                    )
                })}
                <Text size="small" variant="grey" style={{ marginTop: '8px' }}>
                    {t('entity-poll.vote', { count: votes })}
                </Text>
                {viewer?.loggedIn && !hasVoted && (
                    <Flexer style={{ marginTop: '16px' }}>
                        <Button
                            size="normal"
                            variant="secondary"
                            onClick={toggleShowVotes}
                        >
                            {t('entity-poll.vote-action')}
                        </Button>
                    </Flexer>
                )}
            </>
        )
    } else {
        return (
            <>
                <RadioFields
                    legend={legend}
                    size={size}
                    name="choices"
                    options={choices.map((choice) => {
                        return {
                            value: choice.guid,
                            label: choice.text,
                        }
                    })}
                    value={value}
                    onChange={handleChange}
                />
                <Errors errors={errors} />
                <Flexer style={{ marginTop: '16px' }}>
                    {canEdit && (
                        <Button
                            size="normal"
                            variant="secondary"
                            onClick={toggleShowVotes}
                        >
                            {t('entity-poll.view-votes')}
                        </Button>
                    )}
                    <Button
                        size="normal"
                        variant="primary"
                        disabled={!value}
                        onClick={handleSubmit}
                    >
                        {t('entity-poll.vote-action')}
                    </Button>
                </Flexer>
            </>
        )
    }
}

const Mutation = gql`
    mutation Poll($input: voteOnPollInput!) {
        voteOnPoll(input: $input) {
            entity {
                guid
                ... on Poll {
                    hasVoted
                    choices {
                        guid
                        text
                        votes
                    }
                }
            }
        }
    }
`

PollViewForm.propTypes = {
    size: PropTypes.oneOf(['small', 'normal']),
}

export default graphql(Mutation)(PollViewForm)
