import React, { forwardRef } from 'react'
import { useTranslation } from 'react-i18next'

import EditWidgetPlaceholder from 'js/page/Widget/components/components/EditWidgetPlaceholder'
import useGetRichDescription from 'js/page/Widget/hooks/useGetRichDescription'
import useGetSetting from 'js/page/Widget/hooks/useGetSetting'

import TextEdit from './TextEdit'
import TextView from './TextView'

const Text = forwardRef(
    (
        { guid, entity, isEditing, editMode, onSave, rowBackgroundColor },
        ref,
    ) => {
        const { t } = useTranslation()

        const getSetting = useGetSetting(entity)
        const getRichDescription = useGetRichDescription(entity)

        const settings = {
            textSize: getSetting('textSize', 'normal'),
            richDescription: getRichDescription('richDescription'),
            backgroundColor: getSetting('backgroundColor', ''),
        }

        if (isEditing) {
            return (
                <TextEdit
                    ref={ref}
                    guid={guid}
                    settings={settings}
                    onSave={onSave}
                    rowBackgroundColor={rowBackgroundColor}
                />
            )
        }

        if (!settings.richDescription) {
            if (editMode)
                return <EditWidgetPlaceholder title={t('widget-text.title')} />
            return null
        }

        return (
            <TextView
                settings={settings}
                rowBackgroundColor={rowBackgroundColor}
            />
        )
    },
)

Text.displayName = 'Text'

export default Text
