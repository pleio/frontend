import React, { forwardRef, useImperativeHandle, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { media } from 'helpers'
import styled from 'styled-components'

import FormItem from 'js/components/Form/FormItem'
import Spacer from 'js/components/Spacer/Spacer'
import TabMenu from 'js/components/TabMenu'
import TabPage from 'js/components/TabPage'
import BgColorSetting from 'js/page/PageEditor/components/BgColorSetting'
import WidgetViewPortal from 'js/page/Widget/components/components//WidgetViewPortal'
import convertObjectToWidgetSettings from 'js/page/Widget/helpers/convertObjectToWidgetSettings'

import Text from './Text'

const Wrapper = styled.form`
    .TextWidgetEditor {
        ${media.mobileLandscapeDown`
            margin-left: -${(p) => p.theme.padding.horizontal.small};
            margin-right: -${(p) => p.theme.padding.horizontal.small};
        `};

        ${media.tabletUp`
            margin-left: -${(p) => p.theme.padding.horizontal.normal};
            margin-right: -${(p) => p.theme.padding.horizontal.normal};
        `};
    }
`

const TextEdit = forwardRef(
    ({ guid, rowBackgroundColor, settings, onSave }, ref) => {
        const { t } = useTranslation()

        const [tab, setTab] = useState('general')

        useImperativeHandle(ref, () => ({
            onSave() {
                submitForm()
            },
        }))

        const { richDescription, textSize, backgroundColor } = settings

        const defaultValues = {
            richDescription,
            textSize,
            backgroundColor,
        }

        const { control, handleSubmit, watch, setValue } = useForm({
            mode: 'onChange',
            defaultValues,
        })
        const watchValues = watch()

        const filterValues = ({
            richDescription,
            textSize,
            backgroundColor,
        }) => {
            return convertObjectToWidgetSettings({
                richDescription: {
                    richDescription,
                },
                textSize,
                backgroundColor,
            })
        }

        const onSubmit = () => {
            onSave(filterValues(watchValues))
        }
        const submitForm = handleSubmit(onSubmit)

        const editBackgroundColor = (val) => {
            setValue('backgroundColor', val)
        }

        return (
            <Wrapper as="form">
                <TabMenu
                    label={t('global.settings')}
                    options={[
                        {
                            onClick: () => setTab('general'),
                            label: t('global.general'),
                            isActive: tab === 'general',
                            id: 'tab-general',
                            ariaControls: 'tabpanel-general',
                        },
                        {
                            onClick: () => setTab('appearance'),
                            label: t('global.appearance'),
                            isActive: tab === 'appearance',
                            id: 'tab-appearance',
                            ariaControls: 'tabpanel-appearance',
                        },
                    ]}
                    showBorder
                    edgePadding
                    edgeMargin
                />

                <TabPage
                    id={`tabpanel-${tab}`}
                    ariaLabelledby={`tab-${tab}`}
                    visible
                >
                    {tab === 'general' && (
                        <FormItem
                            control={control}
                            type="rich"
                            name="richDescription"
                            id={`richDescription-${guid}`}
                            placeholder={t('form.start-typing')}
                            options={{
                                textFormat: true,
                                textStyle: true,
                                textLink: true,
                                textQuote: true,
                                textList: true,
                                insertMedia: true,
                                insertTable: true,
                                insertAccordion: true,
                                insertButton: true,
                                textAnchor: true,
                                textCSSClass: true,
                                textLanguage: true,
                            }}
                            borderStyle="none"
                            textSize={watch('textSize')}
                            className="TextWidgetEditor"
                        />
                    )}

                    {tab === 'appearance' && (
                        <Spacer spacing="small" style={{ paddingTop: '24px' }}>
                            <FormItem
                                control={control}
                                type="select"
                                name="textSize"
                                id={`textSize-${guid}`}
                                options={[
                                    {
                                        value: 'small',
                                        label: t('size.sm'),
                                    },
                                    {
                                        value: 'normal',
                                        label: t('size.md'),
                                    },
                                    {
                                        value: 'large',
                                        label: t('size.lg'),
                                    },
                                ]}
                                label={t('widget-text.text-size')}
                                helper={t('widget-text.text-size-helper')}
                            />

                            <BgColorSetting
                                name={`${guid}-background-color`}
                                title={t('widget-text.background-color')}
                                value={watch('backgroundColor')}
                                onChange={editBackgroundColor}
                            />
                        </Spacer>
                    )}
                </TabPage>
                <WidgetViewPortal
                    Component={Text}
                    settings={filterValues(watchValues)}
                    guid={guid}
                    rowBackgroundColor={rowBackgroundColor}
                />
            </Wrapper>
        )
    },
)

TextEdit.displayName = 'TextEdit'

export default TextEdit
