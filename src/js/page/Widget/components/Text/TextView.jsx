import React from 'react'
import { media } from 'helpers'
import { getReadableColor, textContrastIsAA } from 'helpers/getContrast'
import styled, { css } from 'styled-components'

import TiptapView from 'js/components/Tiptap/TiptapView'
import getTransparentColor from 'js/page/PageEditor/helpers/getTransparentColor'

const Wrapper = styled.div`
    display: block;
    position: relative;
    width: 100%;

    ${(p) =>
        !!p.$backgroundColor &&
        css`
            background-color: ${(p) => p.$backgroundColor};
            color: ${(p) => getReadableColor(p.$backgroundColor)};
            border-radius: ${(p) => p.theme.radius.normal};
            box-shadow: ${(p) => p.theme.card.shadow};

            ${media.mobileLandscapeDown`
                padding: ${(p) =>
                    `${p.theme.padding.vertical.small} ${p.theme.padding.horizontal.small}`};
            `};

            ${media.tabletUp`
                padding: ${(p) =>
                    `${p.theme.padding.vertical.normal} ${p.theme.padding.horizontal.normal}`};
            `};
        `};

    ${(p) =>
        (p.$backgroundColor === '#FFFFFF' || p.$backgroundColor === 'white') &&
        css`
            box-shadow: 0 0 0 1px ${(p) => p.theme.color.grey['30-alpha']};
        `}

    ${(p) => {
        const bgColor = p.$backgroundColor || p.$rowBackgroundColor
        if (bgColor && bgColor !== '#FFFFFF' && bgColor !== 'white') {
            return css`
                .ProseMirror {
                    .mark-link {
                        color: ${textContrastIsAA(
                            bgColor,
                            p.theme.color.primary.main,
                        )
                            ? p.theme.color.primary.main
                            : 'inherit'};
                    }

                    blockquote {
                        border-color: ${getTransparentColor(bgColor)};
                    }

                    figcaption {
                        color: ${getReadableColor(bgColor)};
                    }

                    table {
                        th {
                            background-color: transparent;
                        }

                        td,
                        th {
                            border-color: ${getTransparentColor(bgColor)};
                        }
                    }

                    .FileWrapper {
                        border-color: ${getTransparentColor(bgColor)};

                        .AttachmentName,
                        .AttachmentSize {
                            color: ${getReadableColor(bgColor)};
                        }
                    }

                    details,
                    details[open] summary {
                        border-color: ${getTransparentColor(bgColor)};
                    }
                }
            `
        }
    }};
`

const TextView = ({ settings, rowBackgroundColor }) => {
    const { richDescription, textSize, backgroundColor } = settings

    return (
        <Wrapper
            $backgroundColor={backgroundColor}
            $rowBackgroundColor={rowBackgroundColor}
        >
            <TiptapView content={richDescription} textSize={textSize} />
        </Wrapper>
    )
}

export default TextView
