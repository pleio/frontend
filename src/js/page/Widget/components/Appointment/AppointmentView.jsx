import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql, useLazyQuery, useMutation, useQuery } from '@apollo/client'
import { media } from 'helpers'
import showDate, { isSameDate, showTime } from 'helpers/date/showDate'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import Card, { CardContent } from 'js/components/Card/Card'
import DatePicker from 'js/components/DatePicker/DatePicker'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import { H3 } from 'js/components/Heading'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Setting from 'js/components/Setting'
import Spacer from 'js/components/Spacer/Spacer'
import Text from 'js/components/Text/Text'
import EditWidgetPlaceholder from 'js/page/Widget/components/components/EditWidgetPlaceholder'

import CalendarIcon from 'icons/calendar-day.svg'

const Wrapper = styled(Card)`
    display: flex;
    flex-direction: column;

    .AppointmentViewDescription {
        font-size: ${(p) => p.theme.font.size.normal};
        line-height: ${(p) => p.theme.font.lineHeight.normal};

        &:not(:first-child) {
            margin-top: 4px;
        }
    }

    .AppointmentViewDatePicker {
        ${media.mobileLandscapeDown`
            margin-left: -${(p) => p.theme.padding.horizontal.small};
            margin-right: -${(p) => p.theme.padding.horizontal.small};
        `}

        ${media.tabletUp`
            margin-left: -${(p) => p.theme.padding.horizontal.normal};
            margin-right: -${(p) => p.theme.padding.horizontal.normal};
        `}
    }

    .AppointmentViewTimeSlots {
        display: flex;
        justify-content: center;
        flex-wrap: wrap;
        margin: 12px -16px -4px;

        > * {
            margin: 4px;
        }
    }

    .AppointmentViewChosen {
        &:not(:first-child) {
            margin-top: 12px;

            .AppointmentViewChosenButton {
                padding-top: 6px;
                border-top: 1px solid ${(p) => p.theme.color.grey[30]};
            }
        }

        ${media.mobileLandscapeDown`
            margin-left: -${(p) => p.theme.padding.horizontal.small};
            margin-right: -${(p) => p.theme.padding.horizontal.small};
        `}

        ${media.tabletUp`
            margin-left: -${(p) => p.theme.padding.horizontal.normal};
            margin-right: -${(p) => p.theme.padding.horizontal.normal};
        `}
    }

    .AppointmentViewChosenButton {
        width: 100%;
        display: flex;
        align-items: center;
        padding: 0 16px 8px 0;
        border-bottom: 1px solid ${(p) => p.theme.color.grey[30]};

        svg {
            width: 48px;
        }

        &:hover .AppointmentViewChosenChange {
            text-decoration: underline;
        }
    }

    .AppointmentViewChosenType {
        font-size: ${(p) => p.theme.font.size.normal};
        line-height: ${(p) => p.theme.font.lineHeight.normal};
        font-weight: ${(p) => p.theme.font.weight.semibold};
    }

    .AppointmentViewChosenTime {
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
    }

    .AppointmentViewChosenChange {
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        color: ${(p) => p.theme.color.primary.main};
    }

    .AppointmentViewName {
        display: flex;

        > * {
            width: 50%;

            &:first-child {
                .TextFieldWrapper {
                    border-right: none;
                    border-top-right-radius: 0;
                    border-bottom-right-radius: 0;
                }
            }
            &:last-child {
                .TextFieldWrapper {
                    border-top-left-radius: 0;
                    border-bottom-left-radius: 0;
                }
            }
        }
    }
`

const AppointmentView = ({ guid, editMode, settings }) => {
    const { t } = useTranslation()

    const [selectedDay, setSelectedDay] = useState()
    const [times, setTimes] = useState()
    const [typeOptions, setTypeOptions] = useState()
    const [selectedTime, setSelectedTime] = useState()
    const [completed, setCompleted] = useState(false)
    const [error, setError] = useState()

    const { title, description, agendaId, appointmentTypeIds } = settings

    const {
        control,
        handleSubmit,
        getValues,
        setValue,
        watch,
        formState: { errors, isValid, isSubmitting },
    } = useForm({
        mode: 'onBlur',
    })

    const [mutate] = useMutation(gql`
        mutation ScheduleAppointment($input: ScheduleAppointmentInput!) {
            scheduleAppointment(input: $input) {
                success
            }
        }
    `)

    const submit = async ({
        appointmentTypeId,
        firstName,
        lastName,
        email,
        phone,
    }) => {
        setError()

        const { startDateTime, endDateTime } = selectedTime
        const input = {
            agendaId,
            appointmentTypeId,
            startDateTime,
            endDateTime,
            attendee: {
                firstName,
                lastName,
                email,
                phone,
            },
        }

        await mutate({
            variables: {
                input,
            },
        })
            .then(() => {
                setCompleted(true)
            })
            .catch((error) => {
                console.error(error)
                setError(error)
            })
    }

    const resetForm = () => {
        setSelectedDay()
        setSelectedTime()
        setValue('contactType')
        slotsRefetch()
        setCompleted(false)
    }

    const { loading, data } = useQuery(AppointmentDataQuery)

    const [
        getSlots,
        { loading: slotsLoading, data: slotsData, refetch: slotsRefetch },
    ] = useLazyQuery(AppointmentSlotsQuery)

    useEffect(() => {
        const appointmentTypeId = getValues('appointmentTypeId')
        if (appointmentTypeId) {
            getSlots({
                variables: {
                    agendaId,
                    appointmentTypeId,
                },
            })
        }
    }, [agendaId, watch('appointmentTypeId')])

    useEffect(() => {
        const slot = slotsData?.appointmentTimes?.find((slot) =>
            isSameDate(slot.day, selectedDay),
        )
        setTimes(slot?.times || null)
    }, [selectedDay])

    useEffect(() => {
        // Reset selected day
        setSelectedDay()
    }, [slotsData])

    useEffect(() => {
        const typeOptions = data?.appointmentData?.appointmentTypes
            ?.filter((type) => appointmentTypeIds.indexOf(type.id) !== -1)
            .map(({ id, name }) => {
                return { value: id, label: name }
            })
        if (typeOptions?.length > 0) {
            setTypeOptions(typeOptions)
            setValue('appointmentTypeId', typeOptions[0].value)
        }
    }, [data, appointmentTypeIds])

    if (loading) return null
    if (!typeOptions) {
        if (editMode)
            return (
                <EditWidgetPlaceholder title={t('widget-appointment.title')} />
            )
        return null
    }

    const selectedType = data?.appointmentData?.appointmentTypes?.find(
        (type) => type.id === getValues('appointmentTypeId'),
    )

    return (
        <Wrapper as="form" onSubmit={handleSubmit(submit)}>
            <CardContent>
                <Spacer spacing="small">
                    {title && <H3 as="h2">{title}</H3>}
                    {description && (
                        <p className="AppointmentViewDescription">
                            {description}
                        </p>
                    )}
                    {!selectedTime ? (
                        <>
                            {typeOptions.length > 1 && (
                                <FormItem
                                    control={control}
                                    type="select"
                                    name="appointmentTypeId"
                                    required
                                    id={`${guid}-appointmentTypeId`}
                                    label={t('widget-appointment.title')}
                                    options={typeOptions}
                                />
                            )}

                            {slotsLoading ? (
                                <LoadingSpinner />
                            ) : slotsData?.appointmentTimes?.length > 0 ? (
                                <DatePicker
                                    value={selectedDay}
                                    onDayClick={(val) => {
                                        const dateISO = val.toISOString()
                                        setSelectedDay(
                                            selectedDay !== dateISO
                                                ? dateISO
                                                : null,
                                        )
                                    }}
                                    disabledDays={(day) => {
                                        return slotsData?.appointmentTimes.every(
                                            (slot) =>
                                                !isSameDate(day, slot.day),
                                        )
                                    }}
                                    className="AppointmentViewDatePicker"
                                />
                            ) : (
                                <Text
                                    variant="grey"
                                    size="small"
                                    textAlign="center"
                                >
                                    {t('widget-appointment.no-available-times')}
                                </Text>
                            )}

                            {selectedDay && times?.length > 0 && (
                                <div className="AppointmentViewTimeSlots">
                                    {times.map(
                                        ({ startDateTime, endDateTime }) => (
                                            <Button
                                                size="small"
                                                key={`${startDateTime}-${endDateTime}`}
                                                type="button"
                                                onClick={() =>
                                                    setSelectedTime({
                                                        startDateTime,
                                                        endDateTime,
                                                    })
                                                }
                                                variant="tertiary"
                                            >
                                                {showTime(startDateTime)} -{' '}
                                                {showTime(endDateTime)}
                                            </Button>
                                        ),
                                    )}
                                </div>
                            )}
                        </>
                    ) : (
                        <>
                            {selectedTime && (
                                <div className="AppointmentViewChosen">
                                    <button
                                        type="button"
                                        className="AppointmentViewChosenButton"
                                        onClick={() => setSelectedTime()}
                                    >
                                        <CalendarIcon />
                                        <div>
                                            <div className="AppointmentViewChosenType">
                                                {selectedType?.name}
                                            </div>
                                            <div className="AppointmentViewChosenTime">
                                                {`${showDate(
                                                    selectedDay,
                                                    false,
                                                    true,
                                                )} ${t('global.at')} ${showTime(
                                                    selectedTime.startDateTime,
                                                )} - ${showTime(
                                                    selectedTime.endDateTime,
                                                )}`}
                                            </div>
                                            {!completed && (
                                                <div className="AppointmentViewChosenChange">
                                                    {t(
                                                        'widget-appointment.change-appointment',
                                                    )}
                                                </div>
                                            )}
                                        </div>
                                    </button>
                                </div>
                            )}

                            {completed ? (
                                <div
                                    style={{
                                        display: 'flex',
                                        flexDirection: 'column',
                                        alignItems: 'center',
                                    }}
                                >
                                    <div>
                                        {t(
                                            'widget-appointment.appointment-made',
                                        )}
                                    </div>
                                    <Button
                                        size="normal"
                                        type="button"
                                        variant="secondary"
                                        onClick={resetForm}
                                        style={{ marginTop: '12px' }}
                                    >
                                        {t(
                                            'widget-appointment.another-appointment',
                                        )}
                                    </Button>
                                </div>
                            ) : (
                                <>
                                    <Setting
                                        subtitle="Your details"
                                        inputWidth="full"
                                    >
                                        <div className="AppointmentViewName">
                                            <FormItem
                                                control={control}
                                                className="TextFieldWrapper"
                                                type="text"
                                                name="firstName"
                                                errors={errors}
                                                required
                                                id={`${guid}-firstName`}
                                                label={t(
                                                    'widget-appointment.first-name',
                                                )}
                                            />
                                            <FormItem
                                                control={control}
                                                className="TextFieldWrapper"
                                                type="text"
                                                name="lastName"
                                                errors={errors}
                                                required
                                                id={`${guid}-lastName`}
                                                label={t(
                                                    'widget-appointment.last-name',
                                                )}
                                            />
                                        </div>
                                    </Setting>

                                    <FormItem
                                        control={control}
                                        type="email"
                                        name="email"
                                        errors={errors}
                                        required
                                        id={`${guid}-email`}
                                        label={t('form.email-address')}
                                        helper={t(
                                            'widget-appointment.email-helper',
                                        )}
                                    />

                                    <FormItem
                                        control={control}
                                        type="tel"
                                        name="phone"
                                        errors={errors}
                                        required={!selectedType.hasVideocall}
                                        id={`${guid}-phone`}
                                        label={t(
                                            'widget-appointment.phone-number',
                                        )}
                                        helper={
                                            selectedType.hasVideocall
                                                ? t(
                                                      'widget-appointment.phone-number-helper',
                                                  )
                                                : null
                                        }
                                    />

                                    <Errors errors={error} />

                                    <Flexer mt>
                                        <Button
                                            type="submit"
                                            size="normal"
                                            variant="primary"
                                            disabled={!isValid}
                                            loading={isSubmitting}
                                        >
                                            {t(
                                                'widget-appointment.make-appointment',
                                            )}
                                        </Button>
                                    </Flexer>
                                </>
                            )}
                        </>
                    )}
                </Spacer>
            </CardContent>
        </Wrapper>
    )
}

const AppointmentSlotsQuery = gql`
    query AppointmentWidgetEdit(
        $agendaId: String!
        $appointmentTypeId: String!
    ) {
        appointmentTimes(
            agendaId: $agendaId
            appointmentTypeId: $appointmentTypeId
        ) {
            day
            times {
                startDateTime
                endDateTime
            }
        }
    }
`

const AppointmentDataQuery = gql`
    query AppointmentWidgetEdit {
        site {
            guid
            videocallEnabled
        }
        appointmentData {
            agendas {
                id
                name
            }
            appointmentTypes {
                id
                name
                hasVideocall
            }
        }
    }
`

export default AppointmentView
