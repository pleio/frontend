import React, { forwardRef } from 'react'

import useGetSetting from 'js/page/Widget/hooks/useGetSetting'

import AppointmentEdit from './AppointmentEdit'
import AppointmentView from './AppointmentView'

const Appointment = forwardRef(
    ({ guid, entity, isEditing, onSave, editMode }, ref) => {
        const getSetting = useGetSetting(entity)

        const settings = {
            title: getSetting('title'),
            description: getSetting('description'),
            agendaId: getSetting('agendaId'),
            appointmentTypeIds: getSetting('appointmentTypeIds')
                ? getSetting('appointmentTypeIds').split(',')
                : [],
        }

        if (isEditing) {
            return (
                <AppointmentEdit
                    ref={ref}
                    guid={guid}
                    settings={settings}
                    onSave={onSave}
                />
            )
        }

        return (
            <AppointmentView
                guid={guid}
                settings={settings}
                editMode={editMode}
            />
        )
    },
)

Appointment.displayName = 'Appointment'

export default Appointment
