import React, { forwardRef, useImperativeHandle } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'

import FormItem from 'js/components/Form/FormItem'
import Spacer from 'js/components/Spacer/Spacer'
import WidgetViewPortal from 'js/page/Widget/components/components//WidgetViewPortal'
import convertObjectToWidgetSettings from 'js/page/Widget/helpers/convertObjectToWidgetSettings'

import Appointment from './Appointment'

const AppointmentEdit = forwardRef(({ guid, settings, onSave }, ref) => {
    const { loading, data } = useQuery(Query)

    const { t } = useTranslation()

    useImperativeHandle(ref, () => ({
        onSave() {
            submitForm()
        },
    }))

    const { title, description, agendaId, appointmentTypeIds } = settings

    const defaultValues = {
        title,
        description,
        agendaId,
        appointmentTypeIds,
    }

    const {
        control,
        handleSubmit,
        watch,
        formState: { errors },
    } = useForm({
        defaultValues,
    })

    const watchValues = watch()

    const filterValues = ({
        title,
        description,
        agendaId,
        appointmentTypeIds,
    }) => {
        return convertObjectToWidgetSettings({
            title,
            description,
            agendaId,
            appointmentTypeIds: appointmentTypeIds.join(','),
        })
    }

    const onSubmit = () => {
        onSave(filterValues(watchValues))
    }

    const submitForm = handleSubmit(onSubmit)

    if (loading || !data) return null

    return (
        <>
            <Spacer spacing="small">
                <FormItem
                    control={control}
                    type="text"
                    name="title"
                    id={`${guid}-title`}
                    label={t('form.title')}
                />
                <FormItem
                    control={control}
                    type="textarea"
                    name="description"
                    id={`${guid}-description`}
                    label={t('form.description')}
                    size="small"
                />
                <FormItem
                    control={control}
                    type="select"
                    name="agendaId"
                    required
                    errors={errors}
                    id={`${guid}-agendaId`}
                    label={t('widget-appointment.agenda')}
                    options={data?.appointmentData?.agendas?.map(
                        ({ id, name }) => {
                            return { value: id, label: name }
                        },
                    )}
                />
                <FormItem
                    control={control}
                    type="select"
                    name="appointmentTypeIds"
                    required
                    errors={errors}
                    isMulti
                    id={`${guid}-appointmentTypeIds`}
                    label={t('widget-appointment.types')}
                    options={data?.appointmentData?.appointmentTypes?.map(
                        ({ id, name }) => {
                            return { value: id, label: name }
                        },
                    )}
                />
            </Spacer>

            <WidgetViewPortal
                Component={Appointment}
                settings={filterValues(watchValues)}
                guid={guid}
            />
        </>
    )
})

const Query = gql`
    query AppointmentWidgetEdit {
        site {
            guid
            videocallEnabled
        }
        appointmentData {
            agendas {
                id
                name
            }
            appointmentTypes {
                id
                name
            }
        }
    }
`

AppointmentEdit.displayName = 'AppointmentEdit'

export default AppointmentEdit
