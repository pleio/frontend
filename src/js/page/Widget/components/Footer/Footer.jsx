import React, { forwardRef } from 'react'
import PropTypes from 'prop-types'

import { hasJsonStructure } from 'js/lib/helpers'
import useGetSetting from 'js/page/Widget/hooks/useGetSetting'

import FooterEdit from './FooterEdit'
import FooterView from './FooterView'

const Footer = forwardRef(
    ({ guid, entity, isEditing, editMode, onSave }, ref) => {
        const getSetting = useGetSetting(entity)

        const settings = {
            links: hasJsonStructure(getSetting('links'))
                ? JSON.parse(getSetting('links'))
                : [],
        }

        if (isEditing) {
            return (
                <FooterEdit
                    ref={ref}
                    guid={guid}
                    settings={settings}
                    onSave={onSave}
                />
            )
        }

        return <FooterView settings={settings} editMode={editMode} />
    },
)

Footer.displayName = 'Footer'

Footer.propTypes = {
    entity: PropTypes.object.isRequired,
    isEditing: PropTypes.bool,
    onSave: PropTypes.func,
}

export default Footer
