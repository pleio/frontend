import React, { forwardRef, useImperativeHandle, useState } from 'react'
import { DragDropContext, Droppable } from 'react-beautiful-dnd'
import { useFieldArray, useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { media } from 'helpers'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import { H4 } from 'js/components/Heading'
import Modal from 'js/components/Modal/Modal'
import WidgetViewPortal from 'js/page/Widget/components/components//WidgetViewPortal'
import InsertFromContent from 'js/page/Widget/components/components/InsertFromContent/InsertFromContent'
import convertObjectToWidgetSettings from 'js/page/Widget/helpers/convertObjectToWidgetSettings'

import LinkListEditLink from './components/LinkListEditLink'
import Footer from './Footer'

const Wrapper = styled.div`
    .LinkListEditLinks {
        ${media.mobileLandscapeDown`
            margin-left: -${(p) => p.theme.padding.horizontal.small};
            margin-right: -${(p) => p.theme.padding.horizontal.small};
        `};

        ${media.tabletUp`
            margin-left: -${(p) => p.theme.padding.horizontal.normal};
            margin-right: -${(p) => p.theme.padding.horizontal.normal};
        `};
    }
`

const FooterEdit = forwardRef(({ guid, settings, onSave }, ref) => {
    const { t } = useTranslation()

    useImperativeHandle(ref, () => ({
        onSave() {
            submitForm()
        },
    }))

    const { links } = settings

    const { control, handleSubmit, watch } = useForm({
        defaultValues: {
            links,
        },
    })
    const watchValues = watch()

    const filterValues = ({ links }) => {
        return convertObjectToWidgetSettings({
            links: JSON.stringify(links),
        })
    }

    const onSubmit = () => {
        onSave(filterValues(watchValues))
    }
    const submitForm = handleSubmit(onSubmit)

    const [showSearchModal, setShowSearchModal] = useState(false)

    const { fields, prepend, remove, move } = useFieldArray({
        control,
        name: 'links',
    })

    const onDragEnd = (result) => {
        if (!result.destination) return
        move(result.source.index, result.destination.index)
    }

    const handleInsert = (entities) => {
        const links = entities.map((item) => ({
            url: item.url,
            label: item.title,
        }))
        prepend(links)
    }

    return (
        <Wrapper>
            <div>
                <Flexer mt wrap>
                    <Button
                        variant="primary"
                        onClick={() =>
                            prepend({
                                label: '',
                                url: '',
                            })
                        }
                    >
                        {t('action.add-item')}
                    </Button>
                    <span>{t('global.or')}</span>
                    <Button
                        variant="secondary"
                        onClick={() => setShowSearchModal(true)}
                    >
                        {t('action.insert-from-content')}..
                    </Button>
                    <Modal
                        size="normal"
                        isVisible={showSearchModal}
                        title={t('action.insert-from-content')}
                        onClose={() => setShowSearchModal(false)}
                        showCloseButton
                    >
                        <InsertFromContent
                            guid={guid}
                            onInsert={handleInsert}
                            onClose={() => setShowSearchModal(false)}
                        />
                    </Modal>
                </Flexer>

                <H4 style={{ margin: '12px 0' }}>
                    {t('global.result', {
                        count: watchValues.links?.length,
                    })}
                </H4>

                <DragDropContext onDragEnd={onDragEnd}>
                    <Droppable droppableId="linklist">
                        {(provided) => (
                            <ul
                                className="LinkListEditLinks"
                                {...provided.droppableProps}
                                ref={provided.innerRef}
                            >
                                {fields.map(({ id }, index) => (
                                    <LinkListEditLink
                                        key={id}
                                        id={id}
                                        index={index}
                                        control={control}
                                        watchLinks={watchValues.links}
                                        onRemove={() => remove(index)}
                                    />
                                ))}
                                {provided.placeholder}
                            </ul>
                        )}
                    </Droppable>
                </DragDropContext>
            </div>

            <WidgetViewPortal
                Component={Footer}
                settings={filterValues(watchValues)}
                guid={guid}
            />
        </Wrapper>
    )
})

FooterEdit.displayName = 'FooterEdit'

export default FooterEdit
