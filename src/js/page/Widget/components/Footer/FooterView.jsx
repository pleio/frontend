import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { getCleanUrl, isExternalUrl } from 'helpers'
import styled from 'styled-components'

import withGlobalState from 'js/lib/withGlobalState'
import EditWidgetPlaceholder from 'js/page/Widget/components/components/EditWidgetPlaceholder'

const Wrapper = styled.div`
    margin: 0 -8px;

    .FooterLink {
        display: inline-flex;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        margin: 0 8px;
        padding: 4px 0;

        &:hover {
            text-decoration: underline;
        }
    }
`

const FooterView = ({ settings, editMode }) => {
    const { links } = settings

    const { t } = useTranslation()

    if (links.length === 0) {
        if (!editMode) return null

        return <EditWidgetPlaceholder title={t('widget-footer.title')} />
    }

    return (
        <Wrapper>
            {links.map((link, index) => {
                const url = getCleanUrl(link.url)
                const isExternal = isExternalUrl(url)
                const LinkElement = isExternal ? 'a' : Link

                return (
                    <LinkElement
                        className="FooterLink"
                        key={'item' + index}
                        href={isExternal ? url : null}
                        to={!isExternal ? url : null}
                        target={isExternal ? '_blank' : null}
                        rel={isExternal ? 'noopener noreferrer' : null}
                    >
                        {link.label || link.url}
                    </LinkElement>
                )
            })}
        </Wrapper>
    )
}

export default withGlobalState(FooterView)
