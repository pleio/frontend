import React, { useState } from 'react'
import { Draggable } from 'react-beautiful-dnd'
import { useTranslation } from 'react-i18next'
import styled, { css } from 'styled-components'

import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import IconButton from 'js/components/IconButton/IconButton'

import RemoveIcon from 'icons/delete-small.svg'
import EditIcon from 'icons/edit-small.svg'
import MoveIcon from 'icons/move-vertical.svg'

const Wrapper = styled.li`
    overflow: hidden;
    font-size: ${(p) => p.theme.font.size.small};
    line-height: ${(p) => p.theme.font.lineHeight.small};
    color: ${(p) => p.theme.color.text.black};
    background-color: white;
    border-bottom: 1px solid ${(p) => p.theme.color.grey[20]};

    &:last-child {
        border-bottom-color: transparent;
    }

    ${(p) =>
        p.$isDragging &&
        css`
            box-shadow: ${p.theme.shadow.soft.center};
            border-bottom-color: transparent;
        `};

    .LinkListEditLinkLabel {
        flex-grow: 1;

        .TextFieldInput {
            padding-left: 0;
        }
    }

    .LinkListEditLinkProperties {
        padding: 8px 20px 16px;
    }
`

const LinkListEditLink = ({ id, index, control, watchLinks, onRemove }) => {
    const { t } = useTranslation()

    const { label } = watchLinks?.[index] ?? {}

    const [isEditing, setIsEditing] = useState(false)
    const toggleIsEditing = () => setIsEditing(!isEditing)

    const buttonProps = {
        type: 'button',
        size: 'large',
        variant: 'secondary',
        hoverSize: 'normal',
        radiusStyle: 'rounded',
    }

    return (
        <Draggable draggableId={id} index={index}>
            {(provided, snapshot) => (
                <Wrapper
                    ref={provided.innerRef}
                    $isDragging={snapshot.isDragging}
                    {...provided.draggableProps}
                >
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        <IconButton
                            as="div"
                            size="large"
                            variant="secondary"
                            hoverSize="normal"
                            aria-label={t('aria.move-link', {
                                label,
                            })}
                            {...provided.dragHandleProps}
                        >
                            <MoveIcon />
                        </IconButton>

                        <FormItem
                            control={control}
                            type="text"
                            name={`links.${index}.label`}
                            placeholder={t('form.title')}
                            borderStyle="none"
                            className="LinkListEditLinkLabel"
                        />

                        <Flexer gutter="tiny" divider="normal">
                            <IconButton
                                {...buttonProps}
                                onClick={toggleIsEditing}
                                aria-pressed={isEditing}
                                aria-label={t('aria.edit-link', {
                                    label,
                                })}
                                tooltip={t('action.edit')}
                            >
                                <EditIcon />
                            </IconButton>
                            <IconButton
                                {...buttonProps}
                                onClick={onRemove}
                                aria-label={t('aria.remove-link', {
                                    label,
                                })}
                                tooltip={t('action.delete')}
                            >
                                <RemoveIcon />
                            </IconButton>
                        </Flexer>
                    </div>

                    {isEditing && (
                        <div className="LinkListEditLinkProperties">
                            <FormItem
                                control={control}
                                type="text"
                                name={`links.${index}.url`}
                                label={t('form.link')}
                            />
                        </div>
                    )}
                </Wrapper>
            )}
        </Draggable>
    )
}

export default LinkListEditLink
