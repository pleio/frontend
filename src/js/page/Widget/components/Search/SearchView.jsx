import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import { format } from 'date-fns'
import { getSearchLink } from 'helpers'
import styled from 'styled-components'

import AnimatePresence from 'js/components/AnimatePresence/AnimatePresence'
import Card from 'js/components/Card/Card'
import HideVisually from 'js/components/HideVisually/HideVisually'
import SearchBar from 'js/components/SearchBar/SearchBar'
import SearchFilters from 'js/search/components/SearchFilters'

const Wrapper = styled(Card)`
    flex-grow: 0 !important;
    box-shadow: 0 0 0 1px ${(p) => p.theme.color.grey['40-alpha']} !important; // Overrides styling in Section for the sake of accessibility

    .SearchWidgetFilters {
        padding: 16px 20px 20px;
        border-top: 1px solid ${(p) => p.theme.color.grey[30]};
    }
`

const SearchView = ({ guid, settings, site, group, typeOptions }) => {
    const navigate = useNavigate()

    const { t } = useTranslation()

    const { tagCategories } = site

    const {
        expandFilters,
        typeFilter,
        tagFilter,
        dateFrom,
        dateTo,
        relatedSites,
        showTypeFilter,
        showTagFilter,
        showTagCategories,
        showDateFilter,
        showRelatedSites,
    } = settings

    const showAnyTagFilter = showTagFilter || showTagCategories.length > 0
    const showAnyFilter =
        showRelatedSites || showTypeFilter || showAnyTagFilter || showDateFilter

    const [q, setQ] = useState('')
    const handleChangeQ = (evt) => setQ(evt.target.value)

    const [showFilters, setShowFilters] = useState(expandFilters)
    const handleToggleFilters = () => setShowFilters(!showFilters)

    useEffect(() => {
        setShowFilters(expandFilters)
    }, [expandFilters])

    const currentParams = {
        subtype: typeFilter || null,
        tags: tagFilter?.length ? JSON.stringify(tagFilter) : null,
        datefrom: dateFrom ? format(dateFrom, 'yyyy-MM-dd') : null,
        dateto: dateTo ? format(dateTo, 'yyyy-MM-dd') : null,
        relatedsites: relatedSites || null,
    }

    const onSubmit = (evt) => {
        evt.preventDefault()

        const searchLink = getSearchLink({
            q: q.trim(),
            ...currentParams,
        })

        if (group) {
            navigate(`${group.url}${searchLink}`)
        } else {
            navigate(searchLink)
        }
    }

    const tagCategoryFilters = showTagFilter
        ? tagCategories
        : tagCategories.filter(
              (category) => showTagCategories?.indexOf(category.name) > -1,
          )

    return (
        <Wrapper
            style={{
                display: 'flex',
                flexDirection: 'column',
            }}
        >
            <HideVisually>
                <h2>{t('widget-search.title')}</h2>
            </HideVisually>

            <form method="GET" onSubmit={onSubmit}>
                <SearchBar
                    name={`${guid}-search-bar`}
                    value={q}
                    onChange={handleChangeQ}
                    label={t('global.search')}
                    showFilters={showFilters}
                    onToggleFilters={showAnyFilter ? handleToggleFilters : null}
                    borderStyle="none"
                    size="large"
                />
            </form>

            {showAnyFilter && (
                <AnimatePresence visible={showFilters}>
                    <SearchFilters
                        archived={false}
                        tags={null}
                        name={guid}
                        getSearchLink={getSearchLink}
                        dateFrom={dateFrom}
                        dateTo={dateTo}
                        currentParams={{
                            ...currentParams,
                            q: q.trim(),
                        }}
                        searchPublishedFilterEnabled={
                            site.searchPublishedFilterEnabled
                        }
                        tagFilter={tagFilter}
                        tagCategories={tagCategoryFilters}
                        showDateFilter={showDateFilter}
                        typeFilter={typeFilter}
                        showTypeFilter={showTypeFilter}
                        typeOptions={typeOptions}
                        relatedSites={relatedSites}
                        showRelatedSites={
                            showRelatedSites && site.searchRelatedSitesEnabled
                        }
                        className="SearchWidgetFilters"
                    />
                </AnimatePresence>
            )}
        </Wrapper>
    )
}

export default SearchView
