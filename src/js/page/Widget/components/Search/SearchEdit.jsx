import React, { forwardRef, useImperativeHandle, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { formatISO } from 'date-fns'

import FilterWrapper from 'js/components/FilterWrapper'
import FormItem from 'js/components/Form/FormItem'
import { H4 } from 'js/components/Heading'
import Select from 'js/components/Select/Select'
import Setting from 'js/components/Setting'
import Spacer from 'js/components/Spacer/Spacer'
import WidgetViewPortal from 'js/page/Widget/components/components//WidgetViewPortal'
import setTagFilterHelper from 'js/page/Widget/components/Feed/setTagFilter'
import convertObjectToWidgetSettings from 'js/page/Widget/helpers/convertObjectToWidgetSettings'

import Search from './Search'

const SearchEdit = forwardRef(
    ({ guid, site, group, typeOptions, settings, onSave }, ref) => {
        const { t } = useTranslation()

        useImperativeHandle(ref, () => ({
            onSave() {
                submitForm()
            },
        }))

        const [tagFilter, setTagFilter] = useState(settings.tagFilter)
        const { searchRelatedSitesEnabled, tagCategories, searchRelatedSites } =
            site

        const { control, handleSubmit, watch } = useForm({
            defaultValues: settings,
        })
        const watchValues = watch()

        const filterValues = ({
            expandFilters,
            relatedSites,
            typeFilter,
            dateFrom,
            dateTo,
            showRelatedSites,
            showTypeFilter,
            showDateFilter,
            showTagFilter,
            showTagCategories,
        }) => {
            return convertObjectToWidgetSettings({
                expandFilters: expandFilters.toString(),
                relatedSites: relatedSites.toString(),
                typeFilter,
                tagFilter: JSON.stringify(tagFilter),
                dateFrom,
                dateTo,
                showRelatedSites: showRelatedSites.toString(),
                showTypeFilter: showTypeFilter.toString(),
                showTagFilter: showTagFilter.toString(),
                showTagCategories: JSON.stringify(showTagCategories),
                showDateFilter: showDateFilter.toString(),
            })
        }

        const onSubmit = () => {
            onSave(filterValues(watchValues))
        }
        const submitForm = handleSubmit(onSubmit)

        const today = formatISO(new Date())

        return (
            <>
                <Spacer spacing="small">
                    <Setting
                        subtitle={t('widget-search.default-filtering')}
                        helper={t('widget-feed.filters-default-helper')}
                        inputWidth="full"
                    >
                        <Spacer spacing="small">
                            <FilterWrapper>
                                <FormItem
                                    control={control}
                                    type="select"
                                    label={t('global.type')}
                                    name="typeFilter"
                                    id={`${guid}-typeFilter`}
                                    placeholder={t('filters.filter-by')}
                                    options={typeOptions}
                                    isClearable
                                />

                                {tagCategories.map((category, index) => {
                                    const options = category.values.map(
                                        (tag) => ({
                                            value: tag,
                                            label: tag,
                                        }),
                                    )

                                    const categoryName = category.name

                                    const value =
                                        tagFilter.find(
                                            (cat) => cat.name === categoryName,
                                        )?.values || []

                                    const handleChange = (tags) => {
                                        setTagFilterHelper(
                                            setTagFilter,
                                            categoryName,
                                            tags,
                                        )
                                    }

                                    const id = `${guid}-tag-category-${index}`

                                    return (
                                        <Select
                                            key={id}
                                            name={id}
                                            label={category.name}
                                            placeholder={t('filters.filter-by')}
                                            options={options}
                                            value={value}
                                            onChange={handleChange}
                                            isMulti
                                        />
                                    )
                                })}

                                <FormItem
                                    type="date"
                                    control={control}
                                    name="dateFrom"
                                    toDate={watch('dateTo') || today}
                                    label={t('search.date-from')}
                                    isClearable
                                />

                                <FormItem
                                    type="date"
                                    control={control}
                                    name="dateTo"
                                    fromDate={watch('dateFrom')}
                                    toDate={today}
                                    label={t('search.date-to')}
                                    isClearable
                                />
                            </FilterWrapper>
                        </Spacer>
                    </Setting>

                    <Setting
                        subtitle={t('widget-search.show-filters')}
                        helper={t('widget-feed.filters-show-helper')}
                        inputWidth="full"
                    >
                        {searchRelatedSitesEnabled &&
                            searchRelatedSites.length > 0 && (
                                <FormItem
                                    control={control}
                                    type="switch"
                                    label={`${t(
                                        'search.related-sites',
                                    )}: ${searchRelatedSites
                                        .map((site) => site.name)
                                        .join(', ')}`}
                                    name="showRelatedSites"
                                    id={`${guid}-showRelatedSites`}
                                    size="small"
                                />
                            )}
                        <FormItem
                            control={control}
                            type="switch"
                            name="showTypeFilter"
                            id={`${guid}-showTypeFilter`}
                            label={t('global.type')}
                            size="small"
                        />

                        {tagCategories.length > 0 && (
                            <>
                                <FormItem
                                    control={control}
                                    type="switch"
                                    name="showTagFilter"
                                    id={`${guid}-showTagFilter`}
                                    label={t('widget-feed.all-tag-categories')}
                                    size="small"
                                />

                                <FormItem
                                    type="checkboxes"
                                    options={tagCategories.map((el) => ({
                                        label: el.name,
                                        value: el.name,
                                        disabled: watch('showTagFilter'),
                                        checked: watch('showTagFilter'),
                                    }))}
                                    control={control}
                                    name="showTagCategories"
                                    id={`${guid}-showTagCategories`}
                                    size="small"
                                    style={{
                                        marginLeft: '30px',
                                    }}
                                />
                            </>
                        )}

                        <FormItem
                            control={control}
                            type="switch"
                            name="showDateFilter"
                            id={`${guid}-showDateFilter`}
                            label={t('global.date')}
                            size="small"
                        />
                    </Setting>

                    <div>
                        <H4>{t('widget-search.expand-filters')}</H4>
                        <FormItem
                            control={control}
                            type="checkbox"
                            name="expandFilters"
                            id={`${guid}-expandFilters`}
                            label={t('widget-search.expand-filters-helper')}
                            size="small"
                        />
                    </div>
                </Spacer>

                <WidgetViewPortal
                    Component={Search}
                    settings={filterValues(watchValues)}
                    guid={guid}
                    site={site}
                    group={group}
                />
            </>
        )
    },
)

SearchEdit.displayName = 'SearchEdit'

export default SearchEdit
