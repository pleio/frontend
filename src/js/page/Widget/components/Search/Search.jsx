import React, { forwardRef } from 'react'

import normalizeContentTypes from 'js/activity/utils/normalizeContentTypes'
import useGetSetting from 'js/page/Widget/hooks/useGetSetting'

import SearchEdit from './SearchEdit'
import SearchView from './SearchView'

const Search = forwardRef(
    ({ guid, site, entity, group, isEditing, onSave }, ref) => {
        const getSetting = useGetSetting(entity)

        const settings = {
            relatedSites: getSetting('relatedSites') === 'true',
            typeFilter: getSetting('typeFilter'),
            tagFilter: getSetting('tagFilter')
                ? JSON.parse(getSetting('tagFilter'))
                : [],
            dateFrom: getSetting('dateFrom'),
            dateTo: getSetting('dateTo'),
            showRelatedSites: getSetting('showRelatedSites') === 'true',
            showTypeFilter: getSetting('showTypeFilter') === 'true',
            showTagFilter: getSetting('showTagFilter') === 'true', // Show all tag category filters
            showTagCategories: getSetting('showTagCategories') // Show specific tag category filters
                ? JSON.parse(getSetting('showTagCategories'))
                : [],
            showDateFilter: getSetting('showDateFilter') === 'true',
            expandFilters: getSetting('expandFilters') === 'true',
        }

        const typeOptions = normalizeContentTypes(
            site.searchFilter.contentTypes,
            ['group', 'magazine_issue', 'page'],
        )

        if (isEditing) {
            return (
                <SearchEdit
                    ref={ref}
                    guid={guid}
                    site={site}
                    group={group}
                    typeOptions={typeOptions}
                    settings={settings}
                    onSave={onSave}
                />
            )
        }

        return (
            <SearchView
                guid={guid}
                settings={settings}
                site={site}
                group={group}
                typeOptions={typeOptions}
            />
        )
    },
)

Search.displayName = 'Search'

export default Search
