import React from 'react'
import { useTranslation } from 'react-i18next'

import AnimatePresence from 'js/components/AnimatePresence/AnimatePresence'
import CustomTagsField from 'js/components/CustomTagsField'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import { H4 } from 'js/components/Heading'
import Spacer from 'js/components/Spacer/Spacer'
import TagCategoriesField from 'js/components/TagCategories/TagCategoriesField'
import Text from 'js/components/Text/Text'
import useSortingOptions from 'js/page/Widget/hooks/useSortingOptions'

const Content = ({
    guid,
    site,
    types,
    groupIsChecked,
    categoryTagsValue,
    setCategoryTagsValue,
    customTags,
    handleAddTags,
    handleRemoveTags,
    control,
}) => {
    const { t } = useTranslation()

    const { customTagsAllowed, tagCategories } = site

    const sortingOptions = useSortingOptions()

    return (
        <Spacer spacing="small">
            <FormItem
                control={control}
                type="checkboxes"
                name="subtypes"
                id={`subtypes-${guid}`}
                size="small"
                options={types.map(({ value, label }) => ({
                    value,
                    label,
                    disabled: value !== 'group' && groupIsChecked,
                }))}
                title={t('widget-objects.filter-type')}
                helper={
                    groupIsChecked && t('widget-objects.filter-type-helper')
                }
                gridTemplateColumns="1fr 1fr"
            />

            <TagCategoriesField
                name="categoryTags"
                label={t('widget-objects.filter-tag-categories')}
                tagCategories={tagCategories}
                value={categoryTagsValue}
                setTags={setCategoryTagsValue}
            />

            {customTagsAllowed && (
                <CustomTagsField
                    name="customTags"
                    value={customTags}
                    label={t('global.custom-tags')}
                    onAddTag={handleAddTags}
                    onRemoveTag={handleRemoveTags}
                />
            )}

            <AnimatePresence visible={!groupIsChecked}>
                <Spacer spacing="small">
                    <FormItem
                        control={control}
                        type="select"
                        name="sortBy"
                        id={`sortBy-${guid}`}
                        title={t('filters.sorting')}
                        options={sortingOptions}
                    />

                    <div>
                        <Flexer justifyContent="flex-start" gutter="small">
                            <FormItem
                                control={control}
                                type="switch"
                                size="small"
                                name="isFeatured"
                                id={`isFeatured-${guid}`}
                            />
                            <H4 as="label" htmlFor={`isFeatured-${guid}`}>
                                {t('form.featured')}
                            </H4>
                        </Flexer>
                        <Text size="small" variant="grey">
                            {t('widget-objects.featured-helper')}
                        </Text>
                    </div>
                </Spacer>
            </AnimatePresence>
        </Spacer>
    )
}

export default Content
