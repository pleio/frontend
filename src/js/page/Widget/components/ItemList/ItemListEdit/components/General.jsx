import React from 'react'
import { useTranslation } from 'react-i18next'

import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import { Col, Row } from 'js/components/Grid/Grid'
import Setting from 'js/components/Setting'
import Spacer from 'js/components/Spacer/Spacer'
import Switch from 'js/components/Switch/Switch'

const General = ({
    guid,
    control,
    watchValues,
    setValue,
    templateOptions,
    itemWidthMax,
}) => {
    const { t } = useTranslation()

    const watchLayout = watchValues.layout
    const canSetLimit = ['list', 'feed'].includes(watchLayout)
    const appearanceIsCard = watchValues.appearance === 'card'
    const layoutIsFeed = watchLayout === 'feed'
    const layoutIsList = watchLayout === 'list'
    const showExcerpt = watchValues.template?.includes('excerpt')

    return (
        <Spacer spacing="small">
            <FormItem
                control={control}
                type="text"
                name="title"
                id={`title-${guid}`}
                label={t('form.title')}
                helper={t('form.title-helper-empty')}
            />

            <Row>
                <Col mobileUp={1 / 2}>
                    <FormItem
                        type="radio"
                        control={control}
                        name="appearance"
                        size="small"
                        title={t('widget.appearance')}
                        options={[
                            {
                                value: 'list',
                                label: t('widget.layout-list'),
                            },
                            {
                                value: 'card',
                                label: t('widget.layout-card'),
                            },
                        ]}
                    />
                </Col>
                {appearanceIsCard && (
                    <Col mobileUp={1 / 2}>
                        <FormItem
                            type="radio"
                            control={control}
                            name="layout"
                            size="small"
                            title={t('widget.layout')}
                            options={[
                                {
                                    value: 'feed',
                                    label: t('widget.layout-feed'),
                                },
                                {
                                    value: 'carousel',
                                    label: t('widget.layout-carousel'),
                                },
                            ]}
                        />
                    </Col>
                )}
            </Row>

            {templateOptions?.length > 0 && (
                <FormItem
                    control={control}
                    type="checkboxes"
                    name="template"
                    id={`template-${guid}`}
                    size="small"
                    options={templateOptions}
                    title={t('widget.template')}
                    gridTemplateColumns="1fr 1fr"
                />
            )}

            {layoutIsFeed && (
                <Setting
                    subtitle={t('widget-objects.item-width')}
                    helper={t('widget-objects.item-width-helper')}
                    htmlFor={`itemWidth-${guid}`}
                    inputWidth="small"
                >
                    <FormItem
                        control={control}
                        type="number"
                        name="itemWidth"
                        id={`itemWidth-${guid}`}
                        min={1}
                        max={itemWidthMax}
                    />
                </Setting>
            )}

            <Setting
                subtitle={t('widget.nr-of-items')}
                helper={
                    layoutIsFeed
                        ? t('widget.nr-of-items-helper')
                        : layoutIsList
                          ? t('widget.nr-of-items-list-helper')
                          : null
                }
                htmlFor={`numberOfItems-${guid}`}
                inputWidth="small"
            >
                <FormItem
                    control={control}
                    type="number"
                    name="numberOfItems"
                    id={`numberOfItems-${guid}`}
                    min={1}
                    max={10}
                />
            </Setting>

            {appearanceIsCard && showExcerpt && (
                <Setting
                    subtitle={
                        <Flexer justifyContent="flex-start" gutter="small">
                            <FormItem
                                control={control}
                                type="switch"
                                size="small"
                                name="enableExcerptMaxLines"
                                id={`enableExcerptMaxLines-${guid}`}
                            />
                            <span>{t('widget-objects.excerpt-max-lines')}</span>
                        </Flexer>
                    }
                    helper={t('widget-objects.excerpt-max-lines-helper')}
                    htmlFor={`enableExcerptMaxLines-${guid}`}
                    inputWidth="small"
                >
                    <FormItem
                        control={control}
                        type="number"
                        name="excerptMaxLines"
                        id={`excerptMaxLines-${guid}`}
                        min={1}
                        max={4}
                        disabled={!watchValues.enableExcerptMaxLines}
                    />
                </Setting>
            )}

            {canSetLimit && (
                <Setting
                    subtitle={
                        <Flexer justifyContent="flex-start" gutter="small">
                            <Switch
                                control={control}
                                size="small"
                                name="allowDisableLimit"
                                id={`allowDisableLimit-${guid}`}
                                checked={!watchValues.allowDisableLimit}
                                onChange={(evt) =>
                                    setValue(
                                        'allowDisableLimit',
                                        !evt.target.checked,
                                    )
                                }
                            />
                            <span>{t('form.limit')}</span>
                        </Flexer>
                    }
                    helper={t('widget-objects.limit-helper')}
                    htmlFor={`allowDisableLimit-${guid}`}
                    inputWidth="small"
                >
                    <FormItem
                        control={control}
                        type="number"
                        name="limit"
                        id={`limit-${guid}`}
                        min={watchValues.numberOfItems}
                        disabled={watchValues.allowDisableLimit}
                    />
                </Setting>
            )}
        </Spacer>
    )
}

export default General
