import React, {
    forwardRef,
    useEffect,
    useImperativeHandle,
    useState,
} from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { sanitizeTagCategories, sortByProperty } from 'helpers'

import Spacer from 'js/components/Spacer/Spacer'
import TabMenu from 'js/components/TabMenu'
import TabPage from 'js/components/TabPage'
import WidgetViewPortal from 'js/page/Widget/components/components//WidgetViewPortal'
import convertObjectToWidgetSettings from 'js/page/Widget/helpers/convertObjectToWidgetSettings'
import useCustomTags from 'js/page/Widget/hooks/useCustomTags'

import ItemList from '../ItemList'

import Content from './components/Content'
import General from './components/General'

const ItemListEdit = forwardRef(
    (
        { guid, site, group, settings, onSave, itemWidthMax, columnWidth },
        ref,
    ) => {
        const { t } = useTranslation()

        useImperativeHandle(ref, () => ({
            onSave(isPreview) {
                submitForm(isPreview)
            },
        }))

        const [tab, setTab] = useState('general')

        const { tagCategories, entityFilter } = site

        const {
            title,
            layout,
            template,
            categoryTags,
            tags,
            itemWidth,
            numberOfItems,
            enableExcerptMaxLines,
            excerptMaxLines,
            limit,
            allowDisableLimit,
            groupTypes,
            subtypes,
            showDate,
            sortBy,
            isFeatured,
        } = settings

        const [categoryTagsValue, setCategoryTagsValue] = useState(categoryTags)

        const {
            customTags,
            handleAddTags,
            handleRemoveTags,
            getParsedCustomTags,
        } = useCustomTags(tags)

        const onSubmit = () => {
            onSave(filterValues(watchValues))
        }

        const defaultValues = {
            title,
            appearance: layout === 'list' ? 'list' : 'card',
            layout,
            template,
            subtypes,
            itemWidth,
            numberOfItems,
            enableExcerptMaxLines,
            excerptMaxLines,
            allowDisableLimit,
            limit,
            showDate,
            sortBy,
            isFeatured,
        }

        const { control, handleSubmit, setValue, watch } = useForm({
            defaultValues,
        })

        const watchValues = watch()

        // Update layout based on appearance ('appearance' setting is only visual, 'layout' is the actual setting that is saved)
        const watchAppearance = watchValues.appearance
        const watchLayout = watchValues.layout
        useEffect(() => {
            if (watchAppearance === 'list' && watchLayout !== 'list') {
                setValue('layout', 'list')
            }
            if (watchAppearance === 'card' && watchLayout === 'list') {
                setValue('layout', 'feed')
            }
        }, [watchAppearance, watchLayout, setValue])

        // Limit should not be less than numberOfItems
        const watchNumberOfItems = watchValues.numberOfItems
        const watchLimit = watchValues.limit
        useEffect(() => {
            if (watchNumberOfItems > watchLimit) {
                setValue('limit', watchNumberOfItems)
            }
        }, [watchNumberOfItems, watchLimit, setValue])

        const submitForm = (isPreview) => {
            handleSubmit((data) => onSubmit(data, isPreview))()
        }

        const groupIsChecked = watchValues.subtypes.includes('group')
        const appearanceIsCard = watchValues.appearance === 'card'
        const templateOptions = groupIsChecked
            ? [
                  ...(appearanceIsCard
                      ? [
                            {
                                value: 'excerpt',
                                label: t('widget.template-excerpt'),
                            },
                        ]
                      : []),
              ]
            : [
                  ...(appearanceIsCard
                      ? [
                            {
                                value: 'coverImage',
                                label: t('form.cover'),
                            },
                            {
                                value: 'author',
                                label: t('form.author'),
                            },
                        ]
                      : []),
                  {
                      value: 'datePublished',
                      label: t('widget.template-date-published'),
                  },
                  ...(appearanceIsCard
                      ? [
                            {
                                value: 'excerpt',
                                label: t('widget.template-excerpt'),
                            },
                            {
                                value: 'likes',
                                label: t('widget.template-likes'),
                            },
                            {
                                value: 'comments',
                                label: t('widget.template-comments'),
                            },
                        ]
                      : []),
              ]

        const types =
            entityFilter?.contentTypes.map(({ key, value }) => {
                return { value: key, label: value }
            }) || []

        if (groupTypes) {
            // Always show files in group - backwards compatible with old group plugin setting with active "files"
            if (!groupTypes.includes('files')) {
                groupTypes.push('files')
            }
        }

        // 'group' is not part of `entityFilter` on the server and is therefore 'manually' added on the frontend
        types.push({
            value: 'group',
            label: t('entity-group.content-name'),
        })

        types.sort(sortByProperty('label'))

        const filterValues = ({
            title,
            subtypes,
            layout,
            template,
            itemWidth,
            numberOfItems,
            enableExcerptMaxLines,
            excerptMaxLines,
            limit,
            allowDisableLimit,
            showDate,
            sortBy,
            isFeatured,
        }) => {
            const types = groupIsChecked ? ['group'] : subtypes

            // Prevent saving more template options than the layout allows (would result in bigger query than needed)
            const filteredTemplate = template.filter((t) =>
                templateOptions.find((o) => o.value === t),
            )

            return convertObjectToWidgetSettings({
                title,
                layout,
                template: filteredTemplate.join(','),
                subtypes: types.join(','),
                categoryTags: JSON.stringify(
                    sanitizeTagCategories(tagCategories, categoryTagsValue),
                ),
                tags: getParsedCustomTags(),
                itemWidth: itemWidth.toString(),
                numberOfItems: numberOfItems.toString(),
                enableExcerptMaxLines: enableExcerptMaxLines.toString(),
                excerptMaxLines: excerptMaxLines.toString(),
                limit: limit.toString(),
                allowDisableLimit: allowDisableLimit.toString(),
                showDate: showDate.toString(),
                sortBy,
                isFeatured: isFeatured.toString(),
            })
        }

        return (
            <>
                <Spacer spacing="small">
                    <TabMenu
                        label={t('global.settings')}
                        options={[
                            {
                                onClick: () => setTab('general'),
                                label: t('global.general'),
                                isActive: tab === 'general',
                                id: 'tab-general',
                                ariaControls: 'tabpanel-general',
                            },
                            {
                                onClick: () => setTab('content'),
                                label: t('global.content'),
                                isActive: tab === 'content',
                                id: 'tab-content',
                                ariaControls: 'tabpanel-content',
                            },
                        ]}
                        edgePadding
                        edgeMargin
                        showBorder
                    />

                    <TabPage
                        id={`tabpanel-${tab}`}
                        ariaLabelledby={`tab-${tab}`}
                        visible
                    >
                        {tab === 'general' && (
                            <General
                                guid={guid}
                                groupIsChecked={groupIsChecked}
                                control={control}
                                watchValues={watchValues}
                                setValue={setValue}
                                templateOptions={templateOptions}
                                itemWidthMax={itemWidthMax}
                            />
                        )}

                        {tab === 'content' && (
                            <Content
                                guid={guid}
                                control={control}
                                site={site}
                                types={types}
                                groupIsChecked={groupIsChecked}
                                categoryTagsValue={categoryTagsValue}
                                setCategoryTagsValue={setCategoryTagsValue}
                                customTags={customTags}
                                handleAddTags={handleAddTags}
                                handleRemoveTags={handleRemoveTags}
                            />
                        )}
                    </TabPage>
                </Spacer>

                <WidgetViewPortal
                    Component={ItemList}
                    settings={filterValues(watchValues)}
                    guid={guid}
                    site={site}
                    group={group}
                    columnWidth={columnWidth}
                />
            </>
        )
    },
)

ItemListEdit.displayName = 'ItemListEdit'

export default ItemListEdit
