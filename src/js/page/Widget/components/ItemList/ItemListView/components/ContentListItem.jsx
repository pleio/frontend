import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { isExternalUrl } from 'helpers'
import { showShortDate } from 'helpers/date/showDate'
import styled from 'styled-components'

import Avatar from 'js/components/Avatar/Avatar'
import FileFolderIcon from 'js/files/components/FileFolderIcon'
import getIconNameByMimetype from 'js/files/helpers/getIconNameByMimetype'
import { media } from 'js/lib/helpers'

const Wrapper = styled.li`
    &:not(:last-child) {
        border-bottom: 1px solid ${(p) => p.theme.color.grey[20]};
    }

    > a {
        display: flex;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        color: ${(p) => p.theme.color.text.black};

        ${media.mobileLandscapeDown`
            padding: 8px ${(p) => p.theme.padding.horizontal.small};
            flex-wrap: wrap;
        `};

        ${media.tabletUp`
            padding: 8px ${(p) => p.theme.padding.horizontal.normal};
        `};

        &:hover .ObjectItemLabel {
            text-decoration: underline;
        }
    }

    .ObjectItemImageText {
        display: flex;
        gap: 10px;
    }

    .ObjectItemText {
        padding: 2px 0;
    }

    .ObjectItemLabel {
        color: ${(p) => p.theme.color.primary.main};
    }

    .ObjectItemType {
        font-size: ${(p) => p.theme.font.size.tiny};
        line-height: ${(p) => p.theme.font.lineHeight.tiny};
        color: ${(p) => p.theme.color.text.grey};
        white-space: nowrap;
    }

    .ObjectItemDate {
        margin-left: auto;
        padding: 4px 0 4px 8px;
        font-size: ${(p) => p.theme.font.size.tiny};
        line-height: ${(p) => p.theme.font.lineHeight.tiny};
        color: ${(p) => p.theme.color.text.grey};
        white-space: nowrap;
    }
`

const ContentListItem = ({ index, entity, subtypes }) => {
    const { t } = useTranslation()

    if (!entity) return null

    const types = {
        Event: t('entity-event.content-name'),
        Blog: t('entity-blog.content-name'),
        Discussion: t('entity-discussion.content-name'),
        News: t('entity-news.content-name'),
        Page: t('global.page'),
        Question: t('entity-question.content-name'),
        Wiki: t('entity-wiki.content-name'),
        Folder: t('entity-file.folder'),
        File: t('entity-file.content-name'),
        Pad: t('entity-file.pad'),
        Podcast: t('entity-podcast.content-name'),
    }

    const {
        url,
        title,
        name,
        timePublished,
        hasChildren,
        icon,
        subtype,
        mimeType,
        __typename,
    } = entity

    const isExternal = isExternalUrl(url)
    const LinkElement = isExternal ? 'a' : Link

    const iconName =
        subtype === 'folder'
            ? hasChildren
                ? 'folder-filled'
                : 'folder'
            : subtype === 'pad'
              ? 'pad'
              : getIconNameByMimetype(mimeType)

    // When only showing file, folder and/or pad, then show icon and hide subtype
    const onlyFileFolderPads = subtypes?.every((subtype) =>
        ['file', 'folder', 'pad'].includes(subtype),
    )

    return (
        <Wrapper>
            <LinkElement
                href={isExternal ? url : null}
                to={!isExternal ? url : null}
                data-feed={index}
                target={isExternal ? '_blank' : null}
                rel={isExternal ? 'noopener noreferrer' : null}
            >
                <div className="ObjectItemImageText">
                    {__typename === 'Group' && (
                        <Avatar
                            disabled
                            image={icon?.download}
                            name={name}
                            size="small"
                        />
                    )}
                    {onlyFileFolderPads && (
                        <FileFolderIcon
                            name={iconName}
                            style={{ width: 20, height: 24 }}
                        />
                    )}
                    <div className="ObjectItemText">
                        <span className="ObjectItemLabel">{title || name}</span>
                        {subtypes?.length > 1 && !onlyFileFolderPads && (
                            <>
                                {' '}
                                <span className="ObjectItemType">
                                    {types[__typename]}
                                </span>
                            </>
                        )}
                    </div>
                </div>
                {timePublished && (
                    <time dateTime={timePublished} className="ObjectItemDate">
                        {showShortDate(timePublished)}
                    </time>
                )}
            </LinkElement>
        </Wrapper>
    )
}

export default ContentListItem
