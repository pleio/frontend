import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'
import { useIsMount } from 'helpers'

import Card from 'js/activity/components/Card'
import FetchMore from 'js/components/FetchMore'
import { Col, Row } from 'js/components/Grid/Grid'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Slider from 'js/components/Slider/Slider'
import { ownerViewFields } from 'js/lib/fragments/owner'
import EditWidgetPlaceholder from 'js/page/Widget/components/components/EditWidgetPlaceholder'
import ListWidgetWrapper from 'js/page/Widget/components/components/ListWidgetWrapper'

import ContentListItem from './components/ContentListItem'

const ItemListViewEntities = ({
    settings,
    containerGuid,
    editMode,
    currentColumnWidths,
}) => {
    const isMount = useIsMount()

    const {
        title,
        layout,
        template,
        subtypes,
        tags,
        tagCategories,
        orderBy,
        orderDirection,
        itemWidth,
        numberOfItems,
        enableExcerptMaxLines,
        excerptMaxLines,
        allowDisableLimit,
        limit,
        isFeatured,
    } = settings

    const [queryLimit, setQueryLimit] = useState(numberOfItems)
    const { loading, data, fetchMore, refetch } = useQuery(GET_ENTITIES, {
        variables: {
            containerGuid,
            subtypes,
            tags,
            tagCategories,
            orderBy,
            orderDirection,
            offset: 0,
            limit: layout === 'carousel' ? numberOfItems : queryLimit,
            isFeatured,
            showCoverImage: template.includes('coverImage'),
            showAuthor: template.includes('author'),
            showDatePublished: template.includes('datePublished'),
            showExcerpt: template.includes('excerpt'),
            showLikes: template.includes('likes'),
            showComments: template.includes('comments'),
        },
        skip: subtypes.length === 0,
    })

    const { t } = useTranslation()

    useEffect(() => {
        if (isMount) {
            // Prevents the wrong height being set on the widget (because of caching). Use useEffect, not useLayoutEffect.
            refetch()
        }
    }, [isMount, settings, refetch])

    const noResults = !loading && (!data || data?.entities?.edges?.length === 0)
    if (noResults && !title) {
        if (!editMode) return null
        return <EditWidgetPlaceholder title={t('widget-objects.title')} />
    }

    const ContentCard = ({ ...props }) => (
        <Card
            {...props}
            viewer={data.viewer}
            hideLikes={!template.includes('likes')}
            hideGroup={!!containerGuid}
            hideActions
            excerptMaxLines={enableExcerptMaxLines && excerptMaxLines}
        />
    )

    return (
        <ListWidgetWrapper
            layout={layout}
            noResults={noResults}
            title={title}
            widgetTitle={t('widget-objects.title')}
        >
            {loading ? (
                <LoadingSpinner
                    style={{
                        flexGrow: 1,
                    }}
                />
            ) : (
                data && (
                    <>
                        {layout === 'feed' ? (
                            // Feed
                            <FetchMore
                                edges={data.entities.edges}
                                getMoreResults={(data) => data.entities.edges}
                                fetchMore={fetchMore}
                                fetchCount={numberOfItems}
                                setLimit={setQueryLimit}
                                maxLimit={
                                    !allowDisableLimit
                                        ? Math.min(limit, data.entities.total)
                                        : data.entities.total
                                }
                                resultsMessage={t('global.count-items', {
                                    count: data.entities.edges.length,
                                })}
                            >
                                <Row $growContent>
                                    {data.entities.edges.map((entity, i) => {
                                        return (
                                            <Col
                                                key={i}
                                                mobileLandscape={
                                                    1 /
                                                    Math.min(
                                                        currentColumnWidths?.mobileLandscape,
                                                        itemWidth,
                                                    )
                                                }
                                                tablet={
                                                    1 /
                                                    Math.min(
                                                        currentColumnWidths?.tablet,
                                                        itemWidth,
                                                    )
                                                }
                                                desktopUp={
                                                    1 /
                                                    Math.min(
                                                        currentColumnWidths?.desktop,
                                                        itemWidth,
                                                    )
                                                }
                                            >
                                                <ContentCard
                                                    data-feed={i}
                                                    entity={{ entity }}
                                                    hideComments={
                                                        !template.includes(
                                                            'comments',
                                                        )
                                                    }
                                                />
                                            </Col>
                                        )
                                    })}
                                </Row>
                            </FetchMore>
                        ) : layout === 'carousel' ? (
                            // Carousel
                            <Slider>
                                {data.entities.edges.map((entity, i) => {
                                    return (
                                        <ContentCard
                                            key={i}
                                            data-feed={i}
                                            entity={{ entity }}
                                            hideComments={
                                                !template.includes('comments')
                                            }
                                        />
                                    )
                                })}
                            </Slider>
                        ) : (
                            // List
                            <FetchMore
                                isScrollableBox
                                containHeight
                                edges={data.entities.edges}
                                getMoreResults={(data) => data.entities.edges}
                                fetchMore={fetchMore}
                                fetchCount={numberOfItems}
                                setLimit={setQueryLimit}
                                maxLimit={
                                    !allowDisableLimit
                                        ? Math.min(limit, data.entities.total)
                                        : data.entities.total
                                }
                                resultsMessage={t('global.count-items', {
                                    count: data.entities.edges.length,
                                })}
                            >
                                <ul
                                    style={{
                                        marginTop: '12px',
                                        marginBottom: '8px',
                                    }}
                                >
                                    {data.entities.edges.map((entity, i) => {
                                        return (
                                            <ContentListItem
                                                key={i}
                                                index={i}
                                                entity={entity}
                                                subtypes={subtypes}
                                            />
                                        )
                                    })}
                                </ul>
                            </FetchMore>
                        )}
                    </>
                )
            )}
        </ListWidgetWrapper>
    )
}

const coreTemplate = `
title
url
statusPublished
subtype
`

const translationTemplate = `
localTitle
isTranslationEnabled
`

const featuredTemplate = `
featured @include(if: $showCoverImage) {
    image {
        ... on File {
            guid
            download
        }
    }
    video
    videoTitle
    videoThumbnailUrl
    positionY
    alt
}
`

const ownerTemplate = `
    showOwner @include(if: $showAuthor)
    owner @include(if: $showAuthor) {
        ${ownerViewFields}
    }
`

const timePublishedTemplate = `
timePublished @include(if: $showDatePublished)
`

const excerptTemplate = `
excerpt @include(if: $showExcerpt)
`

const likeTemplate = `
hasVoted @include(if: $showLikes)
votes @include(if: $showLikes)
`

const commentsTemplate = `
commentCount @include(if: $showComments)
canComment @include(if: $showComments)
`

const GET_ENTITIES = gql`
    query WidgetObjects(
        $offset: Int
        $limit: Int
        $containerGuid: String
        $subtypes: [String!]
        $tags: [String!]
        $tagCategories: [TagCategoryInput!]
        $orderBy: OrderBy
        $orderDirection: OrderDirection
        $isFeatured: Boolean
        $showCoverImage: Boolean!
        $showAuthor: Boolean!
        $showDatePublished: Boolean!
        $showExcerpt: Boolean!
        $showLikes: Boolean!
        $showComments: Boolean!
    ) {
        viewer {
            guid
            user {
                guid
            }
        }
        entities(
            subtypes: $subtypes
            tags: $tags
            tagCategories: $tagCategories
            offset: $offset
            limit: $limit
            containerGuid: $containerGuid
            orderBy: $orderBy
            orderDirection: $orderDirection
            isFeatured: $isFeatured
            matchStrategy: any
        ) {
            total
            edges {
                guid
                __typename
                ... on Blog {
                    ${coreTemplate}
                    ${translationTemplate}
                    ${featuredTemplate}
                    ${ownerTemplate}
                    ${timePublishedTemplate}
                    ${excerptTemplate}
                    ${likeTemplate}
                    ${commentsTemplate}
                }
                ... on News {
                    ${coreTemplate}
                    ${translationTemplate}
                    ${featuredTemplate}
                    ${ownerTemplate}
                    ${timePublishedTemplate}
                    ${excerptTemplate}
                    ${likeTemplate}
                    ${commentsTemplate}
                }
                ... on Discussion {
                    ${coreTemplate}
                    ${translationTemplate}
                    ${featuredTemplate}
                    ${ownerTemplate}
                    ${timePublishedTemplate}
                    ${excerptTemplate}
                    ${likeTemplate}
                    ${commentsTemplate}
                }
                ... on Event {
                    ${coreTemplate}
                    ${translationTemplate}
                    ${featuredTemplate}
                    ${ownerTemplate}
                    ${timePublishedTemplate}
                    ${excerptTemplate}
                    ${likeTemplate}
                    ${commentsTemplate}
                    startDate
                    endDate
                }
                ... on Question {
                    ${coreTemplate}
                    ${translationTemplate}
                    ${featuredTemplate}
                    ${ownerTemplate}
                    ${timePublishedTemplate}
                    ${excerptTemplate}
                    ${likeTemplate}
                    ${commentsTemplate}
                }
                ... on Wiki {
                    ${coreTemplate}
                    ${translationTemplate}
                    ${featuredTemplate}
                    ${ownerTemplate}
                    ${timePublishedTemplate}
                    ${excerptTemplate}
                }
                ... on Page {
                    title
                    url
                    statusPublished
                    ${translationTemplate}
                    ${ownerTemplate}
                    ${timePublishedTemplate}
                    ${excerptTemplate}
                }
                ... on Folder {
                    ${coreTemplate}
                    ${featuredTemplate}
                    ${ownerTemplate}
                    ${timePublishedTemplate}
                    ${excerptTemplate}
                    ${likeTemplate}
                    hasChildren
                }
                ... on File {
                    ${coreTemplate}
                    ${featuredTemplate}
                    ${ownerTemplate}
                    ${timePublishedTemplate}
                    ${excerptTemplate}
                    ${likeTemplate}
                    mimeType
                }
                ... on Pad {
                    ${coreTemplate}
                    ${translationTemplate}
                    ${featuredTemplate}
                    ${ownerTemplate}
                    ${timePublishedTemplate}
                    ${excerptTemplate}
                    ${likeTemplate}
                }
                ... on ExternalContent {
                    __typename
                    title
                    url
                    ${timePublishedTemplate}
                    ${featuredTemplate}
                    ${excerptTemplate}
                }
                ... on Podcast {
                    ${coreTemplate}
                    ${translationTemplate}
                    ${ownerTemplate}
                    ${timePublishedTemplate}
                    ${excerptTemplate}
                    ${likeTemplate}
                }
                ... on Episode {
                    ${coreTemplate}
                    ${translationTemplate}
                    ${ownerTemplate}
                    ${timePublishedTemplate}
                    ${excerptTemplate}
                    podcast {
                        guid
                        title
                        url
                    }
                }
            }
        }
    }
`

export default ItemListViewEntities
