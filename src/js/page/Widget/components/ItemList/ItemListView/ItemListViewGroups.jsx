import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'
import { useIsMount } from 'helpers'

import FetchMore from 'js/components/FetchMore'
import { Col, Row } from 'js/components/Grid/Grid'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Slider from 'js/components/Slider/Slider'
import GroupCard from 'js/group/List/components/Card'
import { iconViewFragment } from 'js/lib/fragments/icon'
import EditWidgetPlaceholder from 'js/page/Widget/components/components/EditWidgetPlaceholder'
import ListWidgetWrapper from 'js/page/Widget/components/components/ListWidgetWrapper'

import ContentListItem from './components/ContentListItem'

const ItemListViewGroups = ({ settings, editMode, currentColumnWidths }) => {
    const {
        title,
        layout,
        template,
        tags,
        tagCategories,
        itemWidth,
        numberOfItems,
        allowDisableLimit,
        limit,
    } = settings

    const [queryLimit, setQueryLimit] = useState(numberOfItems)
    const { loading, data, fetchMore, refetch } = useQuery(GET_GROUPS, {
        variables: {
            tags,
            tagCategories,
            offset: 0,
            limit: queryLimit,
            showExcerpt: template.includes('excerpt'),
            isCard: ['feed', 'carousel'].includes(layout),
        },
    })

    const { t } = useTranslation()

    const isMount = useIsMount()

    useEffect(() => {
        if (isMount) {
            // Prevents the wrong height being set on the widget (because of caching). Use useEffect, not useLayoutEffect.
            refetch()
        }
    }, [isMount, settings, refetch])

    const noResults = !loading && (!data || data?.groups?.edges?.length === 0)
    if (noResults && !title) {
        if (!editMode) return null
        return <EditWidgetPlaceholder title={t('widget-objects.title')} />
    }

    return (
        <ListWidgetWrapper
            layout={layout}
            title={title}
            widgetTitle={t('widget-objects.title')}
        >
            {loading ? (
                <LoadingSpinner
                    style={{
                        flexGrow: 1,
                    }}
                />
            ) : (
                data && (
                    <>
                        {layout === 'feed' ? (
                            // Feed
                            <FetchMore
                                edges={data.groups.edges}
                                getMoreResults={(data) => data.groups.edges}
                                fetchMore={fetchMore}
                                fetchCount={numberOfItems}
                                setLimit={setQueryLimit}
                                maxLimit={
                                    !allowDisableLimit
                                        ? Math.min(limit, data.groups.total)
                                        : data.groups.total
                                }
                                resultsMessage={t('global.count-items', {
                                    count: data.groups.edges.length,
                                })}
                            >
                                <Row $growContent>
                                    {data.groups.edges.map((entity, i) => {
                                        return (
                                            <Col
                                                key={i}
                                                mobileLandscape={
                                                    1 /
                                                    Math.min(
                                                        currentColumnWidths?.mobileLandscape,
                                                        itemWidth,
                                                    )
                                                }
                                                tablet={
                                                    1 /
                                                    Math.min(
                                                        currentColumnWidths?.tablet,
                                                        itemWidth,
                                                    )
                                                }
                                                desktopUp={
                                                    1 /
                                                    Math.min(
                                                        currentColumnWidths?.desktop,
                                                        itemWidth,
                                                    )
                                                }
                                            >
                                                <GroupCard
                                                    data-feed={i}
                                                    entity={entity}
                                                />
                                            </Col>
                                        )
                                    })}
                                </Row>
                            </FetchMore>
                        ) : layout === 'carousel' ? (
                            // Carousel
                            <Slider>
                                {data.groups.edges.map((entity, i) => {
                                    return (
                                        <GroupCard
                                            key={i}
                                            data-feed={i}
                                            entity={entity}
                                        />
                                    )
                                })}
                            </Slider>
                        ) : (
                            // List
                            <FetchMore
                                isScrollableBox
                                containHeight
                                edges={data.groups.edges}
                                getMoreResults={(data) => data.groups.edges}
                                fetchMore={fetchMore}
                                fetchCount={numberOfItems}
                                setLimit={setQueryLimit}
                                maxLimit={
                                    !allowDisableLimit
                                        ? Math.min(limit, data.groups.total)
                                        : data.groups.total
                                }
                                resultsMessage={t('global.count-items', {
                                    count: data.groups.edges.length,
                                })}
                            >
                                <ul
                                    style={{
                                        marginTop: '12px',
                                        marginBottom: '8px',
                                    }}
                                >
                                    {data.groups.edges.map((entity, i) => (
                                        <ContentListItem
                                            key={i}
                                            index={i}
                                            entity={entity}
                                        />
                                    ))}
                                </ul>
                            </FetchMore>
                        )}
                    </>
                )
            )}
        </ListWidgetWrapper>
    )
}

const GET_GROUPS = gql`
    query WidgetGroups(
        $tags: [String!]
        $tagCategories: [TagCategoryInput!]
        $offset: Int
        $limit: Int
        $showExcerpt: Boolean!
        $isCard: Boolean!
    ) {
        groups(
            tags: $tags
            tagCategories: $tagCategories
            offset: $offset
            limit: $limit
            matchStrategy: any
        ) {
            total
            edges {
                guid
                name
                ${iconViewFragment}
                url
                excerpt @include(if: $showExcerpt)
                description @include(if: $isCard)
                richDescription @include(if: $isCard)
                canEdit @include(if: $isCard)
                isMembershipOnRequest @include(if: $isCard)
                isClosed @include(if: $isCard)
                isFeatured @include(if: $isCard)
                membership @include(if: $isCard)
                memberCount @include(if: $isCard)
            }
        }
    }
`

export default ItemListViewGroups
