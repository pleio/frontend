import React, { useEffect, useState } from 'react'
import { useIsMount } from 'helpers'

import ItemListViewEntitiesWrapper from './ItemListViewEntities'
import ItemListViewGroups from './ItemListViewGroups'

const ItemListView = ({
    guid,
    settings,
    containerGuid,
    editMode,
    currentColumnWidths,
}) => {
    const isMount = useIsMount()

    const [keyNumber, setKeyNumber] = useState(0)
    useEffect(() => {
        if (editMode && !isMount) {
            // Force rerender to update the query when settings have changed.
            setKeyNumber((prev) => prev + 1)
        }
    }, [editMode, isMount, settings])

    const hasGroups = settings?.subtypes.indexOf('group') > -1
    const Wrapper = hasGroups ? ItemListViewGroups : ItemListViewEntitiesWrapper

    return (
        <Wrapper
            key={`${guid}-view-${keyNumber}`}
            settings={settings}
            containerGuid={containerGuid}
            editMode={editMode}
            currentColumnWidths={currentColumnWidths}
        />
    )
}

export default ItemListView
