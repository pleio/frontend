import React, { forwardRef } from 'react'
import { gql, useQuery } from '@apollo/client'
import { hasJsonStructure, sanitizeTagCategories } from 'helpers'

import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import useGetSetting from 'js/page/Widget/hooks/useGetSetting'

import ItemListEdit from './ItemListEdit/ItemListEdit'
import ItemListView from './ItemListView/ItemListView'

const ItemList = forwardRef(
    (
        { guid, group, entity, isEditing, onSave, editMode, columnWidth },
        ref,
    ) => {
        const getSetting = useGetSetting(entity)
        const { data, loading } = useQuery(SITE_DATA)

        const columnWidths = {
            12: {
                mobileLandscape: 2,
                tablet: 3,
                desktop: 4,
            },
            8: {
                mobileLandscape: 2,
                tablet: 2,
                desktop: 2,
            },
            6: {
                mobileLandscape: 2,
                tablet: 2,
                desktop: 2,
            },
            4: {
                mobileLandscape: 1,
                tablet: 1,
                desktop: 1,
            },
            3: {
                mobileLandscape: 1,
                tablet: 1,
                desktop: 1,
            },
        }
        const currentColumnWidths = columnWidths[columnWidth]

        const itemWidthMax = currentColumnWidths.desktop

        if (loading) return <LoadingSpinner />
        if (!data) return null

        const { site } = data

        let subtypes = getSetting('subtypes')
        subtypes = subtypes ? subtypes.split(',') : []
        let customTags = getSetting('tags')
        customTags = customTags ? customTags.split(',') : []
        const categoryTags = hasJsonStructure(getSetting('categoryTags'))
            ? JSON.parse(getSetting('categoryTags'))
            : []

        const allowDisableLimit =
            getSetting('allowDisableLimit', 'true') === 'true'

        if (isEditing) {
            const settings = {
                title: getSetting('title'),
                layout: getSetting('layout', 'list'),
                template: getSetting('template')?.split(',') || [],
                subtypes,
                groupTypes: group?.plugins,
                categoryTags,
                tags: customTags,
                itemWidth: Math.min(getSetting('itemWidth', 1), itemWidthMax),
                numberOfItems: getSetting('numberOfItems', 5),
                enableExcerptMaxLines:
                    getSetting('enableExcerptMaxLines') === 'true',
                excerptMaxLines: getSetting('excerptMaxLines', 1),
                limit: getSetting('limit', 20),
                allowDisableLimit,
                showDate: getSetting('showDate') === 'true',
                sortBy: getSetting('sortBy', 'timePublished-desc'),
                isFeatured: getSetting('isFeatured') === 'true',
            }

            return (
                <ItemListEdit
                    ref={ref}
                    guid={guid}
                    site={site}
                    settings={settings}
                    onSave={onSave}
                    columnWidth={columnWidth}
                    itemWidthMax={itemWidthMax}
                    group={group}
                />
            )
        }

        const sortBy = getSetting('sortBy', 'timePublished-desc').split('-')

        const settings = {
            title: getSetting('title'),
            layout: getSetting('layout', 'list'),
            template: getSetting('template')?.split(',') || [],
            subtypes,
            tags: site.customTagsAllowed ? customTags : [],
            tagCategories: sanitizeTagCategories(
                site.tagCategories,
                categoryTags,
            ),
            showDate: getSetting('showDate') === 'true',
            orderBy: sortBy[0],
            orderDirection: sortBy[1],
            itemWidth: Math.min(getSetting('itemWidth', 1), itemWidthMax),
            numberOfItems: parseInt(getSetting('numberOfItems', 5), 10),
            enableExcerptMaxLines:
                getSetting('enableExcerptMaxLines') === 'true',
            excerptMaxLines: parseInt(getSetting('excerptMaxLines', 1), 10),
            allowDisableLimit,
            limit: parseInt(getSetting('limit', 20), 10),
            isFeatured: getSetting('isFeatured') === 'true',
        }

        return (
            <ItemListView
                guid={guid}
                site={site}
                settings={settings}
                containerGuid={group?.guid}
                editMode={editMode}
                currentColumnWidths={currentColumnWidths}
            />
        )
    },
)

ItemList.displayName = 'ItemList'

const SITE_DATA = gql`
    query ItemListWidget {
        site {
            guid
            customTagsAllowed
            tagCategories {
                name
                values
            }
            entityFilter {
                contentTypes {
                    key
                    value
                }
            }
        }
    }
`

export default ItemList
