import React, { forwardRef, useImperativeHandle } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import CustomTagsField from 'js/components/CustomTagsField'
import FormItem from 'js/components/Form/FormItem'
import Setting from 'js/components/Setting'
import TagCategoriesField from 'js/components/TagCategories/TagCategoriesField'
import WidgetViewPortal from 'js/page/Widget/components/components//WidgetViewPortal'
import convertObjectToWidgetSettings from 'js/page/Widget/helpers/convertObjectToWidgetSettings'
import useCategoryTags from 'js/page/Widget/hooks/useCategoryTags'
import useCustomTags from 'js/page/Widget/hooks/useCustomTags'

import Events from './Events'

const EventsEdit = forwardRef(
    ({ settings, onSave, site, guid, entity, group }, ref) => {
        const { t } = useTranslation()

        const { customTagsAllowed, tagCategories } = site
        const { itemCount, tags } = settings

        const {
            categoryTagsValue,
            setCategoryTagsValue,
            stringifyTagCategories,
        } = useCategoryTags(entity)

        const {
            customTags,
            handleAddTags,
            handleRemoveTags,
            getParsedCustomTags,
        } = useCustomTags(tags)

        useImperativeHandle(ref, () => ({
            onSave() {
                submitForm()
            },
        }))

        const defaultValues = {
            itemCount,
            tags,
            tagCategories,
        }

        const { control, handleSubmit, watch } = useForm({
            defaultValues,
        })
        const watchValues = watch()

        const filterValues = ({ itemCount }) => {
            return convertObjectToWidgetSettings({
                itemCount: itemCount.toString(),
                categoryTags: stringifyTagCategories(tagCategories),
                tags: getParsedCustomTags(),
            })
        }

        const onSubmit = () => {
            onSave(filterValues(watchValues))
        }
        const submitForm = handleSubmit(onSubmit)

        return (
            <>
                <div
                    style={{
                        marginBottom: '24px',
                    }}
                >
                    <TagCategoriesField
                        name="categoryTags"
                        label={t('widget-objects.filter-tag-categories')}
                        tagCategories={tagCategories}
                        value={categoryTagsValue}
                        setTags={setCategoryTagsValue}
                    />

                    {customTagsAllowed && (
                        <div
                            style={{
                                marginTop: '16px',
                            }}
                        >
                            <CustomTagsField
                                name="customTags"
                                value={customTags}
                                label={t('global.custom-tags')}
                                onAddTag={handleAddTags}
                                onRemoveTag={handleRemoveTags}
                            />
                        </div>
                    )}
                </div>

                <Setting
                    subtitle="widget.nr-of-items"
                    helper={t('widget.nr-of-items-list-helper')}
                    htmlFor="itemCount"
                    inputWidth="small"
                >
                    <FormItem
                        type="number"
                        min={1}
                        max={10}
                        control={control}
                        name="itemCount"
                    />
                </Setting>

                <WidgetViewPortal
                    Component={Events}
                    settings={filterValues(watchValues)}
                    guid={guid}
                    site={site}
                    group={group}
                />
            </>
        )
    },
)

EventsEdit.displayName = 'EventsEdit'

export default EventsEdit
