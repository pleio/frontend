import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { media, useIsMount } from 'helpers'
import showDate, { isSameDate, showDateTime } from 'helpers/date/showDate'
import styled from 'styled-components'

import Card, { CardContent } from 'js/components/Card/Card'
import FetchMore from 'js/components/FetchMore'
import { H3 } from 'js/components/Heading'
import HideVisually from 'js/components/HideVisually/HideVisually'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import NoResultsMessage from 'js/components/NoResultsMessage/NoResultsMessage'
import Tooltip from 'js/components/Tooltip/Tooltip'
import DateCalendar from 'js/events/components/DateCalendar'
import EditWidgetPlaceholder from 'js/page/Widget/components/components/EditWidgetPlaceholder'

const Wrapper = styled(Card)`
    display: flex;
    flex-direction: column;

    .EventsItem {
        &:not(:last-child) {
            border-bottom: 1px solid ${(p) => p.theme.color.grey[20]};
        }

        > a {
            display: flex;
            align-items: center;

            ${media.mobileLandscapeDown`
                padding: 8px ${(p) => p.theme.padding.horizontal.small};
            `};

            ${media.tabletUp`
                padding: 8px ${(p) => p.theme.padding.horizontal.normal};
            `};

            &:hover .EventsItemTitle {
                text-decoration: underline;
            }
        }
    }

    .EventsItemMeta {
        flex-grow: 1;
        display: flex;
        flex-direction: column;
        overflow: hidden;
        margin-right: 8px;
    }

    .EventsItemTitle {
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        color: ${(p) => p.theme.color.primary.main};
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .EventsItemDate {
        margin-top: 2px;
        display: flex;
        font-size: ${(p) => p.theme.font.size.tiny};
        line-height: ${(p) => p.theme.font.lineHeight.tiny};
        color: ${(p) => p.theme.color.text.grey};
        white-space: nowrap;
    }

    .EventsMore {
        height: 32px;
        display: flex;
        align-items: center;
        justify-content: center;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        color: ${(p) => p.theme.color.primary.main};

        &:hover {
            text-decoration: underline;
        }
    }
`

const EventsView = ({ containerGuid, editMode = false, settings, ...rest }) => {
    const { t } = useTranslation()

    const { itemCount = 5, tags, tagCategories } = settings

    const [queryLimit, setQueryLimit] = useState(itemCount)
    const { loading, data, fetchMore, refetch } = useQuery(GET_EVENTS, {
        variables: {
            containerGuid,
            offset: 0,
            limit: queryLimit,
            tags: tags || null,
            tagCategories,
        },
    })

    const isMount = useIsMount()

    useEffect(() => {
        if (isMount) {
            // Prevents the wrong height being set on the widget (because of caching). Use useEffect, not useLayoutEffect.
            refetch()
        }
    }, [])

    if (
        !data ||
        (data && !loading && (!data.events || data.events.edges.length === 0))
    ) {
        if (!editMode) return null

        return (
            <EditWidgetPlaceholder
                title={t('widget-events.title')}
                helper={t('widget-events.no-future-events')}
            />
        )
    }

    return (
        <Wrapper {...rest}>
            <CardContent style={{ paddingBottom: 0 }}>
                <H3 as="h2" style={{ marginBottom: '8px' }}>
                    {t('widget-events.title')}
                </H3>
            </CardContent>

            {loading ? (
                <LoadingSpinner
                    style={{
                        flexGrow: 1,
                    }}
                />
            ) : (
                <FetchMore
                    isScrollableBox
                    containHeight
                    edges={data.events.edges}
                    getMoreResults={(data) => data.events.edges}
                    fetchMore={fetchMore}
                    fetchCount={5}
                    setLimit={setQueryLimit}
                    maxLimit={data.events.total}
                    resultsMessage={t('widget-events.result', {
                        count: data.events.edges.length,
                    })}
                    noResults={
                        <NoResultsMessage
                            title={t('widget-events.no-future-events')}
                        />
                    }
                >
                    <ul
                        style={{
                            marginBottom: '12px',
                        }}
                    >
                        {data.events.edges.map((entity) => {
                            const { title, startDate, endDate } = entity

                            return (
                                <li key={entity.guid} className="EventsItem">
                                    <Link to={entity.url}>
                                        <div className="EventsItemMeta">
                                            <div className="EventsItemTitle">
                                                {title}
                                            </div>
                                            <div className="EventsItemDate">
                                                <HideVisually>
                                                    {t(
                                                        'entity-event.start-date',
                                                    )}{' '}
                                                    <time dateTime={startDate}>
                                                        {showDate(startDate)}
                                                    </time>
                                                </HideVisually>
                                                <Tooltip
                                                    content={showDateTime(
                                                        startDate,
                                                    )}
                                                >
                                                    <span aria-hidden>
                                                        {showDate(startDate)}
                                                    </span>
                                                </Tooltip>
                                                {!!endDate &&
                                                    !isSameDate(
                                                        startDate,
                                                        endDate,
                                                    ) && (
                                                        <>
                                                            <span
                                                                style={{
                                                                    padding:
                                                                        '0 3px',
                                                                }}
                                                            >
                                                                -
                                                            </span>
                                                            <HideVisually>
                                                                {t(
                                                                    'entity-event.end-date',
                                                                )}{' '}
                                                                <time
                                                                    dateTime={
                                                                        endDate
                                                                    }
                                                                >
                                                                    {showDate(
                                                                        endDate,
                                                                    )}
                                                                </time>
                                                            </HideVisually>
                                                            <Tooltip
                                                                content={showDateTime(
                                                                    endDate,
                                                                )}
                                                            >
                                                                <span
                                                                    aria-hidden
                                                                >
                                                                    {showDate(
                                                                        endDate,
                                                                    )}
                                                                </span>
                                                            </Tooltip>
                                                        </>
                                                    )}
                                            </div>
                                        </div>
                                        <DateCalendar
                                            startDate={startDate}
                                            endDate={endDate}
                                            size="small"
                                        />
                                    </Link>
                                </li>
                            )
                        })}
                    </ul>
                </FetchMore>
            )}
        </Wrapper>
    )
}

const GET_EVENTS = gql`
    query Events(
        $containerGuid: String
        $limit: Int!
        $offset: Int!
        $tags: [String]
        $tagCategories: [TagCategoryInput]
    ) {
        entity(guid: $containerGuid) {
            guid
            ... on Group {
                url
            }
        }
        events(
            containerGuid: $containerGuid
            offset: $offset
            limit: $limit
            tags: $tags
            tagCategories: $tagCategories
        ) {
            total
            edges {
                ... on Event {
                    guid
                    title
                    startDate
                    endDate
                    url
                }
            }
        }
    }
`

export default EventsView
