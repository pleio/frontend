import React, { forwardRef } from 'react'
import { hasJsonStructure } from 'helpers'

import useGetSetting from 'js/page/Widget/hooks/useGetSetting'

import EventsEdit from './EventsEdit'
import EventsView from './EventsView'

const Events = forwardRef(
    ({ guid, site, entity, group, onSave, editMode, isEditing }, ref) => {
        const getSetting = useGetSetting(entity)

        const settings = {
            itemCount: parseInt(getSetting('itemCount') || 5, 10),
            tags: getSetting('tags') ? getSetting('tags').split(',') : [],
            tagCategories: hasJsonStructure(getSetting('categoryTags'))
                ? JSON.parse(getSetting('categoryTags'))
                : [],
        }

        if (isEditing) {
            return (
                <EventsEdit
                    ref={ref}
                    guid={guid}
                    settings={settings}
                    onSave={onSave}
                    site={site}
                    group={group}
                    entity={entity}
                />
            )
        }

        return (
            <EventsView
                key={`${isEditing}`} // Trigger graphql refetch after edit
                editMode={editMode}
                settings={settings}
                containerGuid={group?.guid}
            />
        )
    },
)

Events.displayName = 'Events'

export default Events
