import React, { forwardRef, useImperativeHandle, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import Spacer from 'js/components/Spacer/Spacer'
import TabMenu from 'js/components/TabMenu'
import TabPage from 'js/components/TabPage'
import WidgetViewPortal from 'js/page/Widget/components/components//WidgetViewPortal'
import convertObjectToWidgetSettings from 'js/page/Widget/helpers/convertObjectToWidgetSettings'

import Carousel, { Settings, WidgetSetting } from '../Carousel'

import Content from './components/Content'
import General from './components/General'

interface CarouselEdit {
    guid: string
    containerGuid?: string
    settings: Settings
    onSave: (settings: WidgetSetting[]) => void
}

const CarouselEdit = forwardRef(
    ({ guid, containerGuid, settings, onSave }: CarouselEdit, ref) => {
        const { t } = useTranslation()

        useImperativeHandle(ref, () => ({
            onSave() {
                submitForm()
            },
        }))

        const [tab, setTab] = useState('general')

        const { title, height, items } = settings

        const { control, handleSubmit, watch, setValue } = useForm({
            defaultValues: {
                title,
                height,
                items,
            },
        })
        const watchValues = watch()

        const filterValues = ({ title, height, items }) => {
            const filteredItems = items
                .filter(({ title, url, image }) => title || url || image?.url)
                .map(({ id, title, url, buttonText, image }) => ({
                    id,
                    title,
                    url,
                    buttonText,
                    image,
                }))

            return convertObjectToWidgetSettings({
                title,
                height,
                items: {
                    links: filteredItems,
                },
            })
        }

        const onSubmit = () => {
            // @ts-ignore
            onSave(filterValues(watchValues))
        }
        const submitForm = handleSubmit(onSubmit)

        return (
            <>
                <Spacer spacing="small">
                    {/* @ts-ignore */}
                    <TabMenu
                        label={t('global.settings')}
                        options={[
                            {
                                onClick: () => setTab('general'),
                                label: t('global.general'),
                                isActive: tab === 'general',
                                id: 'tab-general',
                                ariaControls: 'tabpanel-general',
                            },
                            {
                                onClick: () => setTab('content'),
                                label: t('global.content'),
                                isActive: tab === 'content',
                                id: 'tab-content',
                                ariaControls: 'tabpanel-content',
                            },
                        ]}
                        edgePadding
                        edgeMargin
                        showBorder
                    />

                    <TabPage
                        id={`tabpanel-${tab}`}
                        ariaLabelledby={`tab-${tab}`}
                        visible
                    >
                        {tab === 'general' && (
                            <General guid={guid} control={control} />
                        )}

                        {tab === 'content' && (
                            <Content
                                guid={guid}
                                control={control}
                                setValue={setValue}
                            />
                        )}
                    </TabPage>
                </Spacer>

                <WidgetViewPortal
                    Component={Carousel}
                    settings={filterValues(watchValues)}
                    guid={guid}
                    containerGuid={containerGuid}
                />
            </>
        )
    },
)

CarouselEdit.displayName = 'CarouselEdit'

export default CarouselEdit
