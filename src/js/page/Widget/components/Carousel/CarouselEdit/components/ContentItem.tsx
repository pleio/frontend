import React, { useState } from 'react'
import { Draggable } from 'react-beautiful-dnd'
import { useTranslation } from 'react-i18next'
import styled, { css } from 'styled-components'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import IconButton from 'js/components/IconButton/IconButton'
import Img from 'js/components/Img/Img'
import Spacer from 'js/components/Spacer/Spacer'
import UploadOrSelectFilesModal from 'js/files/UploadOrSelectFiles/UploadOrSelectFilesModal'

import RemoveIcon from 'icons/delete-small.svg'
import EditIcon from 'icons/edit-small.svg'
import MoveIcon from 'icons/move-vertical.svg'

interface WrapperProps extends React.HTMLAttributes<HTMLLIElement> {
    $isDragging: boolean
    $hasTitle: boolean
}

const Wrapper = styled.li<WrapperProps>`
    overflow: hidden;
    font-size: ${(p) => p.theme.font.size.small};
    line-height: ${(p) => p.theme.font.lineHeight.small};
    color: ${(p) => p.theme.color.text.black};
    background-color: white;
    border-bottom: 1px solid ${(p) => p.theme.color.grey[20]};

    &:last-child {
        border-bottom-color: transparent;
    }

    ${(p) =>
        p.$isDragging &&
        css`
            box-shadow: ${p.theme.shadow.soft.center};
            border-bottom-color: transparent;
        `};

    .content-item--title {
        flex-grow: 1;
        align-self: stretch;
        display: flex;
        align-items: center;
        padding: 4px 0;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};

        ${(p) =>
            p.$hasTitle
                ? css`
                      font-weight: ${(p) => p.theme.font.weight.semibold};
                  `
                : css`
                      color: ${(p) => p.theme.color.text.grey};
                  `};
    }

    .content-item--properties {
        padding: 8px 20px 16px;
    }
`

interface Props {
    id: string
    index: number
    control: any
    watchItems: [
        {
            title: string
            image: { id: string; url: string }
            url: string
            isEditing: boolean
        },
    ]
    setValue: (string, boolean) => void
    onRemove: () => void
}

const ContentItem = ({
    id,
    index,
    control,
    setValue,
    watchItems,
    onRemove,
}: Props) => {
    const { t } = useTranslation()

    const { title, image, url, isEditing } = watchItems?.[index] ?? {}

    const toggleIsEditing = () => {
        setValue(`items.${index}.isEditing`, !isEditing)
    }

    const [showUploadModal, setShowUploadModal] = useState(false)

    const buttonProps = {
        type: 'button' as const,
        size: 'large' as const,
        variant: 'secondary' as const,
        hoverSize: 'normal' as const,
        radiusStyle: 'rounded' as const,
    }

    const itemTitle = title || url || t('global.new-item')

    return (
        <Draggable draggableId={id} index={index}>
            {(provided, snapshot) => (
                <Wrapper
                    ref={provided.innerRef}
                    $isDragging={snapshot.isDragging}
                    $hasTitle={!!(title || url)}
                    {...provided.draggableProps}
                >
                    <div style={{ display: 'flex', alignItems: 'flex-start' }}>
                        <IconButton
                            as="div"
                            size="large"
                            variant="secondary"
                            hoverSize="normal"
                            aria-label={t('aria.move-link', {
                                label: itemTitle,
                            })}
                            {...provided.dragHandleProps}
                        >
                            <MoveIcon />
                        </IconButton>

                        <div
                            className="content-item--title"
                            aria-label={t('aria.move-link', {
                                label: itemTitle,
                            })}
                            {...provided.dragHandleProps}
                        >
                            {itemTitle}
                        </div>

                        <Flexer gutter="tiny" divider="normal">
                            <IconButton
                                {...buttonProps}
                                onClick={toggleIsEditing}
                                aria-pressed={isEditing}
                                aria-label={t('aria.edit-link', {
                                    label: itemTitle,
                                })}
                                tooltip={t('action.edit')}
                            >
                                <EditIcon />
                            </IconButton>
                            <IconButton
                                {...buttonProps}
                                onClick={onRemove}
                                aria-label={t('aria.remove-link', {
                                    label: itemTitle,
                                })}
                                tooltip={t('action.delete')}
                            >
                                <RemoveIcon />
                            </IconButton>
                        </Flexer>
                    </div>

                    {isEditing && (
                        <Spacer
                            spacing="small"
                            className="content-item--properties"
                        >
                            {/* @ts-ignore */}
                            <FormItem
                                control={control}
                                type="text"
                                name={`items.${index}.title`}
                                label={t('form.title')}
                            />
                            {/* @ts-ignore */}
                            <FormItem
                                control={control}
                                type="text"
                                name={`items.${index}.url`}
                                label={t('form.link')}
                            />
                            {/* @ts-ignore */}
                            <FormItem
                                control={control}
                                type="text"
                                name={`items.${index}.buttonText`}
                                label={t('form.button-text')}
                                helper={
                                    !url &&
                                    t('widget-carousel.button-text-helper')
                                }
                            />
                            {!image && (
                                <Flexer justifyContent="flex-start">
                                    {!image && (
                                        <Button
                                            size="small"
                                            variant="tertiary"
                                            onClick={() => {
                                                setShowUploadModal(
                                                    (img) => !img,
                                                )
                                            }}
                                        >
                                            {t('form.add-image')}
                                        </Button>
                                    )}
                                </Flexer>
                            )}
                            {/* @ts-ignore */}
                            <FormItem
                                type="text"
                                control={control}
                                value={image}
                                name={`items.${index}.image`}
                                style={{ display: 'none' }}
                            />
                            {image && (
                                <>
                                    <Img
                                        src={image?.url}
                                        aria-hidden
                                        style={{ height: '100px' }}
                                    />
                                    <Flexer
                                        justifyContent="flex-start"
                                        style={{ marginTop: 12 }}
                                    >
                                        <Button
                                            size="small"
                                            variant="secondary"
                                            onClick={() => {
                                                setShowUploadModal(
                                                    (img) => !img,
                                                )
                                            }}
                                        >
                                            {t('form.edit-image')}..
                                        </Button>
                                        <Button
                                            size="small"
                                            variant="secondary"
                                            onClick={() => {
                                                setValue(
                                                    `items.${index}.image`,
                                                    '',
                                                )
                                            }}
                                        >
                                            {t('form.remove-image')}
                                        </Button>
                                    </Flexer>
                                </>
                            )}
                        </Spacer>
                    )}

                    <UploadOrSelectFilesModal
                        title={t('action.insert-image')}
                        accept="image"
                        showModal={showUploadModal}
                        setShowModal={setShowUploadModal}
                        onComplete={({ guid, download }) => {
                            setValue(`items.${index}.image`, {
                                id: guid,
                                url: download,
                            })
                        }}
                    />
                </Wrapper>
            )}
        </Draggable>
    )
}

export default ContentItem
