import React from 'react'
import { useTranslation } from 'react-i18next'

import FormItem from 'js/components/Form/FormItem'
import Spacer from 'js/components/Spacer/Spacer'

interface Props {
    guid: string
    control: any
}

const General = ({ guid, control }: Props) => {
    const { t } = useTranslation()

    const heightOptions = [
        { value: 'sm', label: t('size.sm') },
        { value: 'md', label: t('size.md') },
        { value: 'lg', label: t('size.lg') },
    ]

    return (
        <Spacer spacing="small">
            {/* @ts-ignore */}
            <FormItem
                control={control}
                type="text"
                name="title"
                id={`title-${guid}`}
                label={t('form.title')}
                helper={t('form.title-helper-empty')}
            />
            {/* @ts-ignore */}
            <FormItem
                control={control}
                type="radio"
                name="height"
                id={`height-${guid}`}
                title={t('widget-carousel.height')}
                options={heightOptions}
                size="small"
            />
        </Spacer>
    )
}

export default General
