import React, { useState } from 'react'
import { DragDropContext, Droppable } from 'react-beautiful-dnd'
import { useFieldArray, useWatch } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { media } from 'helpers'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import { H4 } from 'js/components/Heading'
import Modal from 'js/components/Modal/Modal'
import InsertFromContent from 'js/page/Widget/components/components/InsertFromContent/InsertFromContent'

import ContentItem from './ContentItem'

const Wrapper = styled.div`
    .content--items {
        ${media.mobileLandscapeDown`
            margin-left: -${(p) => p.theme.padding.horizontal.small};
            margin-right: -${(p) => p.theme.padding.horizontal.small};
        `};

        ${media.tabletUp`
            margin-left: -${(p) => p.theme.padding.horizontal.normal};
            margin-right: -${(p) => p.theme.padding.horizontal.normal};
        `};
    }
`

interface Props {
    guid: string
    control: any
    setValue: (string, boolean) => void
}

const Content = ({ guid, control, setValue }: Props) => {
    const { t } = useTranslation()

    const [showSearchModal, setShowSearchModal] = useState(false)

    const { fields, prepend, remove, move } = useFieldArray({
        control,
        name: 'items',
    })

    const onDragEnd = (result) => {
        if (!result.destination) return
        move(result.source.index, result.destination.index)
    }

    const handleInsert = (entities) => {
        const items = entities.map(({ title, url, featured }) => {
            return {
                title,
                url,
                image: featured?.image
                    ? {
                          id: featured?.image?.guid,
                          url: featured?.image?.download,
                      }
                    : null,
            }
        })
        prepend(items)
    }

    const watchItems = useWatch({
        name: 'items',
        control,
    })

    return (
        <Wrapper>
            <Flexer mt wrap>
                <Button
                    variant="primary"
                    onClick={() =>
                        prepend({
                            title: '',
                            url: '',
                            image: '',
                            buttonText: '',
                            isEditing: true,
                        })
                    }
                >
                    {t('action.add-item')}
                </Button>
                <span>{t('global.or')}</span>
                <Button
                    variant="secondary"
                    onClick={() => setShowSearchModal(true)}
                >
                    {t('action.insert-from-content')}..
                </Button>
                <Modal
                    size="normal"
                    isVisible={showSearchModal}
                    title={t('action.insert-from-content')}
                    onClose={() => setShowSearchModal(false)}
                    showCloseButton
                >
                    <InsertFromContent
                        // @ts-ignore
                        guid={guid}
                        onInsert={handleInsert}
                        onClose={() => setShowSearchModal(false)}
                    />
                </Modal>
            </Flexer>

            <H4 style={{ margin: '16px 0 8px' }}>
                {t('global.count-items', {
                    count: fields.length,
                })}
            </H4>

            <DragDropContext onDragEnd={onDragEnd}>
                <Droppable droppableId="itemlist">
                    {(provided) => (
                        <ul
                            className="content--items"
                            {...provided.droppableProps}
                            ref={provided.innerRef}
                        >
                            {fields.map(({ id }, index) => (
                                <ContentItem
                                    key={id}
                                    id={id}
                                    index={index}
                                    control={control}
                                    setValue={setValue}
                                    watchItems={watchItems}
                                    onRemove={() => remove(index)}
                                />
                            ))}
                            {provided.placeholder}
                        </ul>
                    )}
                </Droppable>
            </DragDropContext>
        </Wrapper>
    )
}

export default Content
