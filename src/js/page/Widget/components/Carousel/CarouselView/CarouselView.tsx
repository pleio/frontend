import React from 'react'
import { useTranslation } from 'react-i18next'
import styled, { css } from 'styled-components'

import Carousel from 'js/components/Carousel/Carousel'
import { H3 } from 'js/components/Heading'
import HideVisually from 'js/components/HideVisually/HideVisually'
import EditWidgetPlaceholder from 'js/page/Widget/components/components/EditWidgetPlaceholder'

import { Settings } from '../Carousel'

interface WrapperProps extends React.HTMLAttributes<HTMLDivElement> {
    $height: string
}

const Wrapper = styled.div<WrapperProps>`
    position: relative;

    .PageRowFullWidth.PageRowFirst &:first-child {
        margin-top: -40px;
    }

    .carousel {
        border-radius: 16px;
        overflow: hidden;

        ${(p) =>
            p.$height === 'sm' &&
            css`
                height: 330px;
            `};
        ${(p) =>
            p.$height === 'md' &&
            css`
                height: 430px;
            `};
        ${(p) =>
            p.$height === 'lg' &&
            css`
                height: 530px;
            `};

        .PageRowFullWidth & {
            width: auto;
            margin-left: -20px;
            margin-right: -20px;
            border-radius: 0;
        }
    }

    .carousel-view--title {
        margin-bottom: 12px;
    }
`

interface CarouselView {
    settings: Settings
    editMode: boolean
}

const CarouselView = ({ settings, editMode }: CarouselView) => {
    const { title, height, items } = settings

    const { t } = useTranslation()

    const widgetTitle = t('widget-carousel.title')

    if (!items || items?.length === 0) {
        if (!editMode) return null
        return <EditWidgetPlaceholder title={widgetTitle} />
    }

    return (
        <Wrapper $height={height}>
            {title ? (
                <H3 as="h2" className="carousel-view--title">
                    {title}
                </H3>
            ) : (
                <HideVisually as="h2">{widgetTitle}</HideVisually>
            )}
            <Carousel className="carousel" slides={items} />
        </Wrapper>
    )
}

export default CarouselView
