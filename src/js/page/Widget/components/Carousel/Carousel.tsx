import React, { forwardRef } from 'react'

import useGetSetting from 'js/page/Widget/hooks/useGetSetting'

import CarouselEdit from './CarouselEdit/CarouselEdit'
import CarouselView from './CarouselView/CarouselView'

export interface Settings {
    title: string
    height: string
    items: any[]
}

export interface WidgetSetting {
    key: string
    value: string
    links: any[]
}

interface Widget {
    settings?: WidgetSetting[]
}

interface Carousel {
    guid: string
    containerGuid?: string
    entity: Widget
    isEditing: boolean
    editMode: boolean
    onSave: (settings: WidgetSetting[]) => void
}

const Carousel = forwardRef(
    (
        { guid, containerGuid, entity, isEditing, editMode, onSave }: Carousel,
        ref,
    ) => {
        const getSetting = useGetSetting(entity)

        const getLinks = (key, defaultValue = []) => {
            const setting = entity?.settings?.find(
                (setting) => setting.key === key,
            )
            return setting ? setting.links : defaultValue
        }

        const settings = {
            title: getSetting('title', ''),
            height: getSetting('height', 'md'),
            items: getLinks('items'),
        }

        if (isEditing) {
            return (
                <CarouselEdit
                    ref={ref}
                    guid={guid}
                    containerGuid={containerGuid}
                    settings={settings}
                    onSave={onSave}
                />
            )
        }

        return <CarouselView settings={settings} editMode={editMode} />
    },
)

Carousel.displayName = 'Carousel'

export default Carousel
