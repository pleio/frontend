import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { Trans, useTranslation } from 'react-i18next'
import { Link, useLocation } from 'react-router-dom'
import { useMutation } from '@apollo/client'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import Card, { CardContent } from 'js/components/Card/Card'
import { ADDMUTATION } from 'js/components/EntityActions/EntityAddEditForm'
import DescriptionField from 'js/components/EntityActions/General/components/DescriptionField'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import { H3 } from 'js/components/Heading'
import Spacer from 'js/components/Spacer/Spacer'
import Text from 'js/components/Text/Text'
import JoinGroupButton from 'js/group/components/JoinGroupButton'

const Wrapper = styled(Card)`
    display: flex;
    flex-direction: column;
    overflow: hidden;

    .CallToActionText {
        white-space: pre-wrap;

        &:not(:first-child) {
            margin-top: 4px;
        }
    }
`

const CreateWidgetView = ({
    guid,
    viewer,
    group,
    site,
    settings,
    tags,
    tagCategories,
}) => {
    const location = useLocation()
    const { t } = useTranslation()

    const [addedEntity, setAddedEntity] = useState(null)

    const { title, description, subtype } = settings

    const {
        control,
        handleSubmit,
        reset,
        formState: { errors, isValid, isSubmitting },
    } = useForm({
        mode: 'onChange',
        defaultValues: { name: '', richDescription: null },
    })

    const [addEntity, { error: addEntityErrors }] = useMutation(ADDMUTATION)

    const onSubmit = async ({ title, richDescription }) => {
        const groupGuid = group?.guid

        const input = {
            ...(groupGuid ? { containerGuid: groupGuid } : {}),
            subtype,
            title,
            richDescription,
            tagCategories,
            tags,
            accessId: groupGuid
                ? group.defaultReadAccessId
                : site?.defaultReadAccessId,
        }

        await addEntity({
            variables: {
                input,
            },
            refetchQueries: ['ActivityList', 'WidgetObjects'],
        })
            .then(({ data }) => {
                setAddedEntity(data.addEntity?.entity)
                reset()
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    const onError = (errors) => {
        return new Promise((resolve, reject) => {
            reject(new Error(errors))
        })
    }

    const { loggedIn } = viewer
    const canCreate = group
        ? group.membership === 'joined'
        : viewer?.canWriteToContainer

    return (
        <Wrapper>
            <CardContent as="form" onSubmit={handleSubmit(onSubmit, onError)}>
                {addedEntity ? (
                    <>
                        {title && <H3 as="h2">{title}</H3>}

                        <Text className="CallToActionText">
                            <Trans i18nKey="widget-create.publish-success">
                                You succesfully published
                                <Link to={addedEntity.url}>
                                    {{ title: addedEntity.title }}
                                </Link>
                                !
                            </Trans>
                        </Text>

                        <Flexer mt>
                            <Button
                                size="normal"
                                variant="tertiary"
                                onClick={() => setAddedEntity(null)}
                            >
                                {t('action.go-back')}
                            </Button>
                        </Flexer>
                    </>
                ) : (
                    <>
                        <Spacer spacing="small">
                            {title && <H3 as="h2">{title}</H3>}

                            {description && (
                                <Text className="CallToActionText">
                                    {description}
                                </Text>
                            )}

                            {canCreate ? (
                                <>
                                    <FormItem
                                        control={control}
                                        type="text"
                                        name="title"
                                        id={`title-${guid}`}
                                        label={t('form.title')}
                                        required
                                        size="large"
                                    />

                                    <DescriptionField
                                        control={control}
                                        viewer={viewer}
                                        subtype={subtype}
                                        group={group}
                                        errors={errors}
                                        required
                                    />

                                    <Errors errors={addEntityErrors} />

                                    <Flexer>
                                        <Button
                                            size="normal"
                                            variant="primary"
                                            type="submit"
                                            disabled={!isValid}
                                            loading={isSubmitting}
                                        >
                                            {t('action.publish')}
                                        </Button>
                                    </Flexer>
                                </>
                            ) : group && group.membership !== 'joined' ? (
                                <div
                                    style={{
                                        display: 'flex',
                                        alignItems: 'center',
                                        flexDirection: 'column',
                                    }}
                                >
                                    <Text
                                        size="small"
                                        variant="grey"
                                        style={{ marginBottom: '4px' }}
                                    >
                                        {t('comments.message-not-a-member')}
                                    </Text>
                                    <JoinGroupButton
                                        entity={group}
                                        loggedIn={loggedIn}
                                        size="small"
                                    />
                                </div>
                            ) : !loggedIn ? (
                                <div
                                    style={{
                                        display: 'flex',
                                        alignItems: 'center',
                                        flexDirection: 'column',
                                    }}
                                >
                                    <Text
                                        size="small"
                                        variant="grey"
                                        style={{ marginBottom: '4px' }}
                                    >
                                        {t('widget-create.login-to-create')}
                                    </Text>
                                    <Button
                                        size="small"
                                        variant="primary"
                                        as={Link}
                                        to="/login"
                                        state={{
                                            next: location.pathname,
                                        }}
                                    >
                                        {t('action.login')}
                                    </Button>
                                </div>
                            ) : null}
                        </Spacer>
                    </>
                )}
            </CardContent>
        </Wrapper>
    )
}

export default CreateWidgetView
