import React, { forwardRef, useImperativeHandle, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { sanitizeTagCategories } from 'helpers'

import CustomTagsField from 'js/components/CustomTagsField'
import GroupGuidField from 'js/components/EntityActions/Advanced/components/GroupGuidField'
import FormItem from 'js/components/Form/FormItem'
import Spacer from 'js/components/Spacer/Spacer'
import TabMenu from 'js/components/TabMenu'
import TabPage from 'js/components/TabPage'
import TagCategoriesField from 'js/components/TagCategories/TagCategoriesField'
import WidgetViewPortal from 'js/page/Widget/components/components//WidgetViewPortal'
import convertObjectToWidgetSettings from 'js/page/Widget/helpers/convertObjectToWidgetSettings'
import useCustomTags from 'js/page/Widget/hooks/useCustomTags'

import CreateWidget from './CreateWidget'

const CreateWidgetEdit = forwardRef(
    (
        { guid, widgetGroup, site, settings, categoryTags, tags, onSave },
        ref,
    ) => {
        const { customTagsAllowed, tagCategories } = site

        const { t } = useTranslation()

        useImperativeHandle(ref, () => ({
            onSave() {
                submitForm()
            },
        }))

        const [tab, setTab] = useState('general')

        const [categoryTagsValue, setCategoryTagsValue] = useState(categoryTags)

        const {
            customTags,
            handleAddTags,
            handleRemoveTags,
            getParsedCustomTags,
        } = useCustomTags(tags)

        const [groupGuid, setGroupGuid] = useState(settings.groupGuid || null)

        const { control, handleSubmit, watch } = useForm({
            defaultValues: settings,
        })

        const watchValues = watch()

        const filterValues = ({ title, description, subtype }) => {
            return convertObjectToWidgetSettings({
                title,
                description,
                subtype,
                categoryTags: JSON.stringify(
                    sanitizeTagCategories(tagCategories, categoryTagsValue),
                ),
                tags: getParsedCustomTags(),
                groupGuid,
            })
        }

        const onSubmit = () => {
            onSave(filterValues(watchValues))
        }
        const submitForm = handleSubmit(onSubmit)

        const typeOptions = [
            {
                value: 'question',
                label: t('entity-question.content-name'),
            },
        ]

        return (
            <>
                <Spacer spacing="small">
                    <TabMenu
                        label={t('global.settings')}
                        options={[
                            {
                                onClick: () => setTab('general'),
                                label: t('global.general'),
                                isActive: tab === 'general',
                                id: 'tab-general',
                                ariaControls: 'tabpanel-general',
                            },
                            {
                                onClick: () => setTab('settings'),
                                label: t('global.settings'),
                                isActive: tab === 'settings',
                                id: 'tab-settings',
                                ariaControls: 'tabpanel-settings',
                            },
                        ]}
                        showBorder
                        edgePadding
                        edgeMargin
                    />

                    <TabPage
                        visible={tab === 'general'}
                        id={`tabpanel-general`}
                        ariaLabelledby={`tab-general`}
                    >
                        <Spacer spacing="small">
                            <FormItem
                                control={control}
                                type="text"
                                name="title"
                                id={`title-${guid}`}
                                label={t('form.title')}
                            />

                            <FormItem
                                control={control}
                                type="textarea"
                                name="description"
                                id={`description-${guid}`}
                                label={t('form.description')}
                                size="small"
                            />
                        </Spacer>
                    </TabPage>

                    <TabPage
                        visible={tab === 'settings'}
                        id={`tabpanel-settings`}
                        ariaLabelledby={`tab-settings`}
                    >
                        <Spacer spacing="small">
                            <p>{t('widget-create.settings-description')}</p>

                            <FormItem
                                control={control}
                                type="select"
                                label={t('global.type')}
                                name="subtype"
                                id={`${guid}-subtype`}
                                options={typeOptions}
                                value="question"
                            />

                            <TagCategoriesField
                                name="categoryTags"
                                label={t('form.tags')}
                                tagCategories={tagCategories}
                                value={categoryTagsValue}
                                setTags={setCategoryTagsValue}
                            />

                            {customTagsAllowed && (
                                <CustomTagsField
                                    name="customTags"
                                    value={customTags}
                                    label={t('global.custom-tags')}
                                    onAddTag={handleAddTags}
                                    onRemoveTag={handleRemoveTags}
                                />
                            )}

                            {!widgetGroup && (
                                <GroupGuidField
                                    value={groupGuid}
                                    onChange={setGroupGuid}
                                />
                            )}
                        </Spacer>
                    </TabPage>
                </Spacer>

                <WidgetViewPortal
                    Component={CreateWidget}
                    settings={filterValues(watchValues)}
                    guid={guid}
                    group={widgetGroup}
                />
            </>
        )
    },
)

CreateWidgetEdit.displayName = 'CreateWidgetEdit'

export default CreateWidgetEdit
