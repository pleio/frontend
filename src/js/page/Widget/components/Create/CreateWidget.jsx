import React, { forwardRef } from 'react'
import { gql, useQuery } from '@apollo/client'
import { hasJsonStructure, sanitizeTagCategories } from 'helpers'

import useGetSetting from 'js/page/Widget/hooks/useGetSetting'

import CreateWidgetEdit from './CreateWidgetEdit'
import CreateWidgetView from './CreateWidgetView'

const CreateWidget = forwardRef(
    ({ guid, group, entity, isEditing, onSave, editMode }, ref) => {
        const getSetting = useGetSetting(entity)

        const subtype = getSetting('subtype', 'question')
        const groupGuid = group?.guid || getSetting('groupGuid')
        const { data, loading } = useQuery(SITE_DATA, {
            variables: {
                getGroup: !!groupGuid,
                groupGuid,
                subtype,
            },
        })
        if (loading || !data) return null

        const { site, viewer, groupEntity } = data

        let customTags = getSetting('tags')
        customTags = customTags ? customTags.split(',') : []
        const categoryTags = hasJsonStructure(getSetting('categoryTags'))
            ? JSON.parse(getSetting('categoryTags'))
            : []

        const settings = {
            title: getSetting('title'),
            description: getSetting('description'),
            subtype,
            groupGuid: getSetting('groupGuid'),
        }

        if (isEditing) {
            return (
                <CreateWidgetEdit
                    ref={ref}
                    guid={guid}
                    widgetGroup={group}
                    site={site}
                    settings={settings}
                    categoryTags={categoryTags}
                    tags={customTags}
                    onSave={onSave}
                />
            )
        }

        const tags = site.customTagsAllowed ? customTags : []
        const tagCategories = sanitizeTagCategories(
            site.tagCategories,
            categoryTags,
        )

        return (
            <CreateWidgetView
                guid={guid}
                group={groupEntity}
                viewer={viewer}
                site={site}
                settings={settings}
                tags={tags}
                tagCategories={tagCategories}
                editMode={editMode}
            />
        )
    },
)

const SITE_DATA = gql`
    query CreateWidgetSite(
        $subtype: String
        $getGroup: Boolean!
        $groupGuid: String
    ) {
        site {
            guid
            customTagsAllowed
            tagCategories {
                name
                values
            }
            defaultReadAccessId
            entityFilter {
                contentTypes {
                    key
                    value
                }
            }
        }
        viewer {
            guid
            loggedIn
            canInsertMedia(subtype: $subtype)
            canWriteToContainer(subtype: $subtype)
        }
        groupEntity: entity(guid: $groupGuid) @include(if: $getGroup) {
            guid
            ... on Group {
                name
                defaultReadAccessId
                membership
                isMembershipOnRequest
                isJoinButtonVisible
            }
        }
    }
`

CreateWidget.displayName = 'CreateWidget'

export default CreateWidget
