import React, { Fragment, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigationType } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import {
    getLocalStorage,
    sanitizeTagCategories,
    setLocalStorage,
} from 'helpers'

import Card from 'js/activity/components/Card'
import normalizeContentTypes from 'js/activity/utils/normalizeContentTypes'
import blogListFragment from 'js/blog/fragments/listFragment'
import FetchMore from 'js/components/FetchMore'
import HideVisually from 'js/components/HideVisually/HideVisually'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import NoResultsMessage from 'js/components/NoResultsMessage/NoResultsMessage'
import discussionListFragment from 'js/discussions/fragments/listFragment'
import eventListFragment from 'js/events/fragments/listFragment'
import CreateStatusUpdate from 'js/group/StatusUpdate/Create'
import statusUpdateListFragment from 'js/group/StatusUpdate/fragments/listFragment.js'
import { listFragment as magazineIssueListFragment } from 'js/magazine-issue/lib/fragments'
import newsListFragment from 'js/news/fragments/listFragment'
import pageListFragment from 'js/page/listFragment'
import {
    episodeListFragment,
    podcastListFragment,
} from 'js/podcast/fragments/list'
import questionListFragment from 'js/questions/fragments/listFragment'
import wikiListFragment from 'js/wiki/fragments/listFragment'

import PinnedIcon from 'icons/pinned.svg'

import FeedFields from './FeedFields'

const FeedViewList = ({
    guid,
    site,
    viewer,
    width,
    containerGuid,
    inititalQueryLimit,
    colCount,
    settings,
    editMode,
    statusPublished,
    userGuid,
    canEditContainer,
    group,
    hideComments,
    hideLikes,
}) => {
    const {
        showEventFilter,
        showTypeFilter,
        showGroupFilter,
        showTagFilter,
        showTagCategories,
        sortingOptions,
        itemView,
        showDescription,
    } = settings

    const navigationType = useNavigationType()

    const [eventFilter, setEventFilter] = useState(null)

    const [typeFilter, setTypeFilter] = useState(settings.typeFilter)

    const [groupFilter, setGroupFilter] = useState('')

    const [tagFilter, setTagFilter] = useState([])

    const [sortBy, setSortBy] = useState(null)

    const [sortDirection, setSortDirection] = useState(null)

    const { t } = useTranslation()

    const [queryLimit, setQueryLimit] = useState(inititalQueryLimit)

    const groupGuid = group?.guid
    const sortPinned = !!groupGuid

    const variables = {
        containerGuid,
        subtypes: typeFilter,
        groupFilter,
        tagCategories: tagFilter,
        orderBy: eventFilter ? 'startDate' : sortBy,
        orderDirection: eventFilter
            ? eventFilter === 'upcoming'
                ? 'asc'
                : 'desc'
            : sortDirection,
        offset: 0,
        limit: queryLimit,
        sortPinned,
        statusPublished,
        userGuid,
        eventFilter,
    }

    const { loading, data, fetchMore, refetch } = useQuery(FEEDQUERY, {
        variables,
    })

    const tagCategoryFilters = showTagFilter
        ? site?.tagCategories
        : site?.tagCategories.filter(
              (category) => showTagCategories?.indexOf(category.name) > -1,
          )

    useEffect(() => {
        if (editMode) {
            setQueryLimit(inititalQueryLimit)
            setTimeout(() => refetch(), 0)
        }

        setFilters()

        if (navigationType !== 'POP') {
            setTimeout(() => refetch(), 0)
        }
    }, [settings])

    // Don't move these setLocalStorage calls, otherwise retrieving stored values will not work
    useEffect(() => {
        setLocalStorage(`${guid}-eventFilter`, eventFilter)
    }, [guid, eventFilter])

    useEffect(() => {
        setLocalStorage(`${guid}-typeFilter`, typeFilter)
    }, [guid, typeFilter])

    useEffect(() => {
        setLocalStorage(`${guid}-groupFilter`, groupFilter)
    }, [guid, groupFilter])

    useEffect(() => {
        setLocalStorage(`${guid}-tagFilter`, tagFilter)
    }, [guid, tagFilter])

    useEffect(() => {
        setLocalStorage(`${guid}-sortBy`, sortBy)
    }, [guid, sortBy])

    useEffect(() => {
        setLocalStorage(`${guid}-sortDirection`, sortDirection)
    }, [guid, sortDirection])

    const groupOptions = [
        ...(viewer.loggedIn
            ? [
                  {
                      value: 'mine',
                      label: t('filters.group-mine'),
                  },
              ]
            : []),
        {
            value: 'all',
            label: t('filters.group-all'),
        },
    ]

    const handleChangeSorting = (value) => {
        const [sortBy, sortDirection] = value.split('-')

        setSortBy(sortBy)
        setSortDirection(sortDirection)
    }

    const setFilters = () => {
        const eventFilterOptions = ['previous', 'upcoming']
        if (showEventFilter) {
            // Get eventFilter from local storage..
            const localStorageEventFilter = getLocalStorage(
                `${guid}-eventFilter`,
            )
            if (eventFilterOptions?.indexOf(localStorageEventFilter) > -1) {
                setEventFilter(localStorageEventFilter)
            } else {
                setEventFilter('upcoming')
            }
        } else {
            setEventFilter(null)
        }

        // Get typeFilter from local storage..
        const getTypeFilterStorage = !editMode && showTypeFilter
        const typeFilterStorage =
            getTypeFilterStorage && getLocalStorage(`${guid}-typeFilter`)
        if (getTypeFilterStorage && typeFilterStorage) {
            setTypeFilter(typeFilterStorage)
        } else {
            setTypeFilter(settings.typeFilter)
        }

        // Get groupFilter from local storage..
        const getGroupFilterStorage = !editMode && showGroupFilter
        const groupFilterStorage =
            getGroupFilterStorage && getLocalStorage(`${guid}-groupFilter`)
        if (
            getGroupFilterStorage &&
            (groupFilterStorage === '' ||
                groupOptions.findIndex(
                    (option) => option.value === groupFilterStorage,
                ) > -1)
        ) {
            setGroupFilter(groupFilterStorage)
        } else {
            setGroupFilter(settings.groupFilter)
        }

        // Get tagFilter from settings
        const newTagFilter = sanitizeTagCategories(
            site.tagCategories,
            settings.tagFilter,
        )

        // Get tagFilter from local storage..
        const getTagFilterStorage = !editMode && tagCategoryFilters.length > 0
        const tagFilterStorage =
            getTagFilterStorage && getLocalStorage(`${guid}-tagFilter`)

        if (tagFilterStorage) {
            // Filter by visible categories
            const sanitizedLocalStorageTagFilter = sanitizeTagCategories(
                tagCategoryFilters,
                tagFilterStorage,
                true,
            )
            sanitizedLocalStorageTagFilter.forEach((category) => {
                const categoryIndex = newTagFilter.findIndex(
                    (el) => el.name === category.name,
                )

                // Overwrite category if it already has saved values
                if (categoryIndex > -1) {
                    newTagFilter[categoryIndex].values = category.values
                } else {
                    newTagFilter.push(category)
                }
            })
        }
        setTagFilter(newTagFilter)

        const getSortByStorage = !editMode && sortingOptions?.length > 0
        // Get sortBy from local storage..
        const sortByStorage =
            getSortByStorage && getLocalStorage(`${guid}-sortBy`)
        const sortDirectionStorage =
            getSortByStorage && getLocalStorage(`${guid}-sortDirection`)
        const sortDirectionOptions = ['asc', 'desc']
        if (getSortByStorage && sortingOptions?.indexOf(sortByStorage) > -1) {
            setSortBy(sortByStorage)
            setSortDirection(
                sortDirectionOptions.indexOf(sortDirectionStorage) > -1
                    ? sortDirectionStorage
                    : 'desc',
            )
        } else if (sortingOptions?.length > 0) {
            // Check if saved filter exists in available options
            if (sortingOptions?.indexOf(settings.sortBy) > -1) {
                setSortBy(settings.sortBy)
                setSortDirection(settings.sortDirection)
            } else {
                setSortBy(sortingOptions[0])
                setSortDirection(sortingOptions[0] === 'title' ? 'asc' : 'desc')
            }
        } else {
            setSortBy(settings.sortBy)
            setSortDirection(settings.sortDirection)
        }
    }

    let lastPinned = false
    const hideSubtype = typeFilter && typeFilter.length === 1
    const hideExcerpt = showDescription !== undefined && !showDescription

    const getPinnedElement = (isPinned, i) => {
        const showPinnedEnd = !isPinned && lastPinned
        lastPinned = isPinned

        return (
            <>
                {sortPinned && isPinned && i === 0 && (
                    <div className="FeedPinnedStart">
                        <PinnedIcon />
                        {t('widget-feed.pinned')}
                    </div>
                )}
                {sortPinned && showPinnedEnd && (
                    <div className="FeedPinnedEnd" />
                )}
            </>
        )
    }

    return (
        <>
            <HideVisually as="h2">{t('widget-feed.title')}</HideVisually>

            {settings?.allowCreateStatusUpdate && (
                <CreateStatusUpdate group={group} />
            )}

            {site && (
                <FeedFields
                    name={`widget-${guid}`}
                    width={width}
                    eventFilterProps={
                        showEventFilter
                            ? {
                                  options: [
                                      {
                                          value: 'upcoming',
                                          label: t('filters.event-future'),
                                      },
                                      {
                                          value: 'previous',
                                          label: t('filters.event-past'),
                                      },
                                  ],
                                  value: eventFilter,
                                  onChange: setEventFilter,
                              }
                            : null
                    }
                    typeFilterProps={
                        showTypeFilter && typeFilter
                            ? {
                                  options: normalizeContentTypes(
                                      site.activityFilter.contentTypes,
                                      group ? ['page'] : null,
                                  ),
                                  value: typeFilter,
                                  onChange: setTypeFilter,
                              }
                            : null
                    }
                    groupFilterProps={
                        showGroupFilter
                            ? {
                                  options: groupOptions,
                                  value: groupFilter,
                                  onChange: setGroupFilter,
                              }
                            : null
                    }
                    tagFilterProps={
                        (showTagFilter || showTagCategories?.length > 0) &&
                        site.tagCategories.length > 0
                            ? {
                                  tagCategories: tagCategoryFilters,
                                  tagsPerCategory: tagFilter,
                                  onChange: setTagFilter,
                              }
                            : null
                    }
                    sortingProps={
                        !showEventFilter && sortingOptions?.length > 0
                            ? {
                                  options: [
                                      ...(sortingOptions.indexOf('lastAction') >
                                      -1
                                          ? [
                                                {
                                                    value: 'lastAction-desc',
                                                    label: t(
                                                        'filters.sorting-activity-new',
                                                    ),
                                                },
                                                {
                                                    value: 'lastAction-asc',
                                                    label: t(
                                                        'filters.sorting-activity-old',
                                                    ),
                                                },
                                            ]
                                          : []),
                                      ...(sortingOptions.indexOf(
                                          'timePublished',
                                      ) > -1
                                          ? [
                                                {
                                                    value: 'timePublished-desc',
                                                    label: t(
                                                        'filters.sorting-published-new',
                                                    ),
                                                },
                                                {
                                                    value: 'timePublished-asc',
                                                    label: t(
                                                        'filters.sorting-published-old',
                                                    ),
                                                },
                                            ]
                                          : []),
                                      ...(sortingOptions.indexOf('title') > -1
                                          ? [
                                                {
                                                    value: 'title-asc',
                                                    label: t(
                                                        'filters.sorting-title-new',
                                                    ),
                                                },
                                                {
                                                    value: 'title-desc',
                                                    label: t(
                                                        'filters.sorting-title-old',
                                                    ),
                                                },
                                            ]
                                          : []),
                                  ],
                                  sortBy: `${sortBy}-${sortDirection}`,
                                  onChangeSorting: handleChangeSorting,
                              }
                            : null
                    }
                    style={{ marginBottom: '16px' }}
                />
            )}

            {loading ? (
                <LoadingSpinner />
            ) : (
                data?.activities && (
                    <FetchMore
                        edges={data.activities.edges}
                        getMoreResults={(data) => data.activities.edges}
                        fetchMore={fetchMore}
                        fetchCount={inititalQueryLimit}
                        setLimit={setQueryLimit}
                        maxLimit={data.activities.total}
                        resultsMessage={t('global.count-items', {
                            count: data.activities.edges.length,
                        })}
                        noResults={
                            <NoResultsMessage
                                title={t('widget-feed.no-results')}
                                subtitle={t('widget-feed.no-results-helper')}
                            />
                        }
                    >
                        <ul>
                            {itemView === 'tile' ? (
                                <li className="TileWrapper">
                                    {data.activities.edges.map((entity, i) => (
                                        <Fragment key={entity.guid}>
                                            {getPinnedElement(
                                                entity.entity.isPinned,
                                                i,
                                            )}
                                            <div
                                                className="FeedItemWrapperCol"
                                                style={{
                                                    width: 100 / colCount + '%',
                                                }}
                                            >
                                                <Card
                                                    data-feed={i}
                                                    entity={entity}
                                                    viewer={viewer}
                                                    hideGroup={!!containerGuid}
                                                    hideSubtype={hideSubtype}
                                                    hideExcerpt={hideExcerpt}
                                                    canPin={
                                                        sortPinned &&
                                                        canEditContainer
                                                    }
                                                    hideComments={hideComments}
                                                    hideLikes={hideLikes}
                                                />
                                            </div>
                                        </Fragment>
                                    ))}
                                </li>
                            ) : (
                                data.activities.edges.map((entity, i) => (
                                    <li key={entity.guid}>
                                        {getPinnedElement(
                                            entity.entity.isPinned,
                                            i,
                                        )}
                                        <Card
                                            data-feed={i}
                                            entity={entity}
                                            viewer={viewer}
                                            hideGroup={!!containerGuid}
                                            hideSubtype={hideSubtype}
                                            hideExcerpt={hideExcerpt}
                                            canPin={
                                                sortPinned && canEditContainer
                                            }
                                            hideComments={hideComments}
                                            hideLikes={hideLikes}
                                        />
                                    </li>
                                ))
                            )}
                        </ul>
                    </FetchMore>
                )
            )}
        </>
    )
}

const FEEDQUERY = gql`
    query ActivityList(
        $containerGuid: String
        $offset: Int!
        $limit: Int!
        $subtypes: [String!]
        $groupFilter: [String!]
        $tagCategories: [TagCategoryInput!]
        $orderBy: OrderBy
        $orderDirection: OrderDirection
        $sortPinned: Boolean
        $statusPublished: [StatusPublished]
        $userGuid: String
        $eventFilter: EventFilter
    ) {
        activities(
            offset: $offset
            limit: $limit
            containerGuid: $containerGuid
            tagCategories: $tagCategories
            subtypes: $subtypes
            groupFilter: $groupFilter
            orderBy: $orderBy
            orderDirection: $orderDirection
            sortPinned: $sortPinned
            statusPublished: $statusPublished
            userGuid: $userGuid
            eventFilter: $eventFilter
        ) {
            total
            edges {
                guid
                type
                entity {
                    guid
                    ...StatusUpdateListFragment
                    ...BlogListFragment
                    ${podcastListFragment}
                    ${episodeListFragment}
                    ...DiscussionListFragment
                    ...EventListFragment
                    ...NewsListFragment
                    ...QuestionListFragment
                    ...WikiListFragment
                    ...PageListFragment
                    ${magazineIssueListFragment}
                }
            }
        }
    }
    ${statusUpdateListFragment}
    ${blogListFragment}
    ${discussionListFragment}
    ${eventListFragment}
    ${newsListFragment}
    ${questionListFragment}
    ${wikiListFragment}
    ${pageListFragment}
`

export default FeedViewList
