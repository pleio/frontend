import { produce } from 'immer'

/*
Reusable function to set a tag filter state. It adds/removes categories and tags when needed.
*/

interface TagCategory {
    name: string
    values: string[]
}

type Produce = (draft: TagCategory[]) => TagCategory[]

export default (
    setTagFilter: (tagCategories: Produce) => void,
    categoryName: string,
    tags: string[],
    allowEmptyValues: boolean,
) => {
    setTagFilter(
        produce((draft: TagCategory[]) => {
            const category = draft.find((cat) => cat.name === categoryName)
            if (category) {
                if (allowEmptyValues || tags.length > 0) {
                    category.values = tags
                } else {
                    const categoryIndex = draft.findIndex(
                        (cat) => cat.name === categoryName,
                    )
                    draft.splice(categoryIndex, 1)
                }
            } else {
                draft.push({
                    name: categoryName,
                    values: tags,
                })
            }
        }),
    )
}
