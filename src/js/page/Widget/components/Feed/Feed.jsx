import React, { forwardRef } from 'react'

import useGetSetting from 'js/page/Widget/hooks/useGetSetting'

import FeedEdit from './FeedEdit/FeedEdit'
import FeedView from './FeedView'

const Feed = forwardRef(
    (
        {
            guid,
            site,
            entity,
            canEditContainer,
            group,
            onSave,
            editMode,
            isEditing,
        },
        ref,
    ) => {
        const getSetting = useGetSetting(entity)

        const settings = {
            showTagFilter: getSetting('showTagFilter') === '1', // Show all tag category filters
            showTagCategories: getSetting('showTagCategories') // Show specific tag category filters
                ? JSON.parse(getSetting('showTagCategories'))
                : [],
            showTypeFilter: getSetting('showTypeFilter') === '1',
            showGroupFilter: getSetting('showGroupFilter') === '1',
            showEventFilter: getSetting('showEventFilter') === '1',
            sortingOptions: getSetting('sortingOptions')
                ? JSON.parse(getSetting('sortingOptions'))
                : [],
            tagFilter: getSetting('tagFilter')
                ? JSON.parse(getSetting('tagFilter'))
                : [],
            typeFilter: getSetting('typeFilter')
                ? JSON.parse(getSetting('typeFilter'))
                : [],
            groupFilter: getSetting('groupFilter') || '',
            sortBy: getSetting('sortBy') || 'lastAction',
            sortDirection: getSetting('sortDirection') || 'desc',
            itemView: getSetting('itemView') || 'list',
            itemCount: parseInt(getSetting('itemCount') || 5, 10),
            showDescription: getSetting('showDescription', 'true') === 'true',
            allowCreateStatusUpdate:
                getSetting('allowCreateStatusUpdate') === 'true',
        }

        if (isEditing) {
            return (
                <FeedEdit
                    ref={ref}
                    guid={guid}
                    site={site}
                    settings={settings}
                    group={group}
                    onSave={onSave}
                    canEditContainer={canEditContainer}
                />
            )
        }

        return (
            <FeedView
                guid={guid}
                settings={settings}
                editMode={editMode}
                containerGuid={group?.guid}
                group={group}
                canEditContainer={canEditContainer}
            />
        )
    },
)

Feed.displayName = 'Feed'

export default Feed
