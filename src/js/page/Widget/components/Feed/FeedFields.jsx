import React from 'react'
import { useTranslation } from 'react-i18next'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import FilterWrapper from 'js/components/FilterWrapper'
import Select from 'js/components/Select/Select'
import setTagFilter from 'js/page/Widget/components/Feed/setTagFilter'

const Wrapper = styled(FilterWrapper)`
    .SortingDivider {
        display: none;
        margin: 0 !important;

        &:nth-child(n + ${(p) => p.$colWidth + 1}) {
            display: block;
            order: -1;
            width: 100% !important;

            + .SortingColumn {
                order: -2;
            }
        }
    }

    .SortingColumn {
        margin-left: auto;
    }
`

const FeedFields = ({
    name,
    width,
    sortingProps,
    eventFilterProps,
    typeFilterProps,
    groupFilterProps,
    tagFilterProps,
    ...rest
}) => {
    const { t } = useTranslation()

    const tagCategories = tagFilterProps?.tagCategories

    if (
        (!!tagFilterProps && !tagCategories) ||
        (!eventFilterProps &&
            !typeFilterProps &&
            !groupFilterProps &&
            !tagFilterProps &&
            !sortingProps)
    )
        return null

    let colWidth = 1
    if (width >= 300 && width < 490) colWidth = 2
    else if (width >= 490 && width < 660) colWidth = 3
    else if (width >= 660) colWidth = 4

    const typeName = name ? `${name}-typeFilter` : 'typeFilter'
    const groupName = name ? `${name}-groupFilter` : 'groupFilter'
    const sortName = name ? `${name}-sortBy` : 'sortBy'

    return (
        <Wrapper $colWidth={colWidth} {...rest}>
            {!!eventFilterProps && (
                <Select
                    name="filter"
                    label={t('filters.sorting')}
                    options={eventFilterProps.options}
                    onChange={eventFilterProps.onChange}
                    value={eventFilterProps.value}
                />
            )}

            {!!typeFilterProps && (
                <Select
                    label={t('global.type')}
                    name={typeName}
                    placeholder={t('filters.filter-by')}
                    options={typeFilterProps.options}
                    value={typeFilterProps.value}
                    onChange={typeFilterProps.onChange}
                    isMulti
                />
            )}

            {!!groupFilterProps && (
                <Select
                    label={t('filters.group-title')}
                    name={groupName}
                    placeholder={t('filters.filter-by')}
                    options={groupFilterProps.options}
                    value={groupFilterProps.value}
                    onChange={groupFilterProps.onChange}
                    isClearable
                />
            )}

            {tagFilterProps &&
                tagCategories.map((category, index) => {
                    const options = category.values.map((tag) => ({
                        value: tag,
                        label: tag,
                    }))

                    const categoryName = category.name
                    const value =
                        tagFilterProps.tagsPerCategory.find(
                            (cat) => cat.name === categoryName,
                        )?.values || []

                    const handleChange = (tags) => {
                        setTagFilter(
                            tagFilterProps.onChange,
                            categoryName,
                            tags,
                            true,
                        )
                    }

                    const id = `${name || 'feed'}-tag-category-${index}`

                    return (
                        <Select
                            key={id}
                            name={id}
                            label={category.name}
                            placeholder={t('filters.filter-by')}
                            options={options}
                            value={value}
                            onChange={handleChange}
                            isMulti
                        />
                    )
                })}

            {sortingProps && <div className="SortingDivider" />}
            {sortingProps && (
                <div className="SortingColumn">
                    <div>
                        <Select
                            label={t('filters.sorting')}
                            name={sortName}
                            options={sortingProps.options}
                            value={sortingProps.sortBy}
                            onChange={sortingProps.onChangeSorting}
                        />
                    </div>
                </div>
            )}
        </Wrapper>
    )
}

FeedFields.propTypes = {
    typeFilterProps: PropTypes.shape({
        options: PropTypes.array.isRequired,
        value: PropTypes.array.isRequired,
        onChange: PropTypes.func.isRequired,
    }),
    groupFilterProps: PropTypes.shape({
        options: PropTypes.array.isRequired,
        value: PropTypes.string.isRequired,
        onChange: PropTypes.func.isRequired,
    }),
    tagFilterProps: PropTypes.shape({
        tagCategories: PropTypes.array,
        tagsPerCategory: PropTypes.array.isRequired,
        onChange: PropTypes.func.isRequired,
    }),
    sortingProps: PropTypes.shape({
        options: PropTypes.array.isRequired,
        sortBy: PropTypes.string.isRequired,
        onChangeSorting: PropTypes.func.isRequired,
    }),
}

export default FeedFields
