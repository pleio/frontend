/*
    Make sure settings object is populated before rendering FeedView.
    If not, values in the localStorage can be overwritten by incorrect ones.
*/

import React from 'react'
import { useMeasure } from 'react-use'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import styled from 'styled-components'

import FeedViewList from './FeedViewList'

const Wrapper = styled.div`
    .TileWrapper {
        display: flex;
        flex-wrap: wrap;
        margin-left: -10px;
        margin-right: -10px;

        > .FeedItemWrapperCol {
            display: flex;
            flex-direction: column;
            padding: 0 10px;

            > * {
                flex-grow: 1;
            }
        }
    }

    .FeedPinnedStart {
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
        margin-bottom: 12px;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};

        svg {
            margin-right: 8px;
        }
    }

    .FeedPinnedEnd {
        position: relative;
        width: 100%;
        height: 1px;
        padding: 4px 0 24px 0;

        &:before {
            content: '';
            width: 96px;
            position: absolute;
            left: 0;
            right: 0;
            bottom: 24px;
            height: 1px;
            margin: 0 auto;
            background-color: ${(p) => p.theme.color.grey[40]};
        }
    }
`

const FeedView = ({
    data: siteData,
    guid,
    containerGuid,
    settings,
    editMode,
    statusPublished,
    userGuid,
    canEditContainer,
    group,
    children,
    ...rest
}) => {
    const { site, viewer } = siteData

    const [ref, { width }] = useMeasure()

    if (!viewer || !site) return null

    const { itemView, itemCount } = settings

    let colCount
    if (itemView === 'tile' && width) {
        if (width >= 600 && width < 900) colCount = 2
        else if (width >= 900) colCount = 3
        else colCount = 1
    }

    // Number of (rows of) items to be displayed initially
    const inititalQueryLimit =
        itemView === 'tile' ? colCount * itemCount : itemCount

    return (
        // div is needed because wrapper gets flex-grow
        <Wrapper ref={ref} {...rest}>
            {/* Load list when initial limit is calculated */}
            {inititalQueryLimit && (
                <FeedViewList
                    guid={guid}
                    site={site}
                    viewer={viewer}
                    containerGuid={containerGuid}
                    inititalQueryLimit={inititalQueryLimit}
                    colCount={colCount}
                    settings={settings}
                    editMode={editMode}
                    statusPublished={statusPublished}
                    userGuid={userGuid}
                    canEditContainer={canEditContainer}
                    group={group}
                    width={width}
                    hideComments={settings.hideComments}
                    hideLikes={settings.hideLikes}
                />
            )}
            {children}
        </Wrapper>
    )
}

const SITEQUERY = gql`
    query ActivityListSite {
        viewer {
            guid
            loggedIn
            user {
                guid
            }
        }
        site {
            guid
            activityFilter {
                contentTypes {
                    key
                    value
                }
            }
            tagCategories {
                name
                values
            }
        }
    }
`
export default graphql(SITEQUERY)(FeedView)
