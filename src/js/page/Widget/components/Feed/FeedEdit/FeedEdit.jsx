import React, { forwardRef, useImperativeHandle, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import FormItem from 'js/components/Form/FormItem'
import Select from 'js/components/Select/Select'
import Setting from 'js/components/Setting'
import Spacer from 'js/components/Spacer/Spacer'
import TabMenu from 'js/components/TabMenu'
import TabPage from 'js/components/TabPage'
import WidgetViewPortal from 'js/page/Widget/components/components//WidgetViewPortal'
import convertObjectToWidgetSettings from 'js/page/Widget/helpers/convertObjectToWidgetSettings'

import Feed from '../Feed'

import Filters from './components/Filters'

const FeedEdit = forwardRef(
    ({ guid, site, settings, group, canEditContainer, onSave }, ref) => {
        const { t } = useTranslation()

        useImperativeHandle(ref, () => ({
            onSave() {
                submitForm()
            },
        }))

        const [tab, setTab] = useState('general')

        const [tagFilter, setTagFilter] = useState(settings.tagFilter)

        const [sortBy, setSortBy] = useState(settings.sortBy)
        const [sortDirection, setSortDirection] = useState(
            settings.sortDirection,
        )

        const handleChangeSorting = (value) => {
            const [sortBy, sortDirection] = value.split('-')

            setSortBy(sortBy)
            setSortDirection(sortDirection)
        }

        const { control, handleSubmit, watch, setValue } = useForm({
            mode: 'onChange',
            defaultValues: settings,
        })
        const watchValues = watch()

        const filterValues = ({
            showTypeFilter,
            showGroupFilter,
            showEventFilter,
            showTagFilter,
            showTagCategories,
            sortingOptions,
            itemView,
            itemCount,
            showDescription,
            typeFilter,
            groupFilter,
            allowCreateStatusUpdate,
        }) => {
            return convertObjectToWidgetSettings({
                showTypeFilter: showTypeFilter ? '1' : '0',
                showGroupFilter: showGroupFilter ? '1' : '0',
                showEventFilter: showEventFilter ? '1' : '0',
                showTagFilter: showTagFilter ? '1' : '0',
                showTagCategories: JSON.stringify(showTagCategories),
                sortingOptions: JSON.stringify(sortingOptions),
                tagFilter: JSON.stringify(tagFilter),
                typeFilter: JSON.stringify(typeFilter),
                groupFilter,
                sortBy,
                sortDirection,
                itemView,
                itemView,
                itemCount: itemCount.toString(),
                showDescription: showDescription.toString(),
                allowCreateStatusUpdate: allowCreateStatusUpdate.toString(),
            })
        }

        const onSubmit = () => {
            onSave(filterValues(watchValues))
        }
        const submitForm = handleSubmit(onSubmit)

        const inGroup = !!group

        return (
            <>
                <Spacer spacing="small">
                    <TabMenu
                        label={t('global.settings')}
                        options={[
                            {
                                onClick: () => setTab('general'),
                                label: t('global.general'),
                                isActive: tab === 'general',
                                id: 'tab-general',
                                ariaControls: 'tabpanel-general',
                            },
                            {
                                onClick: () => setTab('filters'),
                                label: t('global.filters'),
                                isActive: tab === 'filters',
                                id: 'tab-filters',
                                ariaControls: 'tabpanel-filters',
                            },
                            ...(watch('showEventFilter')
                                ? []
                                : [
                                      {
                                          onClick: () => setTab('sorting'),
                                          label: t('filters.sorting'),
                                          isActive: tab === 'sorting',
                                          id: 'tab-sorting',
                                          ariaControls: 'tabpanel-sorting',
                                      },
                                  ]),
                        ]}
                        edgePadding
                        edgeMargin
                        showBorder
                    />

                    <TabPage
                        id={`tabpanel-${tab}`}
                        ariaLabelledby={`tab-${tab}`}
                        visible
                    >
                        {tab === 'general' && (
                            <Spacer spacing="small">
                                <Setting
                                    subtitle={t('widget-feed.item-view')}
                                    helper={t('widget-feed.item-view-helper')}
                                    inputWidth="normal"
                                    htmlFor={`itemView-${guid}`}
                                >
                                    <FormItem
                                        control={control}
                                        type="select"
                                        name="itemView"
                                        id={`itemView-${guid}`}
                                        options={[
                                            {
                                                value: 'list',
                                                label: t(
                                                    'widget-feed.item-view-list',
                                                ),
                                            },
                                            {
                                                value: 'tile',
                                                label: t(
                                                    'widget-feed.item-view-tile',
                                                ),
                                            },
                                        ]}
                                        style={{ width: '100%' }}
                                    />
                                </Setting>

                                <Setting
                                    subtitle={t('widget-feed.number-of-items')}
                                    helper={
                                        watch('itemView') === 'tile'
                                            ? t(
                                                  'widget-feed.number-of-rows-helper',
                                              )
                                            : t(
                                                  'widget-feed.number-of-items-helper',
                                              )
                                    }
                                    inputWidth="small"
                                    htmlFor={`itemCount-${guid}`}
                                >
                                    <FormItem
                                        control={control}
                                        type="select"
                                        name="itemCount"
                                        id={`itemCount-${guid}`}
                                        options={[
                                            {
                                                value: 3,
                                                label: '3',
                                            },
                                            {
                                                value: 5,
                                                label: '5',
                                            },
                                            {
                                                value: 10,
                                                label: '10',
                                            },
                                        ]}
                                        style={{ width: '100%' }}
                                    />
                                </Setting>

                                <Setting
                                    subtitle={t('form.description')}
                                    helper={t(
                                        'widget-feed.show-description-helper',
                                    )}
                                    htmlFor={`showDescription-${guid}`}
                                >
                                    <FormItem
                                        control={control}
                                        type="switch"
                                        name="showDescription"
                                        id={`showDescription-${guid}`}
                                        size="normal"
                                    />
                                </Setting>

                                {inGroup && (
                                    <Setting
                                        subtitle={t(
                                            'widget-feed.show-status-update',
                                        )}
                                        helper={t(
                                            'widget-feed.show-status-update-helper',
                                        )}
                                        htmlFor={`allowCreateStatusUpdate-${guid}`}
                                    >
                                        <FormItem
                                            control={control}
                                            type="switch"
                                            name="allowCreateStatusUpdate"
                                            id={`allowCreateStatusUpdate-${guid}`}
                                            size="normal"
                                        />
                                    </Setting>
                                )}
                            </Spacer>
                        )}

                        {tab === 'filters' && (
                            <Filters
                                guid={guid}
                                site={site}
                                inGroup={inGroup}
                                control={control}
                                watch={watch}
                                setValue={setValue}
                                tagFilter={tagFilter}
                                setTagFilter={setTagFilter}
                            />
                        )}

                        {tab === 'sorting' && (
                            <Spacer spacing="small">
                                <Setting
                                    subtitle={t('global.default')}
                                    helper={t(
                                        'widget-feed.sorting-default-helper',
                                    )}
                                    inputWidth="normal"
                                    htmlFor={`sorting-${guid}`}
                                >
                                    <Select
                                        name={`sorting-${guid}`}
                                        options={[
                                            {
                                                value: 'lastAction-desc',
                                                label: t(
                                                    'filters.sorting-activity-new',
                                                ),
                                            },
                                            {
                                                value: 'lastAction-asc',
                                                label: t(
                                                    'filters.sorting-activity-old',
                                                ),
                                            },
                                            {
                                                value: 'timePublished-desc',
                                                label: t(
                                                    'filters.sorting-published-new',
                                                ),
                                            },
                                            {
                                                value: 'timePublished-asc',
                                                label: t(
                                                    'filters.sorting-published-old',
                                                ),
                                            },
                                            {
                                                value: 'title-asc',
                                                label: t(
                                                    'filters.sorting-title-new',
                                                ),
                                            },
                                            {
                                                value: 'title-desc',
                                                label: t(
                                                    'filters.sorting-title-old',
                                                ),
                                            },
                                        ]}
                                        value={`${sortBy}-${sortDirection}`}
                                        onChange={handleChangeSorting}
                                    />
                                </Setting>
                                <Setting
                                    subtitle={t('global.show')}
                                    helper={t(
                                        'widget-feed.sorting-show-helper',
                                    )}
                                    htmlFor={`sortingOptions-${guid}`}
                                    inputWidth="full"
                                >
                                    <FormItem
                                        type="checkboxes"
                                        options={[
                                            {
                                                value: 'lastAction',
                                                label: t(
                                                    'filters.sorting-activity',
                                                ),
                                            },
                                            {
                                                value: 'timePublished',
                                                label: t(
                                                    'filters.sorting-published',
                                                ),
                                            },
                                            {
                                                value: 'title',
                                                label: t(
                                                    'filters.sorting-title',
                                                ),
                                            },
                                        ]}
                                        control={control}
                                        name="sortingOptions"
                                        id={`sortingOptions-${guid}`}
                                        size="small"
                                        style={{
                                            marginTop: '-8px',
                                        }}
                                    />
                                </Setting>
                            </Spacer>
                        )}
                    </TabPage>
                </Spacer>

                <WidgetViewPortal
                    Component={Feed}
                    settings={filterValues(watchValues)}
                    guid={guid}
                    site={site}
                    group={group}
                    canEditContainer={canEditContainer}
                />
            </>
        )
    },
)

FeedEdit.displayName = 'FeedEdit'

export default FeedEdit
