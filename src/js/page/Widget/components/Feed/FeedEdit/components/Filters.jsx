import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'

import normalizeContentTypes from 'js/activity/utils/normalizeContentTypes'
import FormItem from 'js/components/Form/FormItem'
import { Col, Row } from 'js/components/Grid/Grid'
import Select from 'js/components/Select/Select'
import Setting from 'js/components/Setting'
import setTagFilterHelper from 'js/page/Widget/components/Feed/setTagFilter'

const Filters = ({
    guid,
    site,
    inGroup,
    control,
    setValue,
    watch,
    tagFilter,
    setTagFilter,
}) => {
    const { t } = useTranslation()

    const typeFilterValue = watch('typeFilter')

    useEffect(() => {
        if (
            watch('typeFilter').length !== 1 ||
            watch('typeFilter').indexOf('event') === -1
        ) {
            setValue('showEventFilter', false)
        }
    }, [typeFilterValue, watch, setValue])

    if (!Array.isArray(tagFilter)) return null

    return (
        <>
            <Setting
                subtitle={t('global.default')}
                helper={t('widget-feed.filters-default-helper')}
                inputWidth="full"
            >
                <Row $gutter={12} $spacing={12}>
                    <Col mobileUp={1 / 2}>
                        <FormItem
                            control={control}
                            type="select"
                            label={t('global.type')}
                            name="typeFilter"
                            id="typeFilter"
                            placeholder={t('filters.filter-by')}
                            options={normalizeContentTypes(
                                site.activityFilter.contentTypes,
                                inGroup ? ['page'] : null,
                            )}
                            isMulti
                        />
                    </Col>
                    {!inGroup && (
                        <Col mobileUp={1 / 2}>
                            <FormItem
                                control={control}
                                type="select"
                                label={t('filters.group-title')}
                                name="groupFilter"
                                id="groupFilter"
                                placeholder={t('filters.filter-by')}
                                options={[
                                    {
                                        value: 'mine',
                                        label: t('filters.group-mine'),
                                    },
                                    {
                                        value: 'all',
                                        label: t('filters.group-all'),
                                    },
                                ]}
                                isClearable
                            />
                        </Col>
                    )}
                    {site.tagCategories?.map((category, index) => {
                        const options = category.values.map((tag) => ({
                            value: tag,
                            label: tag,
                        }))

                        const categoryName = category.name

                        const value =
                            tagFilter.find((cat) => cat.name === categoryName)
                                ?.values || []

                        const handleChange = (tags) => {
                            setTagFilterHelper(setTagFilter, categoryName, tags)
                        }

                        const id = `${guid}-tag-category-${index}`

                        return (
                            <Col key={id} mobileUp={1 / 2}>
                                <Select
                                    name={id}
                                    label={category.name}
                                    placeholder={t('filters.filter-by')}
                                    options={options}
                                    value={value}
                                    onChange={handleChange}
                                    isMulti
                                />
                            </Col>
                        )
                    })}
                </Row>
            </Setting>

            <Setting
                subtitle={t('global.show')}
                helper={t('widget-feed.filters-show-helper')}
                inputWidth="full"
                style={{ marginTop: '16px' }}
            >
                <FormItem
                    control={control}
                    type="switch"
                    name="showTypeFilter"
                    id={`showTypeFilter-${guid}`}
                    label={t('global.type')}
                    size="small"
                    disabled={watch('showEventFilter')}
                    style={{
                        marginTop: '-8px',
                    }}
                />
                {!inGroup && (
                    <FormItem
                        control={control}
                        type="switch"
                        name="showGroupFilter"
                        id={`showGroupFilter-${guid}`}
                        label={t('filters.group-title')}
                        size="small"
                    />
                )}
                {watch('typeFilter').length === 1 &&
                    watch('typeFilter')[0] === 'event' && (
                        <FormItem
                            control={control}
                            type="switch"
                            name="showEventFilter"
                            id={`showEventFilter-${guid}`}
                            label={t('filters.event-date')}
                            size="small"
                            disabled={watch('showTypeFilter')}
                        />
                    )}

                {site.tagCategories.length > 0 && (
                    <>
                        <FormItem
                            control={control}
                            type="switch"
                            name="showTagFilter"
                            id={`showTagFilter-${guid}`}
                            label={t('widget-feed.all-tag-categories')}
                            size="small"
                        />

                        <FormItem
                            type="checkboxes"
                            options={site.tagCategories.map((el) => ({
                                label: el.name,
                                value: el.name,
                                disabled: watch('showTagFilter'),
                                checked: watch('showTagFilter'),
                            }))}
                            control={control}
                            name="showTagCategories"
                            id={`showTagCategories-${guid}`}
                            size="small"
                            style={{
                                marginLeft: '30px',
                            }}
                        />
                    </>
                )}
            </Setting>
        </>
    )
}

export default Filters
