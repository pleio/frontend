import React, { forwardRef, useImperativeHandle, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import FormItem from 'js/components/Form/FormItem'
import Spacer from 'js/components/Spacer/Spacer'
import TabMenu from 'js/components/TabMenu'
import TabPage from 'js/components/TabPage'
import WidgetViewPortal from 'js/page/Widget/components/components//WidgetViewPortal'
import ImageAttachmentField from 'js/page/Widget/components/components/ImageAttachmentField'
import convertObjectToWidgetSettings from 'js/page/Widget/helpers/convertObjectToWidgetSettings'

import CallToAction from './CallToAction'

const CallToActionEdit = forwardRef(({ guid, settings, onSave }, ref) => {
    const { t } = useTranslation()

    useImperativeHandle(ref, () => ({
        onSave() {
            submitForm()
        },
    }))

    const [tab, setTab] = useState('general')

    const [imageField, setImageField] = useState(undefined)

    const { control, handleSubmit, setValue, watch } = useForm({
        mode: 'onChange',
        defaultValues: settings,
    })
    const watchValues = watch()

    const filterValues = ({
        title,
        description,
        link,
        buttonText,
        image,
        imageAlt,
    }) => {
        return convertObjectToWidgetSettings({
            title,
            description: {
                richDescription: description,
            },
            link,
            buttonText,
            image: {
                attachment: imageField || image,
            },
            imageAlt,
        })
    }

    const onSubmit = () => {
        onSave(filterValues(watchValues))
    }
    const submitForm = handleSubmit(onSubmit)

    return (
        <>
            <Spacer spacing="small">
                <TabMenu
                    label={t('global.settings')}
                    options={[
                        {
                            onClick: () => setTab('general'),
                            label: t('global.general'),
                            isActive: tab === 'general',
                            id: 'tab-general',
                            ariaControls: 'tabpanel-general',
                        },
                        {
                            onClick: () => setTab('image'),
                            label: t('global.image'),
                            isActive: tab === 'image',
                            id: 'tab-image',
                            ariaControls: 'tabpanel-image',
                        },
                    ]}
                    showBorder
                    edgePadding
                    edgeMargin
                />

                <TabPage
                    id={`tabpanel-${tab}`}
                    aria-labelledby={`tab-${tab}`}
                    visible
                >
                    {tab === 'general' && (
                        <Spacer spacing="small">
                            <FormItem
                                control={control}
                                type="text"
                                name="title"
                                id={`title-${guid}`}
                                label={t('form.title')}
                            />
                            <FormItem
                                type="rich"
                                name="description"
                                size="small"
                                label={t('form.description')}
                                id={`description-${guid}`}
                                control={control}
                                options={{
                                    textStyle: true,
                                    textLink: true,
                                    textList: true,
                                }}
                            />
                            <FormItem
                                control={control}
                                type="text"
                                name="link"
                                id={`link-${guid}`}
                                label={t('form.link')}
                            />
                            <FormItem
                                control={control}
                                type="text"
                                name="buttonText"
                                id={`buttonText-${guid}`}
                                label={t('form.button-text')}
                            />
                        </Spacer>
                    )}

                    {tab === 'image' && (
                        <ImageAttachmentField
                            name={guid}
                            imageField={imageField}
                            setImageField={setImageField}
                            setValue={setValue}
                            watchValues={watchValues}
                            control={control}
                        />
                    )}
                </TabPage>
            </Spacer>

            <WidgetViewPortal
                Component={CallToAction}
                settings={filterValues(watchValues)}
                guid={guid}
            />
        </>
    )
})

CallToActionEdit.displayName = 'CallToActionEdit'

export default CallToActionEdit
