import React, { forwardRef } from 'react'

import useGetRichDescription from 'js/page/Widget/hooks/useGetRichDescription'
import useGetSetting from 'js/page/Widget/hooks/useGetSetting'

import CallToActionEdit from './CallToActionEdit'
import CallToActionView from './CallToActionView'

const CallToAction = forwardRef(
    ({ guid, entity, isEditing, onSave, editMode }, ref) => {
        const getSetting = useGetSetting(entity)
        const getRichDescription = useGetRichDescription(entity)

        const getAttachment = (key, defaultValue = '') => {
            const setting = entity.settings.find(
                (setting) => setting.key === key,
            )
            return setting ? setting.attachment : defaultValue
        }

        const settings = {
            title: getSetting('title'),
            description: getRichDescription('description'),
            link: getSetting('link'),
            buttonText: getSetting('buttonText'),
            image: getAttachment('image'),
            imageAlt: getSetting('imageAlt'),
        }

        if (isEditing) {
            return (
                <CallToActionEdit
                    ref={ref}
                    guid={guid}
                    settings={settings}
                    onSave={onSave}
                />
            )
        }

        return <CallToActionView settings={settings} editMode={editMode} />
    },
)

CallToAction.displayName = 'CallToAction'

export default CallToAction
