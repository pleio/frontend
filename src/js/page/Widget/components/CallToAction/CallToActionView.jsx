import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { getCleanUrl, isExternalUrl } from 'helpers'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import Card, { CardContent } from 'js/components/Card/Card'
import { H3 } from 'js/components/Heading'
import HideVisually from 'js/components/HideVisually/HideVisually'
import Img from 'js/components/Img/Img'
import TiptapView from 'js/components/Tiptap/TiptapView'
import EditWidgetPlaceholder from 'js/page/Widget/components/components/EditWidgetPlaceholder'

import ExternalIcon from 'icons/new-window-small.svg'

const Wrapper = styled(Card)`
    display: flex;
    flex-direction: column;
    overflow: hidden;

    .CallToActionImage {
        position: relative;

        &:not(:last-child) {
            margin-bottom: -4px;
        }
    }

    .CallToActionImageLink {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }

    .CallToActionText {
        white-space: pre-wrap;

        &:not(:first-child) {
            margin-top: 4px;
        }
    }

    .CallToActionButton:not(:first-child) {
        margin-top: 16px;
    }
`

const CallToActionView = ({ editMode, settings }) => {
    const { t } = useTranslation()

    const { title, description, link, buttonText, image, imageAlt } = settings

    if (!title && !description && !buttonText && !image) {
        if (editMode)
            return (
                <EditWidgetPlaceholder
                    title={t('widget-call-to-action.title')}
                />
            )
        return null
    }

    const url = getCleanUrl(link)
    const isExternal = isExternalUrl(url)
    const LinkElement = isExternal ? 'a' : Link

    return (
        <Wrapper>
            {image?.url && (
                <div className="CallToActionImage">
                    <Img src={image.url} alt={imageAlt} align="center" />
                    {link && (
                        <LinkElement
                            className="CallToActionImageLink"
                            to={!isExternal ? url : null}
                            href={isExternal ? url : null}
                            target={isExternal ? '_blank' : null}
                            rel={isExternal ? 'noopener noreferrer' : null}
                            aria-hidden
                        />
                    )}
                </div>
            )}
            {(title || description || (buttonText && link)) && (
                <CardContent>
                    {title && link && (
                        <H3 as="h2">
                            <LinkElement
                                to={!isExternal ? url : null}
                                href={isExternal ? url : null}
                                target={isExternal ? '_blank' : null}
                                rel={isExternal ? 'noopener noreferrer' : null}
                            >
                                {title}
                            </LinkElement>
                        </H3>
                    )}
                    {title && !link && <H3 as="h2">{title}</H3>}
                    {description && (
                        <TiptapView
                            className="CallToActionText"
                            content={description}
                        />
                    )}
                    {buttonText && link && (
                        <Button
                            as={LinkElement}
                            to={!isExternal ? url : null}
                            href={isExternal ? url : null}
                            target={isExternal ? '_blank' : null}
                            rel={isExternal ? 'noopener noreferrer' : null}
                            size="large"
                            variant="primary"
                            className="CallToActionButton"
                        >
                            {buttonText}
                            {isExternal ? (
                                <>
                                    <HideVisually>
                                        {t('global.opens-in-new-window')}
                                    </HideVisually>
                                    <ExternalIcon
                                        style={{
                                            flexShrink: 0,
                                            marginLeft: '8px',
                                        }}
                                    />
                                </>
                            ) : null}
                        </Button>
                    )}
                </CardContent>
            )}
        </Wrapper>
    )
}

export default CallToActionView
