import React, { forwardRef } from 'react'

import useGetSetting from 'js/page/Widget/hooks/useGetSetting'

import LeadEdit from './LeadEdit'
import LeadView from './LeadView'

const Lead = forwardRef(
    ({ guid, entity, onSave, editMode, isEditing }, ref) => {
        const getSetting = useGetSetting(entity)

        const getAttachment = (key, defaultValue = '') => {
            const setting = entity.settings.find(
                (setting) => setting.key === key,
            )
            return setting ? setting.attachment || setting.value : defaultValue
        }

        const settings = {
            title: getSetting('title'),
            poshorizontal: getSetting('poshorizontal') || '1',
            posvertical: getSetting('posvertical') || '1',
            link: getSetting('link'),
            linkText: getSetting('linkText'),
            image: getAttachment('image'),
            imageAlt: getSetting('imageAlt'),
        }

        if (isEditing) {
            return (
                <LeadEdit
                    ref={ref}
                    guid={guid}
                    settings={settings}
                    onSave={onSave}
                />
            )
        }

        return <LeadView settings={settings} editMode={editMode} />
    },
)

Lead.displayName = 'Lead'

export default Lead
