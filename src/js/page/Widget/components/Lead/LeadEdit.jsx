import React, { forwardRef, useImperativeHandle, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import FormItem from 'js/components/Form/FormItem'
import { Col, Row } from 'js/components/Grid/Grid'
import { H4 } from 'js/components/Heading'
import Spacer from 'js/components/Spacer/Spacer'
import TabMenu from 'js/components/TabMenu'
import TabPage from 'js/components/TabPage'
import WidgetViewPortal from 'js/page/Widget/components/components//WidgetViewPortal'
import ImageAttachmentField from 'js/page/Widget/components/components/ImageAttachmentField'
import convertObjectToWidgetSettings from 'js/page/Widget/helpers/convertObjectToWidgetSettings'

import Lead from './Lead'

const LeadEdit = forwardRef(({ guid, settings, onSave }, ref) => {
    const { t } = useTranslation()

    const [tab, setTab] = useState('general')

    const [imageField, setImageField] = useState(undefined)

    useImperativeHandle(ref, () => ({
        onSave() {
            submitForm()
        },
    }))

    const { control, handleSubmit, setValue, watch } = useForm({
        defaultValues: settings,
    })
    const watchValues = watch()

    const filterValues = ({
        image,
        imageAlt,
        title,
        link,
        linkText,
        poshorizontal,
        posvertical,
    }) => {
        return convertObjectToWidgetSettings({
            image: {
                attachment: imageField || image,
            },
            imageAlt,
            title,
            link,
            linkText,
            poshorizontal,
            posvertical,
        })
    }

    const onSubmit = () => {
        onSave(filterValues(watchValues))
    }
    const submitForm = handleSubmit(onSubmit)

    return (
        <>
            <Spacer spacing="small">
                <TabMenu
                    label={t('global.settings')}
                    options={[
                        {
                            onClick: () => setTab('general'),
                            label: t('global.general'),
                            isActive: tab === 'general',
                            id: 'tab-general',
                            ariaControls: 'tabpanel-general',
                        },
                        {
                            onClick: () => setTab('image'),
                            label: t('global.image'),
                            isActive: tab === 'image',
                            id: 'tab-image',
                            ariaControls: 'tabpanel-image',
                        },
                    ]}
                    showBorder
                    edgePadding
                    edgeMargin
                />

                <TabPage
                    id={`tabpanel-${tab}`}
                    ariaLabelledby={`tab-${tab}`}
                    visible
                >
                    {tab === 'general' && (
                        <Spacer spacing="small">
                            <FormItem
                                control={control}
                                type="text"
                                name="title"
                                label={t('form.title')}
                            />
                            <FormItem
                                control={control}
                                type="text"
                                name="link"
                                label={t('form.link')}
                                helper={t('widget-lead.link-helper')}
                            />
                            <FormItem
                                control={control}
                                type="text"
                                name="linkText"
                                label={t('widget-lead.link-text')}
                            />

                            <H4
                                style={{
                                    marginBottom: '8px',
                                }}
                            >
                                {t('widget-lead.text-position')}
                            </H4>
                            <Row $gutter={12} $spacing={16}>
                                <Col mobileLandscapeUp={1 / 2}>
                                    <FormItem
                                        control={control}
                                        type="select"
                                        name="poshorizontal"
                                        label={t('widget-lead.horizontal')}
                                        options={[
                                            {
                                                value: '0',
                                                label: t(
                                                    'widget-lead.horizontal-left',
                                                ),
                                            },
                                            {
                                                value: '1',
                                                label: t(
                                                    'widget-lead.horizontal-center',
                                                ),
                                            },
                                            {
                                                value: '2',
                                                label: t(
                                                    'widget-lead.horizontal-right',
                                                ),
                                            },
                                        ]}
                                    />
                                </Col>
                                <Col mobileLandscapeUp={1 / 2}>
                                    <FormItem
                                        control={control}
                                        type="select"
                                        name="posvertical"
                                        label={t('widget-lead.vertical')}
                                        options={[
                                            {
                                                value: '0',
                                                label: t(
                                                    'widget-lead.vertical-top',
                                                ),
                                            },
                                            {
                                                value: '1',
                                                label: t(
                                                    'widget-lead.vertical-middle',
                                                ),
                                            },
                                            {
                                                value: '2',
                                                label: t(
                                                    'widget-lead.vertical-bottom',
                                                ),
                                            },
                                        ]}
                                    />
                                </Col>
                            </Row>
                        </Spacer>
                    )}

                    {tab === 'image' && (
                        <ImageAttachmentField
                            name={guid}
                            imageField={imageField}
                            setImageField={setImageField}
                            setValue={setValue}
                            watchValues={watchValues}
                            control={control}
                        />
                    )}
                </TabPage>
            </Spacer>

            <WidgetViewPortal
                Component={Lead}
                settings={filterValues(watchValues)}
                guid={guid}
            />
        </>
    )
})

LeadEdit.displayName = 'LeadEdit'

export default LeadEdit
