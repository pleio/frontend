import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { useMeasure } from 'react-use'
import { getCleanUrl, isExternalUrl, media } from 'helpers'
import { transparentize } from 'polished'
import styled, { css } from 'styled-components'

import Card from 'js/components/Card/Card'
import { Container } from 'js/components/Grid/Grid'
import Img from 'js/components/Img/Img'
import EditWidgetPlaceholder from 'js/page/Widget/components/components/EditWidgetPlaceholder'

import ArrowRightIcon from 'icons/arrow-right-small.svg'

const Wrapper = styled(Card)`
    position: relative;
    overflow: hidden;
    background-color: transparent;

    .PageRowFullWidth.PageRowFirst &:first-child {
        margin-top: -40px;
    }

    .PageRowFullWidth & {
        width: auto;
        margin-left: -20px;
        margin-right: -20px;
        border-radius: 0;
    }

    .LeadImage {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;

        img {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            object-fit: cover;
        }
    }

    h2 {
        color: white;
        text-shadow:
            0 2px 5px ${() => transparentize(0.75, 'black')},
            0 0 1px ${() => transparentize(0.75, 'black')};
    }

    .LeadTitle {
        font-family: ${(p) => p.theme.fontHeading.family};
        font-weight: ${(p) => p.theme.fontHeading.weight.bold};

        ${(p) =>
            p.$fontSize === 'small' &&
            css`
                font-size: ${(p) => p.theme.fontHeading.size.normal};
                line-height: ${(p) => p.theme.fontHeading.lineHeight.normal};
            `}
        ${(p) =>
            p.$fontSize === 'normal' &&
            css`
                font-size: ${(p) => p.theme.fontHeading.size.large};
                line-height: ${(p) => p.theme.fontHeading.lineHeight.large};
            `}
        ${(p) =>
            p.$fontSize === 'large' &&
            css`
                font-size: ${(p) => p.theme.fontHeading.size.huge};
                line-height: ${(p) => p.theme.fontHeading.lineHeight.huge};
            `}
    }

    .LeadContent {
        height: 100%;

        ${media.mobilePortrait`
            min-height: 150px;
            padding-top: 20px;
            padding-bottom: 20px;
        `};

        ${media.mobileLandscapeUp`
            min-height: 330px;
            padding-top: 24px;
            padding-bottom: 24px;
        `};
    }

    .LeadReadMore {
        position: relative;
        margin-top: 4px;
        display: inline-flex;
        align-items: center;
        height: 28px;
        padding: 0 10px;
        border-radius: 16px;
        background-color: white;
        color: ${(p) => p.theme.color.primary.main};
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        font-weight: ${(p) => p.theme.font.weight.semibold};
        overflow: hidden;

        &:before {
            content: '';
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
        }
    }

    &:hover .LeadReadMore:before {
        background-color: ${(p) => p.theme.color.hover};
    }

    &:active .LeadReadMore:before {
        background-color: ${(p) => p.theme.color.active};
    }
`

const LeadView = ({ settings, editMode }) => {
    const [ref, { width }] = useMeasure()

    const {
        title,
        link,
        linkText,
        image,
        imageAlt,
        poshorizontal,
        posvertical,
    } = settings

    const textPositions = ['left', 'center', 'right']
    const flexPositions = ['flex-start', 'center', 'flex-end']

    const flexStyle = {
        position: 'relative', // Position content on top of image
        display: 'flex',
        flexDirection: 'row',
        justifyContent: flexPositions[poshorizontal],
        alignItems: flexPositions[posvertical],
        textAlign: textPositions[poshorizontal],
    }

    const { t } = useTranslation()

    const url = getCleanUrl(link)
    const isExternal = isExternalUrl(url)
    const LinkElement = isExternal ? 'a' : Link

    let fontSize = 'small'
    const sizeWidth = width
    if (sizeWidth > 550) fontSize = 'large'
    else if (sizeWidth > 350) fontSize = 'normal'
    else fontSize = 'small'

    if (!image) {
        if (!editMode) return null
        return <EditWidgetPlaceholder title={t('widget-lead.title')} />
    }

    return (
        <Wrapper
            ref={ref}
            as={link ? LinkElement : null}
            to={!isExternal ? url : null}
            href={isExternal ? url : null}
            target={isExternal ? '_blank' : null}
            rel={isExternal ? 'noopener noreferrer' : null}
            $fontSize={fontSize}
        >
            {/* If image is still an url (legacy) */}
            {typeof image === 'string' ? (
                <div className="LeadImage">
                    <img src={image} alt={imageAlt} />
                </div>
            ) : (
                <Img
                    src={image?.url}
                    alt={imageAlt}
                    objectFit="cover"
                    fullSize
                    className="LeadImage"
                />
            )}

            <Container className="LeadContent" style={flexStyle}>
                <div>
                    {!!title && <h2 className="LeadTitle">{title}</h2>}
                    {!!link && (
                        <div aria-hidden="true" className="LeadReadMore">
                            <ArrowRightIcon style={{ marginRight: '5px' }} />
                            {linkText || t('global.read-more')}
                        </div>
                    )}
                </div>
            </Container>
        </Wrapper>
    )
}

export default LeadView
