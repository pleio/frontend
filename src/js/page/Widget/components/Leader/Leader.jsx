import React, { forwardRef } from 'react'

import useGetSetting from 'js/page/Widget/hooks/useGetSetting'

import LeaderEdit from './LeaderEdit'
import LeaderView from './LeaderView'

const Leader = forwardRef(
    ({ guid, entity, isEditing, editMode, onSave }, ref) => {
        const getSetting = useGetSetting(entity)

        const getAttachment = (key, defaultValue = '') => {
            const setting = entity.settings.find(
                (setting) => setting.key === key,
            )
            return setting ? setting.attachment || setting.value : defaultValue
        }

        const settings = {
            image: getAttachment('leaderImage'),
            imageAlt: getSetting('imageAlt'),
        }

        if (isEditing) {
            return (
                <LeaderEdit
                    ref={ref}
                    guid={guid}
                    settings={settings}
                    onSave={onSave}
                />
            )
        } else {
            return (
                <LeaderView
                    guid={guid}
                    settings={settings}
                    editMode={editMode}
                />
            )
        }
    },
)

Leader.displayName = 'Leader'

export default Leader
