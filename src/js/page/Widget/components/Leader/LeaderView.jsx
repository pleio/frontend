import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import HideVisually from 'js/components/HideVisually/HideVisually'
import IconButton from 'js/components/IconButton/IconButton'
import Img from 'js/components/Img/Img'
import { createCookie, readCookie } from 'js/lib/cookies'
import EditWidgetPlaceholder from 'js/page/Widget/components/components/EditWidgetPlaceholder'

import CrossIcon from 'icons/cross.svg'

const Wrapper = styled.div`
    position: relative;

    .PageRowFullWidth.PageRowFirst &:first-child {
        margin-top: -40px;
    }

    .PageRowFullWidth & {
        width: auto;
        margin-left: -20px;
        margin-right: -20px;
        border-radius: 0;
    }

    .LeaderImage {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;

        img {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            object-fit: cover;
        }
    }
`

export const CloseLeaderButton = styled(IconButton)`
    position: absolute;
    top: 8px;
    right: 8px;
    opacity: 0;
    transition: opacity ${(p) => p.theme.transition.normal};
    border-radius: 50%;
    background: white;

    &:hover,
    &:focus {
        opacity: 1;
    }
`

const LeadView = ({ guid, settings, editMode }) => {
    const [isVisible, setIsVisible] = useState(false)

    useEffect(() => {
        if (editMode || (!editMode && !readCookie(`hideLeader-${guid}`))) {
            setIsVisible(true)
        }
    }, [editMode, guid])

    const onClose = () => {
        setIsVisible(false)
        createCookie(`hideLeader-${guid}`, '1', 365)
    }

    const { t } = useTranslation()
    const { image, imageAlt } = settings

    if (!isVisible || !image) {
        if (!editMode) return null
        return <EditWidgetPlaceholder title={t('widget-leader.title')} />
    }

    return (
        <Wrapper className="lead ___home">
            {/* If image is still an URL (legacy) */}
            {typeof image === 'string' ? (
                <div className="LeaderImage">
                    <img src={image} alt={imageAlt} />
                </div>
            ) : (
                <Img
                    src={image?.url}
                    alt={imageAlt}
                    objectFit="cover"
                    className="LeaderImage"
                    fullSize
                />
            )}

            <div className="lead__justify">
                <div className="container" />
            </div>
            {!editMode && (
                <CloseLeaderButton
                    variant="secondary"
                    size="large"
                    onClick={onClose}
                >
                    <CrossIcon />
                    <HideVisually>{t('widget-leader.close')}</HideVisually>
                </CloseLeaderButton>
            )}
        </Wrapper>
    )
}

export default LeadView
