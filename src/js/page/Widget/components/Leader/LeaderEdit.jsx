import React, { forwardRef, useImperativeHandle, useState } from 'react'
import { useForm } from 'react-hook-form'

import WidgetViewPortal from 'js/page/Widget/components/components//WidgetViewPortal'
import ImageAttachmentField from 'js/page/Widget/components/components/ImageAttachmentField'
import convertObjectToWidgetSettings from 'js/page/Widget/helpers/convertObjectToWidgetSettings'

import Leader from './Leader'

const LeaderEdit = forwardRef(({ guid, settings, onSave }, ref) => {
    const [imageField, setImageField] = useState(undefined)

    useImperativeHandle(ref, () => ({
        onSave() {
            submitForm()
        },
    }))

    const { control, handleSubmit, setValue, watch } = useForm({
        defaultValues: settings,
    })
    const watchValues = watch()

    const filterValues = ({ image, imageAlt }) => {
        return convertObjectToWidgetSettings({
            leaderImage: {
                attachment: imageField || image,
            },
            imageAlt,
        })
    }

    const onSubmit = () => {
        onSave(filterValues(watchValues))
    }
    const submitForm = handleSubmit(onSubmit)

    return (
        <>
            <ImageAttachmentField
                name={guid}
                imageField={imageField}
                setImageField={setImageField}
                setValue={setValue}
                watchValues={watchValues}
                control={control}
            />

            <WidgetViewPortal
                Component={Leader}
                settings={filterValues(watchValues)}
                guid={guid}
            />
        </>
    )
})

LeaderEdit.displayName = 'LeaderEdit'

export default LeaderEdit
