import React, { forwardRef } from 'react'
import { gql, useQuery } from '@apollo/client'
import { hasJsonStructure, sanitizeTagCategories } from 'helpers'

import useGetSetting from 'js/page/Widget/hooks/useGetSetting'

import FeaturedEdit from './FeaturedEdit'
import FeaturedView from './FeaturedView'

const Featured = forwardRef(
    ({ guid, entity, group, isEditing, editMode, onSave }, ref) => {
        const getSetting = useGetSetting(entity)

        const { data, loading } = useQuery(SITE_DATA)
        if (loading || !data) return null
        const { site } = data

        const subtypes = getSetting('subtypes')
        const settings = {
            title: getSetting('title'),
            subtypes: subtypes ? subtypes.split(',') : [],
            tags: site.customTagsAllowed
                ? getSetting('tags')
                    ? getSetting('tags').split(',')
                    : []
                : [],
            limit: parseInt(getSetting('limit', '6'), 10),
        }

        const categoryTags = hasJsonStructure(getSetting('categoryTags'))
            ? JSON.parse(getSetting('categoryTags'))
            : []

        if (isEditing) {
            settings.categoryTags = categoryTags

            return (
                <FeaturedEdit
                    ref={ref}
                    guid={guid}
                    site={site}
                    group={group}
                    settings={settings}
                    onSave={onSave}
                />
            )
        }

        settings.tagCategories = sanitizeTagCategories(
            site.tagCategories,
            categoryTags,
        )

        return (
            <FeaturedView
                site={site}
                settings={settings}
                editMode={editMode}
                containerGuid={group?.guid}
            />
        )
    },
)

Featured.displayName = 'Featured'

const SITE_DATA = gql`
    query FeaturedWidget {
        site {
            guid
            customTagsAllowed
            tagCategories {
                name
                values
            }
            entityFilter {
                contentTypes {
                    key
                    value
                }
            }
        }
    }
`

export default Featured
