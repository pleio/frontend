import React, { forwardRef, useImperativeHandle, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { sanitizeTagCategories, sortByProperty } from 'helpers'

import CustomTagsField from 'js/components/CustomTagsField'
import FormItem from 'js/components/Form/FormItem'
import Setting from 'js/components/Setting'
import Spacer from 'js/components/Spacer/Spacer'
import TagCategoriesField from 'js/components/TagCategories/TagCategoriesField'
import WidgetViewPortal from 'js/page/Widget/components/components//WidgetViewPortal'
import convertObjectToWidgetSettings from 'js/page/Widget/helpers/convertObjectToWidgetSettings'
import useCustomTags from 'js/page/Widget/hooks/useCustomTags'

import Featured from './Featured'

const FeaturedEdit = forwardRef(
    ({ guid, site, group, settings, onSave }, ref) => {
        useImperativeHandle(ref, () => ({
            onSave() {
                submitForm()
            },
        }))

        const { t } = useTranslation()

        const { customTagsAllowed, tagCategories } = site

        const { title, subtypes, limit, categoryTags, tags } = settings

        const types = [
            { value: 'event', label: t('entity-event.content-name') },
            { value: 'blog', label: t('entity-blog.content-name') },
            { value: 'discussion', label: t('entity-discussion.content-name') },
            { value: 'news', label: t('entity-news.content-name') },
            { value: 'question', label: t('entity-question.content-name') },
            { value: 'wiki', label: t('entity-wiki.content-name') },
            { value: 'podcast', label: t('entity-podcast.content-name') },
        ].map((type) => {
            type.checked = subtypes.indexOf(type.value) > -1
            return type
        })

        types.sort(sortByProperty('label'))

        const [categoryTagsValue, setCategoryTagsValue] = useState(categoryTags)

        const {
            customTags,
            handleAddTags,
            handleRemoveTags,
            getParsedCustomTags,
        } = useCustomTags(tags)

        const defaultValues = {
            title,
            subtypes,
            limit,
        }

        const { control, handleSubmit, watch } = useForm({
            defaultValues,
        })
        const watchValues = watch()

        const filterValues = ({ title, subtypes, limit }) => {
            return convertObjectToWidgetSettings({
                title,
                subtypes: subtypes.join(','),
                categoryTags: JSON.stringify(
                    sanitizeTagCategories(tagCategories, categoryTagsValue),
                ),
                tags: getParsedCustomTags(),
                limit: limit.toString(),
            })
        }

        const onSubmit = () => {
            onSave(filterValues(watchValues))
        }
        const submitForm = handleSubmit(onSubmit)

        return (
            <>
                <Spacer spacing="small">
                    <FormItem
                        control={control}
                        type="text"
                        name="title"
                        id={`title-${guid}`}
                        label={t('form.title')}
                    />

                    <FormItem
                        control={control}
                        type="checkboxes"
                        name="subtypes"
                        id={`subtypes-${guid}`}
                        size="small"
                        options={types.map(({ value, label }) => ({
                            value,
                            label,
                        }))}
                        title={t('widget-objects.filter-type')}
                        gridTemplateColumns="1fr 1fr"
                    />

                    <div
                        style={{
                            marginBottom: '24px',
                        }}
                    >
                        <TagCategoriesField
                            name="categoryTags"
                            label={t('widget-objects.filter-tag-categories')}
                            tagCategories={tagCategories}
                            value={categoryTagsValue}
                            setTags={setCategoryTagsValue}
                        />

                        {customTagsAllowed && (
                            <div
                                style={{
                                    marginTop: '16px',
                                }}
                            >
                                <CustomTagsField
                                    name="customTags"
                                    value={customTags}
                                    label={t('global.custom-tags')}
                                    onAddTag={handleAddTags}
                                    onRemoveTag={handleRemoveTags}
                                />
                            </div>
                        )}
                    </div>

                    <Setting
                        subtitle="widget.nr-of-items"
                        htmlFor="limit"
                        inputWidth="small"
                    >
                        <FormItem
                            type="number"
                            min={1}
                            max={6}
                            control={control}
                            name="limit"
                            label={t('form.limit')}
                        />
                    </Setting>
                </Spacer>

                <WidgetViewPortal
                    Component={Featured}
                    settings={filterValues(watchValues)}
                    guid={guid}
                    group={group}
                />
            </>
        )
    },
)

FeaturedEdit.displayName = 'FeaturedEdit'

export default FeaturedEdit
