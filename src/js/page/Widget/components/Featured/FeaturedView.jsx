import React, { useContext, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'

import Card from 'js/activity/components/Card'
import { H3 } from 'js/components/Heading'
import Slider from 'js/components/Slider/Slider'
import { featuredViewFragment } from 'js/lib/fragments/featured'
import globalStateContext from 'js/lib/withGlobalState/globalStateContext'
import EditWidgetPlaceholder from 'js/page/Widget/components/components/EditWidgetPlaceholder'

const FeaturedView = ({ editMode, settings, containerGuid, ...rest }) => {
    const { t } = useTranslation()

    const { globalState } = useContext(globalStateContext)

    const { title, subtypes, limit, tagCategories, tags } = settings

    const { data, stopPolling, startPolling } = useQuery(Query, {
        variables: {
            limit,
            subtypes,
            tags,
            tagCategories,
            containerGuid,
        },
        fetchPolicy: 'cache-and-network',
        skip: subtypes?.length === 0,
    })

    useEffect(() => {
        if (globalState.inactiveWindow || globalState.inactiveUser) {
            stopPolling()
        } else {
            startPolling(120000) // every 120 seconds
        }
    }, [globalState, stopPolling, startPolling])

    if (
        !data ||
        !data.entities ||
        data.entities.edges.length === 0 ||
        limit === 0
    ) {
        if (!editMode) return null
        return <EditWidgetPlaceholder title={t('widget-featured.title')} />
    }

    const { entities, viewer } = data

    return (
        <div {...rest}>
            {title && (
                <H3 as="h2" style={{ marginBottom: '12px', color: 'inherit' }}>
                    {title}
                </H3>
            )}
            <Slider>
                {entities.edges.map((entity, i) => {
                    return (
                        <Card
                            key={i}
                            data-feed={i}
                            entity={{ entity }}
                            viewer={viewer}
                            hideComments
                            hideLikes
                            hideGroup={!!containerGuid}
                            hideActions
                        />
                    )
                })}
            </Slider>
        </div>
    )
}

const Query = gql`
    query FeaturedList(
        $limit: Int!
        $subtypes: [String!]
        $tags: [String!]
        $tagCategories: [TagCategoryInput!]
        $containerGuid: String
    ) {
        viewer {
            guid
            user {
                guid
            }
        }
        entities(
            offset: 0
            limit: $limit
            subtypes: $subtypes
            tags: $tags
            tagCategories: $tagCategories
            containerGuid: $containerGuid
            isFeatured: true
        ) {
            total
            edges {
                guid
                ... on News {
                    title
                    localTitle
                    isTranslationEnabled
                    url
                    timePublished
                    statusPublished
                    isBookmarked
                    canBookmark
                    subtype
                    canEdit
                    ${featuredViewFragment}
                    group {
                        guid
                        ... on Group {
                            name
                            url
                        }
                    }
                }
                ... on Blog {
                    title
                    localTitle
                    isTranslationEnabled
                    url
                    timePublished
                    statusPublished
                    isBookmarked
                    canBookmark
                    subtype
                    canEdit
                    ${featuredViewFragment}
                    group {
                        guid
                        ... on Group {
                            name
                            url
                        }
                    }
                }
                ... on Discussion {
                    title
                    localTitle
                    isTranslationEnabled
                    url
                    timePublished
                    statusPublished
                    isBookmarked
                    canBookmark
                    subtype
                    canEdit
                    ${featuredViewFragment}
                    group {
                        guid
                        ... on Group {
                            name
                            url
                        }
                    }
                }
                ... on Question {
                    title
                    localTitle
                    isTranslationEnabled
                    url
                    timePublished
                    statusPublished
                    isBookmarked
                    canBookmark
                    subtype
                    canEdit
                    ${featuredViewFragment}
                    group {
                        guid
                        ... on Group {
                            name
                            url
                        }
                    }
                }
                ... on Event {
                    title
                    localTitle
                    isTranslationEnabled
                    url
                    timePublished
                    statusPublished
                    isBookmarked
                    canBookmark
                    subtype
                    canEdit
                    ${featuredViewFragment}
                    startDate
                    endDate
                    isAttending
                    attendees(
                        limit: 3
                        state: "accept"
                        orderBy: timeUpdated
                        orderDirection: desc
                    ) {
                        totalAccept
                        edges {
                            guid
                            icon
                            url
                            name
                            state
                        }
                    }
                    group {
                        guid
                        ... on Group {
                            name
                            url
                        }
                    }
                }
                ... on Wiki {
                    title
                    localTitle
                    isTranslationEnabled
                    url
                    timePublished
                    statusPublished
                    isBookmarked
                    canBookmark
                    canEdit
                    ${featuredViewFragment}
                    subtype
                    group {
                        guid
                        ... on Group {
                            name
                            url
                        }
                    }
                }
                ... on Podcast {
                    title
                    localTitle
                    isTranslationEnabled
                    url
                    timePublished
                    statusPublished
                    subtype
                    canEdit
                    group {
                        guid
                        ... on Group {
                            name
                            url
                        }
                    }
                }
            }
        }
    }
`

export default FeaturedView
