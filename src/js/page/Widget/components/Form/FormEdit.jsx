import React, { forwardRef, useImperativeHandle } from 'react'
import { useForm } from 'react-hook-form'
import { Trans, useTranslation } from 'react-i18next'

import FormItem from 'js/components/Form/FormItem'
import Spacer from 'js/components/Spacer/Spacer'
import Text from 'js/components/Text/Text'
import WidgetViewPortal from 'js/page/Widget/components/components//WidgetViewPortal'
import convertObjectToWidgetSettings from 'js/page/Widget/helpers/convertObjectToWidgetSettings'

import Form from './Form'

const FormEdit = forwardRef(
    ({ guid, settings, formsUrlBase, formsUrl, onSave }, ref) => {
        const { t } = useTranslation()

        useImperativeHandle(ref, () => ({
            onSave() {
                submitForm()
            },
        }))

        const { formId } = settings

        const defaultValues = {
            formId,
        }

        const { control, handleSubmit, watch } = useForm({
            defaultValues,
        })
        const watchValues = watch()

        const filterValues = ({ formId }) => {
            return convertObjectToWidgetSettings({
                formId,
            })
        }

        const onSubmit = () => {
            onSave(filterValues(watchValues))
        }
        const submitForm = handleSubmit(onSubmit)

        return (
            <>
                <Spacer spacing="normal">
                    <Text size="small">
                        <Trans i18nKey="widget-form.description">
                            Create a form at
                            <a
                                href={formsUrl}
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                {{ url: formsUrlBase }}
                            </a>{' '}
                            and use the form id to embed it here.
                        </Trans>
                    </Text>
                    <FormItem
                        control={control}
                        type="text"
                        name="formId"
                        id={`formId-${guid}`}
                        label={t('widget-form.form-id')}
                        helper={
                            <Trans i18nKey="widget-form.form-id-helper">
                                Open a form at
                                <a
                                    href={formsUrl}
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    {{ url: formsUrlBase }}
                                </a>
                                , copy the form id from the address bar (the
                                number after /forms/) and paste here.
                            </Trans>
                        }
                    />
                </Spacer>

                <WidgetViewPortal
                    Component={Form}
                    settings={filterValues(watchValues)}
                    guid={guid}
                />
            </>
        )
    },
)

FormEdit.displayName = 'FormEdit'

export default FormEdit
