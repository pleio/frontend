import React, { forwardRef, useImperativeHandle, useState } from 'react'
import { Trans, useTranslation } from 'react-i18next'

import Spacer from 'js/components/Spacer/Spacer'
import Text from 'js/components/Text/Text'
import Textfield from 'js/components/Textfield/Textfield'
import useGetSetting from 'js/page/Widget/hooks/useGetSetting'

import FormEdit from './FormEdit'
import FormView from './FormView'

const Form = forwardRef(
    ({ guid, site, entity, editMode, isEditing, onSave }, ref) => {
        const canCreateFormWidget = ['pro', 'pro_plus'].includes(
            site?.sitePlanType,
        )

        const getSetting = useGetSetting(entity)

        if (!canCreateFormWidget) return null

        const settings = {
            formId: getSetting('formId'),
        }

        const formsUrlBase = 'formulieren.pleio.nl'
        const formsUrl = `https://${formsUrlBase}/`

        if (isEditing) {
            return (
                <FormEdit
                    ref={ref}
                    guid={guid}
                    settings={settings}
                    onSave={onSave}
                    formsUrlBase={formsUrlBase}
                    formsUrl={formsUrl}
                />
            )
        }

        const formId = getSetting('formId')

        return (
            <FormView
                editMode={editMode}
                guid={guid}
                formId={formId}
                formsUrl={formsUrl}
            />
        )
    },
)

Form.displayName = 'Form'

export default Form
