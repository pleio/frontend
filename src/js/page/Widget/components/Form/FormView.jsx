import React, { useEffect, useState } from 'react'
import { Helmet } from 'react-helmet'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import Card from 'js/components/Card/Card'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import EditWidgetPlaceholder from 'js/page/Widget/components/components/EditWidgetPlaceholder'

const Wrapper = styled(Card)`
    position: relative;
    overflow: hidden;
    min-height: 100px;

    .FormViewLoading {
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        display: flex;
        justify-content: center;
        align-items: center;
        background-color: white;
        z-index: 1;
    }
`

const waitForGlobal = function (key, callback) {
    if (window[key]) {
        callback()
    } else {
        setTimeout(function () {
            waitForGlobal(key, callback)
        }, 100)
    }
}

const FormView = ({ guid, formId, formsUrl, editMode }) => {
    const { t } = useTranslation()

    const [loading, setLoading] = useState(false)

    const id = `form-${guid}-${formId}`

    useEffect(() => {
        if (!formId) return

        setLoading(true)

        waitForGlobal('forms', function () {
            const settings = {
                apiUrl: `${formsUrl}api/v1`,
                formId: `${formId}`,
            }

            const executeScript = () => {
                const formElement = document.getElementById(id)?.firstChild
                if (formElement) {
                    // eslint-disable-next-line @typescript-eslint/no-unused-vars
                    const form = new window.forms.Form(formElement, settings)
                }
            }

            executeScript()

            setLoading(false)
        })
    }, [id, formId])

    if (!formId) {
        if (!editMode) return null
        return <EditWidgetPlaceholder title={t('widget-form.title')} />
    }

    return (
        <Wrapper>
            <Helmet>
                <script
                    src={`${formsUrl}static/dist/chunk-vendors.js`}
                ></script>
                <link
                    href={`${formsUrl}static/dist/chunk-common.css`}
                    rel="stylesheet"
                />
                <script src={`${formsUrl}static/dist/chunk-common.js`}></script>
                <script src={`${formsUrl}static/dist/embed.js`}></script>
                <link
                    rel="stylesheet"
                    href="https://rsms.me/inter/inter.css"
                ></link>
            </Helmet>

            <div id={id}>
                <div />
            </div>

            {loading && (
                <div className="FormViewLoading">
                    <LoadingSpinner />
                </div>
            )}
        </Wrapper>
    )
}

export default FormView
