import React from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import { H4 } from 'js/components/Heading'

const Wrapper = styled.div`
    padding: 6px 8px 8px;
    min-height: 80px;
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
    border-radius: ${(p) => p.theme.radius.normal};

    .WidgetPlaceholderTitle {
        color: inherit;
    }

    .WidgetPlaceholderText {
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
    }
`

interface Props {
    title?: string
    helper?: string
}

const EditWidgetPlaceholder = ({ title, helper }: Props) => {
    const { t } = useTranslation()

    return (
        <Wrapper className="WidgetPlaceholder" /* Used in Row to set outline */>
            <div>
                <H4 as="h2" className="WidgetPlaceholderTitle">
                    {title || t('page.widget-placeholder-title')}
                </H4>
                <div className="WidgetPlaceholderText">
                    {helper || t('page.widget-placeholder-helper')}
                </div>
            </div>
        </Wrapper>
    )
}

export default EditWidgetPlaceholder
