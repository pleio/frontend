/*
This component is used in LinkList and ItemList widget.
*/

import React from 'react'
import styled, { css } from 'styled-components'

import Card from 'js/components/Card/Card'
import { H3 } from 'js/components/Heading'
import HideVisually from 'js/components/HideVisually/HideVisually'
import { media } from 'js/lib/helpers'

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;

    .ListWidgetTitle {
        ${(p) =>
            ['list', 'grid'].includes(p.$layout) &&
            css`
                ${media.mobileLandscapeDown`
                    padding: ${(p) =>
                        `${p.theme.padding.vertical.small} ${p.theme.padding.horizontal.small} 0`};
                `}

                ${media.tabletUp`
                    padding: ${(p) =>
                        `${p.theme.padding.vertical.normal} ${p.theme.padding.horizontal.normal} 0`};
                `};
            `};

        ${(p) =>
            p.$layout === 'grid' &&
            css`
                text-align: center;
            `};

        ${(p) =>
            ['carousel', 'feed'].includes(p.$layout) &&
            !p.$noResults &&
            css`
                margin-bottom: 12px;
            `};
    }
`

const ListWidgetWrapper = ({
    title,
    widgetTitle,
    layout,
    noResults,
    children,
    ...rest
}) => {
    return (
        <Wrapper
            as={['list', 'grid'].includes(layout) && Card}
            $layout={layout}
            $noResults={noResults}
            {...rest}
        >
            {title ? (
                <H3 as="h2" className="ListWidgetTitle">
                    {title}
                </H3>
            ) : (
                <HideVisually as="h2">{widgetTitle}</HideVisually>
            )}
            {children}
        </Wrapper>
    )
}

export default ListWidgetWrapper
