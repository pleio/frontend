import React from 'react'
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd'
import { useTranslation } from 'react-i18next'
import { produce } from 'immer'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import { H4 } from 'js/components/Heading'
import IconButton from 'js/components/IconButton/IconButton'
import hashString from 'js/lib/helpers/hashString'
import useSubtypes from 'js/lib/hooks/useSubtypes'

import CrossIcon from 'icons/cross.svg'
import MoveIcon from 'icons/move-vertical.svg'

import InsertFromContentHeader from './InsertFromContentHeader'

const Wrapper = styled.li`
    display: flex;
    background: white;
    border-bottom: 1px solid transparent;

    &:not(:last-child) {
        border-bottom-color: ${(p) => p.theme.color.grey[20]};
    }

    &[aria-current='dragging'] {
        border-bottom-color: transparent;
        box-shadow: ${(p) => p.theme.shadow.soft.center};
        border-radius: ${(p) => p.theme.radius.small};
    }

    .SearchResultsItemsText {
        margin-right: auto;
        padding: 9px 0;
        line-height: ${(p) => p.theme.font.lineHeight.small};
    }

    .SearchResultsItemsTitle {
        margin-right: 4px;
        font-size: ${(p) => p.theme.font.size.small};
    }

    .SearchResultsItemsSubtype {
        font-size: ${(p) => p.theme.font.size.tiny};
        color: ${(p) => p.theme.color.text.grey};
    }

    .SearchResultsItemsMove {
        display: flex;
        align-items: center;
        height: 40px;
        margin-right: 8px;
    }
`

const InsertFromContentItems = ({ items, setItems, onInsertLinks }) => {
    const { t } = useTranslation()

    const { subtypes } = useSubtypes()

    const onDragEnd = (result) => {
        if (!result.destination) return

        const sourcePosition = result.source.index
        const destinationPosition = result.destination.index

        setItems(
            produce((newState) => {
                const [movedItem] = newState.splice(sourcePosition, 1)
                newState.splice(destinationPosition, 0, movedItem)
            }),
        )
    }

    return (
        <>
            <InsertFromContentHeader>
                <H4>
                    {t('global.count-items', {
                        count: items.length,
                    })}
                </H4>
                {items.length > 0 && (
                    <Button
                        size="normal"
                        variant="primary"
                        onClick={onInsertLinks}
                        style={{
                            marginLeft: 'auto',
                        }}
                    >
                        {t('widget-linklist.insert-item', {
                            count: items.length,
                        })}
                    </Button>
                )}
            </InsertFromContentHeader>
            <DragDropContext onDragEnd={onDragEnd}>
                <Droppable droppableId="link-list-search-results-items">
                    {(provided) => (
                        <>
                            <ul ref={provided.innerRef}>
                                {items.map((entity, i) => {
                                    const { guid, title, subtype } = entity

                                    const removeItem = () => {
                                        setItems(
                                            produce((newState) => {
                                                newState.splice(i, 1)
                                            }),
                                        )
                                    }

                                    return (
                                        <Draggable
                                            key={hashString(
                                                `${guid}-draggable`,
                                            )}
                                            draggableId={guid}
                                            index={i}
                                        >
                                            {(provided, snapshot) => (
                                                <Wrapper
                                                    ref={provided.innerRef}
                                                    aria-current={
                                                        snapshot.isDragging
                                                            ? 'dragging'
                                                            : null
                                                    }
                                                    {...provided.draggableProps}
                                                    {...provided.dragHandleProps}
                                                >
                                                    <div className="SearchResultsItemsMove">
                                                        <MoveIcon />
                                                    </div>
                                                    <div className="SearchResultsItemsText">
                                                        <span className="SearchResultsItemsTitle">
                                                            {title}
                                                        </span>
                                                        {subtype && (
                                                            <span className="SearchResultsItemsSubtype">
                                                                {
                                                                    subtypes[
                                                                        subtype
                                                                    ]
                                                                        .contentName
                                                                }
                                                            </span>
                                                        )}
                                                    </div>
                                                    <IconButton
                                                        className="SearchResultsItemsRemove"
                                                        variant="secondary"
                                                        size="large"
                                                        onClick={removeItem}
                                                    >
                                                        <CrossIcon />
                                                    </IconButton>
                                                </Wrapper>
                                            )}
                                        </Draggable>
                                    )
                                })}
                            </ul>
                            {provided.placeholder}
                        </>
                    )}
                </Droppable>
            </DragDropContext>
        </>
    )
}

export default InsertFromContentItems
