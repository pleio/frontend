import React from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import IconButton from 'js/components/IconButton/IconButton'

import AddIcon from 'icons/add.svg'

const LI = styled.li`
    display: flex;
    align-items: stretch;

    &:not(:last-child) {
        border-bottom: 1px solid ${(p) => p.theme.color.grey[20]};
    }

    .LinkListResultsItemLink {
        flex-grow: 1;
        padding: 10px 8px 10px 0;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        color: ${(p) => p.theme.color.primary.main};

        &:hover .LinkListResultsItemTitle {
            text-decoration: underline;
        }
    }

    .LinkListResultsItemType {
        font-size: ${(p) => p.theme.font.size.tiny};
        line-height: ${(p) => p.theme.font.lineHeight.tiny};
        color: ${(p) => p.theme.color.text.grey};
        white-space: nowrap;
    }
`

const InsertFromContentSearchResult = ({ entity, onAddResult }) => {
    const { t } = useTranslation()

    const types = {
        Event: t('entity-event.content-name'),
        Blog: t('entity-blog.content-name'),
        Discussion: t('entity-discussion.content-name'),
        News: t('entity-news.content-name'),
        Page: t('global.page'),
        Question: t('entity-question.content-name'),
        Wiki: t('entity-wiki.content-name'),
        Folder: t('entity-file.folder'),
        File: t('entity-file.content-name'),
        Pad: t('entity-file.pad'),
    }

    return (
        <LI>
            <a
                className="LinkListResultsItemLink"
                href={entity.url}
                target="_blank"
                rel="noopener noreferrer"
            >
                <span className="LinkListResultsItemTitle">{entity.title}</span>{' '}
                <span className="LinkListResultsItemType">
                    {types[entity.__typename]}
                </span>
            </a>
            <IconButton
                size="large"
                hoverSize="normal"
                variant="secondary"
                aria-label={t('widget-linklist.add-item', {
                    title: entity.title,
                })}
                onClick={() => onAddResult(entity)}
                style={{ height: 'auto' }}
            >
                <AddIcon />
            </IconButton>
        </LI>
    )
}

export default InsertFromContentSearchResult
