import styled from 'styled-components'

export default styled.div`
    display: flex;
    align-items: center;
    background-color: white;
    overflow: hidden;
    border-bottom: 1px solid ${(p) => p.theme.color.grey[30]};
    min-height: 56px;
`
