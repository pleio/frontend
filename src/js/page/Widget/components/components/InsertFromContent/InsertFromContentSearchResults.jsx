import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'
import { useIsMount } from 'helpers'

import Button from 'js/components/Button/Button'
import FetchMore from 'js/components/FetchMore'
import { H4 } from 'js/components/Heading'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Text from 'js/components/Text/Text'
import { featuredViewFragment } from 'js/lib/fragments/featured'
import hashString from 'js/lib/helpers/hashString'

import InsertFromContentHeader from './InsertFromContentHeader'
import InsertFromContentSearchResult from './InsertFromContentSearchResult'

const InsertFromContentSearchResults = ({
    typeFilter,
    groupFilter,
    orderBy,
    orderDirection,
    tags,
    items,
    setItems,
}) => {
    const [queryLimit, setQueryLimit] = useState(10)
    const { loading, data, fetchMore, refetch } = useQuery(QUERY, {
        variables: {
            subtypes: typeFilter,
            tags,
            groupFilter,
            orderBy,
            orderDirection,
            offset: 0,
            limit: queryLimit,
        },
    })

    const isMount = useIsMount()

    useEffect(() => {
        if (isMount && !!data) {
            // Refetch if results were shown before (prevents long lists)
            refetch()
        }
    })

    const handleAddResults = () => {
        const newLinks = data.entities.edges.filter(
            (entity) =>
                items.findIndex((item) => item.guid === entity.guid) === -1,
        )
        setItems([...items, ...newLinks])
    }

    const handleAddResult = (entity) => {
        if (items.findIndex((el) => el.guid === entity.guid) === -1) {
            setItems([...items, entity])
        }
    }

    const { t } = useTranslation()

    return (
        <>
            <InsertFromContentHeader>
                <div style={{ padding: '8px 0' }}>
                    <H4 as="h3">{t('widget-linklist.results')}</H4>
                    {!loading && (
                        <Text
                            size="small"
                            variant="grey"
                            style={{
                                minHeight: '20px',
                                marginTop: '-4px',
                            }}
                        >
                            {t('global.count-of-total', {
                                count: data.entities.edges.length,
                                total: data.entities.total,
                            })}
                        </Text>
                    )}
                </div>
                {!loading && data.entities.edges.length > 0 && (
                    <Button
                        size="normal"
                        variant="secondary"
                        onClick={handleAddResults}
                        style={{
                            marginLeft: 'auto',
                        }}
                    >
                        {t('widget-linklist.add-item', {
                            count: data.entities.edges.length,
                        })}
                    </Button>
                )}
            </InsertFromContentHeader>

            {loading ? (
                <LoadingSpinner style={{ margin: '16px 0' }} />
            ) : (
                <FetchMore
                    isScrollableBox
                    containHeight
                    edges={data.entities.edges}
                    getMoreResults={(data) => data.entities.edges}
                    fetchMore={fetchMore}
                    fetchCount={10}
                    setLimit={setQueryLimit}
                    maxLimit={data.entities.total}
                    resultsMessage={t('global.result', {
                        count: data.entities.edges.length,
                    })}
                >
                    <ul>
                        {data.entities.edges.map((entity) => (
                            <InsertFromContentSearchResult
                                key={hashString(`${entity.guid}-search-result`)}
                                entity={entity}
                                onAddResult={handleAddResult}
                            />
                        ))}
                    </ul>
                </FetchMore>
            )}
        </>
    )
}

const itemTemplate = `
title
url
excerpt
`

const QUERY = gql`
    query LinkListSearchResults(
        $offset: Int
        $limit: Int
        $containerGuid: String
        $subtypes: [String!]
        $tags: [String!]
        $orderBy: OrderBy
        $orderDirection: OrderDirection
    ) {
        entities(
            subtypes: $subtypes
            tags: $tags
            offset: $offset
            limit: $limit
            containerGuid: $containerGuid
            orderBy: $orderBy
            orderDirection: $orderDirection
        ) {
            total
            edges {
                guid
                __typename
                ... on Blog {
                    ${itemTemplate}
                    subtype
                    ${featuredViewFragment}
                }
                ... on News {
                    ${itemTemplate}
                    subtype
                    ${featuredViewFragment}
                }
                ... on Discussion {
                    ${itemTemplate}
                    subtype
                    ${featuredViewFragment}
                }
                ... on Event {
                    ${itemTemplate}
                    subtype
                    ${featuredViewFragment}
                }
                ... on Question {
                    ${itemTemplate}
                    subtype
                    ${featuredViewFragment}
                }
                ... on Wiki {
                    ${itemTemplate}
                    subtype
                    ${featuredViewFragment}
                }
                ... on Page {
                    ${itemTemplate}
                }
                ... on Folder {
                    ${itemTemplate}
                    subtype
                }
                ... on File {
                    ${itemTemplate}
                    subtype
                }
                ... on Pad {
                    ${itemTemplate}
                    subtype
                }
                ... on ExternalContent {
                    ${itemTemplate}
                }
            }
        }
    }
`

export default InsertFromContentSearchResults
