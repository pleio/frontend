import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { sortByProperty } from 'helpers'
import { Set } from 'immutable'

import { Col, Row } from 'js/components/Grid/Grid'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Select from 'js/components/Select/Select'
import TagFields from 'js/components/TagFields/TagFields'
import useSortingOptions from 'js/page/Widget/hooks/useSortingOptions'

import InsertFromContentItems from './InsertFromContentItems'
import InsertFromContentSearchResults from './InsertFromContentSearchResults'

/*
This component is used in LinkList and Footer widget.
*/

const InsertFromContent = ({ data, guid, onInsert, onClose }) => {
    const [typeFilter, setTypeFilter] = useState([])
    const [tagFilter, setTagFilter] = useState(new Set())
    const [orderBy, setOrderBy] = useState('timePublished')
    const [orderDirection, setOrderDirection] = useState('desc')

    const { t } = useTranslation()
    const sortingOptions = useSortingOptions()

    const [items, setItems] = useState([])

    if (data.loading || !data.site) return <LoadingSpinner />

    const handleTypeFilter = (value) => {
        setTypeFilter(value)
    }

    const handleChangeSorting = (value) => {
        const [orderBy, orderDirection] = value.split('-')

        setOrderBy(orderBy)
        setOrderDirection(orderDirection)
    }

    const contentTypes =
        data?.site?.entityFilter?.contentTypes
            .map(({ key, value }) => ({ value: key, label: value }))
            .sort(sortByProperty('label')) || []

    const name = `widget-${guid}`

    const handleInsertItems = () => {
        onInsert(items)
        onClose()
    }

    return (
        <>
            <p>{t('widget-linklist.filters-helper')}</p>
            <Row $gutter={8} $spacing={12} style={{ marginTop: '16px' }}>
                <Col mobileUp={1 / 2}>
                    <Select
                        label={t('global.type')}
                        name={`${name}-typeFilter`}
                        placeholder={t('filters.filter-by')}
                        options={contentTypes}
                        value={typeFilter}
                        onChange={handleTypeFilter}
                        isMulti
                    />
                </Col>
                <Col mobileUp={1 / 2} style={{ marginLeft: 'auto' }}>
                    <Select
                        label={t('filters.sorting')}
                        name={`widget-${guid}-sorting`}
                        options={sortingOptions}
                        value={`${orderBy}-${orderDirection}`}
                        onChange={handleChangeSorting}
                    />
                </Col>
            </Row>
            <TagFields
                tags={tagFilter}
                customTagsAllowed={data.site.customTagsAllowed}
                tagCategories={data.site.tagCategories}
                setTags={setTagFilter}
                style={{ marginTop: '12px' }}
            />

            <Row $spacing={20}>
                <Col mobileLandscapeUp={1 / 2}>
                    <InsertFromContentSearchResults
                        tags={tagFilter.toJS()}
                        typeFilter={typeFilter}
                        orderBy={orderBy}
                        orderDirection={orderDirection}
                        items={items}
                        setItems={setItems}
                    />
                </Col>
                <Col mobileLandscapeUp={1 / 2}>
                    <InsertFromContentItems
                        items={items}
                        setItems={setItems}
                        onInsertLinks={handleInsertItems}
                    />
                </Col>
            </Row>
        </>
    )
}

const Query = gql`
    query LinkListSearchFilters {
        site {
            guid
            customTagsAllowed
            tagCategories {
                name
                values
            }
            entityFilter {
                contentTypes {
                    key
                    value
                }
            }
        }
    }
`
export default graphql(Query)(InsertFromContent)
