import React, { FC, useContext } from 'react'
import { createPortal } from 'react-dom'

import { useSiteStore } from 'js/lib/stores'
import PageEditorContext from 'js/page/PageEditor/PageEditorContext'
import ThemeProvider from 'js/theme/ThemeProvider'

const WidgetViewPortal: FC<{
    Component: React.ElementType
    settings: any
    [key: string]: any
}> = ({ Component, settings, ...rest }) => {
    const { site } = useSiteStore()

    const { widgetEditing } = useContext(PageEditorContext)

    const viewEntity = { settings }
    const viewPortal = widgetEditing?.refWidgetView?.current

    if (viewPortal) {
        return createPortal(
            <ThemeProvider theme="site" siteStyle={site?.style}>
                {/* Reset site theme */}
                <Component entity={viewEntity} editMode {...rest} />
            </ThemeProvider>,
            viewPortal,
        )
    }
    return null
}

export default WidgetViewPortal
