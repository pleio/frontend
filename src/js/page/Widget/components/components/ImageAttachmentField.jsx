import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import Img from 'js/components/Img/Img'
import Spacer from 'js/components/Spacer/Spacer'
import UploadOrSelectFilesModal from 'js/files/UploadOrSelectFiles/UploadOrSelectFilesModal'

const ImageAttachmentField = ({
    name,
    imageField,
    setImageField,
    setValue,
    watchValues,
    control,
    ...rest
}) => {
    const { t } = useTranslation()

    const removeImage = () => {
        setValue('image', null)
        setImageField(null)
    }

    const [showModal, setShowModal] = useState(false)

    const handleComplete = ({ guid, download }) => {
        setImageField({
            id: guid,
            url: download,
        })
    }

    const hasImage = watchValues.image || imageField

    return (
        <Spacer spacing="small" {...rest}>
            {hasImage && (
                <Img
                    src={imageField?.url || watchValues.image?.url}
                    alt={watchValues.imageAlt}
                />
            )}

            <Flexer gutter="small" justifyContent="flex-start">
                <Button
                    size="small"
                    variant="secondary"
                    onClick={() => setShowModal(true)}
                >
                    {t('action.select-image')}..
                </Button>
                {hasImage && (
                    <Button
                        size="small"
                        variant="secondary"
                        onClick={removeImage}
                    >
                        {t('form.remove-image')}
                    </Button>
                )}
            </Flexer>

            {hasImage && (
                <FormItem
                    type="text"
                    control={control}
                    name="imageAlt"
                    id={`imageAlt-${name}`}
                    label={t('editor.image-alt')}
                    helper={t('editor.image-alt-helper')}
                />
            )}

            <UploadOrSelectFilesModal
                title={t('action.select-image')}
                accept="image"
                showModal={showModal}
                setShowModal={setShowModal}
                onComplete={handleComplete}
            />
        </Spacer>
    )
}

export default ImageAttachmentField
