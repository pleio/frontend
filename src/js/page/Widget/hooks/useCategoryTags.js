import { useState } from 'react'
import { hasJsonStructure, sanitizeTagCategories } from 'helpers'

import useGetSetting from 'js/page/Widget/hooks/useGetSetting'

const useCategoryTags = (entity) => {
    const getSetting = useGetSetting(entity)

    const categoryTags = hasJsonStructure(getSetting('categoryTags'))
        ? JSON.parse(getSetting('categoryTags'))
        : []

    const [categoryTagsValue, setCategoryTagsValue] = useState(
        categoryTags || [],
    )

    const stringifyTagCategories = (siteTagCategories) =>
        JSON.stringify(
            sanitizeTagCategories(siteTagCategories, categoryTagsValue),
        )

    return {
        categoryTagsValue,
        stringifyTagCategories,
        setCategoryTagsValue,
    }
}

export default useCategoryTags
