const useGetRichDescription = (entity) => {
    const getRichDescription = (key, defaultValue = '') => {
        const setting = entity.settings.find((setting) => setting.key === key)
        return setting ? setting.richDescription : defaultValue
    }

    return getRichDescription
}

export default useGetRichDescription
