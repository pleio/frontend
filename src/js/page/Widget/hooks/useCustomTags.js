import { useState } from 'react'
import { Set } from 'immutable'

const useCustomTags = (tags) => {
    const [customTags, setCustomTags] = useState(new Set(tags || null))

    const handleAddTags = (tag) => {
        setCustomTags(customTags.add(tag))
    }
    const handleRemoveTags = (tag) => {
        setCustomTags(customTags.delete(tag))
    }

    const resetCustomTags = (tags) => {
        setCustomTags(new Set(tags))
    }

    const getParsedCustomTags = () => {
        return customTags.toJS().join(',')
    }

    return {
        customTags,
        handleAddTags,
        handleRemoveTags,
        getParsedCustomTags,
        resetCustomTags,
    }
}

export default useCustomTags
