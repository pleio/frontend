const useGetSetting = (entity) => {
    const getSetting = (key, defaultValue = '') => {
        const setting = entity?.settings?.find((setting) => setting.key === key)
        return setting ? setting.value : defaultValue
    }

    return getSetting
}

export default useGetSetting
