import { useTranslation } from 'react-i18next'

const useSortingOptions = () => {
    const { t } = useTranslation()

    return [
        {
            value: 'timePublished-desc',
            label: t('filters.sorting-published'),
        },
        {
            value: 'timePublished-asc',
            label: t('filters.sorting-published-old'),
        },
        {
            value: 'lastAction-desc',
            label: t('filters.sorting-activity'),
        },
        {
            value: 'lastAction-asc',
            label: t('filters.sorting-activity-old'),
        },
        {
            value: 'title-asc',
            label: t('filters.sorting-title'),
        },
        {
            value: 'title-desc',
            label: t('filters.sorting-title-old'),
        },
    ]
}

export default useSortingOptions
