import React from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import NotFound from 'js/core/NotFound'

import AddEditForm from './AddEditForm'

const Add = () => {
    const { t } = useTranslation()
    const location = useLocation()
    const { groupGuid } = useParams()

    const { loading, data } = useQuery(Query, {
        variables: {
            groupGuid,
        },
    })

    if (loading) return null
    if (!data?.site) return <NotFound />

    const { site, group } = data

    return (
        <AddEditForm
            title={t('entity-cms.create')}
            site={site}
            subtype="page"
            group={group}
            containerGuid={location?.state?.parentGuid}
        />
    )
}

const Query = gql`
    query addEntity($groupGuid: String) {
        site {
            guid
            tagCategories {
                name
                values
            }
        }
        group: entity(guid: $groupGuid) {
            guid
            ... on Group {
                defaultReadAccessId
                isClosed
            }
        }
    }
`

export default Add
