import { ownerFragment } from 'js/lib/fragments/owner'

const pageListFragment = `
    fragment PageListFragment on Page {
        guid
        title
        url
        pageType
        excerpt
        description
        richDescription
        statusPublished
        timeCreated
        timePublished
        tags
        ${ownerFragment}
        parent {
            guid
            title
            parent {
                guid
                title
                parent {
                    guid
                    title
                }
            }
        }
    }
`
export default pageListFragment
