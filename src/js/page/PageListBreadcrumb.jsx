import React, { Fragment } from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import Breadcrumb from 'js/page/Breadcrumb'

import RightSmallIcon from 'icons/chevron-right.svg'

const PageListBreadcrumb = ({ guid, rootUrl, ...rest }) => {
    const { t } = useTranslation()

    const { loading, data } = useQuery(GET_BREADCRUMB, {
        variables: {
            guid,
        },
        fetchPolicy: 'cache-and-network',
    })

    if (loading || !data?.breadcrumb) return null

    return (
        <div {...rest}>
            <Breadcrumb breadcrumb={data?.breadcrumb} rootUrl={rootUrl}>
                {rootUrl && (
                    <>
                        <Link to={rootUrl} className="Breadcrumb">
                            {t('entity-cms.title-list')}
                        </Link>
                        <RightSmallIcon />
                    </>
                )}
            </Breadcrumb>
        </div>
    )
}

const GET_BREADCRUMB = gql`
    query Breadcrumb($guid: String!) {
        breadcrumb(guid: $guid) {
            ... on Page {
                guid
                title
                url
            }
        }
    }
`

export default PageListBreadcrumb
