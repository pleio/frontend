import React, { useContext, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { useWindowScrollPosition } from 'helpers'

import Document from 'js/components/Document'
import accessControlFragment from 'js/components/EntityActions/fragments/accessControl'
import NotFound from 'js/core/NotFound'
import { ownerFragment } from 'js/lib/fragments/owner'
import globalStateContext from 'js/lib/withGlobalState/globalStateContext'
import { rowsViewFragment } from 'js/page/Page/rowsFragment'

import Page from './Page/Page'
import Text from './Text/Text'

const Item = ({ guid: homePageGuid, isHome }) => {
    const { setGlobalState } = useContext(globalStateContext)

    const params = useParams()

    const guid = homePageGuid || params.guid

    const { loading, data } = useQuery(GET_PAGE_VIEW, {
        variables: {
            guid,
        },
        fetchPolicy: 'cache-and-network',
    })

    useEffect(() => {
        if (data?.entity) {
            setGlobalState((newState) => {
                newState.canEditPage = data?.entity?.canEdit
            })
        }
    }, [data])

    useWindowScrollPosition(guid)

    if (loading) return null
    if (!data?.entity) return <NotFound />

    const { site, entity } = data
    const { pageType, group } = entity

    return (
        <>
            <Document
                title={!isHome && entity.title}
                containerTitle={group?.name}
            />
            {pageType === 'text' ? (
                <Text entity={entity} />
            ) : (
                <Page site={site} entity={entity} />
            )}
        </>
    )
}

const GET_PAGE_VIEW = gql`
    query PageView($guid: String!) {
        site {
            guid
            tagCategories {
                name
                values
            }
            activityFilter {
                contentTypes {
                    key
                    value
                }
            }
            searchFilter {
                contentTypes {
                    key
                    value
                }
            }
            searchPublishedFilterEnabled
            searchRelatedSitesEnabled
            sitePlanType
            scheduleAppointmentEnabled
        }
        entity(guid: $guid) {
            guid
            ... on Page {
                pageType
                statusPublished
                canEdit
                preventDeleteAndArchive
                title
                url
                description
                richDescription
                timePublished
                tags
                tagCategories {
                    name
                    values
                }
                parent {
                    guid
                    url
                }
                ${ownerFragment}
                hasChildren
                children {
                    guid
                }
                group {
                    guid
                    ... on Group {
                        name
                        url
                        isClosed
                        introduction
                        canEdit
                        memberCount
                        membership
                        members(limit: 3) {
                            edges {
                                user {
                                    guid
                                    username
                                    url
                                    name
                                    icon
                                }
                            }
                        }
                        plugins
                        menu {
                            id
                        }
                    }
                }
                rows {
                    ${rowsViewFragment}
                }
                ${accessControlFragment}
            }
        }
    }
`

export default Item
