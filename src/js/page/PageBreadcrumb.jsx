import React from 'react'
import { gql, useQuery } from '@apollo/client'
import styled from 'styled-components'

import { Container } from 'js/components/Grid/Grid'
import Breadcrumb from 'js/page/Breadcrumb'

const Wrapper = styled.div`
    position: relative;
    display: flex;
    background-color: white;
    min-height: ${(p) => p.theme.headerBarHeight}px;
    box-shadow: ${(p) => p.theme.shadow.hard.bottom};
    padding: 4px 0;
`

const PageBreadcrumb = ({ guid, ...rest }) => {
    const { loading, data } = useQuery(GET_BREADCRUMB, {
        variables: {
            guid,
        },
        fetchPolicy: 'cache-and-network',
    })

    if (loading || !data?.breadcrumb || data?.breadcrumb?.length === 1)
        return null

    return (
        <Wrapper {...rest}>
            <Container style={{ flexGrow: '1' }}>
                <Breadcrumb
                    breadcrumb={data?.breadcrumb}
                    style={{ height: '100%' }}
                />
            </Container>
        </Wrapper>
    )
}

const GET_BREADCRUMB = gql`
    query Breadcrumb($guid: String!) {
        breadcrumb(guid: $guid) {
            ... on Page {
                guid
                title
                url
            }
        }
    }
`

export default PageBreadcrumb
