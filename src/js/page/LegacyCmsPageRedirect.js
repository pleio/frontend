import React from 'react'
import { Navigate, useLocation } from 'react-router-dom'

import NotFound from 'js/core/NotFound'

function convertPath(pathname) {
    if (pathname === '/cms') {
        return '/pages'
    }
    return pathname.replace('/cms/view/', '/page/view/')
}

const LegacyCmsPageRedirect = () => {
    const location = useLocation()
    const newPath = convertPath(location.pathname)

    if (newPath === location.pathname) {
        return <NotFound />
    }

    return <Navigate to={newPath} replace />
}

export default LegacyCmsPageRedirect
