import React from 'react'

import FeedItem from 'js/components/FeedItem/FeedItem'
import FeedItemContent from 'js/components/FeedItem/FeedItemContent'

const Card = ({
    'data-feed': dataFeed,
    entity,
    hideExcerpt,
    excerptMaxLines,
}) => {
    const { pageType } = entity

    const entityCopy = { ...entity }
    entityCopy.subtype = pageType === 'text' ? 'page-text' : 'page-widget'

    return (
        <FeedItem data-feed={dataFeed}>
            <FeedItemContent
                entity={entityCopy}
                hideExcerpt={hideExcerpt}
                excerptMaxLines={excerptMaxLines}
            />
        </FeedItem>
    )
}

export default Card
