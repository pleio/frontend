import React, { useLayoutEffect, useState } from 'react'
import { DragDropContext } from 'react-beautiful-dnd'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import findByKeyValue from 'helpers/findByKeyValue'
import compose from 'lodash.flowright'
import styled from 'styled-components'

import Card from 'js/components/Card/Card'

import Item from './components/Item'
import List from './components/List'

const Wrapper = styled(Card)`
    padding: 8px 0;
    margin-bottom: 20px;
    overflow: hidden; // needed for isDraggingOver effect

    li {
        position: relative; // needed for positioning the collapse button
    }
`

const TextPageNav = ({ data, mutate, editMode }) => {
    const [menu, setMenu] = useState()

    useLayoutEffect(() => {
        if (data?.entity) {
            setMenu(data.entity.menu)
        }
    }, [data, setMenu])

    if (data.loading || !menu) return null

    const handleDragEnd = (result) => {
        const { source, destination } = result
        if (!destination || destination.index === source.index) return

        const listId = result.type

        const sourcePosition = result.source.index
        const destinationPosition = result.destination.index

        const newMenu = JSON.parse(JSON.stringify(menu))

        const children = [...findByKeyValue(newMenu, 'guid', listId).children]

        const [movedItem] = children.splice(sourcePosition, 1)
        children.splice(destinationPosition, 0, movedItem)

        const update = (guid, children) => (obj) => {
            if (obj.guid === guid) {
                if (obj.children) {
                    obj.children = children
                }
                return true
            } else if (obj.children)
                return obj.children.some(update(guid, children))
        }

        const newMenuArray = [newMenu]
        newMenuArray.some(update(listId, children))

        setMenu(newMenu)

        mutate({
            variables: {
                input: {
                    guid: result.draggableId,
                    sourcePosition: source.index,
                    destinationPosition: destination.index,
                },
            },
        })
    }

    return (
        <Wrapper as="ul">
            <li>
                <Item item={menu} editMode={editMode} depth={0} />
                <DragDropContext onDragEnd={handleDragEnd}>
                    <List
                        containerGuid={menu.guid}
                        items={menu.children}
                        depth={1}
                        editMode={editMode}
                    />
                </DragDropContext>
            </li>
        </Wrapper>
    )
}

const Query = gql`
    query SubNav($guid: String!) {
        entity(guid: $guid) {
            guid
            ... on Page {
                title
                url
                menu {
                    guid
                    title
                    link
                    children {
                        guid
                        title
                        link
                        children {
                            guid
                            title
                            link
                            children {
                                guid
                                title
                                link
                            }
                        }
                    }
                }
            }
        }
    }
`

const Mutation = gql`
    mutation SubNavReorder($input: reorderInput!) {
        reorder(input: $input) {
            container {
                guid
                ... on Page {
                    menu {
                        guid
                        title
                        link
                        children {
                            guid
                            title
                            link
                            children {
                                guid
                                title
                                link
                                children {
                                    guid
                                    title
                                    link
                                }
                            }
                        }
                    }
                }
            }
        }
    }
`

export default compose(
    graphql(Query, {
        options: {
            fetchPolicy: 'cache-and-network',
        },
    }),
    graphql(Mutation),
)(TextPageNav)
