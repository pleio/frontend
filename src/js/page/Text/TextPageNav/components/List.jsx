import React from 'react'
import { Draggable, Droppable } from 'react-beautiful-dnd'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import Item from './Item'

const Wrapper = styled.ul`
    position: relative;
    transition: box-shadow ${(p) => p.theme.transition.fast};
    box-shadow: ${(p) =>
        p.$isDraggingOver ? '0 0 0 9999px rgba(255,255,255,0.75)' : 'none'};
    z-index: 1;
`

const List = ({ editMode, containerGuid, items, depth, ...rest }) => (
    <Droppable
        droppableId={`droppable-${containerGuid}`}
        type={containerGuid}
        items={items}
        isDropDisabled={!editMode}
    >
        {(provided, snapshot) => {
            return (
                <>
                    <Wrapper
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                        $isDraggingOver={snapshot.isDraggingOver}
                    >
                        {items.map((item, i) => (
                            <Draggable
                                index={i}
                                key={item.guid}
                                draggableId={item.guid}
                                isDragDisabled={!editMode}
                            >
                                {(provided) => (
                                    <li
                                        ref={provided.innerRef}
                                        {...provided.draggableProps}
                                        {...provided.dragHandleProps}
                                    >
                                        <Item
                                            item={item}
                                            editMode={editMode}
                                            depth={depth}
                                            {...rest}
                                        />
                                    </li>
                                )}
                            </Draggable>
                        ))}
                        {provided.placeholder}
                    </Wrapper>
                </>
            )
        }}
    </Droppable>
)

List.propTypes = {
    containerGuid: PropTypes.string.isRequired,
    items: PropTypes.array.isRequired,
    depth: PropTypes.number.isRequired,
}

export default List
