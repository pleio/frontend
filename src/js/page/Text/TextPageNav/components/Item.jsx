import React, { useCallback, useLayoutEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { NavLink, useMatch, useParams } from 'react-router-dom'
import { useIsMount } from 'helpers'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import IconButton from 'js/components/IconButton/IconButton'

import ChevronDownIcon from 'icons/chevron-down.svg'

import AddPageButton from './AddPageButton'
import List from './List'

const StyledIconButton = styled(IconButton)`
    position: absolute;
    top: 0;
    height: 28px;
    width: 28px;
    right: 8px;
    background-color: transparent;

    &:hover {
        color: ${(p) => p.theme.color.icon.black};
    }
    &:not(:hover):focus:before {
        background-color: transparent;
    }
`

const Wrapper = styled.div`
    position: relative;
    width: 100%;
    display: flex;
    align-items: center;
    padding: 4px 16px;
    color: ${(p) => p.theme.color.text.black};
    text-align: left;
    text-decoration: none;
    text-shadow: none;
    user-select: none;

    white-space: ${(p) => !!p.nowrap && 'nowrap'};

    > * {
        position: relative;
    }

    > .RootPageTitle {
        font-size: ${(p) => p.theme.font.size.normal};
        line-height: ${(p) => p.theme.font.lineHeight.normal};
        font-weight: ${(p) => p.theme.font.weight.bold};

        &:before {
            top: 10px;
        }
    }

    &:before {
        content: '';
        width: 4px;
        height: 4px;
        border-radius: 50%;
        position: absolute;
        top: 12px;
        top: ${(p) => (p.$depth ? '12px' : '14px')};
        left: ${(p) => (p.$depth ? p.$depth * 16 : 16) - 9}px;
    }

    > span {
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
    }

    &:not([disabled]) {
        &:hover {
            background-color: ${(p) => p.theme.color.hover};
        }

        &:active {
            background-color: ${(p) => p.theme.color.active};
        }
    }

    &[aria-current],
    &[aria-selected='true'] {
        color: ${(p) => p.theme.color.primary.main};
        pointer-events: none;

        &:before {
            background-color: ${(p) => p.theme.color.primary.main};
        }
    }
`

const Item = ({ editMode, item, depth, ...rest }) => {
    const { guid } = useParams()

    const isMount = useIsMount()

    const [showChildren, setShowChildren] = useState(false)

    const findItemByGuid = useCallback((guid, children) => {
        if (children === undefined || !children.length) return null

        for (let i = 0, l = children.length; i < l; i++) {
            const child = children[i]
            if (child.guid === guid) return child
            const item = findItemByGuid(guid, child.children)
            if (item) return item
        }

        return null
    }, [])

    useLayoutEffect(() => {
        if (isMount) {
            // Show children if a descendant or current item is the current page
            if (!!findItemByGuid(guid, item.children) || guid === item.guid) {
                setShowChildren(true)
            }
        }
    }, [isMount, guid, showChildren, findItemByGuid, item])

    const toggleOpen = () => {
        setShowChildren(!showChildren)
    }

    const { title, children } = item
    const hasChildren = children?.length > 0

    const { t } = useTranslation()

    // Only the top-level item has an url
    const url = item.link || item.url

    const isRootPage = depth === 0

    const matchExact = useMatch({ path: url, end: true })

    const offset = (depth || 1) * 16

    return (
        <>
            <Wrapper
                as={NavLink}
                to={url}
                end
                $depth={depth}
                style={{
                    paddingLeft: `${offset}px`,
                    paddingRight: hasChildren && '24px',
                }}
            >
                <span className={isRootPage ? 'RootPageTitle' : ''}>
                    {title}
                </span>
            </Wrapper>

            {!isRootPage && hasChildren && (
                <StyledIconButton
                    variant={showChildren ? 'secondary' : 'tertiary'}
                    size="small"
                    aria-label={
                        showChildren
                            ? t('entity-cms.hide-subpages')
                            : t('entity-cms.show-subpages')
                    }
                    onClick={toggleOpen}
                >
                    <ChevronDownIcon
                        style={{
                            transform: `scaleY(${showChildren ? -1 : 1})`,
                        }}
                    />
                </StyledIconButton>
            )}

            {editMode && matchExact && depth < 3 && (
                <AddPageButton
                    parentGuid={guid}
                    style={{
                        marginLeft: `${offset + (depth > 0 ? 16 : 0)}px`,
                    }}
                />
            )}

            {!isRootPage && hasChildren && showChildren && (
                <List
                    containerGuid={item.guid}
                    items={children}
                    depth={depth + 1}
                    editMode={editMode}
                    {...rest}
                />
            )}
        </>
    )
}

Item.propTypes = {
    item: PropTypes.shape({
        link: PropTypes.string,
        title: PropTypes.string.isRequired,
        children: PropTypes.array,
    }).isRequired,
    editMode: PropTypes.bool.isRequired,
    depth: PropTypes.number.isRequired,
}

export default Item
