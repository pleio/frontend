import React from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation, useNavigate } from 'react-router-dom'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'

import AddIcon from 'icons/add.svg'

const AddButton = styled(Button)`
    order: 1;
    align-self: flex-start;
`

const AddPageButton = ({ parentGuid, ...rest }) => {
    const navigate = useNavigate()
    const location = useLocation()

    const { t } = useTranslation()

    const handleClick = () => {
        navigate('/pages/add', {
            state: {
                parentGuid,
                prevPathname: location.pathname,
            },
        })
    }

    return (
        <AddButton
            size="small"
            variant="secondary"
            onClick={handleClick}
            {...rest}
        >
            <AddIcon style={{ marginRight: '4px' }} />
            {t('entity-cms.nav-add-page')}
        </AddButton>
    )
}

AddPageButton.propTypes = {
    parentGuid: PropTypes.string,
}

export default AddPageButton
