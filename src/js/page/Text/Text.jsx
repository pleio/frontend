import React, { useContext } from 'react'
import { useParams } from 'react-router-dom'

import Card, { CardContent } from 'js/components/Card/Card'
import Flexer from 'js/components/Flexer/Flexer'
import { Col, Container, Row } from 'js/components/Grid/Grid'
import { H1 } from 'js/components/Heading'
import ItemActions from 'js/components/Item/ItemActions/ItemActions'
import Author from 'js/components/Item/ItemHeader/components/Author'
import ItemStatusTag from 'js/components/Item/ItemStatusTag'
import Section from 'js/components/Section/Section'
import TiptapView from 'js/components/Tiptap/TiptapView'
import globalStateContext from 'js/lib/withGlobalState/globalStateContext'

import TextPageNav from './TextPageNav/TextPageNav'

const Text = ({ entity }) => {
    const {
        globalState: { editMode },
    } = useContext(globalStateContext)
    const { containerGuid } = useParams()

    const { guid, title, richDescription, statusPublished, showOwner, owner } =
        entity

    const onEdit = `/page/edit/${guid}`
    const onAfterDelete = '/pages'

    const isPublished = statusPublished === 'published'

    const showAuthor = showOwner && owner

    const page = (
        <Card>
            <CardContent>
                {showAuthor && (
                    <Author entity={entity} style={{ marginBottom: '16px' }} />
                )}
                <Flexer alignItems="flex-start" gutter="none">
                    <H1
                        style={{
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'flex-start',
                        }}
                    >
                        {title}
                        <ItemStatusTag
                            entity={entity}
                            style={{
                                marginTop: '8px',
                            }}
                        />
                    </H1>
                    <ItemActions
                        canChangeAccess
                        canDuplicate
                        showEditButton
                        entity={entity}
                        onEdit={onEdit}
                        onAfterDelete={onAfterDelete}
                        refetchQueries={['PageView']}
                        style={{ marginLeft: 'auto' }}
                    />
                </Flexer>

                <TiptapView
                    content={richDescription}
                    style={{ marginTop: '16px' }}
                />
            </CardContent>
        </Card>
    )

    return (
        <Section>
            {isPublished ? (
                <Container>
                    <Row>
                        <Col mobileLandscapeUp={1 / 3} tabletUp={1 / 4}>
                            <TextPageNav
                                guid={containerGuid || guid}
                                editMode={editMode}
                            />
                        </Col>
                        <Col mobileLandscapeUp={2 / 3} tabletUp={3 / 4}>
                            {page}
                        </Col>
                    </Row>
                </Container>
            ) : (
                <Container size="normal">{page}</Container>
            )}
        </Section>
    )
}

export default Text
