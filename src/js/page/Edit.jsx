import React from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import accessControlFragment from 'js/components/EntityActions/fragments/accessControl'
import NotFound from 'js/core/NotFound'
import { rowsEditFragment } from 'js/page/Page/rowsFragment'

import AddEditForm from './AddEditForm'
import PageEditor from './PageEditor/PageEditor'

const Edit = () => {
    const { t } = useTranslation()
    const { guid } = useParams()

    const { loading, data } = useQuery(Query, {
        variables: {
            guid,
        },
        fetchPolicy: 'no-cache',
    })

    if (loading) return null

    const { site, viewer, entity } = data || {}

    if (!entity || !entity?.canEdit) return <NotFound />

    const { pageType } = entity

    if (pageType === 'text') {
        return <AddEditForm title={t('entity-cms.edit')} entity={entity} />
    } else {
        return <PageEditor entity={entity} site={site} viewer={viewer} />
    }
}

const Query = gql`
    query PageEdit($guid: String!) {
        viewer {
            guid
            isAdmin
        }
        site {
            guid
            scheduleAppointmentEnabled
            tagCategories {
                name
                values
            }
            customTagsAllowed
            activityFilter {
                contentTypes {
                    key
                    value
                }
            }
            searchFilter {
                contentTypes {
                    key
                    value
                }
            }
            searchPublishedFilterEnabled
            searchRelatedSitesEnabled
            searchRelatedSites {
                name
                url
            }
            sitePlanType
        }
        entity(guid: $guid) {
            guid
            ... on Page {
                pageType
                canEdit
                canArchiveAndDelete
                preventDeleteAndArchive
                title
                url
                description
                richDescription
                timePublished
                timeUpdated
                tags
                tagCategories {
                    name
                    values
                }
                statusPublished
                isRecommendedInSearch
                parent {
                    guid
                    url
                }
                group {
                    guid
                    ... on Group {
                        url
                        introduction
                        canEdit
                        memberCount
                        membership
                        members(limit: 3) {
                            edges {
                                user {
                                    guid
                                    username
                                    url
                                    name
                                    icon
                                }
                            }
                        }
                        plugins
                        isClosed
                    }
                }
                rows {
                    ${rowsEditFragment}
                }
                ${accessControlFragment}
            }
        }
    }
`

export default Edit
