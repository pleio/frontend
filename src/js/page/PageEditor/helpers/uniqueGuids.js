import { getUniqueId } from 'helpers'

// To be able to be dragged, rows and widgets need a unique guid that doesn't change when the row changes position
export function addUniqueGuids(rows) {
    return rows.map((row) => ({
        ...row,
        rowGuid: getUniqueId('row'),
        columns: row.columns.map((column) => ({
            ...column,
            widgets: column.widgets.map((widget) => ({
                ...widget,
                widgetGuid: getUniqueId('widget'),
            })),
        })),
    }))
}

// But we don't want to send the unique guids to the server
export function removeUniqueGuids(rows) {
    return rows.map((row) => {
        // eslint-disable-next-line
        const { rowGuid, ...rest } = row
        return {
            ...rest,
            columns: row.columns.map((column) => {
                // const { widgets, ...rest } = column
                return {
                    ...column,
                    widgets: column.widgets.map((widget) => {
                        // eslint-disable-next-line
                        const { widgetGuid, ...rest } = widget
                        return rest
                    }),
                }
            }),
        }
    })
}
