import { getReadableColor } from 'helpers/getContrast'
import { transparentize } from 'polished'

// Get a transparent black or white
export default (color) => {
    const readableColor = getReadableColor(color)
    const transparantColor = transparentize(
        readableColor === 'black' ? 0.58 : 0.21,
        readableColor,
    )
    return transparantColor
}
