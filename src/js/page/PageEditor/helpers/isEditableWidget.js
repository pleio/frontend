const nonEditableWidgets = ['groupMembers', 'groupIntroduction']

export default function (type) {
    return !nonEditableWidgets.includes(type)
}
