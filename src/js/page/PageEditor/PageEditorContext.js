import { createContext } from 'react'

const PageEditorContext = createContext()

export default PageEditorContext
