import React, { useContext, useState } from 'react'
import { Draggable } from 'react-beautiful-dnd'
import { produce } from 'immer'
import styled, { css, useTheme } from 'styled-components'

import { Container, Row as GridRow } from 'js/components/Grid/Grid'
import Section from 'js/components/Section/Section'
import getTransparentColor from 'js/page/PageEditor/helpers/getTransparentColor'
import PageEditorContext from 'js/page/PageEditor/PageEditorContext'

import AddColumnActions from './AddColumnActions'
import Column from './Column'
import RowActions from './RowActions'

const getActiveShadowStyling = (bgColor) => {
    const transparantColor = getTransparentColor(bgColor)
    return css`
        box-shadow:
            0 -1px ${transparantColor},
            inset 0 -1px ${transparantColor};
    `
}

const Wrapper = styled(Section)`
    position: relative;
    display: flex;

    .WidgetPlaceholder {
        box-shadow: 0 0 0 1px ${(p) => getTransparentColor(p.backgroundColor)};
    }

    .DashedBoxLine {
        stroke: ${(p) => getTransparentColor(p.backgroundColor)};
    }

    ${(p) =>
        p.$isFocused &&
        css`
            ${(p) => getActiveShadowStyling(p.backgroundColor)};

            &:first-child {
                .RowActions,
                .ColumnActions,
                .WidgetActions {
                    /* Appear on top of header bar, only when focused because of page options dropdown inside header */
                    z-index: 12;
                }
            }

            .PageColumnDashedBox {
                opacity: 1;
            }

            .AddColumnActions > * {
                opacity: 1;
                pointer-events: auto;
            }
        `};

    ${(p) =>
        p.$isDragging &&
        css`
            ${(p) => getActiveShadowStyling(p.backgroundColor)};
        `};

    .PageRowContainer {
        position: relative;
    }

    ${(p) =>
        p.$isFullWidth &&
        css`
            padding-left: 0 !important;
            padding-right: 0 !important;

            .PageRowContainer {
                max-width: none;
            }
        `};
`

const Row = ({ rowIndex, entity }) => {
    const { isFullWidth, columns, backgroundColor } = entity
    const numberOfColumns = columns.length

    const theme = useTheme()

    const [showBackgroundColorSetting, setShowBackgroundColorSetting] =
        useState(false)

    const [isFocused, setIsFocused] = useState(false)

    const { setRows, isDragging, widgetEditing } = useContext(PageEditorContext)

    const updateFocus = (val) => {
        // Prevent setting focus to different row when color setting is open
        if (!showBackgroundColorSetting) {
            setIsFocused(val)
        }
    }

    const handleAddColumn = (position) => {
        const newWidth =
            numberOfColumns === 2 ? 4 : numberOfColumns === 3 ? 3 : 6

        const emptyColumn = {
            width: [newWidth],
            widgets: [],
        }

        const newColumns =
            position === 'left'
                ? [emptyColumn, ...columns]
                : [...columns, emptyColumn]

        setRows(
            produce((newState) => {
                newState[rowIndex].columns = newColumns
            }),
        )

        setRows(
            produce((newState) => {
                newState[rowIndex].columns.forEach(
                    (column) => (column.width = [newWidth]),
                )
            }),
        )
    }

    return (
        <Draggable
            key={`${entity.rowGuid}`} // Do not remove - react-beautiful-dnd needs this
            draggableId={`${entity.rowGuid}`}
            index={rowIndex}
            isDragDisabled={!!widgetEditing}
        >
            {(provided, snapshot) => (
                <Wrapper
                    ref={provided.innerRef}
                    {...provided.draggableProps}
                    $isFullWidth={isFullWidth && numberOfColumns === 1}
                    backgroundColor={backgroundColor || theme.color.grey[10]} // Is also passed down to Section
                    $isFocused={!isDragging && !widgetEditing && isFocused}
                    $isDragging={isDragging}
                    className={`PageRow${
                        isFullWidth && numberOfColumns === 1
                            ? ' PageRowFullWidth'
                            : ''
                    }`}
                    // .PageRow is used to adapt the side padding in the editor
                    // .PageRowFullWidth is used to make Lead/Leader stretch to the side edges of the screen
                    onMouseEnter={() => updateFocus(true)}
                    onMouseLeave={() => updateFocus(false)}
                >
                    <RowActions
                        className="RowActions"
                        show={
                            !widgetEditing &&
                            (snapshot.isDragging || (isFocused && !isDragging))
                        }
                        onFocus={() => updateFocus(true)}
                        onBlur={() => updateFocus(false)}
                        snapshot={snapshot}
                        provided={provided}
                        rowIndex={rowIndex}
                        numberOfColumns={numberOfColumns}
                        isFullWidth={isFullWidth}
                        backgroundColor={backgroundColor}
                        showBackgroundColorSetting={showBackgroundColorSetting}
                        setShowBackgroundColorSetting={
                            setShowBackgroundColorSetting
                        }
                    />

                    <Container className="PageRowContainer">
                        {!isDragging && !isFullWidth && numberOfColumns < 4 && (
                            <AddColumnActions
                                className="AddColumnActions"
                                onAddColumn={handleAddColumn}
                                onFocus={() => updateFocus(true)}
                                onBlur={() => updateFocus(false)}
                            />
                        )}

                        <GridRow
                            $spacing={20}
                            // Grow widget height to column
                            $growContent
                        >
                            {entity.columns.map((column, index) => (
                                <Column
                                    key={`${rowIndex}-${index}`}
                                    rowIndex={rowIndex}
                                    columnIndex={index}
                                    entity={column}
                                    numberOfColumns={numberOfColumns}
                                    backgroundColor={
                                        backgroundColor || theme.color.grey[10]
                                    }
                                    showBackgroundColorSetting={
                                        showBackgroundColorSetting
                                    }
                                />
                            ))}
                        </GridRow>
                    </Container>
                </Wrapper>
            )}
        </Draggable>
    )
}

export default Row
