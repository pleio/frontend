import React from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

import Flexer from 'js/components/Flexer/Flexer'
import ThemeProvider from 'js/theme/ThemeProvider'

const Wrapper = styled.div`
    position: absolute;
    left: 0;
    right: 0;
    display: flex;
    justify-content: center;
    z-index: 1; // Position on top of row
    pointer-events: none;

    &:has(.colorFieldPopover) {
        z-index: 2; // Position on top of next row's action buttons
    }

    ${(p) =>
        p.$position === 'top' &&
        css`
            top: 0;
            transform: translateY(-100%);

            .ActionsBarButtons {
                transform: translateY(8px);
            }
        `};

    ${(p) =>
        p.$position === 'bottom' &&
        css`
            bottom: 0;
            transform: translateY(100%);

            .ActionsBarButtons {
                transform: translateY(-8px);
            }
        `};

    .ActionsBarButtonsBg {
        padding: 2px;
        background: white;
        border: 1px solid ${(p) => p.theme.color.grey[30]};

        ${(p) =>
            p.$isWidget &&
            css`
                border-color: ${(p) => p.theme.color.secondary.main};
            `};

        ${(p) =>
            p.$position === 'top' &&
            css`
                border-top-left-radius: ${(p) => p.theme.radius.large};
                border-top-right-radius: ${(p) => p.theme.radius.large};
                box-shadow: ${(p) => p.theme.shadow.soft.top};
                border-bottom: none;
            `};

        ${(p) =>
            p.$position === 'bottom' &&
            css`
                border-bottom-left-radius: ${(p) => p.theme.radius.large};
                border-bottom-right-radius: ${(p) => p.theme.radius.large};
                box-shadow: ${(p) => p.theme.shadow.soft.bottom};
                border-top: none;
            `};
    }

    .ActionsBarButtons {
        display: flex;
        justify-content: center;
        opacity: 0;
        transition:
            opacity ${(p) => p.theme.transition.fast},
            transform ${(p) => p.theme.transition.materialFast};
    }

    ${(p) =>
        p.$show &&
        css`
            /* Override main isDragging styling */
            display: flex !important;

            .ActionsBarButtons {
                opacity: 1;
                transform: translateY(0);
                pointer-events: auto !important;
            }
        `};

    ${(p) =>
        p.$hide &&
        css`
            .ActionsBarButtons {
                opacity: 0 !important;
                transform: translateY(8px) !important;
                pointer-events: none !important;
            }
        `};

    ${(p) =>
        p.$fullWidthInteractiveArea &&
        css`
            .ActionsBarButtons {
                /* Full width area to hover and show actions */
                width: 100%;
            }
        `};

    ${(p) =>
        p.$alwaysInteractiveArea &&
        css`
            .ActionsBarButtons {
                pointer-events: auto;
            }
        `};
`

const ActionsBar = ({
    show,
    hide,
    fullWidthInteractiveArea,
    alwaysInteractiveArea,
    isWidget,
    position = 'top',
    onFocus,
    onBlur,
    children,
    ...rest
}) => {
    return (
        <ThemeProvider theme="pleio" className="WidgetIgnoreGrow">
            <Wrapper
                $show={show}
                $hide={hide}
                $fullWidthInteractiveArea={fullWidthInteractiveArea}
                $alwaysInteractiveArea={alwaysInteractiveArea}
                $isWidget={isWidget}
                $position={position}
                onFocus={onFocus}
                onBlur={onBlur}
                {...rest}
            >
                <div className="ActionsBarButtons">
                    <Flexer
                        divider="normal"
                        gutter="tiny"
                        className="ActionsBarButtonsBg"
                    >
                        {children}
                    </Flexer>
                </div>
            </Wrapper>
        </ThemeProvider>
    )
}

ActionsBar.propTypes = {
    position: PropTypes.oneOf(['top', 'bottom']),
}

export default ActionsBar
