import React, { useReducer, useRef } from 'react'
import { FocusOn } from 'react-focus-on'
import { useTranslation } from 'react-i18next'
import { useMediaQuery } from 'react-responsive'
import { AnimatePresence, motion } from 'framer-motion'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import { CardContent } from 'js/components/Card/Card'
import Flexer from 'js/components/Flexer/Flexer'
import { H3 } from 'js/components/Heading'
import Modal from 'js/components/Modal/Modal'
import StickyProvider from 'js/components/Sticky/StickyProvider'
import Widget from 'js/page/Widget/Widget'

const Wrapper = styled(motion.div)`
    position: fixed;
    top: 0;
    padding-top: ${(p) => p.theme.headerBarHeight}px;
    left: 0;
    bottom: 0;
    width: 340px;
    background-color: white;
    z-index: 10;
    box-shadow: ${(p) => p.theme.shadow.hard.right};
    display: flex;
    flex-direction: column;

    .WidgetSettingsTitle {
        color: ${(p) => p.theme.color.primary.main};
        margin-bottom: 16px;
    }
`

const ContentWrapper = styled(StickyProvider)`
    flex-grow: 1;
    overflow-y: auto;
    font-size: ${(p) => p.theme.font.size.small};
    line-height: ${(p) => p.theme.font.lineHeight.small};
`

const ActionsElement = ({ onClose, handleSave, widgetOptions = {} }) => {
    const { t } = useTranslation()

    const { saveButton } = widgetOptions

    return (
        <CardContent>
            <Flexer>
                <Button
                    size="normal"
                    variant="tertiary"
                    type="button"
                    onClick={onClose}
                >
                    {t('action.cancel')}
                </Button>
                <Button
                    size="normal"
                    variant="primary"
                    onHandle={handleSave}
                    disabled={saveButton.disabled}
                >
                    {t('action.save')}
                </Button>
            </Flexer>
        </CardContent>
    )
}

const widgetOptionsReducer = (state, action) => {
    switch (action.type) {
        case 'setSaveButtonDisabled':
            return {
                ...state,
                saveButton: {
                    disabled: action.payload,
                },
            }
        default:
            return state
    }
}

const WidgetSettings = ({ site, entity, group, onClose, onSave }) => {
    const { t } = useTranslation()

    const refWidget = useRef()

    const [widgetOptions, dispatchWidgetOptions] = useReducer(
        widgetOptionsReducer,
        { saveButton: { disabled: false } },
    )

    const widget = entity?.widget
    const type = widget?.type
    const typeKeyOverrides = {
        activity: 'feed',
        callToAction: 'call-to-action',
        html: 'code',
        linkList: 'linklist',
    }

    const title = t(`widget-${typeKeyOverrides[type] || type}.title`)

    const handleSave = () => {
        refWidget.current.onSave()
    }

    const guid = `${type}-edit`

    const WidgetElement = entity && (
        <Widget
            ref={refWidget}
            guid={guid}
            site={site}
            entity={widget}
            columnWidth={entity?.columnWidth}
            isEditing
            group={group}
            onSave={onSave}
            dispatchWidgetOptions={dispatchWidgetOptions}
        />
    )

    const showModal = useMediaQuery({ maxWidth: 1443 })

    if (showModal) {
        return (
            <Modal
                isVisible={!!entity}
                title={title}
                size="normal"
                maxWidth="400px"
                onClose={onClose}
                containHeight={type === 'text'} // For now only text widgets are scrollable (overflow-y: auto created problems with select boxes)
                noPadding
            >
                <ContentWrapper
                    id={guid}
                    style={{
                        marginTop: '16px',
                        paddingTop: '4px',
                    }}
                >
                    <CardContent style={{ paddingTop: 0 }}>
                        {WidgetElement}
                    </CardContent>
                    {entity && (
                        <ActionsElement
                            onClose={onClose}
                            handleSave={handleSave}
                            widgetOptions={widgetOptions}
                        />
                    )}
                </ContentWrapper>
            </Modal>
        )
    } else {
        return (
            <AnimatePresence>
                {!!entity && (
                    <FocusOn
                        // eslint-disable-next-line
                        autoFocus={true}
                        scrollLock={false}
                        noIsolation
                        onEscapeKey={onClose}
                    >
                        <Wrapper
                            initial={{ opacity: 0, translateX: '-100%' }}
                            animate={{ opacity: 1, translateX: 0 }}
                            exit={{ opacity: 0, translateX: '-100%' }}
                            transition={{ duration: 0.25 }}
                        >
                            <ContentWrapper id={guid}>
                                <CardContent>
                                    <H3 className="WidgetSettingsTitle">
                                        {title}
                                    </H3>
                                    {WidgetElement}
                                </CardContent>
                                {entity && (
                                    <ActionsElement
                                        onClose={onClose}
                                        handleSave={handleSave}
                                        widgetOptions={widgetOptions}
                                    />
                                )}
                            </ContentWrapper>
                        </Wrapper>
                    </FocusOn>
                )}
            </AnimatePresence>
        )
    }
}

WidgetSettings.propTypes = {
    group: PropTypes.object,
    entity: PropTypes.object,
    onClose: PropTypes.func,
    onSave: PropTypes.func,
}

export default WidgetSettings
