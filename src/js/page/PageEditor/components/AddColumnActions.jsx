import React from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import DashedBox from 'js/components/DashedBox/DashedBox'
import IconButton from 'js/components/IconButton/IconButton'
import Tooltip from 'js/components/Tooltip/Tooltip'

import AddIcon from 'icons/add.svg'

const Wrapper = styled.div`
    position: absolute;
    top: 0;
    left: -32px;
    right: -32px;
    bottom: 0;
    pointer-events: none;
    display: flex;
    justify-content: space-between;
    align-items: center;
    z-index: 1;

    > * {
        width: 32px;
        height: 100%;
        opacity: 0;
        transition: opacity ${(p) => p.theme.transition.fast};

        > * {
            width: 100%;
            height: 100%;
            background-color: transparent;
            color: inherit;
        }
    }
`

const AddColumnActions = ({ onAddColumn, ...rest }) => {
    const { t } = useTranslation()

    const handleAddLeft = () => onAddColumn('left')

    const handleAddRight = () => onAddColumn('right')

    return (
        <Wrapper {...rest}>
            <DashedBox>
                <Tooltip
                    content={t('page.add-column')}
                    placement="left"
                    followCursor="vertical"
                    delay
                >
                    <IconButton
                        variant="secondary"
                        radiusStyle="none"
                        onClick={handleAddLeft}
                        aria-label={t('page.add-column')}
                    >
                        <AddIcon />
                    </IconButton>
                </Tooltip>
            </DashedBox>
            <DashedBox>
                <Tooltip
                    content={t('page.add-column')}
                    placement="right"
                    followCursor="vertical"
                    delay
                >
                    <IconButton
                        variant="secondary"
                        radiusStyle="none"
                        onClick={handleAddRight}
                        aria-label={t('page.add-column')}
                    >
                        <AddIcon />
                    </IconButton>
                </Tooltip>
            </DashedBox>
        </Wrapper>
    )
}

export default AddColumnActions
