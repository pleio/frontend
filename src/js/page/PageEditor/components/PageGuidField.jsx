import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'

import SettingContainer from 'js/admin/layout/SettingContainer'
import Select from 'js/components/Select/Select'

const PAGESQUERY = gql`
    query PagesQuery {
        entities(subtype: "page", offset: 0, limit: 9999) {
            edges {
                guid
                ... on Page {
                    title
                    pageType
                }
            }
        }
    }
`

const PageGuidField = ({ guid, value, onChange }) => {
    const { loading, data } = useQuery(PAGESQUERY)
    const pages =
        data?.entities?.edges.filter(
            (p) => p.pageType === 'campagne' && p.guid !== guid,
        ) || []

    const { t } = useTranslation()

    if (loading || pages.length === 0) {
        return null
    }

    return (
        <SettingContainer
            subtitle="page.subpage-of"
            htmlFor="containerGuid"
            inputWidth="full"
            $mb
        >
            <Select
                name="containerGuid"
                isSearchable
                isClearable
                isLoading={loading}
                value={value}
                options={pages.map(({ guid, title }) => ({
                    value: guid,
                    label: title,
                }))}
                placeholder={t('page.search-page')}
                onChange={onChange}
            />
        </SettingContainer>
    )
}

export default PageGuidField
