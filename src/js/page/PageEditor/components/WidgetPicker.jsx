import React, { useContext, useEffect, useRef, useState } from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import { AriaLiveMessage } from 'js/components/AriaLive'
import Text from 'js/components/Text/Text'
import Textfield from 'js/components/Textfield/Textfield'
import PageEditorContext from 'js/page/PageEditor/PageEditorContext'
import getWidgets from 'js/page/Widget/helpers/getWidgets'

const Wrapper = styled.div`
    max-width: 100%;
    width: ${(p) => (p.$columns === 3 ? '235px' : '314px')};
    border-radius: ${(p) => p.theme.radius.small};
    overflow: hidden;

    .WidgetPickerFilter {
        z-index: 1;
        width: 100%;
        box-shadow: 0 1px 0 0 ${(p) => p.theme.color.grey[30]};
    }

    .WidgetPickerOptions {
        display: flex;
        flex-wrap: wrap;
        background-color: ${(p) => p.theme.color.grey[10]};
        margin: 0 -1px;

        > * {
            position: relative;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            padding: 14px 0 10px;
            width: 80px;
            height: 80px;
            margin: 0 0 -1px -1px;
            border: 1px solid ${(p) => p.theme.color.grey[20]};
            background-color: white;

            &:before {
                content: '';
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
            }

            &:hover:before {
                background-color: ${(p) => p.theme.color.hover};
            }

            &:active:before {
                background-color: ${(p) => p.theme.color.active};
            }

            div {
                position: relative;
                flex-grow: 1;
                display: flex;
                align-items: center;
            }

            span {
                position: relative;
                padding: 4px 4px 0;
                font-size: ${(p) => p.theme.font.size.tiny};
                line-height: ${(p) => p.theme.font.lineHeight.tiny};
                text-align: center;
            }
        }
    }
`

const WidgetPicker = ({ onAddWidget, ...rest }) => {
    const { site, group } = useContext(PageEditorContext)

    const handleAddWidget = (type) => {
        onAddWidget(type)
    }

    const [searchValue, setSearchValue] = useState('')
    const handleChangeSearch = (evt) => {
        setSearchValue(evt.target.value)
    }

    const refSearch = useRef()

    useEffect(() => {
        refSearch?.current.focus()
    }, [])

    const { t } = useTranslation()

    const { scheduleAppointmentEnabled, sitePlanType } = site

    const widgets = getWidgets(
        sitePlanType,
        scheduleAppointmentEnabled,
        !!group,
    )

    const filteredWidgets = widgets.filter(
        (widget) =>
            widget.label.toLowerCase().indexOf(searchValue.toLowerCase()) !==
            -1,
    )

    return (
        <Wrapper $columns={widgets.length > 3 ? 4 : 3} {...rest}>
            <Textfield
                ref={refSearch}
                className="WidgetPickerFilter"
                borderStyle="none"
                placeholder={t('page.search-widget')}
                value={searchValue}
                onChange={handleChangeSearch}
            />
            <AriaLiveMessage
                message={
                    filteredWidgets.length
                        ? `${filteredWidgets.length}`
                        : t('page.widgets-not-found')
                }
            />
            {filteredWidgets.length ? (
                <div className="WidgetPickerOptions">
                    {filteredWidgets.map((widget) => (
                        <button
                            key={widget.value}
                            type="button"
                            onClick={() => handleAddWidget(widget.value)}
                        >
                            <div>{widget.icon}</div>
                            <span>{widget.label}</span>
                        </button>
                    ))}
                </div>
            ) : (
                <Text
                    textAlign="center"
                    size="small"
                    style={{ padding: '16px' }}
                >
                    {t('page.widgets-not-found')}
                </Text>
            )}
        </Wrapper>
    )
}

export default WidgetPicker
