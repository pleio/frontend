import React, { useContext, useEffect, useRef, useState } from 'react'
import { Droppable } from 'react-beautiful-dnd'
import { getUniqueId } from 'helpers'
import { produce } from 'immer'
import styled, { css } from 'styled-components'

import DashedBox from 'js/components/DashedBox/DashedBox'
import { Col } from 'js/components/Grid/Grid'
import getTransparentColor from 'js/page/PageEditor/helpers/getTransparentColor'
import PageEditorContext from 'js/page/PageEditor/PageEditorContext'

import isEditableWidget from '../helpers/isEditableWidget'

import AddWidgetButton from './AddWidgetButton'
import ColumnActions from './ColumnActions'
import Widget from './Widget'

const Wrapper = styled(Col)`
    position: relative;
    display: flex;
    flex-direction: column;

    &:after {
        content: '';
        display: block;
        position: absolute;
        top: -1px;
        left: 0;
        right: 0;
        bottom: 19px;
        pointer-events: none;
        border: 1px solid ${(p) => getTransparentColor(p.$backgroundColor)};
        opacity: 0;
    }

    ${(p) =>
        p.$showOutline &&
        css`
            &:after {
                opacity: 1;
            }
        `};

    ${(p) =>
        p.$numberOfWidgets === 0 &&
        css`
            min-height: 80px;
        `};

    ${(p) =>
        p.$numberOfWidgets > 0 &&
        css`
            margin-bottom: 0 !important;
        `}

    .PageColumnWidgetWrapper {
        position: relative;
        display: flex;
        flex-direction: column;

        ${(p) =>
            p.$numberOfWidgets === 0 &&
            css`
                flex-grow: 1;
            `};

        ${(p) =>
            p.$numberOfWidgets === 1 &&
            css`
                /* Grow widget height to column */
                flex-grow: 1;
            `};
    }

    .PageColumnDashedBox {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        opacity: 0;
        pointer-events: none;
        transition: opacity ${(p) => p.theme.transition.fast};
    }

    .PageAddWidget {
        position: absolute;
        left: 10px;
        right: 10px;
        top: 0;
        bottom: 0;
        opacity: 0;
        transition: opacity ${(p) => p.theme.transition.fast};
    }

    ${(p) =>
        p.$showWidgetPicker &&
        css`
            .PageAddWidget {
                opacity: 1;
            }
        `};

    ${(p) =>
        p.$isFocused &&
        css`
            .PageAddWidget {
                opacity: 1;
            }
        `};
`

const Column = ({
    rowIndex,
    columnIndex,
    entity,
    numberOfColumns,
    backgroundColor,
    showBackgroundColorSetting,
}) => {
    const { setRows, isDragging, widgetEditing, setWidgetEditing } =
        useContext(PageEditorContext)

    const refWidgetView = useRef()

    const [isFocused, setIsFocused] = useState(false)
    const [showWidgetPicker, setShowWidgetPicker] = useState(false)
    const [focusedWidget, setFocusedWidget] = useState(false)

    useEffect(() => {
        setFocusedWidget(false) // Keeps column actions accessible
    }, [isDragging])

    const widgetIsFocused = !!focusedWidget

    const { widgets, width } = entity

    const hideWidgetPicker = () => setShowWidgetPicker(false)

    const handleAddWidget = (index, type) => {
        const newWidget = {
            type,
            settings: [],
            widgetGuid: getUniqueId('widget'),
        }

        setRows(
            produce((newState) => {
                newState[rowIndex].columns[columnIndex].widgets.splice(
                    index,
                    0,
                    newWidget,
                )
            }),
        )

        handleEditWidget(index, newWidget)
    }

    const handleEditWidget = (index, widget) => {
        const canEdit = isEditableWidget(widget.type)
        if (!canEdit) return

        setWidgetEditing({
            rowIndex,
            columnIndex,
            index,
            columnWidth,
            widget,
            refWidgetView,
        })
    }

    const showActions = !isDragging && !widgetEditing && isFocused

    const columnWidth = width[0]

    return (
        <Wrapper
            tabletUp={columnWidth / 12}
            $backgroundColor={backgroundColor}
            $isFocused={!widgetEditing && isFocused}
            $showOutline={
                showActions && !widgetIsFocused && widgets?.length > 0
            }
            $numberOfWidgets={widgets.length}
            $showWidgetPicker={showWidgetPicker}
            onMouseEnter={() => setIsFocused(true)}
            onMouseLeave={() => setIsFocused(false)}
        >
            <ColumnActions
                className="ColumnActions"
                show={showActions}
                hide={widgetIsFocused || showBackgroundColorSetting}
                fullWidthInteractiveArea
                alwaysInteractiveArea
                width={width}
                rowIndex={rowIndex}
                columnIndex={columnIndex}
                numberOfColumns={numberOfColumns}
                onFocus={() => {
                    'focus'
                    setIsFocused(true)
                }}
                onBlur={() => setIsFocused(false)}
            />

            <Droppable droppableId={`${rowIndex}-${columnIndex}`} type="WIDGET">
                {(provided) => (
                    <div
                        ref={provided.innerRef}
                        className="PageColumnWidgetWrapper"
                        {...provided.droppableProps}
                    >
                        {widgets?.length > 0 &&
                            widgets.map((widget, index) => {
                                const handleDeleteWidget = () => {
                                    setRows(
                                        produce((newState) => {
                                            newState[rowIndex].columns[
                                                columnIndex
                                            ].widgets.splice(index, 1)
                                        }),
                                    )
                                    setFocusedWidget(false) // Make column actions accessible again
                                }

                                const handleAddWidgetBelow = (type) => {
                                    setIsFocused(false) // Hide actions on this widget
                                    handleAddWidget(index + 1, type)
                                }

                                const id = `${rowIndex}-${columnIndex}-${index}`

                                return (
                                    <Widget
                                        ref={refWidgetView}
                                        key={id}
                                        id={id}
                                        index={index}
                                        entity={widget}
                                        onEdit={() =>
                                            handleEditWidget(index, widget)
                                        }
                                        onDelete={handleDeleteWidget}
                                        onAdd={handleAddWidgetBelow}
                                        focusedWidget={focusedWidget}
                                        setFocusedWidget={setFocusedWidget}
                                        rowBackgroundColor={backgroundColor}
                                        columnWidth={columnWidth}
                                    />
                                )
                            })}

                        {widgets?.length === 0 && (
                            <DashedBox className="PageColumnDashedBox" />
                        )}

                        {provided.placeholder}
                    </div>
                )}
            </Droppable>

            {!isDragging && !widgetEditing && widgets.length === 0 && (
                <AddWidgetButton
                    id={`${rowIndex}-${columnIndex}-add-widget`}
                    className="PageAddWidget"
                    show={showWidgetPicker}
                    setShow={setShowWidgetPicker}
                    onFocus={() => setIsFocused(true)}
                    onBlur={() => setIsFocused(false)}
                    onAdd={(type) => {
                        hideWidgetPicker()
                        handleAddWidget(0, type)
                    }}
                />
            )}
        </Wrapper>
    )
}

export default Column
