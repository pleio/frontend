import React, { useContext } from 'react'
import { useTranslation } from 'react-i18next'
import { produce } from 'immer'

import IconButton from 'js/components/IconButton/IconButton'
import Tooltip from 'js/components/Tooltip/Tooltip'
import PageEditorContext from 'js/page/PageEditor/PageEditorContext'

import ResizeIcon from 'icons/column-resize.svg'
import ResizeHalfIcon from 'icons/column-resize-6.svg'
import RemoveIcon from 'icons/delete.svg'

import ActionsBar from './ActionsBar'

const ColumnActions = ({
    width,
    rowIndex,
    columnIndex,
    numberOfColumns,
    ...rest
}) => {
    const { t } = useTranslation()

    const { setRows } = useContext(PageEditorContext)

    if (numberOfColumns === 1) return null

    const handleRemove = () => {
        const newWidth =
            numberOfColumns === 4 ? 4 : numberOfColumns === 3 ? 6 : 12

        setRows(
            produce((newState) => {
                newState[rowIndex].columns.splice(columnIndex, 1)
                newState[rowIndex].columns.forEach(
                    (column) => (column.width = [newWidth]),
                )
            }),
        )
    }

    const handleResize = () => {
        let newWidthFirst = 6
        let newWidthSecond = 6

        if (width[0] === 6) {
            newWidthFirst = columnIndex === 0 ? 8 : 4
            newWidthSecond = newWidthFirst === 8 ? 4 : 8
        }

        setRows(
            produce((newState) => {
                newState[rowIndex].columns.forEach(
                    (column, index) =>
                        (column.width =
                            index === 0 ? [newWidthFirst] : [newWidthSecond]),
                )
            }),
        )
    }

    return (
        <ActionsBar {...rest}>
            {numberOfColumns === 2 && (
                <Tooltip
                    content={t('page.resize-columns')}
                    placement="top"
                    delay
                >
                    <IconButton
                        size="large"
                        variant="secondary"
                        radiusStyle="rounded"
                        onClick={handleResize}
                        aria-label={
                            width[0] === 6
                                ? t('page.resize-columns')
                                : t('page.resize-column-smaller')
                        }
                    >
                        {width[0] === 6 ? (
                            <ResizeIcon
                                style={
                                    columnIndex === 1
                                        ? { transform: 'scaleX(-1)' }
                                        : {}
                                }
                            />
                        ) : (
                            <ResizeHalfIcon />
                        )}
                    </IconButton>
                </Tooltip>
            )}
            <Tooltip content={t('page.delete-column')} placement="top" delay>
                <IconButton
                    size="large"
                    variant="secondary"
                    radiusStyle="rounded"
                    onClick={handleRemove}
                    aria-label={t('page.delete-column')}
                >
                    <RemoveIcon />
                </IconButton>
            </Tooltip>
        </ActionsBar>
    )
}

export default ColumnActions
