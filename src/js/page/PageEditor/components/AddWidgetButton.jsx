import React from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import Popover from 'js/components/Popover/Popover'

import AddWidgetIcon from 'icons/add-widget.svg'
import CrossIcon from 'icons/cross.svg'

import WidgetPicker from './WidgetPicker'

const Wrapper = styled.div`
    position: relative;
    min-height: 80px;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;

    .AddWidgetButtonOverlay {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;

        &:hover {
            background-color: ${(p) => p.theme.color.hover};
        }

        &:active {
            background-color: ${(p) => p.theme.color.active};
        }

        body:not(.mouse-user) &:focus {
            outline: none;

            + .AddWidgetButton {
                outline: ${(p) => p.theme.focusStyling};
            }
        }
    }

    .AddWidgetButton {
        position: relative;
        display: flex;
        flex-direction: column;
        align-items: center;
        padding: 14px 20px;
        background: white;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.tiny};
        font-weight: ${(p) => p.theme.font.weight.semibold};
        text-align: center;
        color: ${(p) => !p.$showWidgetPicker && p.theme.color.pleio};
        border-radius: ${(p) => p.theme.radius.large};
        box-shadow: ${(p) => p.theme.menu.shadow};
        overflow: hidden;
        pointer-events: none;

        > svg {
            height: 18px;
        }
    }
`

const AddWidgetButton = ({ onAdd, show, setShow, ...rest }) => {
    const { t } = useTranslation()

    const toggleShow = () => {
        setShow(!show)
    }

    const hide = () => {
        setShow(false)
    }

    return (
        <Wrapper {...rest}>
            <button
                type="button"
                className="AddWidgetButtonOverlay"
                onClick={toggleShow}
            />
            <Popover
                visible={show}
                onHidden={hide}
                trigger="manual"
                content={show ? <WidgetPicker onAddWidget={onAdd} /> : null}
                placement="top"
            >
                <div className="AddWidgetButton">
                    {show ? <CrossIcon /> : <AddWidgetIcon />}
                    <span style={{ marginTop: '8px' }}>
                        {show ? t('page.close-widgets') : t('page.add-widget')}
                    </span>
                </div>
            </Popover>
        </Wrapper>
    )
}

export default AddWidgetButton
