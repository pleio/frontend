import React from 'react'
import { ChromePicker } from 'react-color'
import { useTheme } from 'styled-components'

import { H4 } from 'js/components/Heading'
import RadioFields from 'js/components/RadioField/RadioFields'

const BgColorSetting = ({ width, name, title, value, onChange, ...rest }) => {
    const theme = useTheme()

    const customColor = value && value !== 'white'

    return (
        <div {...rest}>
            <H4 style={{ marginBottom: '4px' }}>{title}</H4>
            <RadioFields
                name={name}
                legend={title}
                options={[
                    {
                        value: '',
                        label: 'None',
                    },
                    {
                        value: 'white',
                        label: 'White',
                    },
                    {
                        value: customColor ? value : theme.color.secondary.main, // default
                        label: 'Custom',
                    },
                ]}
                value={value}
                onChange={(evt) => onChange(evt.target.value)}
                size="small"
            />
            {customColor && (
                <div style={{ marginTop: '8px' }}>
                    <ChromePicker
                        className="ColorFieldPicker"
                        disableAlpha
                        color={value}
                        onChange={(color) => onChange(color.hex)}
                        width={width || 'auto'}
                    />
                </div>
            )}
        </div>
    )
}

export default BgColorSetting
