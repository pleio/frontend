import React, { forwardRef, useContext } from 'react'
import { Draggable } from 'react-beautiful-dnd'
import styled, { css } from 'styled-components'

import PageEditorContext from 'js/page/PageEditor/PageEditorContext'
import PageWidget from 'js/page/Widget/Widget'

import WidgetActions from './WidgetActions'

/*
    Component is used in both page and group,
    so a lot of props are needed because context
    doesn't work or it needs different actions.
*/

const Wrapper = styled.div`
    position: relative; // Outline wraps around widget
    display: flex;
    flex-direction: column;
    margin-bottom: 20px; // Always on to prevent jumps when dragging and placing

    &:after {
        content: '';
        display: block;
        position: absolute;
        top: -1px;
        left: -1px;
        right: -1px;
        bottom: -1px;
        pointer-events: none;
        border: 1px solid ${(p) => p.theme.color.pleio};
        border-radius: ${(p) => p.theme.radius.normal};
        opacity: 0;
    }

    /* Grow widget height to column */
    &:last-child:first-child {
        flex-grow: 1;

        > *:not(.WidgetIgnoreGrow) {
            flex-grow: 1;
        }
    }

    ${(p) =>
        p.$showBackground &&
        css`
            color: black; // Fix if color was white because of background
            background-color: white;
        `};

    ${(p) =>
        p.$showOutline &&
        css`
            &:after {
                opacity: 1;
            }
        `};
`

const Widget = forwardRef(
    (
        {
            id,
            index,
            entity,
            onEdit,
            onDelete,
            onAdd,
            focusedWidget,
            setFocusedWidget,
            rowBackgroundColor,
            columnWidth,
        },
        ref,
    ) => {
        const { site, group, isDragging, widgetEditing } =
            useContext(PageEditorContext)

        const { widgetGuid } = entity

        const isFocused = focusedWidget === widgetGuid

        const onFocus = () => {
            setFocusedWidget(widgetGuid)
        }
        const onBlur = () => {
            setFocusedWidget(false)
        }

        const isEditing =
            id ===
            `${widgetEditing?.rowIndex}-${widgetEditing?.columnIndex}-${widgetEditing?.index}`

        return (
            <Draggable
                draggableId={widgetGuid}
                index={index}
                isDragDisabled={!!widgetEditing}
            >
                {(provided, snapshot) => (
                    <Wrapper
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        $showOutline={
                            snapshot.isDragging ||
                            isEditing ||
                            (isFocused && !isDragging && !widgetEditing)
                        }
                        $showBackground={snapshot.isDragging}
                        onMouseEnter={onFocus}
                        onMouseLeave={onBlur}
                    >
                        <WidgetActions
                            show={
                                !widgetEditing &&
                                (snapshot.isDragging ||
                                    (isFocused && !isDragging))
                            }
                            id={id}
                            entity={entity}
                            snapshot={snapshot}
                            provided={provided}
                            widgetEditing={widgetEditing}
                            onEdit={onEdit}
                            onDelete={onDelete}
                            onAdd={onAdd}
                            onFocus={onFocus}
                            onBlur={onBlur}
                        />

                        {isEditing ? (
                            <div ref={ref}>{/* Widget preview portal */}</div>
                        ) : (
                            <PageWidget
                                guid={widgetGuid}
                                site={site}
                                entity={entity}
                                editMode
                                rowBackgroundColor={rowBackgroundColor}
                                columnWidth={columnWidth}
                                group={group}
                                canEditContainer
                            />
                        )}
                    </Wrapper>
                )}
            </Draggable>
        )
    },
)

Widget.displayName = 'Widget'

export default Widget
