import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'
import { sanitizeTagCategories } from 'helpers'

import Button from 'js/components/Button/Button'
import CustomTagsField from 'js/components/CustomTagsField'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import Modal from 'js/components/Modal/Modal'
import Setting from 'js/components/Setting'
import Spacer from 'js/components/Spacer/Spacer'
import TagCategoriesField from 'js/components/TagCategories/TagCategoriesField'
import useCustomTags from 'js/page/Widget/hooks/useCustomTags'

import PageGuidField from './PageGuidField'

const SettingsModal = ({ site, viewer, entity = {}, isVisible, onClose }) => {
    const { t } = useTranslation()

    const [editPage] = useMutation(EDIT_PAGE)

    const [mutationErrors, setMutationErrors] = useState()

    const {
        guid,
        group,
        title,
        tags,
        tagCategories,
        parent,
        richDescription,
        isRecommendedInSearch,
    } = entity

    const [categoryTags, setCategoryTags] = useState(tagCategories || [])

    const { customTags, handleAddTags, handleRemoveTags } = useCustomTags(tags)

    const [containerGuid, setContainerGuid] = useState(parent?.guid)

    const defaultValues = {
        title,
        pageType: null,
        richDescription,
        isRecommendedInSearch: isRecommendedInSearch || false,
    }

    const {
        control,
        handleSubmit,
        formState: { errors, isValid, isSubmitting },
    } = useForm({
        mode: 'onChange',
        defaultValues,
    })

    const submit = async ({
        title,
        richDescription,
        accessId,
        isRecommendedInSearch,
    }) => {
        const input = {
            containerGuid: containerGuid || null,
            guid,
            title,
            richDescription,
            accessId: parseInt(accessId, 10),
            tagCategories: sanitizeTagCategories(
                site.tagCategories,
                categoryTags,
            ),
            tags: customTags.toJS(),
            isRecommendedInSearch,
        }

        setMutationErrors()

        await editPage({
            variables: {
                input,
            },
            refetchQueries: ['PageList', 'PageEdit'],
        })
            .then(() => {
                onClose()
            })
            .catch((errors) => {
                console.error(errors)
                setMutationErrors(errors)
            })
    }

    const onError = (errors) => {
        return new Promise((resolve, reject) => {
            reject(new Error(errors))
        })
    }

    return (
        <Modal
            isVisible={isVisible}
            onClose={onClose}
            title={t('global.settings')}
            size="normal"
        >
            <form onSubmit={handleSubmit(submit, onError)}>
                <Spacer>
                    <FormItem
                        control={control}
                        type="text"
                        name="title"
                        title={t('form.title')}
                        required
                        errors={errors}
                    />

                    <FormItem
                        control={control}
                        type="rich"
                        name="richDescription"
                        title={t('form.description')}
                        options={{
                            textFormat: true,
                            textStyle: true,
                            textLink: true,
                            textQuote: true,
                            textList: true,
                            insertMedia: true,
                            insertAccordion: true,
                            insertTable: true,
                        }}
                        errors={errors}
                    />

                    <TagCategoriesField
                        name="filters"
                        tagCategories={site.tagCategories}
                        value={categoryTags}
                        setTags={setCategoryTags}
                    />
                    {site.customTagsAllowed && (
                        <CustomTagsField
                            name="customTags"
                            value={customTags}
                            onAddTag={handleAddTags}
                            onRemoveTag={handleRemoveTags}
                        />
                    )}

                    {!group && (
                        <PageGuidField
                            guid={guid}
                            value={containerGuid}
                            onChange={setContainerGuid}
                        />
                    )}

                    {viewer?.isAdmin && (
                        <Setting
                            subtitle="form.recommended-search-result"
                            helper="form.recommended-search-result-helper"
                            htmlFor="isRecommendedInSearch"
                        >
                            <FormItem
                                control={control}
                                type="switch"
                                name="isRecommendedInSearch"
                                size="small"
                            />
                        </Setting>
                    )}
                </Spacer>

                <Errors errors={mutationErrors} />

                <Flexer mt>
                    <Button size="normal" variant="tertiary" onClick={onClose}>
                        {t('action.cancel')}
                    </Button>
                    <Button
                        type="submit"
                        size="normal"
                        variant="primary"
                        disabled={!isValid}
                        loading={isSubmitting}
                    >
                        {t('action.save')}
                    </Button>
                </Flexer>
            </form>
        </Modal>
    )
}

const EDIT_PAGE = gql`
    mutation EditPage($input: editPageInput!) {
        editPage(input: $input) {
            entity {
                guid
                ... on Page {
                    title
                    timeUpdated
                }
            }
        }
    }
`

export default SettingsModal
