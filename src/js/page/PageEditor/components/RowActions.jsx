import React, { useContext } from 'react'
import { useTranslation } from 'react-i18next'
import { produce } from 'immer'
import styled from 'styled-components'

import IconButton from 'js/components/IconButton/IconButton'
import Popover from 'js/components/Popover/Popover'
import Tooltip from 'js/components/Tooltip/Tooltip'
import PageEditorContext from 'js/page/PageEditor/PageEditorContext'

import DeleteIcon from 'icons/delete.svg'
import MoveIcon from 'icons/move-vertical.svg'
import PaintIcon from 'icons/paint.svg'
import ResizeRowNarrowIcon from 'icons/row-resize-narrow.svg'
import ResizeRowWideIcon from 'icons/row-resize-wide.svg'

import ActionsBar from './ActionsBar'
import BgColorSetting from './BgColorSetting'

const Wrapper = styled(ActionsBar)`
    .RowColorField {
        padding: ${(p) =>
            `${p.theme.padding.vertical.small} ${p.theme.padding.horizontal.small}`};
    }

    .ColorFieldPicker {
        margin: 0 -${(p) => p.theme.padding.horizontal.small} -12px;
    }

    /* Increase hover area for maintaining focus while hovering to color setting */
    .ActionsBarButtons {
        padding-left: 40px;
        padding-right: 40px;
    }
`

const RowActions = ({
    rowIndex,
    backgroundColor,
    showBackgroundColorSetting,
    setShowBackgroundColorSetting,
    numberOfColumns,
    isFullWidth,
    snapshot,
    provided,
    ...rest
}) => {
    const { t } = useTranslation()

    const { setRows, widgetEditing } = useContext(PageEditorContext)

    const handleRemove = () => {
        setRows(
            produce((newState) => {
                newState.splice(rowIndex, 1)
            }),
        )
    }

    const handleResize = () => {
        setRows(
            produce((newState) => {
                newState[rowIndex].isFullWidth = !isFullWidth
            }),
        )
    }

    const editBackgroundColor = (val) => {
        setRows(
            produce((newState) => {
                newState[rowIndex].backgroundColor = val
            }),
        )
    }

    return (
        <Wrapper {...rest}>
            {!snapshot.isDragging && numberOfColumns === 1 && (
                <Tooltip content={t('page.resize-row')} placement="top" delay>
                    <IconButton
                        variant="secondary"
                        size="large"
                        radiusStyle="rounded"
                        onClick={handleResize}
                        aria-label={t('page.resize-row')}
                    >
                        {isFullWidth ? (
                            <ResizeRowNarrowIcon />
                        ) : (
                            <ResizeRowWideIcon />
                        )}
                    </IconButton>
                </Tooltip>
            )}
            {!snapshot.isDragging && (
                <div>
                    <Popover
                        className="colorFieldPopover"
                        visible={showBackgroundColorSetting}
                        onHidden={() => setShowBackgroundColorSetting(false)}
                        content={
                            <BgColorSetting
                                name={`${rowIndex}-background-color`}
                                title={t('page.row-color')}
                                value={backgroundColor || ''}
                                onChange={editBackgroundColor}
                                className="RowColorField"
                                width="200px"
                            />
                        }
                        placement="top"
                        offset={[
                            0, 2,
                        ]} /* Prevent row hiding when hovering to Popover */
                    >
                        <Tooltip
                            content={t('page.row-color')}
                            placement="top"
                            delay
                        >
                            <IconButton
                                variant="secondary"
                                disabled={!!widgetEditing}
                                size="large"
                                radiusStyle="rounded"
                                onClick={() =>
                                    setShowBackgroundColorSetting(
                                        !showBackgroundColorSetting,
                                    )
                                }
                            >
                                <PaintIcon />
                            </IconButton>
                        </Tooltip>
                    </Popover>
                </div>
            )}
            <Tooltip content={t('page.move-row')} placement="top" delay>
                <IconButton
                    as="div"
                    variant={widgetEditing ? 'tertiary' : 'secondary'}
                    disabled={!!widgetEditing}
                    size="large"
                    radiusStyle="rounded"
                    className="PageTopBaseAction"
                    {...provided.dragHandleProps}
                    aria-label={t('page.press-spacebar-to-move-row')}
                >
                    <MoveIcon />
                </IconButton>
            </Tooltip>
            {!snapshot.isDragging && (
                <Tooltip content={t('page.delete-row')} placement="top" delay>
                    <IconButton
                        variant="secondary"
                        size="large"
                        radiusStyle="rounded"
                        onClick={handleRemove}
                        aria-label={t('page.delete-row')}
                    >
                        <DeleteIcon />
                    </IconButton>
                </Tooltip>
            )}
        </Wrapper>
    )
}

export default RowActions
