import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'

import IconButton from 'js/components/IconButton/IconButton'
import Popover from 'js/components/Popover/Popover'
import Tooltip from 'js/components/Tooltip/Tooltip'

import AddWidgetBelowIcon from 'icons/add-widget-below.svg'
import CrossIcon from 'icons/cross.svg'
import DeleteIcon from 'icons/delete.svg'
import EditIcon from 'icons/edit.svg'
import MoveIcon from 'icons/move.svg'

import isEditableWidget from '../helpers/isEditableWidget'

import ActionsBar from './ActionsBar'
import WidgetPicker from './WidgetPicker'

const WidgetActions = ({
    show,
    id,
    entity,
    snapshot,
    provided,
    widgetEditing,
    onEdit,
    onDelete,
    onAdd,
    onFocus,
    onBlur,
}) => {
    const { t } = useTranslation()

    const canEdit = isEditableWidget(entity?.type)

    const { widgetGuid } = entity

    const [showWidgetPicker, setShowWidgetPicker] = useState(false)
    const toggleWidgetPicker = () => setShowWidgetPicker(!showWidgetPicker)

    const hideWidgetPicker = () => setShowWidgetPicker(false)

    const addWidget = (type) => {
        hideWidgetPicker()
        onAdd(type)
    }

    const actionsBarProps = {
        className: 'WidgetActions',
        isWidget: true,
        show,
        onFocus,
        onBlur,
    }

    return (
        <>
            <ActionsBar {...actionsBarProps}>
                {!snapshot.isDragging && canEdit && (
                    <Tooltip // <Tooltip theme="pleio-white" /> is needed here because tippy's global style doesn't take nested theme into account
                        content={t('page.edit-widget')}
                        placement="top"
                        theme="pleio-white"
                        delay
                    >
                        <IconButton
                            id={`${id}-edit-widget`}
                            variant="primary"
                            size="large"
                            radiusStyle="rounded"
                            onClick={onEdit}
                            aria-label={t('page.edit-widget')}
                        >
                            <EditIcon />
                        </IconButton>
                    </Tooltip>
                )}
                <Tooltip
                    content={t('page.move-widget')}
                    placement="top"
                    theme="pleio-white"
                    delay
                >
                    <IconButton
                        {...provided.dragHandleProps}
                        className="PageTopBaseAction"
                        as="div"
                        variant={widgetEditing ? 'tertiary' : 'primary'}
                        disabled={!!widgetEditing}
                        size="large"
                        radiusStyle="rounded"
                        aria-label={t('page.move-widget')}
                        aria-describedby={`aria-widget-${widgetGuid}`}
                    >
                        <MoveIcon />
                    </IconButton>
                </Tooltip>
                {!snapshot.isDragging && (
                    <Tooltip
                        content={t('page.delete-widget')}
                        placement="top"
                        theme="pleio-white"
                        delay
                    >
                        <IconButton
                            variant="primary"
                            size="large"
                            radiusStyle="rounded"
                            onClick={onDelete}
                            aria-label={t('page.delete-widget')}
                        >
                            <DeleteIcon />
                        </IconButton>
                    </Tooltip>
                )}
            </ActionsBar>
            {!snapshot.isDragging && (
                <ActionsBar
                    {...actionsBarProps}
                    position="bottom"
                    fullWidthInteractiveArea
                >
                    <div>
                        <Popover
                            visible={showWidgetPicker}
                            onHidden={hideWidgetPicker}
                            trigger="manual"
                            content={
                                showWidgetPicker ? (
                                    <WidgetPicker onAddWidget={addWidget} />
                                ) : null
                            }
                            placement="top"
                            offset={[
                                0, 3,
                            ]} /* Prevent row hiding when hovering to Popover */
                        >
                            <div>
                                <Tooltip
                                    content={
                                        showWidgetPicker
                                            ? t('page.close-widgets')
                                            : t('page.add-widget-below')
                                    }
                                    placement="right"
                                    theme={!showWidgetPicker && 'pleio-white'}
                                    delay
                                >
                                    <IconButton
                                        id={`${id}-add-widget`}
                                        variant={
                                            showWidgetPicker
                                                ? 'secondary'
                                                : 'pleio'
                                        }
                                        size="large"
                                        radiusStyle="rounded"
                                        aria-label={
                                            showWidgetPicker
                                                ? t('page.close-widgets')
                                                : t('page.add-widget-below')
                                        }
                                        onClick={toggleWidgetPicker}
                                    >
                                        {showWidgetPicker ? (
                                            <CrossIcon />
                                        ) : (
                                            <AddWidgetBelowIcon />
                                        )}
                                    </IconButton>
                                </Tooltip>
                            </div>
                        </Popover>
                    </div>
                </ActionsBar>
            )}
        </>
    )
}

export default WidgetActions
