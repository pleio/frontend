import React, { useContext, useEffect, useState } from 'react'
import { DragDropContext, Droppable } from 'react-beautiful-dnd'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { Link, useLocation, useNavigate } from 'react-router-dom'
import { gql, useMutation } from '@apollo/client'
import { getUniqueId, media, useIsMount } from 'helpers'
import { produce } from 'immer'
import styled, { css } from 'styled-components'

import Button from 'js/components/Button/Button'
import DisplayDate from 'js/components/DisplayDate'
import Document from 'js/components/Document'
import ConfirmAccessModal from 'js/components/EntityActions/components/ConfirmAccessModal'
import accessControlFragment from 'js/components/EntityActions/fragments/accessControl'
import ErrorBoundary from 'js/components/ErrorBoundary'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import { sanitizeAccessControl } from 'js/components/Form/AccessControlField'
import { Container } from 'js/components/Grid/Grid'
import { H4 } from 'js/components/Heading'
import IconButton from 'js/components/IconButton/IconButton'
import CopyItem from 'js/components/Item/ItemActions/components/CopyItem'
import DeleteForm from 'js/components/Item/ItemActions/components/DeleteForm'
import ItemActions from 'js/components/Item/ItemActions/ItemActions'
import ItemStatusTag from 'js/components/Item/ItemStatusTag'
import Modal from 'js/components/Modal/Modal'
import Sticky from 'js/components/Sticky/Sticky'
import Text from 'js/components/Text/Text'
import globalStateContext from 'js/lib/withGlobalState/globalStateContext'
import { rowsViewFragment } from 'js/page/Page/rowsFragment'
import PageEditorContext from 'js/page/PageEditor/PageEditorContext'
import ThemeProvider from 'js/theme/ThemeProvider'

import AddIcon from 'icons/add.svg'
import BackIcon from 'icons/arrow-left.svg'

import Row from './components/Row'
import SettingsModal from './components/SettingsModal'
import WidgetSettings from './components/WidgetSettings'
import { addUniqueGuids, removeUniqueGuids } from './helpers/uniqueGuids'

const drawerPadding = `${340 + 32}px`

const Header = styled(Sticky)`
    position: relative; // Needed for isEditingWidget overlay
    z-index: 11;
    background-color: white;
    box-shadow: ${(p) => p.theme.shadow.hard.bottom};

    ${(p) =>
        p.$isEditingWidget &&
        css`
            user-select: none;

            &:before {
                content: '';
                position: absolute;
                z-index: 999;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                background-color: rgba(255, 255, 255, 0.5);
                user-select: none;
            }
        `};

    &[data-sticky='true'] {
        box-shadow: ${(p) => p.theme.shadow.soft.bottom};
    }

    ${media.tabletUp`
        @media (max-width: 1719px) {
            padding-left: 32px;
            padding-right: 32px;
        }
    `};

    @media (min-width: 1784px) {
        padding: 0 ${drawerPadding};
    }

    .PageHeaderContainer {
        display: flex;
        flex-wrap: wrap;
        align-items: center;
        min-height: ${(p) => p.theme.headerBarHeight}px;
    }

    ${H4} {
        color: ${(p) => p.theme.color.primary.main};
    }
`

const Wrapper = styled.main`
    position: relative; // Needed for isSubmitting overlay
    flex-grow: 1;
    display: flex;
    flex-direction: column;
    transition: padding ${(p) => p.theme.transition.materialNormal};

    [data-rbd-droppable-id='droppable-rows'] {
        flex-grow: ${(p) => (p.$hasRows ? 1 : 0)};
        display: flex;
        flex-direction: column;
    }

    .PageRow:last-child {
        flex-grow: 1;
    }

    ${(p) =>
        p.$isFooter &&
        css`
            [data-rbd-droppable-id='droppable-rows'] {
                flex-grow: 1;
                justify-content: flex-end;
            }

            .PageRow:last-child {
                flex-grow: 0;
            }
        `};

    ${(p) =>
        p.$isSubmitting &&
        css`
            &:before {
                content: '';
                position: absolute;
                z-index: 999;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                background-color: rgba(255, 255, 255, 0.5);
            }
        `};

    @media (max-width: 1719px) {
        .PageRow {
            padding-left: 32px;
            padding-right: 32px;
        }
    }

    ${(p) =>
        p.$isEditingWidget &&
        css`
            @media (min-width: 1444px) and (max-width: 1783px) {
                padding-left: 340px;
            }
        `};

    @media (min-width: 1784px) {
        .PageRow {
            padding-left: ${drawerPadding};
            padding-right: ${drawerPadding};
        }
    }

    ${(p) =>
        p.$isDragging === 'WIDGET' &&
        css`
            .PageColumnDashedBox {
                opacity: 1;
            }
        `};

    .PageAddRow {
        width: 100%;
        height: 140px;
        opacity: ${(p) => (p.$hasRows ? 0 : 1)};
        transition: opacity ${(p) => p.theme.transition.fast};
        display: flex;
        align-items: center;
        justify-content: center;

        &:hover {
            background-color: ${(p) => p.theme.color.hover};
        }

        &:active {
            background-color: ${(p) => p.theme.color.active};
        }

        &:focus,
        &:hover {
            opacity: 1;
        }

        body:not(.mouse-user) &:focus {
            outline: none;

            .PageAddRowButton {
                outline: ${(p) => p.theme.focusStyling};
            }
        }
    }

    .PageAddRowButton {
        position: relative;
        display: flex;
        flex-direction: column;
        align-items: center;
        padding: 14px 20px;
        background: white;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.tiny};
        font-weight: ${(p) => p.theme.font.weight.semibold};
        text-align: center;
        border-radius: ${(p) => p.theme.radius.large};
        box-shadow: ${(p) => p.theme.menu.shadow};
        overflow: hidden;
        user-select: none;

        &:before {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin: auto;
        }
    }
`

const ErrorWrapper = styled(Flexer)`
    background-color: white;
    padding: 16px 25%;
    width: 100%;
`

const PageEditor = ({ site, viewer, entity, isFooter = false }) => {
    const globalState = useContext(globalStateContext)
    const { setGlobalState } = globalState

    const location = useLocation()
    const navigate = useNavigate()
    const { t } = useTranslation()
    const isMount = useIsMount()

    const showItemsActions = !isFooter

    const [errors, setErrors] = useState()

    const [rows, setRows] = useState(addUniqueGuids(entity.rows || []))

    const [isDirty, setIsDirty] = useState(false)

    const { tagCategories } = site

    const { guid, title, url, timeUpdated, statusPublished, group } = entity

    const [showCopyPageModal, setShowCopyPageModal] = useState(false)

    const [accessControl, setAccessControl] = useState(
        entity?.accessControl || [],
    )
    const [showAccessModal, setShowAccessModal] = useState(false)

    const goBackUrl = location?.state?.prevPathname?.includes(guid)
        ? url
        : location?.state?.prevPathname || url

    const isPublished = statusPublished === 'published'
    const isArchived = statusPublished === 'archived'

    const onBeforeUnload = (evt) => {
        if (isDirty) {
            evt.preventDefault()
            evt.returnValue = ''
        }
    }

    useEffect(() => {
        if (isMount) {
            setGlobalState((newState) => {
                newState.editMode = false
            })
        }

        window.addEventListener('beforeunload', onBeforeUnload)

        return () => {
            window.removeEventListener('beforeunload', onBeforeUnload)
        }
    })

    useEffect(() => {
        if (!isMount && !isDirty) {
            setIsDirty(true)
        }
        // eslint-disable-next-line
    }, [rows])

    const [widgetEditing, setWidgetEditing] = useState(null)
    const closeWidgetEditing = () => {
        setWidgetEditing(null)
    }

    const [isDragging, setIsDragging] = useState(false)

    const [showSettingsModal, setShowSettingsModal] = useState(false)
    const toggleSettingsModal = () => setShowSettingsModal(!showSettingsModal)

    const [showDeleteModal, setShowDeleteModal] = useState(false)
    const toggleDeleteModal = () => setShowDeleteModal(!showDeleteModal)

    const [discardChangesModal, setDiscardChangesModal] = useState(false)
    const toggleDiscardChangesModal = () =>
        setDiscardChangesModal(!discardChangesModal)

    const discardChanges = () => {
        setWidgetEditing(null)
        setRows(addUniqueGuids(entity.rows))
        setDiscardChangesModal(false)
        setTimeout(() => {
            setIsDirty(false)
        }, 0)
    }

    const [leavePageModal, setLeavePageModal] = useState(false)
    const toggleLeavePageModal = () => setLeavePageModal(!leavePageModal)

    const leavePage = () => {
        navigate(goBackUrl)
    }

    const {
        handleSubmit,
        formState: { isSubmitting },
    } = useForm()

    const [editPage] = useMutation(EDIT_PAGE)

    const refetchQueries = ['PageList', 'PageEdit']

    const onSubmit = async ({ isDraft }) => {
        const timePublished = isDraft
            ? null
            : entity?.timePublished
              ? entity.timePublished
              : new Date().toISOString()

        setErrors()

        await editPage({
            variables: {
                input: {
                    guid,
                    rows: removeUniqueGuids(rows),
                    timePublished,
                    accessControl: isDraft
                        ? null
                        : sanitizeAccessControl(accessControl),
                },
            },
            refetchQueries,
        })
            .then(() => {
                setIsDirty(false)
            })
            .catch((errors) => {
                setErrors(errors)
            })
    }

    const onError = (errors) => {
        return new Promise((resolve, reject) => {
            reject(new Error(errors))
        })
    }

    const submitForm = (options) => {
        handleSubmit(() => onSubmit(options), onError)()
    }

    const handleAddRow = () => {
        setRows([
            ...rows,
            {
                isFullWidth: false,
                backgroundColor: '',
                columns: [{ width: [12], widgets: [] }],
                rowGuid: getUniqueId('row'),
            },
        ])
        window.setTimeout(() => {
            const btn = document.getElementById(`${rows.length}-0-add-widget`)
            btn && btn.focus()
        }, 0)
    }

    const saveWidget = (settings) => {
        const { rowIndex, columnIndex, index } = widgetEditing
        setRows(
            produce((newState) => {
                newState[rowIndex].columns[columnIndex].widgets[
                    index
                ].settings = settings
            }),
        )
        closeWidgetEditing()
    }

    const handleDragStart = (start, provided) => {
        const { type } = start

        const objectType =
            type === 'ROW' ? t('page.this-row') : t('page.this-widget')

        provided.announce(t('page.use-arrow-keys-to-move', { objectType }))

        setIsDragging(type)
    }

    const handleDragUpdate = (update, provided) => {
        const { source, destination, type } = update
        const objectType =
            type === 'ROW' ? t('page.the-row') : t('page.the-widget')

        if (!destination) return

        const confirmText = t('page.confirm-with-spacebar-cancel-with-escape')

        if (type === 'WIDGET') {
            const sourceIndex = source.droppableId.split('-')
            const destinationIndex = destination.droppableId.split('-')
            const destinationRow = Number(destinationIndex[0]) + 1
            const destinationColumn = Number(destinationIndex[1]) + 1

            if (sourceIndex[0] !== destinationIndex[0]) {
                provided.announce(
                    `${t('page.item-moved-to-row', {
                        objectType,
                        destinationRow,
                    })} ${confirmText}`,
                )
                return
            }

            if (sourceIndex[1] !== destinationIndex[1]) {
                provided.announce(
                    `${t('page.item-moved-to-column', {
                        objectType,
                        destinationColumn,
                    })} ${confirmText}`,
                )
                return
            }

            if (source.index !== destination.index) {
                provided.announce(
                    `${t('page.item-moved-to-position', {
                        objectType,
                        destinationPosition: destination.index + 1,
                    })} ${confirmText}`,
                )
                return
            }
        }

        if (type === 'ROW' && source.index !== destination.index) {
            provided.announce(
                `${t('page.item-moved-to-position', {
                    objectType,
                    destinationPosition: destination.index + 1,
                })} ${confirmText}`,
            )
            return
        }

        // Same position (after moving around)
        provided.announce(
            `${t('page.item-not-moved', { objectType })} ${confirmText}`,
        )
    }

    const handleDragEnd = (result, provided) => {
        const { type, source, destination, reason } = result
        if (type === 'ROW') {
            if (
                reason === 'CANCEL' ||
                !destination ||
                source?.index === destination?.index
            ) {
                provided.announce(
                    t('page.item-not-moved', {
                        objectType: t('page.the-row'),
                    }),
                )
            } else {
                provided.announce(
                    t('page.row-moved-from-to', {
                        sourcePosition: source.index + 1,
                        destinationPosition: destination.index + 1,
                    }),
                )
            }
        } else if (type === 'WIDGET') {
            if (reason === 'CANCEL' || !destination) {
                provided.announce(
                    t('page.item-not-moved', {
                        objectType: t('page.the-widget'),
                    }),
                )
            } else {
                const sourceIndex = source.droppableId.split('-')
                const destinationIndex = destination.droppableId.split('-')
                const sourceRow = Number(sourceIndex[0]) + 1
                const sourceColumn = Number(sourceIndex[1]) + 1
                const destinationRow = Number(destinationIndex[0]) + 1
                const destinationColumn = Number(destinationIndex[1]) + 1

                provided.announce(
                    t('page.widget-moved-from-to', {
                        sourceRow,
                        sourceColumn,
                        sourcePosition: source.index + 1,
                        destinationRow,
                        destinationColumn,
                        destinationPosition: destination.index + 1,
                    }),
                )
            }
        }

        setIsDragging(false)

        if (!destination) return

        const sourceIndex = source.index
        const destinationIndex = destination.index

        if (type === 'ROW') {
            const draggedRow = rows[sourceIndex]

            setRows(
                produce((newState) => {
                    newState.splice(sourceIndex, 1)
                    newState.splice(destinationIndex, 0, draggedRow)
                }),
            )
        }

        if (type === 'WIDGET') {
            const sourceIdArray = source.droppableId.split('-')
            const sourceRowIndex = parseInt(sourceIdArray[0], 10)
            const sourceColumnIndex = parseInt(sourceIdArray[1], 10)

            const destinationIdArray = destination.droppableId.split('-')
            const destinationRowIndex = parseInt(destinationIdArray[0], 10)
            const destinationColumnIndex = parseInt(destinationIdArray[1], 10)

            const draggedWidget =
                rows[sourceRowIndex].columns[sourceColumnIndex].widgets[
                    sourceIndex
                ]

            setRows(
                produce((newState) => {
                    newState[sourceRowIndex].columns[
                        sourceColumnIndex
                    ].widgets.splice(sourceIndex, 1)
                    newState[destinationRowIndex].columns[
                        destinationColumnIndex
                    ].widgets.splice(destinationIndex, 0, draggedWidget)
                }),
            )
        }
    }

    const handleAfterDelete = () => {
        window.location.href = '/pages'
    }

    const isEditingWidget = !!widgetEditing

    return (
        <ErrorBoundary>
            <PageEditorContext.Provider
                value={{
                    isDragging,
                    setRows,
                    widgetEditing,
                    setWidgetEditing,
                    group,
                    site,
                }}
            >
                <Document title={title} />

                {/* Use Pleio theme for some parts to distinguish editor from normal page elements */}
                <ThemeProvider theme="pleio">
                    <Header $isEditingWidget={isEditingWidget}>
                        <Container className="PageHeaderContainer">
                            <IconButton
                                size="large"
                                variant="secondary"
                                as={isDirty ? null : Link}
                                to={isDirty ? null : goBackUrl}
                                onClick={isDirty ? toggleLeavePageModal : null}
                                tooltip={t('action.go-back')}
                                disabled={isSubmitting || isEditingWidget}
                            >
                                <BackIcon />
                            </IconButton>
                            <div>
                                <H4
                                    style={{
                                        display: 'flex',
                                        alignItems: 'center',
                                    }}
                                >
                                    {title}
                                    <ItemStatusTag
                                        entity={entity}
                                        style={{ marginLeft: '4px' }}
                                    />
                                </H4>
                                {timeUpdated && (
                                    <Text
                                        size="tiny"
                                        variant="grey"
                                        style={{ marginBottom: '2px' }}
                                    >
                                        {t('global.last-saved')}:{' '}
                                        <DisplayDate
                                            date={timeUpdated}
                                            type="timeSince"
                                            placement="right"
                                        />
                                    </Text>
                                )}
                            </div>
                            <Flexer
                                gutter="small"
                                style={{ marginLeft: 'auto' }}
                            >
                                {!isArchived && (
                                    <Flexer>
                                        {isDirty && (
                                            <Button
                                                size="normal"
                                                variant="tertiary"
                                                onClick={
                                                    toggleDiscardChangesModal
                                                }
                                                disabled={
                                                    isSubmitting ||
                                                    isEditingWidget
                                                }
                                            >
                                                {t('page.discard-changes')}
                                            </Button>
                                        )}
                                        {!isPublished && (
                                            <Button
                                                size="normal"
                                                variant="secondary"
                                                disabled={
                                                    isEditingWidget || !isDirty
                                                }
                                                loading={isSubmitting}
                                                onClick={() =>
                                                    submitForm({
                                                        isDraft: true,
                                                    })
                                                }
                                            >
                                                {t('action.save-draft')}
                                            </Button>
                                        )}
                                        <Button
                                            size="normal"
                                            variant="primary"
                                            disabled={
                                                isEditingWidget ||
                                                (isPublished && !isDirty)
                                            }
                                            loading={isSubmitting}
                                            onClick={() => {
                                                if (isPublished) {
                                                    submitForm({
                                                        isDraft: false,
                                                    })
                                                } else {
                                                    setShowAccessModal(true)
                                                }
                                            }}
                                        >
                                            {isPublished
                                                ? isDirty
                                                    ? t('action.save')
                                                    : t('action.saved')
                                                : t('action.publish')}
                                        </Button>
                                    </Flexer>
                                )}

                                {showItemsActions && (
                                    <ItemActions
                                        entity={entity}
                                        canDuplicate
                                        canChangeAccess={isPublished}
                                        onTogglePageSettings={
                                            toggleSettingsModal
                                        }
                                        onAfterArchive={discardChanges}
                                        refetchQueries={refetchQueries}
                                        disabled={
                                            isSubmitting || isEditingWidget
                                        }
                                    />
                                )}
                            </Flexer>
                        </Container>
                    </Header>

                    <SettingsModal
                        isVisible={showSettingsModal}
                        onClose={toggleSettingsModal}
                        entity={entity}
                        site={site}
                        viewer={viewer}
                    />

                    <ConfirmAccessModal
                        showModal={showAccessModal}
                        onConfirm={() => {
                            setShowAccessModal(false)
                            submitForm({
                                isDraft: false,
                            })
                        }}
                        onCancel={() => setShowAccessModal(false)}
                        accessControl={accessControl}
                        setAccessControl={setAccessControl}
                        group={group}
                    />

                    <Modal
                        isVisible={showDeleteModal}
                        onClose={toggleDeleteModal}
                        title={t('entity-cms.delete')}
                        size="small"
                    >
                        <DeleteForm
                            entity={entity}
                            onClose={toggleDeleteModal}
                            title={t('entity-cms.delete')}
                            afterDelete={handleAfterDelete}
                            refetchQueries={['PageList']}
                        />
                    </Modal>

                    <WidgetSettings
                        site={site}
                        entity={widgetEditing}
                        tagCategories={tagCategories}
                        group={group}
                        onClose={closeWidgetEditing}
                        onSave={saveWidget}
                    />

                    <Modal
                        size="small"
                        isVisible={discardChangesModal}
                        title={t('page.discard-changes')}
                        onClose={toggleDiscardChangesModal}
                    >
                        {t('page.discard-changes-confirm')}
                        <Flexer mt>
                            <Button
                                size="normal"
                                variant="tertiary"
                                onClick={toggleDiscardChangesModal}
                            >
                                {t('action.no-cancel')}
                            </Button>
                            <Button
                                size="normal"
                                variant="warn"
                                onClick={discardChanges}
                            >
                                {t('action.yes-discard')}
                            </Button>
                        </Flexer>
                    </Modal>

                    <Modal
                        size="small"
                        isVisible={leavePageModal}
                        title={t('page.leave-page')}
                        onClose={toggleLeavePageModal}
                    >
                        {t('warning.discard-unsaved-changes')}
                        <Flexer mt>
                            <Button
                                size="normal"
                                variant="tertiary"
                                onClick={toggleLeavePageModal}
                            >
                                {t('action.no-cancel')}
                            </Button>
                            <Button
                                size="normal"
                                variant="warn"
                                onClick={leavePage}
                            >
                                {t('action.yes-discard')}
                            </Button>
                        </Flexer>
                    </Modal>
                </ThemeProvider>

                <Wrapper
                    $isFooter={isFooter}
                    $hasRows={rows.length > 0}
                    $isDragging={isDragging}
                    $isEditingWidget={isEditingWidget}
                    $isSubmitting={isSubmitting}
                >
                    {errors && (
                        <ErrorWrapper>
                            <Errors errors={errors} />
                        </ErrorWrapper>
                    )}

                    <DragDropContext
                        onDragStart={handleDragStart}
                        onDragUpdate={handleDragUpdate}
                        onDragEnd={handleDragEnd}
                    >
                        <Droppable
                            droppableId="droppable-rows"
                            type="ROW"
                            direction="vertical"
                        >
                            {(provided) => (
                                <div
                                    ref={provided.innerRef}
                                    {...provided.droppableProps}
                                    data-guid={guid}
                                >
                                    {rows.map((row, index) => (
                                        <Row
                                            key={index}
                                            rowIndex={index}
                                            entity={row}
                                        />
                                    ))}
                                    {provided.placeholder}
                                </div>
                            )}
                        </Droppable>
                    </DragDropContext>

                    {!isDragging && !widgetEditing && (
                        <button
                            type="button"
                            onClick={handleAddRow}
                            className="PageAddRow"
                        >
                            <div className="PageAddRowButton">
                                <AddIcon />
                                <span style={{ marginTop: '8px' }}>
                                    {t('page.add-row')}
                                </span>
                            </div>
                        </button>
                    )}

                    <CopyItem
                        isVisible={showCopyPageModal}
                        entity={entity}
                        onClose={() => setShowCopyPageModal(false)}
                    />
                </Wrapper>
            </PageEditorContext.Provider>
        </ErrorBoundary>
    )
}

const EDIT_PAGE = gql`
    mutation EditPage($input: editPageInput!) {
        editPage(input: $input) {
            entity {
                guid
                ... on Page {
                    title
                    timeUpdated
                    rows {
                        ${rowsViewFragment}
                    }
                ${accessControlFragment}
                }
            }
        }
    }
`

export default PageEditor
