import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useLocation, useNavigate } from 'react-router-dom'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { formatISO } from 'date-fns'
import { sanitizeTagCategories } from 'helpers'
import compose from 'lodash.flowright'

import ActionContainer from 'js/components/ActionContainer/ActionContainer'
import CustomTagsField from 'js/components/CustomTagsField'
import ConfirmAccessModal from 'js/components/EntityActions/components/ConfirmAccessModal'
import RequiredFieldsMessage from 'js/components/EntityActions/components/RequiredFieldsMessage'
import { sanitizeAccessControl } from 'js/components/Form/AccessControlField'
import FormItem from 'js/components/Form/FormItem'
import { Container } from 'js/components/Grid/Grid'
import HideVisually from 'js/components/HideVisually/HideVisually'
import RadioFields from 'js/components/RadioField/RadioFields'
import Setting from 'js/components/Setting'
import Spacer from 'js/components/Spacer/Spacer'
import TagCategoriesField from 'js/components/TagCategories/TagCategoriesField'
import NotFound from 'js/core/NotFound'
import useCustomTags from 'js/page/Widget/hooks/useCustomTags'

import Errors from '../components/Errors'

const AddEditForm = ({
    data,
    mutateAdd,
    mutateEdit,
    title,
    entity,
    containerGuid,
    group,
}) => {
    const { t } = useTranslation()
    const navigate = useNavigate()
    const location = useLocation()

    const [mutationErrors, setErrors] = useState()

    const [showAccessModal, setShowAccessModal] = useState(false)

    const [categoryTags, setCategoryTags] = useState(
        entity?.tagCategories || [],
    )

    const { customTags, handleAddTags, handleRemoveTags } = useCustomTags(
        entity?.tags,
    )

    const defaultAccessIds = {
        1: 'loggedIn',
        2: 'public',
        4: 'group',
    }
    const defaultGroupAccessId = group?.defaultReadAccessId
    const defaultGroupAccessType =
        defaultGroupAccessId && defaultAccessIds[defaultGroupAccessId]

    const [accessControl, setAccessControl] = useState(
        entity?.accessControl ||
            (defaultGroupAccessType
                ? [
                      {
                          type: defaultGroupAccessType,
                          grant: 'read',
                      },
                  ]
                : []),
    )

    const [pageType, setPageType] = useState(
        entity?.pageType || (containerGuid ? 'text' : 'campagne'),
    ) // when editing, we check entity for the actual pageType

    const handleChangePageType = (evt) => {
        if (getValues('richDescription')) {
            if (confirm(t('warning.discard-unsaved-changes'))) {
                resetField('richDescription')
                setPageType(evt.target.value)
            }
        } else {
            setPageType(evt.target.value)
        }
    }

    const handleClose = () => {
        navigate(
            location?.state?.prevPathname ||
                (entity && entity.url) ||
                (group ? `${group.url}/pages` : '/pages'),
            { replace: true },
        )
    }

    const defaultValues = {
        title: entity?.title || null,
        pageType: null,
        richDescription: entity?.richDescription || null,
        isDraft: entity?.statusPublished === 'draft' || false,
        isRecommendedInSearch: entity?.isRecommendedInSearch || false,
    }

    const {
        control,
        handleSubmit,
        formState: { errors, isValid, isSubmitting },
        setValue,
        getValues,
        resetField,
    } = useForm({
        mode: 'onChange',
        defaultValues,
    })

    const onSubmit = async ({
        title,
        richDescription,
        isDraft,
        isRecommendedInSearch,
    }) => {
        const timePublished = entity?.timePublished
            ? entity.timePublished
            : isDraft
              ? null
              : formatISO(new Date())

        const groupGuid = group?.guid

        const input = {
            ...(entity
                ? { guid: entity.guid }
                : {
                      pageType: group
                          ? 'campagne'
                          : containerGuid
                            ? 'text'
                            : pageType,
                  }),
            ...(containerGuid ? { containerGuid } : {}),
            ...(groupGuid ? { groupGuid } : {}),
            title,
            richDescription,
            accessControl: sanitizeAccessControl(accessControl),
            tagCategories: sanitizeTagCategories(
                site.tagCategories,
                categoryTags,
            ),
            tags: customTags.toJS(),
            timePublished,
            isRecommendedInSearch,
        }

        setErrors()

        const mutate = entity ? mutateEdit : mutateAdd

        await mutate({
            variables: {
                input,
            },
            refetchQueries: ['AdminPages'],
        })
            .then(({ data }) => {
                if (entity) {
                    navigate(
                        location?.state?.prevPathname ||
                            data.editPage.entity.url,
                        { replace: true },
                    )
                } else {
                    navigate(
                        data.addPage.entity.pageType === 'text'
                            ? data.addPage.entity.url
                            : `/page/edit/${data.addPage.entity.guid}`,
                        {
                            replace: true,
                        },
                    )
                }
            })
            .catch((errors) => {
                console.error(errors)
                setErrors(errors)
            })
    }

    const onError = (errors) => {
        return new Promise((resolve, reject) => {
            reject(new Error(errors))
        })
    }

    const submitForm = handleSubmit(onSubmit, onError)

    const { error, loading, site, viewer } = data
    if (error) return <NotFound />
    if (loading) return null

    const isPublished = entity?.statusPublished === 'published'

    return (
        <ActionContainer
            title={title}
            publish={() => {
                if (isPublished) {
                    setValue('isDraft', false)
                    submitForm()
                } else {
                    setShowAccessModal(true)
                }
            }}
            onClose={handleClose}
            isValid={isValid}
            isPublished={isPublished}
            canSaveAsDraft={true}
            saveAsDraft={() => {
                setValue('isDraft', true)
                submitForm()
            }}
            isSavingAsDraft={isSubmitting && getValues('isDraft')}
            isPublishing={isSubmitting && !getValues('isDraft')}
        >
            <form>
                <HideVisually aria-hidden>
                    <FormItem
                        control={control}
                        type="switch"
                        name="isDraft"
                        tabIndex="-1"
                    />
                </HideVisually>

                <Container size="small">
                    <Spacer>
                        <Errors errors={mutationErrors} />

                        <FormItem
                            control={control}
                            type="text"
                            name="title"
                            title={t('form.title')}
                            required
                            errors={errors}
                        />

                        {!entity && !containerGuid && !group ? (
                            <RadioFields
                                name="pageType"
                                legend={t('global.type')}
                                options={[
                                    {
                                        value: 'campagne',
                                        label: t('page.type-widget'),
                                    },
                                    {
                                        value: 'text',
                                        label: t('page.type-text'),
                                    },
                                ]}
                                value={pageType}
                                onChange={handleChangePageType}
                            />
                        ) : null}

                        <FormItem
                            key={pageType} // force rerender on pageType update
                            control={control}
                            type="rich"
                            name="richDescription"
                            title={t('form.description')}
                            height="large"
                            options={{
                                textFormat: true,
                                textStyle: true,
                                textLink: true,
                                textQuote: true,
                                textList: true,
                                insertMedia: true,
                                insertAccordion: true,
                                insertButton: pageType === 'text',
                                insertTable: true,
                                textAnchor: true,
                                textCSSClass: true,
                                textLanguage: true,
                            }}
                            errors={errors}
                        />

                        <TagCategoriesField
                            name="filters"
                            tagCategories={site.tagCategories}
                            value={categoryTags}
                            setTags={setCategoryTags}
                        />

                        {site.customTagsAllowed && (
                            <CustomTagsField
                                name="customTags"
                                value={customTags}
                                onAddTag={handleAddTags}
                                onRemoveTag={handleRemoveTags}
                            />
                        )}

                        {viewer?.isAdmin && (
                            <Setting
                                subtitle="form.recommended-search-result"
                                helper="form.recommended-search-result-helper"
                                htmlFor="isRecommendedInSearch"
                            >
                                <FormItem
                                    control={control}
                                    type="switch"
                                    name="isRecommendedInSearch"
                                    size="small"
                                />
                            </Setting>
                        )}

                        <RequiredFieldsMessage />
                    </Spacer>
                </Container>
            </form>

            <ConfirmAccessModal
                showModal={showAccessModal}
                onConfirm={() => {
                    setShowAccessModal(false)
                    setValue('isDraft', false)
                    submitForm()
                }}
                onCancel={() => setShowAccessModal(false)}
                accessControl={accessControl}
                setAccessControl={setAccessControl}
                group={group}
            />
        </ActionContainer>
    )
}

const QUERY = gql`
    query editEntity {
        site {
            guid
            customTagsAllowed
            tagCategories {
                name
                values
            }
        }
        viewer {
            guid
            isAdmin
        }
    }
`

const ADDMUTATION = gql`
    mutation AddPage($input: addPageInput!) {
        addPage(input: $input) {
            entity {
                guid
                ... on Page {
                    url
                    pageType
                }
            }
        }
    }
`

const EDITMUTATION = gql`
    mutation EditPage($input: editPageInput!) {
        editPage(input: $input) {
            entity {
                guid
                ... on Page {
                    url
                }
            }
        }
    }
`

export default compose(
    graphql(ADDMUTATION, { name: 'mutateAdd' }),
    graphql(EDITMUTATION, { name: 'mutateEdit' }),
    graphql(QUERY),
)(AddEditForm)
