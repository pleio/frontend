import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { useDebounce, useIsMount } from 'helpers'

import DisplayAccessLevel from 'js/components/DisplayAccessLevel'
import DisplayDate from 'js/components/DisplayDate'
import accessControlFragment from 'js/components/EntityActions/fragments/accessControl'
import FetchMore from 'js/components/FetchMore'
import { H4 } from 'js/components/Heading'
import ItemActions from 'js/components/Item/ItemActions/ItemActions'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Spacer from 'js/components/Spacer/Spacer'
import Table from 'js/components/Table'
import Text from 'js/components/Text/Text'
import { iconViewFragment } from 'js/lib/fragments/icon'
import PageListBreadcrumb from 'js/page/PageListBreadcrumb'

import PagesFilters from './PageListFilters'

const useTableHeaders = () => {
    const { t } = useTranslation()

    return [
        {
            name: 'title',
            label: t('form.title'),
            style: { minWidth: 200 },
        },
        {
            name: 'contentType',
            label: t('global.type'),
            style: { minWidth: 60 },
        },
        {
            name: 'ownerName',
            label: t('global.owner'),
            style: { minWidth: 80 },
        },
        {
            options: [
                {
                    name: 'timeUpdated',
                    label: t('global.last-edited'),
                    defaultOrderDirection: 'desc',
                },
                {
                    name: 'timePublished',
                    label: t('global.published'),
                    defaultOrderDirection: 'desc',
                },
                {
                    name: 'lastSeen',
                    label: t('admin.content-last-viewed'),
                    defaultOrderDirection: 'desc',
                },
            ],
            align: 'right',
            style: { minWidth: 100 },
        },
        {
            label: t('permissions.read'),
            name: 'readAccess',
            align: 'right',
            style: {
                width: 40,
                paddingRight: 0,
            },
        },
        {
            style: { width: 40 },
        },
    ]
}

const PageList = ({ rootUrl }) => {
    const { parentGuid, groupGuid } = useParams()

    const { t } = useTranslation()
    const tableHeaders = useTableHeaders()

    const [searchInput, setSearchInput] = useState('')
    const q = useDebounce(searchInput, 200)

    const [queryLimit, setQueryLimit] = useState(50)
    const [user, setUser] = useState('')
    const [tagFilter, setTagFilter] = useState([])
    const [dateFrom, setDateFrom] = useState('')
    const [dateTo, setDateTo] = useState('')
    const [statusPublished, setStatusPublished] = useState([])

    const [orderBy, setOrderBy] = useState('title')
    const [orderDirection, setOrderDirection] = useState('asc')

    const addDay = (date) => {
        const newDate = new Date(date)
        newDate.setDate(newDate.getDate() + 1)
        return newDate
    }

    const { loading, data, fetchMore, refetch } = useQuery(GET_ADMIN_PAGES, {
        variables: {
            q,
            // When searching, don't filter by parent/child relationship
            parentGuid: q ? '' : parentGuid || '1',
            containerGuid: groupGuid || '1',
            offset: 0,
            limit: queryLimit,
            userGuid: user,
            tagCategories: tagFilter.length ? tagFilter : null,
            dateFrom: dateFrom || null,
            dateTo: dateTo ? addDay(dateTo) : null,
            orderBy,
            orderDirection,
            statusPublished,
        },
        fetchPolicy: 'cache-and-network',
    })

    const isMount = useIsMount()

    useEffect(() => {
        if (isMount) {
            refetch()
        }
    })

    useEffect(() => {
        refetch()
    }, [orderBy, orderDirection, refetch])

    return (
        <Spacer>
            <PageListBreadcrumb guid={parentGuid} rootUrl={rootUrl} />

            <PagesFilters
                searchInput={searchInput}
                setSearchInput={setSearchInput}
                tagCategories={data?.site?.tagCategories}
                tagFilter={tagFilter}
                setTagFilter={setTagFilter}
                user={user}
                setUser={setUser}
                dateFrom={dateFrom}
                setDateFrom={setDateFrom}
                dateTo={dateTo}
                setDateTo={setDateTo}
                statusPublished={statusPublished}
                setStatusPublished={setStatusPublished}
            />

            {loading ? (
                <LoadingSpinner />
            ) : data.entities.edges.length === 0 ? (
                <Text textAlign="center">{t('admin.pages-not-found')}</Text>
            ) : (
                <div>
                    <H4 role="status" style={{ margin: '16px 0 4px' }}>
                        {t('page.count-pages', {
                            count: data.entities.total,
                        })}
                    </H4>

                    <Table
                        rowHeight="normal"
                        headers={tableHeaders}
                        orderBy={orderBy}
                        orderDirection={orderDirection}
                        setOrderBy={setOrderBy}
                        setOrderDirection={setOrderDirection}
                        tableWidth={520}
                    >
                        <FetchMore
                            as="tbody"
                            edges={data.entities.edges}
                            getMoreResults={(data) => data.entities.edges}
                            fetchMore={fetchMore}
                            fetchCount={25}
                            setLimit={setQueryLimit}
                            maxLimit={data.entities.total}
                            resultsMessage={t('global.result', {
                                count: data.entities.total,
                            })}
                            fetchMoreButtonHeight={48}
                        >
                            {data.entities.edges.map((page) => {
                                const {
                                    guid,
                                    url,
                                    title,
                                    pageType,
                                    lastSeen,
                                    timePublished,
                                    timeUpdated,
                                    accessId,
                                    hasChildren,
                                    children,
                                    group,
                                } = page

                                const selectedDateSorting =
                                    orderBy === 'lastSeen'
                                        ? lastSeen
                                        : orderBy === 'timePublished'
                                          ? timePublished
                                          : timeUpdated

                                return (
                                    <tr key={guid}>
                                        <td>
                                            <Link
                                                to={url}
                                                className="TableLink"
                                            >
                                                {title}
                                            </Link>
                                            {hasChildren && (
                                                <>
                                                    {' ― '}
                                                    <Link
                                                        to={`${rootUrl}/${guid}`}
                                                        className="TableLink"
                                                    >
                                                        {t(
                                                            'page.count-subpages',
                                                            {
                                                                count: children.length,
                                                            },
                                                        )}
                                                    </Link>
                                                </>
                                            )}
                                        </td>
                                        <td>
                                            {pageType === 'text'
                                                ? t('page.type-text')
                                                : t('page.type-widget')}
                                        </td>
                                        <td>{page.owner.name}</td>
                                        <td
                                            style={{
                                                textAlign: 'right',
                                            }}
                                        >
                                            <DisplayDate
                                                date={selectedDateSorting}
                                            />
                                        </td>
                                        <td
                                            style={{
                                                padding: 0,
                                            }}
                                        >
                                            <DisplayAccessLevel
                                                accessId={accessId}
                                                style={{
                                                    display: 'flex',
                                                    alignItems: 'center',
                                                    justifyContent: 'center',
                                                    height: '100%',
                                                }}
                                            />
                                        </td>
                                        <td>
                                            <ItemActions
                                                entity={page}
                                                onEdit={
                                                    group
                                                        ? `${group.url}/page/edit/${guid}`
                                                        : `/page/edit/${guid}`
                                                }
                                                canChangeAccess
                                                refetchQueries={['AdminPages']}
                                            />
                                        </td>
                                    </tr>
                                )
                            })}
                        </FetchMore>
                    </Table>
                </div>
            )}
        </Spacer>
    )
}

const GET_ADMIN_PAGES = gql`
    query AdminPages(
        $q: String!
        $parentGuid: String
        $containerGuid: String
        $limit: Int!
        $offset: Int!
        $userGuid: String
        $tagCategories: [TagCategoryInput!]
        $orderBy: OrderBy
        $orderDirection: OrderDirection # $dateFrom: String # $dateTo: String
        $statusPublished: [StatusPublished]
    ) {
        entities(
            q: $q
            parentGuid: $parentGuid
            containerGuid: $containerGuid
            subtypes: ["landingpage", "page"]
            limit: $limit
            offset: $offset
            userGuid: $userGuid
            tagCategories: $tagCategories
            orderBy: $orderBy
            orderDirection: $orderDirection # dateFrom: $dateFrom # dateTo: $dateTo
            statusPublished: $statusPublished
        ) {
            total
            edges {
                guid
                ... on Page {
                    pageType
                    url
                    accessId
                    canEdit
                    canArchiveAndDelete
                    preventDeleteAndArchive
                    statusPublished
                    timePublished
                    timeUpdated
                    lastSeen
                    title
                    owner {
                        guid
                        name
                    }
                    hasChildren
                    children {
                        guid
                    }
                    group {
                        guid
                        ... on Group {
                            url
                            name
                            ${iconViewFragment}
                            isClosed
                        }
                    }
                    ${accessControlFragment}
                }
            }
        }
        site {
            guid
            tagCategories {
                name
                values
            }
        }
    }
`

export default PageList
