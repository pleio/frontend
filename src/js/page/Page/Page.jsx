import React, { useContext, useEffect } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import { useIsMount } from 'helpers'
import styled from 'styled-components'

import HideVisually from 'js/components/HideVisually/HideVisually'
import globalStateContext from 'js/lib/withGlobalState/globalStateContext'
import PageBreadcrumb from 'js/page/PageBreadcrumb'

import PageRows from './PageRows'

const Wrapper = styled.div`
    flex-grow: 1;
    display: flex;
    flex-direction: column;

    .PageRow:last-child {
        flex-grow: 1;
    }
`

const Page = ({ site, entity }) => {
    const isMount = useIsMount()

    const {
        globalState: { editMode },
    } = useContext(globalStateContext)

    const navigate = useNavigate()
    const location = useLocation()

    const { guid, title, rows, group, canEdit } = entity

    useEffect(() => {
        if (editMode && !isMount) {
            navigate(`/page/edit/${guid}`, {
                state: { prevPathname: location.pathname },
            })
        }
        // eslint-disable-next-line
    }, [editMode])

    return (
        <Wrapper data-guid={guid}>
            <HideVisually as="h1">{title}</HideVisually>

            <PageBreadcrumb guid={guid} />

            <PageRows
                site={site}
                guid={guid}
                rows={rows}
                group={group}
                canEdit={canEdit}
            />
        </Wrapper>
    )
}

export default Page
