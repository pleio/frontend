const userSettings = `
    userSettings {
        key
        value
    }
`

const getRowsFields = (includeUserSettings) => `
    __typename @include(if: false)
    isFullWidth
    backgroundColor
    columns {
        __typename @include(if: false)
        width
        widgets {
            __typename @include(if: false)
            guid
            type
            ${includeUserSettings ? userSettings : ''}
            settings {
                __typename @include(if: false)
                key
                value
                richDescription
                attachment {
                    id
                    url
                    name
                    mimeType
                }
                links {
                    __typename @include(if: false)
                    id
                    title
                    description
                    url
                    image {
                        id
                        url
                    }
                    imageAlt
                    buttonText
                }
            }
        }
    }
`

export const rowsEditFragment = getRowsFields(false)

export const rowsViewFragment = getRowsFields(true)
