import React from 'react'
import { useTheme } from 'styled-components'

import { Col, Container, Row } from 'js/components/Grid/Grid'
import Widget from 'js/page/Widget/Widget'

import PageRow from './Row'

const PageRows = ({ site, guid, rows, group = null, canEdit = false }) => {
    const theme = useTheme()

    return rows.map((row, rowIndex) => {
        const { isFullWidth, columns, backgroundColor } = row
        const numberOfColumns = columns.length
        const groupFirstRow = group && rowIndex === 0

        const rowBackgroundColor = backgroundColor || theme.color.grey[10]

        return (
            <PageRow
                key={rowIndex}
                backgroundColor={rowBackgroundColor}
                isFullWidth={isFullWidth && numberOfColumns === 1}
                noTopPadding={
                    rowIndex > 0
                        ? rows[rowIndex - 1].backgroundColor === backgroundColor // If two rows have the same background color..
                        : groupFirstRow || null // ..or if the first row in a group, less padding is needed.
                }
                className={`PageRow${
                    isFullWidth && numberOfColumns === 1
                        ? ' PageRowFullWidth'
                        : ''
                }
                        ${!group && rowIndex === 0 ? ' PageRowFirst' : ''}
                        `}
                // .PageRow is used to grow the height of the last row
                // .PageRowFullWidth is used to make Lead/Leader stretch to the side edges of the screen
                // .PageRowFirst is used to make Lead/Leader start at the top of the first row (not in group)
            >
                <Container className="PageRowContainer">
                    <Row $growContent $spacing={20}>
                        {columns.map((column, columnIndex) => {
                            const columnWidth = column.width[0]

                            return (
                                <Col
                                    key={`${rowIndex}-${columnIndex}`}
                                    tabletUp={columnWidth / 12}
                                    className="PageColumn"
                                >
                                    {column.widgets.map((widget, index) => {
                                        const widgetGuid =
                                            widget.guid ||
                                            `${guid}-${rowIndex}-${columnIndex}-${index}` // Fallback for widgets without guid
                                        return (
                                            <Widget
                                                key={widgetGuid}
                                                guid={widgetGuid}
                                                containerGuid={guid}
                                                site={site}
                                                entity={widget}
                                                group={group}
                                                canEditContainer={canEdit}
                                                columnWidth={columnWidth}
                                                rowBackgroundColor={
                                                    rowBackgroundColor
                                                }
                                            />
                                        )
                                    })}
                                </Col>
                            )
                        })}
                    </Row>
                </Container>
            </PageRow>
        )
    })
}

export default PageRows
