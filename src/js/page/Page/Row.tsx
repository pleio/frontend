import styled, { css } from 'styled-components'

import Section, { Props as SectionProps } from 'js/components/Section/Section'
import shouldForwardProp from 'js/lib/helpers/shouldForwardProp'

interface Props extends Partial<SectionProps> {
    isFullWidth: boolean
}

const Row = styled(Section).withConfig({
    shouldForwardProp: shouldForwardProp(['backgroundColor', 'isFullWidth']),
})<Props>`
    .PageColumn {
        > *:not(:last-child) {
            margin-bottom: 20px;
        }
    }

    ${(p) =>
        p.isFullWidth &&
        css`
            padding-left: 0 !important;
            padding-right: 0 !important;

            .PageRowContainer {
                max-width: none;
            }
        `};
`

export default Row
