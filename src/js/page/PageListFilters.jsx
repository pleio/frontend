import React from 'react'
import { useTranslation } from 'react-i18next'

import useStatusOptions from 'js/admin/hooks/useStatusOptions'
import UserField from 'js/components/EntityActions/Advanced/components/UserField.jsx'
// import DateField from 'js/components/DateField/DateField'
import FilterWrapper from 'js/components/FilterWrapper'
import Select from 'js/components/Select/Select'
import Textfield from 'js/components/Textfield/Textfield'
import TagCategoriesFilter from 'js/search/components/TagCategoriesFilter'

const PagesFilters = ({
    searchInput,
    setSearchInput,
    tagCategories,
    tagFilter,
    setTagFilter,
    user,
    setUser,
    statusPublished,
    setStatusPublished,
}) => {
    const { t } = useTranslation()

    // const today = new Date().toISOString()

    const statusOptions = useStatusOptions()

    return (
        <div>
            <FilterWrapper>
                {/* div is needed for FilterWrapper */}
                <div>
                    <Textfield
                        name="language"
                        label={t('form.search-by', {
                            attribute: t('form.title'),
                        })}
                        value={searchInput}
                        onChange={(evt) => setSearchInput(evt.target.value)}
                        onClear={() => setSearchInput('')}
                    />
                </div>

                <UserField
                    name="pages-filter-by-owner"
                    label={t('global.owner')}
                    value={user}
                    onChange={(user) => setUser(user?.guid || '')}
                />

                <Select
                    label={t('global.status')}
                    name="filter-by-status"
                    placeholder={t('filters.filter-by')}
                    options={statusOptions}
                    value={statusPublished}
                    onChange={setStatusPublished}
                    isMulti
                />

                {/* <DateField
                    name={`pages-date-from`}
                    label={t('search.date-from')}
                    value={dateFrom}
                    fromDate={dateFrom}
                    toDate={dateTo || today}
                    onChange={setDateFrom}
                    clearLabel={t('search.clear-date-from')}
                    isClearable
                />
                <DateField
                    name={`pages-date-to`}
                    label={t('search.date-to')}
                    value={dateTo}
                    fromDate={dateFrom}
                    toDate={today}
                    onChange={setDateTo}
                    clearLabel={t('search.clear-date-to')}
                    isClearable
                /> */}
            </FilterWrapper>

            <div style={{ marginTop: '5px' }}>
                <TagCategoriesFilter
                    name="pages-filter-by-tags"
                    tagCategories={tagCategories || []}
                    tagFilter={tagFilter}
                    setTagFilter={setTagFilter}
                />
            </div>
        </div>
    )
}

export default PagesFilters
