import React from 'react'
import { Route, Routes } from 'react-router-dom'

import NotFound from 'js/core/NotFound'

import Item from './Item'

export const PageRoutes = () => (
    <Routes>
        <Route path="/view/:guid" element={<Item />} />
        <Route path="/view/:guid/:slug" element={<Item />} />
        <Route
            path="/view/:containerGuid/:containerSlug/:guid"
            element={<Item />}
        />
        <Route element={<NotFound />} />
    </Routes>
)
