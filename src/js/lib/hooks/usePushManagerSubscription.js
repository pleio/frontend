import { useLayoutEffect } from 'react'

export const usePushManagerSubscription = (callback) => {
    const getSubscription = async () => {
        const registration = await navigator.serviceWorker.getRegistration('/')
        return registration.pushManager.getSubscription()
    }

    useLayoutEffect(() => {
        if (!('serviceWorker' in navigator)) return

        getSubscription().then((subscription) => {
            callback(!!subscription)
        })
    }, [callback])

    return { getSubscription }
}
