import { useTranslation } from 'react-i18next'

type Subtype = {
    contentName: string
    serverName: string
    editUrl: string
}

type TypeFilter = {
    value: string
    label: string
}

function sortLabels(a: TypeFilter, b: TypeFilter) {
    return a.label < b.label ? -1 : a.label > b.label ? 1 : 0
}

function getTypeFilters(subtypesMap: Record<string, Subtype>) {
    return Object.entries(subtypesMap).map(([value, label]) => ({
        value,
        label: label.contentName,
    }))
}

function filterSubtypes(
    requestedItems: string[],
    allSubTypes: Record<string, Subtype>,
) {
    return Object.fromEntries(
        Object.entries(allSubTypes).filter(([key]) =>
            requestedItems.includes(key),
        ),
    )
}

const useSubtypes = (requestedItems = []) => {
    const { t } = useTranslation()

    const allSubtypes = {
        blog: {
            contentName: t(`entity-blog.content-name`),
            serverName: 'blog',
            editUrl: 'blog',
        },
        discussion: {
            contentName: t(`entity-discussion.content-name`),
            serverName: 'discussion',
            editUrl: 'discussion',
        },
        episode: {
            contentName: t(`entity-episode.content-name`),
            serverName: 'episode',
            editUrl: 'episodes',
        },
        event: {
            contentName: t(`entity-event.content-name`),
            serverName: 'event',
            editUrl: 'events',
        },
        file: {
            contentName: t(`entity-file.content-name`),
            serverName: 'file',
            editUrl: 'files',
        },
        folder: {
            contentName: t(`entity-folder.content-name`),
            serverName: 'folder',
            editUrl: 'folders',
        },
        news: {
            contentName: t(`entity-news.content-name`),
            serverName: 'news',
            editUrl: 'news',
        },
        pad: {
            contentName: t(`entity-pad.content-name`),
            serverName: 'pad',
            editUrl: 'pads',
        },
        'page-text': {
            contentName: t(`entity-page-text.content-name`),
            serverName: 'page-text',
            editUrl: 'pages',
        },
        'page-widget': {
            contentName: t(`entity-cms.content-name`),
            serverName: 'page-widget',
            editUrl: 'pages',
        },
        podcast: {
            contentName: t(`entity-podcast.content-name`),
            serverName: 'podcast',
            editUrl: 'podcasts',
        },
        question: {
            contentName: t(`entity-question.content-name`),
            serverName: 'question',
            editUrl: 'questions',
        },
        statusupdate: {
            contentName: t(`entity-statusupdate.content-name`),
            serverName: 'statusupdate',
            editUrl: 'statusupdates',
        },
        task: {
            contentName: t(`entity-task.content-name`),
            serverName: 'task',
            editUrl: 'tasks',
        },
        wiki: {
            contentName: t(`entity-wiki.content-name`),
            serverName: 'wiki',
            editUrl: 'wiki',
        },
        'magazine-issue': {
            contentName: t('entity-magazine.issue-content-name'),
            serverName: 'magazine_issue',
            editUrl: 'magazines',
        },
        image: {
            contentName: t(`global.image`),
            serverName: 'image',
            editUrl: 'files',
        },
        group: {
            contentName: t(`entity-group.content-name`),
            serverName: 'group',
            editUrl: 'groups',
        },
        page: {
            contentName: t(`entity-cms.content-name`),
            serverName: 'page',
            editUrl: 'pages',
        },
        user: {
            contentName: t(`admin.user`),
            serverName: 'user',
            editUrl: '',
        },
    }

    const subtypes = requestedItems.length
        ? filterSubtypes(requestedItems, allSubtypes)
        : allSubtypes // Return all subtypes if no filter supplied

    const subtypeLabels = getTypeFilters(subtypes).sort(sortLabels)

    return { subtypes, subtypeLabels }
}

export default useSubtypes
