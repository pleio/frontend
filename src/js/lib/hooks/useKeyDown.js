import { useEffect } from 'react'

const useKeyDown = (keyMapping) => {
    useEffect(() => {
        const onKeyDown = (event) => {
            if (event.key in keyMapping) {
                keyMapping[event.key]()
            }
        }

        document.addEventListener('keydown', onKeyDown)

        return () => {
            document.removeEventListener('keydown', onKeyDown)
        }
    }, [keyMapping])
}

export { useKeyDown }
