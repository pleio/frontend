import React, { useEffect, useState } from 'react'
import { gql, useQuery } from '@apollo/client'
import { getLocalStorage, setLocalStorage, useIsMount } from 'helpers'
import { produce } from 'immer'

import globalStateContext from './globalStateContext'

const initialState = {
    inactiveWindow: false,
    inactiveUser: false,
    editMode: false,
    canEditPage: false,
    showChatOverlay: getLocalStorage('show-chat-overlay', false),
    chatOverlayExpanded: getLocalStorage('chat-overlay-expanded', false),
    activeMainThread: getLocalStorage('active-main-thread', null),
}

const GlobalStateProvider = ({ children }) => {
    const isMount = useIsMount()

    const { data } = useQuery(GET_VIEWER)

    const [state, setState] = useState({ globalState: initialState })

    const setGlobalState = (setFn) => {
        const globalState = produce(state.globalState, setFn)
        setState({ globalState })
    }

    useEffect(
        () => {
            if (data?.viewer) {
                setGlobalState((newState) => {
                    newState.isAdmin = data.viewer.isAdmin
                    newState.allowTextCssClasses =
                        data.viewer.allowTextCssClasses
                })
            }
        },
        // Adding `setGlobalState` to dependency array results in infinite loop
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [data],
    )

    useEffect(() => {
        if (isMount) return
        setLocalStorage(
            'show-chat-overlay',
            state?.globalState?.showChatOverlay,
        )
    }, [state?.globalState?.showChatOverlay])

    useEffect(() => {
        if (isMount) return
        setLocalStorage(
            'chat-overlay-expanded',
            state?.globalState?.chatOverlayExpanded,
        )
    }, [state?.globalState?.chatOverlayExpanded])

    useEffect(() => {
        if (isMount) return
        // Automatically expand and reset chat overlay when a thread is opened
        if (state?.globalState?.activeMainThread) {
            setGlobalState((newState) => {
                newState.subthreadGuid = null
                newState.chatNewThread = false
                newState.chatOverlayExpanded = true
                newState.showChatOverlay = true
            })
        }
        setLocalStorage(
            'active-main-thread',
            state?.globalState?.activeMainThread,
        )
    }, [state?.globalState?.activeMainThread])

    useEffect(() => {
        if (isMount) return
        if (state?.globalState?.chatNewThread) {
            setGlobalState((newState) => {
                newState.activeMainThread = null
                newState.subthreadGuid = null
                newState.chatOverlayExpanded = true
                newState.showChatOverlay = true
            })
        }
    }, [state?.globalState?.chatNewThread])

    if (!state.setGlobalState) {
        setState(
            produce((draft) => {
                draft.setGlobalState = setGlobalState
            }),
        )
    }

    return data ? (
        <globalStateContext.Provider value={state}>
            {children}
        </globalStateContext.Provider>
    ) : null
}

const GET_VIEWER = gql`
    query Viewer {
        viewer {
            guid
            isAdmin
            allowTextCssClasses: richTextEditorFeature(
                feature: allowTextCssClasses
            )
        }
    }
`

export default GlobalStateProvider
