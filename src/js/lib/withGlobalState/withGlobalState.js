import React, { useContext } from 'react'

import globalStateContext from './globalStateContext'

export default (WrappedComponent) => {
    const WithGlobalState = (props) => {
        const stateContextProps = useContext(globalStateContext)
        return <WrappedComponent {...props} {...stateContextProps} />
    }

    return WithGlobalState
}
