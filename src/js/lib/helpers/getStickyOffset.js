export default function getStickyOffset(element) {
    // Get height of all sticky's before current sticky
    const stickyElements = []
    let stopList = false
    document
        .querySelectorAll(
            '[data-sticky]:not([data-sticky-ignore="true"]):not([data-sticky-contained="true"])',
        )
        .forEach((el) => {
            if (el === element) {
                stopList = true
            } else if (!stopList) {
                stickyElements.push(el.clientHeight)
            }
        })
    if (stickyElements.length > 0) {
        // Add all the heights together to calculate offset
        return stickyElements.reduce((a, b) => a + b, 0)
    } else {
        return 0
    }
}
