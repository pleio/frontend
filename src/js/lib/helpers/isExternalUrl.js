export default function isExternalUrl(url) {
    if (!url) return false
    const isExternalSite =
        /^(http?:\/\/|https?:\/\/|ftp?:\/\/|tel?:\/\/|www.|\/\/www.|file?:\/\/|mailto:)/i
    const isLocalPathname = /\\\\/i

    return (
        isExternalSite.test(url) ||
        isLocalPathname.test(url) ||
        url.startsWith('/file/download')
    )
}
