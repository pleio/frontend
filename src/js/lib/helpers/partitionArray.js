export default function partitionArray(array, filter) {
    const pass = []
    const fail = []
    array.forEach((e, idx, arr) => (filter(e, idx, arr) ? pass : fail).push(e))
    return [pass, fail]
}
