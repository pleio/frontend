import { useLayoutEffect } from 'react'
import { useNavigationType } from 'react-router-dom'

import useSessionStorage from './useSessionStorage'

// sets scrollY position of window based on a setting condition, i.e. when api calls are done
// also sets the scroll position when unmounting, i.e. a user navigates to a different page
export default function useWindowScrollPosition(localStorageKey) {
    const navigationType = useNavigationType()

    const [scrollYStorage, setScrollYStorage] = useSessionStorage(
        localStorageKey,
        0,
    )

    // purely on un mount: store the scroll position the user was at to sessionStorage
    useLayoutEffect(() => {
        if (navigationType === 'POP' && scrollYStorage > 0) {
            setTimeout(() => {
                window.scrollTo({
                    top: Number(scrollYStorage),
                    left: 0,
                    behavior: 'auto',
                })
            }, 0)
        }

        return () => {
            setScrollYStorage(window.scrollY)
        }
    }, [])
}
