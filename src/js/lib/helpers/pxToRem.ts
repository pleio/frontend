/**
 * Convert pixel value to rem value
 */
export default function pxToRem(value: number) {
    return `${value / 16}rem`
}
