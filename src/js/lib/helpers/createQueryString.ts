// We ARE interested in value 0 (zero), so we can't just test `!!value`
const falsy = [undefined, null, false, '']

export default (paramObject: object) =>
    Object.entries(paramObject).reduce((acc, [key, value]) => {
        return !falsy.includes(value)
            ? (acc += `${acc === '' ? '?' : '&'}${key}=${encodeURIComponent(
                  value,
              )}`)
            : acc
    }, '')
