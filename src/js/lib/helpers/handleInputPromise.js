let timer = null

export default function handleInputPromise(onHandle, disableInput, showSaved) {
    // If promise is returned disable input
    if (onHandle && Promise.resolve(onHandle)) {
        // Disable input
        disableInput(true)

        // Re-enable input after promise has resolved or rejected
        return onHandle
            .then(() => {
                disableInput(false)

                // Show saved state if available
                if (showSaved) {
                    showSaved(true)

                    clearTimeout(timer)
                    timer = setTimeout(() => showSaved(false), 1500)
                }
            })
            .catch(() => {
                return disableInput(false)
            })
    }
}
