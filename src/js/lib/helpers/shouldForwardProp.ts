/**
 * A generic generator for `shouldForwardProp` functions. It accepts an array of
 * prop names that should NOT be forwarded to the DOM.
 */
export default function shouldForwardProp(props: string[]) {
    const forbidden = new Set(props)

    return (prop: string) => !forbidden.has(prop)
}
