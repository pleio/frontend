export default function getAttribute(name, object, defaultValue) {
    if (typeof object[name] !== 'undefined') {
        return object[name]
    }

    return defaultValue
}
