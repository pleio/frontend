import { useState } from 'react'
import { getLocalStorage, setLocalStorage } from 'helpers'

export default function useLocalStorage(
    key: string,
    initialValue: any = false,
    skipStorage: boolean = false,
) {
    const [value, setValue] = useState(initialValue)

    // State to store our value
    // Pass initial state function to useState so logic is only executed once
    const [storedValue, setStoredValue] = useState(
        getLocalStorage(key, initialValue),
    )

    // Return a wrapped version of useState's setter function that ...
    // ... persists the new value to localStorage.
    const setStoredValueFn = (value: any) => {
        try {
            // Allow value to be a function so we have same API as useState
            const valueToStore =
                value instanceof Function ? value(storedValue) : value
            // Save state
            setStoredValue(valueToStore)
            // Save to local storage
            setLocalStorage(key, valueToStore)
        } catch (error) {
            // A more advanced implementation would handle the error case
            console.error(error)
        }
    }

    return skipStorage ? [value, setValue] : [storedValue, setStoredValueFn]
}
