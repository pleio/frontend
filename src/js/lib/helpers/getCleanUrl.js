export default function getCleanUrl(url) {
    if (!url) return url
    if (url.startsWith(window.location.origin)) {
        return url.slice(window.location.origin.length)
    } else if (url.startsWith(window.location.host)) {
        return url.slice(window.location.host.length)
    } else if (url.startsWith('www.')) {
        return `//${url}`
    } else {
        return url
    }
}
