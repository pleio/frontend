export default function findByKeyValue(object, key, value) {
    if (
        Object.prototype.hasOwnProperty.call(object, key) &&
        object[key] === value
    )
        return object

    for (let i = 0; i < Object.keys(object).length; i++) {
        if (typeof object[Object.keys(object)[i]] === 'object') {
            const o = findByKeyValue(object[Object.keys(object)[i]], key, value)
            if (o != null) return o
        }
    }

    return null
}
