import { createQueryString } from 'helpers'

describe('createQueryString', () => {
    test('creates a queryString from a correct object', () => {
        expect(createQueryString({ id: '123', user: 'user3' })).toEqual(
            '?id=123&user=user3',
        )
    })
    test('creates an empty queryString from an empty object', () => {
        expect(createQueryString({})).toEqual('')
    })
})
