import filterViewerFromAttendees from '../filterViewerFromAttendees'

describe('filterViewerFromAttendees', () => {
    const users = [
        {
            guid: 123,
            name: 'Tom',
        },
        {
            guid: 456,
            name: 'Jim',
        },
        {
            guid: 789,
            name: 'Anne',
        },
    ]
    test('filters the viewer from a list of attendees by guid', () => {
        const outcome = [users[0], users[2]]
        expect(filterViewerFromAttendees(users, 456)).toStrictEqual(outcome)
    })
    test('returns only the first two items if an nonexistent viewer is supplied', () => {
        const outcome = [users[0], users[1]]
        expect(filterViewerFromAttendees(users, 321)).toStrictEqual(outcome)
    })
})
