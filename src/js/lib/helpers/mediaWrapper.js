import {
    useDesktopUp,
    useMobileLandscapeDown,
    useMobileLandscapeUp,
    useMobilePortrait,
    useTabletDown,
    useTabletUp,
} from 'helpers/breakpoints'

export const MobileLandscapeDown = ({ children }) => {
    return useMobileLandscapeDown() ? children : null
}

export const MobilePortrait = ({ children }) => {
    return useMobilePortrait() ? children : null
}

export const MobileLandscapeUp = ({ children }) => {
    return useMobileLandscapeUp() ? children : null
}

export const TabletDown = ({ children }) => {
    return useTabletDown() ? children : null
}

export const TabletUp = ({ children }) => {
    return useTabletUp() ? children : null
}

export const DesktopUp = ({ children }) => {
    return useDesktopUp() ? children : null
}
