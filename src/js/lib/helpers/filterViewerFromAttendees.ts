export default (users: any[], viewerGuid: number) => {
    const filteredUsers = [...users]

    // Remove one user to make room for check icon
    const viewerIndex = filteredUsers.findIndex(
        (attendee) => attendee.guid === viewerGuid,
    )
    if (viewerIndex !== -1) {
        // Remove viewer from users
        filteredUsers.splice(viewerIndex, 1)
    } else {
        // Remove third user
        filteredUsers.splice(2, 1)
    }
    return filteredUsers
}
