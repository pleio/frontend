export default function getVideoFromUrl(value) {
    if (!value) return null
    const youtubeRegex =
        /^(https?:\/\/)?((www\.)?(youtube(-nocookie)?|youtube.googleapis)\.com.*(v\/|v=|vi=|vi\/|e\/|embed\/|user\/.*\/u\/\d+\/)|youtu\.be\/)([_0-9a-z-]+)/i
    const youtubeLink = value.match(youtubeRegex)

    if (youtubeLink) {
        return { type: 'youtube', id: youtubeLink[7] }
    } else {
        const vimeoRegex =
            /(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/(?:[^/]*)\/videos\/|album\/(?:\d+)\/video\/|video\/|)(\d+)(?:[a-zA-Z0-9_-]+)?/i
        const vimeoLink = value.match(vimeoRegex)

        if (vimeoLink) {
            return { type: 'vimeo', id: vimeoLink[1] }
        } else {
            const kalturaRegex = /(https?:\/\/videoleren.nvwa.nl(.*?)(?="|$))/i
            const kalturaLink = value.match(kalturaRegex)

            if (kalturaLink) {
                const id = value.match(/\/([^/]+)\/?$/)[1]
                return { type: 'kaltura', id }
            } else {
                return null
            }
        }
    }
}
