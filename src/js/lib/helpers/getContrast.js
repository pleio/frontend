import { getContrast } from 'polished'

// Check if given color has enough contrast as normal sized text to pass WCAG AA
export function textContrastIsAA(textColor, backgroundColor = '#fff') {
    return getContrast(textColor, backgroundColor) >= 4.5
}

// Get readable text color based on background (preferencing white)
export function getReadableColor(backgroundColor) {
    return textContrastIsAA('#fff', backgroundColor) ? 'white' : 'black'
}

// Check if given color has enough contrast as normal sized text to pass WCAG AA
export function nonTextContrastIsAA(color, backgroundColor = '#fff') {
    return getContrast(color, backgroundColor) >= 3
}

export default getContrast
