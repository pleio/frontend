export default function getLocalStorage(key, initialValue = false) {
    try {
        // Get from local storage by key
        const item = window.localStorage.getItem(key)
        // Parse stored json or if none return initialValue
        return item ? JSON.parse(item) : initialValue
    } catch (error) {
        // If error also return initialValue
        console.error(error)
        return initialValue
    }
}
