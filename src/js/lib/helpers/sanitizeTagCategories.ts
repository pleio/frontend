/*
Remove all tags and categories from entityTagCategories that do not occur in siteTagCategories.
*/

// TODO - generate types from graphql schema
interface TagCategory {
    name: string
    values: string[]
    __typename?: string
}

export default (
    siteTagCategories: TagCategory[] = [],
    entityTagCategories: TagCategory[] = [],
    allowEmptyValues: boolean,
) => {
    if (siteTagCategories?.length > 0 && entityTagCategories?.length > 0) {
        const filteredCategories = entityTagCategories.filter((category) => {
            // Check if site category exists
            const siteCategory = siteTagCategories.find(
                (siteCategory) => siteCategory.name === category.name,
            )

            if (allowEmptyValues) {
                return !!siteCategory
            }

            if (siteCategory) {
                // Check if at least one tag exists in this site category
                return category.values.some(
                    (tag) => siteCategory.values.indexOf(tag) !== -1,
                )
            } else {
                return false
            }
        })

        const filteredTags = filteredCategories.map((category) => {
            const siteCategory = siteTagCategories.find(
                ({ name }) => name === category.name,
            )
            const hasSiteCategory = (tag: string) =>
                siteCategory?.values.indexOf(tag) !== -1

            // Check if tags exists in site category
            const values =
                category.values?.length > 0
                    ? category.values.filter(hasSiteCategory)
                    : []

            return {
                name: category.name,
                values,
            }
        })

        return filteredTags
    } else {
        return []
    }
}
