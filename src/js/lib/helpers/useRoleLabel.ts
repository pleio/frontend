import { useTranslation } from 'react-i18next'

export default function useRoleLabel(roles: string[], isOwner: boolean) {
    const { t } = useTranslation()

    if (isOwner) {
        return t('global.owner')
    } else if (roles?.length > 0) {
        const hasRoles = (checkRoles: string[]) =>
            checkRoles.some((r) => roles.includes(r))

        // Site roles are in CAPS and group roles are in lowercase
        if (hasRoles(['ADMIN', 'admin'])) {
            return t('role.admin')
        } else if (hasRoles(['EDITOR'])) {
            return t('role.editor')
        } else if (hasRoles(['QUESTION_MANAGER'])) {
            return t('role.question-expert')
        } else if (hasRoles(['NEWS_EDITOR', 'newsEditor'])) {
            return t('role.news-editor')
        } else if (hasRoles(['USER_ADMIN'])) {
            return t('role.user-admin')
        } else {
            return null
        }
    } else {
        return null
    }
}
