import { format, getDate, getMonth, isValid, parse } from 'date-fns'
import { toZonedTime } from 'date-fns-tz'

import { getDatePattern } from './general'

// Format date to localized date pattern
export function parseDatePattern(isoDate: string) {
    return format(isoDate, getDatePattern())
}

export function parsePatternToDate(date: string) {
    const parsedDate = parse(date, getDatePattern(), new Date())
    return isValid(parsedDate) ? parsedDate : new Date()
}

export function getFullYearInt(isoDate: string) {
    const formattedYear = format(new Date(isoDate), 'yyyy')
    return parseInt(formattedYear, 10)
}

export function parseToCalendarDate(isoDate: string) {
    const zonedDate = toZonedTime(isoDate, 'UTC')
    const formattedDate = format(zonedDate, "yyyyMMdd'T'HHmmss'Z'")
    return formattedDate.replace('+00:00', 'Z')
}

export function isSameDayAndMonth(date1: string, date2: string) {
    return (
        getDate(date1) === getDate(date2) && getMonth(date1) === getMonth(date2)
    )
}
