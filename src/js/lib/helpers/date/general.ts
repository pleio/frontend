import { set, setDefaultOptions } from 'date-fns'
import { de, enGB, fr, nl } from 'date-fns/locale'

type Locale = 'nl' | 'fr' | 'de' | 'en'

export let activeLocale: Locale = 'nl'

function getLocaleFile(locale: Locale) {
    return locale === 'nl'
        ? nl
        : locale === 'fr'
          ? fr
          : locale === 'de'
            ? de
            : enGB
}

export function getActiveLocaleFile() {
    return getLocaleFile(activeLocale)
}

export function setLocale(clientLocale: string = '') {
    const locale = (clientLocale ||
        localStorage.getItem('locale') ||
        'nl') as Locale
    setDefaultOptions({ locale: getLocaleFile(locale) })

    activeLocale = locale
}

export function getDatePattern() {
    const datePatterns = {
        en: 'MM/dd/yyyy',
        fr: 'dd/MM/yyyy',
        de: 'dd.MM.yyyy',
        nl: 'dd-MM-yyyy',
    }

    return datePatterns[activeLocale]
}

export function getTimePattern() {
    const timePatterns = {
        en: 'h:mm a',
        fr: 'hh:mm',
        de: 'hh:mm',
        nl: 'hh:mm',
    }

    return timePatterns[activeLocale]
}

export function updateDatePreserveTime(
    originalDate: string,
    newDate: Date,
): Date {
    const date =
        typeof originalDate === 'string' ? new Date(originalDate) : originalDate
    return set(date, {
        year: newDate.getFullYear(),
        month: newDate.getMonth(),
        date: newDate.getDate(),
    })
}

export function getShortMonths() {
    const shortMonthsEn = [
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'May',
        'Jun',
        'Jul',
        'Aug',
        'Sep',
        'Oct',
        'Nov',
        'Dec',
    ]
    const shortMonthsNl = [
        'Jan',
        'Feb',
        'Mrt',
        'Apr',
        'Mei',
        'Jun',
        'Jul',
        'Aug',
        'Sep',
        'Okt',
        'Nov',
        'Dec',
    ]
    const shortMonthsDe = [
        'Jan',
        'Feb',
        'Mär',
        'Apr',
        'Mai',
        'Jun',
        'Jul',
        'Aug',
        'Sep',
        'Okt',
        'Nov',
        'Dez',
    ]
    const shortMonthsFr = [
        'Jan',
        'Fév',
        'Mar',
        'Avr',
        'Mai',
        'Jun',
        'Jul',
        'Aoû',
        'Sep',
        'Oct',
        'Nov',
        'Déc',
    ]

    return activeLocale === 'nl'
        ? shortMonthsNl
        : activeLocale === 'fr'
          ? shortMonthsFr
          : activeLocale === 'de'
            ? shortMonthsDe
            : shortMonthsEn
}
