import { advanceTo, clear } from 'jest-date-mock'

import { setLocale } from '../general'
import showDate, {
    getWeekdayOccurrenceInMonth,
    isSameDate,
    isSameTime,
    showDateTime,
    showDateWithoutYear,
    showDay,
    showFullDate,
    showFullDay,
    showMonth,
    showShortDate,
    showTime,
    showWeekDay,
    timeSince,
} from '../showDate'

const date1 = '2024-08-03T21:07:15.100000+00:00'
const date2 = '2024-09-03T21:08:15.100000+00:00'
const currentYear = new Date().getFullYear()

describe('showDate', () => {
    beforeAll(() => {
        setLocale('nl')
    })

    describe('isSameDate', () => {
        test('two different dates', () => {
            expect(isSameDate(date1, date2)).toBe(false)
        })

        test('two identical dates', () => {
            expect(isSameDate(date1, date1)).toBe(true)
        })
    })

    describe('isSameTime', () => {
        test('two different date times', () => {
            expect(isSameTime(date1, date2)).toBe(false)
        })

        test('two identical date times', () => {
            expect(isSameTime(date1, date1)).toBe(true)
        })
    })

    describe('showDateWithoutYear', () => {
        test('showDateWithoutYear', () => {
            expect(showDateWithoutYear(date1)).toBe('3 aug')
        })
    })

    describe('showDay', () => {
        test('showDay', () => {
            expect(showDay(date1)).toBe('3')
        })
    })

    describe('showFullDate', () => {
        beforeEach(() => {
            // Set the initial date
            advanceTo(new Date('2024-01-01'))
        })

        afterEach(() => {
            clear() // Reset to current system time after each test
        })

        test('showFullDate in current year', () => {
            const currentYear = '2024-08-03T21:07:15.100000+00:00'
            expect(showFullDate(currentYear)).toBe('Zaterdag, 3 augustus')
        })

        test('showFullDate in last year', () => {
            const lastYear = '2023-08-03T21:07:15.100000+00:00'
            expect(showFullDate(lastYear)).toBe('Donderdag, 3 augustus 2023')
        })
    })

    describe('showDate', () => {
        beforeEach(() => {
            // Set the initial date
            advanceTo(new Date('2024-01-01'))
        })

        afterEach(() => {
            clear() // Reset to current system time after each test
        })

        test('show current year and show day', () => {
            const date1 = '2024-08-03T00:00:00Z'
            expect(showDate(date1, true, true)).toBe('za 3 aug 2024')
        })

        test('show current year and do not show day', () => {
            const date1 = '2024-08-03T00:00:00Z'
            expect(showDate(date1, true, false)).toBe('3 aug 2024')
        })

        test('do not show current year and do not show day', () => {
            const date1 = '2024-08-03T00:00:00Z'
            expect(showDate(date1, false, false)).toBe('3 aug')
        })

        test('do not show current year and show day', () => {
            const date1 = '2024-08-03T00:00:00Z'
            expect(showDate(date1, false, true)).toBe('za 3 aug')
        })

        test('show older year and do not show day', () => {
            const date3 = '2023-09-03T00:00:00Z'
            expect(showDate(date3, false, false)).toBe('3 sep 2023')
        })
    })

    describe('showFullDay', () => {
        test('showFullDay', () => {
            expect(showFullDay(date1)).toBe('zaterdag')
        })
    })

    describe('showMonth', () => {
        test('showMonth', () => {
            expect(showMonth(date1)).toBe('aug')
        })
    })

    describe('showShortDate', () => {
        test('showShortDate', () => {
            expect(showShortDate(date1)).toBe('03-08-2024')
        })
        test('showShortDate', () => {
            expect(showShortDate(null)).toBe('')
        })
    })

    describe('showTime', () => {
        test('showTime', () => {
            expect(showTime(date1)).toBe('21:07')
        })
    })

    describe('showWeekDay', () => {
        test('showWeekDay', () => {
            expect(showWeekDay(date1)).toBe('za')
        })
    })

    describe('timeSince', () => {
        beforeEach(() => {
            // Set the initial date
            advanceTo(new Date('2024-09-06'))
        })

        afterEach(() => {
            clear() // Reset to current system time after each test
        })

        test('time since is more than 8 days from now', () => {
            const date1 = '2024-08-03T21:07:15.100000+00:00'
            expect(timeSince(date1)).toBe('3 aug')
        })
        test('time since is less than 8 days from now', () => {
            const date1 = '2024-09-03T21:07:15.100000+00:00'
            expect(timeSince(date1)).toBe('2 dagen geleden')
        })
    })

    describe('showDateTime', () => {
        test('default formatting', () => {
            expect(showDateTime(date1)).toEqual('3 aug 2024 global.at 21:07')
        })

        test('current year, without year or day', () => {
            expect(
                showDateTime(
                    `${currentYear}-08-03T21:07:15.100000+00:00`,
                    false,
                    false,
                ),
            ).toEqual('3 aug global.at 21:07')
        })

        test('previous year, with year, without day', () => {
            expect(
                showDateTime('2024-08-03T21:07:15.100000+00:00', false, false),
            ).toEqual('3 aug 2024 global.at 21:07')
        })

        test('with year and day', () => {
            expect(showDateTime(date1, true, true)).toEqual(
                'za 3 aug 2024 global.at 21:07',
            )
        })
    })

    describe('getWeekdayOccurrenceInMonth', () => {
        test('getWeekdayOccurrenceInMonth', () => {
            expect(getWeekdayOccurrenceInMonth(date1)).toBe(1)
        })

        test('getWeekdayOccurrenceInMonth with invalid date', () => {
            expect(getWeekdayOccurrenceInMonth('invalid date')).toBe('')
        })

        test('getWeekdayOccurrenceInMonth with date in month that starts on a wednesday', () => {
            expect(getWeekdayOccurrenceInMonth('2025-01-06')).toBe(1) // 1st monday
            expect(getWeekdayOccurrenceInMonth('2025-01-13')).toBe(2)
            expect(getWeekdayOccurrenceInMonth('2025-01-20')).toBe(3)
            expect(getWeekdayOccurrenceInMonth('2025-01-27')).toBe(4)
        })
    })
})
