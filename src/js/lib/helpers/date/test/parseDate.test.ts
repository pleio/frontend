import { format } from 'date-fns'
import { advanceTo, clear } from 'jest-date-mock'

import {
    getFullYearInt,
    parseDatePattern,
    parsePatternToDate,
    parseToCalendarDate,
} from '../parseDate'

const date1 = '2024-08-23T21:07:15.100000+00:00'

describe('formatDate', () => {
    describe('parseDatePattern', () => {
        test('parse nl date pattern', () => {
            // The "nl" is loaded by default
            expect(parseDatePattern(date1)).toBe('23-08-2024')
        })
    })

    describe('parsePatternToDate', () => {
        beforeEach(() => {
            // Set the initial date
            advanceTo(new Date('2024-01-01'))
        })

        afterEach(clear)

        test('valid parsing', () => {
            const parsedDate = parsePatternToDate('23-08-2024')
            expect(format(parsedDate, 'dd-MM-yyyy')).toBe('23-08-2024')
        })

        test('invalid parsing', () => {
            const parsedDate = parsePatternToDate('undefined')
            expect(format(parsedDate, 'dd-MM-yyyy')).toBe('01-01-2024')
        })
    })

    describe('getFullYearInt', () => {
        test('get year from date string', () => {
            expect(getFullYearInt(date1)).toBe(2024)
        })
    })

    describe('parseToCalendarDate', () => {
        test('parse to calendar date string', () => {
            expect(parseToCalendarDate('2024-10-23T10:52:01+02:00')).toBe(
                '20241023T085201Z',
            )
        })
    })
})
