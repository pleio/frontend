import { getShortMonths, updateDatePreserveTime } from '../general'

describe('general date helper functions', () => {
    describe('getShortMonths', () => {
        test('first short month', () => {
            expect(getShortMonths().length).toEqual(12)
            expect(getShortMonths()[0]).toEqual('Jan')
        })
    })

    describe('updateDatePreserveTime', () => {
        test('update date, but keep original time', () => {
            const originalDateTime = '2024-08-23T21:07:15.100000+00:00'
            const newDateTime = '2024-10-18T21:16:30.100000+00:00'
            const newDateWithOldTime = '2024-10-18T21:07:15.100000+00:00'

            expect(
                updateDatePreserveTime(originalDateTime, new Date(newDateTime)),
            ).toStrictEqual(new Date(newDateWithOldTime))
        })
    })
})
