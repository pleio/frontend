import {
    addDays,
    differenceInYears,
    format,
    formatDistanceToNow,
    isAfter,
    isSameDay,
    isSameMinute,
    isThisYear,
    parseISO,
    subDays,
} from 'date-fns'
import i18next from 'i18next'

import { activeLocale, getDatePattern } from './general'

function stripDot(formattedDate: string) {
    return activeLocale === 'nl'
        ? formattedDate.replace('.', '')
        : formattedDate
}

export function getDistanceToNow(isoDate: string) {
    return formatDistanceToNow(parseISO(isoDate), {
        addSuffix: true,
    })
}

export function isSameDate(startIsoDate: string, endIsoDate: string) {
    return isSameDay(startIsoDate, endIsoDate)
}

export function isSameTime(startIsoDate: string, endIsoDate: string) {
    return isSameMinute(startIsoDate, endIsoDate)
}

export function showDateWithoutYear(date: string) {
    return stripDot(format(date, 'd MMM'))
}

export function showTime(isoDate: string) {
    return format(isoDate, 'p')
}

export function showDay(isoDate: string) {
    return format(isoDate, 'd')
}

export function showFullDay(isoDate: string) {
    return format(isoDate, 'EEEE')
}

export function showWeekDay(isoDate: string) {
    return format(isoDate, 'EEEEEE')
}

export function showMonth(isoDate: string) {
    return stripDot(format(isoDate, 'MMM'))
}

export function showShortDate(isoDate: string): string {
    if (typeof isoDate !== 'string' || !isoDate.length) return ''
    return format(isoDate, getDatePattern())
}

export function showFullDate(isoDate: string) {
    const showYear = !isThisYear(isoDate)
    const formattedDate = format(
        isoDate,
        `EEEE, d MMMM${showYear ? ' yyyy' : ''}`,
    )
    return formattedDate.charAt(0).toUpperCase() + formattedDate.slice(1)
}

export default function showDate(
    isoDate: string,
    showCurrentYear: boolean = false,
    showDay: boolean = false,
) {
    // When using a previous year, it is always being displayed
    const showYear = showCurrentYear || !isThisYear(isoDate)
    const day = showDay ? 'EEEEEE ' : ''
    const year = showYear ? ' yyyy' : ''
    const pattern = `${day}d LLL${year}`
    return stripDot(format(isoDate, pattern))
}

export function timeSince(isoDate: string) {
    const eightDaysAgo = subDays(new Date(), 8)

    if (isAfter(isoDate, eightDaysAgo)) {
        // show passed time since date
        return getDistanceToNow(isoDate)
    } else {
        return showDate(isoDate)
    }
}

export function showDateTime(
    isoDate: string,
    showCurrentYear: boolean = true,
    showDay: boolean = false,
) {
    if (!isoDate) return ''

    return `${showDate(isoDate, showCurrentYear, showDay)} ${i18next.t(
        'global.at',
    )} ${showTime(isoDate)}`
}

export function getAge(birthday: string, futureDays: number = 0) {
    // Future days return the age at a future date
    const referenceDate = addDays(new Date(), futureDays)
    return differenceInYears(referenceDate, new Date(birthday))
}

export function getWeekdayOccurrenceInMonth(isoDate: string) {
    const date = new Date(isoDate)

    // Check if the date is valid
    if (isNaN(date.getTime())) {
        console.error('Invalid date in getWeekdayOccurrenceInMonth:', isoDate)
        return ''
    }

    const weekday = format(date, 'EEEE')
    const dayOfMonth = parseInt(format(date, 'd'))

    return Array.from({ length: dayOfMonth }, (_, i) => i + 1)
        .map((day) => new Date(date.getFullYear(), date.getMonth(), day))
        .filter((d) => format(d, 'EEEE') === weekday).length
}
