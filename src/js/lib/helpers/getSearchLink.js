import { createQueryString } from 'helpers'

export default (params) => {
    const queryString = createQueryString(params)
    return `/search${queryString}`
}
