import { alertFileTooLarge, humanFileSize } from 'helpers/fileSize'
import i18next from 'i18next'

global.alert = jest.fn()

const fileSize = 5_000_000 // 5 MB
const limitInMb = 1
const fileName = 'testFile.txt'
const humanFileSizeOutcome = '5 MB'

jest.mock('helpers/fileSize/humanFileSize', () =>
    jest.fn(() => humanFileSizeOutcome),
)

describe('alertFileTooLarge', () => {
    beforeEach(() => {
        jest.clearAllMocks()
    })

    test('shows an alert with the correct translation and file size', () => {
        alertFileTooLarge(fileSize, limitInMb, fileName)

        expect(humanFileSize).toHaveBeenCalledWith(fileSize)

        expect(i18next.t).toHaveBeenCalledWith('global.alert-file-too-big', {
            fileName,
            fileSize: humanFileSizeOutcome,
            maxSizeInMb: limitInMb,
        })

        expect(global.alert).toHaveBeenCalledWith('global.alert-file-too-big')
    })
})
