import { humanFileSize } from 'js/lib/helpers'

describe('humanFileSize', () => {
    test('returns bytes as B when below threshold', () => {
        expect(humanFileSize(500)).toBe('500 B')
        expect(humanFileSize(999)).toBe('999 B')
    })

    test('converts to KB when above threshold', () => {
        expect(humanFileSize(1500)).toBe('1.5 KB')
        expect(humanFileSize(1000)).toBe('1.0 KB')
    })

    test('converts to MB when above threshold', () => {
        expect(humanFileSize(1_500_000)).toBe('1.5 MB')
        expect(humanFileSize(1_000_000)).toBe('1.0 MB')
    })

    test('converts to GB when above threshold', () => {
        expect(humanFileSize(1_500_000_000)).toBe('1.5 GB')
        expect(humanFileSize(1_000_000_000)).toBe('1.0 GB')
    })

    test('handles negative byte values correctly', () => {
        expect(humanFileSize(-1500)).toBe('-1.5 KB')
        expect(humanFileSize(-1_500_000)).toBe('-1.5 MB')
    })

    test('handles different decimal places', () => {
        expect(humanFileSize(1500, 2)).toBe('1.50 KB')
        expect(humanFileSize(1_500_000, 2)).toBe('1.50 MB')
        expect(humanFileSize(1_500_000, 0)).toBe('2 MB')
    })

    test('handles very large values in YB', () => {
        expect(humanFileSize(1e24)).toBe('1.0 YB')
        expect(humanFileSize(1e27)).toBe('1000.0 YB') // Just for fun, large number
    })

    test('rounds and formats odd values', () => {
        expect(humanFileSize(1_033_078, 2)).toBe('1.03 MB')
        expect(humanFileSize(1_033_078)).toBe('1.0 MB')
        expect(humanFileSize(1_005_078, 2)).toBe('1.01 MB')
        expect(humanFileSize(1_455_078)).toBe('1.5 MB')
    })

    test('with incorrect input', () => {
        expect(humanFileSize(null)).toBe('')
        // @ts-ignore
        expect(humanFileSize('')).toBe('')
        // @ts-ignore
        expect(humanFileSize({})).toBe('')
    })
})
