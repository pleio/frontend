function fileSizeInMb(sizeByte) {
    return sizeByte / 1024 / 1024
}

export default function isFileSizeOk(fileSize, limitInMb) {
    return fileSizeInMb(fileSize) <= limitInMb
}
