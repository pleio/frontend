import i18next from 'i18next'

import humanFileSize from './humanFileSize'

export default function alertFileTooLarge(
    fileSize: number,
    limitInMb: number,
    fileName: string,
) {
    alert(
        i18next.t('global.alert-file-too-big', {
            fileName,
            fileSize: humanFileSize(fileSize),
            maxSizeInMb: limitInMb,
        }),
    )
}
