export default (params) => {
    if (params?.groupGuid && params?.groupSlug) {
        return `/groups/view/${params.groupGuid}/${params.groupSlug}`
    }
    return ''
}
