export default function convertFileListToArray(input: FileList) {
    const files = []

    for (let i = 0; i < input.length; i++) {
        files.push(input[i])
    }

    return files
}
