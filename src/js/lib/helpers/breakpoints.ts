import { useMediaQuery } from 'react-responsive'

const breakpoints = {
    mobilePortrait: 0, // mobile
    mobileLandscape: 576, // mobile landscape
    tablet: 768, // tablet
    desktop: 992, // desktop
    desktopLarge: 1200, // large desktop
}

export function useMobileLandscapeDown() {
    return useMediaQuery({ maxWidth: breakpoints.tablet - 1 })
}

export function useMobilePortrait() {
    return useMediaQuery({ maxWidth: breakpoints.mobileLandscape - 1 })
}

export function useMobileLandscape() {
    return useMediaQuery({
        minWidth: breakpoints.mobileLandscape,
        maxWidth: breakpoints.tablet - 1,
    })
}

export function useMobileLandscapeUp() {
    return useMediaQuery({ minWidth: breakpoints.mobileLandscape })
}

export function useTabletDown() {
    return useMediaQuery({ maxWidth: breakpoints.desktop - 1 })
}

export function useTablet() {
    return useMediaQuery({
        minWidth: breakpoints.tablet,
        maxWidth: breakpoints.desktop - 1,
    })
}

export function useTabletUp() {
    return useMediaQuery({ minWidth: breakpoints.tablet })
}

export function useDesktopUp() {
    return useMediaQuery({ minWidth: breakpoints.desktop })
}

export default breakpoints
