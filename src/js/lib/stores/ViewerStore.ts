import { gql } from '@apollo/client'
import { create } from 'zustand'

import client from 'js/lib/client'

interface ViewerState {
    viewer: {
        guid: string
        loggedIn: boolean
        user: {
            guid: string
            name?: string
            icon?: string
        }
    }
    error: Error | null
    getViewer: () => void
}

// Here we should eventually retrieve all viewer details that are used on the client
const GET_VIEWER_DETAILS = gql`
    query GetViewerDetails {
        viewer {
            guid
            loggedIn
            user {
                guid
                name
                icon
            }
        }
    }
`

export const useViewerStore = create<ViewerState>((set) => {
    return {
        viewer: null,
        error: null,
        // Updating the store by calling this function:
        getViewer: async () => {
            try {
                const result = await client.query({
                    query: GET_VIEWER_DETAILS,
                })
                set({ viewer: result.data?.viewer })
            } catch (error) {
                console.error('Failed to fetch viewer data', error)
                set({ error })
            }
        },
    }
})

useViewerStore.getState().getViewer()
