import { gql } from '@apollo/client'
import { create } from 'zustand'

import client from 'js/lib/client'

export interface SiteStyleState {
    fontBody?: string
    fontHeading?: string
    colorPrimary?: string
    colorSecondary?: string
    colorHeader?: string
}

interface SiteStoreState {
    site: {
        guid: string
        name: string
        customTagsAllowed: boolean
        showCustomTagsInFeed: boolean
        showCustomTagsInDetail: boolean
        showTagsInFeed: boolean
        showTagsInDetail: boolean
        commentWithoutAccountEnabled: boolean
        integratedVideocallEnabled: boolean
        questionLockAfterActivityLink: string
        language: string
        languageOptions?: { label?: string; value: string }[]
        style: SiteStyleState
    }
    error: Error | null
    getSite: () => void
}

// Here we should eventually retrieve all site settings that are used in the client
// I'd suggest to create a similar store for admin site settings, which probably is a lot more data
const GET_SITE_FIELDS = gql`
    query GetClientSite {
        site {
            guid
            name
            customTagsAllowed
            showCustomTagsInFeed
            showCustomTagsInDetail
            showTagsInFeed
            showTagsInDetail
            commentWithoutAccountEnabled
            integratedVideocallEnabled
            questionLockAfterActivityLink
            language
            languageOptions {
                label
                value
            }
            style {
                fontBody
                fontHeading
                colorPrimary
                colorSecondary
                colorHeader
            }
        }
    }
`

export const useSiteStore = create<SiteStoreState>((set) => {
    return {
        site: null,
        error: null,
        // Updating the store by calling this function:
        getSite: async () => {
            try {
                const result = await client.query({
                    query: GET_SITE_FIELDS,
                })
                set({ site: result.data?.site })
            } catch (error) {
                console.error('Failed to fetch site data', error)
                set({ error })
            }
        },
    }
})

useSiteStore.getState().getSite()
