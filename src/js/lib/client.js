import { ApolloClient, from, InMemoryCache, split } from '@apollo/client'
import { loadDevMessages, loadErrorMessages } from '@apollo/client/dev'
import { setContext } from '@apollo/client/link/context'
import { onError } from '@apollo/client/link/error'
import { GraphQLWsLink } from '@apollo/client/link/subscriptions'
import { getMainDefinition } from '@apollo/client/utilities'
import createUploadLink from 'apollo-upload-client/createUploadLink.mjs'
import { createClient } from 'graphql-ws'

import possibleTypes from '../../possibleTypes.json'

import { readCookie } from './cookies'

const middlewareLink = setContext((_, { headers }) => {
    const newHeaders = {
        ...headers,
    }

    const csrfToken = readCookie('CSRF_TOKEN')
    const elggCookie =
        typeof global.COOKIES !== 'undefined' ? global.COOKIES.Elgg : false

    if (csrfToken) {
        newHeaders['X-CSRF-Token'] = csrfToken
    }

    if (elggCookie) {
        newHeaders['X-Elgg'] = elggCookie
    }

    return {
        headers: newHeaders,
    }
})

if (process.env.NODE_ENV === 'development') {
    loadErrorMessages()
    loadDevMessages()
}

let url
if (global.SITE) {
    url = global.SITE + 'graphql'
} else {
    url = '/graphql'
}

const typePolicies = {}
// Merge Entity types and other types with guid
const otherTypes = [
    'Activity',
    'GroupNotificationItem',
    'PollChoice',
    'Site',
    'Viewer',
    'Chat',
    'ChatMessage',
]
const typesWithGuid = possibleTypes.Entity.concat(otherTypes)
// Add guid as default keyFields
typesWithGuid.forEach((type) => {
    typePolicies[type] = {
        keyFields: ['guid'],
    }
})

// Pleio version of offsetLimitPagination
const edgesOffsetLimitPagination = {
    // return keys where the list should be cached on
    keyArgs: (args) => {
        const cacheKeys = []
        for (const key in args) {
            if (key !== 'limit' && key !== 'offset') {
                cacheKeys.push(key)
            }
        }
        return cacheKeys
    },
    // merge data for apollo caching
    merge(existing, incoming, { args }) {
        const result = existing
            ? {
                  ...existing,
                  ...incoming,
              }
            : incoming

        if (args && args.offset > 0 && existing && 'edges' in incoming) {
            result.edges = existing ? existing.edges.slice(0) : []
            const start = args ? args.offset : result.edges.length
            const end = start + incoming.edges.length
            for (let i = start; i < end; ++i) {
                result.edges[i] = incoming.edges[i - start]
            }
        }

        return result
    },
}

typePolicies.Query = {
    fields: {
        // SiteSettings has no id field so tell caching how to merge
        siteSettings: {
            merge(_ignored, incoming) {
                return incoming
            },
        },
        entities: edgesOffsetLimitPagination,
        activities: edgesOffsetLimitPagination,
        groups: edgesOffsetLimitPagination,
        notifications: edgesOffsetLimitPagination,
        usersByBirthDate: edgesOffsetLimitPagination,
        search: edgesOffsetLimitPagination,
        events: edgesOffsetLimitPagination,
        files: edgesOffsetLimitPagination,
        siteUsers: edgesOffsetLimitPagination,
        users: edgesOffsetLimitPagination,
        attendees: edgesOffsetLimitPagination,
        members: edgesOffsetLimitPagination,
        searchJournal: edgesOffsetLimitPagination,
        bookmarks: edgesOffsetLimitPagination,
        revisions: edgesOffsetLimitPagination,
        profileSyncLogs: edgesOffsetLimitPagination,
        brokenLinks: edgesOffsetLimitPagination,
        fetchRssEndpoint: edgesOffsetLimitPagination,
        publishRequests: edgesOffsetLimitPagination,
        commentModerationRequests: edgesOffsetLimitPagination,
    },
}

typePolicies.Chat = {
    fields: {
        messages: edgesOffsetLimitPagination,
    },
}

typePolicies.Podcast = {
    keyFields: ['guid'],
    fields: {
        episodes: edgesOffsetLimitPagination,
    },
}

const httpLink = createUploadLink({ uri: url, credentials: 'same-origin' })
const urlProtocol = window.location.protocol === 'https:' ? 'wss' : 'ws'
const wsLink = new GraphQLWsLink(
    createClient({
        url: urlProtocol + '://' + location.host + '/subscriptions',
        lazy: true,
        lazyCloseTimeout: 10000,
    }),
)
const splitLink = split(
    ({ query }) => {
        const definition = getMainDefinition(query)
        return (
            definition.kind === 'OperationDefinition' &&
            definition.operation === 'subscription'
        )
    },
    wsLink,
    httpLink,
)
const cache = new InMemoryCache({
    possibleTypes,
    typePolicies,
})

if (window.__APOLLO_STATE__) {
    cache.restore(window.__APOLLO_STATE__)
}

const isAuthError = onError(({ networkError }) => {
    if (networkError && networkError.statusCode === 401)
        window.location.href = `/login${
            location?.pathname ? `?next=${location.pathname}` : ''
        }`
})

const client = new ApolloClient({
    cache,
    link: from([isAuthError, middlewareLink, splitLink]),
    queryDeduplication: true,
})

export default client
