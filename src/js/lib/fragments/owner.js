const ownerFields = `
    guid
    username
    name
    url
    icon
    roles
    vcard {
        guid
        name
        value
    }
`

export const ownerViewFields = `
    ${ownerFields}
`

export const ownerFragment = `
    owner {
        ${ownerFields}
    }
    showOwner
`

export const commentOwnerFragment = `
    owner {
        ${ownerFields}
    }
    ownerName
`
