const sharedFields = `
    positionY
    alt
    caption
    video
    videoTitle
    videoThumbnailUrl
`

export const featuredEditFragment = `
    featured {
        ${sharedFields}
        image {
            ... on File {
                guid
                download
                title
                mimeType
                subtype
                hasChildren
                size
            }
        }
    }
`

export const featuredViewFragment = `
    featured {
        ${sharedFields}
        image {
            ... on File {
                guid
                download
            }
        }
    }
`
