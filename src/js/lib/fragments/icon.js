const sharedFields = `
    guid
    download
`

export const iconEditFragment = `
    icon {
        ... on File {
            ${sharedFields}
            title
            mimeType
            subtype
            hasChildren
            size
        }
    }
`

export const iconViewFragment = `
    icon {
        ... on File {
            ${sharedFields}
        }
    }
`
