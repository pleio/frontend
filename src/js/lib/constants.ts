// Maximum number of characters for different fields
export const MAX_LENGTH = {
    title: 256,
    groupName: 200,
}

// Maximum file sizes in MB
export const MAX_FILE_SIZE = {
    file: 2048,
    audio: 4096,
    attachment: 250,
    image: 5,
}

export const FILE_TYPE = {
    file: {
        accept: '.pdf,.csv,.png,.jpg,.doc,.docx,.xls,.xlsx,.ppt,.pptx',
        maxSize: MAX_FILE_SIZE.file,
    },
    audio: {
        accept: '.mp3,.m4a,.mp4',
        maxSize: MAX_FILE_SIZE.audio,
    },
    image: {
        accept: 'image/*',
        maxSize: MAX_FILE_SIZE.image,
    },
}

// Global regex patterns
export const REGEX = {
    email: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    colorHex: /^#[0-9A-F]{6}$/i,
}

/**
 * ID of the element that marks the start of the content. This can be used to
 * scroll to the start of the content when needed.
 */
export const START_OF_CONTENT_ELEMENT_ID = 'start-of-content'
