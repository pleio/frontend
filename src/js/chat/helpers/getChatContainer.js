export default function getChatContainer(container) {
    if (!container) return null
    const isGroup = !!container.group
    return {
        isSingle: isGroup ? container.group : container.users?.length === 1,
        type: isGroup ? 'group' : 'users',
        title: isGroup
            ? container.group.name
            : Array.from(container.users, (u) => u.name).join(', '),
        entities: isGroup ? [container.group] : container.users,
    }
}
