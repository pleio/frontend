import { iconViewFragment } from 'js/lib/fragments/icon'

export default `
guid
unread
type
isNotificationPushEnabled
videoCallToken
container {
    users {
        guid
        icon
        name
        url
        isChatOnline
    }
    group {
        guid
        ${iconViewFragment}
        name
        url
        isClosed
    }
}
`
