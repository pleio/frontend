export default `
guid
richDescription
canEdit
sender {
    guid
    name
    icon
    url
    roles
    vcard {
        guid
        name
        value
    }
}
timeCreated
replyCount
reactions {
    count
    unicode
    users
}
chat {
    guid
    container {
        group {
            guid
            isClosed
        }
    }
}
`
