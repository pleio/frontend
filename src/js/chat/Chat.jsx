import React, { useContext } from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { media } from 'helpers'
import styled, { css } from 'styled-components'

import Document from 'js/components/Document'
import globalStateContext from 'js/lib/withGlobalState/globalStateContext'

import MainThread from './components/MainThread'
import Menu from './components/Menu'
import NewThread from './components/NewThread'
import Subthread from './components/Subthread'

const Wrapper = styled.div`
    z-index: 1;
    flex-grow: 1;
    overflow: hidden; // Enable scrollbars in children
    background-color: white;
    display: flex;

    .ChatThreadContainer {
        display: flex;
        flex-grow: 1;
    }

    ${media.mobilePortrait`
        flex-direction: column;

        .ChatMenu {
            width: 100%;
            flex-shrink: 0;
            max-height: 50%;
            border-bottom: 1px solid ${(p) => p.theme.color.grey[30]};
        }

        .ChatThreadContainer {
            overflow: hidden;
        }

        .ChatThread {
            width: 100%;
            flex-grow: 1;
            overflow: hidden;
        }
    `};

    ${media.mobileLandscape`
        .ChatMenu {
            width: 220px;
        }
    `};

    ${media.mobileLandscapeUp`
        align-items: flex-start;
        justify-content: center;

        .ChatMenu {
            flex-shrink: 0;
            height: 100%;
            border-right: 1px solid ${(p) => p.theme.color.grey[30]};
        }

        .ChatThreadContainer {
            height: 100%;
            margin-right: -1px; // Overflows border on the right side
        }

        .ChatThread {
            width: 100%;
            height: 100%;
            border-right: 1px solid ${(p) => p.theme.color.grey[30]};
        }
    `};

    ${media.tabletUp`
        .ChatMenu {
            width: 260px;
        }
    `};

    ${(p) =>
        p.$showSubthread
            ? css`
                  .ChatThreadContainer {
                      max-width: ${`calc( ${p.theme.containerWidth.small} * 2)`};
                  }

                  ${media.mobilePortrait`
                    .MainThread {
                        display: none;
                    }
                `};
              `
            : css`
                  .ChatThreadContainer {
                      max-width: ${(p) => p.theme.containerWidth.small};
                  }

                  ${media.desktopUp`
                    padding-right: 260px;
                `};
              `};
`

const Chat = () => {
    const { guid } = useParams()

    const { t } = useTranslation()

    const {
        globalState: { chatNewThread, subthreadGuid },
    } = useContext(globalStateContext)

    return (
        <>
            <Document title={t('chat.title')} />
            <Wrapper $showSubthread={!!subthreadGuid}>
                <Menu className="ChatMenu" />
                <div className="ChatThreadContainer">
                    {chatNewThread ? (
                        <NewThread className="ChatThread ChatNewThread" />
                    ) : (
                        <>
                            <MainThread
                                guid={guid}
                                className="ChatThread MainThread"
                            />
                            <Subthread
                                guid={subthreadGuid}
                                className="ChatThread Subthread"
                                size="large"
                            />
                        </>
                    )}
                </div>
            </Wrapper>
        </>
    )
}

export default Chat
