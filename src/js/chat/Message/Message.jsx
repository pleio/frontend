import React, { forwardRef, useContext, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

import Button from 'js/components/Button/Button'
import DisplayDate from 'js/components/DisplayDate'
import DropdownButton from 'js/components/DropdownButton/DropdownButton'
import Flexer from 'js/components/Flexer/Flexer'
import IconButton from 'js/components/IconButton/IconButton'
import Modal from 'js/components/Modal/Modal'
import TiptapView from 'js/components/Tiptap/TiptapView'
import UserAvatar from 'js/components/UserAvatar'
import globalStateContext from 'js/lib/withGlobalState/globalStateContext'

import OptionsIcon from 'icons/options.svg'
import RepliesIcon from 'icons/replies.svg'
import RepliesSmallIcon from 'icons/replies-small.svg'

import DeleteMessage from './components/DeleteMessage'
import EditMessageForm from './components/EditMessageForm'
import EmojiReactions from './components/EmojiReactions'
import OpenEmojiPicker from './components/OpenEmojiPicker'
import { useToggleEmoji } from './hooks/useToggleEmoji'

const Wrapper = styled.div`
    position: relative;
    display: flex;
    align-items: flex-start;
    padding: 8px 12px;
    border-radius: ${(p) => p.theme.radius.normal};

    .MessageMeta {
        display: flex;
        flex-wrap: wrap;
        align-items: baseline;
    }

    .MessageAuthorIcon {
        margin: 4px 10px 0 0;
    }

    .MessageAuthorName {
        margin-right: 6px;
        font-weight: ${(p) => p.theme.font.weight.semibold};

        ${(p) =>
            p.$size === 'normal' &&
            css`
                font-size: ${(p) => p.theme.font.size.small};
                line-height: ${(p) => p.theme.font.lineHeight.small};
            `};

        ${(p) =>
            p.$size === 'large' &&
            css`
                font-size: ${(p) => p.theme.font.size.normal};
                line-height: ${(p) => p.theme.font.lineHeight.normal};
            `};

        &:hover {
            text-decoration: underline;
        }
    }

    .MessageDate {
        color: ${(p) => p.theme.color.text.grey};

        ${(p) =>
            p.$size === 'normal' &&
            css`
                font-size: ${(p) => p.theme.font.size.tiny};
                line-height: ${(p) => p.theme.font.lineHeight.tiny};
            `};

        ${(p) =>
            p.$size === 'large' &&
            css`
                font-size: ${(p) => p.theme.font.size.small};
                line-height: ${(p) => p.theme.font.lineHeight.small};
            `};
    }

    .MessageRepliesButton {
        width: 100%;
        padding-left: 0;
        background-color: transparent;

        .ButtonContent {
            justify-content: flex-start;
        }

        svg {
            margin: 0 5px 1px 0;
        }
    }

    ${(p) =>
        !p.$disabled &&
        p.$hasActions &&
        css`
            &:hover {
                background-color: ${p.theme.color.hover};

                .MessageActions {
                    opacity: 1;
                }
            }

            .MessageActions {
                position: absolute;
                top: -8px;
                right: 8px;
                display: flex;
                background-color: white;
                padding: 2px;
                border: 1px solid ${p.theme.color.grey[30]};
                border-radius: ${p.theme.radius.large};
                overflow: hidden;
                opacity: 0;

                ${p.$showActions &&
                css`
                    opacity: 1;
                `};
            }
        `};
`

const Message = forwardRef(
    (
        {
            entity,
            size = 'normal',
            disabled,
            displayDate,
            canReply,
            username,
            scrollLockId,
            ...rest
        },
        ref,
    ) => {
        const {
            guid,
            canEdit,
            richDescription,
            timeCreated,
            sender,
            replyCount,
            reactions,
        } = entity

        const {
            globalState: { subthreadGuid },
            setGlobalState,
        } = useContext(globalStateContext)

        const { t } = useTranslation()

        const toggleEmoji = useToggleEmoji(guid)

        const [isFocused, setIsFocused] = useState(false)

        const [isEditing, setIsEditing] = useState(false)
        const toggleEditing = () => {
            setIsEditing(!isEditing)
        }

        const [showDeleteModal, setShowDeleteModal] = useState(false)
        const toggleDeleteModal = () => {
            setShowDeleteModal(!showDeleteModal)
        }

        const deleteMessage = () => {
            setIsFocused(false)
            setShowDeleteModal(true)
        }

        const handleBlur = (evt) => {
            if (!evt.currentTarget.contains(evt.relatedTarget)) {
                setIsFocused(false)
            }
        }

        const { name, url } = sender

        const toggleSubthread = () => {
            setGlobalState((newState) => {
                newState.subthreadGuid = subthreadGuid === guid ? null : guid
            })
        }

        const hasActions = !disabled && !isEditing && (canReply || canEdit)

        return (
            <Wrapper
                ref={ref}
                $disabled={disabled}
                $showActions={isFocused}
                $hasActions={hasActions}
                $size={size}
                onFocus={() => setIsFocused(true)}
                onBlur={handleBlur}
                {...rest}
            >
                <div className="MessageAuthorIcon">
                    <UserAvatar user={sender} />
                </div>
                <div
                    style={{
                        flexGrow: 1,
                    }}
                >
                    {isEditing ? (
                        <EditMessageForm
                            entity={entity}
                            toggleEditing={toggleEditing}
                        />
                    ) : (
                        <>
                            <div className="MessageMeta">
                                <Link to={url} className="MessageAuthorName">
                                    {name}
                                </Link>
                                {displayDate && (
                                    <DisplayDate
                                        date={timeCreated}
                                        type={displayDate}
                                        placement="right"
                                        className="MessageDate"
                                    />
                                )}
                            </div>

                            <TiptapView
                                content={richDescription}
                                textSize={size}
                            />

                            <EmojiReactions
                                guid={guid}
                                username={username}
                                reactions={reactions}
                                scrollLockId={scrollLockId}
                            />

                            {canReply && replyCount > 0 && (
                                <Button
                                    variant="tertiary"
                                    size="normal"
                                    outline={false}
                                    className="MessageRepliesButton"
                                    onClick={toggleSubthread}
                                >
                                    <RepliesSmallIcon />
                                    {t('comments.count-replies', {
                                        count: replyCount,
                                    })}
                                </Button>
                            )}
                        </>
                    )}
                </div>
                {hasActions && (
                    <div className="MessageActions">
                        <Flexer gutter="none">
                            <OpenEmojiPicker
                                toggleEmoji={toggleEmoji}
                                onHide={() => setIsFocused(false)}
                                buttonType="menu"
                                scrollLockId={scrollLockId}
                            />

                            {canReply && (
                                <IconButton
                                    size="normal"
                                    variant="secondary"
                                    tooltip={t('comments.reply')}
                                    placement="top"
                                    radiusStyle="rounded"
                                    aria-pressed={subthreadGuid === guid}
                                    onClick={toggleSubthread}
                                >
                                    <RepliesIcon />
                                </IconButton>
                            )}
                        </Flexer>

                        {canEdit && (
                            <>
                                <DropdownButton
                                    options={[
                                        {
                                            onClick: () => toggleEditing(),
                                            name: t('action.edit'),
                                        },
                                        {
                                            onClick: deleteMessage,
                                            name: t('action.delete'),
                                            warn: true,
                                        },
                                    ]}
                                    placement="right-start"
                                    offset={[-3, 3]}
                                >
                                    <IconButton
                                        size="normal"
                                        variant="secondary"
                                        tooltip={t('global.options')}
                                        aria-label={t('action.show-options')}
                                        placement="top"
                                        radiusStyle="rounded"
                                    >
                                        <OptionsIcon />
                                    </IconButton>
                                </DropdownButton>
                                <Modal
                                    isVisible={showDeleteModal}
                                    onClose={toggleDeleteModal}
                                    showCloseButton
                                    title={t('chat.delete-message')}
                                    size="normal"
                                >
                                    <DeleteMessage
                                        entity={entity}
                                        onClose={toggleDeleteModal}
                                    />
                                </Modal>
                            </>
                        )}
                    </div>
                )}
            </Wrapper>
        )
    },
)

Message.displayName = 'Message'

Message.propTypes = {
    size: PropTypes.oneOf(['normal', 'large']),
}

export default Message
