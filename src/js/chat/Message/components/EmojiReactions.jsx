import React from 'react'
import { useTranslation } from 'react-i18next'
import styled, { css } from 'styled-components'

import { useToggleEmoji } from 'js/chat/Message/hooks/useToggleEmoji'
import Flexer from 'js/components/Flexer/Flexer'
import HideVisually from 'js/components/HideVisually/HideVisually'
import Tooltip from 'js/components/Tooltip/Tooltip'

import OpenEmojiPicker from './OpenEmojiPicker'

function getEmojiByUnicode(unicode) {
    if (typeof unicode !== 'string') return unicode
    return String.fromCodePoint(
        ...unicode.split('-').map((c) => parseInt(c, 16)),
    )
}

const CountButtonHeight = '26px'

const CountButton = styled.button`
    display: flex;
    align-items: center;
    justify-content: center;
    gap: 4px;
    height: ${CountButtonHeight};
    padding: 0 6px;
    background-color: ${(p) => p.theme.color.hover};
    border: 2px solid transparent;
    border-radius: calc(${CountButtonHeight} / 2);
    cursor: pointer;
    line-height: 1;

    ${(p) =>
        p.$checked &&
        css`
            background-color: white;
            color: ${(p) => p.theme.color.secondary.main};
            border-width: 2px;
            border-color: ${p.theme.color.secondary.main};

            &:hover {
                background-color: ${(p) => p.theme.color.hover};
            }
        `};

    ${(p) =>
        !p.$checked &&
        css`
            &:hover {
                background-color: white;
                border-color: black;
            }
        `};

    &:active {
        background-color: ${(p) => p.theme.color.active};
    }

    .EmojiReactionIcon {
        font-size: 18px;
        margin-top: 3px;
    }

    .EmojiReactionCount {
        font-size: 14px;
        font-weight: ${(p) =>
            p.$checked ? p.theme.font.weight.bold : p.theme.font.weight.normal};
    }
`

const EmojiReactions = ({ guid, username, reactions, scrollLockId }) => {
    const { t } = useTranslation()

    const toggleEmoji = useToggleEmoji(guid)

    return (
        <Flexer
            justifyContent="flex-start"
            gutter="tiny"
            wrap
            style={{ marginTop: '4px', marginBottom: 0 }}
        >
            {reactions?.length > 0 && (
                <>
                    {reactions.map((reaction, i) => (
                        <Tooltip
                            key={i}
                            content={reaction.users.join(', ')}
                            placement="top"
                            delay
                        >
                            <CountButton
                                onClick={() => {
                                    toggleEmoji(reaction.unicode)
                                }}
                                $checked={reaction.users.includes(username)}
                                aria-label={t('chat.toggle-emoji')}
                            >
                                <HideVisually>
                                    {t('chat.emoji-reactions', {
                                        count: reaction.count,
                                    })}
                                </HideVisually>

                                <span className="EmojiReactionIcon">
                                    {getEmojiByUnicode(reaction.unicode)}
                                </span>

                                <span className="EmojiReactionCount">
                                    {reaction.count}
                                </span>
                            </CountButton>
                        </Tooltip>
                    ))}

                    <OpenEmojiPicker
                        toggleEmoji={toggleEmoji}
                        scrollLockId={scrollLockId}
                    />
                </>
            )}
        </Flexer>
    )
}

export default EmojiReactions
