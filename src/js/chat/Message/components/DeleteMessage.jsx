import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'

import Message from '../Message'

const Wrapper = styled.form`
    .DeleteMessagePreview {
        margin-top: 16px;
        border: 1px solid ${(p) => p.theme.color.grey[30]};
    }
`

const DeleteMessage = ({ entity, onClose }) => {
    const [deleteMessage, { loading, error }] = useMutation(DELETE_MESSAGE)

    const onSubmit = (e) => {
        e.preventDefault()

        deleteMessage({
            variables: {
                input: {
                    guid: entity.guid,
                },
            },
        }).then(() => {
            onClose()
        })
    }

    const { t } = useTranslation()

    return (
        <Wrapper onSubmit={onSubmit}>
            <p>{t('chat.delete-message-confirm')}</p>

            <Message
                entity={entity}
                disabled
                displayDate="dateTime"
                className="DeleteMessagePreview"
            />

            <Errors errors={error} />

            <Flexer mt>
                <Button
                    size="normal"
                    variant="tertiary"
                    type="button"
                    onClick={onClose}
                >
                    {t('action.no-cancel')}
                </Button>
                <Button
                    size="normal"
                    variant="warn"
                    type="submit"
                    loading={loading}
                >
                    {t('action.yes-delete')}
                </Button>
            </Flexer>
        </Wrapper>
    )
}

const DELETE_MESSAGE = gql`
    mutation deleteChatMessage($input: deleteChatMessageInput!) {
        deleteChatMessage(input: $input) {
            success
        }
    }
`

export default DeleteMessage
