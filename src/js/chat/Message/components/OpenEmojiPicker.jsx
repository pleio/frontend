import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'
import { useScrollLock } from 'usehooks-ts'

import EmojiPicker from 'js/components/EmojiPicker/EmojiPicker'
import IconButton from 'js/components/IconButton/IconButton'
import Popover from 'js/components/Popover/Popover'

import PickEmojiIcon from 'icons/pick-emoji.svg'

const SmallEmojiPickerButton = styled(IconButton)`
    width: 32px;
    height: 26px;

    .IconButtonBackground {
        background-color: ${(p) => p.theme.color.hover};
        border-radius: 13px;
    }

    &:hover .IconButtonBackground {
        border: 2px solid black;
    }
`

const OpenEmojiPicker = ({
    toggleEmoji,
    onHide,
    buttonType = 'main',
    scrollLockId,
}) => {
    const { t } = useTranslation()

    const [showEmojiPicker, setShowEmojiPicker] = useState(false)

    // When the emoji picker is open, we want to lock the body scroll and the chat scroll.
    const { lock: lockBody, unlock: unlockBody } = useScrollLock({
        autoLock: false,
    })
    const { lock, unlock } = useScrollLock({
        autoLock: false,
        lockTarget: scrollLockId ? `#${scrollLockId}` : 'body',
    })

    const hidePicker = () => {
        setShowEmojiPicker(false)
        onHide?.()
        unlock()
        unlockBody()
    }

    const handleClick = () => {
        if (showEmojiPicker) {
            unlock()
            unlockBody()
        } else {
            lock()
            lockBody()
        }

        setShowEmojiPicker(!showEmojiPicker)
    }

    return (
        <Popover
            visible={showEmojiPicker}
            content={
                <EmojiPicker
                    onEmojiClick={(emo) => {
                        toggleEmoji(emo.unified)
                        hidePicker()
                    }}
                />
            }
            onClickOutside={hidePicker}
            offset={[10, 10]}
            maxWidth={null}
        >
            {buttonType === 'main' ? (
                <SmallEmojiPickerButton
                    size="small"
                    variant="secondary"
                    tooltip={t('chat.select-emoji')}
                    placement="top"
                    radiusStyle="round"
                    onClick={handleClick}
                >
                    <PickEmojiIcon />
                </SmallEmojiPickerButton>
            ) : (
                <IconButton
                    size="normal"
                    variant="secondary"
                    tooltip={t('chat.select-emoji')}
                    placement="top"
                    radiusStyle="rounded"
                    onClick={handleClick}
                >
                    <PickEmojiIcon width="20" height="20" />
                </IconButton>
            )}
        </Popover>
    )
}

export default OpenEmojiPicker
