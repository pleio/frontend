import React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'

import MessageEditor from 'js/chat/components/MessageEditor'
import messageFragment from 'js/chat/fragments/message'
import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'

const EditMessageForm = ({ entity, toggleEditing }) => {
    const { t } = useTranslation()

    const { guid, richDescription, chat } = entity
    const group = chat?.container?.group

    const defaultValues = {
        richDescription,
    }

    const {
        control,
        handleSubmit,
        formState: { isValid, isDirty, isSubmitting },
    } = useForm({ mode: 'onChange', defaultValues })

    const [editMessage] = useMutation(EDIT_MESSAGE)

    const submitEdit = async ({ richDescription }) => {
        if (!isDirty) {
            toggleEditing()
            return
        }

        await editMessage({
            variables: {
                input: {
                    guid,
                    richDescription,
                },
            },
        })
            .then(() => {
                toggleEditing()
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    const handleKeyDownEditor = (evt) => {
        if (evt.key === 'Enter' && !evt.shiftKey) {
            evt.preventDefault()
            handleSubmit(submitEdit)()
        }
        if (evt.key === 'Escape') {
            toggleEditing()
        }
    }

    return (
        <form
            onSubmit={handleSubmit(submitEdit)}
            style={{ position: 'relative' }}
        >
            <MessageEditor
                control={control}
                name="richDescription"
                id={`${guid}-edit-message`}
                onKeyDown={handleKeyDownEditor}
                group={group}
            />
            <Flexer
                style={{
                    position: 'absolute',
                    bottom: '12px',
                    right: '12px',
                }}
            >
                <Button
                    size="normal"
                    variant="tertiary"
                    onClick={toggleEditing}
                >
                    {t('action.cancel')}
                </Button>
                <Button
                    size="normal"
                    variant="primary"
                    type="submit"
                    disabled={!isValid || !isDirty}
                    loading={isSubmitting}
                >
                    {t('action.save')}
                </Button>
            </Flexer>
        </form>
    )
}

const EDIT_MESSAGE = gql`
    mutation EditChatMessage($input: editChatMessageInput!) {
        editChatMessage(input: $input) {
            chatMessage {
                ${messageFragment}
            }
        }
    }
`

export default EditMessageForm
