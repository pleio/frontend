import { gql, useMutation } from '@apollo/client'

export const useToggleEmoji = (guid) => {
    const [toggleReaction] = useMutation(TOGGLE_UNICODE_REACTION)

    const toggleEmoji = (unicode) => {
        toggleReaction({
            variables: {
                input: {
                    guid,
                    unicode,
                },
            },
        })
    }

    return toggleEmoji
}

const TOGGLE_UNICODE_REACTION = gql`
    mutation ($input: toggleChatReactionInput!) {
        toggleChatReaction(input: $input) {
            chatMessage {
                guid
                reactions {
                    unicode
                    users
                    count
                }
            }
        }
    }
`
