import React from 'react'
import { useTranslation } from 'react-i18next'

import ChatContainerDropdown from 'js/chat/components/ChatContainerDropdown'
import getChatContainer from 'js/chat/helpers/getChatContainer'
import Avatar from 'js/components/Avatar/Avatar'
import IconButton from 'js/components/IconButton/IconButton'
import Tooltip from 'js/components/Tooltip/Tooltip'
import { useSiteStore } from 'js/lib/stores'

import CameraIcon from 'icons/camera.svg'
import ChevronDownIcon from 'icons/chevron-down.svg'

import ChatHeaderTitle from './ChatHeaderTitle'

const ChatContainer = ({
    chat,
    buttonClass,
    titleClass,
    isOverlay = false,
}) => {
    const { t } = useTranslation()

    const { site } = useSiteStore()

    const { guid, type } = chat
    const container = getChatContainer(chat?.container)

    const Title = (
        <ChatHeaderTitle isLarge={!isOverlay} className={titleClass}>
            {container.title}
        </ChatHeaderTitle>
    )

    return (
        <>
            {container.isSingle ? (
                <ChatContainerDropdown
                    container={container}
                    chatGuid={guid}
                    arrowElement={
                        <ChevronDownIcon
                            style={{
                                marginLeft: 6,
                                marginRight: isOverlay ? 2 : null,
                                marginTop: isOverlay ? null : 12,
                            }}
                        />
                    }
                    isExternal={!isOverlay}
                >
                    <button type="button" className={buttonClass}>
                        {!isOverlay && (
                            <Avatar
                                disabled
                                image={container.entities[0]?.icon}
                                name={container.entities[0]?.name}
                                size="normal"
                                radiusStyle={
                                    type === 'group' ? 'rounded' : 'round'
                                }
                                style={{ marginRight: '10px' }}
                            />
                        )}
                        {Title}
                    </button>
                </ChatContainerDropdown>
            ) : (
                <Tooltip
                    content={container.title}
                    placement="bottom"
                    offset={[0, 0]}
                    delay={[1500, 0]}
                >
                    {Title}
                </Tooltip>
            )}
            {site.integratedVideocallEnabled && (
                <IconButton
                    size="normal"
                    variant="primary"
                    radiusStyle="rounded"
                    as="a"
                    href={`/videocall/chat/${guid}`}
                    target="_blank"
                    rel="noopener noreferrer"
                    tooltip={t('global.video-call')}
                    placement="top"
                >
                    <CameraIcon />
                </IconButton>
            )}
        </>
    )
}

export default ChatContainer
