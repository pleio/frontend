import React from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

const Wrapper = styled.div`
    position: relative; // Position .MessageDivider accordingly
    display: flex;
    z-index: 1; // Position on top of messages
    pointer-events: none;

    ${(p) =>
        p.$size === 'normal' &&
        css`
            padding: 4px 20px;
        `};

    ${(p) =>
        p.$size === 'large' &&
        css`
            padding: 8px 20px;
        `};

    &[data-sticky='true'] {
        z-index: 2;

        .MessageDivider,
        .MessageDate {
            box-shadow: ${(p) => p.theme.shadow.soft.bottom};
        }
    }

    .MessageDivider {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 50%;
        background-color: white;
        border-bottom: 1px solid ${(p) => p.theme.color.grey[20]};
    }

    .MessageDate {
        position: relative; // Position on top of .MessageDivider
        display: flex;
        align-items: center;
        background-color: white;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        font-weight: ${(p) => p.theme.font.weight.semibold};
        border: 1px solid ${(p) => p.theme.color.grey[30]};
        border-radius: 16px;
        pointer-events: auto;

        ${(p) =>
            p.$size === 'normal' &&
            css`
                height: 28px;
                padding: 0 12px;
            `};

        ${(p) =>
            p.$size === 'large' &&
            css`
                height: 32px;
                padding: 0 16px;
            `};
    }
`

const MessageDivider = ({ size = 'normal', children, ...rest }) => (
    <Wrapper $size={size} {...rest}>
        <div className="MessageDivider" />
        {children && <div className="MessageDate">{children}</div>}
    </Wrapper>
)

MessageDivider.propTypes = {
    size: PropTypes.oneOf(['normal', 'large']),
}

export default MessageDivider
