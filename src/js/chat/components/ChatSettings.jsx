import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { Trans, useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { gql, useMutation } from '@apollo/client'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import Card from 'js/components/Card/Card'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import { H4 } from 'js/components/Heading'
import { usePushManagerSubscription } from 'js/lib/hooks'

const PushWarning = styled(Card)`
    padding: 8px 16px;

    a {
        color: ${(p) => p.theme.color.pleio};
        text-decoration: underline;
    }
`

const ChatSettings = ({ user = {}, onClose }) => {
    const { t } = useTranslation()

    const [subscribed, setSubscribed] = useState(null)
    usePushManagerSubscription(setSubscribed)

    const { control, register, handleSubmit } = useForm({
        defaultValues: {
            isChatUsersNotificationPushEnabled:
                user.isChatUsersNotificationPushEnabled,
            isChatGroupNotificationPushEnabled:
                user.isChatGroupNotificationPushEnabled,
        },
    })

    const [updateChatNotificationSettings, { loading }] = useMutation(
        UPDATE_CHAT_NOTIFICATION_SETTINGS,
        { refetchQueries: ['TopMenu'] },
    )

    const onSubmit = ({
        guid,
        isChatUsersNotificationPushEnabled,
        isChatGroupNotificationPushEnabled,
    }) => {
        updateChatNotificationSettings({
            variables: {
                input: {
                    guid,
                    isChatUsersNotificationPushEnabled,
                    isChatGroupNotificationPushEnabled,
                },
            },
        })
            .then(onClose)
            .catch((error) => console.error(error))
    }

    if (!user) return null

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <input type="hidden" {...register('guid')} value={user.guid} />

            {!subscribed && (
                <PushWarning>
                    <Trans i18nKey="chat.push-settings-message">
                        Push notifications are disabled for your current device.
                        You can enable this in
                        <Link
                            to={`/user/${user.guid}/settings#notifications`}
                            onClick={onClose}
                        >
                            your settings
                        </Link>
                        .
                    </Trans>
                </PushWarning>
            )}

            <fieldset style={{ marginTop: 16 }}>
                <H4 style={{ marginBottom: 8 }}>
                    {t('settings.notifications')}
                </H4>

                <FormItem
                    type="checkbox"
                    name="isChatUsersNotificationPushEnabled"
                    control={control}
                    label={t('chat.notification-user-message')}
                    size="small"
                />
                <FormItem
                    type="checkbox"
                    name="isChatGroupNotificationPushEnabled"
                    control={control}
                    label={t('chat.notification-group-message')}
                    size="small"
                />
            </fieldset>

            <Flexer mt>
                <Button size="normal" variant="tertiary" onClick={onClose}>
                    {t('action.cancel')}
                </Button>
                <Button
                    size="normal"
                    variant="primary"
                    loading={loading}
                    type="submit"
                >
                    {t('action.save')}
                </Button>
            </Flexer>
        </form>
    )
}

const UPDATE_CHAT_NOTIFICATION_SETTINGS = gql`
    mutation ($input: editNotificationsInput!) {
        editNotifications(input: $input) {
            user {
                guid
            }
        }
    }
`

export default ChatSettings
