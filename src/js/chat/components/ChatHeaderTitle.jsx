import React from 'react'

import { H3, H4 } from 'js/components/Heading'

const ChatHeaderTitle = ({ isLarge = false, children, ...rest }) => {
    const TitleComponent = isLarge ? H3 : H4

    return (
        <TitleComponent
            as="h2"
            {...rest}
            style={{
                whiteSpace: 'nowrap',
                overflow: 'hidden', // Needed for ellipsis
                textOverflow: 'ellipsis',
            }}
        >
            {children}
        </TitleComponent>
    )
}

export default ChatHeaderTitle
