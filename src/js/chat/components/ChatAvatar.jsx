import React from 'react'
import styled from 'styled-components'

import Avatar from 'js/components/Avatar/Avatar'

const Wrapper = styled.div`
    position: relative;
    width: 24px;
    height: 24px;

    .ChatAvatarMultipleIndicator {
        position: absolute;
        top: 11px;
        left: 11px;
        display: flex;
        align-items: center;
        justify-content: center;
        min-width: 16px;
        height: 16px;
        padding: 0 2px;
        border-radius: 8px;
        background-color: ${(p) => p.theme.color.grey[20]};
        border: 2px solid white;
        font-size: ${(p) => p.theme.font.size.tiny};
        line-height: ${(p) => p.theme.font.lineHeight.tiny};
        font-weight: ${(p) => p.theme.font.weight.bold};
        color: black;
        white-space: nowrap;
    }
`

const ChatAvatar = ({ container, ...rest }) => {
    const { type, entities } = container
    const isMultiple = entities.length > 1
    const isOnline = entities[0]?.isChatOnline

    return (
        <Wrapper $isMultiple={isMultiple} {...rest}>
            <Avatar
                disabled
                image={entities[0]?.icon}
                name={entities[0]?.name}
                size={isMultiple ? 'tiny' : 'small'}
                radiusStyle={type === 'group' ? 'rounded' : 'round'}
                status={
                    type === 'users' ? (isOnline ? 'online' : 'offline') : null
                }
            />
            {isMultiple && (
                <div className="ChatAvatarMultipleIndicator">
                    {entities.length > 99 ? '99+' : entities.length}
                </div>
            )}
        </Wrapper>
    )
}

export default ChatAvatar
