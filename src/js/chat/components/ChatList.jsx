import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'
import styled from 'styled-components'

import chatFragment from 'js/chat/fragments/chat'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'

import ChatListItem from './ChatListItem'
import MenuSection from './MenuSection'

const Wrapper = styled.div`
    overflow-y: auto;
    padding: 8px 4px;

    > *:not(:last-child) {
        margin-bottom: 8px;
    }
`

const ChatList = ({ mainThreadGuid, onClickItem, ...rest }) => {
    const { t } = useTranslation()

    const { loading, data, subscribeToMore, refetch } = useQuery(GET_CHATS)

    useEffect(() => {
        const unsubscribe = subscribeToMore({
            document: ON_CHATS_UPDATE,
            updateQuery: async (prev, { subscriptionData }) => {
                if (!subscriptionData.data) return prev

                const { action } = subscriptionData.data.onChatsUpdate

                if (action === 'refetch') {
                    refetch()
                }
            },
        })
        return () => unsubscribe()
    }, [subscribeToMore, refetch])

    if (loading) {
        return <LoadingSpinner style={{ height: '48px' }} {...rest} />
    } else {
        return (
            <Wrapper {...rest}>
                {data.groupChats.edges.length > 0 && (
                    <MenuSection title={t('chat.menu-groups')} name="groups">
                        <ul>
                            {data.groupChats.edges.map((entity) => {
                                return (
                                    <ChatListItem
                                        key={entity.guid}
                                        entity={entity}
                                        mainThreadGuid={mainThreadGuid}
                                        onClick={onClickItem}
                                    />
                                )
                            })}
                        </ul>
                    </MenuSection>
                )}

                {data.userChats.edges.length > 0 && (
                    <MenuSection title={t('chat.menu-users')} name="users">
                        <ul>
                            {data.userChats.edges.map((entity) => {
                                return (
                                    <ChatListItem
                                        key={entity.guid}
                                        entity={entity}
                                        mainThreadGuid={mainThreadGuid}
                                        onClick={onClickItem}
                                    />
                                )
                            })}
                        </ul>
                    </MenuSection>
                )}
            </Wrapper>
        )
    }
}

const GET_CHATS = gql`
    query Chats {
        groupChats: chats(type: group, limit: 999) {
            total
            edges {
                ${chatFragment}
            }
        }
        userChats: chats(type: user, limit: 999) {
            total
            edges {
                ${chatFragment}
            }
        }
    }
`

const ON_CHATS_UPDATE = gql`
    subscription onChatsUpdate {
        onChatsUpdate {
            guid
            action
        }
    }
`

export default ChatList
