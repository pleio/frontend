import React from 'react'
import { useTranslation } from 'react-i18next'
import { NavLink } from 'react-router-dom'
import styled, { css } from 'styled-components'

import getChatContainer from 'js/chat/helpers/getChatContainer'
import Tooltip from 'js/components/Tooltip/Tooltip'

import ChatAvatar from './ChatAvatar'

const Wrapper = styled.li`
    .ChatListItemLink {
        width: 100%;
        position: relative;
        display: flex;
        padding: 4px 8px 4px 10px;
        border-radius: ${(p) => p.theme.radius.tiny};

        &:hover {
            background-color: ${(p) => p.theme.color.hover};

            .ChatAvatarMultipleIndicator {
                border-color: ${(p) => p.theme.color.grey[10]};
            }
        }

        &:active {
            background-color: ${(p) => p.theme.color.active};
        }

        &[aria-current='page'] {
            color: ${(p) => p.theme.color.primary.main};

            &:before {
                position: absolute;
                content: '';
                width: 4px;
                height: 24px;
                border-radius: 2px;
                background-color: ${(p) => p.theme.color.primary.main};
                top: 4px;
                left: 0;
            }
        }

        body:not(.mouse-user) &:focus {
            outline: ${(p) => p.theme.focusStyling};
        }
    }

    .ChatListItemAvatar {
        margin-right: 10px;
    }

    .MenuItemName {
        padding: 2px 0;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;

        ${(p) =>
            p.$unread &&
            css`
                font-weight: ${(p) => p.theme.font.weight.bold};
            `};
    }
`

const ChatListItem = ({ entity, mainThreadGuid, onClick }) => {
    const { t } = useTranslation()

    const { guid, unread } = entity

    const LinkElement = onClick ? 'button' : NavLink

    const linkElementProps = onClick
        ? {
              type: 'button',
              'aria-current': mainThreadGuid === guid ? 'page' : null,
              onClick: () => onClick(entity),
          }
        : {
              to: `/chat/${guid}`,
          }

    const container = getChatContainer(entity.container)

    if (!container) {
        console.error(t('error.chat-container-not-found'), entity)
        return null
    }

    const isMultiple = !container.isSingle

    return (
        <Wrapper $unread={unread} $isMultiple={isMultiple}>
            <Tooltip
                content={container.title}
                placement="top"
                offset={[0, 0]}
                delay={[1500, 0]}
            >
                <LinkElement {...linkElementProps} className="ChatListItemLink">
                    <ChatAvatar
                        container={container}
                        className="ChatListItemAvatar"
                    />
                    <div className="MenuItemName">
                        {container.title}
                        {unread > 0 ? ` (${unread})` : null}
                    </div>
                </LinkElement>
            </Tooltip>
        </Wrapper>
    )
}

export default ChatListItem
