import React from 'react'
import { useForm } from 'react-hook-form'
import { Trans, useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import messageFragment from 'js/chat/fragments/message'
import Button from 'js/components/Button/Button'
import Text from 'js/components/Text/Text'
import Tooltip from 'js/components/Tooltip/Tooltip'

import SendFillIcon from 'icons/send-fill.svg'

import MessageEditor from './MessageEditor'

const Wrapper = styled.form`
    flex-shrink: 0;
    padding: ${(p) =>
        `0 ${p.theme.padding.horizontal.small} ${p.theme.padding.vertical.small}`};
    margin-top: auto;

    .TiptapEditorContent {
        max-height: 50vh;
        overflow-y: auto;
    }

    .ThreadSendBoxEditor > * {
        border-radius: ${(p) => p.theme.radius.large};

        .TiptapEditorToolbar {
            border-top-left-radius: ${(p) => p.theme.radius.large};
            border-top-right-radius: ${(p) => p.theme.radius.large};
        }
    }
`

const SendMessage = ({
    size = 'normal',
    group,
    chatGuid,
    chatMessageGuid,
    ...rest
}) => {
    const { t } = useTranslation()

    const defaultValues = {
        richDescription: '',
    }

    const {
        control,
        handleSubmit,
        reset,
        formState: { isValid, isSubmitting },
    } = useForm({ mode: 'onChange', defaultValues })

    const [addMessage] = useMutation(ADD_MESSAGE)

    const submit = async ({ richDescription }) => {
        await addMessage({
            variables: {
                input: {
                    chatGuid,
                    chatMessageGuid,
                    richDescription,
                },
            },
        })
            .then(() => {
                reset()
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    const submitForm = handleSubmit(submit)

    const handleKeyDown = (evt) => {
        if (evt.key === 'Enter' && !evt.shiftKey) {
            evt.preventDefault()
            if (isValid) submitForm()
        }
    }

    return (
        <Wrapper onSubmit={handleSubmit(submit)} {...rest}>
            <div style={{ position: 'relative' }}>
                <MessageEditor
                    control={control}
                    name="richDescription"
                    id={`${chatGuid || chatMessageGuid}-send-message`}
                    group={group}
                    className="ThreadSendBoxEditor"
                    onKeyDown={handleKeyDown}
                    textSize={size}
                />

                <div
                    style={{
                        position: 'absolute',
                        bottom: '12px',
                        right: '12px',
                    }}
                >
                    <Tooltip
                        content={t('action.send')}
                        theme="primary-white"
                        delay
                    >
                        <Button
                            size="normal"
                            variant={isValid ? 'primary' : 'tertiary'}
                            type="submit"
                            disabled={!isValid}
                            outline={false}
                            loading={isSubmitting}
                            aria-label={t('action.send')}
                        >
                            <SendFillIcon />
                        </Button>
                    </Tooltip>
                </div>
            </div>
            {isValid ? (
                <Text size="tiny" variant="grey" style={{ marginTop: '8px' }}>
                    <Trans i18nKey="chat.send-message-helper">
                        <strong>Enter</strong> to send.{' '}
                        <strong>Shift + Enter</strong> to add a new line.
                    </Trans>
                </Text>
            ) : null}
        </Wrapper>
    )
}

const ADD_MESSAGE = gql`
    mutation AddChatMessage($input: addChatMessageInput!) {
        addChatMessage(input: $input) {
            chatMessage {
                ${messageFragment}
            }
        }
    }
`

SendMessage.propTypes = {
    size: PropTypes.oneOf(['normal', 'large']),
}

export default SendMessage
