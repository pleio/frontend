import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useMutation, useQuery } from '@apollo/client'

import DropdownButton from 'js/components/DropdownButton/DropdownButton'

const ChatContainerDropdown = ({
    chatGuid,
    container,
    isExternal,
    arrowElement,
    children,
    ...rest
}) => {
    const { data } = useQuery(GET_CHAT_DETAILS, {
        variables: {
            chatGuid,
        },
    })

    return data?.chat ? (
        <ChatContainerDropdownView
            chat={data.chat}
            container={container}
            isExternal={isExternal}
            arrowElement={arrowElement}
            {...rest}
        >
            {children}
        </ChatContainerDropdownView>
    ) : (
        children
    )
}

const ChatContainerDropdownView = ({
    isExternal, // Should links open in new tab
    container,
    chat,
    arrowElement,
    children,
    ...rest
}) => {
    const { t } = useTranslation()

    const [updateChatSettings] = useMutation(UPDATE_CHAT_SETTINGS, {
        variables: {
            input: {
                guid: chat.guid,
                isNotificationPushEnabled: !chat?.isNotificationPushEnabled,
            },
        },
        refetchQueries: ['GetChatDetails'],
    })

    const { type, entities } = container

    const options = [
        {
            name:
                type === 'group'
                    ? t('chat.view-group')
                    : t('chat.view-profile'),
            ...(isExternal
                ? {
                      href: entities[0]?.url,
                      target: '_blank',
                  }
                : {
                      to: entities[0]?.url,
                  }),
        },
        {
            name: chat.isNotificationPushEnabled
                ? t('chat.mute-conversation')
                : t('chat.unmute-conversation'),
            onClick: updateChatSettings,
            disableCloseAfterClick: true,
        },
    ]

    return (
        <DropdownButton
            options={options}
            appendToDocument // Dropdown isn't visible because of overflow hidden and needed for ellipsis
            showArrow
            arrowElement={arrowElement}
            {...rest}
        >
            {children}
        </DropdownButton>
    )
}

const GET_CHAT_DETAILS = gql`
    query GetChatDetails($chatGuid: String) {
        chat(chatGuid: $chatGuid) {
            guid
            isNotificationPushEnabled
        }
    }
`
const UPDATE_CHAT_SETTINGS = gql`
    mutation ($input: editChatInput!) {
        editChat(input: $input) {
            chat {
                isNotificationPushEnabled
            }
        }
    }
`

export default ChatContainerDropdown
