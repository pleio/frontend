import React, { forwardRef } from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

import Text from 'js/components/Text/Text'

const Wrapper = styled.div`
    flex-grow: 1; // Needed for the layout to work
    display: flex;
    flex-direction: column;
    justify-content: center;
    width: 100%;
    text-align: center;
    padding: ${(p) => `32px ${p.theme.padding.horizontal.small} 56px`};

    .WelcomeImage {
        width: 100%;
        margin: 0 auto;

        + * {
            margin-top: 24px; // Spacing between image and text
        }
    }

    .WelcomeTitle {
        margin-bottom: 4px;
        font-weight: ${(p) => p.theme.fontHeading.weight.bold};
        color: ${(p) => p.theme.color.text.black};

        ${(p) =>
            p.$size === 'normal' &&
            css`
                font-size: ${(p) => p.theme.fontHeading.size.small};
                line-height: ${(p) => p.theme.fontHeading.lineHeight.small};
            `};

        ${(p) =>
            p.$size === 'large' &&
            css`
                font-size: ${(p) => p.theme.fontHeading.size.normal};
                line-height: ${(p) => p.theme.fontHeading.lineHeight.normal};
            `};
    }
`

const Welcome = forwardRef(
    ({ title, subtitle, image, imageWidth, size = 'normal', ...rest }, ref) => (
        <Wrapper ref={ref} $size={size} {...rest}>
            <img
                src={image}
                className="WelcomeImage"
                alt=""
                aria-hidden
                style={{ maxWidth: imageWidth, marginTop: '8px' }}
            />
            {title && <h2 className="WelcomeTitle">{title}</h2>}
            {subtitle && (
                <Text size={size} className="WelcomeSubtitle">
                    {subtitle}
                </Text>
            )}
        </Wrapper>
    ),
)

Welcome.displayName = 'Welcome'

Welcome.propTypes = {
    title: PropTypes.string,
    subtitle: PropTypes.string,
    image: PropTypes.string.isRequired,
    imageWidth: PropTypes.string,
    size: PropTypes.oneOf(['normal', 'large']),
}

export default Welcome
