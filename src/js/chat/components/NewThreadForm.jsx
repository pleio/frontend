import React, { useEffect, useRef, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useMeasure } from 'react-use'
import { gql, useLazyQuery, useMutation } from '@apollo/client'
import { useDebounce } from 'helpers'
import styled from 'styled-components'

import Welcome from 'js/chat/components/Welcome'
import chatFragment from 'js/chat/fragments/chat'
import Flexer from 'js/components/Flexer/Flexer'
import { H4 } from 'js/components/Heading'
import IconButton from 'js/components/IconButton/IconButton'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Popover from 'js/components/Popover/Popover'
import Tag from 'js/components/Tag/Tag'
import Textfield from 'js/components/Textfield/Textfield'
import UserLink from 'js/components/UserLink'

import siteImage from 'images/site.png'

import EditSquareIcon from 'icons/edit-square.svg'

const Wrapper = styled.div`
    flex-grow: 1;
    display: flex;
    flex-direction: column;

    .NewThreadSearchResults {
        padding: 5px 0;
    }

    .NewThreadSearchResult {
        width: 100%;
        padding: 4px 12px;

        &:hover {
            background-color: ${(p) => p.theme.color.hover};
        }

        &:active {
            background-color: ${(p) => p.theme.color.active};
        }

        &:focus {
            color: ${(p) => p.theme.color.primary};
        }
    }

    .NewThreadSearchWrapper {
        padding: ${(p) =>
            `0 ${p.theme.padding.horizontal.small} ${p.theme.padding.vertical.small}`};
    }

    .NewThreadSearchLabel {
        flex-shrink: 0;
        padding: 11px 0;
    }

    .NewThreadSearchBox {
        flex-grow: 1;
        border-radius: ${(p) => p.theme.radius.normal};
        border: 1px solid ${(p) => p.theme.color.grey[40]};
    }

    .NewThreadUsers {
        padding: 12px 8px 0 10px; // Aligns nicely with .NewThreadSearchLabel
        max-height: 132px;
        overflow-y: auto;
    }

    .NewThreadSubmit {
        margin-right: 2px;
    }
`

const NewThreadForm = ({ onComplete, ...rest }) => {
    const { t } = useTranslation()

    const [userQuery, setUserQuery] = useState('')
    const [users, setUsers] = useState([])

    const [findUser, { loading, data }] = useLazyQuery(GET_USERS)

    const debouncedVal = useDebounce(userQuery, 200)

    const refInput = useRef()

    useEffect(() => {
        if (!debouncedVal) return

        findUser({
            variables: {
                q: debouncedVal,
            },
        })
    }, [debouncedVal, findUser])

    useEffect(() => {
        setUserQuery('')
        refInput.current.focus()
    }, [users])

    const [addChat, { loading: addChatLoading }] = useMutation(ADD_CHAT)

    const onSubmit = () => {
        addChat({
            variables: {
                input: {
                    userGuids: users.map((user) => user.guid),
                },
            },
        })
            .then(
                ({
                    data: {
                        addChat: { chat },
                    },
                }) => {
                    onComplete?.(chat)
                },
            )
            .catch((errors) => {
                console.error(errors)
            })
    }

    const [ref, { width, height }] = useMeasure()

    return (
        <Wrapper {...rest}>
            <Welcome
                ref={ref}
                subtitle={t('chat.new-thread-welcome')}
                image={siteImage}
                imageWidth="160px"
                size="normal"
            />

            <Popover
                visible={debouncedVal}
                placement="top"
                content={
                    <div
                        style={{
                            width,
                            maxHeight: height,
                        }}
                    >
                        <div className="NewThreadSearchResults">
                            {loading ? (
                                <LoadingSpinner style={{ height: '48px' }} />
                            ) : data?.users?.edges.length > 0 ? (
                                data?.users?.edges.map((user) => (
                                    <UserLink
                                        key={user.guid}
                                        entity={user}
                                        avatarSize="small"
                                        as="button"
                                        type="button"
                                        onClick={() =>
                                            setUsers([...users, user])
                                        }
                                        className="NewThreadSearchResult"
                                    />
                                ))
                            ) : (
                                <Flexer
                                    style={{
                                        minHeight: '48px',
                                        padding: '0 8px',
                                    }}
                                >
                                    {t('user.no-results')}
                                </Flexer>
                            )}
                        </div>
                    </div>
                }
                onHide={() => setUserQuery('')}
                arrow={false}
                offset={[0, 0]}
            >
                <Flexer
                    alignItems="flex-start"
                    justifyContent="flex-start"
                    className="NewThreadSearchWrapper"
                >
                    <H4
                        as="label"
                        htmlFor="new-thread-search-user"
                        className="NewThreadSearchLabel"
                    >
                        To:
                    </H4>
                    <div className="NewThreadSearchBox">
                        {users?.length > 0 && (
                            <Flexer
                                gutter="tiny"
                                justifyContent="flex-start"
                                wrap
                                className="NewThreadUsers"
                            >
                                {users.map((user, i) => (
                                    <Tag
                                        key={user.guid}
                                        onRemove={
                                            addChatLoading
                                                ? null
                                                : () => {
                                                      const newUsers = [
                                                          ...users,
                                                      ]
                                                      newUsers.splice(i, 1)
                                                      setUsers(newUsers)
                                                  }
                                        }
                                    >
                                        {user.name}
                                    </Tag>
                                ))}
                            </Flexer>
                        )}
                        <Textfield
                            ref={refInput}
                            name="new-thread-search-user"
                            size="large"
                            placeholder={`${t('global.search-user')}..`}
                            value={userQuery}
                            onChange={(e) => setUserQuery(e.target.value)}
                            borderStyle="none"
                            autoFocus
                            ElementAfter={
                                users?.length > 0 && (
                                    <IconButton
                                        size="large"
                                        variant="primary"
                                        radiusStyle="rounded"
                                        onClick={onSubmit}
                                        tooltip={t('chat.write-message')}
                                        placement="top"
                                        className="NewThreadSubmit"
                                        loading={addChatLoading}
                                    >
                                        <EditSquareIcon />
                                    </IconButton>
                                )
                            }
                        />
                    </div>
                </Flexer>
            </Popover>
        </Wrapper>
    )
}

const GET_USERS = gql`
    query NewThreadUsers($q: String!) {
        users(limit: 20, q: $q) {
            edges {
                guid
                name
                icon
            }
        }
    }
`

const ADD_CHAT = gql`
    mutation AddChat($input: addChatInput!) {
        addChat(input: $input) {
            chat {
               ${chatFragment}
            }
        }
    }
`

export default NewThreadForm
