import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'
import styled from 'styled-components'

import ChatContainer from 'js/chat/components/ChatContainer'
import getChatContainer from 'js/chat/helpers/getChatContainer'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import { iconViewFragment } from 'js/lib/fragments/icon'

import collabImage from 'images/collaboration.png'

import Messages from './Messages'
import SendMessage from './SendMessage'
import Welcome from './Welcome'

const Wrapper = styled.div`
    position: relative;
    display: flex;
    flex-direction: column;
    max-height: 100%;
    max-width: ${(p) => p.theme.containerWidth.small};

    .ThreadHeader {
        display: flex;
        flex-shrink: 0;
        padding: ${(p) =>
            `${p.theme.padding.vertical.small} ${p.theme.padding.horizontal.small} 8px`};
        background-color: white;
        z-index: 3; // Position on top of <MessageDivider />
    }

    .ThreadHeaderButton {
        display: flex;
        border-radius: ${(p) => p.theme.radius.normal};
        padding: 4px 12px 4px 4px;
        margin: -4px;
        overflow: hidden; // Needed for ellipsis

        &:hover {
            background-color: ${(p) => p.theme.color.hover};
        }

        &:active {
            background-color: ${(p) => p.theme.color.active};
        }
    }

    .ThreadHeaderTitle {
        margin-top: 2px;
    }

    .ThreadSendMessage {
        order: 1;
    }
`

const MainThread = ({ guid, ...rest }) => {
    const { t } = useTranslation()

    const { loading, data } = useQuery(GET_CHAT, {
        variables: {
            chatGuid: guid,
        },
        skip: !guid,
    })

    if (loading) {
        return (
            <Wrapper {...rest}>
                <LoadingSpinner style={{ flexGrow: 1 }} />
            </Wrapper>
        )
    }

    if (!data) {
        return (
            <Wrapper {...rest}>
                <Welcome
                    title={t('chat.introduction-title')}
                    subtitle={t('chat.introduction-message')}
                    image={collabImage}
                    imageWidth="320px"
                />
            </Wrapper>
        )
    }

    const { chat } = data

    const container = getChatContainer(chat.container)

    if (!container) {
        console.error(t('error.chat-container-not-found'), chat)
        return null
    }

    return (
        <Wrapper {...rest}>
            <div className="ThreadHeader">
                <ChatContainer
                    chat={chat}
                    buttonClass="ThreadHeaderButton"
                    titleClass="ThreadHeaderTitle"
                />
            </div>
            {/* Moved down with order: 1 */}
            <SendMessage
                chatGuid={guid}
                size="large"
                group={chat.container.group}
                className="ThreadSendMessage"
            />
            <Messages chatGuid={guid} size="large" />
        </Wrapper>
    )
}

const GET_CHAT = gql`
    query Chat($chatGuid: String) {
        chat(chatGuid: $chatGuid) {
            guid
            type
            container {
                users {
                    guid
                    icon
                    name
                    url
                }
                group {
                    guid
                    isClosed
                    name
                    ${iconViewFragment}
                    url
                }
            }
        }
    }
`

export default MainThread
