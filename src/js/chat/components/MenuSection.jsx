import React from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

import IconButton from 'js/components/IconButton/IconButton'
import { useLocalStorage } from 'js/lib/helpers'

import TriangleDownIcon from 'icons/triangle-down.svg'

const Wrapper = styled.div`
    ${(p) =>
        !p.$isVisible &&
        css`
            margin-bottom: 0 !important; // Override margin-bottom from ChatList
        `};

    .MenuSectionHeader {
        display: flex;
        padding: 4px 0 4px 10px;
    }

    .MenuSectionToggleButton[aria-expanded='false'] svg {
        transform: rotate(-90deg);
    }

    .MenuSectionHeaderTitle {
        padding-left: 10px;
        font-size: ${(p) => p.theme.font.size.normal};
        line-height: ${(p) => p.theme.font.lineHeight.normal};
        font-weight: ${(p) => p.theme.font.weight.bold};
    }
`

const MenuSection = ({ name, title, children }) => {
    const [isVisible, setIsVisible] = useLocalStorage(
        `chat-menu-section-${name}-visible`,
        true,
    )

    return (
        <Wrapper $isVisible={isVisible}>
            <div className="MenuSectionHeader">
                <IconButton
                    size="small"
                    variant="secondary"
                    radiusStyle="rounded"
                    onClick={() => setIsVisible(!isVisible)}
                    aria-expanded={isVisible}
                    className="MenuSectionToggleButton"
                >
                    <TriangleDownIcon />
                </IconButton>
                <h3 className="MenuSectionHeaderTitle">{title}</h3>
            </div>
            {isVisible && children}
        </Wrapper>
    )
}

MenuSection.propTypes = {
    name: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
}

export default MenuSection
