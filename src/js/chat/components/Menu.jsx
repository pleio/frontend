import React, { useContext } from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import IconButton from 'js/components/IconButton/IconButton'
import { globalStateContext } from 'js/lib/withGlobalState'

import EditSquareSmallIcon from 'icons/edit-square.svg'

import ChatList from './ChatList'
import MenuTitleButton from './MenuTitleButton'

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;

    .MenuHeader {
        display: flex;
        align-items: center;
        padding: ${(p) =>
            `${p.theme.padding.vertical.small} ${p.theme.padding.horizontal.small} 0 ${p.theme.padding.vertical.small}`};
    }

    .MenuHeaderTitleButton {
        padding: 2px 8px;
        margin-left: -8px;
    }

    .MenuHeaderNewThread {
        margin-left: auto;
    }
`

const Menu = ({ ...rest }) => {
    const { t } = useTranslation()
    const { setGlobalState } = useContext(globalStateContext)

    return (
        <Wrapper {...rest}>
            <div className="MenuHeader">
                <MenuTitleButton className="MenuHeaderTitleButton" />
                <IconButton
                    size="normal"
                    variant="primary"
                    radiusStyle="rounded"
                    onClick={() => {
                        setGlobalState((newState) => {
                            newState.chatNewThread = true
                        })
                    }}
                    tooltip={t('chat.new-message')}
                    placement="top"
                    className="MenuHeaderNewThread"
                >
                    <EditSquareSmallIcon />
                </IconButton>
            </div>
            <ChatList />
        </Wrapper>
    )
}

export default Menu
