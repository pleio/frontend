import React from 'react'
import { gql, useQuery } from '@apollo/client'
import styled from 'styled-components'

import Messages from './Messages'
import SendMessage from './SendMessage'

const Wrapper = styled.div`
    position: relative;
    display: flex;
    flex-direction: column;
    max-height: 100%;

    .SubthreadSendMessage {
        order: 1;
    }
`

const Subthread = ({ guid, size, group, ...rest }) => {
    const { data } = useQuery(GET_MESSAGE_GROUP, {
        variables: {
            chatMessageGuid: guid,
        },
        skip: !guid || !!group,
    })

    if (!guid) return null

    return (
        <Wrapper {...rest}>
            <SendMessage
                chatMessageGuid={guid}
                group={group || data?.chat?.container?.group}
                className="SubthreadSendMessage"
            />
            <Messages chatMessageGuid={guid} size={size}></Messages>
        </Wrapper>
    )
}

const GET_MESSAGE_GROUP = gql`
    query Subthread($chatMessageGuid: String) {
        chat(chatGuid: null, chatMessageGuid: $chatMessageGuid) {
            guid
            container {
                group {
                    guid
                    isClosed
                }
            }
        }
    }
`

export default Subthread
