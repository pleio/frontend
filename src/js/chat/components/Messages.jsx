import React, { Fragment, useContext, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'
import { isSameDay, isToday, isYesterday, parseISO } from 'date-fns'
import { showFullDate } from 'helpers/date/showDate'
import styled, { css } from 'styled-components'

import messageFragment from 'js/chat/fragments/message'
import FetchMore from 'js/components/FetchMore'
import IconButton from 'js/components/IconButton/IconButton'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Sticky from 'js/components/Sticky/Sticky'
import StickyProvider from 'js/components/Sticky/StickyProvider'
import globalStateContext from 'js/lib/withGlobalState/globalStateContext'

import siteImage from 'images/site.png'

import CrossIcon from 'icons/cross-large.svg'

import Message from '../Message/Message'

import MessageDivider from './MessageDivider'
import Welcome from './Welcome'

const Wrapper = styled(({ id, ...props }) => (
    <StickyProvider id={id} {...props} />
))`
    width: 100%;
    overflow-y: auto;
    display: flex;
    flex-direction: column-reverse; // Start scrolled down if overflowing
    padding: 0 0 8px 0;

    ${(p) =>
        p.$noMessages &&
        css`
            flex-grow: 1;
            margin: auto 0;
        `}

    .MessagesList {
        margin-bottom: auto; // Align to the top (visible if there are few messages)
        display: flex;
        flex-direction: column-reverse; // Newest message will be focused first

        > li:not([aria-hidden]) {
            margin: 0 8px;
        }
    }

    .ThreadNoMessages {
        display: flex;
        flex-direction: column;
        align-items: center;
        padding: ${(p) =>
            `8px ${p.theme.padding.horizontal.small} ${p.theme.padding.vertical.small}`};

        svg {
            margin-bottom: 4px;
            color: ${(p) => p.theme.color.icon.grey};
        }
    }

    /* Subthread */
    .SubthreadHeader {
        display: flex;
        margin: 8px 8px 0;
    }

    .SubthreadHeaderMessage {
        flex-grow: 1;

        .MessageActions {
            right: 40px;
        }
    }

    .SubthreadHeaderClose {
        position: absolute;
        top: 8px;
        right: 8px;
        z-index: 2;
        background-color: white;
        border-radius: ${(p) => p.theme.radius.normal};
    }
`

const Messages = ({ chatGuid, chatMessageGuid, size, ...rest }) => {
    const {
        globalState: { subthreadGuid },
        setGlobalState,
    } = useContext(globalStateContext)

    const [queryLimit, setQueryLimit] = useState(10)
    const { loading, data, fetchMore, subscribeToMore } = useQuery(
        GET_MESSAGES,
        {
            variables: {
                chatGuid,
                chatMessageGuid,
                offset: 0,
                limit: queryLimit,
            },
            skip: !chatGuid && !chatMessageGuid,
        },
    )

    useEffect(() => {
        const unsubscribe = subscribeToMore({
            document: ON_CHAT_UPDATE,
            variables: {
                chatGuid,
                chatMessageGuid,
            },
            updateQuery: (prev, { subscriptionData }) => {
                if (!subscriptionData.data) return prev

                const { guid, action, message } =
                    subscriptionData.data.onChatUpdate
                let newTotal = prev.chat?.messages?.total || 0
                const edges = [...(prev.chat?.messages?.edges || [])]

                if (action === 'add') {
                    newTotal = newTotal + 1
                    edges.unshift(message)
                } else if (action === 'delete') {
                    newTotal = newTotal - 1
                    const index = edges.findIndex((el) => el.guid === guid)
                    edges.splice(index, 1)
                    if (guid === subthreadGuid) {
                        setGlobalState((newState) => {
                            newState.subthreadGuid = null
                        })
                    }
                } else if (action === 'edit') {
                    const index = edges.findIndex((el) => el.guid === guid)
                    edges.splice(index, 1, message)
                }

                return {
                    ...prev,
                    chat: {
                        ...prev.chat,
                        messages: {
                            ...prev.chat.messages,
                            total: newTotal,
                            edges,
                        },
                    },
                }
            },
        })
        return () => unsubscribe()
    }, [
        subscribeToMore,
        chatGuid,
        chatMessageGuid,
        subthreadGuid,
        setGlobalState,
    ])

    const { t } = useTranslation()

    const containerMessage = data?.chat?.container?.message
    const messages = data?.chat?.messages
    const messagesTotal = messages?.total
    const username = data?.viewer?.user?.name

    const noMessages =
        !loading &&
        !containerMessage &&
        (!messages || messages?.edges?.length === 0)

    const scrollLockId = 'chatMessages'

    return (
        <Wrapper
            id={scrollLockId}
            reverseOrder
            $noMessages={noMessages}
            {...rest}
        >
            {loading ? (
                <LoadingSpinner style={{ flexGrow: 1 }} />
            ) : noMessages ? (
                <>
                    {chatGuid && (
                        <Welcome
                            title={t('chat.no-messages-title')}
                            subtitle={
                                data?.chat?.type === 'group'
                                    ? t('chat.no-messages-group-helper')
                                    : t('chat.no-messages-user-helper')
                            }
                            image={siteImage}
                            imageWidth="160px"
                            size={size}
                        />
                    )}
                </>
            ) : (
                <FetchMore
                    as="ul"
                    edges={messages.edges}
                    getMoreResults={(data) => data.chat.messages.edges}
                    fetchMore={fetchMore}
                    fetchCount={10}
                    setLimit={setQueryLimit}
                    maxLimit={messagesTotal}
                    resultsMessage={t('global.result', {
                        count: messagesTotal,
                    })}
                    fetchMoreButtonHeight={size === 'large' ? 48 : undefined}
                    className="MessagesList"
                >
                    {messages.edges.map((message, i) => {
                        const { timeCreated } = message

                        let isNewDay = false
                        // Don't show day dividers in subthread
                        if (chatGuid) {
                            isNewDay =
                                i === messages.edges.length - 1 ||
                                !isSameDay(
                                    parseISO(messages.edges[i + 1].timeCreated),
                                    parseISO(timeCreated),
                                )
                        }

                        return (
                            <Fragment
                                key={`${chatGuid || chatMessageGuid}-${
                                    message.guid
                                }`}
                            >
                                <li>
                                    <Message
                                        entity={message}
                                        displayDate={
                                            chatMessageGuid
                                                ? 'timeSince'
                                                : 'time'
                                        }
                                        canReply={!!chatGuid}
                                        size={size}
                                        username={username}
                                        scrollLockId={scrollLockId}
                                    />
                                </li>
                                {isNewDay && (
                                    <li aria-hidden>
                                        <MessageDivider as={Sticky} size={size}>
                                            {isToday(timeCreated) ? (
                                                t('date.today')
                                            ) : isYesterday(timeCreated) ? (
                                                t('date.yesterday')
                                            ) : (
                                                <>{showFullDate(timeCreated)}</>
                                            )}
                                        </MessageDivider>
                                    </li>
                                )}
                            </Fragment>
                        )
                    })}
                </FetchMore>
            )}

            {/* Subthread */}
            {containerMessage && (
                <>
                    {messagesTotal > 0 && (
                        <MessageDivider as={Sticky}>
                            {t('comments.count-replies', {
                                count: messagesTotal,
                            })}
                        </MessageDivider>
                    )}
                    <div className="SubthreadHeader">
                        <Message
                            className="SubthreadHeaderMessage"
                            entity={containerMessage}
                            displayDate="timeSince"
                            size={size}
                            username={username}
                        />
                        <IconButton
                            className="SubthreadHeaderClose"
                            radiusStyle="rounded"
                            variant="secondary"
                            size="large"
                            tooltip={t('chat.close-thread')}
                            onClick={() => {
                                setGlobalState((newState) => {
                                    newState.subthreadGuid = null
                                })
                            }}
                        >
                            <CrossIcon />
                        </IconButton>
                    </div>
                </>
            )}
        </Wrapper>
    )
}

const GET_MESSAGES = gql`
    query Chat($chatGuid: String, $chatMessageGuid: String, $offset: Int, $limit: Int) {
        viewer {
            guid
            user {
                guid
                name
            }
        }
        chat(chatGuid: $chatGuid, chatMessageGuid: $chatMessageGuid) {
            guid
            type
            messages(offset: $offset, limit: $limit) {
                total
                edges {
                    ${messageFragment}
                }
            }
            container {
                message {
                    ${messageFragment}
                }
            }
        }
    }
`

const ON_CHAT_UPDATE = gql`
    subscription onChatUpdate($chatGuid: String, $chatMessageGuid: String) {
        onChatUpdate(chatGuid: $chatGuid, chatMessageGuid: $chatMessageGuid) {
            guid
            action
            message {
                ${messageFragment}
            }
        }
    }
`

export default Messages
