import React, { useContext } from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import styled from 'styled-components'

import NewThreadForm from 'js/chat/components/NewThreadForm'
import { H3 } from 'js/components/Heading'
import IconButton from 'js/components/IconButton/IconButton'
import globalStateContext from 'js/lib/withGlobalState/globalStateContext'

import CrossIcon from 'icons/cross-large.svg'

const Wrapper = styled.div`
    position: relative;
    display: flex;
    flex-direction: column;

    .ChatNewThreadHeader {
        padding: ${(p) =>
            `${p.theme.padding.vertical.small} ${p.theme.padding.horizontal.small} 8px`};
    }

    .ChatNewThreadClose {
        position: absolute;
        top: 8px;
        right: 8px;
        z-index: 2;
    }
`

const NewThread = ({ ...rest }) => {
    const { setGlobalState } = useContext(globalStateContext)

    const { t } = useTranslation()
    const navigate = useNavigate()

    return (
        <Wrapper {...rest}>
            <div className="ChatNewThreadHeader">
                <H3 as="h2">{t('chat.new-message')}</H3>
                <IconButton
                    className="ChatNewThreadClose"
                    radiusStyle="rounded"
                    variant="secondary"
                    size="large"
                    tooltip={t('chat.close-thread')}
                    onClick={() => {
                        setGlobalState((newState) => {
                            newState.chatNewThread = false
                        })
                    }}
                >
                    <CrossIcon />
                </IconButton>
            </div>
            <NewThreadForm
                onComplete={(chat) => {
                    setGlobalState((newState) => {
                        newState.chatNewThread = false
                    })
                    navigate(`/chat/${chat.guid}`)
                }}
            />
        </Wrapper>
    )
}

export default NewThread
