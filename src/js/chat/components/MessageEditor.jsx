import React from 'react'
import { useTranslation } from 'react-i18next'

import FormItem from 'js/components/Form/FormItem'

const MessageEditor = ({ ...props }) => {
    const { t } = useTranslation()

    return (
        <FormItem
            type="rich"
            placeholder={t('chat.send-message-placeholder')}
            required
            // see https://gitlab.com/pleio/frontend/-/merge_requests/2411#note_1830631522 for more info on why delayMentionSugestions is set
            delayMentionSuggestions={750}
            options={{
                textStyle: true,
                textLink: true,
                insertMention: true,
                insertMedia: true,
                disableAlignImage: true,
            }}
            autofocus
            disableNewParagraph
            showPaddingBottom
            hideKeyboardShortcuts
            {...props}
        />
    )
}

export default MessageEditor
