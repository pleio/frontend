import React from 'react'
import { Trans, useTranslation } from 'react-i18next'
import { gql, useMutation, useQuery } from '@apollo/client'
import styled from 'styled-components'

import StatusIndicator from 'js/components/Avatar/StatusIndicator'
import DropdownButton from 'js/components/DropdownButton/DropdownButton'
import { H3, H4 } from 'js/components/Heading'

const Wrapper = styled.button`
    position: relative;
    display: flex;
    align-items: center;
    border-radius: ${(p) => p.theme.radius.normal};

    &:hover {
        background-color: ${(p) => p.theme.color.hover};
    }

    &:active {
        background-color: ${(p) => p.theme.color.active};
    }

    .MenuHeaderStatus {
        display: inline-block;
        vertical-align: text-top;
        margin-left: 4px;
    }

    svg {
        margin-left: 4px !important;
    }
`

const MenuTitleButton = ({ size, ...rest }) => {
    const { t } = useTranslation()
    const { data, loading } = useQuery(GET_USER_STATUS)

    const [toggleIsChatOnlineOverride, { loading: toggleLoading }] =
        useMutation(TOGGLE_IS_CHAT_ONLINE_OVERRIDE, {
            refetchQueries: [{ query: GET_USER_STATUS }],
        })

    const TitleElement = size === 'small' ? H4 : H3

    const isLoading = loading || toggleLoading

    return (
        <DropdownButton
            options={[
                {
                    name: (
                        <span>
                            <Trans i18nKey="chat.set-status">
                                Set yourself as{' '}
                                <strong>
                                    {{
                                        status: data?.viewer?.user
                                            ?.isChatOnlineOverride
                                            ? t('chat.online')
                                            : t('chat.away'),
                                    }}
                                </strong>
                            </Trans>
                        </span>
                    ),
                    onClick: toggleIsChatOnlineOverride,
                },
            ]}
            trigger="click"
            placement="bottom-start"
            showArrow={!isLoading}
        >
            <Wrapper type="button" disabled={isLoading} {...rest}>
                <TitleElement as={size === 'small' ? 'h2' : 'h1'}>
                    {t('chat.title')}
                    {!isLoading && (
                        <StatusIndicator
                            $status={
                                data?.viewer?.user?.isChatOnlineOverride
                                    ? 'offline'
                                    : 'online'
                            }
                            className="MenuHeaderStatus"
                        />
                    )}
                </TitleElement>
            </Wrapper>
        </DropdownButton>
    )
}

export default MenuTitleButton

const TOGGLE_IS_CHAT_ONLINE_OVERRIDE = gql`
    mutation ToggleIsChatOnlineOverride {
        toggleIsChatOnlineOverride {
            isChatOnlineOverride
        }
    }
`

const GET_USER_STATUS = gql`
    query GetChatStatus {
        viewer {
            guid
            user {
                guid
                isChatOnlineOverride
            }
        }
    }
`
