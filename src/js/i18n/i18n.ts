import { initReactI18next } from 'react-i18next'
import { gql } from '@apollo/client'
import i18n, { LanguageDetectorAsyncModule } from 'i18next'

import client from 'js/lib/client'
import { setLocale } from 'js/lib/helpers/date/general'

import { messages } from './utils'

const GET_LANGUAGE = gql`
    query SetupI18n {
        site {
            guid
            language
        }
        viewer {
            guid
            user {
                guid
                language
            }
        }
    }
`

const getLanguage = async () => {
    const { data } = await client.query({
        query: GET_LANGUAGE,
    })
    if (!data) return 'nl'
    const { viewer, site } = data

    const language = viewer?.user?.language || site?.language || 'nl'

    setLocale(language)

    return language
}

const languageDetector: LanguageDetectorAsyncModule = {
    type: 'languageDetector',
    async: true,
    init: function () {
        /* use services and options */
    },
    detect: function (callback) {
        getLanguage().then(callback)
    },
    cacheUserLanguage: function () {
        /* cache language */
    },
}

i18n.use(languageDetector)
    .use(initReactI18next)
    .init({
        fallbackLng: 'nl',
        resources: messages,
        defaultNS: 'all',
        react: {
            useSuspense: false, // TODO: check if this is still valid
        },
        // debug: true,
    })

i18n.on('languageChanged', (lang) => {
    const html = document.querySelector('html')
    if (html) {
        html.setAttribute('lang', lang)
    }
    setLocale(lang)
})

export default i18n
