import de from 'js/i18n/messages/de.json'
import en from 'js/i18n/messages/en.json'
import fr from 'js/i18n/messages/fr.json'
import nl from 'js/i18n/messages/nl.json'

export const messages = { nl, en, fr, de }
