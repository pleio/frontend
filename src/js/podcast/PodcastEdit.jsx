import React from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import getGroupUrl from 'helpers/getGroupUrl'

import NotFound from 'js/core/NotFound'

import PodcastForm from './components/PodcastForm'
import { podcastFragment } from './fragments/entity'
import getRefetchQueries from './helpers/getRefetchQueries'

const PodcastEdit = () => {
    const { t } = useTranslation()
    const navigate = useNavigate()
    const params = useParams()
    const { guid } = params

    const { loading, data } = useQuery(QUERY, {
        variables: {
            guid,
        },
    })

    if (loading) return null
    if (!data || !data.entity) return <NotFound />

    const refetchQueries = getRefetchQueries(true, Boolean(data.entity.group))

    const afterDelete = () => {
        navigate(`${getGroupUrl(params)}/podcasts`, {
            replace: true,
        })
    }

    const onMutateSuccess = (data) => {
        navigate(location?.state?.prevPathname || data.editPodcast.podcast.url)
    }

    return (
        <PodcastForm
            title={t('entity-podcast.edit-title')}
            deleteTitle={t('entity-podcast.delete-title')}
            entity={data.entity}
            refetchQueries={refetchQueries}
            mutationQuery={EDIT_MUTATION}
            afterDelete={afterDelete}
            onMutateSuccess={onMutateSuccess}
        />
    )
}

const QUERY = gql`
    query EditPodcast($guid: String!) {
        entity(guid: $guid) {
            guid
            ${podcastFragment}
        }
    }
`

const EDIT_MUTATION = gql`
    mutation editPodcast($input: editPodcastInput!) {
        editPodcast(input: $input) {
            podcast {
                guid
                ${podcastFragment}
            }
        }
    }
`

export { PodcastEdit }
