import React from 'react'
import { Route, Routes } from 'react-router-dom'

import NotFound from 'js/core/NotFound'

import Episode from './Episode'
import Item from './Item'
import List from './List'

const PodcastRoutes = () => (
    <Routes>
        <Route path="/" element={<List />} />
        <Route path="/view/:guid/:slug" element={<Item />} />
        <Route path="/episodes/view/:guid/:slug" element={<Episode />} />
        <Route path="*" element={<NotFound />} />
    </Routes>
)

export default PodcastRoutes
