import { entityEditGroupFragment } from 'js/components/EntityActions/fragments/group'
import { iconViewFragment } from 'js/lib/fragments/icon'
import { ownerFragment } from 'js/lib/fragments/owner'

const podcastFragment = `
    ... on Podcast {
        ${iconViewFragment}
        authors {
            name
        }
        title
        description
        abstract
        richDescription
        url
        accessId
        timePublished
        statusPublished
        scheduleArchiveEntity
        scheduleDeleteEntity
        isFeatured
        canEdit
        writeAccessId
        tags
        tagCategories {
            name
            values
        }
        ${ownerFragment}
        inputLanguage
        isTranslationEnabled
        ${entityEditGroupFragment}
    }
`

const episodeFragment = `
    ... on Episode {
        file {
            ... on File {
                guid
                download
                title
            }
        }
        podcast {
            guid
            url
        }
        title
        description
        abstract
        richDescription
        url
        accessId
        timePublished
        statusPublished
        scheduleArchiveEntity
        scheduleDeleteEntity
        isFeatured
        canEdit
        writeAccessId
        tags
        tagCategories {
            name
            values
        }
        ${ownerFragment}
        inputLanguage
        isTranslationEnabled
        ${entityEditGroupFragment}
    }
`

export { episodeFragment, podcastFragment }
