import { iconViewFragment } from 'js/lib/fragments/icon'
import { ownerFragment } from 'js/lib/fragments/owner'

const podcastListFragment = `
... on Podcast {
  guid
  localTitle
  title
  url
  localExcerpt
  excerpt
  subtype
  tagCategories {
    values
    name
  }
  tags

  statusPublished
  timePublished
  isPinned
  canEdit
  isTranslationEnabled

  isBookmarked
  canBookmark
  hasVoted
  votes

  group {
    guid
    url
    name
    status
    description
    richDescription
    description
    excerpt
    introduction
    isIntroductionPublic
    ${iconViewFragment}
    canEdit
    canChangeOwnership
    isClosed
    isHidden
    isMembershipOnRequest
    isFeatured
    isJoinButtonVisible
  }

  ${ownerFragment}

  episodes {
    edges {
      title
      guid
      file {
        ... on File {
          guid
          download
        }
      }
    }
  }
}
`

const episodeListFragment = `
... on Episode {
  guid
  url
  statusPublished
  timePublished
  subtype
  isPinned
  localTitle
  title
  canEdit
  excerpt
  localExcerpt
  tagCategories {
    values
    name
  }
  tags
  __typename
  file {
      ... on File {
          guid
          download
      }
  }
  podcast {
    guid
      title
      url
      authors {
          name
      }
      ${iconViewFragment}
      episodes {
          edges {
              url
              guid
          }
      }
  }
  group {
      guid
      ... on Group {
          url
          name
          ${iconViewFragment}
          membership
          isJoinButtonVisible
      }
    }
  }
`

export { episodeListFragment, podcastListFragment }
