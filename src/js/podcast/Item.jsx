import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import Document from 'js/components/Document'
import FetchMore from 'js/components/FetchMore'
import { Container } from 'js/components/Grid/Grid'
import { H4 } from 'js/components/Heading'
import ToggleTranslation from 'js/components/Item/components/ToggleTranslation'
import LikeAndBookmark from 'js/components/LikeAndBookmark'
import Section from 'js/components/Section/Section'
import Spacer from 'js/components/Spacer/Spacer'
import TiptapView from 'js/components/Tiptap/TiptapView'
import NotFound from 'js/core/NotFound'
import { iconViewFragment } from 'js/lib/fragments/icon'
import ThemeProvider from 'js/theme/ThemeProvider'

import PodcastEpisodes from './components/PodcastEpisodes'
import PodcastMenu from './components/PodcastMenu'
import PodcastLinks from './helpers/podcastRoutes'

const Item = () => {
    const [queryLimit, setQueryLimit] = React.useState(10)
    const { t } = useTranslation()
    const params = useParams()
    const { guid } = params

    const { loading, data, fetchMore } = useQuery(GET_PAGE_VIEW, {
        variables: {
            guid,
            offset: 0,
            limit: queryLimit,
        },
        fetchPolicy: 'cache-and-network',
    })

    const [isTranslated, setIsTranslated] = useState(null)
    useEffect(() => {
        if (!loading && data?.entity) {
            setIsTranslated(!!data.entity.localTitle)
        }
    }, [loading, data])

    if (loading) return null
    if (!data?.entity) return <NotFound />

    const onEdit = PodcastLinks.PodcastEdit(guid, params)

    const { viewer, entity } = data
    const {
        localTitle,
        title,
        localRichDescription,
        richDescription,
        group,
        statusPublished,
        isTranslationEnabled,
    } = entity

    const hasTranslations = !!localRichDescription && isTranslationEnabled

    const translatedTitle = isTranslated ? localTitle : title
    const translatedRichDescription = isTranslated
        ? localRichDescription
        : richDescription

    const episodes = entity.episodes?.edges
    const isPublished = statusPublished === 'published'

    return (
        <ThemeProvider bodyColor="#FFFFFF">
            <Document title={translatedTitle} containerTitle={group?.name} />

            <Section backgroundColor="white" grow>
                <Container size="normal">
                    <Spacer
                        spacing="normal"
                        style={{
                            position: 'relative',
                        }}
                    >
                        <PodcastMenu
                            icon={entity.icon}
                            title={translatedTitle}
                            entity={entity}
                            onEdit={onEdit}
                            canCreate={entity.canEdit}
                        />

                        <TiptapView content={translatedRichDescription} />

                        {hasTranslations && (
                            <ToggleTranslation
                                isTranslated={isTranslated}
                                setIsTranslated={setIsTranslated}
                                style={{ marginTop: '12px' }}
                            />
                        )}

                        {isPublished && viewer.loggedIn && (
                            <LikeAndBookmark entity={entity} />
                        )}

                        <H4 as="h2">
                            {t('entity-podcast.episodes-quantity', {
                                count: data.entity.episodes.total,
                            })}
                        </H4>
                    </Spacer>

                    <FetchMore
                        edges={episodes}
                        getMoreResults={(data) => data.entity.episodes.edges}
                        fetchMore={fetchMore}
                        fetchCount={5}
                        setLimit={setQueryLimit}
                        maxLimit={data.entity.episodes.total}
                        resultsMessage={t('widget-podcast.result', {
                            count: 5,
                        })}
                    >
                        <PodcastEpisodes episodes={episodes} />
                    </FetchMore>
                </Container>
            </Section>
        </ThemeProvider>
    )
}

const GET_PAGE_VIEW = gql`
    query PodcastItem($guid: String!, $offset: Int, $limit: Int) {
        viewer {
            guid
            loggedIn
        }
        entity(guid: $guid) {
            guid
            __typename
            ... on Podcast {
                subtype
                statusPublished
                canEdit
                canArchiveAndDelete
                localTitle
                title
                url
                isTranslationEnabled
                localDescription
                description
                localRichDescription
                richDescription
                timePublished
                tags
                isPinned
                authors {
                    name
                }
                showOwner
                owner {
                    guid
                    name
                    canEdit
                    canDelete
                    url
                }
                tagCategories {
                    name
                    values
                }
                accessId
                votes
                hasVoted
                isBookmarked
                isFollowing
                canBookmark
                publishRequest {
                    status
                }

                ${iconViewFragment}

                group {
                    guid
                    ... on Group {
                        url
                        name
                        ${iconViewFragment}
                        membership
                    }
                }

                episodes(offset: $offset, limit: $limit) {
                    total
                    edges {
                        guid
                        url
                        title
                        description
                        excerpt
                        richDescription
                        timeUpdated
                        timePublished
                    }
                }
            }
        }
    }
`

export default Item
