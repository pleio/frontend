import React from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import NotFound from 'js/core/NotFound'

import EpisodeForm from './components/EpisodeForm'
import { episodeFragment } from './fragments/entity'
import getRefetchQueries from './helpers/getRefetchQueries'

const EpisodeEdit = () => {
    const { t } = useTranslation()
    const navigate = useNavigate()
    const params = useParams()
    const { episodeGuid } = params

    const { loading, data } = useQuery(QUERY, {
        variables: {
            guid: episodeGuid,
        },
    })

    if (loading) return null
    if (!data || !data.entity) return <NotFound />

    const refetchQueries = getRefetchQueries(true, Boolean(data.entity.group))

    const afterDelete = () =>
        navigate(data.entity.podcast.url, {
            replace: true,
        })
    const onMutateSuccess = (data) => {
        navigate(location?.state?.prevPathname || data.editEpisode.episode.url)
    }

    return (
        <EpisodeForm
            title={t('entity-podcast.episode.edit-title')}
            deleteTitle={t('entity-podcast.episode.delete-title')}
            entity={data.entity}
            refetchQueries={refetchQueries}
            mutationQuery={EDIT_MUTATION}
            afterDelete={afterDelete}
            onMutateSuccess={onMutateSuccess}
        />
    )
}

const QUERY = gql`
    query EditEpisode($guid: String!) {
        entity(guid: $guid) {
            guid
            ${episodeFragment}
        }
    }
`

const EDIT_MUTATION = gql`
    mutation editEpisode($input: editEpisodeInput!) {
        editEpisode(input: $input) {
            episode {
                guid
                ${episodeFragment}
            }
        }
    }
`

export { EpisodeEdit }
