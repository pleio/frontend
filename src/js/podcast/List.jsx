import React from 'react'
import { useTranslation } from 'react-i18next'
import { useQuery } from '@apollo/client'

import { Container } from 'js/components/Grid/Grid'
import PageHeader from 'js/components/PageHeader/PageHeader'
import Section from 'js/components/Section/Section'
import { useWindowScrollPosition } from 'js/lib/helpers'
import FeedView from 'js/page/Widget/components/Feed/FeedView'

import PODCAST_SETTINGS from './GraphQL/query'
import PodcastLinks from './helpers/podcastRoutes'

// First load data, otherwise useWindowScrollPosition and FeedView won't work properly
const ListWrapper = () => {
    const { data, loading } = useQuery(PODCAST_SETTINGS)

    if (loading) return null
    return <List data={data} />
}

const List = ({ data }) => {
    const { viewer, site } = data

    const { t } = useTranslation()

    const settings = {
        showTagFilter: site?.pageTagFilters.showTagFilter,
        showTagCategories: site?.pageTagFilters.showTagCategories,
        sortingOptions: ['timePublished', 'lastAction'],
        typeFilter: ['podcast'],
        sortBy: 'lastAction',
        sortDirection: 'desc',
        itemView: 'list',
        itemCount: 10,
    }

    useWindowScrollPosition('podcasts')

    return (
        <>
            <PageHeader
                title={t('entity-podcast.title-list')}
                canCreate={viewer && viewer.canWriteToContainer}
                createLink={PodcastLinks.PodcastAdd()}
                createLabel={t('entity-podcast.create-title')}
            />

            <Section>
                <Container size="tiny">
                    <FeedView guid="podcasts" settings={settings} />
                </Container>
            </Section>
        </>
    )
}

export default ListWrapper
