import styled from 'styled-components'

export const LargePlayerDescription = styled.div`
    flex: 1;
`

export const LargePlayerControls = styled.div`
    flex: 1;
`

export const PlayerContainer = styled.div`
    justify-content: center;
    display: flex;
`
