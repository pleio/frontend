import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import Avatar from 'js/components/Avatar/Avatar'
import { H3 } from 'js/components/Heading'
import Text from 'js/components/Text/Text'

const PodcastPlayerHeader = ({ podcast, size }) => {
    const { title, owner, authors, icon, url, showOwner } = podcast

    const iconSource = icon?.download
    const renderedAuthorName = authors?.map((name) => name.name).join(', ')

    return (
        <HeaderContainer>
            {!!iconSource && (
                <Avatar
                    size={size === 'sm' ? 'large' : 'xlarge'}
                    radiusStyle="square"
                    disabled
                    image={iconSource}
                />
            )}

            <div className="WrapperMenuContent">
                <H3>
                    <Link to={url}>{title}</Link>
                </H3>

                {showOwner && (
                    <Text size="tiny" variant="grey">
                        {renderedAuthorName || (
                            <Link to={owner.url}>{owner.name}</Link>
                        )}
                    </Text>
                )}
            </div>
        </HeaderContainer>
    )
}

const HeaderContainer = styled.div`
    display: flex;
    gap: 10px;

    .WrapperMenuContent {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: flex-start;
        width: 100%;
    }

    .PodcastTitle {
        color: ${(p) => p.theme.color.secondary.main};
    }
`

export default PodcastPlayerHeader
