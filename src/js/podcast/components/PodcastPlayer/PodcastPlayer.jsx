import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link as RouterLink } from 'react-router-dom'
import { useMeasure } from 'react-use'

import AudioPlayer from 'js/components/AudioPlayer'
import Flexer from 'js/components/Flexer/Flexer'
import { H4 } from 'js/components/Heading'
import Spacer from 'js/components/Spacer/Spacer'
import Text from 'js/components/Text/Text'
import TiptapView from 'js/components/Tiptap/TiptapView'

import * as Styled from './PodcastPlayer.styles'
import PodcastPlayerHeader from './PodcastPlayerHeader'

const PodcastPlayer = ({ podcast }) => {
    const [ref, { width }] = useMeasure()

    const [currentEpisodeIndex, setCurrentEpisodeIndex] = useState(0)
    const [autoPlay, setAutoPlay] = useState(false)
    const { t } = useTranslation()

    const episodes = podcast.episodes.edges

    /**
     * Handle the next episode within the podcast
     */
    const handleNext = () => {
        // Update the current episode with the next one
        // as long it's not the last episode
        if (currentEpisodeIndex < episodes.length - 1) {
            setCurrentEpisodeIndex((prevIndex) => prevIndex + 1)
            setAutoPlay(true)
        }
    }

    /**
     * Handle the previous episode within the podcast
     */
    const handlePrevious = () => {
        // Update the current episode with the previous one
        // as long it's not the first episode
        if (currentEpisodeIndex > 0) {
            setCurrentEpisodeIndex((prevIndex) => prevIndex - 1)
            setAutoPlay(true)
        }
    }

    const currentEpisode = episodes[currentEpisodeIndex]

    const renderPlayer = (size) => {
        if (!currentEpisode)
            return (
                <Text>
                    {t('entity-podcast.episodes-quantity', {
                        count: 0,
                    })}
                </Text>
            )

        return (
            <Styled.PlayerContainer>
                <AudioPlayer
                    key={`audio-${currentEpisode.guid}`}
                    src={currentEpisode.file?.download || ''}
                    autoPlay={autoPlay}
                    onNext={handleNext}
                    onPrevious={handlePrevious}
                    canGoNext={currentEpisodeIndex < episodes.length - 1}
                    canGoPrevious={currentEpisodeIndex > 0}
                    size={size}
                />
            </Styled.PlayerContainer>
        )
    }

    const renderDescription = () => {
        if (!currentEpisode) return null
        return (
            <div>
                <H4>
                    <RouterLink to={currentEpisode.url}>
                        {currentEpisode.title}
                    </RouterLink>
                </H4>

                <TiptapView
                    content={currentEpisode.excerpt}
                    contentType={'html'}
                />
            </div>
        )
    }

    const getSize = (width) => {
        if (!width) {
            return null
        } else if (width < 250) {
            return 'xs'
        } else if (width < 300) {
            return 'sm'
        } else if (width < 600) {
            return 'md'
        }
        return 'lg'
    }

    const size = getSize(width)

    return (
        <div ref={ref}>
            {size !== 'lg' ? (
                <Spacer>
                    <PodcastPlayerHeader size={size} podcast={podcast} />
                    {renderPlayer(size)}
                    {renderDescription()}
                </Spacer>
            ) : (
                <Flexer divider="normal">
                    <Styled.LargePlayerDescription>
                        <Spacer>
                            <PodcastPlayerHeader
                                size={size}
                                podcast={podcast}
                            />

                            {renderDescription()}
                        </Spacer>
                    </Styled.LargePlayerDescription>

                    <Styled.LargePlayerControls>
                        {renderPlayer(size)}
                    </Styled.LargePlayerControls>
                </Flexer>
            )}
        </div>
    )
}

export default PodcastPlayer
