import React from 'react'
import { useTranslation } from 'react-i18next'
import { useMutation } from '@apollo/client'

import AddEditForm from 'js/components/EntityActions/AddEditForm'
import IconField from 'js/components/EntityActions/components/IconField'
import FormItem from 'js/components/Form/FormItem'

const PodcastForm = ({
    title,
    deleteTitle,
    entity,
    refetchQueries,
    mutationQuery,
    afterDelete,
    onMutateSuccess,
    canSaveAsDraft = false,
}) => {
    const { t } = useTranslation()
    const mutation = useMutation(mutationQuery)

    const mutationInputFilter = (input, { icon, authors }) => {
        // eslint-disable-next-line
        const { subtype, ...filteredData } = input // leave out subtype

        const authorsName = (authors || '')
            .split(',')
            .map((author) => ({ name: author.trim() }))
            .filter((authorsObject) => authorsObject.name !== '')

        const podcastData = {
            ...filteredData,
            authors: authorsName.length === 0 ? [{ name: null }] : authorsName,
            iconGuid: icon?.guid || null,
        }

        return podcastData
    }

    const renderCustomFields = ({ control }) => (
        <>
            <IconField
                iconProps={{
                    objectFit: 'cover',
                    style: {
                        width: '80px',
                        height: '80px',
                    },
                }}
            />

            <FormItem
                control={control}
                type="text"
                name="authors"
                title={t('form.author')}
                helper={t('entity-podcast.author-helper')}
                style={{ marginTop: '24px' }}
            />
        </>
    )

    return (
        <AddEditForm
            title={title}
            deleteTitle={deleteTitle}
            subtype="podcast"
            entity={entity}
            refetchQueries={refetchQueries}
            mutation={mutation}
            canSaveAsDraft={canSaveAsDraft}
            onMutateSuccess={onMutateSuccess}
            mutationInputFilter={mutationInputFilter}
            afterDelete={afterDelete}
            renderCustomFields={renderCustomFields}
            customDefaultValues={{
                icon: entity?.icon || null,
                authors:
                    entity?.authors?.map((author) => author.name).join(', ') ||
                    null,
            }}
        />
    )
}

export default PodcastForm
