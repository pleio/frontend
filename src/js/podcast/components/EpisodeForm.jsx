import React from 'react'
import { useTranslation } from 'react-i18next'
import { useMutation } from '@apollo/client'

import AddEditForm from 'js/components/EntityActions/AddEditForm'
import FormItem from 'js/components/Form/FormItem'
import { Col, Row } from 'js/components/Grid/Grid'

const EpisodeForm = ({
    title,
    deleteTitle,
    entity,
    podcastGuid,
    refetchQueries,
    mutationQuery,
    afterDelete,
    onMutateSuccess,
    canSaveAsDraft = false,
}) => {
    const { t } = useTranslation()
    const mutation = useMutation(mutationQuery)

    const mutationInputFilter = (input, values, dirtyFields) => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const { subtype, ...filteredData } = input
        const { podcastFile } = values
        const fileGuid = podcastFile?.guid

        return {
            // Only add podcastGuid if it's not falsy
            ...(podcastGuid && { podcastGuid }),
            fileGuid: dirtyFields.podcastFile ? podcastFile : fileGuid, // If the file hasn't changed, only submit the file guid
            ...filteredData,
        }
    }
    const renderCustomFields = ({ entity, control }) => (
        <Row>
            <Col mobileUp={1 / 2}>
                <FormItem
                    control={control}
                    type="audio"
                    name="podcastFile"
                    defaultValue={entity?.file}
                    title={t('form.file')}
                    required
                />
            </Col>
        </Row>
    )

    return (
        <AddEditForm
            title={title}
            deleteTitle={deleteTitle}
            subtype="podcast"
            entity={entity}
            refetchQueries={refetchQueries}
            mutation={mutation}
            canSaveAsDraft={canSaveAsDraft}
            onMutateSuccess={onMutateSuccess}
            mutationInputFilter={mutationInputFilter}
            afterDelete={afterDelete}
            renderCustomFields={renderCustomFields}
        />
    )
}

export default EpisodeForm
