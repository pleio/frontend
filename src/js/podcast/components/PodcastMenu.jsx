import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useParams } from 'react-router-dom'
import styled from 'styled-components'

import Avatar from 'js/components/Avatar/Avatar'
import { H1 } from 'js/components/Heading'
import ItemActions from 'js/components/Item/ItemActions/ItemActions'
import ItemStatusTag from 'js/components/Item/ItemStatusTag'
import Text from 'js/components/Text/Text'

import PodcastLinks from '../helpers/podcastRoutes'

const PodcastMenu = ({
    title,
    entity,
    icon,
    isEpisode,
    onEdit,
    canCreate = false,
}) => {
    const refetchQueries = ['ActivityList', 'Podcast', 'PodcastItem']
    const { owner, authors, showOwner } = entity
    const { t } = useTranslation()
    const params = useParams()

    const authorName = () => {
        const listNames = isEpisode ? entity?.podcast?.authors : authors

        return listNames?.map((name) => name.name).join(', ')
    }

    const renderedAuthorName = authorName()

    const onAfterDelete = isEpisode
        ? entity.podcast.url
        : PodcastLinks.Home(params)

    const createEpisodeLabel = t('entity-podcast.episode.create-title')
    const customItemOptions =
        !isEpisode && canCreate
            ? [
                  {
                      name: createEpisodeLabel,
                      label: createEpisodeLabel,
                      canCreate,
                      to: PodcastLinks.PodcastEpisodeAdd(params.guid, params),
                      state: { prevPathname: location.pathname },
                  },
              ]
            : []

    return (
        <WrapperMenu>
            {!!icon && (
                <Avatar
                    size="xxlarge"
                    radiusStyle="square"
                    disabled
                    image={icon?.download}
                />
            )}
            <div className="WrapperMenuContent">
                <H1>{title}</H1>
                {isEpisode ? (
                    <Text size="small">
                        <Link to={entity.podcast.url}>
                            {entity.podcast.title}
                        </Link>
                    </Text>
                ) : (
                    showOwner && (
                        <Text size="small" variant="grey">
                            {renderedAuthorName || (
                                <Link to={owner.url}>{owner.name}</Link>
                            )}
                        </Text>
                    )
                )}
                <ItemStatusTag entity={entity} />
            </div>

            <ItemActions
                showEditButton
                entity={entity}
                onEdit={onEdit}
                onAfterDelete={onAfterDelete}
                refetchQueries={refetchQueries}
                customOptions={[customItemOptions]}
                style={{ marginLeft: 'auto' }}
            />
        </WrapperMenu>
    )
}

const WrapperMenu = styled.div`
    display: flex;
    align-items: flex-start;
    gap: 12px;

    .WrapperMenuContent {
        flex-grow: 1;
        display: flex;
        align-self: stretch;
        flex-direction: column;
        justify-content: center;
        align-items: flex-start;
    }
`

export default PodcastMenu
