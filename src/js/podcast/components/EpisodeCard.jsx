/**
 * This template differs from most other cards in:
 * - likes/comments are not enabled for podcast episode
 * - it shows a link to the podcast below the episode title
 */
import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import { CardContent } from 'js/components/Card/Card'
import FeedItem from 'js/components/FeedItem/FeedItem'
import FeedItemActions from 'js/components/FeedItem/FeedItemActions'
import FeedItemMeta from 'js/components/FeedItem/FeedItemMeta'
import { H3 } from 'js/components/Heading'
import ToggleTranslation from 'js/components/Item/components/ToggleTranslation'
import ItemStatusTag from 'js/components/Item/ItemStatusTag'
import ItemTags from 'js/components/ItemTags'
import Text from 'js/components/Text/Text'
import TiptapView from 'js/components/Tiptap/TiptapView'
import Truncate from 'js/components/Truncate/Truncate'

import getRefetchQueries from '../helpers/getRefetchQueries'

const EpisodeCard = ({
    'data-feed': dataFeed,
    entity,
    canPin,
    hideGroup,
    hideSubtype,
    hideExcerpt,
    hideActions,
}) => {
    const { data } = useQuery(GET_SITE)

    const onEdit = `${entity.group ? entity.group.url : ''}/podcasts/edit/${
        entity.guid
    }`

    const {
        localTitle,
        title,
        url,
        localExcerpt,
        excerpt,
        tags,
        tagCategories,
        podcast,
        isTranslationEnabled,
    } = entity

    const hasTranslations = !!localTitle && isTranslationEnabled
    const [isTranslated, setIsTranslated] = useState(hasTranslations)
    const translatedTitle = isTranslated ? localTitle : title
    const translatedExcerpt = isTranslated ? localExcerpt || excerpt : excerpt

    const showExcerpt = !hideExcerpt && excerpt
    const subtype = !hideSubtype && entity.subtype
    const group = !hideGroup && entity.group

    return (
        <FeedItem data-feed={dataFeed}>
            <CardContent className="FeedItemContent">
                {title && (
                    <H3 className="FeedItemTitle">
                        <Link
                            to={url}
                            state={{ prevPathname: location.pathname }}
                            style={{ display: 'block' }}
                        >
                            <Truncate lines={showExcerpt ? 2 : 3}>
                                {translatedTitle}
                            </Truncate>
                        </Link>
                        <ItemStatusTag
                            entity={entity}
                            style={{
                                margin: '6px 0',
                            }}
                        />
                    </H3>
                )}

                <Text size="small">
                    <Link
                        to={podcast.url}
                        state={{ prevPathname: location.pathname }}
                        style={{ display: 'block' }}
                    >
                        {podcast.title}
                    </Link>
                </Text>

                <div className="FeedItemHeader">
                    <FeedItemMeta
                        entity={entity}
                        subtype={subtype}
                        group={group}
                    />
                    <FeedItemActions
                        entity={entity}
                        canPin={canPin}
                        onEdit={onEdit}
                        refetchQueries={getRefetchQueries(
                            false,
                            !!entity.group,
                        )}
                        hideActions={hideActions}
                    />
                </div>

                {showExcerpt && (
                    <div className="FeedItemExcerpt">
                        <TiptapView
                            content={translatedExcerpt}
                            contentType={'html'}
                        />
                    </div>
                )}

                {hasTranslations && (
                    <ToggleTranslation
                        isTranslated={isTranslated}
                        setIsTranslated={setIsTranslated}
                        style={{ marginTop: '4px' }}
                    />
                )}

                <ItemTags
                    showCustomTags={data?.site?.showCustomTagsInFeed}
                    showTags={data?.site?.showTagsInFeed}
                    customTags={tags}
                    tagCategories={tagCategories}
                    style={{ marginTop: '12px' }}
                />
            </CardContent>
        </FeedItem>
    )
}

const GET_SITE = gql`
    query EpisodeFeedItemContent {
        site {
            guid
            showTagsInFeed
            showCustomTagsInFeed
        }
    }
`

export default EpisodeCard
