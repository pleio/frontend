import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import DisplayDate from 'js/components/DisplayDate'
import Text from 'js/components/Text/Text'
import TiptapView from 'js/components/Tiptap/TiptapView'

const PodcastEpisodes = ({ episodes }) => (
    <WrapperListView>
        {episodes.map(({ guid, timePublished, url, title, excerpt }) => (
            <li key={guid}>
                <Text variant="grey" size="small">
                    <DisplayDate
                        date={timePublished}
                        type="timeSince"
                        placement="right"
                    />
                </Text>

                <Link to={url} className="EpisodeTitle">
                    {title}
                </Link>

                <TiptapView content={excerpt} contentType={'html'} />
            </li>
        ))}
    </WrapperListView>
)

const WrapperListView = styled.ul`
    display: flex;
    flex-direction: column;
    margin-bottom: 16px;
    gap: 24px;
    margin-top: 16px;

    .EpisodeTitle {
        color: ${(p) => p.theme.color.secondary.main};
        font-size: 16px;
        font-weight: 700;
    }
`

export default PodcastEpisodes
