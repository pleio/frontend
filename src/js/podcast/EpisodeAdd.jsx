import React from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate, useParams } from 'react-router-dom'
import { gql } from '@apollo/client'

import EpisodeForm from './components/EpisodeForm'
import getRefetchQueries from './helpers/getRefetchQueries'

const EpisodeAdd = () => {
    const { t } = useTranslation()
    const navigate = useNavigate()
    const { podcastGuid, groupGuid } = useParams()

    const refetchQueries = getRefetchQueries(true, Boolean(groupGuid))

    const onMutateSuccess = (data) => {
        navigate(data.addEpisode.episode.url)
    }

    return (
        <EpisodeForm
            title={t('entity-podcast.episode.create-title')}
            podcastGuid={podcastGuid}
            canSaveAsDraft
            refetchQueries={refetchQueries}
            mutationQuery={ADD_MUTATION}
            onMutateSuccess={onMutateSuccess}
        />
    )
}

const ADD_MUTATION = gql`
    mutation addEpisode($input: addEpisodeInput!) {
        addEpisode(input: $input) {
            episode {
                guid
                status
                ... on Episode {
                    url
                }
            }
        }
    }
`

export { EpisodeAdd }
