import { getGroupUrl } from 'js/lib/helpers'

const PodcastLinks = {
    Home: (groupParams) => `${getGroupUrl(groupParams)}/podcasts`,
    PodcastAdd: (groupParams) => `${getGroupUrl(groupParams)}/podcasts/add`,
    PodcastEdit: (guid, groupParams) =>
        `${getGroupUrl(groupParams)}/podcasts/edit/${guid}`,
    PodcastEpisodeAdd: (guid, groupParams) =>
        `${getGroupUrl(groupParams)}/podcasts/${guid}/episodes/add`,
    PodcastEpisodeEdit: (guid, groupParams) =>
        `${getGroupUrl(groupParams)}/podcasts/episodes/edit/${guid}`,
}

export default PodcastLinks
