/**
 * Load the correct query names, depending on if we need
 * to update the detail view and/or group lists.
 *
 * @param {boolean} isDetail
 * @param {boolean} inGroup
 */
export default (isDetail, inGroup) => {
    const refetchQueries = ['ActivityList', 'Podcast']

    if (isDetail) {
        refetchQueries.push('PodcastItem', 'EpisodeItem')
    }

    if (inGroup) {
        refetchQueries.push('GroupPodcasts')
    }

    return refetchQueries
}
