import { gql } from '@apollo/client'

const PODCAST_SETTINGS = gql`
    query Podcast {
        viewer {
            guid
            canWriteToContainer(subtype: "podcast")
        }
        site {
            guid
            pageTagFilters(contentType: "podcast") {
                showTagFilter
                showTagCategories
            }
        }
    }
`

export default PODCAST_SETTINGS
