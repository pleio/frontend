import React from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate, useParams } from 'react-router-dom'
import { gql } from '@apollo/client'

import PodcastForm from './components/PodcastForm'
import getRefetchQueries from './helpers/getRefetchQueries'

const PodcastAdd = () => {
    const { t } = useTranslation()
    const navigate = useNavigate()
    const { groupGuid } = useParams()

    const refetchQueries = getRefetchQueries(false, Boolean(groupGuid))

    const onMutateSuccess = (data) => {
        navigate(data.addPodcast.podcast.url)
    }

    return (
        <PodcastForm
            title={t('entity-podcast.create-title')}
            canSaveAsDraft
            refetchQueries={refetchQueries}
            mutationQuery={ADD_MUTATION}
            onMutateSuccess={onMutateSuccess}
        />
    )
}

const ADD_MUTATION = gql`
    mutation addPodcast($input: addPodcastInput!) {
        addPodcast(input: $input) {
            podcast {
                guid
                status
                ... on Podcast {
                    url
                }
            }
        }
    }
`

export { PodcastAdd }
