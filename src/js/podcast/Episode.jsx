import React, { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import AudioPlayer from 'js/components/AudioPlayer'
import Document from 'js/components/Document'
import { Container } from 'js/components/Grid/Grid'
import ToggleTranslation from 'js/components/Item/components/ToggleTranslation'
import ItemTags from 'js/components/ItemTags'
import Section from 'js/components/Section/Section'
import Spacer from 'js/components/Spacer/Spacer'
import TiptapView from 'js/components/Tiptap/TiptapView'
import NotFound from 'js/core/NotFound'
import { iconViewFragment } from 'js/lib/fragments/icon'
import ThemeProvider from 'js/theme/ThemeProvider'

import PodcastMenu from './components/PodcastMenu'
import PodcastLinks from './helpers/podcastRoutes'

const getEpisodeIndex = (guid, episodes) =>
    episodes.findIndex((episode) => episode.guid === guid)

const Episode = () => {
    const { guid } = useParams()
    const navigate = useNavigate()

    const { loading, data } = useQuery(GET_EPISODE, {
        variables: {
            guid,
        },
    })

    const [isTranslated, setIsTranslated] = useState(null)
    useEffect(() => {
        if (!loading && data?.entity) {
            setIsTranslated(!!data.entity.localTitle)
        }
    }, [loading, data])

    if (loading) return null

    if (!data?.entity?.file) return <NotFound />

    const { site, entity } = data

    const {
        localTitle,
        title,
        localRichDescription,
        richDescription,
        group,
        tags,
        tagCategories,
        isTranslationEnabled,
    } = entity

    const hasTranslations = !!localTitle && isTranslationEnabled

    const translatedTitle = isTranslated ? localTitle : title
    const translatedRichDescription = isTranslated
        ? localRichDescription
        : richDescription

    const episodes = entity.podcast.episodes.edges
    const currentEpisodeIndex = getEpisodeIndex(guid, episodes)
    const canGoNext = currentEpisodeIndex < episodes.length - 1
    const canGoPrevious = currentEpisodeIndex > 0

    /**
     * Handle the next episode within the podcast
     */
    const handleNext = () => {
        // Navigate to the next episode as long the current not the last episode
        if (canGoNext) {
            const nextEpisode = episodes[currentEpisodeIndex + 1]

            navigate(nextEpisode.url)
        }
    }

    /**
     * Handle the previous episode within the podcast
     */
    const handlePrevious = () => {
        // Navigate to the previous episode as long the current is not the first episode
        if (canGoPrevious) {
            const prevEpisode = episodes[currentEpisodeIndex - 1]
            navigate(prevEpisode.url)
        }
    }

    const onEdit = PodcastLinks.PodcastEpisodeEdit(guid)

    return (
        <ThemeProvider bodyColor="#FFFFFF">
            <Document title={translatedTitle} containerTitle={group?.name} />

            <Section backgroundColor="white" grow>
                <Container size="normal">
                    <Spacer>
                        <PodcastMenu
                            icon={entity.podcast?.icon}
                            title={translatedTitle}
                            entity={entity}
                            onEdit={onEdit}
                            canCreate={entity.canEdit}
                            isEpisode
                        />

                        <TiptapView content={translatedRichDescription} />

                        {hasTranslations && (
                            <ToggleTranslation
                                isTranslated={isTranslated}
                                setIsTranslated={setIsTranslated}
                                style={{ marginTop: '12px' }}
                            />
                        )}

                        <AudioPlayer
                            key={`audio-${guid}`}
                            src={entity.file?.download}
                            onNext={handleNext}
                            onPrevious={handlePrevious}
                            canGoNext={canGoNext}
                            canGoPrevious={canGoPrevious}
                        />

                        <ItemTags
                            showCustomTags={site?.showCustomTagsInDetail}
                            showTags={site?.showTagsInDetail}
                            customTags={tags}
                            tagCategories={tagCategories}
                        />
                    </Spacer>
                </Container>
            </Section>
        </ThemeProvider>
    )
}

const GET_EPISODE = gql`
    fragment EpisodeParts on Episode {
        guid
        subtype
        isPinned
        localTitle
        title
        canEdit
        canArchiveAndDelete
        localRichDescription
        richDescription
        inputLanguage
        isTranslationEnabled
        tagCategories {
            name
            values
        }
        tags
        __typename
        file {
            ... on File {
                guid
                download
            }
        }
        podcast {
            guid
            title
            url
            authors {
                name
            }
            ${iconViewFragment}
            episodes(orderBy: timePublished, orderDirection: asc) {
                edges {
                    title
                    url
                    guid
                }
            }
        }
        owner {
            guid
            name
            icon
            url
        }
        group {
            guid
            ... on Group {
                url
                name
                ${iconViewFragment}
                membership
            }
        }
    }

    query EpisodeItem($guid: String!) {
        site {
            guid
            showTagsInDetail
            showCustomTagsInDetail
        }
        entity(guid: $guid) {
            guid
            status
            ...EpisodeParts
        }
    }
`

export default Episode
