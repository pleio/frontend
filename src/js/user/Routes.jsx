import React from 'react'
import { Navigate, Route, Routes } from 'react-router-dom'

import NotFound from 'js/core/NotFound'
import RequireAuth from 'js/router/RequireAuth'

import ActivityContainer from './Activity/ActivityContainer'
import ActivityContainerPage from './Activity/ActivityContainerPage'
import Files from './Files/Files'
import Notifications from './Notifications/Notifications'
import Groups from './Profile/Groups'
import Profile from './Profile/Profile'
import ProfileContainer from './Profile/ProfileContainer'
import Publications from './Profile/Publications'
import PublishRequestsComments from './PublishRequests/PublishRequestsComments'
import PublishRequestsContainer from './PublishRequests/PublishRequestsContainer'
import PublishRequestsContent from './PublishRequests/PublishRequestsContent'
import PublishRequestsFiles from './PublishRequests/PublishRequestsFiles'
import Settings from './Settings/Settings'

const Component = () => (
    <Routes>
        <Route path="/" element={<Navigate to="profile" replace />} />

        <Route
            path="/profile/*"
            element={
                <Routes>
                    <Route element={<ProfileContainer />}>
                        <Route path="/" element={<Profile />} />
                        <Route path="/reply" element={<Profile />} />
                        <Route
                            path="/publications"
                            element={<Publications />}
                        />
                        <Route path="/groups" element={<Groups />} />
                    </Route>
                </Routes>
            }
        />

        <Route
            path="/activity/*"
            element={
                <RequireAuth viewer="loggedIn">
                    <Routes>
                        <Route element={<ActivityContainer />}>
                            <Route
                                path="/"
                                element={<Navigate to="published" replace />}
                            />
                            <Route
                                path="/published"
                                element={
                                    <ActivityContainerPage activePage="published" />
                                }
                            />
                            <Route
                                path="/drafts"
                                element={
                                    <ActivityContainerPage activePage="drafts" />
                                }
                            />
                            <Route
                                path="/archived"
                                element={
                                    <ActivityContainerPage activePage="archived" />
                                }
                            />
                        </Route>
                    </Routes>
                </RequireAuth>
            }
        />

        <Route
            path="/publish-requests/*"
            element={
                <RequireAuth viewer="loggedIn">
                    <Routes>
                        <Route element={<PublishRequestsContainer />}>
                            <Route
                                path="/content"
                                element={<PublishRequestsContent />}
                            />
                            <Route
                                path="/files"
                                element={<PublishRequestsFiles />}
                            />
                            <Route
                                path="/comments"
                                element={<PublishRequestsComments />}
                            />
                        </Route>
                    </Routes>
                </RequireAuth>
            }
        />

        <Route
            path="/settings"
            element={
                <RequireAuth viewer="loggedIn">
                    <Settings />
                </RequireAuth>
            }
        />
        <Route
            path="/notifications"
            element={
                <RequireAuth viewer="loggedIn">
                    <Notifications />
                </RequireAuth>
            }
        />
        <Route path="/files" element={<Files />} />
        <Route path="/files/:containerGuid" element={<Files />} />
        <Route element={<NotFound />} />
    </Routes>
)

export default Component
