import styled from 'styled-components'

import Flexer from 'js/components/Flexer/Flexer'

const NotificationSettingWrapper = styled(Flexer)`
    align-items: flex-start;
    justify-content: space-between;

    &.GroupNotification:not(:last-child) {
        border-bottom: 1px solid ${(p) => p.theme.color.grey[20]};
    }

    .NotificationsCheckboxes {
        flex-shrink: 0;
        display: flex;

        > * {
            width: 40px;
            height: 40px;
            display: flex;
            align-items: center;
            justify-content: center;
        }
    }
`

export default NotificationSettingWrapper
