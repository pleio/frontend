import React from 'react'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import Setting from 'js/components/Setting'
import Switch from 'js/components/Switch/Switch'

const SettingsNotifications = ({ mutate, entity }) => {
    const onChange = (evt) => {
        mutate({
            variables: {
                input: {
                    guid: entity.guid,
                    newsletter: evt.target.checked,
                },
            },
        })
    }

    return (
        <Setting
            title="settings.newsletter"
            helper="settings.newsletter-helper"
            htmlFor="newsletter"
        >
            <Switch
                name="newsletter"
                checked={entity.getsNewsletter}
                onChange={onChange}
                releaseInputArea
            />
        </Setting>
    )
}

const Mutation = gql`
    mutation editNotifications($input: editNotificationsInput!) {
        editNotifications(input: $input) {
            user {
                guid
                getsNewsletter
            }
        }
    }
`

export default graphql(Mutation)(SettingsNotifications)
