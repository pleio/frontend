import React from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import Flexer from 'js/components/Flexer/Flexer'
import Setting from 'js/components/Setting'
import SettingCard from 'js/components/SettingCard'
import ShowMore from 'js/components/ShowMore'
import Spacer from 'js/components/Spacer/Spacer'

import EmailDirectIcon from 'icons/mail-direct.svg'
import PushIcon from 'icons/push.svg'

import EmailNotificationsSettings from './EmailNotificationsSettings'
import GroupNotification from './GroupNotification'
import Notification from './Notification'
import NotificationsIcons from './NotificationsIcons'
import PushNotifications from './PushNotifications'

const IconHelperWrapper = styled(Flexer)`
    font-size: ${(p) => p.theme.font.size.small};
    line-height: ${(p) => p.theme.font.lineHeight.small};

    .IconHelperIcon {
        width: 22px;
        height: 32px;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .IconHelperText {
        padding: 2px 0;
    }
`

const IconHelper = ({ id, icon, children }) => (
    <IconHelperWrapper
        as="label"
        id={id}
        justifyContent="flex-start"
        aria-hidden
    >
        <div className="IconHelperIcon">{icon}</div>
        <div className="IconHelperText">{children}</div>
    </IconHelperWrapper>
)

const NotificationsSettings = ({
    entity,
    pushNotificationsEnabled,
    isCurrentUser,
}) => {
    const { t } = useTranslation()

    return (
        <>
            {pushNotificationsEnabled && isCurrentUser && (
                <PushNotifications withCard />
            )}

            <SettingCard radiusSize="large">
                <Spacer>
                    <Setting
                        title="settings.notifications"
                        helper="settings.notifications-helper"
                        htmlFor="emailNotifications"
                    />

                    <div>
                        {pushNotificationsEnabled && (
                            <IconHelper
                                id="notifications-push-definition"
                                icon={<PushIcon />}
                            >
                                {t('settings.notifications-push-definition')}
                            </IconHelper>
                        )}
                        <IconHelper
                            id="notifications-mail-definition"
                            icon={<EmailDirectIcon />}
                        >
                            {t('settings.notifications-mail-definition')}
                        </IconHelper>
                    </div>

                    <Spacer spacing="tiny">
                        <Notification
                            entity={entity}
                            entityType="mention"
                            pushNotificationsEnabled={pushNotificationsEnabled}
                        />

                        <Notification
                            entity={entity}
                            entityType="comment"
                            pushNotificationsEnabled={pushNotificationsEnabled}
                        />
                    </Spacer>

                    {entity.groupNotifications.length > 0 && (
                        <div>
                            <Flexer
                                alignItems="flex-end"
                                justifyContent="space-between"
                            >
                                <Setting
                                    subtitle="settings.notifications-groups"
                                    helper="settings.notifications-groups-helper"
                                />

                                <NotificationsIcons
                                    pushNotificationsEnabled={
                                        pushNotificationsEnabled
                                    }
                                />
                            </Flexer>

                            <ShowMore
                                maxHeight={220}
                                label={t('settings.show-all-groups')}
                            >
                                {entity.groupNotifications.map((group) => (
                                    <GroupNotification
                                        key={group.guid}
                                        entity={group}
                                        userGuid={entity.guid}
                                        pushNotificationsEnabled={
                                            pushNotificationsEnabled
                                        }
                                    />
                                ))}
                            </ShowMore>
                        </div>
                    )}
                </Spacer>
            </SettingCard>

            <EmailNotificationsSettings entity={entity} />
        </>
    )
}

export default NotificationsSettings
