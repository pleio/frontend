import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'

import Checkbox from 'js/components/Checkbox/Checkbox'
import Switch from 'js/components/Switch/Switch'

import Wrapper from './NotificationSettingWrapper'
import notificationsReducer from './notificationsReducer'

const GroupNotification = ({ entity, userGuid, pushNotificationsEnabled }) => {
    const { t } = useTranslation()
    const isLoaded = React.useRef(false)

    const { guid, name } = entity

    const [
        {
            isNotificationsEnabled,
            isNotificationPushEnabled,
            isNotificationDirectMailEnabled,
        },
        dispatch,
    ] = React.useReducer(notificationsReducer, {
        isNotificationsEnabled: entity.isNotificationsEnabled,
        isNotificationPushEnabled: entity.isNotificationPushEnabled,
        isNotificationDirectMailEnabled: entity.isNotificationDirectMailEnabled,
    })

    const [mutate] = useMutation(EDIT_GROUP_NOTIFICATIONS)

    React.useEffect(() => {
        if (!isLoaded.current) {
            // Prevent first render to call the mutation
            isLoaded.current = true
            return
        }

        mutate({
            variables: {
                input: {
                    guid,
                    isNotificationsEnabled,
                    isNotificationPushEnabled,
                    isNotificationDirectMailEnabled,
                    userGuid,
                },
            },
            refetchQueries: ['ProfileSettings'],
        }).catch((error) => {
            console.error(error)
        })
    }, [
        isNotificationsEnabled,
        isNotificationPushEnabled,
        isNotificationDirectMailEnabled,
        guid,
        userGuid,
        mutate,
    ])

    return (
        <>
            <Wrapper className="GroupNotification">
                <Switch
                    name={`${guid}-isNotificationsEnabled`}
                    checked={isNotificationsEnabled}
                    onChange={() => dispatch({ type: 'TOGGLE_NOTIFICATIONS' })}
                    size="small"
                    label={name}
                    aria-label={t('settings.notifications-groups-switch', {
                        groupName: name,
                    })}
                    style={{ flexGrow: 1, padding: '10px 0' }} // Makes for minimum height of 40px
                />

                <div className="NotificationsCheckboxes">
                    {pushNotificationsEnabled && isNotificationsEnabled && (
                        <Checkbox
                            name={`${guid}-isNotificationPushEnabled`}
                            checked={isNotificationPushEnabled}
                            onChange={() => dispatch({ type: 'TOGGLE_PUSH' })}
                            size="small"
                            aria-label={t(
                                'settings.notifications-toggle-push-for',
                                { subject: name },
                            )}
                            aria-describedby="notifications-push-definition"
                        />
                    )}

                    {isNotificationsEnabled && (
                        <Checkbox
                            name={`${guid}-isNotificationDirectMailEnabled`}
                            checked={isNotificationDirectMailEnabled}
                            onChange={() => dispatch({ type: 'TOGGLE_MAIL' })}
                            size="small"
                            aria-label={t(
                                'settings.notifications-toggle-email-for',
                                {
                                    subject: name,
                                },
                            )}
                            aria-describedby="notifications-mail-definition"
                        />
                    )}
                </div>
            </Wrapper>
        </>
    )
}

const EDIT_GROUP_NOTIFICATIONS = gql`
    mutation editGroupNotifications($input: editGroupNotificationsInput!) {
        editGroupNotifications(input: $input) {
            group {
                guid
                isNotificationsEnabled
                isNotificationPushEnabled
                isNotificationDirectMailEnabled
            }
        }
    }
`

export default GroupNotification
