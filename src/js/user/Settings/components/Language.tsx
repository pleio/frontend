import React from 'react'
import { gql, useMutation } from '@apollo/client'
import i18next from 'i18next'

import Select from 'js/components/Select/Select'
import Setting from 'js/components/Setting'

const Language = ({ site, entity }) => {
    const [changeUserLang, { loading }] = useMutation(CHANGE_USER_LANGUAEG)

    const handleChange = (val: string) => {
        changeUserLang({
            variables: {
                input: {
                    guid: entity.guid,
                    language: val,
                },
            },
            refetchQueries: ['ProfileSettings', 'TopMenu'],
        })
            .then(() => {
                i18next.changeLanguage(val).then((t) => {
                    t('key') // -> same as i18next.t
                })
            })
            .catch((errors: Error) => {
                console.error(errors)
            })
    }

    return (
        // @ts-ignore
        <Setting
            title="settings.language"
            helper="settings.language-helper"
            inputWidth="normal"
            htmlFor="language"
        >
            <Select
                // @ts-ignore
                name="language"
                options={site.languageOptions}
                value={entity.language}
                onChange={handleChange}
                isLoading={loading}
            />
        </Setting>
    )
}

const CHANGE_USER_LANGUAEG = gql`
    mutation editNotifications($input: editNotificationsInput!) {
        editNotifications(input: $input) {
            user {
                guid
                language
            }
        }
    }
`

export default Language
