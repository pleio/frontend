import React from 'react'
import styled from 'styled-components'

import EmailDirectIcon from 'icons/mail-direct.svg'
import PushIcon from 'icons/push.svg'

const Wrapper = styled.div`
    display: flex;

    > * {
        width: 40px;
        height: 32px;
        display: flex;
        align-items: center;
        justify-content: center;
    }
`

const NotificationsIcons = ({ pushNotificationsEnabled }) => {
    return (
        <Wrapper aria-hidden>
            {pushNotificationsEnabled && (
                <div>
                    <PushIcon />
                </div>
            )}
            <div>
                <EmailDirectIcon />
            </div>
        </Wrapper>
    )
}

export default NotificationsIcons
