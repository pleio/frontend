import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'

import AnimatePresence from 'js/components/AnimatePresence/AnimatePresence'
import Select from 'js/components/Select/Select'
import Setting from 'js/components/Setting'
import SettingCard from 'js/components/SettingCard'
import Spacer from 'js/components/Spacer/Spacer'
import Switch from 'js/components/Switch/Switch'

const EmailNotificationsSettings = ({ entity }) => {
    const { t } = useTranslation()

    const [mutate] = useMutation(EDIT_NOTIFICATIONS)

    const onChange = (evt) => {
        mutate({
            variables: {
                input: {
                    guid: entity.guid,
                    emailNotifications: evt.target.checked,
                },
            },
        })
    }

    const changeFrequency = (val) => {
        mutate({
            variables: {
                input: {
                    guid: entity.guid,
                    emailNotificationsFrequency: val,
                },
            },
        })
    }

    const options = [
        {
            value: 24,
            label: t('settings.notifications-frequency-daily'),
        },
        {
            value: 4,
            label: t('settings.notifications-frequency-4-hours'),
        },
    ]

    return (
        <SettingCard radiusSize="large">
            <Setting
                title="settings.notifications-email"
                helper="settings.notifications-email-helper"
                htmlFor="emailNotifications"
            >
                <Switch
                    name="emailNotifications"
                    checked={entity.emailNotifications}
                    onChange={onChange}
                    releaseInputArea
                />
            </Setting>

            <AnimatePresence visible={entity.emailNotifications}>
                <Spacer style={{ paddingTop: '24px' }}>
                    <Setting
                        subtitle="settings.notifications-frequency"
                        helper="settings.notifications-frequency-helper"
                        htmlFor="emailNotificationsFrequency"
                        inputWidth="normal"
                    >
                        <Select
                            name="emailNotificationsFrequency"
                            options={options}
                            value={entity.emailNotificationsFrequency}
                            onChange={changeFrequency}
                        />
                    </Setting>
                </Spacer>
            </AnimatePresence>
        </SettingCard>
    )
}

const EDIT_NOTIFICATIONS = gql`
    mutation editNotifications($input: editNotificationsInput!) {
        editNotifications(input: $input) {
            user {
                guid
                emailNotifications
                emailNotificationsFrequency
            }
        }
    }
`

export default EmailNotificationsSettings
