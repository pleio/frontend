import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'

import Checkbox from 'js/components/Checkbox/Checkbox'
import Switch from 'js/components/Switch/Switch'
import Text from 'js/components/Text/Text'

import Wrapper from './NotificationSettingWrapper'
import NotificationsIcons from './NotificationsIcons'
import notificationsReducer from './notificationsReducer'

function useTexts(entityType) {
    const { t } = useTranslation()

    const mentionTexts = {
        labelText: t('settings.notifications-mentions-switch-label'),
        switchText: t('settings.notifications-mentions-switch'),
        switchHelperText: t('settings.notifications-mentions-switch-helper'),
    }

    const commentTexts = {
        labelText: t('settings.notifications-comments-switch-label'),
        switchText: t('settings.notifications-comments-switch'),
        switchHelperText: t('settings.notifications-comments-switch-helper'),
    }

    return entityType === 'mention' ? mentionTexts : commentTexts
}

function getSettings(entity, entityType) {
    const mentionSettings = {
        enabled: entity.isMentionNotificationsEnabled,
        push: entity.isMentionNotificationPushEnabled,
        email: entity.isMentionNotificationDirectMailEnabled,
    }

    const commentSettings = {
        enabled: entity.isCommentNotificationsEnabled,
        push: entity.isCommentNotificationPushEnabled,
        email: entity.isCommentNotificationDirectMailEnabled,
    }

    return entityType === 'mention' ? mentionSettings : commentSettings
}

const Notification = ({ entity, entityType, pushNotificationsEnabled }) => {
    const { t } = useTranslation()

    const isLoaded = React.useRef(false)

    const { labelText, switchText, switchHelperText } = useTexts(entityType)

    const settings = getSettings(entity, entityType)

    const { guid } = entity

    const [
        {
            isNotificationsEnabled,
            isNotificationPushEnabled,
            isNotificationDirectMailEnabled,
        },
        dispatch,
    ] = React.useReducer(notificationsReducer, {
        isNotificationsEnabled: settings.enabled,
        isNotificationPushEnabled: settings.push,
        isNotificationDirectMailEnabled: settings.email,
    })

    const [mutate] = useMutation(EDIT_NOTIFICATIONS)

    React.useEffect(() => {
        if (!isLoaded.current) {
            // Prevent first render to call the mutation
            isLoaded.current = true
            return
        }

        const input =
            entityType === 'mention'
                ? {
                      isMentionNotificationsEnabled: isNotificationsEnabled,
                      isMentionNotificationPushEnabled:
                          isNotificationPushEnabled,
                      isMentionNotificationDirectMailEnabled:
                          isNotificationDirectMailEnabled,
                  }
                : {
                      isCommentNotificationsEnabled: isNotificationsEnabled,
                      isCommentNotificationPushEnabled:
                          isNotificationPushEnabled,
                      isCommentNotificationDirectMailEnabled:
                          isNotificationDirectMailEnabled,
                  }

        mutate({
            variables: {
                input: {
                    guid,
                    ...input,
                },
            },
            refetchQueries: ['ProfileSettings'],
        }).catch((error) => {
            console.error(error)
        })
    }, [
        isNotificationsEnabled,
        isNotificationPushEnabled,
        isNotificationDirectMailEnabled,
        guid,
        mutate,
        entityType,
    ])

    return (
        <>
            <Wrapper>
                <div>
                    <Switch
                        name={`${guid}-isNotificationsEnabled`}
                        checked={isNotificationsEnabled}
                        onChange={() =>
                            dispatch({ type: 'TOGGLE_NOTIFICATIONS' })
                        }
                        label={labelText}
                        aria-label={switchText}
                        size="small"
                    />

                    <Text variant="grey" size="small">
                        {switchHelperText}
                    </Text>
                </div>

                <div>
                    {isNotificationsEnabled && (
                        <NotificationsIcons
                            pushNotificationsEnabled={pushNotificationsEnabled}
                        />
                    )}

                    <div className="NotificationsCheckboxes">
                        {pushNotificationsEnabled && isNotificationsEnabled && (
                            <Checkbox
                                name={`${guid}-isNotificationPushEnabled`}
                                checked={isNotificationPushEnabled}
                                onChange={() =>
                                    dispatch({ type: 'TOGGLE_PUSH' })
                                }
                                size="small"
                                aria-label={t(
                                    'settings.notifications-toggle-push-for',
                                    { subject: labelText },
                                )}
                                aria-describedby="notifications-push-definition"
                            />
                        )}

                        {isNotificationsEnabled && (
                            <Checkbox
                                name={`${guid}-isNotificationDirectMailEnabled`}
                                checked={isNotificationDirectMailEnabled}
                                onChange={() =>
                                    dispatch({ type: 'TOGGLE_MAIL' })
                                }
                                size="small"
                                aria-label={t(
                                    'settings.notifications-toggle-email-for',
                                    { subject: labelText },
                                )}
                                aria-describedby="notifications-mail-definition"
                            />
                        )}
                    </div>
                </div>
            </Wrapper>
        </>
    )
}

const EDIT_NOTIFICATIONS = gql`
    mutation editNotifications($input: editNotificationsInput!) {
        editNotifications(input: $input) {
            user {
                guid
            }
        }
    }
`

export default Notification
