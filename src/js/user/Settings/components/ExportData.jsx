import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import Modal from 'js/components/Modal/Modal'
import Setting from 'js/components/Setting'

const ExportData = () => {
    const { t } = useTranslation()
    const [showModal, setShowModal] = useState(false)

    const [mutate] = useMutation(CREATE_CONTENT_SNAPSHOT)

    const createContentSnapshot = async () => {
        await mutate()
            .then(() => {
                setShowModal(false)
            })
            .catch((error) => {
                console.error(error)
            })
    }

    const {
        handleSubmit,
        formState: { isSubmitting },
    } = useForm()

    return (
        <>
            <Setting
                title="settings.export-data"
                helper="settings.export-data-helper"
            >
                <Button
                    variant="secondary"
                    size="normal"
                    onClick={() => setShowModal(true)}
                >
                    {t('settings.export-data')}
                </Button>
            </Setting>

            <Modal
                size="small"
                isVisible={showModal}
                title={t('settings.export-data')}
                onClose={() => setShowModal(false)}
            >
                <form onSubmit={handleSubmit(createContentSnapshot)}>
                    {t('settings.export-data-confirm')}
                    <Flexer mt>
                        <Button
                            size="normal"
                            variant="tertiary"
                            onClick={() => setShowModal(false)}
                        >
                            {t('action.no-cancel')}
                        </Button>
                        <Button
                            size="normal"
                            variant="primary"
                            type="submit"
                            loading={isSubmitting}
                        >
                            {t('action.yes-confirm')}
                        </Button>
                    </Flexer>
                </form>
            </Modal>
        </>
    )
}

const CREATE_CONTENT_SNAPSHOT = gql`
    mutation CreateContentSnapshot {
        createContentSnapshot {
            success
        }
    }
`

export default ExportData
