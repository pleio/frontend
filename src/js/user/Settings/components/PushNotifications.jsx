import React, { useState } from 'react'
import { Trans, useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'

import Setting from 'js/components/Setting'
import SettingCard from 'js/components/SettingCard'
import Switch from 'js/components/Switch/Switch'
import { usePushManagerSubscription } from 'js/lib/hooks'

function isIOS() {
    return (
        [
            'iPad Simulator',
            'iPhone Simulator',
            'iPod Simulator',
            'iPad',
            'iPhone',
            'iPod',
        ].includes(navigator.platform) ||
        // iPad on iOS 13 detection
        (navigator.userAgent.includes('Mac') && 'ontouchend' in document)
    )
}

const urlB64ToUint8Array = (base64String) => {
    const padding = '='.repeat((4 - (base64String.length % 4)) % 4)
    const base64 = (base64String + padding)
        .replace(/-/g, '+')
        .replace(/_/g, '/')

    const rawData = window.atob(base64)
    const outputArray = new Uint8Array(rawData.length)

    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i)
    }
    return outputArray
}

const WrapperComponent = ({ withCard, children }) => {
    if (withCard) {
        return <SettingCard radiusSize="large">{children}</SettingCard>
    }
    return children
}

const PushNotifications = ({ withCard = false }) => {
    const vapid = document.querySelector(
        'meta[name="webpush-vapid-key"]',
    )?.content

    const pushEnabled =
        vapid && 'serviceWorker' in navigator && 'Notification' in window

    const { t } = useTranslation()

    const [pushIsDenied, setPushIsDenied] = useState(
        'Notification' in window && Notification.permission === 'denied',
    )
    const [subscribed, setSubscribed] = useState(null)

    const { getSubscription } = usePushManagerSubscription(setSubscribed)

    const [isLoading, setIsLoading] = useState(false)

    const [mutate] = useMutation(ADD_WEB_PUSH_SUBSCRIPTION)

    if (!pushEnabled) return null

    const subscribe = async () => {
        const registration = await navigator.serviceWorker.getRegistration('/')

        return registration.pushManager.subscribe({
            userVisibleOnly: true,
            applicationServerKey: urlB64ToUint8Array(vapid),
        })
    }

    const addSubscription = async (subscription) => {
        const data = subscription.toJSON()
        mutate({
            variables: {
                input: {
                    browser: navigator.userAgent
                        .match(/(firefox|msie|chrome|safari|trident)/gi)[0]
                        .toLowerCase(),
                    endpoint: data.endpoint,
                    auth: data.keys.auth,
                    p256dh: data.keys.p256dh,
                },
            },
        })
            .then(() => {
                setSubscribed(!!subscription)
                setIsLoading(false)
            })
            .catch((error) => {
                console.error(error)
                setIsLoading(false)
            })
    }

    const updateSubscription = async () => {
        setIsLoading(true)
        const subscription = await getSubscription()
        if (subscription) {
            const success = await subscription.unsubscribe()
            if (success) {
                setSubscribed(false)
                setIsLoading(false)
            }
        } else {
            const permission = await Notification.requestPermission()
            if (permission === 'granted') {
                const subscription = await subscribe()
                await addSubscription(subscription)
            }
            if (permission === 'denied') {
                setPushIsDenied(true)
                setIsLoading(false)
            }
        }
    }

    let helperText = pushIsDenied
        ? t('settings.push-notifications-denied-helper')
        : t('settings.push-notifications-helper')

    if (isIOS()) {
        helperText += `<br/><br/>${t('settings.push-notifications-ios-helper')}`
    }

    return (
        <WrapperComponent withCard={withCard}>
            <Setting
                title="settings.push-notifications"
                helper={<Trans>{helperText}</Trans>}
                htmlFor="pushNotifications"
            >
                {subscribed !== null && pushEnabled !== null && (
                    <Switch
                        name="pushNotifications"
                        disabled={pushIsDenied}
                        checked={subscribed}
                        onChange={updateSubscription}
                        releaseInputArea
                        loading={isLoading}
                    />
                )}
            </Setting>
        </WrapperComponent>
    )
}

const ADD_WEB_PUSH_SUBSCRIPTION = gql`
    mutation addWebPushSubscription($input: addWebPushSubscriptionInput!) {
        addWebPushSubscription(input: $input) {
            success
        }
    }
`

export default PushNotifications
