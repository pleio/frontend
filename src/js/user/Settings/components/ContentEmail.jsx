import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import AnimatePresence from 'js/components/AnimatePresence/AnimatePresence'
import Select from 'js/components/Select/Select'
import Setting from 'js/components/Setting'
import TagCategoriesField from 'js/components/TagCategories/TagCategoriesRowField'

const ContentEmail = ({ guid, emailOverview, tagCategories, mutate }) => {
    const { t } = useTranslation()

    const setPeriod = (val) => {
        mutate({
            variables: {
                input: {
                    guid,
                    frequency: val,
                },
            },
        })
    }

    const setTags = (tags) => {
        mutate({
            variables: {
                input: {
                    guid,
                    tags,
                },
            },
        })
    }

    const options = [
        {
            value: 'never',
            label: t('settings.content-email-never'),
        },
        {
            value: 'monthly',
            label: t('settings.content-email-monthly'),
        },
        {
            value: 'weekly',
            label: t('settings.content-email-weekly'),
        },
        {
            value: 'daily',
            label: t('settings.content-email-daily'),
        },
    ]

    return (
        <>
            <Setting
                title="settings.content-email"
                helper="settings.content-email-description"
                inputWidth="normal"
                htmlFor="periods"
            >
                <Select
                    name="periods"
                    options={options}
                    value={emailOverview.frequency}
                    onChange={setPeriod}
                />
            </Setting>

            <AnimatePresence
                visible={
                    tagCategories.length > 0 &&
                    emailOverview.frequency !== 'never'
                }
            >
                <Setting
                    subtitle="settings.content-email-tags"
                    helper="settings.content-email-tags-description"
                    inputWidth="full"
                    style={{ paddingTop: '24px' }}
                >
                    <TagCategoriesField
                        tagCategories={tagCategories}
                        value={emailOverview.tags}
                        onChange={setTags}
                    />
                </Setting>
            </AnimatePresence>
        </>
    )
}

const Mutation = gql`
    mutation editEmailOverview($input: editEmailOverviewInput!) {
        editEmailOverview(input: $input) {
            user {
                guid
                emailOverview {
                    frequency
                    tags
                }
            }
        }
    }
`

export default graphql(Mutation)(ContentEmail)
