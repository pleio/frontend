const notificationsReducer = (state, action) => {
    switch (action.type) {
        case 'TOGGLE_NOTIFICATIONS': {
            const isNotificationsEnabled = !state.isNotificationsEnabled
            return {
                ...state,
                isNotificationsEnabled,
                isNotificationPushEnabled: isNotificationsEnabled
                    ? state.isNotificationPushEnabled
                    : false,
                isNotificationDirectMailEnabled: isNotificationsEnabled
                    ? state.isNotificationDirectMailEnabled
                    : false,
            }
        }
        case 'TOGGLE_PUSH':
            return {
                ...state,
                isNotificationPushEnabled: !state.isNotificationPushEnabled,
            }
        case 'TOGGLE_MAIL':
            return {
                ...state,
                isNotificationDirectMailEnabled:
                    !state.isNotificationDirectMailEnabled,
            }
        default:
            return state
    }
}

export default notificationsReducer
