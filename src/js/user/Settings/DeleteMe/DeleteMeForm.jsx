import React from 'react'
import { useTranslation } from 'react-i18next'
import PropTypes from 'prop-types'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import Text from 'js/components/Text/Text'

const DeleteMeForm = ({ onSubmit, closeModal }) => {
    const { t } = useTranslation()

    return (
        <>
            <Text size="small" as="p">
                {t('settings.remove-account-form-paragraph-1')}
            </Text>
            <Text size="small" as="p">
                {t('settings.remove-account-form-paragraph-2')}
            </Text>
            <Text size="small" as="p">
                {t('settings.remove-account-form-paragraph-3')}
            </Text>

            <Flexer mt>
                <Button
                    size="normal"
                    variant="tertiary"
                    type="button"
                    onClick={closeModal}
                >
                    {t('action.no-back')}
                </Button>
                <Button size="normal" variant="primary" onHandle={onSubmit}>
                    {t('action.yes-confirm')}
                </Button>
            </Flexer>
        </>
    )
}

DeleteMeForm.propTypes = {
    onSubmit: PropTypes.func,
    closeModal: PropTypes.func,
}

export default DeleteMeForm
