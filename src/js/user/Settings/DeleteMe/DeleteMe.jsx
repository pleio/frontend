import React, { useState } from 'react'
import { Trans, useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import Button from 'js/components/Button/Button'
import Modal from 'js/components/Modal/Modal'
import Setting from 'js/components/Setting'

import CancelDeleteForm from './CancelDeleteForm'
import DeleteMeForm from './DeleteMeForm'
import DeleteMeSuccessForm from './DeleteMeSuccessForm'

const DeleteMe = ({ entity, mutate }) => {
    const [success, setSuccess] = useState(entity.requestDelete)
    const [showDeleteMeModal, setShowDeleteMeModal] = useState(false)
    const [showCancelDeleteModal, setShowCancelDeleteMeModal] = useState(false)

    const toggleDeleteMeModal = () => {
        setShowDeleteMeModal(!showDeleteMeModal)
    }

    const toggleCancelDeleteModal = () => {
        setShowCancelDeleteMeModal(!showCancelDeleteModal)
    }

    const handleSubmitDeleteMeSuccess = () => {
        return toggleDeleteMe(setShowDeleteMeModal(false))
    }

    const handleSubmitCancelDelete = () => {
        return toggleDeleteMe(setShowCancelDeleteMeModal(false))
    }

    const toggleDeleteMe = (afterResolve) => {
        return new Promise((resolve, reject) => {
            mutate({
                variables: {
                    input: {
                        guid: entity.guid,
                    },
                },
                refetchQueries: ['ProfileSettings'],
            })
                .then(() => {
                    setSuccess(!entity.requestDelete)
                    resolve()
                    afterResolve()
                })
                .catch((errors) => {
                    reject(new Error(errors))
                })
        })
    }

    const { t } = useTranslation()
    const { requestDelete } = entity

    return (
        <>
            <Setting
                title="settings.account"
                helper={
                    requestDelete ? (
                        <Trans i18nKey="settings.remove-request-sent-description">
                            You have sent an account removal request. Your
                            account remains at:
                            <a
                                href="https://account.pleio.nl"
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                {{ url: 'account.pleio.nl' }}
                            </a>
                            .
                        </Trans>
                    ) : (
                        <Trans i18nKey="settings.remove-request-description">
                            You can send a request to remove your account. Your
                            account remains at:
                            <a
                                href="https://account.pleio.nl"
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                {{ url: 'account.pleio.nl' }}
                            </a>
                            .
                        </Trans>
                    )
                }
            >
                <Button
                    size="normal"
                    variant="secondary"
                    onClick={
                        requestDelete
                            ? toggleCancelDeleteModal
                            : toggleDeleteMeModal
                    }
                >
                    {requestDelete
                        ? t('settings.withdraw-remove-request')
                        : t('settings.cancel-membership')}
                </Button>
            </Setting>

            <Modal
                size="small"
                title={
                    success
                        ? t('settings.remove-request-sent')
                        : t('settings.cancel-membership')
                }
                isVisible={showDeleteMeModal}
                onClose={toggleDeleteMeModal}
            >
                {success ? (
                    <DeleteMeSuccessForm
                        onSubmit={handleSubmitDeleteMeSuccess}
                        closeModal={toggleDeleteMeModal}
                    />
                ) : (
                    <DeleteMeForm
                        onSubmit={toggleDeleteMe}
                        closeModal={toggleDeleteMeModal}
                    />
                )}
            </Modal>

            <Modal
                size="small"
                title={t('settings.withdraw-remove-request')}
                isVisible={showCancelDeleteModal}
                onClose={toggleCancelDeleteModal}
            >
                <CancelDeleteForm
                    onSubmit={handleSubmitCancelDelete}
                    closeModal={toggleCancelDeleteModal}
                />
            </Modal>
        </>
    )
}

const Mutation = gql`
    mutation toggleRequestDeleteUser($input: toggleRequestDeleteUserInput!) {
        toggleRequestDeleteUser(input: $input) {
            viewer {
                guid
            }
        }
    }
`

export default graphql(Mutation)(DeleteMe)
