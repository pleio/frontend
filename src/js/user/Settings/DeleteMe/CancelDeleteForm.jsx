import React from 'react'
import { useTranslation } from 'react-i18next'
import PropTypes from 'prop-types'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import Text from 'js/components/Text/Text'

const CancelDeleteForm = ({ onSubmit, closeModal }) => {
    const { t } = useTranslation()

    return (
        <>
            <Text size="small" as="p">
                {t('settings.withdraw-remove-request-description')}
            </Text>

            <Flexer mt>
                <Button
                    size="normal"
                    variant="tertiary"
                    type="button"
                    onClick={closeModal}
                >
                    {t('action.no-back')}
                </Button>
                <Button size="normal" variant="primary" onHandle={onSubmit}>
                    {t('action.yes-confirm')}
                </Button>
            </Flexer>
        </>
    )
}

CancelDeleteForm.propTypes = {
    onSubmit: PropTypes.func,
    closeModal: PropTypes.func,
    isLoading: PropTypes.bool,
}

export default CancelDeleteForm
