import React from 'react'
import { useTranslation } from 'react-i18next'
import PropTypes from 'prop-types'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import Text from 'js/components/Text/Text'

const DeleteMeSuccessForm = ({ closeModal, onSubmit }) => {
    const { t } = useTranslation()

    return (
        <div>
            <Text size="small" as="p">
                {t('settings.remove-request-sent-long')}
            </Text>

            <Flexer mt>
                <Button size="normal" variant="secondary" onHandle={onSubmit}>
                    {t('settings.withdraw-remove-request')}
                </Button>
                <Button size="normal" variant="primary" onClick={closeModal}>
                    {t('action.ok')}
                </Button>
            </Flexer>
        </div>
    )
}

DeleteMeSuccessForm.propTypes = {
    onSubmit: PropTypes.func,
    closeModal: PropTypes.func,
}

export default DeleteMeSuccessForm
