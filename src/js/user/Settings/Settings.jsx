import React from 'react'
import { Trans, useTranslation } from 'react-i18next'
import { Link, useLocation, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import styled from 'styled-components'

import Document from 'js/components/Document'
import { Container } from 'js/components/Grid/Grid'
import { H1 } from 'js/components/Heading'
import Section from 'js/components/Section/Section'
import SettingCard from 'js/components/SettingCard'
import Spacer from 'js/components/Spacer/Spacer'
import Sticky from 'js/components/Sticky/Sticky'
import TabMenu from 'js/components/TabMenu'
import DefaultTabPage from 'js/components/TabPage'
import Text from 'js/components/Text/Text'
import NotFound from 'js/core/NotFound'
import { START_OF_CONTENT_ELEMENT_ID } from 'js/lib/constants'

import ContentEmail from './components/ContentEmail'
import ExportData from './components/ExportData'
import Language from './components/Language'
import NotificationsSettings from './components/NotificationsSettings'
import SettingsNewsletter from './components/SettingsNewsletter'
import DeleteMe from './DeleteMe/DeleteMe'

const TabMenuWrapper = styled(Sticky)`
    position: relative;

    &[data-sticky='true'] {
        background: white;
        box-shadow: ${(p) => p.theme.shadow.soft.bottom};
        z-index: 1;
    }
`

const TABS = {
    GENERAL: '#general',
    NOTIFICATIONS: '#notifications',
    MAILING: '#mailing',
}

const TabPage = ({ visible, children, ...rest }) => {
    return (
        <DefaultTabPage visible={visible} {...rest}>
            <Spacer spacing="large" style={{ marginTop: '32px' }}>
                {children}
            </Spacer>
        </DefaultTabPage>
    )
}

const Settings = () => {
    const { t } = useTranslation()
    const { userGuid } = useParams()

    const { hash } = useLocation()

    const { loading, data } = useQuery(Query, {
        variables: {
            userGuid,
        },
    })

    if (loading) return null
    if (!data?.entity?.canEdit) return <NotFound />

    const { site, viewer, entity } = data
    const {
        languageOptions,
        cancelMembershipEnabled,
        pushNotificationsEnabled,
        tagCategories,
    } = site

    const startOfContentHash = `#${START_OF_CONTENT_ELEMENT_ID}`

    return (
        <>
            <Document title={t('settings.title')} />
            <Section backgroundColor="white" grow>
                <Container size="small" style={{ marginBottom: '8px' }}>
                    <div>
                        <H1>{t('settings.title')}</H1>

                        {viewer.user.guid !== entity.guid && (
                            <Text size="small" variant="grey">
                                <Trans i18nKey="settings.other-user">
                                    {{ name: entity.name }}
                                    <Link
                                        to={`/user/${entity.username}/profile`}
                                    >
                                        View profile
                                    </Link>
                                </Trans>
                            </Text>
                        )}
                    </div>
                </Container>

                <TabMenuWrapper>
                    <Container size="small">
                        <TabMenu
                            label={t('global.settings')}
                            options={[
                                {
                                    link: TABS.GENERAL,
                                    isActive:
                                        !hash ||
                                        hash === startOfContentHash ||
                                        hash === TABS.GENERAL,
                                    label: t('global.general'),
                                    id: 'tab-general',
                                    ariaControls: 'tabpanel-general',
                                },
                                {
                                    link: TABS.NOTIFICATIONS,
                                    isActive: hash === TABS.NOTIFICATIONS,
                                    label: t('settings.notifications'),
                                    id: 'tab-notifications',
                                    ariaControls: 'tabpanel-notifications',
                                },
                                {
                                    link: TABS.MAILING,
                                    isActive: hash === TABS.MAILING,
                                    label: t('settings.mailing'),
                                    id: 'tab-mailing',
                                    ariaControls: 'tabpanel-mailing',
                                },
                            ]}
                            edgePadding
                            edgeMargin
                            showBorder
                            style={{ position: 'static' }}
                        />
                    </Container>
                </TabMenuWrapper>

                <Container size="small">
                    <TabPage
                        visible={
                            !hash ||
                            hash === startOfContentHash ||
                            hash === TABS.GENERAL
                        }
                        id="tabpanel-general"
                        ariaLabelledby="tab-general"
                    >
                        {languageOptions.length > 1 && (
                            <SettingCard radiusSize="large">
                                <Language site={site} entity={entity} />
                            </SettingCard>
                        )}

                        <SettingCard radiusSize="large">
                            <ExportData />
                        </SettingCard>

                        {cancelMembershipEnabled &&
                            viewer.user.guid === entity.guid && (
                                <SettingCard radiusSize="large">
                                    <DeleteMe entity={entity} />
                                </SettingCard>
                            )}
                    </TabPage>

                    <TabPage
                        visible={hash === TABS.NOTIFICATIONS}
                        id="tabpanel-notifications"
                        ariaLabelledby="tab-notifications"
                    >
                        <NotificationsSettings
                            entity={entity}
                            isCurrentUser={viewer.user.guid === entity.guid}
                            pushNotificationsEnabled={pushNotificationsEnabled}
                        />
                    </TabPage>

                    <TabPage
                        visible={hash === TABS.MAILING}
                        id="tabpanel-mailing"
                        ariaLabelledby="tab-mailing"
                    >
                        {window.__SETTINGS__.site.newsletter && (
                            <SettingCard radiusSize="large">
                                <SettingsNewsletter entity={entity} />
                            </SettingCard>
                        )}

                        <SettingCard radiusSize="large">
                            <ContentEmail
                                guid={entity.guid}
                                emailOverview={entity.emailOverview}
                                tagCategories={tagCategories}
                            />
                        </SettingCard>
                    </TabPage>
                </Container>
            </Section>
        </>
    )
}

const Query = gql`
    query ProfileSettings($userGuid: String!) {
        viewer {
            guid
            loggedIn
            user {
                guid
            }
        }
        entity(guid: $userGuid) {
            guid
            ... on User {
                guid
                name
                username
                canEdit
                getsNewsletter
                emailOverview {
                    frequency
                    tags
                }
                requestDelete
                language
                isMentionNotificationsEnabled
                isMentionNotificationPushEnabled
                isMentionNotificationDirectMailEnabled
                isCommentNotificationsEnabled
                isCommentNotificationPushEnabled
                isCommentNotificationDirectMailEnabled
                emailNotifications
                groupNotifications {
                    guid
                    name
                    isNotificationsEnabled
                    isNotificationPushEnabled
                    isNotificationDirectMailEnabled
                }
                emailNotificationsFrequency
            }
        }
        site {
            guid
            cancelMembershipEnabled
            pushNotificationsEnabled
            tagCategories {
                name
                values
            }
            languageOptions {
                value
                label
            }
        }
    }
`

export default Settings
