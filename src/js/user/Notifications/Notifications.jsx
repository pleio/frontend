import React, { useLayoutEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { media, useIsMount } from 'helpers'
import styled from 'styled-components'

import Document from 'js/components/Document'
import FetchMore from 'js/components/FetchMore'
import { Container } from 'js/components/Grid/Grid'
import { H1 } from 'js/components/Heading'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Section from 'js/components/Section/Section'
import Text from 'js/components/Text/Text'
import { iconViewFragment } from 'js/lib/fragments/icon'

import Notification from './components/Notification'

const Wrapper = styled.ul`
    margin-top: 40px;

    ${media.mobileLandscapeDown`
        margin-left:-20px;
        margin-right:-20px;
    `}

    ${media.tabletUp`
        .NotificationButton {
            padding-left: 0;
            padding-right: 0;
        }
    `}
`

const Notifications = ({ data: { viewer } }) => {
    const { t } = useTranslation()

    const [queryLimit, setQueryLimit] = useState(25)
    const { loading, data, refetch, fetchMore } = useQuery(
        NOTIFICATIONS_QUERY,
        {
            variables: {
                offset: 0,
                limit: queryLimit,
                unread: false,
            },
        },
    )

    const isMount = useIsMount()

    useLayoutEffect(() => {
        if (isMount) {
            // fetchPolicy: 'cache-and-network' resets scroll to top when refetching
            refetch()
        }
    }, [isMount, refetch])

    if (viewer && !viewer.user) return null

    return (
        <>
            <Document title={t('notifications.title')} />
            <Section backgroundColor="white" grow>
                <Container>
                    <H1>{t('notifications.title')}</H1>
                    <Wrapper>
                        {loading ? (
                            <LoadingSpinner
                                style={{
                                    marginTop: '-8px',
                                    marginBottom: '16px',
                                }}
                            />
                        ) : (
                            <FetchMore
                                edges={data.notifications.edges}
                                getMoreResults={(data) =>
                                    data.notifications.edges
                                }
                                fetchMore={fetchMore}
                                fetchType="scroll"
                                fetchCount={25}
                                setLimit={setQueryLimit}
                                maxLimit={data.notifications.total}
                                noResults={
                                    <Text
                                        size="small"
                                        variant="grey"
                                        className="NotificationsEmpty"
                                    >
                                        {t('notifications.no-notifications')}
                                    </Text>
                                }
                            >
                                <ul>
                                    {data.notifications.edges.map(
                                        (notification, i) => (
                                            <Notification
                                                key={i}
                                                notification={notification}
                                            />
                                        ),
                                    )}
                                </ul>
                            </FetchMore>
                        )}
                    </Wrapper>
                </Container>
            </Section>
        </>
    )
}

export const NOTIFICATIONS_QUERY = gql`
    query NotificationsList($offset: Int, $limit: Int, $unread: Boolean!) {
        notifications(offset: $offset, limit: $limit, unread: $unread) {
            total
            edges {
                id
                action
                customMessage
                performer {
                    guid
                    name
                    username
                    icon
                }
                entity {
                    guid
                    ... on Blog {
                        title
                        url
                    }
                    ... on News {
                        title
                        url
                    }
                    ... on Discussion {
                        title
                        url
                    }
                    ... on Event {
                        title
                        url
                    }
                    ... on Question {
                        title
                        url
                    }
                    ... on Task {
                        title
                        url
                    }
                    ... on FileFolder {
                        title
                        url
                    }
                    ... on StatusUpdate {
                        url
                    }
                    ... on Wiki {
                        title
                        url
                    }
                    ... on Comment {
                        title
                        url
                    }
                    ... on ChatMessage {
                        guid
                        chat {
                            guid
                        }
                    }
                }
                container {
                    guid
                    ... on Group {
                        name
                        url
                        ${iconViewFragment}
                    }
                }
                isUnread
                timeCreated
            }
        }
    }
`

const Query = gql`
    query Notifications($offset: Int) {
        viewer {
            guid
            loggedIn
            user {
                guid
            }
        }
    }
`

export default graphql(Query)(Notifications)
