import { css } from 'styled-components'

export default css`
    position: relative;

    &:not(:first-child) .NotificationLink {
        border-top: 1px solid ${(p) => p.theme.color.grey[20]};
    }

    ${(p) =>
        p.$isUnread &&
        css`
            &:first-child .NotificationLink {
                border-top: 1px solid ${(p) => p.theme.color.grey[30]};
            }

            &:hover {
                .MarkAsReadButton {
                    opacity: 1;
                }

                .NotificationDate {
                    opacity: 0;
                    pointer-events: none;
                }
            }
        `}

    .NotificationLink {
        display: flex;
        align-items: center;
        width: 100%;
        padding: 12px 16px;

        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        color: ${(p) => p.theme.color.text.black};
        text-align: left;

        user-select: none;
    }

    .NotificationAvatar {
        margin-right: 12px;
    }

    .NotificationDate {
        margin-left: 8px;
        flex-shrink: 0;
        font-size: ${(p) => p.theme.font.size.tiny};
        line-height: ${(p) => p.theme.font.lineHeight.tiny};
        color: ${(p) => p.theme.color.text.grey};
    }

    .MarkAsReadButton {
        position: absolute;
        top: 0;
        bottom: 0;
        right: 16px;
        margin: auto 0;
        opacity: 0;
        background-color: white;

        &:hover,
        &:focus {
            opacity: 1;
        }
    }

    .VideoLink a {
        color: ${(p) => p.theme.color.primary.main};
        font-weight: ${(p) => p.theme.font.weight.semibold};

        &:hover {
            text-decoration: underline;
        }
    }
`
