import styled, { css } from 'styled-components'

export default styled.span`
    ${(p) =>
        p.$isBold &&
        css`
            font-weight: ${p.theme.font.weight.semibold};
        `};

    &.NotificationLinkText {
        color: ${(p) => p.theme.color.primary.main};

        .NotificationLink:hover & {
            text-decoration: underline;
        }
    }
`
