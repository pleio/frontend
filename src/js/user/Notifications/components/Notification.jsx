import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { gql, useMutation } from '@apollo/client'
import styled from 'styled-components'

import Avatar from 'js/components/Avatar/Avatar'
import DisplayDate from 'js/components/DisplayDate'
import IconButton from 'js/components/IconButton/IconButton'

import CheckIcon from 'icons/check.svg'

import Highlight from './NotificationHighlight'
import styles from './NotificationStyles'

const Wrapper = styled.li`
    ${styles}
`

const NavLinkWrapper = ({ url, navigateTo, children }) => {
    if (!url)
        return <div className="NotificationLink VideoLink">{children}</div>

    return (
        <Link to={url} className="NotificationLink" onClick={navigateTo}>
            {children}
        </Link>
    )
}

const Notification = ({ notification, hideNotifications }) => {
    const { t } = useTranslation()

    const [mutate, { loading }] = useMutation(MARK_AS_READ)

    const {
        id,
        entity,
        action,
        performer,
        isUnread,
        customMessage,
        container,
        timeCreated,
    } = notification

    if (!entity && action !== 'custom') return null

    const markAsRead = (evt) => {
        evt.stopPropagation()

        mutate({
            variables: {
                input: {
                    id,
                },
            },
            refetchQueries: ['NotificationsUnread', 'NotificationsList'],
        })
    }

    const navigateTo = () => {
        // Mark as read when navigated from Unread notifications
        if (isUnread) {
            mutate({
                variables: {
                    input: {
                        id,
                    },
                },
                refetchQueries: ['NotificationsUnread', 'NotificationsList'],
            })
        }

        hideNotifications?.()
    }

    let activity
    switch (action) {
        case 'commented':
            activity = t('notifications.replied-to')
            break

        case 'mentioned':
            activity =
                entity.__typename === 'Comment'
                    ? t('notifications.mentioned-you-in-comment')
                    : entity.__typename === 'ChatMessage'
                      ? t('notifications.mentioned-you-in-chat-message')
                      : t('notifications.mentioned-you-in')
            break

        case 'created':
            switch (entity.__typename) {
                case 'Blog':
                    activity = t('notifications.created-blog')
                    break
                case 'Discussion':
                    activity = t('notifications.created-discussion')
                    break
                case 'Event':
                    activity = t('notifications.created-event')
                    break
                case 'StatusUpdate':
                    activity = t('notifications.created-update')
                    break
                case 'Question':
                    activity = t('notifications.created-question')
                    break
                case 'Wiki':
                    activity = t('notifications.created-wiki')
                    break
                case 'File':
                    activity = t('notifications.uploaded-file')
                    break
                case 'Pad':
                    activity = t('notifications.created-pad')
                    break
                default:
                    activity = t('notifications.created-default')
            }
            break
    }

    let item
    if (action === 'custom') {
        item = customMessage
    } else if (entity.title) {
        item = entity.title
    } else if (entity.name) {
        item = entity.name
    } else if (entity.__typename === 'StatusUpdate') {
        item = t('notifications.an-update')
    }

    const url =
        action === 'welcome'
            ? `/user/${performer.username}/settings#notifications`
            : entity.__typename === 'ChatMessage'
              ? `/chat/${entity?.chat?.guid}`
              : entity?.url || container?.url || null

    const inGroup = container?.name

    const itemHasLink = !!entity?.url

    const avatarProps = {
        disabled: true,
        size: isUnread ? 'small' : 'normal',
        className: 'NotificationAvatar',
    }

    return (
        <Wrapper $isUnread={isUnread}>
            <NavLinkWrapper url={url} navigateTo={navigateTo}>
                {performer ? (
                    <Avatar
                        {...avatarProps}
                        image={performer.icon}
                        name={performer.name}
                    />
                ) : container ? (
                    <Avatar
                        {...avatarProps}
                        radiusStyle="rounded"
                        image={container.icon}
                        name={container.name}
                    />
                ) : null}
                <div style={{ flexGrow: 1 }}>
                    {action === 'welcome' ? (
                        t('notifications.welcome')
                    ) : action === 'custom' ? (
                        <Highlight>
                            <div
                                dangerouslySetInnerHTML={{
                                    __html: customMessage,
                                }}
                            />
                        </Highlight>
                    ) : (
                        <>
                            {performer ? (
                                <Highlight $isBold>{performer?.name}</Highlight>
                            ) : (
                                t('notifications.someone')
                            )}{' '}
                            {activity}{' '}
                            {item && (
                                <Highlight
                                    $isBold
                                    className={
                                        itemHasLink && 'NotificationLinkText'
                                    }
                                >
                                    {item}
                                </Highlight>
                            )}
                            {inGroup && (
                                <>
                                    {` ${t('notifications.in')} `}
                                    <Highlight
                                        $isBold
                                        className={
                                            !itemHasLink &&
                                            'NotificationLinkText'
                                        }
                                    >
                                        {container.name}
                                    </Highlight>
                                </>
                            )}
                        </>
                    )}
                </div>
                <div className="NotificationDate">
                    <DisplayDate
                        date={timeCreated}
                        type={isUnread ? 'timeSince' : 'shortDate'}
                    />
                </div>
            </NavLinkWrapper>

            {isUnread && (
                <IconButton
                    size="normal"
                    variant="secondary"
                    showOutline
                    tooltip={t('notifications.read')}
                    placement="left"
                    onClick={markAsRead}
                    loading={loading}
                    className="MarkAsReadButton"
                >
                    <CheckIcon />
                </IconButton>
            )}
        </Wrapper>
    )
}

const MARK_AS_READ = gql`
    mutation Notification($input: markAsReadInput!) {
        markAsRead(input: $input) {
            success
            notification {
                id
                isUnread
            }
        }
    }
`

export default Notification
