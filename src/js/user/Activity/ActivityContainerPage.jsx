import React from 'react'
import { useParams } from 'react-router-dom'

import FeedView from 'js/page/Widget/components/Feed/FeedView'

const ActivityContainerPage = ({ activePage }) => {
    const { userGuid } = useParams()

    const settings = {
        showTypeFilter: true,
        typeFilter: [],
        sortBy: activePage === 'published' ? 'timePublished' : 'timeCreated',
        sortDirection: 'desc',
        itemView: 'list',
        itemCount: 10,
    }

    return (
        <FeedView
            userGuid={userGuid}
            settings={settings}
            statusPublished={
                activePage === 'published'
                    ? []
                    : activePage === 'archived'
                      ? ['archived']
                      : ['draft'] // "pure" drafts and drafts waiting for moderation
            }
        />
    )
}

export default ActivityContainerPage
