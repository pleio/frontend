import React from 'react'
import { useTranslation } from 'react-i18next'
import { Outlet, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import Document from 'js/components/Document'
import { Container } from 'js/components/Grid/Grid'
import { H1 } from 'js/components/Heading'
import Section from 'js/components/Section/Section'
import TabMenu from 'js/components/TabMenu'

const ActivityContainer = () => {
    const { t } = useTranslation()

    const { userGuid } = useParams()

    const { data } = useQuery(GET_ACTIVITY_TOTALS, {
        variables: {
            userGuid,
        },
    })

    const options = [
        {
            to: `../activity/published`,
            label: `${t('global.published')} (${data?.published?.total || 0})`,
            end: true,
        },
        {
            to: `../activity/drafts`,
            label: `${t('global.drafts')} (${data?.drafts?.total || 0})`,
            end: true,
        },
        {
            to: `../activity/archived`,
            label: `${t('global.archived')} (${data?.archived?.total || 0})`,
            end: true,
        },
    ]

    return (
        <>
            <Document title={t('user.activity')} />
            <Section
                backgroundColor="white"
                divider
                style={{ paddingBottom: 0 }}
            >
                <Container size="small">
                    <H1 style={{ marginBottom: '16px' }}>
                        {t('user.activity')}
                    </H1>
                    <TabMenu options={options} />
                </Container>
            </Section>
            <Section>
                <Container size="small">
                    <Outlet />
                </Container>
            </Section>
        </>
    )
}

const GET_ACTIVITY_TOTALS = gql`
    query ActivityListTotals($userGuid: String) {
        published: activities(userGuid: $userGuid) {
            total
        }
        drafts: activities(userGuid: $userGuid, statusPublished: [draft]) {
            total
        }
        archived: activities(userGuid: $userGuid, statusPublished: [archived]) {
            total
        }
    }
`

export default ActivityContainer
