import React from 'react'
import { useParams } from 'react-router-dom'

import FeedView from 'js/page/Widget/components/Feed/FeedView'

const Publications = () => {
    const { userGuid } = useParams()

    const settings = {
        typeFilter: [
            'blog',
            'discussion',
            'event',
            'news',
            'question',
            'wiki',
            'podcast',
        ],
        sortingOptions: ['timePublished', 'title'],
        sortBy: 'timePublished',
        sortDirection: 'desc',
        itemView: 'tile',
        itemCount: 5,
        hideLikes: true,
        hideComments: true,
    }

    return (
        <FeedView
            guid="user-publications"
            settings={settings}
            userGuid={userGuid}
        />
    )
}

export default Publications
