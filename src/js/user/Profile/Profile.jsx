import React from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import { Col, Row } from 'js/components/Grid/Grid'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Spacer from 'js/components/Spacer/Spacer'
import NotFound from 'js/core/NotFound'
import { iconViewFragment } from 'js/lib/fragments/icon'
import ContentListItem from 'js/page/Widget/components/ItemList/ItemListView/components/ContentListItem'

import ProfileListCard from './components/ProfileListCard'
import ProfileSections from './components/ProfileSections'

const Profile = () => {
    const { t } = useTranslation()
    const { userGuid } = useParams()

    const { loading, data } = useQuery(GET_PROFILE, {
        // Prevent lists from showing cached data (when switching between Profile and Publications/Groups)
        // Query needs to be refetched on editProfileField though
        fetchPolicy: 'no-cache',
        variables: {
            userGuid,
            limit: 3,
        },
    })

    if (loading) return <LoadingSpinner />
    if (!data || !data.entity) return <NotFound />

    const { entity, entities, groups } = data

    const { profile } = entity

    const hasPublications = entities.edges.length > 0
    const hasGroups = groups.edges.length > 0

    const showSidebar = hasPublications || hasGroups

    return (
        <>
            <Row $gutter={32}>
                <Col
                    mobileLandscapeUp={showSidebar ? 2 / 3 : 1}
                    tabletUp={2 / 3}
                >
                    {profile && (
                        <ProfileSections user={entity} fields={profile} />
                    )}
                </Col>
                {showSidebar && (
                    <Col mobileLandscapeUp={1 / 3}>
                        <Spacer>
                            {hasPublications && (
                                <ProfileListCard
                                    title={t('profile.publications')}
                                    viewAllLink="../publications"
                                >
                                    <ul>
                                        {entities.edges.map((entity, i) => (
                                            <ContentListItem
                                                key={`publications-${entity.guid}`}
                                                index={i}
                                                entity={entity}
                                            />
                                        ))}
                                    </ul>
                                </ProfileListCard>
                            )}

                            {hasGroups && (
                                <ProfileListCard
                                    title={t('entity-group.title-list')}
                                    viewAllLink="../groups"
                                >
                                    <ul>
                                        {groups.edges.map((entity, i) => (
                                            <ContentListItem
                                                key={`groups-${entity.guid}`}
                                                index={i}
                                                entity={entity}
                                            />
                                        ))}
                                    </ul>
                                </ProfileListCard>
                            )}
                        </Spacer>
                    </Col>
                )}
            </Row>
        </>
    )
}

const entityTemplate = `
title
url
subtype
timePublished
`

const GET_PROFILE = gql`
    query Profile($userGuid: String!, $limit: Int) {
        entity(guid: $userGuid) {
            guid
            ... on User {
                canEdit
                profile {
                    key
                    name
                    autocomplete
                    value
                    category
                    accessId
                    fieldType
                    isEditable
                    fieldOptions
                }
            }
        }
        entities(
            subtypes: ["blog","discussion","event","news","question","wiki","podcast"]
            offset: 0
            limit: $limit
            userGuid: $userGuid
            orderBy: timePublished
        ) {
            total
            edges {
                guid
                __typename
                ... on Blog {
                    ${entityTemplate}
                }
                ... on News {
                    ${entityTemplate}
                }
                ... on Discussion {
                    ${entityTemplate}
                }
                ... on Event {
                    ${entityTemplate}
                }
                ... on Question {
                    ${entityTemplate}
                }
                ... on Wiki {
                    ${entityTemplate}
                }
                ... on Podcast {
                    ${entityTemplate}
                }
            }
        }
        groups(offset: 0, limit: $limit, userGuid: $userGuid) {
            total
            edges {
                guid
                name
                ${iconViewFragment}
                url
            }
        }
    }
`

export default Profile
