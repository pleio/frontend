import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Outlet, useMatch, useNavigate, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import Document from 'js/components/Document'
import Flexer from 'js/components/Flexer/Flexer'
import { Container } from 'js/components/Grid/Grid'
import { H2 } from 'js/components/Heading'
import HideVisually from 'js/components/HideVisually/HideVisually'
import IconButton from 'js/components/IconButton/IconButton'
import Modal from 'js/components/Modal/Modal'
import Section from 'js/components/Section/Section'
import TabMenu from 'js/components/TabMenu'
import NotFound from 'js/core/NotFound'

import CloseIcon from 'icons/cross-large.svg'
import EmailIcon from 'icons/mail.svg'

import EditAvatar from './components/EditAvatar'
import EditName from './components/EditName'
import SendMessage from './components/SendMessage'
import UserChatButton from './components/UserChatButton'
import VideoCall from './VideoCall/VideoCall'

const Wrapper = styled(Section)`
    z-index: 1;

    .UserProfileHeader {
        display: flex;
        align-items: center;
    }

    .UserProfileAvatar {
        flex-shrink: 0;
        position: relative;
        width: 80px;
        height: 80px;
        border-radius: 50%;
        background-color: ${(p) => p.theme.color.grey[20]};
        background-position: center;
        background-size: cover;
        background-repeat: no-repeat;
        margin-right: 16px;

        &:hover a {
            opacity: 1;
        }

        a {
            position: absolute;
            top: 0;
            right: 0;
            opacity: 0;
            transition: opacity ${(p) => p.theme.transition.fast};

            &:focus {
                opacity: 1;
            }
        }
    }
`

const ProfileContainer = () => {
    const { t } = useTranslation()
    const navigate = useNavigate()
    const { userGuid } = useParams()

    const { loading, data } = useQuery(GET_PROFILE_CONTAINER, {
        variables: {
            userGuid,
        },
    })

    const matchReply = useMatch({
        path: `user/${userGuid}/profile/reply`,
        end: true,
    })

    const canSendMessage =
        data?.viewer?.loggedIn &&
        data?.viewer?.user?.guid !== data?.entity?.guid

    const [showAvatarModal, setShowAvatarModal] = useState(false)

    const [showSendMessageModal, setShowSendMessageModal] = useState(false)
    const toggleSendMessageModal = () => {
        setShowSendMessageModal(!showSendMessageModal)
    }

    useEffect(() => {
        if (matchReply && !!canSendMessage) {
            setShowSendMessageModal(true)
        }
    }, [matchReply, canSendMessage])

    if (loading) return null
    if (!data || !data.entity) return <NotFound />

    const { entity, site, entities, groups } = data

    const { guid, name, icon, canEdit } = entity

    const hasPublications = entities.total > 0
    const hasGroups = groups.total > 0

    const showTabmenu = hasPublications || hasGroups

    const options = showTabmenu
        ? [
              {
                  to: `../profile`,
                  label: t('profile.title'),
                  end: true,
              },
              ...(hasPublications
                  ? [
                        {
                            to: `../profile/publications`,
                            label: t('profile.publications'),
                            end: true,
                        },
                    ]
                  : []),
              ...(hasGroups
                  ? [
                        {
                            to: `../profile/groups`,
                            label: t('entity-group.title-list'),
                            end: true,
                        },
                    ]
                  : []),
          ]
        : []

    return (
        <>
            <Document title={name} />
            <Wrapper
                backgroundColor="white"
                noBottomPadding
                divider={showTabmenu}
            >
                <Container size="large">
                    <Flexer
                        justifyContent="space-between"
                        alignItems="flex-end"
                        wrap
                    >
                        <div className="UserProfileHeader">
                            {icon ? (
                                <button
                                    className="UserProfileAvatar"
                                    style={{
                                        backgroundImage: `url(${icon}?size=80)`,
                                    }}
                                    onClick={() => setShowAvatarModal(true)}
                                    aria-label={t('profile.enlarge-picture')}
                                >
                                    {canEdit && <EditAvatar />}
                                </button>
                            ) : (
                                <div className="UserProfileAvatar">
                                    {canEdit && <EditAvatar />}
                                </div>
                            )}

                            <div>
                                {canEdit ? (
                                    <EditName
                                        guid={guid}
                                        name={name}
                                        editUserNameEnabled={
                                            site?.editUserNameEnabled
                                        }
                                    />
                                ) : (
                                    <H2 as="h1">{name}</H2>
                                )}
                            </div>
                        </div>

                        <Flexer>
                            {canSendMessage && (
                                <>
                                    {site?.chatEnabled && (
                                        <UserChatButton entity={entity} />
                                    )}

                                    <Button
                                        size="normal"
                                        variant="secondary"
                                        type="button"
                                        onClick={toggleSendMessageModal}
                                    >
                                        <EmailIcon
                                            style={{ margin: '0 8px 0 -2px' }}
                                        />
                                        {t('global.send-message')}
                                    </Button>
                                    <SendMessage
                                        entity={entity}
                                        isVisible={showSendMessageModal}
                                        onClose={() => {
                                            navigate('../profile', {
                                                replace: true,
                                            })
                                            setShowSendMessageModal(false)
                                        }}
                                    />
                                </>
                            )}

                            {site.videocallProfilepage && (
                                <VideoCall guid={guid} name={name} />
                            )}
                        </Flexer>
                    </Flexer>

                    <TabMenu options={options} style={{ marginTop: 16 }} />
                </Container>
            </Wrapper>

            <Section backgroundColor="white" grow style={{ paddingTop: 24 }}>
                <Container size="large">
                    <Outlet />
                </Container>
            </Section>

            <Modal
                isVisible={showAvatarModal}
                onClose={() => setShowAvatarModal(false)}
                maxWidth="600px"
                size="normal"
                hideBackground
                noPadding
            >
                <div
                    style={{
                        display: 'flex',
                        justifyContent: 'center',
                        width: '100%',
                    }}
                >
                    <div
                        style={{
                            position: 'relative',
                        }}
                    >
                        <IconButton
                            variant="secondary"
                            size="large"
                            dropShadow
                            onClick={() => setShowAvatarModal(false)}
                            aria-label={t('profile.close-picture')}
                            style={{
                                position: 'absolute',
                                top: '8px',
                                right: '8px',
                            }}
                        >
                            <CloseIcon />
                        </IconButton>
                        <HideVisually as="h2">
                            {t('profile.picture-of', {
                                name,
                            })}
                        </HideVisually>
                        <img src={`${icon}?size=600`} alt={name} />
                    </div>
                </div>
            </Modal>
        </>
    )
}

const GET_PROFILE_CONTAINER = gql`
    query ProfileContainer($userGuid: String!) {
        site {
            guid
            editUserNameEnabled
            videocallProfilepage
            chatEnabled
        }
        viewer {
            guid
            loggedIn
            user {
                guid
            }
        }
        entity(guid: $userGuid) {
            guid
            ... on User {
                name
                canEdit
                icon
            }
        }
        entities(
            subtypes: [
                "blog"
                "discussion"
                "event"
                "news"
                "question"
                "wiki"
                "podcast"
            ]
            offset: 0
            limit: 0
            userGuid: $userGuid
        ) {
            total
        }
        groups(offset: 0, limit: 0, userGuid: $userGuid) {
            total
        }
    }
`

export default ProfileContainer
