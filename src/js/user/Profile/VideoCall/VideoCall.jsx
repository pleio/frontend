import React, { useState } from 'react'
import { Trans, useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import Modal from 'js/components/Modal/Modal'

import CameraIcon from 'icons/camera.svg'

import VideoCallDetails from './VideoCallDetails'

const VideoCall = ({ guid, name }) => {
    const { t } = useTranslation()

    const [showModal, setShowModal] = useState(false)

    const [initiateVideoCall, mutationResponse] = useMutation(
        INITIATE_VIDEO_CALL,
        { fetchPolicy: 'no-cache' },
    )

    const handleInitiateVideoCall = () => {
        initiateVideoCall({ variables: { userGuid: guid } })
    }

    return (
        <>
            <Button
                size="normal"
                variant="secondary"
                type="button"
                onClick={() => setShowModal(true)}
            >
                <CameraIcon style={{ margin: '0 8px 0 -2px' }} />
                {t('global.video-call')}
            </Button>

            <Modal
                title={t('global.video-call')}
                isVisible={showModal}
                size="small"
            >
                {mutationResponse?.called ? (
                    <VideoCallDetails
                        mutationResponse={mutationResponse}
                        closeModal={() => setShowModal(false)}
                        guestName={name}
                        guid={guid}
                    />
                ) : (
                    <>
                        <Trans i18nKey="video-call.invite">
                            Invite <strong>{{ name }}</strong> to a video call.
                            A link will be sent that is valid for 24 hours.
                        </Trans>

                        <Flexer mt>
                            <Button
                                size="normal"
                                variant="secondary"
                                style={{ marginRight: '8px' }}
                                onClick={() => setShowModal(false)}
                            >
                                {t('action.cancel')}
                            </Button>
                            <Button
                                size="normal"
                                variant="primary"
                                onClick={handleInitiateVideoCall}
                            >
                                {t('action.invite')}
                            </Button>
                        </Flexer>
                    </>
                )}
            </Modal>
        </>
    )
}

const INITIATE_VIDEO_CALL = gql`
    mutation InitiateVideoCall($userGuid: String!) {
        initiateVideocall(userGuid: $userGuid) {
            success
            hostUrl
            guestUrl
        }
    }
`

export default VideoCall
