import React from 'react'
import { Trans, useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'

import Button from 'js/components/Button/Button'
import ClipboardCopy from 'js/components/ClipboardCopy/ClipboardCopy'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Text from 'js/components/Text/Text'

const VideoCallDetails = ({
    mutationResponse,
    closeModal,
    guestName,
    guid,
}) => {
    const { t } = useTranslation()

    const { data, loading, error } = mutationResponse

    if (loading) return <LoadingSpinner />

    const videocallDataSuccessful =
        !error &&
        data?.initiateVideocall?.success &&
        data?.initiateVideocall?.hostUrl &&
        data?.initiateVideocall?.guestUrl

    return (
        <>
            {videocallDataSuccessful ? (
                <Text>
                    <p>
                        <Trans i18nKey="video-call.guest-link">
                            The guest link has been sent to
                            <strong>{{ name: guestName }}</strong>. You can copy
                            the link to share it somewhere else:
                        </Trans>
                    </p>
                    <ClipboardCopy
                        copyText={data.initiateVideocall.guestUrl}
                        style={{ marginTop: '16px' }}
                    />

                    <p style={{ marginTop: '24px' }}>
                        <Trans i18nKey="video-call.host-link">
                            You can find the host link in your
                            <Link to={`/user/${guid}/notifications`}>
                                notifications
                            </Link>
                            . Copy it here to use it right away:
                        </Trans>
                    </p>
                    <ClipboardCopy
                        copyText={data.initiateVideocall.hostUrl}
                        style={{ marginTop: '16px' }}
                    />
                </Text>
            ) : (
                <Errors errors={error} />
            )}
            <Flexer mt>
                <Button onClick={closeModal}>{t('action.ok')}</Button>
            </Flexer>
        </>
    )
}

export default VideoCallDetails
