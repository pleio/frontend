import React, { useState } from 'react'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import FetchMore from 'js/components/FetchMore'
import { Col, Row } from 'js/components/Grid/Grid'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import listFragment from 'js/group/fragments/listFragment'
import Card from 'js/group/List/components/Card'

const Groups = () => {
    const { userGuid } = useParams()

    const [queryLimit, setQueryLimit] = useState(20)

    const { loading, data, fetchMore } = useQuery(GET_USER_GROUPS, {
        variables: {
            userGuid,
            offset: 0,
            limit: queryLimit,
        },
    })

    return (
        <>
            {loading ? (
                <LoadingSpinner />
            ) : (
                <FetchMore
                    edges={data.groups.edges}
                    getMoreResults={(data) => data.groups.edges}
                    fetchMore={fetchMore}
                    fetchCount={20}
                    setLimit={setQueryLimit}
                    maxLimit={data.groups.total}
                >
                    <Row $growContent>
                        {data.groups.edges.map((group) => (
                            <Col mobileLandscapeUp={1 / 2} key={group.guid}>
                                <Card entity={group} />
                            </Col>
                        ))}
                    </Row>
                </FetchMore>
            )}
        </>
    )
}

const GET_USER_GROUPS = gql`
    query UserGroups($offset: Int, $limit: Int, $userGuid: String) {
        groups(offset: $offset, limit: $limit, userGuid: $userGuid) {
            total
            edges {
                ${listFragment}
            }
        }
    }
`

export default Groups
