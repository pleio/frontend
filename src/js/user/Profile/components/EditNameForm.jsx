import React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'

const EditNameForm = ({ mutate, guid, name, toggleEditName }) => {
    const { t } = useTranslation()

    const submit = async ({ name }) => {
        await mutate({
            variables: {
                input: {
                    guid,
                    name,
                },
            },
        })
            .then(() => {
                toggleEditName()
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    const defaultValues = {
        name,
    }

    const {
        control,
        handleSubmit,
        formState: { errors, isDirty, isValid, isSubmitting },
    } = useForm({ mode: 'onChange', defaultValues })

    return (
        <form
            onSubmit={handleSubmit(submit)}
            noValidate
            style={{
                display: 'flex',
                alignItems: 'center',
                flexWrap: 'wrap',
                marginRight: '-12px',
            }}
        >
            <FormItem
                type="text"
                control={control}
                name="name"
                label={t('global.name')}
                errors={errors}
                required
                rules={{
                    maxLength: {
                        value: 100,
                        message: t('error.user-name-max-length'),
                    },
                }}
                style={{ marginRight: '12px' }}
            />
            <Flexer justifyContent="flex-start">
                <Button
                    size="normal"
                    variant="tertiary"
                    type="button"
                    onClick={toggleEditName}
                >
                    {t('action.cancel')}
                </Button>
                <Button
                    size="normal"
                    variant="primary"
                    type="submit"
                    disabled={!isValid || !isDirty}
                    loading={isSubmitting}
                >
                    {t('action.save')}
                </Button>
            </Flexer>
        </form>
    )
}

const Mutation = gql`
    mutation EditUserName($input: editUserNameInput!) {
        editUserName(input: $input) {
            user {
                guid
                name
            }
        }
    }
`

export default graphql(Mutation)(EditNameForm)
