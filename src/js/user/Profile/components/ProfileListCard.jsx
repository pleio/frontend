import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import Card, { CardContent } from 'js/components/Card/Card'
import Flexer from 'js/components/Flexer/Flexer'
import { H4 } from 'js/components/Heading'

const Wrapper = styled(Card)`
    padding-bottom: 10px;
`

const ProfileListCard = ({ title, viewAllLink, children, ...rest }) => {
    const { t } = useTranslation()

    return (
        <Wrapper radiusSize="large" {...rest}>
            <CardContent style={{ paddingBottom: 4 }}>
                <H4 as="h2">{title}</H4>
            </CardContent>
            {children}
            {viewAllLink && (
                <Flexer justifyContent="center">
                    <Button
                        variant="tertiary"
                        outline={false}
                        size="normal"
                        as={Link}
                        to={viewAllLink}
                    >
                        {t('action.view-all')}
                    </Button>
                </Flexer>
            )}
        </Wrapper>
    )
}

export default ProfileListCard
