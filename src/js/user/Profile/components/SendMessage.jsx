import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { getQueryVariable } from 'helpers'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import Modal from 'js/components/Modal/Modal'
import Spacer from 'js/components/Spacer/Spacer'
import Text from 'js/components/Text/Text'

const SendMessage = ({ mutate, entity, isVisible, onClose }) => {
    const [messageSent, setMessageSent] = useState(false)

    const onSubmit = async ({ subject, message, sendCopyToSender }) => {
        await mutate({
            variables: {
                input: {
                    guid: entity.guid,
                    subject,
                    message,
                    sendCopyToSender,
                },
            },
        })
            .then(() => {
                setMessageSent(true)
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    const handleClose = () => {
        setMessageSent(false)
        onClose()
    }

    const { t } = useTranslation()
    const { name } = entity

    const onError = (errors) => {
        return new Promise((resolve, reject) => {
            reject(new Error(errors))
        })
    }

    const subject = getQueryVariable('subject')

    const defaultValues = {
        subject,
        message: '',
        sendCopyToSender: true,
    }

    const {
        control,
        handleSubmit,
        formState: { errors, isValid, isSubmitting },
    } = useForm({
        mode: 'onChange',
        defaultValues,
    })

    return (
        <Modal
            size="small"
            isVisible={isVisible}
            onClose={onClose}
            title={t('global.send-message')}
            containHeight
        >
            {messageSent ? (
                <>
                    <Text>{t('global.message-sent')}</Text>
                    <Flexer mt>
                        <Button
                            size="normal"
                            variant="primary"
                            onClick={handleClose}
                        >
                            {t('action.ok')}
                        </Button>
                    </Flexer>
                </>
            ) : (
                <Spacer
                    as="form"
                    spacing="small"
                    onSubmit={handleSubmit(onSubmit, onError)}
                    style={{
                        display: 'flex',
                        flexDirection: 'column',
                        overflow: 'hidden',
                    }}
                >
                    <Text as="p" size="small">
                        {t('profile.send-message-description', { name })}
                    </Text>

                    <FormItem
                        type="text"
                        control={control}
                        name="subject"
                        label={t('global.subject')}
                        required
                        errors={errors}
                    />

                    <FormItem
                        control={control}
                        type="rich"
                        name="message"
                        placeholder={t('form.write-message')}
                        options={{
                            textFormat: true,
                            textStyle: true,
                        }}
                        required
                        errors={errors}
                        contentType="html"
                        style={{ overflowY: 'auto' }}
                    />

                    <FormItem
                        type="switch"
                        control={control}
                        name="sendCopyToSender"
                        label={t('global.send-message-copy')}
                        size="small"
                    />

                    <Flexer mt>
                        <Button
                            size="normal"
                            variant="tertiary"
                            type="button"
                            onClick={onClose}
                        >
                            {t('action.cancel')}
                        </Button>
                        <Button
                            size="normal"
                            variant="primary"
                            type="submit"
                            disabled={!isValid}
                            loading={isSubmitting}
                        >
                            {t('action.send')}
                        </Button>
                    </Flexer>
                </Spacer>
            )}
        </Modal>
    )
}

const Mutation = gql`
    mutation SendMessageModal($input: sendMessageToUserInput!) {
        sendMessageToUser(input: $input) {
            success
        }
    }
`

export default graphql(Mutation)(SendMessage)
