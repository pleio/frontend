import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'

import Flexer from 'js/components/Flexer/Flexer'
import { H2 } from 'js/components/Heading'
import IconButton from 'js/components/IconButton/IconButton'

import EditIcon from 'icons/edit-small.svg'

import EditNameForm from './EditNameForm'

const EditName = ({ guid, name, editUserNameEnabled }) => {
    const { t } = useTranslation()

    const [isEditing, setIsEditing] = useState(false)
    const toggleEditName = () => {
        setIsEditing(!isEditing)
    }

    if (isEditing) {
        return (
            <EditNameForm
                guid={guid}
                name={name}
                toggleEditName={toggleEditName}
            />
        )
    }

    const buttonProps = {
        size: 'normal',
        variant: 'secondary',
        radiusStyle: 'rounded',
        tooltip: t('profile.edit-name'),
        placement: 'right',
    }

    return (
        <Flexer gutter="tiny">
            <H2 as="h1">{name}</H2>
            {editUserNameEnabled ? (
                <IconButton {...buttonProps} onClick={toggleEditName}>
                    <EditIcon />
                </IconButton>
            ) : (
                <IconButton
                    {...buttonProps}
                    as="a"
                    href="https://account.pleio.nl/profile/"
                    target="_blank"
                    rel="noopener noreferrer"
                    aria-label={`${t('profile.edit-name')}${t(
                        'global.opens-in-new-window',
                    )}`}
                >
                    <EditIcon />
                </IconButton>
            )}
        </Flexer>
    )
}

export default EditName
