import React, { useContext } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import chatFragment from 'js/chat/fragments/chat'
import Button from 'js/components/Button/Button'
import globalStateContext from 'js/lib/withGlobalState/globalStateContext'

import ChatIcon from 'icons/chat.svg'

const UserChatButton = ({ mutate, entity }) => {
    const { setGlobalState } = useContext(globalStateContext)

    const onSubmit = () => {
        mutate({
            variables: {
                input: {
                    userGuids: [entity.guid],
                },
            },
        })
            .then(
                ({
                    data: {
                        addChat: { chat },
                    },
                }) => {
                    setGlobalState((newState) => {
                        newState.activeMainThread = chat
                        newState.chatOverlayExpanded = true
                    })
                },
            )
            .catch((errors) => {
                console.error(errors)
            })
    }

    const { t } = useTranslation()

    return (
        <Button size="normal" variant="secondary" onClick={onSubmit}>
            <ChatIcon style={{ margin: '0 8px 0 -2px' }} />
            {t('chat.open-chat')}
        </Button>
    )
}

const ADD_CHAT = gql`
    mutation AddChat($input: addChatInput!) {
        addChat(input: $input) {
            chat {
               ${chatFragment}
            }
        }
    }
`

export default graphql(ADD_CHAT)(UserChatButton)
