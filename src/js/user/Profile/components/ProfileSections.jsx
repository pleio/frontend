/*
Component is used in user profile and group container.
Props 'user' and 'fields' are separate because of this.
*/

import React from 'react'
import { useTranslation } from 'react-i18next'
import { partitionArray } from 'helpers'

import Spacer from 'js/components/Spacer/Spacer'

import ProfileSection from '../ProfileSection/ProfileSection'

const ProfileSections = ({
    user,
    fields,
    isEditingDefault,
    allFieldsRequired = false,
    ...rest
}) => {
    const { canEdit } = user

    const { t } = useTranslation()
    const [fieldsWithoutSection, fieldsWithSection] = partitionArray(
        fields,
        (field) => !field.category,
    )

    const sections = [
        // Filter duplicates..
        ...new Set(
            fields
                // ..of all field categories..
                .map((field) => field.category)
                // ..that are not null
                .filter((category) => !!category),
        ),
    ]

    return (
        <Spacer {...rest}>
            <ProfileSection
                user={user}
                title={t('profile.section-general')}
                profileFields={fieldsWithoutSection}
                canEdit={canEdit}
                isEditingDefault={isEditingDefault}
                allFieldsRequired={allFieldsRequired}
            />

            {sections.map((section, i) => (
                <ProfileSection
                    key={`${section}-${i}`}
                    user={user}
                    title={section}
                    profileFields={fieldsWithSection.filter(
                        (field) => section === field.category,
                    )}
                    canEdit={canEdit}
                    isEditingDefault={isEditingDefault}
                    allFieldsRequired={allFieldsRequired}
                />
            ))}
        </Spacer>
    )
}

export default ProfileSections
