import React from 'react'
import { useTranslation } from 'react-i18next'

import IconButton from 'js/components/IconButton/IconButton'

import EditIcon from 'icons/edit-small.svg'

const EditAvatar = () => {
    const { t } = useTranslation()

    return (
        <IconButton
            size="small"
            variant="secondary"
            dropShadow
            as="a"
            href="https://account.pleio.nl/profile/"
            target="_blank"
            rel="noopener noreferrer"
            aria-label={`${t('profile.edit-user-picture')}${t(
                'global.opens-in-new-window',
            )}`}
            tooltip={t('profile.edit-picture')}
            placement="right"
            onClick={(e) => e.stopPropagation()}
        >
            <EditIcon />
        </IconButton>
    )
}

export default EditAvatar
