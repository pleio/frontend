import React from 'react'
import { useFieldArray, useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'
import { format, formatISO } from 'date-fns'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import Spacer from 'js/components/Spacer/Spacer'
import ProfileField from 'js/user/ProfileField/ProfileField'

function setProfileErrors(fieldErrors, setError) {
    if (!fieldErrors) return

    fieldErrors.forEach((fieldError) => {
        const { key, message } = fieldError
        setError(key, {
            type: 'manual',
            message,
        })
    })
}

const ProfileSection = ({
    userGuid,
    profileFields,
    toggleIsEditing,
    allFieldsRequired,
}) => {
    const { t } = useTranslation()

    const [editProfileField] = useMutation(EDIT_PROFILE_FIELDS)

    const submit = async ({ fields }) => {
        const fieldsValue = fields.map(
            ({ key, fieldType, value, accessId }) => {
                let newValue = value

                if (fieldType === 'multiSelectField') {
                    newValue = value.join(',')
                } else if (fieldType === 'dateField') {
                    newValue = value ? format(value, 'yyyy-MM-dd') : ''
                }

                return {
                    key,
                    value: newValue,
                    accessId,
                }
            },
        )

        await editProfileField({
            variables: {
                input: {
                    guid: userGuid,
                    fields: fieldsValue,
                },
            },
            refetchQueries: ['Profile', 'GroupContainer'],
        })
            .then(({ data }) => {
                const success = data?.editProfileField?.success

                if (success) {
                    toggleIsEditing()
                } else {
                    setProfileErrors(data?.editProfileField?.fields, setError)
                }
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    const {
        control,
        handleSubmit,
        setError,
        clearErrors,
        setValue,
        watch,
        formState: { errors, isValid, isSubmitting },
    } = useForm({
        mode: 'onChange',
        defaultValues: {
            fields: profileFields.map((field) => {
                const newField = { ...field }
                const { fieldType, value } = field
                if (fieldType === 'multiSelectField') {
                    newField.value = value ? value.split(',') : []
                } else if (field.fieldType === 'dateField') {
                    newField.value = value ? formatISO(value) : null
                }
                return newField
            }),
        },
    })

    const { fields } = useFieldArray({
        control,
        name: 'fields',
    })

    const submitForm = handleSubmit(submit)

    const onSubmit = () => {
        clearErrors()
        submitForm()
    }

    return (
        <form onSubmit={submitForm} autoComplete={'on'}>
            <Spacer spacing="tiny">
                {fields.map((field, index) => (
                    <ProfileField
                        key={field.key}
                        index={index}
                        control={control}
                        errors={errors}
                        setValue={setValue}
                        watch={watch}
                        field={field}
                        isEditingSection
                        required={allFieldsRequired}
                    />
                ))}
            </Spacer>
            <Flexer mt>
                <Button
                    size="normal"
                    variant="tertiary"
                    onClick={() => {
                        toggleIsEditing()
                    }}
                >
                    {t('action.cancel')}
                </Button>
                <Button
                    size="normal"
                    variant="primary"
                    disabled={!isValid}
                    loading={isSubmitting}
                    onClick={onSubmit}
                >
                    {t('action.save')}
                </Button>
            </Flexer>
        </form>
    )
}

const EDIT_PROFILE_FIELDS = gql`
    mutation EditProfileField($input: editProfileFieldInput!) {
        editProfileField(input: $input) {
            success
            fields {
                key
                message
            }
            user {
                guid
                ... on User {
                    name
                    canEdit
                    icon
                    profile {
                        key
                        name
                        value
                        category
                        accessId
                        fieldType
                        isEditable
                        fieldOptions
                    }
                }
            }
        }
    }
`

export default ProfileSection
