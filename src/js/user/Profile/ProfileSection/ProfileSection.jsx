import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import Flexer from 'js/components/Flexer/Flexer'
import { H4 } from 'js/components/Heading'
import IconButton from 'js/components/IconButton/IconButton'
import SettingCard from 'js/components/SettingCard'
import Spacer from 'js/components/Spacer/Spacer'
import ProfileField from 'js/user/ProfileField/ProfileField'

import EditIcon from 'icons/edit-small.svg'

import ProfileSectionForm from './ProfileSectionForm'

const Wrapper = styled(SettingCard)`
    position: relative;
    font-size: ${(p) => p.theme.font.size.small};
    line-height: ${(p) => p.theme.font.lineHeight.small};

    .ProfileSectionTitleWrapper {
        margin-bottom: 4px;
        min-height: 32px; // Prevents jumping when toggling edit mode
    }
`

const ProfileSection = ({
    user,
    title,
    profileFields,
    canEdit,
    isEditingDefault = false,
    allFieldsRequired = false,
    ...rest
}) => {
    const userGuid = user.guid

    const { t } = useTranslation()

    const [isEditing, setIsEditing] = useState(isEditingDefault)
    const toggleIsEditing = () => setIsEditing(!isEditing)

    if (profileFields?.length === 0) {
        return null
    }

    return (
        <Wrapper radiusSize="large" {...rest}>
            <Flexer
                justifyContent="space-between"
                alignItems="flex-start"
                className="ProfileSectionTitleWrapper"
            >
                <H4 as="h2">{title}</H4>
                {!isEditing && canEdit && (
                    <IconButton
                        size="normal"
                        variant="secondary"
                        radiusStyle="rounded"
                        onClick={toggleIsEditing}
                        tooltip={t('profile.edit-section')}
                        className="ProfileSectionEditButton"
                    >
                        <EditIcon />
                    </IconButton>
                )}
            </Flexer>

            {isEditing && canEdit ? (
                // Edit
                <ProfileSectionForm
                    userGuid={userGuid}
                    profileFields={profileFields}
                    toggleIsEditing={toggleIsEditing}
                    allFieldsRequired={allFieldsRequired}
                />
            ) : (
                // View
                <Spacer spacing="tiny">
                    {profileFields.map((field) => (
                        <ProfileField key={field.key} field={field} />
                    ))}
                </Spacer>
            )}
        </Wrapper>
    )
}

export default ProfileSection
