import React, { useState } from 'react'
import { Trans, useTranslation } from 'react-i18next'
import { Link, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import Document from 'js/components/Document'
import { Container } from 'js/components/Grid/Grid'
import { H1 } from 'js/components/Heading'
import Section from 'js/components/Section/Section'
import Text from 'js/components/Text/Text'
import NotFound from 'js/core/NotFound'
import FilesView from 'js/files/FilesView'

const Files = () => {
    const { userGuid, containerGuid } = useParams()
    const { t } = useTranslation()

    const { loading, data } = useQuery(GET_FILES_CONTAINER, {
        variables: {
            containerGuid,
            userGuid,
        },
    })

    const [selected, setSelected] = useState([])

    if (loading) return null

    if (containerGuid && data?.containerEntity === null) {
        return <NotFound />
    }

    const myFiles = data?.viewer?.user?.guid === userGuid

    return (
        <>
            <Document
                title={data?.containerEntity?.title}
                containerTitle={
                    myFiles ? t('user.my-files') : t('global.files')
                }
            />
            <Section backgroundColor="white" grow>
                <Container>
                    {myFiles ? (
                        <H1>{t('user.my-files')}</H1>
                    ) : (
                        <>
                            <H1>{t('global.files')}</H1>
                            <Text size="small" variant="grey">
                                <Trans i18nKey="settings.other-user">
                                    {{ name: data?.userEntity?.name }}
                                    <Link to={`/user/${userGuid}/profile`}>
                                        View profile
                                    </Link>
                                </Trans>
                            </Text>
                        </>
                    )}
                </Container>
                <Container $noPadding>
                    <FilesView
                        key={containerGuid}
                        rootContainerGuid={userGuid}
                        selected={selected}
                        setSelected={setSelected}
                        selectable="multiple"
                        canCreateAndEdit
                    />
                </Container>
            </Section>
        </>
    )
}

const GET_FILES_CONTAINER = gql`
    query FilesContainer($containerGuid: String, $userGuid: String) {
        viewer {
            guid
            user {
                guid
            }
        }
        userEntity: entity(guid: $userGuid) {
            guid
            ... on User {
                guid
                name
                canEdit
            }
        }
        containerEntity: entity(guid: $containerGuid) {
            guid
            ... on Folder {
                title
            }
        }
    }
`

export default Files
