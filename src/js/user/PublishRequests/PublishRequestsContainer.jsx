import React from 'react'
import { useTranslation } from 'react-i18next'
import { Outlet, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import Document from 'js/components/Document'
import { Container } from 'js/components/Grid/Grid'
import { H1 } from 'js/components/Heading'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Section from 'js/components/Section/Section'
import TabMenu from 'js/components/TabMenu'

const PublishRequestsContainer = () => {
    const { t } = useTranslation()

    const { userGuid } = useParams()

    const { data, loading } = useQuery(GET_PUBLISH_REQUESTS_TOTALS, {
        variables: {
            userGuid,
        },
    })

    if (loading) return <LoadingSpinner />

    const { site, content, files, comments } = data
    const {
        contentModerationEnabled,
        requireContentModerationFor,
        commentModerationEnabled,
        requireCommentModerationFor,
    } = site

    const options = [
        ...(contentModerationEnabled &&
        requireContentModerationFor.filter((t) => t !== 'file').length > 0
            ? [
                  {
                      to: `../publish-requests/content`,
                      label: `${t(
                          'content-moderation.status.content',
                      )} (${content?.total || 0})`,
                      end: true,
                  },
              ]
            : []),
        ...(contentModerationEnabled &&
        requireContentModerationFor.includes('file')
            ? [
                  {
                      to: `../publish-requests/files`,
                      label: `${t(
                          'content-moderation.status.files',
                      )} (${files?.total || 0})`,
                      end: true,
                  },
              ]
            : []),
        ...(commentModerationEnabled && requireCommentModerationFor.length > 0
            ? [
                  {
                      to: `../publish-requests/comments`,
                      label: `${t(
                          'content-moderation.status.comments',
                      )} (${comments?.total || 0})`,
                      end: true,
                  },
              ]
            : []),
    ]

    return (
        <>
            <Document title={t('content-moderation.publish-requests')} />
            <Section
                backgroundColor="white"
                divider
                style={{ paddingBottom: 0 }}
            >
                <Container size="small">
                    <H1 style={{ marginBottom: '16px' }}>
                        {t('content-moderation.publish-requests')}
                    </H1>
                    <TabMenu options={options} />
                </Container>
            </Section>
            <Section>
                <Container size="small">
                    <Outlet />
                </Container>
            </Section>
        </>
    )
}

const GET_PUBLISH_REQUESTS_TOTALS = gql`
    query UserPublishRequestsTotals($userGuid: String) {
        site {
            guid
            contentModerationEnabled
            requireContentModerationFor
            commentModerationEnabled
            requireCommentModerationFor
        }
        content: publishRequests(userGuid: $userGuid) {
            total
        }
        files: publishRequests(userGuid: $userGuid, files: true) {
            total
        }
        comments: commentModerationRequests(userGuid: $userGuid) {
            total
        }
    }
`

export default PublishRequestsContainer
