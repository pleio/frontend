import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import Card from 'js/activity/components/Card'
import FetchMore from 'js/components/FetchMore'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import NoResultsMessage from 'js/components/NoResultsMessage/NoResultsMessage'
import NotFound from 'js/core/NotFound'

const PublishRequestsFiles = () => {
    const { t } = useTranslation()
    const { userGuid } = useParams()

    const inititalQueryLimit = 10
    const [queryLimit, setQueryLimit] = useState(inititalQueryLimit)

    const variables = {
        offset: 0,
        limit: queryLimit,
        userGuid,
    }

    const { loading, data, fetchMore } = useQuery(GET_PUBLISH_REQUESTS, {
        variables,
    })

    if (loading) return <LoadingSpinner />

    const { site, publishRequests } = data

    if (
        !site.contentModerationEnabled ||
        !site.requireContentModerationFor.includes('file')
    )
        return <NotFound />

    return (
        <FetchMore
            edges={publishRequests.edges}
            getMoreResults={(data) => data.publishRequests.edges}
            fetchMore={fetchMore}
            fetchCount={inititalQueryLimit}
            setLimit={setQueryLimit}
            maxLimit={publishRequests.total}
            resultsMessage={t('global.count-items', {
                count: publishRequests.edges.length,
            })}
            noResults={
                <NoResultsMessage
                    title={t('widget-feed.no-results')}
                    subtitle={t('widget-feed.no-results-helper')}
                />
            }
        >
            <ul>
                {publishRequests.edges.map((entity, i) => (
                    <li key={entity.guid}>
                        <Card data-feed={i} entity={entity} />
                    </li>
                ))}
            </ul>
        </FetchMore>
    )
}

const GET_PUBLISH_REQUESTS = gql`
    query GetPublishRequests($offset: Int, $limit: Int, $userGuid: String) {
        site {
            guid
            contentModerationEnabled
            requireContentModerationFor
        }
        publishRequests(
            userGuid: $userGuid
            offset: $offset
            limit: $limit
            files: true
        ) {
            total
            edges {
                guid
                entity {
                    ... on File {
                        guid
                        subtype
                        title
                        url
                        excerpt
                        localExcerpt
                        isTranslationEnabled
                    }
                }
            }
        }
    }
`

export default PublishRequestsFiles
