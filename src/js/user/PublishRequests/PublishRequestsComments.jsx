import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import styled from 'styled-components'

import DisplayDate from 'js/components/DisplayDate'
import FeedItem from 'js/components/FeedItem/FeedItem'
import FeedItemContent from 'js/components/FeedItem/FeedItemContent'
import FetchMore from 'js/components/FetchMore'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import NoResultsMessage from 'js/components/NoResultsMessage/NoResultsMessage'
import ShowMore from 'js/components/ShowMore'
import Text from 'js/components/Text/Text'
import TiptapView from 'js/components/Tiptap/TiptapView'
import NotFound from 'js/core/NotFound'
import { commentOwnerFragment, ownerFragment } from 'js/lib/fragments/owner'

const PublishRequestsComments = () => {
    const { t } = useTranslation()
    const { userGuid } = useParams()

    const inititalQueryLimit = 10
    const [queryLimit, setQueryLimit] = useState(inititalQueryLimit)

    const variables = {
        offset: 0,
        limit: queryLimit,
        userGuid,
    }

    const { loading, data, fetchMore } = useQuery(GET_PUBLISH_REQUESTS, {
        variables,
    })

    if (loading) return <LoadingSpinner />

    const { site, commentModerationRequests } = data

    if (
        !site.commentModerationEnabled ||
        site.requireCommentModerationFor?.length === 0
    )
        return <NotFound />

    return (
        <FetchMore
            edges={commentModerationRequests.edges}
            getMoreResults={(data) => data.commentModerationRequests.edges}
            fetchMore={fetchMore}
            fetchCount={inititalQueryLimit}
            setLimit={setQueryLimit}
            maxLimit={commentModerationRequests.total}
            resultsMessage={t('global.count-items', {
                count: commentModerationRequests.edges.length,
            })}
            noResults={
                <NoResultsMessage
                    title={t('widget-feed.no-results')}
                    subtitle={t('widget-feed.no-results-helper')}
                />
            }
        >
            <ul>
                {commentModerationRequests.edges.map((entity, i) => (
                    <li key={`${entity.guid}-${i}`}>
                        <CommentsCard data-feed={i} entity={entity} />
                    </li>
                ))}
            </ul>
        </FetchMore>
    )
}

const CommentWrapper = styled(FeedItem)`
    .CommentsCardList {
        border-top: 1px solid ${(p) => p.theme.color.grey[30]};
    }

    .CommentsCardListItem {
        padding: 20px;

        &:not(:last-child) {
            border-bottom: 1px solid ${(p) => p.theme.color.grey[20]};
        }
    }
`

const CommentsCard = ({ entity }) => {
    return (
        <CommentWrapper>
            <FeedItemContent entity={entity.entity} />
            <ul className="CommentsCardList">
                {entity.comments.map(
                    ({ guid, timeCreated, richDescription }) => (
                        <li key={guid} className="CommentsCardListItem">
                            <Text size="tiny" variant="grey">
                                <DisplayDate
                                    date={timeCreated}
                                    type="timeSince"
                                    placement="right"
                                />
                            </Text>
                            <ShowMore maxHeight={200}>
                                <TiptapView content={richDescription} />
                            </ShowMore>
                        </li>
                    ),
                )}
            </ul>
        </CommentWrapper>
    )
}

const entityFragment = `
    guid
    subtype
    title
    url
    ${ownerFragment}
`

const GET_PUBLISH_REQUESTS = gql`
    query GetPublishRequests(
        $offset: Int
        $limit: Int
        $status: CommentModerationStatusEnum
    ) {
          site {
            guid
            commentModerationEnabled
            requireCommentModerationFor
        }
        commentModerationRequests(offset: $offset, limit: $limit, status: $status) {
            total
            edges {
                guid
                entity {
                    ... on Blog {
                        ${entityFragment}
                    }
                    ... on Discussion {
                        ${entityFragment}
                    }
                    ... on Episode {
                        ${entityFragment}
                    }
                    ... on Event {
                        ${entityFragment}
                    }
                    ... on News {
                        ${entityFragment}
                    }
                    ... on Podcast {
                        ${entityFragment}
                    }
                    ... on Question {
                        ${entityFragment}
                    }
                    ... on Wiki {
                        ${entityFragment}
                    }
                }
                comments {
                    guid
                    description
                    excerpt
                    richDescription
                    timeCreated
                    ${commentOwnerFragment}
                    pendingModeration
                }
            }
        }
    }
`

export default PublishRequestsComments
