import styled from 'styled-components'

import LockIcon from 'icons/lock.svg'

export default styled(LockIcon)`
    margin-left: 8px;
    color: ${(p) => p.theme.color.icon.grey};
`
