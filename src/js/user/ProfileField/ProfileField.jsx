import React from 'react'
import { useTranslation } from 'react-i18next'
import { formatISO, subYears } from 'date-fns'
import { media } from 'helpers'
import { showShortDate } from 'helpers/date/showDate'
import styled from 'styled-components'

import DropdownButton from 'js/components/DropdownButton/DropdownButton'
import Error from 'js/components/ErrorStyles'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import IconButton from 'js/components/IconButton/IconButton'
import LinkItem from 'js/components/LinkItem/LinkItem'
import ShowMore from 'js/components/ShowMore'
import TiptapView from 'js/components/Tiptap/TiptapView'
import Tooltip from 'js/components/Tooltip/Tooltip'

import IconPublic from 'icons/earth.svg'
import IconPrivate from 'icons/eye-off.svg'
import LockIcon from 'icons/lock.svg'
import IconUsers from 'icons/users.svg'

const Wrapper = styled.div`
    ${media.mobileLandscapeUp`
        display: flex;
        align-items: flex-start;

        .ProfileFieldLabel {
            width: 230px;
        }
    `};

    .ProfileFieldLabel {
        flex-shrink: 0;
        display: flex;
        align-items: center;
        padding: 10px 0;
        color: ${(p) => p.theme.color.text.grey};
    }
    .ProfileFieldValue {
        padding: 10px 0;
    }

    .ProfileFieldLock {
        margin-left: 8px;
        color: ${(p) => p.theme.color.icon.grey};
    }

    .ProfileFieldInputs {
        flex-grow: 1;
    }

    a {
        color: ${(p) => p.theme.color.primary.main};

        &:hover,
        &:focus {
            text-decoration: underline;
        }
    }
`
const ProfileField = ({
    control,
    setValue,
    watch,
    errors,
    field,
    index,
    isEditingSection,
    required,
}) => {
    const { t } = useTranslation()

    const {
        key,
        name,
        autocomplete,
        value,
        fieldType,
        fieldOptions,
        isEditable,
    } = field

    if (!isEditingSection) {
        return (
            <Wrapper>
                <label className="ProfileFieldLabel">{name}</label>
                {value && (
                    <div className="ProfileFieldValue">
                        <ShowMore>
                            {fieldType === 'htmlField' ? (
                                <TiptapView content={value} />
                            ) : fieldType === 'dateField' ? (
                                showShortDate(value)
                            ) : fieldType === 'emailField' ? (
                                <a href={`mailto:${value}`}>{value}</a>
                            ) : fieldType === 'charField' ? (
                                <a href={`tel:${value}`}>{value}</a>
                            ) : fieldType === 'urlField' ? (
                                <LinkItem url={value}>{value}</LinkItem>
                            ) : (
                                value
                            )}
                        </ShowMore>
                    </div>
                )}
            </Wrapper>
        )
    }

    let optionsArray
    if (['selectField', 'multiSelectField'].includes(fieldType)) {
        optionsArray = fieldOptions.map((option) => ({
            value: option,
            label: option,
        }))
    }

    const watchAccessId = watch(`fields.${index}.accessId`)
    const accessOptions = [
        {
            onClick: () => setValue(`fields.${index}.accessId`, 0),
            name: t('profile.visibility-me'),
            active: watchAccessId === 0,
        },
        {
            onClick: () => setValue(`fields.${index}.accessId`, 1),
            name: t('profile.visibility-members'),
            active: watchAccessId === 1,
        },
        ...(!window.__SETTINGS__.site.isClosed
            ? [
                  {
                      onClick: () => setValue(`fields.${index}.accessId`, 2),
                      name: t('profile.visibility-public'),
                      active: watchAccessId === 2,
                  },
              ]
            : []),
    ]

    const accessLabels = {
        0: t('profile.visibility-me'),
        1: t('profile.visibility-members'),
        2: t('profile.visibility-public'),
    }
    const currentAccessLabel = accessLabels[watchAccessId]

    const accessIcons = [
        <IconPrivate key={0} />,
        <IconUsers key={1} />,
        <IconPublic key={3} />,
    ]
    const currentAccessIcon = accessIcons[watchAccessId]

    const defaultFieldProps = {
        control,
        errors,
        id: key,
        name: `fields.${index}.value`,
        autoComplete: autocomplete,
        disabled: !isEditable,
        style: { flexGrow: 1 },
        required,
    }

    return (
        <Wrapper>
            <label className="ProfileFieldLabel" htmlFor={key}>
                {name}
                {!isEditable && (
                    <Tooltip content={t('profile.field-locked')}>
                        <button type="button" className="ProfileFieldLock">
                            <LockIcon />
                        </button>
                    </Tooltip>
                )}
            </label>

            <div style={{ flexGrow: 1 }}>
                <Flexer
                    gutter="small"
                    alignItems="flex-start"
                    className="ProfileFieldInputs"
                >
                    {fieldType === 'textField' ? (
                        <FormItem
                            {...defaultFieldProps}
                            type="text"
                            error={errors?.fields?.[index]?.value?.message}
                        />
                    ) : fieldType === 'emailField' ? (
                        <FormItem
                            {...defaultFieldProps}
                            type="email"
                            error={errors?.fields?.[index]?.value?.message}
                        />
                    ) : fieldType === 'charField' ? (
                        <FormItem
                            {...defaultFieldProps}
                            type="tel"
                            error={errors?.fields?.[index]?.value?.message}
                        />
                    ) : fieldType === 'urlField' ? (
                        <FormItem
                            {...defaultFieldProps}
                            type="url"
                            error={errors?.fields?.[index]?.value?.message}
                        />
                    ) : fieldType === 'htmlField' ? (
                        <FormItem
                            {...defaultFieldProps}
                            type="rich"
                            options={{
                                textStyle: true,
                                textLink: true,
                                textQuote: true,
                                textList: true,
                                insertMedia: true,
                            }}
                        />
                    ) : fieldType === 'selectField' ? (
                        <FormItem
                            {...defaultFieldProps}
                            type="select"
                            options={optionsArray}
                            isClearable
                        />
                    ) : fieldType === 'multiSelectField' ? (
                        <FormItem
                            {...defaultFieldProps}
                            type="select"
                            options={optionsArray}
                            isMulti
                        />
                    ) : fieldType === 'dateField' ? (
                        <FormItem
                            {...defaultFieldProps}
                            type="date"
                            fromDate={formatISO(subYears(new Date(), 120))}
                            toDate={formatISO(new Date())}
                            isClearable
                            clearLabel={t('search.clear-date-from')}
                        />
                    ) : null}

                    <DropdownButton
                        options={accessOptions}
                        placement="bottom-start"
                    >
                        <IconButton
                            variant="secondary"
                            size="large"
                            aria-label={t('profile.field-visibility-action', {
                                name,
                                visibilityLevel: currentAccessLabel,
                            })}
                            tooltip={currentAccessLabel}
                            placement="right"
                        >
                            {currentAccessIcon}
                        </IconButton>
                    </DropdownButton>
                </Flexer>

                {errors[key] && (
                    <Error style={{ margin: '8px 0' }}>
                        {errors[key].message}
                    </Error>
                )}
            </div>
        </Wrapper>
    )
}

export default ProfileField
