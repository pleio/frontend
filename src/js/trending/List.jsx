import React, { Fragment, useState } from 'react'
import { useParams } from 'react-router-dom'
import { gql } from '@apollo/client'

import blogListFragment from 'js/blog/fragments/listFragment'
import FetchMoreWrapper from 'js/components/FetchMoreWrapper'
import { Row } from 'js/components/Grid/Grid'
import PageHeader from 'js/components/PageHeader/PageHeader'
import discussionListFragment from 'js/discussions/fragments/listFragment'
import eventListFragment from 'js/events/fragments/listFragment'
import newsListFragment from 'js/news/fragments/listFragment'
import questionListFragment from 'js/questions/fragments/listFragment'
import wikiListFragment from 'js/wiki/fragments/listFragment'

import Trending from '../activity/components/Trending'

import Card from './components/Card'
import ContentFilters from './containers/ContentFilters'

const GET_TRENDING_LIST = gql`
    query TrendingList(
        $offset: Int!
        $limit: Int!
        $tags: [String]
        $subtype: String
    ) {
        entities(
            offset: $offset
            limit: $limit
            tags: $tags
            subtype: $subtype
        ) {
            total
            edges {
                guid
                ...NewsListFragment
                ...BlogListFragment
                ...DiscussionListFragment
                ...EventListFragment
                ...QuestionListFragment
                ...WikiListFragment
            }
        }
    }
    ${newsListFragment}
    ${blogListFragment}
    ${discussionListFragment}
    ${eventListFragment}
    ${questionListFragment}
    ${wikiListFragment}
`

const List = () => {
    const [subtype, setSubtype] = useState('')
    const params = useParams()
    const tag = decodeURIComponent(params.tag)

    return (
        <Fragment>
            <PageHeader title={tag}>
                <ContentFilters onChangeSubtype={setSubtype} />
            </PageHeader>

            <section className="section ___grey ___grow">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-4 last-lg top-lg">
                            <Row>
                                <Trending showmd />
                            </Row>
                        </div>
                        <div className="col-lg-8">
                            <FetchMoreWrapper
                                childComponent={Card}
                                subtype={subtype}
                                tags={[tag]}
                                offset={0}
                                limit={50}
                                query={GET_TRENDING_LIST}
                            />
                        </div>
                    </div>
                </div>
            </section>
        </Fragment>
    )
}

export default List
