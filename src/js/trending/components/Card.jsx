import React from 'react'
import PropTypes from 'prop-types'

import BlogCard from 'js/blog/components/Card'
import NewsCard from 'js/news/components/Card'
import QuestionCard from 'js/questions/components/Card'

const Card = (props) => {
    const { entity } = props

    switch (entity.__typename) {
        case 'News':
            return <NewsCard entity={entity} />

        case 'Blog':
            return <BlogCard entity={entity} />

        case 'Question':
            return <QuestionCard entity={entity} />

        default:
            console.error('Unknown entity: ', entity)
            return null
    }
}

Card.propTypes = {
    entity: PropTypes.object.isRequired,
}

export default Card
