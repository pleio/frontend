import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'

import Select from 'js/components/Select/Select'

const ContentFilters = ({ onChangeSubtype, children }) => {
    const [subtype, setSubtype] = useState('all')
    const { t } = useTranslation()

    const subtypeOptions = [
        {
            value: 'all',
            label: t('global.all'),
        },
        {
            value: 'blog',
            label: t('entity-blog.content-name'),
        },
        {
            value: 'news',
            label: t('entity-news.title-list'),
        },
        {
            value: 'question',
            label: t('entity-question.title-list'),
        },
    ]

    const changeSubtype = (value) => {
        setSubtype(value)

        if (onChangeSubtype) onChangeSubtype(value)
    }

    return (
        <div className="row">
            <div className="col-sm-4 col-lg-3">
                <Select
                    name="subtype"
                    options={subtypeOptions}
                    value={subtype}
                    onChange={changeSubtype}
                />
            </div>
            {children}
        </div>
    )
}

export default ContentFilters
