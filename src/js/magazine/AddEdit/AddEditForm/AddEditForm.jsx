import React, { useState } from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useLocation, useNavigate } from 'react-router-dom'
import { gql, useMutation } from '@apollo/client'

import ActionContainer from 'js/components/ActionContainer/ActionContainer'
import IconField from 'js/components/EntityActions/components/IconField'
import DeleteModal from 'js/components/EntityActions/DeleteModal'
import FormItem from 'js/components/Form/FormItem'
import { Container } from 'js/components/Grid/Grid'
import Spacer from 'js/components/Spacer/Spacer'
import { MAX_LENGTH } from 'js/lib/constants'
import getUrls from 'js/magazine/lib/getUrls'
import refetchQueries from 'js/magazine/lib/refetchQueries'

import ColophonFields from './components/ColophonFields'

const AddEditForm = ({ entity, title }) => {
    const { t } = useTranslation()
    const navigate = useNavigate()
    const location = useLocation()

    const [showDeleteModal, setShowDeleteModal] = useState(false)
    const toggleDeleteModal = () => setShowDeleteModal(!showDeleteModal)

    const urls = getUrls()

    const afterDelete = () => {
        navigate(urls.list, {
            replace: true,
        })
    }

    const handleClose = () => {
        navigate(
            location?.state?.prevPathname ||
                (entity && entity.url) ||
                urls.list,
        )
    }

    const [addMagazine] = useMutation(ADD_MAGAZINE)
    const [editMagazine] = useMutation(EDIT_MAGAZINE)

    const submit = async ({ title, subtitle, icon, colophonFields }) => {
        const mutate = entity ? editMagazine : addMagazine

        await mutate({
            variables: {
                input: {
                    ...(entity ? { guid: entity.guid } : {}),
                    ...(entity ? {} : { subtype: 'magazine' }),
                    title,
                    subtitle,
                    iconGuid: icon?.guid || null,
                    colophonFields,
                },
            },
            refetchQueries,
        })
            .then(({ data }) => {
                if (entity) {
                    navigate(data.editEntity.entity.url, {
                        replace: true,
                    })
                } else {
                    navigate(data.addEntity.entity.url, {
                        replace: true,
                    })
                }
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    const defaultValues = {
        title: entity?.title || '',
        subtitle: entity?.subtitle || '',
        icon: entity?.icon || null,
        colophonFields: entity?.colophonFields || [],
    }

    const formMethods = useForm({
        mode: 'onChange',
        defaultValues,
    })

    const {
        control,
        handleSubmit,
        formState: { errors, isValid, isSubmitting },
    } = formMethods

    const submitForm = handleSubmit(submit)

    return (
        <ActionContainer
            title={title}
            publish={submitForm}
            onDelete={entity ? toggleDeleteModal : null}
            onClose={handleClose}
            isValid={isValid}
            isPublished={!!entity}
            isPublishing={isSubmitting}
        >
            <FormProvider {...formMethods}>
                <Container size="small">
                    <Spacer
                        as="form"
                        spacing="small"
                        onSubmit={handleSubmit(submit)}
                    >
                        <FormItem
                            control={control}
                            type="text"
                            name="title"
                            title={t('form.title')}
                            required
                            errors={errors}
                            maxLength={MAX_LENGTH.title}
                        />

                        <FormItem
                            control={control}
                            type="text"
                            name="subtitle"
                            title={t('form.subtitle')}
                            errors={errors}
                            maxLength={MAX_LENGTH.title}
                        />

                        <IconField
                            iconProps={{
                                style: {
                                    height: '56px',
                                },
                            }}
                        />

                        <ColophonFields />
                    </Spacer>
                </Container>
            </FormProvider>

            <DeleteModal
                isVisible={showDeleteModal}
                onClose={toggleDeleteModal}
                title={t('entity-magazine.delete')}
                entity={entity}
                afterDelete={afterDelete}
                refetchQueries={refetchQueries}
            />
        </ActionContainer>
    )
}

export const ADD_MAGAZINE = gql`
    mutation addEntity($input: addEntityInput!) {
        addEntity(input: $input) {
            entity {
                guid
                ... on Magazine {
                    title
                    url
                }
            }
        }
    }
`

const EDIT_MAGAZINE = gql`
    mutation editEntity($input: editEntityInput!) {
        editEntity(input: $input) {
            entity {
                guid
                ... on Magazine {
                    title
                    url
                }
            }
        }
    }
`

export default AddEditForm
