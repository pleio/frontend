import React from 'react'
import { Draggable } from 'react-beautiful-dnd'
import { useFormContext, useWatch } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import styled, { css } from 'styled-components'

import FormItem from 'js/components/Form/FormItem'
import IconButton from 'js/components/IconButton/IconButton'

import RemoveIcon from 'icons/delete-small.svg'
import MoveIcon from 'icons/move-vertical.svg'

const Wrapper = styled.li`
    background-color: white;
    padding: 8px 0;

    ${(p) =>
        p.$isDragging &&
        css`
            border-radius: ${(p) => p.theme.radius.normal};
            box-shadow: ${p.theme.shadow.soft.center};
        `};

    .ColophonFieldLabel {
        flex-grow: 1;
    }
`

const AttendanceFormField = ({ id, index, onRemove }) => {
    const { t } = useTranslation()

    const { control } = useFormContext()

    const watchColophon = useWatch({
        name: 'colophonFields',
        control,
    })

    const label = watchColophon?.[index]?.label

    const buttonProps = {
        type: 'button',
        size: 'large',
        variant: 'secondary',
        hoverSize: 'normal',
        radiusStyle: 'rounded',
    }

    const ariaLabel = `${index + 1}. ${label}`

    return (
        <Draggable draggableId={id} index={index}>
            {(provided, snapshot) => (
                <Wrapper
                    ref={provided.innerRef}
                    $isDragging={snapshot.isDragging}
                    {...provided.draggableProps}
                >
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        <IconButton
                            {...buttonProps}
                            as="div"
                            tooltip={t('action.move')}
                            placement="left"
                            aria-label={t('aria.move-field', {
                                label: ariaLabel,
                            })}
                            {...provided.dragHandleProps}
                        >
                            <MoveIcon />
                        </IconButton>

                        <FormItem
                            control={control}
                            type="text"
                            name={`colophonFields.${index}.label`}
                            label={t('global.label')}
                            className="ColophonFieldLabel"
                        />

                        <IconButton
                            {...buttonProps}
                            onClick={onRemove}
                            aria-label={t('aria.remove-field', {
                                label: ariaLabel,
                            })}
                            tooltip={t('action.delete')}
                            placement="right"
                        >
                            <RemoveIcon />
                        </IconButton>
                    </div>
                </Wrapper>
            )}
        </Draggable>
    )
}

export default AttendanceFormField
