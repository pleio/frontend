import React from 'react'
import { DragDropContext, Droppable } from 'react-beautiful-dnd'
import { useFieldArray, useFormContext } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import Setting from 'js/components/Setting'

import ColophonField from './ColophonField'

const ColophonFields = () => {
    const { t } = useTranslation()

    const { control } = useFormContext()

    const { fields, append, remove, move } = useFieldArray({
        control,
        name: 'colophonFields',
    })

    const onDragEnd = (result) => {
        if (!result.destination) return
        move(result.source.index, result.destination.index)
    }

    const handleAddField = () => {
        append({
            label: '',
        })
    }

    return (
        <>
            <Setting
                subtitle="entity-magazine.colophon-fields"
                helper="entity-magazine.colophon-fields-helper"
            />

            <DragDropContext onDragEnd={onDragEnd}>
                <Droppable droppableId="colophon-fields">
                    {(provided) => (
                        <ul
                            {...provided.droppableProps}
                            ref={provided.innerRef}
                            style={{ margin: '8px 0' }}
                        >
                            {fields.map(({ id }, index) => {
                                return (
                                    <ColophonField
                                        key={id}
                                        id={id}
                                        index={index}
                                        onRemove={() => remove(index)}
                                    />
                                )
                            })}
                            {provided.placeholder}
                        </ul>
                    )}
                </Droppable>
            </DragDropContext>

            <Flexer justifyContent="flex-start">
                <Button
                    variant="tertiary"
                    size="normal"
                    onClick={handleAddField}
                >
                    {t('form.add-field')}
                </Button>
            </Flexer>
        </>
    )
}

export default ColophonFields
