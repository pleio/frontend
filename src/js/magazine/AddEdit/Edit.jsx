import React from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import NotFound from 'js/core/NotFound'
import { editFragment } from 'js/magazine/lib/fragments'

import AddEditForm from './AddEditForm/AddEditForm'

const Edit = () => {
    const { t } = useTranslation()
    const { guid } = useParams()

    const { loading, data } = useQuery(GET_MAGAZINE_EDIT, {
        variables: {
            guid,
        },
    })

    if (loading) return null
    if (!data || !data.entity) return <NotFound />

    const { entity } = data

    return <AddEditForm title={t('entity-magazine.edit')} entity={entity} />
}

const GET_MAGAZINE_EDIT = gql`
    query MagazineEdit($guid: String!) {
        entity(guid: $guid) {
            guid
            ${editFragment}
        }
    }
`

export default Edit
