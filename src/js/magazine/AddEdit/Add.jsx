import React from 'react'
import { useTranslation } from 'react-i18next'

import AddEditForm from './AddEditForm/AddEditForm'

const Add = () => {
    const { t } = useTranslation()

    return <AddEditForm title={t('entity-magazine.create')} />
}

export default Add
