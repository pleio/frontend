import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import styled from 'styled-components'

import Document from 'js/components/Document'
import FetchMore from 'js/components/FetchMore'
import { Col, Container, Row } from 'js/components/Grid/Grid'
import ItemActions from 'js/components/Item/ItemActions/ItemActions'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Section from 'js/components/Section/Section'
import Text from 'js/components/Text/Text'
import getUrls from 'js/magazine/lib/getUrls'
import refetchQueries from 'js/magazine/lib/refetchQueries'
import MagazineIssueCard from 'js/magazine-issue/Card'
import { listFragment as issueListFragment } from 'js/magazine-issue/lib/fragments'
import ThemeProvider from 'js/theme/ThemeProvider'

import HeaderLogo from './HeaderLogo'

const Wrapper = styled(Section)`
    .layout-header {
        display: flex;
        align-items: flex-start;
        justify-content: center;
        margin-bottom: 40px;
    }
`

const Layout = ({ entity }) => {
    const { t } = useTranslation()
    const location = useLocation()

    const { guid, title, subtitle, icon, canEdit } = entity

    const [queryLimit, setQueryLimit] = useState(9)
    const { loading, data, fetchMore } = useQuery(GET_MAGAZINE_ISSUES, {
        variables: {
            parentGuid: guid,
            subtypes: ['magazine_issue'],
            offset: 0,
            limit: queryLimit,
        },
    })

    const urls = getUrls(guid)

    return (
        <ThemeProvider bodyColor="#FFFFFF">
            <Document title={title} />
            <Wrapper
                backgroundColor="white" // darken Card shadow
            >
                <Container size="large">
                    <header className="layout-header">
                        <HeaderLogo
                            title={title}
                            subtitle={subtitle}
                            icon={icon}
                        />
                        <ItemActions
                            entity={entity}
                            onEdit={urls.edit}
                            onAfterDelete={urls.list}
                            hideArchive
                            showEditButton
                            refetchQueries={refetchQueries}
                            customOptions={[
                                [
                                    ...(canEdit
                                        ? [
                                              {
                                                  name: t(
                                                      'entity-magazine.create-issue',
                                                  ),
                                                  to: urls.addIssue,
                                                  state: {
                                                      prevPathname:
                                                          location.pathname,
                                                  },
                                              },
                                          ]
                                        : []),
                                ],
                            ]}
                            style={{ marginLeft: 'auto' }}
                        />
                    </header>
                    <article
                        style={{
                            position: 'relative',
                            marginTop: '32px',
                        }}
                    >
                        {loading ? (
                            <LoadingSpinner />
                        ) : data.entities.edges.length === 0 ? (
                            <Text textAlign="center">
                                {t('admin.content-not-found')}
                            </Text>
                        ) : (
                            <FetchMore
                                edges={data.entities.edges}
                                getMoreResults={(data) => data.entities.edges}
                                fetchMore={fetchMore}
                                fetchCount={9}
                                setLimit={setQueryLimit}
                                maxLimit={data.entities.total}
                                resultsMessage={t('global.result', {
                                    count: data.entities.total,
                                })}
                                fetchMoreButtonHeight={48}
                            >
                                <Row
                                    as="ul"
                                    $growContent
                                    $gutter={40}
                                    $spacing={40}
                                >
                                    {data.entities.edges.map((entity) => (
                                        <Col
                                            mobileLandscape={1 / 2}
                                            tabletUp={1 / 3}
                                            as="li"
                                            key={entity.guid}
                                        >
                                            <MagazineIssueCard
                                                entity={entity}
                                                showTheme
                                            />
                                        </Col>
                                    ))}
                                </Row>
                            </FetchMore>
                        )}
                    </article>
                </Container>
            </Wrapper>
        </ThemeProvider>
    )
}

const GET_MAGAZINE_ISSUES = gql`
    query MagazineViewList(
        $parentGuid: String
        $offset: Int
        $limit: Int
        $subtypes: [String]
        $orderBy: OrderBy
        $orderDirection: OrderDirection
    ) {
        entities(
            parentGuid: $parentGuid
            offset: $offset
            limit: $limit
            subtypes: $subtypes
            orderBy: $orderBy
            orderDirection: $orderDirection
        ) {
            total
            edges {
                guid
                ${issueListFragment}
            }
        }
    }
`

export default Layout
