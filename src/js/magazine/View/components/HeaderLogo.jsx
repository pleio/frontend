import React from 'react'
import styled, { css } from 'styled-components'

import DisplayDate from 'js/components/DisplayDate'
import Img from 'js/components/Img/Img'

const Wrapper = styled.div`
    display: flex;
    align-items: center;

    .header-logo--icon {
        margin-right: ${(p) => (p.$isSmall ? '16px' : '20px')};

        img {
            max-width: 120px;
            max-height: ${(p) => (p.$isSmall ? '40px' : '56px')};
        }
    }

    .header-logo--title-container {
        margin-top: ${(p) => (p.$isSmall ? '-6px' : '-8px')};
        display: flex;
        align-items: center;
    }

    .header-logo--title {
        font-family: ${(p) => p.theme.fontHeading.family};

        ${(p) =>
            p.$isSmall
                ? css`
                      font-size: ${(p) => p.theme.fontHeading.size.large};
                      line-height: ${(p) =>
                          p.theme.fontHeading.lineHeight.large};
                      font-weight: ${(p) =>
                          p.theme.fontHeading.weight.semibold};
                  `
                : css`
                      font-size: ${(p) => p.theme.fontHeading.size.huge};
                      line-height: ${(p) =>
                          p.theme.fontHeading.lineHeight.huge};
                      font-weight: ${(p) => p.theme.fontHeading.weight.bold};
                  `};
    }

    .header-logo--subtitle {
        font-weight: ${(p) => p.theme.font.weight.normal};

        > *:not(:first-child):before {
            content: '  •  ';
            white-space: pre;
        }

        ${(p) =>
            p.$isSmall
                ? css`
                      font-size: ${(p) => p.theme.font.size.tiny};
                      line-height: ${(p) => p.theme.font.lineHeight.tiny};
                  `
                : css`
                      font-size: ${(p) => p.theme.font.size.small};
                      line-height: ${(p) => p.theme.font.lineHeight.small};
                  `};
    }
`

const HeaderLogo = ({
    icon,
    title,
    number,
    subtitle,
    date,
    isSmall,
    children,
    ...rest
}) => {
    return (
        <Wrapper $isSmall={isSmall} {...rest}>
            <Img
                src={icon?.download}
                align="center"
                fullSize
                className="header-logo--icon"
            />
            <div>
                <div className="header-logo--title-container">
                    <h1 className="header-logo--title">
                        {title}
                        {number && ` ${number}`}
                    </h1>
                    {children}
                </div>
                <h2 className="header-logo--subtitle">
                    {subtitle && <span>{subtitle}</span>}
                    {date && (
                        <DisplayDate date={date} type="date" disableTooltip />
                    )}
                </h2>
            </div>
        </Wrapper>
    )
}

export default HeaderLogo
