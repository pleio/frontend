import React from 'react'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import NotFound from 'js/core/NotFound'
import { viewFragment } from 'js/magazine/lib/fragments'

import Layout from './components/Layout'

const View = () => {
    const params = useParams()
    const { guid } = params

    const { loading, data } = useQuery(GET_MAGAZINE_VIEW, {
        variables: {
            guid,
        },
    })

    if (loading) return null
    if (!data || !data.entity) return <NotFound />

    const { entity } = data

    return <Layout entity={entity} />
}

const GET_MAGAZINE_VIEW = gql`
    query MagazineView($guid: String!) {
        entity(guid: $guid, incrementViewCount: true) {
            guid
            ${viewFragment}
        }
    }
`

export default View
