import { iconEditFragment, iconViewFragment } from 'js/lib/fragments/icon'

const sharedFields = `
    title
    url
    subtitle
    canEdit
    canArchiveAndDelete
`

export const listFragment = `
... on Magazine {
    ${sharedFields}
    statusPublished
    timePublished
    timeUpdated
    owner {
        guid
        name
    }
}
`

export const editFragment = `
... on Magazine {
    ${sharedFields}
    ${iconEditFragment}
    colophonFields {
        __typename @include(if: false)
        key
        label
    }
}
`

export const viewFragment = `
... on Magazine {
    ${sharedFields}
    ${iconViewFragment}
}
`
