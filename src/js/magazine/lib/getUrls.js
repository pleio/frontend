export default function (guid) {
    return {
        list: '/magazines',
        ...(guid
            ? {
                  edit: `/magazine/edit/${guid}`,
                  addIssue: `/magazine-issue/add/${guid}`,
              }
            : {}),
    }
}
