import React from 'react'
import { Route, Routes } from 'react-router-dom'

import NotFound from 'js/core/NotFound'
import List from 'js/magazine/List/List'
import View from 'js/magazine/View/View'
import RequireAuth from 'js/router/RequireAuth'

export const MagazinesRoutes = () => (
    <Routes>
        <Route
            path="/"
            element={
                <RequireAuth
                    viewer="canWriteToContainer"
                    canWriteToContainerSubtype="magazine"
                >
                    <List />
                </RequireAuth>
            }
        />
        <Route element={<NotFound />} />
    </Routes>
)

export const MagazineRoutes = () => (
    <Routes>
        <Route path="/view/:guid" element={<View />} />
        <Route path="/view/:guid/:slug" element={<View />} />
        <Route element={<NotFound />} />
    </Routes>
)
