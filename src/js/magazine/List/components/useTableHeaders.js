import { useTranslation } from 'react-i18next'

const useTableHeaders = () => {
    const { t } = useTranslation()

    return [
        {
            name: 'title',
            label: t('form.title'),
            style: { minWidth: 200 },
        },
        {
            name: 'ownerName',
            label: t('global.owner'),
            style: { minWidth: 80 },
        },
        {
            options: [
                {
                    name: 'timeUpdated',
                    label: t('global.last-edited'),
                    defaultOrderDirection: 'desc',
                },
                {
                    name: 'timePublished',
                    label: t('global.published'),
                    defaultOrderDirection: 'desc',
                },
            ],
            align: 'right',
            style: { minWidth: 100 },
        },
        {
            style: { width: 40 },
        },
    ]
}

export default useTableHeaders
