import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { useIsMount } from 'helpers'

import DisplayDate from 'js/components/DisplayDate'
import FetchMore from 'js/components/FetchMore'
import ItemActions from 'js/components/Item/ItemActions/ItemActions'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Spacer from 'js/components/Spacer/Spacer'
import Table from 'js/components/Table'
import Text from 'js/components/Text/Text'
import { listFragment } from 'js/magazine/lib/fragments'
import getUrls from 'js/magazine/lib/getUrls'
import refetchQueries from 'js/magazine/lib/refetchQueries'

import useTableHeaders from './useTableHeaders'

const PageList = () => {
    const { t } = useTranslation()
    const tableHeaders = useTableHeaders()

    const [queryLimit, setQueryLimit] = useState(50)

    const [orderBy, setOrderBy] = useState('title')
    const [orderDirection, setOrderDirection] = useState('asc')

    const { loading, data, fetchMore, refetch } = useQuery(GET_MAGAZINES, {
        variables: {
            parentGuid: '1',
            offset: 0,
            limit: queryLimit,
            orderBy,
            orderDirection,
        },
        fetchPolicy: 'cache-and-network',
    })

    const isMount = useIsMount()

    useEffect(() => {
        if (isMount) {
            refetch()
        }
    })

    useEffect(() => {
        refetch()
    }, [orderBy, orderDirection, refetch])

    return (
        <Spacer>
            {loading ? (
                <LoadingSpinner />
            ) : data.entities.edges.length === 0 ? (
                <Text textAlign="center">{t('admin.pages-not-found')}</Text>
            ) : (
                <Table
                    rowHeight="normal"
                    headers={tableHeaders}
                    orderBy={orderBy}
                    orderDirection={orderDirection}
                    setOrderBy={setOrderBy}
                    setOrderDirection={setOrderDirection}
                >
                    <FetchMore
                        as="tbody"
                        edges={data.entities.edges}
                        getMoreResults={(data) => data.entities.edges}
                        fetchMore={fetchMore}
                        fetchCount={25}
                        setLimit={setQueryLimit}
                        maxLimit={data.entities.total}
                        resultsMessage={t('global.result', {
                            count: data.entities.total,
                        })}
                        fetchMoreButtonHeight={48}
                    >
                        {data.entities.edges.map((entity) => {
                            const {
                                guid,
                                url,
                                title,
                                owner,
                                timePublished,
                                timeUpdated,
                            } = entity

                            const selectedDateSorting =
                                orderBy === 'timePublished'
                                    ? timePublished
                                    : timeUpdated

                            const urls = getUrls(guid)

                            return (
                                <tr key={guid}>
                                    <td>
                                        <Link to={url} className="TableLink">
                                            {title}
                                        </Link>
                                    </td>
                                    <td>{owner.name}</td>
                                    <td
                                        style={{
                                            textAlign: 'right',
                                        }}
                                    >
                                        <DisplayDate
                                            date={selectedDateSorting}
                                        />
                                    </td>
                                    <td>
                                        <ItemActions
                                            entity={entity}
                                            onEdit={urls.edit}
                                            onAfterDelete={urls.list}
                                            hideArchive
                                            refetchQueries={refetchQueries}
                                        />
                                    </td>
                                </tr>
                            )
                        })}
                    </FetchMore>
                </Table>
            )}
        </Spacer>
    )
}

const GET_MAGAZINES = gql`
    query MagazinesList(
        $parentGuid: String
        $limit: Int!
        $offset: Int!
        $orderBy: OrderBy
        $orderDirection: OrderDirection # $dateFrom: String # $dateTo: String
    ) {
        entities(
            parentGuid: $parentGuid
            subtypes: ["magazine"]
            limit: $limit
            offset: $offset
            orderBy: $orderBy
            orderDirection: $orderDirection # dateFrom: $dateFrom # dateTo: $dateTo
        ) {
            total
            edges {
                guid
                ${listFragment}
            }
        }
    }
`

export default PageList
