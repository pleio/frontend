import React from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'

import { Container } from 'js/components/Grid/Grid'
import PageHeader from 'js/components/PageHeader/PageHeader'
import Section from 'js/components/Section/Section'
import { useWindowScrollPosition } from 'js/lib/helpers'

import Table from './components/Table'

const List = () => {
    const { parentGuid } = useParams()

    const { t } = useTranslation()

    useWindowScrollPosition('magazines')

    return (
        <>
            <PageHeader
                title={t('entity-magazine.name', {
                    count: 2,
                })}
                canCreate // Route is already wrapped in a canWriteToContainer HOC
                createLink="/magazines/add"
                createLabel={t('entity-magazine.create')}
            />
            <Section backgroundColor="white" grow noTopPadding={!parentGuid}>
                <Container>
                    <Table />
                </Container>
            </Section>
        </>
    )
}

export default List
