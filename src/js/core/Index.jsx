import React from 'react'

import ActivityList from 'js/activity/List'
import PageItem from 'js/page/Item'

const Index = () => {
    if (!window) return <ActivityList />

    const startPage = window.__SETTINGS__.site.startPage
    const startPageCms = window.__SETTINGS__.site.startPageCms

    if (startPage === 'cms' && startPageCms) {
        return <PageItem guid={startPageCms} isHome />
    } else {
        return <ActivityList />
    }
}
export default Index
