import React from 'react'
import { Route, Routes } from 'react-router-dom'

import RequireAuth from 'js/router/RequireAuth'

import ActivityList from '../activity/List'
import Saved from '../saved/List'
import TrendingList from '../trending/List'

import Index from './Index'
import Login from './Login'
import Logout from './Logout'
import NotFound from './NotFound'
import Register from './Register'

const Component = () => (
    <Routes>
        <Route path="/" element={<Index />} />
        <Route path="/activity" element={<ActivityList />} />
        <Route
            path="/saved"
            element={
                <RequireAuth viewer="loggedIn">
                    <Saved />
                </RequireAuth>
            }
        />
        <Route path="/trending/:tag" element={<TrendingList />} />
        <Route path="/login" element={<Login />} />
        <Route path="/logout" element={<Logout />} />
        <Route path="/register" element={<Register />} />
        <Route path="*" element={<NotFound />} />
    </Routes>
)
export default Component
