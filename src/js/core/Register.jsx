import { useLocation } from 'react-router-dom'

const Register = () => {
    const { state } = useLocation()
    window.location.href = `/register${
        state?.next ? `?next=${state.next}` : ''
    }`
    return null
}

export default Register
