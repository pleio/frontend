import { useLocation } from 'react-router-dom'

const Login = () => {
    const { state } = useLocation()
    window.location.href = `/login${state?.next ? `?next=${state.next}` : ''}`
    return null
}

export default Login
