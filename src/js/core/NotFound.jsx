import React from 'react'
import { Trans, useTranslation } from 'react-i18next'
import { Link, useLocation } from 'react-router-dom'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { useTheme } from 'styled-components'

import { Container } from 'js/components/Grid/Grid'
import { H2 } from 'js/components/Heading'
import Section from 'js/components/Section/Section'
import Text from 'js/components/Text/Text'

import NotFoundIcon from 'icons/not-found.svg'

import Document from '../components/Document'

const NotFound = ({ data }) => {
    const { t } = useTranslation()
    const { loading, viewer } = data
    const theme = useTheme()
    const { pathname, search } = useLocation()
    const next = encodeURIComponent(`${pathname}${search}`)

    if (loading) return null

    let message
    if (viewer.loggedIn || !window.__SETTINGS__.showLoginRegister) {
        message = (
            <Text as="p">
                <Trans i18nKey="notfound.message-access">
                    Dit kan zijn omdat je hiertoe geen rechten hebt of omdat
                    deze pagina niet (meer) bestaat.
                    <br />
                    <a href="/" style={{ textDecoration: 'underline' }}>
                        Terug naar de homepage
                    </a>
                    .
                </Trans>
            </Text>
        )
    } else {
        message = (
            <Text as="p">
                <Trans i18nKey="notfound.message-login">
                    Dit kan zijn omdat je eerst moet
                    <Link to="/login" state={{ next }}>
                        inloggen
                    </Link>
                    om deze pagina te bekijken of omdat deze pagina niet meer
                    bestaat.
                </Trans>
            </Text>
        )
    }

    return (
        <Section backgroundColor="white" grow>
            <Document title={t('notfound.title')} />
            <Container style={{ textAlign: 'center' }}>
                <NotFoundIcon
                    style={{
                        margin: '0 auto 16px',
                        width: '62px',
                        height: '78px',
                        color: theme.color.primary.main,
                    }}
                />
                <H2
                    as="h1"
                    style={{
                        marginBottom: '16px',
                        color: theme.color.primary.main,
                    }}
                >
                    {t('notfound.not-available')}
                </H2>
                {message}
            </Container>
        </Section>
    )
}

const Query = gql`
    query NotFound {
        viewer {
            guid
            loggedIn
        }
    }
`

export default graphql(Query)(NotFound)
