import { SiteStyleState } from 'js/lib/stores'

import getThemeConstants from './helpers/getThemeConstants'
import mergeDeepObjects from './helpers/mergeDeepObjects'

export default function getTheme(
    currentTheme: any,
    theme?: string,
    siteStyle?: SiteStyleState,
) {
    const baseValues = currentTheme
        ? JSON.parse(JSON.stringify(currentTheme)) // Deep copy object
        : {}

    const themeValues = getThemeConstants(theme, siteStyle)

    const mergedValues = mergeDeepObjects(baseValues, themeValues)

    return mergedValues
}
