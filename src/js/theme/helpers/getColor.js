import { darken, getLuminance, lighten } from 'polished'

import { textContrastIsAA } from 'js/lib/helpers/getContrast'

export function getHoverColor(color) {
    return getLuminance(color) > 0.8
        ? darken(0.03, color) // if lightest BG-color
        : getLuminance(color) < 0.1
          ? lighten(0.03, color) // if darkest BG-color
          : darken(0.05, color)
}

export function getActiveColor(color) {
    return getLuminance(color) > 0.8
        ? darken(0.06, color) // if lightest BG-color
        : getLuminance(color) < 0.1
          ? lighten(0.06, color) // if darkest BG-color
          : darken(0.1, color)
}

export function getHeaderHoverColor(colorHeader) {
    return getLuminance(colorHeader) > 0.8
        ? darken(0.03, colorHeader) // if lightest BG-color
        : getLuminance(colorHeader) < 0.1
          ? lighten(0.03, colorHeader) // if darkest BG-color
          : textContrastIsAA(colorHeader)
            ? darken(0.05, colorHeader) // if white text color
            : lighten(0.05, colorHeader) // if black text color
}

export function getHeaderActiveColor(colorHeader) {
    return getLuminance(colorHeader) > 0.8
        ? darken(0.06, colorHeader) // if lightest BG-color
        : getLuminance(colorHeader) < 0.1
          ? lighten(0.06, colorHeader) // if darkest BG-color
          : textContrastIsAA(colorHeader)
            ? darken(0.1, colorHeader) // if white text color
            : lighten(0.1, colorHeader) // if black text color
}
