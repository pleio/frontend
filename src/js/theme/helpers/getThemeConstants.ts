/*
    Themes and associated constants
*/

import { getReadableColor } from 'helpers/getContrast'
import { transparentize } from 'polished'

import { SiteStyleState } from 'js/lib/stores'

import baseConstants from './baseConstants'
import {
    getActiveColor,
    getHeaderActiveColor,
    getHeaderHoverColor,
    getHoverColor,
} from './getColor'
import mergeDeepObjects from './mergeDeepObjects'
import tweakFont from './tweakFont'

export default function getThemeConstants(
    theme: string,
    siteStyle: SiteStyleState,
) {
    if (!theme) return null

    switch (theme) {
        case 'site': {
            const {
                fontBody,
                fontHeading,
                colorPrimary,
                colorSecondary,
                colorHeader,
            } = siteStyle

            const FONT_BODY = fontBody
            const FONT_HEADING = fontHeading

            const COLOR_WHITE = '#FFFFFF'
            const COLOR_BLACK = '#000000'
            const COLOR_WARN = '#EB0000'
            const COLOR_PRIMARY = colorPrimary
            const COLOR_SECONDARY = colorSecondary
            const COLOR_HEADER = colorHeader

            const COLOR_HOVER = transparentize(0.97, COLOR_BLACK)
            const COLOR_ACTIVE = transparentize(0.94, COLOR_BLACK)

            const COLOR_GREYS = {
                10: '#F5F5F5', // background
                '10-alpha': transparentize(0.96, COLOR_BLACK),
                20: '#E3E3E3', // list items divider
                '20-alpha': transparentize(0.89, COLOR_BLACK),
                30: '#C9C9C9', // section divider, card border
                '30-alpha': transparentize(0.79, COLOR_BLACK),
                40: '#949494', // input/button/tag border
                '40-alpha': transparentize(0.58, COLOR_BLACK),
            }

            const themeConstants = {
                font: {
                    family: FONT_BODY,
                    weight: tweakFont(FONT_BODY, 'weight'),
                },
                fontHeading: {
                    family: FONT_HEADING,
                    weight: tweakFont(FONT_HEADING, 'weight'),
                },
                color: {
                    primary: {
                        main: COLOR_PRIMARY,
                    },
                    secondary: {
                        main: COLOR_SECONDARY,
                        hover: getHoverColor(COLOR_SECONDARY),
                        active: getActiveColor(COLOR_SECONDARY),
                    },
                    header: {
                        main: COLOR_HEADER,
                        hover: getHeaderHoverColor(COLOR_HEADER),
                        active: getHeaderActiveColor(COLOR_HEADER),
                    },
                    warn: {
                        main: COLOR_WARN,
                        hover: getHoverColor(COLOR_WARN),
                        active: getActiveColor(COLOR_WARN),
                    },
                    text: {
                        black: COLOR_BLACK,
                        grey: transparentize(0.45, COLOR_BLACK),
                    },
                    icon: {
                        black: COLOR_BLACK,
                        grey: transparentize(0.58, COLOR_BLACK),
                        bg: COLOR_WHITE,
                    },
                    grey: COLOR_GREYS,
                    hover: COLOR_HOVER,
                    active: COLOR_ACTIVE,
                    accept: '#00892D',
                    social: {
                        facebook: '#3664a2',
                        linkedin: '#0073b2',
                        mastodon: '#6364ff',
                    },
                },
                menu: {
                    bg: COLOR_WHITE,
                    color: COLOR_BLACK,
                    shadow: baseConstants.shadow.overlay,
                    hover: COLOR_HOVER,
                    active: COLOR_ACTIVE,
                    current: COLOR_PRIMARY,
                    divider: COLOR_GREYS[20],
                },
                tooltip: {
                    bg: '#191919',
                    color: COLOR_WHITE,
                    shadow: 'none',
                },
                button: {
                    bg: COLOR_WHITE,
                    primary: {
                        color: COLOR_WHITE,
                    },
                    disabled: {
                        bg: COLOR_GREYS[20],
                        color: transparentize(0.45, COLOR_BLACK),
                    },
                },
                card: {
                    bg: COLOR_WHITE,
                    shadow: `0 0 0 1px ${transparentize(0.89, COLOR_BLACK)}`,
                },
                editor: {
                    bg: COLOR_WHITE,
                },
                focusStyling: `2px solid ${COLOR_BLACK}`,
            }

            return mergeDeepObjects(baseConstants, themeConstants)
        }

        case 'pleio': {
            const COLOR_THEME = baseConstants.color.pleio

            return {
                color: {
                    body: '#FFFFFF',
                    primary: {
                        main: COLOR_THEME,
                    },
                    secondary: {
                        main: COLOR_THEME,
                        hover: getHoverColor(COLOR_THEME),
                        active: getActiveColor(COLOR_THEME),
                    },
                },
            }
        }

        // Custom color
        default: {
            const COLOR_THEME = theme
            const COLOR_TEXT = getReadableColor(COLOR_THEME)
            const SHADOW_BORDER = `0 0 0 1px ${transparentize(0.7, COLOR_TEXT)}`

            return {
                color: {
                    body: COLOR_THEME,
                    text: {
                        black: COLOR_TEXT,
                        grey: transparentize(0.4, COLOR_TEXT),
                    },
                    icon: {
                        black: COLOR_TEXT,
                        grey: COLOR_TEXT,
                        bg: 'none',
                    },
                    primary: {
                        main: COLOR_TEXT,
                    },
                    secondary: {
                        main: COLOR_TEXT,
                        hover: transparentize(
                            COLOR_TEXT === 'black' ? 0.2 : 0.075,
                            COLOR_TEXT,
                        ),
                        active: transparentize(
                            COLOR_TEXT === 'black' ? 0.1 : 0.15,
                            COLOR_TEXT,
                        ),
                    },
                    grey: {
                        10: transparentize(0.96, COLOR_TEXT),
                        20: transparentize(0.89, COLOR_TEXT),
                        30: transparentize(0.79, COLOR_TEXT),
                        40: transparentize(0.58, COLOR_TEXT),
                    },
                    hover: transparentize(0.97, COLOR_TEXT),
                    active: transparentize(0.94, COLOR_TEXT),
                    social: {
                        facebook: COLOR_TEXT,
                        linkedin: COLOR_TEXT,
                        mastodon: COLOR_TEXT,
                    },
                },
                tooltip: {
                    bg: COLOR_TEXT,
                    color: COLOR_THEME,
                },
                button: {
                    bg: COLOR_THEME,
                    primary: {
                        color: COLOR_THEME,
                    },
                    disabled: {
                        bg: transparentize(0.5, COLOR_TEXT),
                        color: COLOR_THEME,
                    },
                },
                card: {
                    bg: COLOR_THEME,
                    shadow: SHADOW_BORDER,
                },
                editor: {
                    bg: COLOR_THEME,
                },
                focusStyling: `2px solid ${COLOR_TEXT}`,
            }
        }
    }
}
