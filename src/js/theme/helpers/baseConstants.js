/*
    These are fixed values, independent of the active theme
*/

import pxToRem from 'js/lib/helpers/pxToRem'

export default {
    font: {
        size: {
            tiny: pxToRem(12),
            small: pxToRem(14),
            normal: pxToRem(16),
            large: pxToRem(18),
            huge: pxToRem(20),
        },
        lineHeight: {
            tiny: pxToRem(14),
            small: pxToRem(20),
            normal: pxToRem(24),
            large: pxToRem(28),
            huge: pxToRem(28),
        },
    },
    fontHeading: {
        size: {
            small: pxToRem(20),
            normal: pxToRem(24),
            large: pxToRem(30),
            huge: pxToRem(42),
            enormous: pxToRem(50),
        },
        lineHeight: {
            small: pxToRem(28),
            normal: pxToRem(30),
            large: pxToRem(38),
            huge: pxToRem(54),
            enormous: pxToRem(62),
        },
    },
    color: {
        pleio: '#0771F2',
    },
    headerBarHeight: 48,
    radius: {
        tiny: '3px',
        small: '4px',
        normal: '6px',
        large: '8px',
    },
    shadow: {
        hard: {
            top: '0 -1px 0 0 rgba(0, 0, 0, 0.21)',
            right: '1px 0 0 0 rgba(0, 0, 0, 0.21)',
            bottom: '0 1px 0 0 rgba(0, 0, 0, 0.21)',
            left: '-1px 0 0 0 rgba(0, 0, 0, 0.21)',
            outside: '0 0 0 1px rgba(0, 0, 0, 0.21)',
        },
        soft: {
            center: '0 0 0 1px rgba(0, 0, 0, 0.06), 0 0 8px rgba(0,0,0,.15)',
            bottom: '0 1px 6px 0 rgba(0, 0, 0, 0.08), 0 20px 20px -15px rgba(0, 0, 0, 0.05)',
            top: '0 -1px 6px 0 rgba(0, 0, 0, 0.08), 0 -20px 20px -15px rgba(0, 0, 0, 0.05)',
        },
        overlay:
            '0 0 1px 0 rgba(0, 0, 0, 0.15), 0 0px 16px -2px rgba(0, 0, 0, 0.2)',
    },
    transition: {
        fast: '0.15s ease',
        normal: '0.25s ease',
        slow: '0.35s ease',
        materialFast: '0.15s cubic-bezier(0.4, 0, 0.2, 1)',
        materialNormal: '0.25s cubic-bezier(0.4, 0, 0.2, 1)',
        materialSlow: '0.35s cubic-bezier(0.4, 0, 0.2, 1)',
    },
    containerWidth: {
        tiny: '580px',
        small: '640px',
        normal: '700px',
        large: '1040px',
        huge: '1400px',
    },
    dimensions: {
        adminNavigationWidth: '220px',
    },
    padding: {
        vertical: {
            small: '16px',
            normal: '20px',
        },
        horizontal: {
            small: '20px',
            normal: '24px',
        },
    },
}
