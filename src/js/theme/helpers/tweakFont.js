const fonts = {
    'Rijksoverheid Sans': {
        weight: { normal: 'normal', semibold: 'bold', bold: 'bold' },
    },
    Arial: {
        weight: { normal: 'normal', semibold: 'bold', bold: 'bold' },
    },
    'Source Sans Pro': {
        weight: { normal: '400', semibold: '600', bold: '650' },
    },
    Roboto: {
        weight: { normal: '400', semibold: '500', bold: '700' },
    },
    'Open Sans': {
        weight: { normal: '400', semibold: '600', bold: '650' },
    },
    'PT Sans': {
        weight: { normal: '400', semibold: '700', bold: '700' },
    },
    Palanquin: {
        weight: { normal: '400', semibold: '600', bold: '700' },
    },
    Montserrat: {
        weight: { normal: '400', semibold: '600', bold: '700' },
    },
    'Source Serif Pro': {
        weight: { normal: '400', semibold: '600', bold: '700' },
    },
    'General Sans': {
        weight: { normal: '400', semibold: '550', bold: '600' },
    },
}

export default function tweakFont(font, prop) {
    return fonts[font] ? fonts[font][prop] : fonts['Rijksoverheid Sans'][prop]
}
