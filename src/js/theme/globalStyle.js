import { createGlobalStyle, css } from 'styled-components'

import popoverStyle from 'js/components/Popover/popoverStyle'
import tiptapStyle from 'js/components/Tiptap/tiptapStyle'
import tooltipStyle from 'js/components/Tooltip/tooltipStyle'

import normalize from '../normalize'

// Can be overwritten by ThemeProvider
export const themableStyles = css`
    ${popoverStyle}
    ${tooltipStyle}
    ${tiptapStyle};

    /* Set outline to focused elements when using keyboard */
    body:not(.mouse-user) & *:focus {
        outline: ${(p) => p.theme.focusStyling};
    }

    svg #bg {
        fill: ${(p) => p.theme.color.icon.bg};
    }
`

export default createGlobalStyle`
    /* -- Normalize styles */
    ${normalize}

    /* -- Base styles */
    *,
    *:after,
    *:before {
        box-sizing: border-box;
        outline-offset: -2px;
    }

    /* Remove highlight color on Android */
    * {
        -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    }

    *:active, *:active:before, *:active:after {
        transition-duration: 0s;
    }

    html {
        font-family: sans-serif;
        -ms-text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%;
        text-rendering: optimizeLegibility;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }

    body {
        height: 100%;
        margin: 0;
        font-family: ${(p) => p.theme.font.family}, Sans-Serif;
        font-size: ${(p) => p.theme.font.size.normal};
        line-height: ${(p) => p.theme.font.lineHeight.normal};
        word-break: break-word;
        word-wrap: break-word;
        overflow-y: auto;
    }

    *:focus {
        outline: none;
    }

    a {
        text-decoration: none;
        color: inherit;
    }

    img {
        display: block;
        max-width: 100%;
        border: none;
    }

    svg {
        flex-shrink: 0;
    }

    button {
        display: block;
        color: inherit;
        font: inherit;
        margin: 0;
        padding: 0;
        border: none;
        background: none;
        text-align: left;
    }

    /* Override normalize */
    button, [type='button'], [type='reset'], [type='submit'] {
        appearance: none !important;
    }

    a, button {
        cursor: pointer;

        &[disabled] {
            cursor: default;
            pointer-events: none;
        }
    }

    input {
        line-height: inherit;
        font-size: inherit;
    }

    ul, ol {
        margin: 0;
        padding: 0;
        list-style: none;
    }

    li {
        margin: 0;
        padding: 0;
    }

    ul li::before {
        content: '\\200B';
        position: absolute;
        pointer-events: none;
    }

    table {
        border-collapse: collapse;
        border-spacing: 0;
    }

    td,
    th {
        padding: 0;
    }

    hr {
        display: block;
        height: 1px;
        border: none;
        border-top: 1px solid ${(p) => p.theme.color.grey[20]};
        margin: 0;
        padding: 0;
    }

    p {
        margin: 0;
    }

    h1, h2, h3, h4, h5, h6, strong {
        margin: 0;
    }

    b,
    strong {
        font-weight: ${(p) => p.theme.font.weight.semibold};
    }

    input::placeholder,
    textarea::placeholder {
        color: ${(p) => p.theme.color.text.grey};
        opacity: 1; /* Firefox */
    }

    kbd {
        padding: 0 3px 1px;
        color: ${(p) => p.theme.color.text.grey};
        background-color: ${(p) => p.theme.color.grey[10]};
        border-radius: ${(p) => p.theme.radius.tiny};
        border: 1px solid ${(p) => p.theme.color.grey[30]};
    }

    fieldset {
        border: none;
        margin: 0;
        padding: 0;
    }

    .chrome-picker {
        user-select: none;
        border-radius: none !important;
        box-shadow: none !important;
    }

    ${themableStyles}
`
