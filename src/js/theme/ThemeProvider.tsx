import React, { createContext, useState } from 'react'
import styled, {
    createGlobalStyle,
    ThemeProvider as StyledThemeProvider,
} from 'styled-components'

import { getReadableColor } from 'js/lib/helpers/getContrast'
import { SiteStyleState } from 'js/lib/stores'
import getTheme from 'js/theme/getTheme'
import GlobalStyle, { themableStyles } from 'js/theme/globalStyle'

export interface ContextProps {
    container: HTMLElement
}

export const ThemeContext = createContext<ContextProps>(null)

const ThemeContainer = styled.div`
    ${themableStyles}
`

const OverwriteGlobalStyle = createGlobalStyle<{ $bodyColor: string }>`
    body {
        background-color: ${(p) => p.$bodyColor};
        color: ${(p) => getReadableColor(p.$bodyColor)};
    }
`

interface Props {
    siteStyle?: SiteStyleState // required for theme 'site'
    isRoot?: boolean
    theme?: string // 'site' | 'pleio' | HEX color
    bodyColor?: string // HEX color
    children: React.ReactNode
}

const ThemeProvider = ({
    siteStyle,
    isRoot, // Preserve layout (react-root) by not wrapping children in ThemeContainer div
    theme, // Apply specific theme..
    bodyColor, // ..or/and set body background color
    children,
    ...rest
}: Props) => {
    const [ref, setRef] = useState<HTMLDivElement>()

    if (theme === 'site' && !siteStyle) return null

    return (
        <>
            {/* Overwrite body colors */}
            {bodyColor && <OverwriteGlobalStyle $bodyColor={bodyColor} />}
            {theme ? (
                <StyledThemeProvider
                    theme={(currentTheme) =>
                        getTheme(currentTheme, theme, siteStyle)
                    }
                >
                    {isRoot ? (
                        <>
                            <GlobalStyle />
                            {children}
                        </>
                    ) : (
                        <>
                            {/* Needed to append Tooltips to closest ThemeProvider */}
                            <ThemeContext.Provider value={{ container: ref }}>
                                {/* Apply global themable styles */}
                                <ThemeContainer
                                    ref={(node) => setRef(node)}
                                    {...rest}
                                >
                                    {children}
                                </ThemeContainer>
                            </ThemeContext.Provider>
                        </>
                    )}
                </StyledThemeProvider>
            ) : (
                children
            )}
        </>
    )
}

export default ThemeProvider
