import React, { useContext, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation, useNavigate, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { endOfDay, formatISO, startOfDay } from 'date-fns'
import { createQueryString, getQueryVariable, useLocalStorage } from 'helpers'
import { useMobilePortrait } from 'helpers/breakpoints'
import styled from 'styled-components'

import AnimatePresence from 'js/components/AnimatePresence/AnimatePresence'
import Document from 'js/components/Document'
import FetchMore from 'js/components/FetchMore'
import { Col, Container, Row } from 'js/components/Grid/Grid'
import { H1, H4 } from 'js/components/Heading'
import HideVisually from 'js/components/HideVisually/HideVisually'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import NoResultsMessage from 'js/components/NoResultsMessage/NoResultsMessage'
import RecommendedItem from 'js/components/RecommendedItem/RecommendedItem'
import SearchBar from 'js/components/SearchBar/SearchBar'
import Section from 'js/components/Section/Section'
import Spacer from 'js/components/Spacer/Spacer'
import TabPage from 'js/components/TabPage'
import { iconViewFragment } from 'js/lib/fragments/icon'
import SearchContext from 'js/search/providers/SearchContext'
import ThemeProvider from 'js/theme/ThemeProvider'

import Card from './components/Card'
import ResultsHeader from './components/ResultsHeader'
import SearchFilters from './components/SearchFilters'
import SearchTabs from './components/SearchTabs'
import SEARCH_QUERY from './searchQuery'

const TilesGrid = styled.div`
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
    gap: 16px;
`

// Add search terms to search context
const useSearchQuery = () => {
    const { setSearchQuery } = useContext(SearchContext)
    const searchQuery = getQueryVariable('q')
    setSearchQuery(searchQuery)
}

const CardsContainer = ({ subtype, children }) => {
    return subtype === 'image' ? <TilesGrid>{children}</TilesGrid> : children
}

const Search = () => {
    const navigate = useNavigate()
    const location = useLocation()
    const { t } = useTranslation()
    const { groupGuid } = useParams()
    const isMobilePortrait = useMobilePortrait()

    useSearchQuery()

    const { loading: siteLoading, data: siteData } = useQuery(
        GET_SEARCH_CONTAINER,
        {
            variables: {
                containerGuid: groupGuid,
            },
        },
    )

    // Get any query variables
    const variables = {
        q: getQueryVariable('q'),
        subtype: getQueryVariable('subtype'),
        datefrom: getQueryVariable('datefrom'),
        dateto: getQueryVariable('dateto'),
        orderby: getQueryVariable('orderby'),
        orderdirection: getQueryVariable('orderdirection'),
        tags: getQueryVariable('tags'),
        archived: getQueryVariable('archived'),
        relatedsites: getQueryVariable('relatedsites'),
    }

    // Rebuild the query string on update, only re-using the variables that were defined.
    const getSearchLink = (params) => {
        const queryString = createQueryString({ ...variables, ...params })
        // Because Search can be used in different contexts, determine path like this.
        // Falls back to `/search`
        const path = location.pathname || '/search'
        return `${path}${queryString}`
    }

    const onSubmit = (evt) => {
        evt.preventDefault()

        const trimmedSearchInput = searchInput?.trim()
        setSearchInput(trimmedSearchInput)

        // Reset subtype when doing a new text search
        navigate(getSearchLink({ q: trimmedSearchInput, subtype: null }))
    }

    const [searchInput, setSearchInput] = useState()
    useEffect(() => {
        setSearchInput(variables.q)
    }, [variables.q])

    const handleChangeSearchInput = (evt) => setSearchInput(evt.target.value)

    const [showFilters, setShowFilters] = useLocalStorage(
        'search-showFilters',
        !isMobilePortrait,
    )

    const handleToggleFilters = () => setShowFilters(!showFilters)

    const tagCategories = siteData && siteData.site.tagCategories

    const [queryLimit, setQueryLimit] = useState(10)
    const { loading, data, fetchMore } = useQuery(SEARCH_QUERY, {
        variables: {
            q: variables.q || '',
            subtype: variables.subtype,
            dateFrom:
                variables.datefrom &&
                formatISO(startOfDay(new Date(variables.datefrom))),
            dateTo:
                variables.dateto &&
                formatISO(endOfDay(new Date(variables.dateto))),
            orderBy: variables.orderby,
            orderDirection: variables.orderdirection,
            tagCategories: JSON.parse(variables.tags),
            archived: !!variables.archived,
            searchRelatedSites: !!variables.relatedsites,
            offset: 0,
            limit: queryLimit,
            containerGuid: groupGuid,
        },
    })

    if (siteLoading || !siteData.site) return null

    const noResults = (
        <NoResultsMessage
            title={t('search.no-results')}
            subtitle={t('search.no-results-helper')}
        />
    )

    const totalsType = variables.subtype

    const total = totalsType
        ? data?.search?.totals?.find((el) => el.subtype === totalsType)?.total
        : data?.search?.total

    const firstTabLabel = 'global.all'

    return (
        <ThemeProvider bodyColor="#FFFFFF">
            <Document title={t('global.search')} />

            <Section
                backgroundColor="white"
                divider
                style={{
                    paddingBottom: 0,
                    position: 'relative', // Position divider on top of next section
                }}
            >
                <Container size="small">
                    <Spacer spacing="small">
                        <H1>{t('global.search')}</H1>
                        <div>
                            <HideVisually>
                                <h2>{t('global.filters')}</h2>
                            </HideVisually>

                            <form
                                method="GET"
                                onSubmit={onSubmit}
                                style={{ marginBottom: '8px' }}
                            >
                                <SearchBar
                                    name="search-bar"
                                    value={searchInput}
                                    onChange={handleChangeSearchInput}
                                    label={
                                        groupGuid
                                            ? t('entity-group.search-in')
                                            : t('global.search')
                                    }
                                    showFilters={showFilters}
                                    onToggleFilters={handleToggleFilters}
                                    size="large"
                                />
                            </form>
                            <AnimatePresence visible={showFilters}>
                                <SearchFilters
                                    name="search"
                                    getSearchLink={getSearchLink}
                                    tagFilter={JSON.parse(variables.tags) || []}
                                    tagCategories={tagCategories}
                                    dateFrom={
                                        variables.datefrom
                                            ? formatISO(variables.datefrom)
                                            : null
                                    }
                                    dateTo={
                                        variables.dateto
                                            ? formatISO(variables.dateto)
                                            : null
                                    }
                                    archiveSize={siteData.siteStats.archiveSize}
                                    archived={!!variables.archived}
                                    searchPublishedFilterEnabled={
                                        siteData.site
                                            .searchPublishedFilterEnabled
                                    }
                                    relatedSites={!!variables.relatedsites}
                                    searchRelatedSites={
                                        siteData.site.searchRelatedSites
                                    }
                                    showRelatedSites={
                                        siteData.site.searchRelatedSitesEnabled
                                    }
                                    showDateFilter
                                    currentParams={{
                                        q: searchInput?.trim(),
                                    }}
                                    style={{ paddingBottom: '20px' }}
                                />
                            </AnimatePresence>
                        </div>
                    </Spacer>
                </Container>
                <Container>
                    <SearchTabs
                        total={data?.search?.total || 0}
                        totals={data?.search?.totals || []}
                        getSearchLink={getSearchLink}
                        firstTabLabel={firstTabLabel}
                    />
                </Container>
            </Section>

            {!loading && data?.recommendedSearch?.edges?.length > 0 && (
                <Section
                    backgroundColor="white"
                    divider
                    style={{ padding: '20px 0' }}
                >
                    <Container size="small">
                        <H4 style={{ marginBottom: '12px' }}>
                            {t(`search.${siteData.site.recommendedType}`)}
                        </H4>
                        <Row $spacing={16}>
                            {data?.recommendedSearch?.edges.map((entity) => (
                                <Col
                                    mobileLandscapeUp={1 / 2}
                                    key={`search-recommended-${entity.guid}`}
                                >
                                    <RecommendedItem
                                        entity={entity}
                                        showExcerpt
                                        truncateTitle
                                    />
                                </Col>
                            ))}
                        </Row>
                    </Container>
                </Section>
            )}
            <TabPage
                id={`search-results-${totalsType || firstTabLabel}`}
                aria-labelledby={`search-tab-${totalsType || firstTabLabel}`}
                visible
            >
                <Section backgroundColor="white">
                    <Container size="small">
                        <ResultsHeader
                            total={total}
                            orderBy={variables.orderby}
                            orderDirection={variables.orderdirection}
                            getSearchLink={getSearchLink}
                            style={{ marginBottom: '24px' }}
                        />

                        {loading ? (
                            <LoadingSpinner />
                        ) : (
                            <FetchMore
                                edges={data?.search?.edges || []}
                                getMoreResults={(data) =>
                                    data?.search?.edges || []
                                }
                                fetchMore={fetchMore}
                                fetchCount={10}
                                setLimit={setQueryLimit}
                                maxLimit={total}
                                resultsMessage={t('global.result', {
                                    count: total,
                                })}
                                noResults={noResults}
                                fetchMoreButtonHeight={48}
                            >
                                <CardsContainer subtype={totalsType}>
                                    {data?.search?.edges?.map((entity, i) => (
                                        <Card
                                            key={entity.guid}
                                            data-feed={i}
                                            entity={entity}
                                            viewer={siteData.viewer}
                                            hideGroup={!!groupGuid}
                                            tagCategories={tagCategories}
                                        />
                                    ))}
                                </CardsContainer>
                            </FetchMore>
                        )}
                    </Container>
                </Section>
            </TabPage>
        </ThemeProvider>
    )
}

const GET_SEARCH_CONTAINER = gql`
    query SearchContainer($containerGuid: String) {
        viewer {
            guid
            loggedIn
            user {
                guid
                email
            }
        }
        entity(guid: $containerGuid) {
            guid
            ... on Group {
                name
                ${iconViewFragment}
                url
            }
        }
        site {
            guid
            customTagsAllowed
            searchPublishedFilterEnabled
            searchRelatedSitesEnabled
            tagCategories {
                name
                values
            }
            searchRelatedSites {
                name
                url
            }
            recommendedType
        }
        siteStats {
            archiveSize
        }
    }
`

export default Search
