import React from 'react'
import { Route, Routes } from 'react-router-dom'

import NotFound from 'js/core/NotFound'

import Search from './Search'

const Component = () => (
    <Routes>
        <Route path="/" element={<Search />} />
        <Route element={<NotFound />} />
    </Routes>
)

export default Component
