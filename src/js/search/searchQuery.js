import { gql } from '@apollo/client'

import blogListFragment from 'js/blog/fragments/listFragment'
import discussionListFragment from 'js/discussions/fragments/listFragment'
import eventListFragment from 'js/events/fragments/listFragment'
import { iconViewFragment } from 'js/lib/fragments/icon'
import newsListFragment from 'js/news/fragments/listFragment'
import pageListFragment from 'js/page/listFragment'
import questionListFragment from 'js/questions/fragments/listFragment'
import wikiListFragment from 'js/wiki/fragments/listFragment'

const resultFragment = `
    ... on User {
        guid
        name
        url
        iconUrl: icon
    }
    ... on Group {
        guid
        name
        canEdit
        excerpt
        isClosed
        membership
        memberCount
        ${iconViewFragment}
        url
    }
    ... on File {
        guid
        subtype
        title
        url
        mimeType
        excerpt
        localExcerpt
        download
    }
    ... on Folder {
        guid
        subtype
        title
        url
        hasChildren
        excerpt
        localExcerpt
    }
    ... on Pad {
        guid
        subtype
        title
        url
    }
    ... on Podcast {
        guid
        title
        url
        subtype
        timeCreated
        timeUpdated
        timePublished
        tagCategories {
            name
            values
        }
        tags
    }
    ... on Episode {
        guid
        title
        url
        subtype
        timeCreated
        timeUpdated
        timePublished
        podcast {
            guid
            title
            url
        }
        tagCategories {
            name
            values
        }
        tags
    }
    ... on ExternalContent {
        guid
        title
        tags
        tagCategories {
            name
            values
        }
        description
        timeCreated
        timeUpdated
        timePublished
        source {
            key
            name
        }
        url
    }
    ... on RelatedSiteSearchResult {
        guid
        title
        url
        subtype
        siteName
        siteUrl
        url
        excerpt
        timePublished
        mimeType
        startDate
        endDate
        download
    }
    ...BlogListFragment
    ...DiscussionListFragment
    ...EventListFragment
    ...NewsListFragment
    ...QuestionListFragment
    ...WikiListFragment
    ...PageListFragment
`

const SEARCH_QUERY = gql`
    query SearchQuery(
        $q: String!
        $containerGuid: String
        $subtype: String
        $tagCategories: [TagCategoryInput]
        $dateFrom: String
        $dateTo: String
        $archived: Boolean
        $offset: Int
        $limit: Int
        $orderBy: SearchOrderBy
        $orderDirection: OrderDirection
        $searchRelatedSites: Boolean
    ) {
        search(
            q: $q
            containerGuid: $containerGuid
            offset: $offset
            limit: $limit
            subtype: $subtype
            tagCategories: $tagCategories
            dateFrom: $dateFrom
            dateTo: $dateTo
            filterArchived: $archived
            orderBy: $orderBy
            orderDirection: $orderDirection
            searchRelatedSites: $searchRelatedSites
        ) {
            total # Required for InfiniteList
            totals {
                subtype
                title
                total
            }
            edges {
                ${resultFragment}
            }
        }
        recommendedSearch: search(
            q: $q
            containerGuid: $containerGuid
            offset: 0
            limit: 4
            subtype: $subtype
            tagCategories: $tagCategories
            dateFrom: $dateFrom
            dateTo: $dateTo
            filterArchived: $archived
            orderBy: $orderBy
            orderDirection: $orderDirection
            searchRelatedSites: $searchRelatedSites
            isRecommendedInSearch: true
        ) {
            edges {
                ${resultFragment}
            }
        }
    }
    ${blogListFragment}
    ${discussionListFragment}
    ${eventListFragment}
    ${newsListFragment}
    ${questionListFragment}
    ${wikiListFragment}
    ${pageListFragment}
`
export default SEARCH_QUERY
