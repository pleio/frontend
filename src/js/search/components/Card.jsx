import React from 'react'
import PropTypes from 'prop-types'

import BlogCard from 'js/blog/components/Card'
import Card from 'js/components/Card/Card'
import UserCard from 'js/components/userCard/UserCard'
import DiscussionCard from 'js/discussions/components/Card'
import EventCard from 'js/events/components/Card'
import ExternalContentCard from 'js/externalContent/ExternalContentCard'
import FileFolderCard from 'js/files/components/FileFolderCard'
import GroupCard from 'js/group/List/components/Card'
import NewsCard from 'js/news/components/Card'
import PageCard from 'js/page/Card'
import EpisodeCard from 'js/podcast/components/EpisodeCard'
import PodcastCard from 'js/podcast/components/PodcastCard'
import QuestionCard from 'js/questions/components/Card'
import WikiCard from 'js/wiki/components/Card'

import RelatedSiteResultCard from './RelatedSiteResultCard'

const ActivityCard = ({ entity, ...rest }) => {
    switch (entity.__typename) {
        case 'User':
            return (
                <Card>
                    <UserCard entity={entity} {...rest} />
                </Card>
            )

        case 'Group':
            return <GroupCard entity={entity} {...rest} />

        case 'Page':
            return <PageCard entity={entity} {...rest} />

        case 'Wiki':
            return <WikiCard entity={entity} {...rest} />

        case 'News':
            return <NewsCard entity={entity} {...rest} />

        case 'Blog':
            return <BlogCard entity={entity} {...rest} />

        case 'Discussion':
            return <DiscussionCard entity={entity} {...rest} />

        case 'Event':
            return <EventCard entity={entity} {...rest} />

        case 'Question':
            return <QuestionCard entity={entity} {...rest} />

        case 'File':
        case 'Folder':
        case 'Pad':
            return <FileFolderCard entity={entity} {...rest} />

        case 'ExternalContent':
            return <ExternalContentCard entity={entity} {...rest} />

        case 'RelatedSiteSearchResult':
            return <RelatedSiteResultCard entity={entity} {...rest} />

        case 'Podcast':
            return <PodcastCard entity={entity} {...rest} />

        case 'Episode':
            return <EpisodeCard entity={entity} {...rest} />

        default:
            console.error('Unknown entity: ', entity)
            return null
    }
}

ActivityCard.propTypes = {
    entity: PropTypes.object.isRequired,
    tagCategories: PropTypes.array,
}

export default ActivityCard
