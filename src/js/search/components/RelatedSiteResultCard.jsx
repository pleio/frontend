import React from 'react'

import FeedItem from 'js/components/FeedItem/FeedItem'
import FeedItemContent from 'js/components/FeedItem/FeedItemContent'
import FeedItemFooter from 'js/components/FeedItem/FeedItemFooter'
import FeedItemImage from 'js/components/FeedItem/FeedItemImage'

const RelatedSiteResultCard = ({
    entity,
    hideGroup,
    hideSubtype,
    hideExcerpt,
    hideComments,
    hideLikes,
    excerptMaxLines,
    'data-feed': dataFeed,
}) => {
    return (
        <FeedItem data-feed={dataFeed}>
            <FeedItemContent
                entity={entity}
                hideGroup={hideGroup}
                hideSubtype={hideSubtype}
                hideExcerpt={hideExcerpt}
                excerptMaxLines={excerptMaxLines}
            />
            <FeedItemFooter
                entity={entity}
                hideComments={hideComments}
                hideLikes={hideLikes}
            />
            <FeedItemImage entity={entity} />
        </FeedItem>
    )
}

export default RelatedSiteResultCard
