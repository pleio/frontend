import React from 'react'
import { useTranslation } from 'react-i18next'

import FilterWrapper from 'js/components/FilterWrapper'
import Select from 'js/components/Select/Select'
import setTagFilterHelper from 'js/page/Widget/components/Feed/setTagFilter'

const TagCategoriesFilter = ({
    name,
    tagCategories,
    tagFilter,
    setTagFilter,
}) => {
    const { t } = useTranslation()

    return (
        <FilterWrapper>
            {tagCategories.map((category, index) => {
                const options = category.values.map((tag) => ({
                    value: tag,
                    label: tag,
                }))

                const categoryName = category.name
                const value =
                    tagFilter.find((cat) => cat.name === categoryName)
                        ?.values || []

                const handleChange = (val) => {
                    setTagFilterHelper(setTagFilter, categoryName, val)
                }

                const id = `${name}-tag-category-${index}`

                return (
                    <Select
                        key={id}
                        name={id}
                        label={category.name}
                        placeholder={t('filters.filter-by')}
                        options={options}
                        value={value}
                        onChange={handleChange}
                        isMulti
                    />
                )
            })}
        </FilterWrapper>
    )
}

export default TagCategoriesFilter
