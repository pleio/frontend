import React from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import { format, formatISO } from 'date-fns'

import Checkbox from 'js/components/Checkbox/Checkbox'
import DateField from 'js/components/DateField/DateField'
import FilterWrapper from 'js/components/FilterWrapper'
import { H4 } from 'js/components/Heading'
import Select from 'js/components/Select/Select'
import Spacer from 'js/components/Spacer/Spacer'

import TagCategoriesFilter from './TagCategoriesFilter'

const SearchFilters = ({
    name,
    getSearchLink,
    currentParams = {},
    tagFilter,
    tagCategories,
    dateFrom,
    dateTo,
    archiveSize,
    archived,
    relatedSites,
    searchPublishedFilterEnabled,
    showRelatedSites,
    showDateFilter,
    typeFilter,
    searchRelatedSites,
    showTypeFilter,
    typeOptions,
    ...rest
}) => {
    const { t } = useTranslation()
    const navigate = useNavigate()

    const today = formatISO(new Date())

    const updateUrl = (params) => {
        navigate(
            getSearchLink({
                ...currentParams,
                ...params,
            }),
        )
    }

    const changeDateFrom = (val) => {
        updateUrl({ datefrom: val ? format(val, 'yyyy-MM-dd') : '' })
    }

    const changeDateTo = (val) => {
        updateUrl({ dateto: val ? format(val, 'yyyy-MM-dd') : '' })
    }

    const setTagFilter = (produce) => {
        const newTagFilter = produce(tagFilter || [])

        updateUrl({
            tags: newTagFilter.length ? JSON.stringify(newTagFilter) : null,
        })
    }

    const toggleFilterArchived = ({ target }) => {
        updateUrl({ archived: target.checked })
    }

    const toggleSearchRelatedSites = ({ target }) => {
        updateUrl({ relatedsites: target.checked })
    }

    const setTypeFilter = (val) => {
        updateUrl({ subtype: val })
    }

    return (
        <Spacer spacing="tiny" {...rest}>
            {(showRelatedSites || archiveSize > 0) && (
                <div>
                    {showRelatedSites && searchRelatedSites && (
                        <Checkbox
                            name={`${name}-filter-related-sites`}
                            label={`${t('search.related-sites')}: ${
                                searchRelatedSites
                                    ?.map((item) => item.name)
                                    .join(', ') || ''
                            }`}
                            onChange={toggleSearchRelatedSites}
                            defaultChecked={relatedSites}
                            size="small"
                        />
                    )}
                    {archiveSize > 0 && (
                        <Checkbox
                            name={`${name}-filter-archived`}
                            label={t('search.filter-archived')}
                            onChange={toggleFilterArchived}
                            defaultChecked={archived}
                            size="small"
                        />
                    )}
                </div>
            )}

            {showTypeFilter && typeOptions?.length > 0 && (
                <FilterWrapper>
                    <Select
                        label={t('global.type')}
                        name={`${name}-typeFilter`}
                        placeholder={t('filters.filter-by')}
                        value={typeFilter}
                        options={typeOptions}
                        onChange={setTypeFilter}
                    />
                </FilterWrapper>
            )}

            {!relatedSites && tagCategories.length > 0 && (
                <div>
                    <H4 as="h3" style={{ marginBottom: '8px' }}>
                        {t('form.tags')}
                    </H4>
                    <TagCategoriesFilter
                        name={name}
                        tagCategories={tagCategories}
                        tagFilter={tagFilter}
                        setTagFilter={setTagFilter}
                    />
                </div>
            )}

            {showDateFilter && searchPublishedFilterEnabled && (
                <div>
                    <H4 as="h3" style={{ marginBottom: '8px' }}>
                        {t('global.date')}
                    </H4>
                    <FilterWrapper>
                        <DateField
                            name={`${name}-date-from`}
                            label={t('search.date-from')}
                            ariaLabelIconButton={t(
                                'search.show-calendar-for-date-from',
                            )}
                            value={dateFrom}
                            toDate={dateTo || today}
                            onChange={changeDateFrom}
                            clearLabel={t('search.clear-date-from')}
                            isClearable
                        />
                        <DateField
                            name={`${name}-date-to`}
                            label={t('search.date-to')}
                            ariaLabelIconButton={t(
                                'search.show-calendar-for-date-to',
                            )}
                            value={dateTo}
                            fromDate={dateFrom}
                            toDate={today}
                            onChange={changeDateTo}
                            clearLabel={t('search.clear-date-to')}
                            isClearable
                        />
                    </FilterWrapper>
                </div>
            )}
        </Spacer>
    )
}

export default SearchFilters
