import React from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation } from 'react-router-dom'

import TabMenu from 'js/components/TabMenu'

const SearchTabs = ({ total, totals, getSearchLink, firstTabLabel }) => {
    const { t } = useTranslation()
    const location = useLocation()

    if (!totals || total === 0) return null

    const tabs = [
        {
            id: `search-tab-${firstTabLabel}`,
            link: getSearchLink({ subtype: null }),
            label: `${t(firstTabLabel)}`,
            isActive:
                location.pathname + location.search ===
                getSearchLink({ subtype: null }),
            ariaControls: `search-results-${firstTabLabel}`,
        },
        ...(totals.length > 1
            ? totals.map(({ title, subtype, total }) => {
                  const searchLink = getSearchLink({
                      subtype,
                  })

                  return {
                      id: `search-tab-${subtype}`,
                      link: searchLink,
                      label: `${title} (${total})`,
                      isActive:
                          location.pathname + location.search === searchLink,
                      ariaControls: `search-results-${subtype}`,
                  }
              })
            : []),
    ]

    return (
        <TabMenu
            options={tabs}
            style={{
                display: 'flex',
                justifyContent: 'center',
            }}
            label={t('search.search-results-by-type')}
        />
    )
}

export default SearchTabs
