import React from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'

import { AriaLiveMessage } from 'js/components/AriaLive'
import FilterWrapper from 'js/components/FilterWrapper'
import Select from 'js/components/Select/Select'
import Text from 'js/components/Text/Text'

const ResultsHeader = ({
    total,
    orderBy,
    orderDirection,
    getSearchLink,
    ...rest
}) => {
    const { t } = useTranslation()
    const navigate = useNavigate()

    if (!total) return null

    const handleChangeSorting = (val) => {
        if (val === 'score') {
            navigate(
                getSearchLink({
                    orderby: '',
                    orderdirection: '',
                }),
            )
        } else {
            const splitVal = val.split('-')
            navigate(
                getSearchLink({
                    orderby: splitVal[0],
                    orderdirection: splitVal[1],
                }),
            )
        }
    }

    const options = [
        {
            value: 'score',
            label: t('filters.sorting-relevance'),
        },
        {
            value: 'timePublished-desc',
            label: t('filters.sorting-published'),
        },
        {
            value: 'timePublished-asc',
            label: t('filters.sorting-published-old'),
        },
        {
            value: 'title-asc',
            label: t('filters.sorting-title'),
        },
        {
            value: 'title-desc',
            label: t('filters.sorting-title-old'),
        },
    ]

    let value = 'score'
    if (!!orderBy && !!orderDirection) {
        value = `${orderBy}-${orderDirection}`
    }

    return (
        <FilterWrapper {...rest}>
            <AriaLiveMessage
                message={t('global.result', {
                    count: total,
                })}
            />
            <Text
                as="h2"
                size="small"
                variant="grey"
                style={{
                    display: 'flex',
                    alignItems: 'center',
                    marginRight: 'auto',
                }}
            >
                {t('global.result', {
                    count: total,
                })}
            </Text>
            <div>
                <Select
                    name="sort-search-results"
                    label={t('search.sort-by')}
                    options={options}
                    value={value}
                    onChange={handleChangeSorting}
                />
            </div>
        </FilterWrapper>
    )
}

export default ResultsHeader
