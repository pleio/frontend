import React, { useState } from 'react'

import SearchContext from './SearchContext'

const SearchProvider = (props) => {
    const [searchQuery, setSearchQuery] = useState('')

    return (
        <SearchContext.Provider value={{ searchQuery, setSearchQuery }}>
            {props.children}
        </SearchContext.Provider>
    )
}

export default SearchProvider
