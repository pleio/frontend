import React, { useState } from 'react'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import Document from 'js/components/Document'
import { Container } from 'js/components/Grid/Grid'
import HideVisually from 'js/components/HideVisually/HideVisually'
import Section from 'js/components/Section/Section'
import NotFound from 'js/core/NotFound'

import FilesView from './FilesView'

const Files = () => {
    const { containerGuid } = useParams()

    const { data } = useQuery(GET_FILES_CONTAINER, {
        variables: {
            containerGuid,
        },
    })

    const [selected, setSelected] = useState([])

    if (containerGuid && data?.entity === null) {
        return <NotFound />
    }

    return (
        <>
            <Document title={data?.entity?.title} />
            <Section backgroundColor="white" grow>
                <HideVisually as="h1">{data?.entity?.title}</HideVisually>
                <Container $noPadding>
                    <FilesView
                        selected={selected}
                        setSelected={setSelected}
                        selectable="multiple"
                    />
                </Container>
            </Section>
        </>
    )
}

const GET_FILES_CONTAINER = gql`
    query FilesContainer($containerGuid: String) {
        entity(guid: $containerGuid) {
            guid
            ... on Folder {
                title
            }
        }
    }
`

export default Files
