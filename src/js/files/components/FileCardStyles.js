import styled from 'styled-components'

import Card from 'js/components/Card/Card'

const CardFileCircle = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 40px;
    height: 40px;
    background-color: ${(p) => p.theme.color.grey[10]};
    border-radius: 50%;
    overflow: hidden;
`

const FileCardWrapperStyled = styled(Card)`
    margin-bottom: 20px;
`

export { CardFileCircle, FileCardWrapperStyled }
