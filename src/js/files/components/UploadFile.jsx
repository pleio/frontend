import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql, useMutation, useQuery } from '@apollo/client'
import { convertFileListToArray } from 'helpers'

import Button from 'js/components/Button/Button'
import CustomTagsField from 'js/components/CustomTagsField'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import AccessField from 'js/components/Form/AccessField'
import FormItem from 'js/components/Form/FormItem'
import { Col, Row } from 'js/components/Grid/Grid'
import ConfirmationModal from 'js/components/Modal/ConfirmationModal'
import Spacer from 'js/components/Spacer/Spacer'
import TagCategoriesField from 'js/components/TagCategories/TagCategoriesField'
import Text from 'js/components/Text/Text'
import UploadBox from 'js/components/UploadBox/UploadBox'
import { MAX_FILE_SIZE } from 'js/lib/constants'
import useCustomTags from 'js/page/Widget/hooks/useCustomTags'

const UploadFile = ({
    groupGuid,
    containerGuid,
    onClose,
    defaultTags,
    defaultTagCategories,
}) => {
    const { t } = useTranslation()

    const [mutate] = useMutation(ADD_FILE)
    const { data, loading } = useQuery(GET_DATA, {
        variables: {
            groupGuid,
        },
    })

    const [errors, setErrors] = useState('')

    const {
        control,
        handleSubmit,
        formState: { isSubmitting },
    } = useForm()

    const [categoryTags, setCategoryTags] = useState(defaultTagCategories || [])

    const { customTags, handleAddTags, handleRemoveTags } =
        useCustomTags(defaultTags)

    const [files, setFiles] = useState([])
    const onSubmitFiles = (value) => {
        setFiles(value)
    }

    const [showPublishRequestModal, setShowPublishRequestModal] =
        useState(false)

    if (loading) return null
    const { site, viewer } = data

    const {
        contentTranslation,
        languageOptions,
        fileDescriptionFieldEnabled,
        tagCategories,
        customTagsAllowed,
    } = site
    const showInputLanuage = contentTranslation && languageOptions?.length > 1

    const onSubmit = async ({
        richDescription,
        inputLanguage,
        accessId,
        writeAccessId,
    }) => {
        await Promise.all(
            convertFileListToArray(files).map((file) => {
                return mutate({
                    variables: {
                        input: {
                            containerGuid,
                            accessId: parseInt(accessId, 10),
                            writeAccessId: parseInt(writeAccessId, 10),
                            file,
                            richDescription,
                            inputLanguage,
                            tagCategories: categoryTags.map(
                                ({ name, values }) => ({ name, values }),
                            ), // Filter out "__typename" field which can lead to graphql error
                            tags: customTags.toJS(),
                        },
                    },
                    refetchQueries: ['FilesList'],
                })
            }),
        )
            .then(() => {
                onClose()
            })
            .catch((errors) => {
                console.error(errors)
                setErrors(errors)
            })
    }

    const onError = (errors) => {
        return new Promise((resolve, reject) => {
            reject(new Error(errors))
        })
    }

    const submitForm = handleSubmit(onSubmit, onError)

    const preSubmit = () => {
        if (files.length === 0) return

        if (viewer.requiresContentModeration) {
            setShowPublishRequestModal(true)
            return
        }

        submitForm()
    }

    return (
        <>
            <Spacer as="form" spacing="small">
                <UploadBox
                    name="files"
                    maxSize={MAX_FILE_SIZE.file}
                    multiple
                    onSubmit={onSubmitFiles}
                >
                    {files?.length > 0 &&
                        files.map((file, index) => (
                            <div key={index}>{file.name}</div>
                        ))}
                </UploadBox>
                {errors && <Errors errors={errors} />}
                {fileDescriptionFieldEnabled && (
                    <>
                        <FormItem
                            control={control}
                            type="rich"
                            name="richDescription"
                            title={t('form.description')}
                            options={{
                                textLink: true,
                            }}
                        />
                        {showInputLanuage && (
                            <FormItem
                                control={control}
                                type="select"
                                name="inputLanguage"
                                title={t('settings.language')}
                                options={languageOptions}
                            />
                        )}
                    </>
                )}
                <TagCategoriesField
                    name="filters"
                    tagCategories={tagCategories}
                    value={categoryTags}
                    setTags={setCategoryTags}
                />
                {customTagsAllowed && (
                    <CustomTagsField
                        name="customTags"
                        value={customTags}
                        onAddTag={handleAddTags}
                        onRemoveTag={handleRemoveTags}
                    />
                )}
                <Row>
                    {viewer?.canUpdateAccessLevel && (
                        <Col mobileLandscapeUp={1 / 2}>
                            <AccessField
                                control={control}
                                name="accessId"
                                label={t('permissions.read-access')}
                                containerGuid={containerGuid}
                            />
                        </Col>
                    )}
                    <Col
                        mobileLandscapeUp={
                            viewer?.canUpdateAccessLevel ? 1 / 2 : 1
                        }
                    >
                        <AccessField
                            control={control}
                            name="writeAccessId"
                            label={t('permissions.write-access')}
                            containerGuid={containerGuid}
                        />
                    </Col>
                </Row>
                <Flexer mt>
                    <Button size="normal" variant="tertiary" onClick={onClose}>
                        {t('action.cancel')}
                    </Button>
                    <Button
                        size="normal"
                        variant="primary"
                        disabled={files.length === 0}
                        loading={isSubmitting}
                        onClick={preSubmit}
                    >
                        {t('action.upload')}
                    </Button>
                </Flexer>
            </Spacer>

            <ConfirmationModal
                isVisible={showPublishRequestModal}
                onCancel={() => setShowPublishRequestModal(false)}
                onConfirm={() => {
                    setShowPublishRequestModal(false)
                    submitForm()
                }}
                title={t('action.publish')}
                cancelLabel={t('action.cancel')}
                confirmLabel={t('action.send-request')}
            >
                <Spacer>
                    <Text size="small">
                        {t('content-moderation.send-request-helper')}
                    </Text>
                    <Text size="small">
                        {t('content-moderation.send-request-confirm')}
                    </Text>
                </Spacer>
            </ConfirmationModal>
        </>
    )
}

const GET_DATA = gql`
    query fileContainer($groupGuid: String) {
        site {
            guid
            contentTranslation
            languageOptions {
                value
                label
            }
            tagCategories {
                name
                values
            }
            customTagsAllowed
            fileDescriptionFieldEnabled
        }
        viewer {
            guid
            canUpdateAccessLevel
            requiresContentModeration(subtype: "file", groupGuid: $groupGuid)
        }
    }
`

const ADD_FILE = gql`
    mutation addFile($input: addFileInput!) {
        addFile(input: $input) {
            entity {
                guid
            }
        }
    }
`

export default UploadFile
