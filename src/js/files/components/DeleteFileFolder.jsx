import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'

import Errors from '../../components/Errors'

const DeleteFileFolder = ({ mutate, entities, onClose, onComplete }) => {
    const [errors, setErrors] = useState([])

    const handleSubmit = () => {
        return new Promise((resolve, reject) => {
            setErrors([])

            Promise.all(
                entities.map((entity) => {
                    return mutate({
                        variables: {
                            input: {
                                guid: entity.guid,
                            },
                        },
                    })
                }),
            )
                .then(() => {
                    resolve()
                    onComplete ? onComplete() : location.reload()
                })
                .catch((errors) => {
                    reject(new Error(errors))
                    setErrors(errors)
                })
        })
    }

    const { t } = useTranslation()

    let message
    if (entities.length === 1) {
        message = (
            <p>
                {t('entity-file.confirm-delete', {
                    item: entities[0].title,
                })}
            </p>
        )
    } else {
        message = (
            <p>
                {t('entity-file.confirm-delete-multiple', {
                    count: entities.length,
                })}
            </p>
        )
    }

    return (
        <>
            {message}
            <Errors errors={errors} />
            <Flexer mt>
                <Button size="normal" variant="tertiary" onClick={onClose}>
                    {t('action.cancel')}
                </Button>
                <Button size="normal" variant="primary" onHandle={handleSubmit}>
                    {t('action.delete')}
                </Button>
            </Flexer>
        </>
    )
}

const Mutation = gql`
    mutation deleteEntity($input: deleteEntityInput!) {
        deleteEntity(input: $input) {
            success
        }
    }
`

export default graphql(Mutation)(DeleteFileFolder)
