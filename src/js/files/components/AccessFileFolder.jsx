import React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import AccessField from 'js/components/Form/AccessField'
import FormItem from 'js/components/Form/FormItem'
import { Col, Row } from 'js/components/Grid/Grid'
import Spacer from 'js/components/Spacer/Spacer'

const AccessFileFolder = ({
    containerGuid,
    entity,
    onClose,
    onComplete,
    viewer,
}) => {
    const [mutate] = useMutation(EDIT_FILE_FOLDER)

    const submit = async ({ accessId, writeAccessId, isAccessRecursive }) => {
        await mutate({
            variables: {
                input: {
                    guid: entity.guid,
                    accessId: parseInt(accessId, 10),
                    writeAccessId: parseInt(writeAccessId, 10),
                    isAccessRecursive,
                },
                // TODO: show new values after opening modal again (update selected?)
            },
        })
            .then(() => {
                onComplete()
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    const { t } = useTranslation()

    const defaultValues = {
        accessId: entity ? entity.accessId : null,
        writeAccessId: entity ? entity.writeAccessId : null,
        isAccessRecursive: true,
    }

    const {
        control,
        handleSubmit,
        formState: { isSubmitting },
    } = useForm({
        defaultValues,
    })

    if (!entity) return null

    return (
        <Spacer as="form" spacing="small" onSubmit={handleSubmit(submit)}>
            <Row>
                {viewer.canUpdateAccessLevel && (
                    <Col mobileLandscapeUp={1 / 2}>
                        <AccessField
                            control={control}
                            name="accessId"
                            label={t('permissions.read-access')}
                            containerGuid={containerGuid}
                        />
                    </Col>
                )}
                <Col
                    mobileLandscapeUp={viewer.canUpdateAccessLevel ? 1 / 2 : 1}
                >
                    <AccessField
                        control={control}
                        name="writeAccessId"
                        label={t('permissions.write-access')}
                        containerGuid={containerGuid}
                    />
                </Col>
            </Row>
            {entity.subtype === 'folder' && (
                <FormItem
                    control={control}
                    type="checkbox"
                    name="isAccessRecursive"
                    label={t(
                        'entity-file.apply-to-underlying-folders-and-files',
                    )}
                />
            )}
            <Flexer mt>
                <Button size="normal" variant="tertiary" onClick={onClose}>
                    {t('action.cancel')}
                </Button>
                <Button
                    size="normal"
                    variant="primary"
                    type="submit"
                    loading={isSubmitting}
                >
                    {t('action.save')}
                </Button>
            </Flexer>
        </Spacer>
    )
}

const EDIT_FILE_FOLDER = gql`
    mutation editFileFolder($input: editFileFolderInput!) {
        editFileFolder(input: $input) {
            entity {
                guid
                ... on Folder {
                    accessId
                    writeAccessId
                }
                ... on File {
                    accessId
                    writeAccessId
                }
                ... on Pad {
                    accessId
                    writeAccessId
                }
            }
        }
    }
`

export default AccessFileFolder
