import React from 'react'
import { useTranslation } from 'react-i18next'

import iconDoc from 'icons/file-doc.svg'
import iconFolder from 'icons/file-folder.svg'
import iconFolderFilled from 'icons/file-folder-filled.svg'
import iconGif from 'icons/file-gif.svg'
import iconJpg from 'icons/file-jpg.svg'
import iconM4a from 'icons/file-m4a.svg'
import iconMp3 from 'icons/file-mp3.svg'
import iconOther from 'icons/file-other.svg'
import iconPad from 'icons/file-pad.svg'
import iconPdf from 'icons/file-pdf.svg'
import iconPng from 'icons/file-png.svg'
import iconPpt from 'icons/file-ppt.svg'
import iconRar from 'icons/file-rar.svg'
import iconRtf from 'icons/file-rtf.svg'
import iconTxt from 'icons/file-txt.svg'
import iconXls from 'icons/file-xls.svg'
import iconZip from 'icons/file-zip.svg'

const FileFolderIcon = ({ name, ...rest }) => {
    const getType = () => {
        switch (name) {
            case 'doc':
                return iconDoc

            case 'folder':
                return iconFolder

            case 'folder-filled':
                return iconFolderFilled

            case 'gif':
                return iconGif

            case 'jpg':
                return iconJpg

            case 'pdf':
                return iconPdf

            case 'png':
                return iconPng

            case 'ppt':
                return iconPpt

            case 'rar':
                return iconRar

            case 'rtf':
                return iconRtf

            case 'txt':
                return iconTxt

            case 'xls':
                return iconXls

            case 'zip':
                return iconZip

            case 'pad':
                return iconPad

            case 'mp3':
                return iconMp3

            case 'm4a':
                return iconM4a

            default:
                return iconOther
        }
    }

    const Element = getType()

    const { t } = useTranslation()

    if (!Element) return null

    const isFolder = ['folder', 'folder-filled'].includes(name)

    return (
        <Element
            role="img"
            aria-hidden="false"
            aria-label={
                isFolder
                    ? t(`entity-file.type-${name}`)
                    : `${t('entity-file.file-type')}: ${t(`entity-file.type-${name}`)}`
            }
            {...rest}
        />
    )
}

export default FileFolderIcon
