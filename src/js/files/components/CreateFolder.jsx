import React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import AccessField from 'js/components/Form/AccessField'
import FormItem from 'js/components/Form/FormItem'
import { Col, Row } from 'js/components/Grid/Grid'
import Spacer from 'js/components/Spacer/Spacer'

const CreateFolder = ({ containerGuid, viewer, onComplete, onClose }) => {
    const [mutate] = useMutation(ADD_FOLDER)

    const submit = async ({ title, accessId, writeAccessId }) => {
        await mutate({
            variables: {
                input: {
                    type: 'object',
                    subtype: 'folder',
                    title,
                    containerGuid,
                    accessId: parseInt(accessId, 10),
                    writeAccessId: parseInt(writeAccessId, 10),
                },
            },
            refetchQueries: ['FilesList'],
        })
            .then(() => {
                onComplete()
                onClose()
            })
            .catch((errors) => {
                console.error(errors)
            })
    }
    const { t } = useTranslation()

    const defaultValues = {
        title: '',
    }

    const {
        control,
        handleSubmit,
        formState: { isValid, isSubmitting },
    } = useForm({ mode: 'onChange', defaultValues })

    return (
        <Spacer as="form" spacing="small" onSubmit={handleSubmit(submit)}>
            <FormItem
                type="text"
                control={control}
                name="title"
                label={t('global.name')}
                required
            />
            <Row>
                {viewer?.canUpdateAccessLevel && (
                    <Col mobileLandscapeUp={1 / 2}>
                        <AccessField
                            control={control}
                            name="accessId"
                            label={t('permissions.read-access')}
                            containerGuid={containerGuid}
                        />
                    </Col>
                )}
                <Col
                    mobileLandscapeUp={viewer?.canUpdateAccessLevel ? 1 / 2 : 1}
                >
                    <AccessField
                        control={control}
                        name="writeAccessId"
                        label={t('permissions.write-access')}
                        containerGuid={containerGuid}
                    />
                </Col>
            </Row>
            <Flexer mt>
                <Button size="normal" variant="tertiary" onClick={onClose}>
                    {t('action.cancel')}
                </Button>
                <Button
                    size="normal"
                    variant="primary"
                    type="submit"
                    disabled={!isValid}
                    loading={isSubmitting}
                >
                    {t('action.create')}
                </Button>
            </Flexer>
        </Spacer>
    )
}

const ADD_FOLDER = gql`
    mutation addFolder($input: addEntityInput!) {
        addEntity(input: $input) {
            entity {
                guid
            }
        }
    }
`

export default CreateFolder
