import React, { forwardRef, useContext } from 'react'
import { Link, useLocation } from 'react-router-dom'
import styled, { css } from 'styled-components'

import Checkbox from 'js/components/Checkbox/Checkbox'
import DisplayDate from 'js/components/DisplayDate'
import Tooltip from 'js/components/Tooltip/Tooltip'
import { FilesContext } from 'js/files/FilesView'

import EarthIcon from 'icons/earth.svg'
import PrivateIcon from 'icons/eye-off.svg'
import GroupIcon from 'icons/group.svg'
import StarFillIcon from 'icons/star-small-fill.svg'
import SubgroupIcon from 'icons/subgroup.svg'
import UsersIcon from 'icons/users.svg'

import getIconNameByMimetype from '../helpers/getIconNameByMimetype'

import FileFolderIcon from './FileFolderIcon'

const Wrapper = styled.tr`
    ${(p) =>
        p.$isSelected &&
        css`
            background-color: ${(p) => p.theme.color.grey[10]};
        `};

    .FileFolderSelect {
        width: 68px;
        display: flex;
        align-items: center;
        padding-left: 12px;

        ${(p) =>
            !p.$selectableTitle &&
            css`
                position: relative;
            `};

        > svg {
            width: 40px;
        }
    }

    .FileFolderOwner {
        text-align: right;
    }

    .FileFolderTitle {
        display: flex;
        align-items: center;
        overflow: hidden;
        flex-grow: 1; // Enlarge clickable area

        span {
            overflow: hidden;
            text-overflow: ellipsis;
        }
    }

    a.FileFolderTitle:hover,
    button.FileFolderTitle:hover {
        color: ${(p) => p.theme.color.primary.main};
        text-decoration: underline;
    }

    .FileFolderAccess {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .FileFolderStar {
        margin-left: 6px;
        color: ${(p) => p.theme.color.secondary.main};
    }
`

const FileFolder = forwardRef(
    (
        {
            rootContainerGuid,
            canEditRoot,
            entity,
            isFocused,
            isSelected,
            orderBy,
            orderDirection,
            onToggle,
            accessIds,
            showExtraColumns,
            disabledGuids,
        },
        ref,
    ) => {
        const { pathname } = useLocation()
        const { clickToSelect, setContainerGuid, selectableType } =
            useContext(FilesContext)

        const {
            guid,
            title,
            mimeType,
            subtype,
            timePublished,
            timeUpdated,
            owner,
            isBookmarked,
            hasChildren,
        } = entity

        const iconName =
            subtype === 'folder'
                ? hasChildren
                    ? 'folder-filled'
                    : 'folder'
                : subtype === 'pad'
                  ? 'pad'
                  : getIconNameByMimetype(mimeType)

        const accessIcons = {
            0: <PrivateIcon />,
            1: <UsersIcon />,
            2: <EarthIcon />,
            4: <GroupIcon />,
            5: <SubgroupIcon />,
        }

        const read = canEditRoot
            ? {
                  label: accessIds?.find((el) => el.id === entity.accessId)
                      ?.description,
                  icon: accessIcons[entity.accessId > 4 ? 5 : entity.accessId],
              }
            : null
        const write = canEditRoot
            ? {
                  label: accessIds?.find((el) => el.id === entity.writeAccessId)
                      ?.description,
                  icon: accessIcons[
                      entity.writeAccessId > 4 ? 5 : entity.writeAccessId
                  ],
              }
            : null

        const selectableTitle = clickToSelect && subtype !== 'folder'

        const StarElement = isBookmarked ? (
            <StarFillIcon className="FileFolderStar" />
        ) : null

        const isImage = mimeType?.startsWith('image/')
        const isFolder = subtype === 'folder'
        const isDisabled = disabledGuids?.includes(guid)
        const canSelect =
            (selectableType === 'all' ||
                (selectableType === 'image' && isImage) ||
                (selectableType === 'folder' && isFolder)) &&
            !isDisabled

        return (
            <Wrapper
                ref={ref}
                $isSelected={isSelected}
                $selectableTitle={selectableTitle}
                className={isFocused ? 'focused' : ''}
            >
                <td
                    style={{
                        display: 'flex',
                        position: 'relative',
                    }}
                >
                    <div className="FileFolderSelect">
                        <Checkbox
                            name={`file-${guid}`}
                            checked={isSelected}
                            disabled={!canSelect}
                            size="small"
                            onChange={onToggle}
                            releaseInputArea
                            tabIndex="-1" // Keyboard selection is handled by listbox in FilesView
                        />
                        <FileFolderIcon name={iconName} />
                    </div>
                    {selectableTitle ? (
                        <span className="FileFolderTitle" title={title}>
                            <span>{title}</span>
                            {StarElement}
                        </span>
                    ) : clickToSelect && subtype === 'folder' ? (
                        <button
                            type="button"
                            className="FileFolderTitle"
                            title={title}
                            tabIndex="-1" // Keyboard selection is handled by listbox in FilesView
                            onClick={() => setContainerGuid(guid)}
                            disabled={isDisabled}
                        >
                            <span>{title}</span>
                            {StarElement}
                        </button>
                    ) : (
                        <Link
                            className="FileFolderTitle"
                            to={entity.url}
                            state={{
                                rootContainerGuid,
                                orderBy,
                                orderDirection,
                                prevPathname: pathname,
                            }}
                            title={title}
                            tabIndex="-1" // Keyboard selection is handled by listbox in FilesView
                        >
                            <span>{title}</span>
                            {StarElement}
                        </Link>
                    )}
                </td>
                {showExtraColumns && (
                    <>
                        <td className="FileFolderOwner">{owner.name}</td>
                        <td style={{ textAlign: 'right' }}>
                            <DisplayDate
                                date={
                                    orderBy === 'timePublished'
                                        ? timePublished
                                        : timeUpdated
                                }
                            />
                        </td>
                        {canEditRoot && (
                            <>
                                <td
                                    style={{
                                        position: 'relative',
                                    }}
                                >
                                    <Tooltip
                                        content={read.label}
                                        offset={0}
                                        placement="left"
                                    >
                                        <div
                                            className="FileFolderAccess"
                                            style={{ padding: '0 4px 0 0' }}
                                        >
                                            {read.icon}
                                        </div>
                                    </Tooltip>
                                </td>
                                <td
                                    style={{
                                        position: 'relative',
                                    }}
                                >
                                    <Tooltip
                                        content={write.label}
                                        offset={0}
                                        placement="right"
                                    >
                                        <div
                                            className="FileFolderAccess"
                                            style={{
                                                margin: '0 8px 0 0',
                                            }}
                                        >
                                            {write.icon}
                                        </div>
                                    </Tooltip>
                                </td>
                            </>
                        )}
                    </>
                )}
            </Wrapper>
        )
    },
)

FileFolder.displayName = 'FileFolder'

export default FileFolder
