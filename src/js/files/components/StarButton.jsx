import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import StarIcon from 'icons/star.svg'
import StarFillIcon from 'icons/star-fill.svg'

import ActionButton from './ActionButton'

const StarButton = ({ mutate, entity }) => {
    const { t } = useTranslation()

    const { guid, __typename } = entity

    const [isBookmarked, setIsBookmarked] = useState(entity.isBookmarked)

    const toggleStar = () => {
        const isAdding = !isBookmarked

        mutate({
            variables: {
                input: {
                    guid,
                    isAdding,
                },
            },
            optimisticResponse: {
                bookmark: {
                    __typename: 'bookmarkPayload',
                    object: {
                        __typename,
                        guid,
                        isBookmarked: isAdding,
                    },
                },
            },
        })
            .then(({ data }) => {
                setIsBookmarked(data?.bookmark?.object?.isBookmarked) // Passed entity is not automatically updated so we update it locally
            })
            .catch((error) => {
                console.error(error)
            })
    }

    return (
        <ActionButton
            variant={isBookmarked ? 'primary' : 'secondary'}
            tooltip={
                isBookmarked
                    ? t('entity-file.remove-favorite')
                    : t('entity-file.save-as-favorite')
            }
            onClick={toggleStar}
        >
            {isBookmarked ? <StarFillIcon /> : <StarIcon />}
        </ActionButton>
    )
}

const BOOKMARK_FILEFOLDER = gql`
    mutation Bookmark($input: bookmarkInput!) {
        bookmark(input: $input) {
            object {
                guid
                ... on FileFolder {
                    isBookmarked
                }
            }
        }
    }
`

export default graphql(BOOKMARK_FILEFOLDER)(StarButton)
