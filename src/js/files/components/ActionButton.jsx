import React, { forwardRef } from 'react'

import IconButton from 'js/components/IconButton/IconButton'

const ActionButton = forwardRef(({ ...rest }, ref) => (
    <IconButton
        ref={ref}
        size="large"
        variant="primary"
        radiusStyle="rounded"
        placement="top"
        offset={[0, 0]}
        {...rest}
    />
))

ActionButton.displayName = 'ActionButton'

export default ActionButton
