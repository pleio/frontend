import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import styled from 'styled-components'

type Size = 'tiny' | 'small'

const Wrapper = styled.ul<{
    size: Size
}>`
    display: inline;
    margin-top: 8px;

    li {
        display: inline;
        padding-right: 6px;
        margin-right: 6px;

        &:first-child {
            flex-shrink: 0;

            .Breadcrumb {
                padding-left: 0;
            }
        }

        &:last-child {
            .Breadcrumb {
                padding-right: 0;
            }
        }

        &:not(:last-child) {
            position: relative;

            &::after {
                color: ${(p) => p.theme.color.icon.grey};
                content: '›' / '';
                font-size: larger;
                position: absolute;
            }
        }
    }

    .Breadcrumb {
        display: inline;
        position: relative;
        min-width: 20px;
        font-size: ${(p) => p.theme.font.size[p.size]};
        line-height: ${(p) => p.theme.font.lineHeight[p.size]};
        padding-right: 6px;

        &:not([aria-current]) {
            color: ${(p) => p.theme.color.text.grey};

            &:hover {
                color: ${(p) => p.theme.color.text.black};
                text-decoration: underline;
            }
        }

        &[aria-current] {
            font-weight: ${(p) => p.theme.font.weight.semibold};
            color: ${(p) => p.theme.color.text.black};
        }
    }

    svg {
        color: ${(p) => p.theme.color.icon.grey};
    }
`

type BreadcrumbProps = {
    clickToSelect: boolean
    setContainerGuid: (guid: string) => void
    size?: Size
    guid: string
}

const Breadcrumb = ({
    clickToSelect,
    setContainerGuid,
    size = 'small',
    guid,
    ...rest
}: BreadcrumbProps) => {
    const { t } = useTranslation()

    const { data } = useQuery(GET_BREADCRUMB, {
        variables: { guid },
        skip: !guid,
        fetchPolicy: 'cache-and-network',
    })

    if (!data) return null

    const { entity, breadcrumb = [] } = data

    const rootUrl = entity?.rootContainer?.guid
        ? entity?.rootContainer?.url
            ? `${entity.rootContainer.url}/files`
            : `/user/${entity.rootContainer.guid}/files`
        : null

    const hasRoot = ['User', 'Group'].includes(entity?.__typename)

    return (
        <Wrapper size={size} {...rest}>
            {rootUrl && breadcrumb?.length > 0 ? (
                <li>
                    {clickToSelect ? (
                        <button
                            type="button"
                            className="Breadcrumb"
                            onClick={() =>
                                setContainerGuid(entity?.rootContainer?.guid)
                            }
                        >
                            {t('global.files')}
                        </button>
                    ) : (
                        <Link to={`${rootUrl}`} className="Breadcrumb">
                            {t('global.files')}
                        </Link>
                    )}
                </li>
            ) : hasRoot ? (
                <li>
                    <h2 className="Breadcrumb" aria-current>
                        {t('global.files')}
                    </h2>
                </li>
            ) : null}

            {breadcrumb.map(({ guid, title, url }, i: number) => (
                <li key={guid}>
                    {breadcrumb.length - 1 === i ? (
                        <h2 className="Breadcrumb" title={title} aria-current>
                            {title}
                        </h2>
                    ) : clickToSelect ? (
                        <button
                            type="button"
                            className="Breadcrumb"
                            title={title}
                            onClick={() => setContainerGuid(guid)}
                        >
                            {title}
                        </button>
                    ) : (
                        <Link to={url} className="Breadcrumb" title={title}>
                            {title}
                        </Link>
                    )}
                </li>
            ))}
        </Wrapper>
    )
}

const GET_BREADCRUMB = gql`
    query Breadcrumb($guid: String!) {
        entity(guid: $guid) {
            guid
            ... on FileFolder {
                rootContainer {
                    guid
                    ... on Group {
                        url
                    }
                }
            }
        }
        breadcrumb(guid: $guid) {
            ... on Folder {
                guid
                title
                url
            }
        }
    }
`

export default Breadcrumb
