import React, { forwardRef, useContext } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import styled from 'styled-components'

import Checkbox from 'js/components/Checkbox/Checkbox'
import { Col } from 'js/components/Grid/Grid'
import Truncate from 'js/components/Truncate/Truncate'
import { FilesContext } from 'js/files/FilesView'

import getIconNameByMimetype from '../helpers/getIconNameByMimetype'

import FileFolderIcon from './FileFolderIcon'

const Wrapper = styled.div`
    body:not(.mouse-user) &.focused {
        outline: ${(p) => p.theme.focusStyling};
    }

    cursor: ${(p) => (p.$cursorPointer ? 'pointer' : 'inherit')};

    .FileFolderTileImage {
        position: relative;
        height: 110px;
        margin-bottom: 8px;
        overflow: hidden;
        display: flex;
        align-items: center;
        justify-content: center;

        img {
            max-height: 100%;
        }
    }

    .FileFolderTileSelect {
        display: flex;
        align-items: flex-start;
        justify-content: center;
        margin-bottom: 20px;

        label {
            padding-right: 20px;
            font-size: ${(p) => p.theme.font.size.small};
            line-height: ${(p) => p.theme.font.lineHeight.small};
        }
    }

    .FileFolderTileCheckbox {
        flex-shrink: 0;
        margin-top: 3px;
        margin-right: 6px;

        .CheckboxContainer {
            transition: opacity ${(p) => p.theme.transition.fast};
            opacity: ${(p) => (p.$hasChecked ? 1 : 0)};
        }

        .CheckboxInput:hover,
        .CheckboxInput:focus {
            + .CheckboxContainer {
                opacity: 1;
            }
        }
    }
`

const FileFolderTile = forwardRef(
    (
        {
            rootContainerGuid,
            entity,
            selectedLength,
            isFocused,
            isSelected,
            orderBy,
            orderDirection,
            onToggle,
            disabledGuids,
        },
        ref,
    ) => {
        const { pathname } = useLocation()
        const { clickToSelect, setContainerGuid, selectableType } =
            useContext(FilesContext)

        const navigate = useNavigate()

        const {
            guid,
            title,
            mimeType,
            subtype,
            thumbnail,
            download,
            hasChildren,
        } = entity

        const hasChecked = selectedLength > 0

        const handleDoubleClick = (evt) => {
            if (selectedLength > 1 || isDisabled) return

            evt.preventDefault()

            if (clickToSelect) {
                if (subtype === 'folder') {
                    setContainerGuid(guid)
                }
                return
            }

            if (subtype === 'folder') {
                navigate(entity.url)
            } else {
                navigate(entity.url, {
                    state: {
                        rootContainerGuid,
                        orderBy,
                        orderDirection,
                        prevPathname: pathname,
                    },
                })
            }
        }

        const imageStyle = {}
        const iconStyle = { width: '100%' }

        let tile
        if (subtype === 'folder') {
            tile = (
                <FileFolderIcon
                    name={hasChildren ? 'folder-filled' : 'folder'}
                    style={{ ...iconStyle, height: '32px' }}
                />
            )
        } else if (mimeType && mimeType.indexOf('image/') !== -1) {
            tile = <img src={thumbnail || download} alt={title} />
        } else {
            imageStyle.backgroundColor = '#F9F9F9'
            tile = (
                <FileFolderIcon
                    name={
                        subtype === 'pad'
                            ? 'pad'
                            : getIconNameByMimetype(mimeType)
                    }
                    style={{ ...iconStyle, height: '48px' }}
                />
            )
        }

        const isImage = mimeType?.startsWith('image/')
        const isFolder = subtype === 'folder'
        const isDisabled = disabledGuids?.includes(guid)
        const canSelect =
            (selectableType === 'all' ||
                (selectableType === 'image' && isImage) ||
                (selectableType === 'folder' && isFolder)) &&
            !isDisabled

        return (
            <Col
                mobile={1 / 2}
                mobileLandscape={1 / 3}
                tablet={1 / 4}
                desktopUp={1 / 5}
                style={{ position: 'relative' }}
            >
                <Wrapper
                    ref={ref}
                    className={isFocused ? 'focused' : ''}
                    $hasChecked={hasChecked}
                    $cursorPointer={subtype === 'folder'}
                    onDoubleClick={handleDoubleClick}
                >
                    <div className="FileFolderTileImage">{tile}</div>
                    <div className="FileFolderTileSelect">
                        <Checkbox
                            tabIndex="-1"
                            className="FileFolderTileCheckbox"
                            name={`file-${guid}`}
                            size="small"
                            checked={isSelected}
                            onChange={onToggle}
                            disabled={!canSelect}
                            releaseInputArea
                            title={title}
                        />
                        <label htmlFor={`file-${guid}`}>
                            <Truncate lines={2}>{title}</Truncate>
                        </label>
                    </div>
                </Wrapper>
            </Col>
        )
    },
)
FileFolderTile.displayName = 'FileFolderTile'

export default FileFolderTile
