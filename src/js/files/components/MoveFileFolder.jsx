import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import Modal from 'js/components/Modal/Modal'
import SelectFromFiles from 'js/files/SelectFromFiles/SelectFromFiles'

const MoveFileFolder = ({
    entities,
    onClose,
    containerGuid,
    rootContainerGuid,
    onSuccess,
    showMyFiles,
}) => {
    const { t } = useTranslation()

    const [isSubmitting, setIsSubmitting] = useState(false)

    const [showAccessWarningModal, setShowAccessWarningModal] = useState(false)

    const [showSameDestinationModal, setShowSameDestinationModal] =
        useState(false)

    const [destionationFolderGuid, setDestinationFolderGuid] = useState()

    const [moveFileFolder] = useMutation(MOVE_FILE_FOLDER)

    const handleSubmit = (guid) => {
        setIsSubmitting(true)

        return new Promise((resolve, reject) => {
            Promise.all(
                entities.map((entity) => {
                    return moveFileFolder({
                        variables: {
                            input: {
                                guid: entity.guid,
                                containerGuid: guid,
                            },
                        },
                        refetchQueries: ['FilesList', 'FileItem'],
                    })
                }),
            )
                .then(() => {
                    resolve()
                    onClose()
                    onSuccess?.()
                    setIsSubmitting(false)
                })
                .catch((errors) => {
                    console.error(errors)
                    reject(new Error(errors))
                    setIsSubmitting(false)
                })
        })
    }

    const hasDifferentAccessLevel = (folder) => {
        const destinationFolderReadAccessId =
            folder.accessId ?? folder.defaultReadAccessId // Group root level has `defaultReadAccessId` instead of `accessId`
        const destinationFolderWriteAccessId =
            folder.writeAccessId ?? folder.defaultWriteAccessId

        return entities.some(
            (entity) =>
                entity.accessId !== destinationFolderReadAccessId ||
                entity.writeAccessId !== destinationFolderWriteAccessId,
        )
    }

    const handleAddFiles = (folders) => {
        const destinationFolder = folders[0]
        const destinationFolderGuid = destinationFolder?.guid

        if (!destinationFolderGuid) {
            console.error('No destination folder guid found')
            return
        }

        if (destinationFolderGuid === containerGuid) {
            // Cannot move to the same folder
            setShowSameDestinationModal(true)
            return
        }

        if (hasDifferentAccessLevel(destinationFolder)) {
            setDestinationFolderGuid(destinationFolderGuid)
            setShowAccessWarningModal(true)
        } else {
            handleSubmit(destinationFolderGuid)
        }
    }

    const count = entities.length

    // Hide folders that are being moved to prevent moving a folder inside itself or descendants
    const disabledGuids = entities
        .filter((entity) => entity.subtype === 'folder')
        .map((entity) => entity.guid)

    return (
        <>
            <SelectFromFiles
                onClose={onClose}
                selectableType="folder"
                onAddFiles={handleAddFiles}
                disabledGuids={disabledGuids}
                showMyFiles={showMyFiles}
                title={t('entity-file.move-to')}
                rootContainerGuid={rootContainerGuid}
                isSubmitting={isSubmitting}
            />

            <Modal
                isVisible={showAccessWarningModal}
                size="small"
                title={t('entity-file.move-selection', { count })}
                onClose={() => {
                    setShowAccessWarningModal(false)
                }}
            >
                {t('entity-file.move-selection-confirmation', { count })}
                <Flexer mt>
                    <Button
                        size="normal"
                        variant="tertiary"
                        onClick={() => {
                            setShowAccessWarningModal(false)
                        }}
                    >
                        {t('action.cancel')}
                    </Button>
                    <Button
                        size="normal"
                        variant="primary"
                        onClick={() => {
                            setShowAccessWarningModal(false)
                            handleSubmit(destionationFolderGuid)
                        }}
                    >
                        {t('action.move')}
                    </Button>
                </Flexer>
            </Modal>

            <Modal
                isVisible={showSameDestinationModal}
                size="small"
                title={t('entity-file.choose-other-folder')}
                onClose={() => setShowSameDestinationModal(false)}
            >
                {t('entity-file.choose-other-folder-description')}
                <Flexer mt>
                    <Button
                        size="normal"
                        variant="primary"
                        onClick={() => setShowSameDestinationModal(false)}
                    >
                        {t('action.ok')}
                    </Button>
                </Flexer>
            </Modal>
        </>
    )
}

const MOVE_FILE_FOLDER = gql`
    mutation MoveFileFolderModal($input: moveFileFolderInput!) {
        moveFileFolder(input: $input) {
            entity {
                guid
                ... on FileFolder {
                    title
                    url
                }
            }
        }
    }
`

export default MoveFileFolder
