import React, { useState } from 'react'
import { Link } from 'react-router-dom'

import { CardContent } from 'js/components/Card/Card'
import FeedItem from 'js/components/FeedItem/FeedItem'
import Flexer from 'js/components/Flexer/Flexer'
import { H3 } from 'js/components/Heading'
import Img from 'js/components/Img/Img'
import ToggleTranslation from 'js/components/Item/components/ToggleTranslation'
import Truncate from 'js/components/Truncate/Truncate'

import getIconNameByMimetype from '../helpers/getIconNameByMimetype'

import { CardFileCircle } from './FileCardStyles'
import FileFolderIcon from './FileFolderIcon'

const FileFolderCard = ({ entity, 'data-feed': dataFeed }) => {
    const {
        subtype,
        url,
        mimeType,
        hasChildren,
        title,
        localTitle,
        excerpt,
        localExcerpt,
        download,
    } = entity
    const isImage = download && mimeType?.startsWith('image')

    const hasTranslations = !!localTitle || !!localExcerpt
    const [isTranslated, setIsTranslated] = useState(hasTranslations)

    const iconName =
        subtype === 'folder'
            ? hasChildren
                ? 'folder-filled'
                : 'folder'
            : subtype === 'pad'
              ? 'pad'
              : getIconNameByMimetype(mimeType)

    return (
        <FeedItem data-feed={dataFeed}>
            <CardContent>
                <Flexer alignItems="flex-start" justifyContent="flex-start">
                    <CardFileCircle style={{ flex: 'none' }}>
                        <FileFolderIcon name={iconName} />
                    </CardFileCircle>
                    <div>
                        <H3>
                            <Link to={url} style={{ display: 'block' }}>
                                <Truncate lines={2}>
                                    {isTranslated ? localTitle || title : title}
                                </Truncate>
                            </Link>
                        </H3>

                        {isImage && (
                            <Link to={url}>
                                <Img src={download} alt={title} />
                            </Link>
                        )}

                        <Truncate lines={2}>
                            {isTranslated ? localExcerpt || excerpt : excerpt}
                        </Truncate>

                        {hasTranslations && (
                            <ToggleTranslation
                                isTranslated={isTranslated}
                                setIsTranslated={setIsTranslated}
                                style={{ marginTop: '4px' }}
                            />
                        )}
                    </div>
                </Flexer>
            </CardContent>
        </FeedItem>
    )
}

export default FileFolderCard
