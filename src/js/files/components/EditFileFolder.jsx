import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { sanitizeTagCategories } from 'helpers'
import compose from 'lodash.flowright'

import SettingContainer from 'js/admin/layout/SettingContainer'
import Button from 'js/components/Button/Button'
import CustomTagsField from 'js/components/CustomTagsField'
import DateTimeField from 'js/components/DateTimeField'
import UserField from 'js/components/EntityActions/Advanced/components/UserField'
import InputLanguage from 'js/components/EntityActions/General/components/InputLanguage'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import { Col, Row } from 'js/components/Grid/Grid'
import Setting from 'js/components/Setting'
import Spacer from 'js/components/Spacer/Spacer'
import TagCategoriesField from 'js/components/TagCategories/TagCategoriesField'
import ToggleSection from 'js/components/ToggleSection/ToggleSection'
import UploadBox from 'js/components/UploadBox/UploadBox'
import { MAX_FILE_SIZE } from 'js/lib/constants'
import useCustomTags from 'js/page/Widget/hooks/useCustomTags'

const EditFileFolder = ({ data, mutate, entity, onClose, onComplete }) => {
    const { t } = useTranslation()

    const [errors, setErrors] = useState('')

    const [categoryTags, setCategoryTags] = useState(entity.tagCategories)

    const { customTags, handleAddTags, handleRemoveTags } = useCustomTags(
        entity?.tags,
    )

    const [ownerGuid, setOwnerGuid] = useState('')
    const [timePublished, setTimePublished] = useState()

    const [file, setFile] = useState()
    const onSubmitFile = (value) => {
        setFile(value)
    }

    const site = data?.site || {}
    const viewer = data?.viewer || {}

    const {
        guid = '',
        title = '',
        richDescription = '',
        subtype = '',
        inputLanguage,
        isTranslationEnabled,
    } = entity || {}

    const { contentTranslation, languageOptions } = site
    const showInputLanuage = contentTranslation && languageOptions?.length > 1

    const {
        control,
        handleSubmit,
        watch,
        formState: { isValid, isSubmitting },
    } = useForm({
        mode: 'onChange',
        defaultValues: {
            title,
            richDescription,
            inputLanguage,
            isTranslationEnabled,
        },
    })

    if (!entity || !data.site) return null

    const submit = async ({
        title,
        richDescription,
        inputLanguage,
        isTranslationEnabled,
    }) => {
        await mutate({
            variables: {
                input: {
                    guid,
                    title,
                    file,
                    richDescription,
                    inputLanguage,
                    isTranslationEnabled,
                    tagCategories: sanitizeTagCategories(
                        site.tagCategories,
                        categoryTags,
                    ),
                    tags: customTags.toJS(),
                    ...(viewer.isAdmin && ownerGuid ? { ownerGuid } : {}),
                    ...(viewer.isAdmin && timePublished
                        ? { timePublished }
                        : {}),
                },
            },
            refetchQueries: ['FilesList', 'FileItem'],
        })
            .then(() => {
                onClose()
                onComplete()
            })
            .catch((errors) => {
                console.error(errors)
                setErrors(errors)
            })
    }

    return (
        <Spacer as="form" spacing="small" onSubmit={handleSubmit(submit)}>
            <FormItem
                type="text"
                control={control}
                name="title"
                label={t('global.name')}
                required
            />

            {subtype === 'file' && (
                <Setting
                    subtitle={t('entity-file.replace-file')}
                    inputWidth="full"
                    htmlFor="file"
                >
                    <UploadBox
                        name="file"
                        maxSize={MAX_FILE_SIZE.file}
                        onSubmit={onSubmitFile}
                    >
                        {file && <div>{file.name}</div>}
                    </UploadBox>

                    {errors && (
                        <Flexer mt>
                            <Errors errors={errors} />
                        </Flexer>
                    )}
                </Setting>
            )}

            {site.fileDescriptionFieldEnabled && (
                <>
                    <FormItem
                        control={control}
                        type="rich"
                        name="richDescription"
                        title={t('form.description')}
                        options={{
                            textLink: true,
                        }}
                    />

                    {showInputLanuage && (
                        <InputLanguage
                            control={control}
                            languageOptions={languageOptions}
                            showInputLanguage={watch('isTranslationEnabled')}
                        />
                    )}
                </>
            )}

            <TagCategoriesField
                name="filters"
                tagCategories={site.tagCategories}
                value={categoryTags}
                setTags={setCategoryTags}
            />
            <CustomTagsField
                name="customTags"
                value={customTags}
                onAddTag={handleAddTags}
                onRemoveTag={handleRemoveTags}
            />

            {viewer.isAdmin && (
                <ToggleSection
                    title={t('global.advanced')}
                    aria-label={t('form.show-advanced-settings')}
                >
                    <Row style={{ paddingTop: '16px' }}>
                        <Col tabletUp={1 / 2}>
                            <SettingContainer
                                subtitle="global.owner"
                                htmlFor="ownerGuid"
                                inputWidth="full"
                                $mb
                            >
                                <UserField
                                    name="ownerGuid"
                                    value={ownerGuid}
                                    defaultValue={{
                                        value: entity.owner.guid,
                                        label: entity.owner.name,
                                    }}
                                    onChange={(user) =>
                                        setOwnerGuid(user?.guid || '')
                                    }
                                />
                            </SettingContainer>
                        </Col>
                        <Col tabletUp={1 / 2}>
                            <Setting
                                subtitle="global.published"
                                inputWidth="full"
                                $mb
                            >
                                <DateTimeField
                                    name="timePublished"
                                    value={
                                        timePublished || entity.timePublished
                                    }
                                    onChange={setTimePublished}
                                    isClearable
                                />
                            </Setting>
                        </Col>
                    </Row>
                </ToggleSection>
            )}

            <Flexer mt>
                <Button size="normal" variant="tertiary" onClick={onClose}>
                    {t('action.cancel')}
                </Button>
                <Button
                    size="normal"
                    variant="primary"
                    type="submit"
                    disabled={!isValid}
                    loading={isSubmitting}
                >
                    {t('action.save')}
                </Button>
            </Flexer>
        </Spacer>
    )
}

const Query = gql`
    query EditFileFolder {
        site {
            guid
            fileDescriptionFieldEnabled
            contentTranslation
            languageOptions {
                value
                label
            }
            tagCategories {
                name
                values
            }
        }
        viewer {
            guid
            isAdmin
        }
    }
`

const Mutation = gql`
    mutation editFileFolder($input: editFileFolderInput!) {
        editFileFolder(input: $input) {
            entity {
                guid
                ... on Folder {
                    title
                    url
                    richDescription
                    inputLanguage
                }
                ... on File {
                    title
                    url
                    download
                    richDescription
                    tags
                    tagCategories {
                        name
                        values
                    }
                }
                ... on Pad {
                    title
                    url
                    richDescription
                }
            }
        }
    }
`

export default compose(graphql(Query), graphql(Mutation))(EditFileFolder)
