const typesArray = [
    {
        mimeType: 'text/txt',
        iconName: 'txt',
    },
    {
        mimeType: 'text/rtf',
        iconName: 'rtf',
    },
    {
        mimeType: 'application/msword',
        iconName: 'doc',
    },
    {
        mimeType:
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        iconName: 'doc',
    },
    {
        mimeType: 'application/pdf',
        iconName: 'pdf',
    },
    {
        mimeType: 'application/vnd.ms-powerpoint',
        iconName: 'ppt',
    },
    {
        mimeType:
            'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        iconName: 'ppt',
    },
    {
        mimeType: 'application/vnd.ms-excel',
        iconName: 'xls',
    },
    {
        mimeType:
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        iconName: 'xls',
    },
    {
        mimeType: 'image/jpeg',
        iconName: 'jpg',
    },
    {
        mimeType: 'image/png',
        iconName: 'png',
    },
    {
        mimeType: 'image/gif',
        iconName: 'gif',
    },
    {
        mimeType: 'application/zip',
        iconName: 'zip',
    },
    {
        mimeType: 'application/x-rar',
        iconName: 'rar',
    },
    {
        mimeType: 'audio/mpeg',
        iconName: 'mp3',
    },
    {
        mimeType: 'video/mp4',
        iconName: 'mp4',
    },
    {
        mimeType: 'audio/m4a',
        iconName: 'm4a',
    },
]

export default (mimeType, defaultIcon = 'other') => {
    for (let i = 0; i < typesArray.length; i++) {
        if (typesArray[i].mimeType === mimeType) {
            return typesArray[i].iconName
        }
    }

    return defaultIcon
}
