import getIconNameByMimetype from 'js/files/helpers/getIconNameByMimetype'

export default function getIconName(subtype, mimeType, hasChildren) {
    return subtype === 'folder'
        ? hasChildren
            ? 'folder-filled'
            : 'folder'
        : subtype === 'pad'
          ? 'pad'
          : getIconNameByMimetype(mimeType)
}
