import iconDoc from 'icons/file-doc.svg'
import iconFolder from 'icons/file-folder.svg'
import iconGif from 'icons/file-gif.svg'
import iconJpg from 'icons/file-jpg.svg'
import iconOther from 'icons/file-other.svg'
import iconPdf from 'icons/file-pdf.svg'
import iconPng from 'icons/file-png.svg'
import iconPpt from 'icons/file-ppt.svg'
import iconRar from 'icons/file-rar.svg'
import iconRtf from 'icons/file-rtf.svg'
import iconTxt from 'icons/file-txt.svg'
import iconXls from 'icons/file-xls.svg'
import iconZip from 'icons/file-zip.svg'

export default function getIcon(iconName) {
    switch (iconName) {
        case 'doc':
            return iconDoc

        case 'folder':
            return iconFolder

        case 'gif':
            return iconGif

        case 'jpg':
            return iconJpg

        case 'other':
            return iconOther

        case 'pdf':
            return iconPdf

        case 'png':
            return iconPng

        case 'ppt':
            return iconPpt

        case 'rar':
            return iconRar

        case 'rtf':
            return iconRtf

        case 'txt':
            return iconTxt

        case 'xls':
            return iconXls

        case 'zip':
            return iconZip

        default:
            return ''
    }
}
