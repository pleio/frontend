import React from 'react'
import styled from 'styled-components'

import { H5 } from 'js/components/Heading'
import FileFolderIcon from 'js/files/components/FileFolderIcon'

import IconSelected from 'icons/selected.svg'
import IconUnselected from 'icons/unselected.svg'

const Wrapper = styled.div`
    display: flex;

    .DetailsHeaderIcon {
        margin-left: -6px;
        flex-shrink: 0;
        width: 32px;
        height: 32px;
        display: flex;
        align-items: center;
        justify-content: center;
        margin-right: 4px;
    }

    .DetailsHeaderTitle {
        padding: 7px 0 5px 0;
        // Extra padding to make space for the "Close modal" button
        padding-right: ${(p) => (p.$headerPaddingRight ? '40px' : 0)};
    }
`

const DetailsHeader = ({ state, iconName, title, headerPaddingRight }) => {
    return (
        <Wrapper $headerPaddingRight={headerPaddingRight}>
            <div className="DetailsHeaderIcon">
                {state === 'selected' ? (
                    <IconSelected />
                ) : state === 'unselected' ? (
                    <IconUnselected />
                ) : (
                    <FileFolderIcon name={iconName} />
                )}
            </div>
            <H5
                className="DetailsHeaderTitle"
                as="div" // a11y
            >
                {title}
            </H5>
        </Wrapper>
    )
}

export default DetailsHeader
