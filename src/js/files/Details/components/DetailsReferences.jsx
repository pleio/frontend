import React from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import HideVisually from 'js/components/HideVisually/HideVisually'

import ExternalIcon from 'icons/new-window-small.svg'

const Wrapper = styled.ul`
    li:not(:last-child) {
        border-bottom: 1px solid ${(p) => p.theme.color.grey[20]};
    }

    .DetailsReference {
        min-height: 40px;
        padding: 10px 16px;
        display: flex;
        align-items: baseline;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};

        &:hover .DetailsReferenceTitle {
            text-decoration: underline;
        }
    }

    .DetailsReferenceTitle {
        color: ${(p) => p.theme.color.primary.main};
    }

    .DetailsPropertyType {
        margin-left: 4px;
        color: ${(p) => p.theme.color.text.grey};
        font-size: ${(p) => p.theme.font.size.tiny};
        line-height: ${(p) => p.theme.font.lineHeight.tiny};
    }
`

const DetailsReferences = ({ references, ...rest }) => {
    const { t } = useTranslation()

    return (
        <Wrapper {...rest}>
            <ul className="DetailsPropertiesList">
                {references?.map(({ guid, label, url, entityType }) => (
                    <li key={guid}>
                        <a
                            href={url}
                            target="_blank"
                            rel="noopener noreferrer"
                            className="DetailsReference"
                        >
                            <div>
                                <span className="DetailsReferenceTitle">
                                    {label || t('global.untitled')}
                                </span>
                                <span className="DetailsPropertyType">
                                    {entityType}
                                </span>
                            </div>
                            <ExternalIcon style={{ marginLeft: 'auto' }} />
                            <HideVisually>
                                {t('global.opens-in-new-window')}
                            </HideVisually>
                        </a>
                    </li>
                ))}
            </ul>
        </Wrapper>
    )
}

export default DetailsReferences
