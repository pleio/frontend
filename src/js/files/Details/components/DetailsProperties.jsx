import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { humanFileSize } from 'helpers'
import styled from 'styled-components'

import DisplayDate from 'js/components/DisplayDate'
import ToggleTranslation from 'js/components/Item/components/ToggleTranslation'
import ItemTags from 'js/components/ItemTags'
import TiptapView from 'js/components/Tiptap/TiptapView'

const Wrapper = styled.ul`
    display: flex;
    flex-wrap: wrap;
    margin-top: -4px;

    .DetailsProperty {
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
    }

    .DetailsPropertyName {
        color: ${(p) => p.theme.color.text.grey};
    }

    .DetailsPropertyValue {
        margin-top: 2px;
        color: ${(p) => p.theme.color.text.black};
    }
`

const DetailsProperties = ({ entity, accessIds, site }) => {
    const { t } = useTranslation()

    const [isTranslated, setIsTranslated] = useState(false)

    if (!site) return null

    const { showOriginalFileName, showTagsInDetail, showCustomTagsInDetail } =
        site

    const {
        __typename,
        guid,
        canEdit,
        mimeType,
        size,
        timeUpdated,
        timePublished,
        accessId,
        writeAccessId,
        owner,
        richDescription,
        localRichDescription,
        tagCategories,
        tags,
        views,
        originalFileName,
    } = entity

    const properties = [
        ...(__typename !== 'Pad' && richDescription
            ? [
                  {
                      name: t('form.description'),
                      value: (
                          <>
                              <TiptapView
                                  content={
                                      isTranslated
                                          ? localRichDescription
                                          : richDescription
                                  }
                                  textSize="small"
                              />
                              {localRichDescription && (
                                  <ToggleTranslation
                                      isTranslated={isTranslated}
                                      setIsTranslated={setIsTranslated}
                                      style={{ marginTop: '4px' }}
                                  />
                              )}
                          </>
                      ),
                      fullWidth: true,
                  },
              ]
            : []),
        ...(__typename === 'File' && showOriginalFileName && originalFileName
            ? [
                  {
                      name: t('entity-file.original-filename'),
                      value: originalFileName,
                      fullWidth: true,
                  },
              ]
            : []),
        ...(__typename === 'File'
            ? [
                  {
                      name: t('global.type'),
                      value: mimeType,
                  },
                  {
                      name: 'File size',
                      value: humanFileSize(size),
                  },
              ]
            : []),
        {
            name: t('entity-file.modified'),
            value: <DisplayDate date={timeUpdated} />,
        },
        {
            name: t('global.created'),
            value: <DisplayDate date={timePublished} placement="right" />,
        },
        {
            name: t('global.owner'),
            value: owner.name,
            fullWidth: !window.__SETTINGS__.showViewsCount,
        },
        ...(window.__SETTINGS__.showViewsCount
            ? [
                  {
                      name: t('entity-file.views'),
                      value: views,
                  },
              ]
            : []),
        {
            name: t('permissions.read-access'),
            value: accessIds?.find((el) => el.id === accessId)?.description,
            fullWidth: !canEdit,
        },

        ...(canEdit
            ? [
                  {
                      name: t('permissions.write-access'),
                      value: accessIds?.find((el) => el.id === writeAccessId)
                          ?.description,
                  },
              ]
            : []),
        ...((showTagsInDetail && tagCategories.length) ||
        (showCustomTagsInDetail && tags.length)
            ? [
                  {
                      name: t('form.tags'),
                      value: (
                          <ItemTags
                              showTags={showTagsInDetail}
                              showCustomTags={showCustomTagsInDetail}
                              customTags={tags}
                              tagCategories={tagCategories}
                          />
                      ),
                      fullWidth: true,
                  },
              ]
            : []),
    ]

    return (
        <Wrapper>
            {properties.map(({ name, value, fullWidth }) => (
                <li
                    key={`${guid}-${name}`}
                    className="DetailsProperty"
                    style={{
                        width: fullWidth ? '100%' : '50%',
                        marginTop: '12px',
                    }}
                >
                    <div className="DetailsPropertyName">{name}</div>
                    <div className="DetailsPropertyValue">{value}</div>
                </li>
            ))}
        </Wrapper>
    )
}

export default DetailsProperties
