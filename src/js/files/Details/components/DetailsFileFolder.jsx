import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'

import TabMenu from 'js/components/TabMenu'
import TabPage from 'js/components/TabPage'
import getIconNameByMimetype from 'js/files/helpers/getIconNameByMimetype'

import DetailsHeader from './DetailsHeader'
import DetailsProperties from './DetailsProperties'
import DetailsReferences from './DetailsReferences'

const DetailsFileFolder = ({ entity, accessIds, site, headerPaddingRight }) => {
    const { t } = useTranslation()
    const [tab, setTab] = useState('details')

    const { mimeType, subtype, title, referenceCount, hasChildren } = entity

    const iconName =
        subtype === 'folder'
            ? hasChildren
                ? 'folder-filled'
                : 'folder'
            : subtype === 'pad'
              ? 'pad'
              : getIconNameByMimetype(mimeType)

    return (
        <>
            <DetailsHeader
                iconName={iconName}
                title={title}
                headerPaddingRight={headerPaddingRight}
            />
            {referenceCount > 0 && (
                <TabMenu
                    label={t('entity-file.details')}
                    options={[
                        {
                            onClick: () => setTab('details'),
                            label: t('entity-file.details'),
                            isActive: tab === 'details',
                            id: 'tab-details',
                            ariaControls: 'tabpanel-details',
                        },
                        {
                            onClick: () => setTab('references'),
                            label: `${t('entity-file.references')} (${referenceCount})`,
                            isActive: tab === 'references',
                            id: 'tab-references',
                            ariaControls: 'tabpanel-references',
                        },
                    ]}
                    showBorder
                    style={{ padding: '0 16px', margin: '0 -16px 4px' }}
                />
            )}
            <TabPage
                id={`tabpanel-${tab}`}
                aria-labelledby={`tab-${tab}`}
                visible
            >
                {tab === 'details' && (
                    <DetailsProperties
                        entity={entity}
                        accessIds={accessIds}
                        site={site}
                    />
                )}
                {tab === 'references' && (
                    <DetailsReferences
                        references={entity.references}
                        style={{
                            margin: '0 -16px',
                            paddingBottom: '4px',
                        }}
                    />
                )}
            </TabPage>
        </>
    )
}

export default DetailsFileFolder
