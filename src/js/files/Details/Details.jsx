import React from 'react'
import { useTranslation } from 'react-i18next'
import { useTabletUp } from 'helpers/breakpoints'
import styled from 'styled-components'

import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Modal from 'js/components/Modal/Modal'

import DetailsFileFolder from './components/DetailsFileFolder'
import DetailsHeader from './components/DetailsHeader'

const Wrapper = styled.div`
    width: 300px;
    flex-shrink: 0;
    padding: 8px 16px 20px;
`

const Details = ({
    showDetails,
    setShowDetails,
    selected,
    folderEntity,
    rootEntity,
    accessIds,
    site,
    ...rest
}) => {
    const newAccessIds = accessIds || window.__SETTINGS__.site.readAccessIds

    const { t } = useTranslation()

    const isTableUp = useTabletUp()

    const content =
        selected?.length > 1 ? (
            <DetailsHeader
                state="selected"
                title={t('global.selected', {
                    count: selected.length,
                })}
                headerPaddingRight={!isTableUp}
            />
        ) : selected[0] ? (
            <DetailsFileFolder
                entity={selected[0]}
                accessIds={newAccessIds}
                site={site}
                headerPaddingRight={!isTableUp}
            />
        ) : folderEntity ? (
            <DetailsFileFolder
                entity={folderEntity}
                accessIds={newAccessIds}
                headerPaddingRight={!isTableUp}
            />
        ) : rootEntity ? (
            <DetailsHeader
                state="unselected"
                title={t('entity-file.select-for-details')}
                headerPaddingRight={!isTableUp}
            />
        ) : (
            <LoadingSpinner style={{ height: '48px' }} />
        )

    if (isTableUp) {
        if (!showDetails) return null
        return (
            <Wrapper aria-live="polite" {...rest}>
                {content}
            </Wrapper>
        )
    } else {
        return (
            <Modal
                isVisible={showDetails}
                size="small"
                onClose={() => setShowDetails(false)}
                showCloseButton
            >
                {content}
            </Modal>
        )
    }
}

export default Details
