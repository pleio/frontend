import React, { forwardRef } from 'react'
import { useTranslation } from 'react-i18next'
import { useTabletUp } from 'helpers/breakpoints'
import styled from 'styled-components'

import FetchMore from 'js/components/FetchMore'
import Flexer from 'js/components/Flexer/Flexer'
import { Row } from 'js/components/Grid/Grid'
import IconButton from 'js/components/IconButton/IconButton'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Table from 'js/components/Table'
import TableSortableHeader from 'js/components/TableSortableHeader/TableSortableHeader'

import CreateFileIcon from 'icons/create-file.svg'
import CreateFolderIcon from 'icons/create-folder.svg'
import UploadIcon from 'icons/upload.svg'

import FileFolder from './components/FileFolder'
import FileFolderTile from './components/FileFolderTile'

const NoResultsWrapper = styled.div`
    flex-grow: 1;
    margin: 20px 0;
    display: flex;
    align-items: center;
    justify-content: center;

    .NoResultsButton {
        width: 120px;
        height: 120px;
    }

    .NoResultsIcon {
        height: 20px;
        display: flex;
        align-items: center;
        margin-bottom: 8px;
    }
`

const Wrapper = styled.div`
    table {
        position: relative; // Puts table on top of background
    }

    th {
        height: 40px;

        &:first-child {
            padding-left: 12px;
        }
    }

    td:first-child {
        padding-left: 0;
    }
`

const FilesViewList = forwardRef(
    (
        {
            site,
            viewer,
            rootContainerGuid,
            viewType,
            loading,
            files,
            fetchMore,
            setQueryLimit,
            orderBy,
            orderDirection,
            setOrderBy,
            setOrderDirection,
            focused,
            selected,
            refFocused,
            toggleSelected,
            canEditRoot,
            accessIds,
            toggleUploadFileModal,
            toggleCreateFolderModal,
            onCreatePad,
            showExtraColumns,
            canCreateAndEdit,
            disabledGuids,
            ...rest
        },
        ref,
    ) => {
        const { t } = useTranslation()
        const isTableUp = useTabletUp()

        if (!loading && !files?.edges) return null

        if (!loading && files?.edges?.length === 0) {
            if (viewer.canWriteToContainer && canCreateAndEdit) {
                return (
                    <NoResultsWrapper ref={ref} {...rest}>
                        <Flexer wrap as="ul">
                            <li>
                                <IconButton
                                    variant="primary"
                                    className="NoResultsButton"
                                    onClick={toggleCreateFolderModal}
                                >
                                    <div className="NoResultsIcon">
                                        <CreateFolderIcon />
                                    </div>
                                    {t('entity-file.create-folder')}
                                </IconButton>
                            </li>
                            {site.collabEditingEnabled && (
                                <li>
                                    <IconButton
                                        variant="primary"
                                        className="NoResultsButton"
                                        onClick={onCreatePad}
                                    >
                                        <div className="NoResultsIcon">
                                            <CreateFileIcon />
                                        </div>
                                        {t('entity-file.create-pad')}
                                    </IconButton>
                                </li>
                            )}
                            <li>
                                <IconButton
                                    variant="primary"
                                    className="NoResultsButton"
                                    onClick={toggleUploadFileModal}
                                >
                                    <div className="NoResultsIcon">
                                        <UploadIcon />
                                    </div>
                                    {t('entity-file.upload-files')}
                                </IconButton>
                            </li>
                        </Flexer>
                    </NoResultsWrapper>
                )
            } else {
                return null
            }
        }

        const orderByLabels = {
            filename: t('global.name'),
            timeUpdated: t('entity-file.modified'),
            owner: t('global.owner'),
        }

        return (
            <Wrapper
                ref={ref}
                aria-hidden // Screen readers use listbox in FilesView
                {...rest}
            >
                {viewType === 'list' ? (
                    <Table
                        edgePadding
                        rowHeight="small"
                        wrap="none"
                        layout={isTableUp ? 'fixed' : null}
                    >
                        <thead>
                            <tr>
                                <TableSortableHeader
                                    name="filename"
                                    label={t('global.name')}
                                    orderBy={orderBy}
                                    orderDirection={orderDirection}
                                    setOrderBy={setOrderBy}
                                    setOrderDirection={setOrderDirection}
                                    style={{ width: '100%' }}
                                />
                                {showExtraColumns && (
                                    <>
                                        <TableSortableHeader
                                            name="owner"
                                            label={t('global.owner')}
                                            orderBy={orderBy}
                                            orderDirection={orderDirection}
                                            setOrderBy={setOrderBy}
                                            setOrderDirection={
                                                setOrderDirection
                                            }
                                            align="right"
                                            style={{ width: '200px' }}
                                        />

                                        <TableSortableHeader
                                            options={[
                                                {
                                                    name: 'timeUpdated',
                                                    label: t(
                                                        'entity-file.modified',
                                                    ),
                                                    defaultOrderDirection:
                                                        'desc',
                                                },
                                                {
                                                    name: 'timePublished',
                                                    label: t('global.created'),
                                                    defaultOrderDirection:
                                                        'desc',
                                                },
                                            ]}
                                            orderBy={orderBy}
                                            orderDirection={orderDirection}
                                            setOrderBy={setOrderBy}
                                            setOrderDirection={
                                                setOrderDirection
                                            }
                                            align="right"
                                            style={{ width: '120px' }}
                                        />

                                        {canEditRoot && (
                                            <>
                                                <TableSortableHeader
                                                    name="readAccessWeight"
                                                    label={t(
                                                        'permissions.read',
                                                    )}
                                                    orderBy={orderBy}
                                                    orderDirection={
                                                        orderDirection
                                                    }
                                                    setOrderBy={setOrderBy}
                                                    setOrderDirection={
                                                        setOrderDirection
                                                    }
                                                    align="center"
                                                    style={{
                                                        width: '44px',
                                                        padding: '0 4px 0 0',
                                                    }}
                                                />
                                                <TableSortableHeader
                                                    name="writeAccessWeight"
                                                    label={t(
                                                        'permissions.write',
                                                    )}
                                                    orderBy={orderBy}
                                                    orderDirection={
                                                        orderDirection
                                                    }
                                                    setOrderBy={setOrderBy}
                                                    setOrderDirection={
                                                        setOrderDirection
                                                    }
                                                    align="center"
                                                    style={{
                                                        width: '48px',
                                                        padding: '0 8px 0 0',
                                                    }}
                                                />
                                            </>
                                        )}
                                    </>
                                )}
                            </tr>
                        </thead>

                        {loading ? (
                            <tbody>
                                <tr>
                                    <td
                                        colSpan={100}
                                        style={{ padding: '24px' }}
                                    >
                                        <LoadingSpinner />
                                    </td>
                                </tr>
                            </tbody>
                        ) : (
                            <FetchMore
                                as="tbody"
                                edges={files.edges}
                                getMoreResults={(data) => data.files.edges}
                                fetchMore={fetchMore}
                                fetchType="scroll"
                                scrollElement={canCreateAndEdit ? null : ref}
                                fetchCount={50}
                                setLimit={setQueryLimit}
                                maxLimit={files.total}
                                resultsMessage={`${t('entity-file.result', {
                                    count: files.total,
                                })} (${orderByLabels[orderBy]} - ${
                                    orderDirection === 'desc'
                                        ? t('global.desc')
                                        : t('global.asc')
                                })`}
                            >
                                {files.edges.map((entity) => {
                                    const isFocused =
                                        entity.guid === focused.guid
                                    const isSelected = !!selected.find(
                                        (el) => el.guid === entity.guid,
                                    )

                                    const handleToggle = () => {
                                        toggleSelected(entity, isSelected)
                                    }

                                    return (
                                        <FileFolder
                                            key={entity.guid}
                                            ref={isFocused ? refFocused : null}
                                            entity={entity}
                                            rootContainerGuid={
                                                rootContainerGuid
                                            }
                                            orderBy={orderBy}
                                            orderDirection={orderDirection}
                                            isSelected={isSelected}
                                            isFocused={isFocused}
                                            onToggle={handleToggle}
                                            canEditRoot={canEditRoot}
                                            accessIds={accessIds}
                                            showExtraColumns={showExtraColumns}
                                            disabledGuids={disabledGuids}
                                        />
                                    )
                                })}
                            </FetchMore>
                        )}
                    </Table>
                ) : (
                    <div className="FilesTiles">
                        {loading ? (
                            <LoadingSpinner />
                        ) : (
                            <FetchMore
                                as={Row}
                                edges={files.edges}
                                getMoreResults={(data) => data.files.edges}
                                fetchMore={fetchMore}
                                fetchType="scroll"
                                fetchCount={25}
                                setLimit={setQueryLimit}
                                maxLimit={files.total}
                            >
                                {files.edges.map((entity) => {
                                    const isFocused =
                                        entity.guid === focused.guid
                                    const isSelected = !!selected.find(
                                        (el) => el.guid === entity.guid,
                                    )

                                    const handleToggle = () => {
                                        toggleSelected(entity, isSelected)
                                    }

                                    return (
                                        <FileFolderTile
                                            key={entity.guid}
                                            ref={isFocused ? refFocused : null}
                                            entity={entity}
                                            rootContainerGuid={
                                                rootContainerGuid
                                            }
                                            orderBy={orderBy}
                                            orderDirection={orderDirection}
                                            selectedLength={selected.length}
                                            isSelected={isSelected}
                                            isFocused={isFocused}
                                            onToggle={handleToggle}
                                            disabledGuids={disabledGuids}
                                        />
                                    )
                                })}
                            </FetchMore>
                        )}
                    </div>
                )}
            </Wrapper>
        )
    },
)

FilesViewList.displayName = 'FilesViewList'

export default FilesViewList
