import React, {
    createContext,
    useEffect,
    useLayoutEffect,
    useRef,
    useState,
} from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useLocation, useNavigate, useParams } from 'react-router-dom'
import { useMeasure } from 'react-use'
import { gql, useMutation, useQuery } from '@apollo/client'
import {
    getOffsetTop,
    getStickyOffset,
    media,
    useLocalStorage,
    useWindowScrollPosition,
} from 'helpers'
import { showDateTime } from 'helpers/date/showDate'
import { produce } from 'immer'
import PropTypes from 'prop-types'
import styled, { useTheme } from 'styled-components'

import Flexer from 'js/components/Flexer/Flexer'
import HideVisually from 'js/components/HideVisually/HideVisually'
import DeleteEntitiesModal from 'js/components/Item/ItemActions/components/DeleteEntitiesModal'
import Modal from 'js/components/Modal/Modal'
import Sticky from 'js/components/Sticky/Sticky'
import SelectedActions from 'js/components/Toolbar/SelectedActions'
import ActionButton from 'js/files/components/ActionButton'

import CreateFileIcon from 'icons/create-file.svg'
import CreateFolderIcon from 'icons/create-folder.svg'
import DeleteIcon from 'icons/delete.svg'
import DownloadIcon from 'icons/download.svg'
import EditIcon from 'icons/edit.svg'
import InfoIcon from 'icons/info.svg'
import ListIcon from 'icons/list-view.svg'
import MoveIcon from 'icons/move-to-folder.svg'
import StarIcon from 'icons/star.svg'
import StarFillIcon from 'icons/star-fill.svg'
import TileIcon from 'icons/tile-view.svg'
import UploadIcon from 'icons/upload.svg'
import UsersIcon from 'icons/users.svg'

import AccessFileFolder from './components/AccessFileFolder'
import Breadcrumb from './components/Breadcrumb'
import CreateFolder from './components/CreateFolder'
import EditFileFolder from './components/EditFileFolder'
import MoveFileFolder from './components/MoveFileFolder'
import StarButton from './components/StarButton'
import UploadFile from './components/UploadFile'
import Details from './Details/Details'
import FilesViewList from './FilesViewList'
import GET_FILES_LIST from './getFilesList'

const Header = styled(Sticky)`
    display: flex;
    align-items: flex-start;
    ${media.mobileLandscapeDown`
        flex-wrap: wrap;
    `}

    min-height: ${(p) => p.theme.headerBarHeight}px;
    padding: 0 8px;

    &[data-sticky='true'] {
        background-color: white;
        box-shadow: ${(p) => p.theme.shadow.soft.bottom};
        z-index: 9;
    }
`

const Wrapper = styled(Flexer)`
    width: 100%; // Prevents table to grow out of view
    overflow: hidden;

    .FilesListContainer {
        min-width: 0; // Prevents table to grow out of view
        width: 100%; // Prevents table to grow out of view
        display: flex;
        flex-direction: column; // To fix order
    }

    .FilesList {
        padding-bottom: 4px;
        order: 1;
    }

    .FilesTiles {
        padding: 20px 20px 0;
    }
`

export const FilesContext = createContext(null)

const FilesView = ({
    rootContainerGuid,
    groupGuid,
    canCreateAndEdit,
    selected,
    setSelected,
    selectable,
    selectableType = 'all',
    clickToSelect,
    disabledGuids, // Entities that cannot be selected
    ...rest
}) => {
    const location = useLocation()
    const navigate = useNavigate()
    const params = useParams()

    const refFiles = useRef()
    const refFocused = useRef()

    const theme = useTheme()
    const stickyHeight = theme.headerBarHeight

    const refetchQueries = ['FilesList']

    const [filterBookmarks, setFilterBookmarks] = useState(false)
    const toggleFilterBookmarks = () => setFilterBookmarks(!filterBookmarks)

    const [stickyOffset, setStickyOffset] = useState(0)
    const [focused, setFocused] = useState({
        index: null,
        guid: null,
    })
    const [viewType, setViewType] = useLocalStorage(
        `${rootContainerGuid}-files-viewType`,
        selectableType === 'image' ? 'tile' : 'list',
        selectableType === 'image',
    )
    const [orderBy, setOrderBy] = useLocalStorage(
        `${rootContainerGuid}-files-orderBy`,
        'filename',
    )
    const [orderDirection, setOrderDirection] = useLocalStorage(
        `${rootContainerGuid}-files-orderDirection`,
        'asc',
    )

    const [showDetails, setShowDetails] = useLocalStorage(
        `files-show-details`,
        false,
    )

    const [showUploadFileModal, setShowUploadFileModal] = useState(false)
    const [showEditFileFolderModal, setShowEditFileFolderModal] =
        useState(false)
    const [showAccessFileFolderModal, setShowAccessFileFolderModal] =
        useState(false)
    const [showCreateFolderModal, setShowCreateFolderModal] = useState(false)
    const [showDeleteFileFolderModal, setShowDeleteFileFolderModal] =
        useState(false)
    const [showMoveFileFolderModal, setShowMoveFileFolderModal] =
        useState(false)

    const toggleSelected = (entity, isSelected) => {
        if (selectable === 'single') {
            setSelected(isSelected ? [] : [entity])
        } else {
            if (!isSelected) {
                setSelected([...selected, entity])
            } else {
                setSelected(
                    produce((newState) => {
                        newState.splice(
                            newState.findIndex((el) => el.guid === entity.guid),
                            1,
                        )
                    }),
                )
            }
        }
    }

    const toggleViewType = () => {
        setViewType(viewType === 'list' ? 'tile' : 'list')
    }

    const clearSelection = () => {
        setSelected([])
    }

    const handleClear = () => {
        clearSelection()
        refFiles.current.focus()
    }

    const toggleUploadFileModal = () =>
        setShowUploadFileModal(!showUploadFileModal)

    const toggleEditFileFolderModal = () =>
        setShowEditFileFolderModal(!showEditFileFolderModal)

    const toggleAccessFileFolderModal = () =>
        setShowAccessFileFolderModal(!showAccessFileFolderModal)

    const toggleCreateFolderModal = () =>
        setShowCreateFolderModal(!showCreateFolderModal)

    const toggleDeleteFileFolderModal = () =>
        setShowDeleteFileFolderModal(!showDeleteFileFolderModal)

    const toggleMoveFileFolderModal = () =>
        setShowMoveFileFolderModal(!showMoveFileFolderModal)

    const downloadFiles = () => {
        if (selected.length === 1) {
            switch (singleSelected.subtype) {
                case 'folder':
                    window.location = `/bulk_download?folder_guids[]=${singleSelected.guid}`
                    break

                case 'file':
                    window.open(singleSelected.download, '_blank')
                    break
            }
        } else if (selected.length > 1) {
            const params = []
            selected.forEach((item) => {
                switch (item.subtype) {
                    case 'folder':
                        params.push(`folder_guids[]=${item.guid}`)
                        break

                    case 'file':
                        params.push(`file_guids[]=${item.guid}`)
                        break
                }
            })

            window.location = `/bulk_download?${params.join('&')}`
        }
    }

    const setFocusedState = (index) => {
        if (index && !!data) {
            setFocused({
                index,
                guid: data.files.edges[index - 1].guid,
            })
        } else {
            setFocused({
                index: null,
                guid: null,
            })
        }
    }

    const handleFocus = (evt) => {
        // Only when focusing by Tab
        if (evt.target !== refFiles.current) return
        if (selected.length > 0 && !!data) {
            const index = data.files.edges.findIndex(
                (el) => el.guid === singleSelected.guid,
            )
            setFocusedState(index + 1)
        } else {
            setFocusedState(1)
        }
    }

    const handleBlur = (evt) => {
        // Check if blur happened outside component
        if (!refFiles.current.contains(evt.relatedTarget) && !!focused) {
            setFocusedState()
        }
    }

    const handleKeyDown = (evt) => {
        const total = !!data && data.files.edges.length

        switch (evt.key) {
            case 'ArrowDown':
            case 'ArrowRight':
                evt.preventDefault()
                if (focused.index >= total) {
                    setFocusedState(1)
                } else {
                    setFocusedState(focused.index + 1)
                }
                break

            case 'ArrowUp':
            case 'ArrowLeft':
                evt.preventDefault()
                if (focused.index === 1) {
                    setFocusedState(total)
                } else {
                    setFocusedState(focused.index - 1)
                }
                break

            case ' ':
                evt.preventDefault()
                toggleFocused()
                break

            case 'Enter':
                evt.preventDefault()
                viewFocused()
                break
        }
    }

    useEffect(() => {
        setStickyOffset(getStickyOffset())
    }, [])

    useEffect(() => {
        if (!!focused && !!refFocused.current) {
            const focusedTop = getOffsetTop(refFocused.current)
            const focusedHeight = refFocused.current.clientHeight
            const windowTop = window.scrollY || window.pageYOffset
            const windowHeight =
                window.innerHeight || document.documentElement.clientHeight
            const windowBottom = windowTop + windowHeight

            // TODO: Fix scroll if list exists in a modal (check for closest StickyProvider?)
            if (focusedTop + focusedHeight * 2 > windowBottom) {
                const y = focusedTop - windowHeight + focusedHeight * 2
                window.scrollTo({ top: y, behavior: 'smooth' })
            } else if (
                focusedTop - focusedHeight * 2 <
                windowTop + stickyOffset
            ) {
                const y =
                    focusedTop - focusedHeight - stickyOffset - stickyHeight
                window.scrollTo({ top: y, behavior: 'smooth' })
            }
        }
    }, [focused])

    const viewFocused = () => {
        if (focused.guid && !!data) {
            const focusedEntity = data.files.edges.find(
                (el) => el.guid === focused.guid,
            )
            navigate(focusedEntity.url)
            clearSelection()
        }
    }

    const toggleFocused = () => {
        if (focused.guid) {
            const selectedEntity = selected.find(
                (el) => el.guid === focused.guid,
            )
            if (selectedEntity) {
                toggleSelected(selectedEntity, true)
            } else {
                toggleSelected(
                    data.files.edges.find((el) => el.guid === focused.guid),
                    false,
                )
            }
        }
    }

    const [mutateCreatePad] = useMutation(gql`
        mutation AddPad($input: addPadInput!) {
            addPad(input: $input) {
                entity {
                    guid
                    ... on Pad {
                        url
                        group {
                            guid
                            url
                        }
                    }
                }
            }
        }
    `)

    const createPad = () => {
        // If we are in a folder, use the folder's accessId
        // If not the pad will take over the group or site default
        const folder =
            data?.containerEntity?.__typename === 'Folder'
                ? data.containerEntity
                : null

        const input = {
            containerGuid,
            title: t('global.untitled'),
            richDescription: '{"type":"doc","content":[]}',
            ...(folder ? { accessId: folder.accessId } : {}),
        }

        mutateCreatePad({
            variables: {
                input,
            },
            refetchQueries,
        })
            .then(({ data }) => {
                const { entity } = data.addPad
                navigate(entity.url, {
                    state: {
                        rootContainerGuid,
                        containerGuid,
                        orderBy,
                        orderDirection,
                    },
                })
            })
            .catch((error) => {
                console.error(error)
            })
    }

    const { t } = useTranslation()

    const [containerGuid, setContainerGuid] = useState()

    const [queryLimit, setQueryLimit] = useState(50)

    const { loading, data, fetchMore, refetch } = useQuery(GET_FILES_LIST, {
        variables: {
            rootContainerGuid,
            containerGuid,
            filterBookmarks,
            offset: 0,
            limit: queryLimit,
            orderBy,
            orderDirection,
        },
        skip: !containerGuid,
    })

    useLayoutEffect(() => {
        if (containerGuid) {
            setTimeout(() => {
                refetch()
            }, 0)
        }
    }, [containerGuid, refetch, filterBookmarks])

    useLayoutEffect(
        () => {
            clearSelection() // Clear selection when navigating to different folder

            if (!clickToSelect) {
                setContainerGuid(params.containerGuid || rootContainerGuid) // Update files list when navigating to different folder
            } else {
                if (!containerGuid) {
                    setContainerGuid(rootContainerGuid) // Set initial containerGuid
                }
            }
        },
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [params.containerGuid, containerGuid],
    )

    useWindowScrollPosition(`${containerGuid}-files`)

    const [ref, { width }] = useMeasure()

    const showExtraColumns = canCreateAndEdit || width > 500

    const canEditRoot = data?.rootEntity?.canEdit
    const accessIds =
        data?.rootEntity?.readAccessIds || data?.site?.readAccessIds

    const getScreenReaderLabel = (entity) => {
        const {
            title,
            subtype,
            owner: ownerObject,
            timePublished,
            timeUpdated,
            accessId,
            writeAccessId,
        } = entity

        const fileName = `${title} (${
            subtype === 'file' ? t('entity-file.file') : t('entity-file.folder')
        })`

        const owner = `${t('global.owner')} ${ownerObject.name}`

        const date =
            orderBy === 'timePublished'
                ? `${t('global.created')}: ${showDateTime(timePublished)}`
                : `${t('entity-file.modified')}: ${showDateTime(timeUpdated)}`

        const readPermissionDescription = accessIds?.find(
            (el) => el.id === accessId,
        )?.description
        const read = readPermissionDescription
            ? `${t('permissions.read')}: ${readPermissionDescription}`
            : ''

        const writePermissionDescription = accessIds?.find(
            (el) => el.id === writeAccessId,
        )?.description
        const write = writePermissionDescription
            ? `${t('permissions.write')}: ${writePermissionDescription}`
            : ''

        return `${fileName}. ${
            showExtraColumns
                ? `${owner}. ${date}. ${
                      canEditRoot ? `${read || ''}. ${write || ''}.` : ''
                  }`
                : ''
        }`
    }

    const singleSelected = selected.length === 1 && selected[0]
    const canEditSelected = !selected.some((el) => el.canEdit === false)
    const canArchiveAndDeleteSelected = !selected.some(
        (el) => el.canArchiveAndDelete === false,
    )

    return (
        <FilesContext.Provider
            value={{
                selected,
                setSelected,
                clickToSelect,
                setContainerGuid,
                selectableType,
            }}
        >
            <Wrapper
                gutter="none"
                alignItems="stretch"
                divider="large"
                {...rest}
            >
                <div className="FilesListContainer">
                    <FilesViewList
                        className="FilesList"
                        ref={ref}
                        rootContainerGuid={rootContainerGuid}
                        viewType={viewType}
                        loading={loading}
                        files={data?.files}
                        site={data?.site}
                        viewer={data?.viewer}
                        fetchMore={fetchMore}
                        setQueryLimit={setQueryLimit}
                        orderBy={orderBy}
                        orderDirection={orderDirection}
                        focused={focused}
                        selected={selected}
                        refFocused={refFocused}
                        setOrderBy={setOrderBy}
                        setOrderDirection={setOrderDirection}
                        toggleSelected={toggleSelected}
                        canEditRoot={canEditRoot}
                        accessIds={accessIds}
                        onCreatePad={createPad}
                        toggleUploadFileModal={toggleUploadFileModal}
                        toggleCreateFolderModal={toggleCreateFolderModal}
                        showExtraColumns={showExtraColumns}
                        canCreateAndEdit={canCreateAndEdit}
                        disabledGuids={disabledGuids}
                    />

                    {!loading && data?.files?.edges?.length > 0 && (
                        <HideVisually as="div">
                            <span id="listbox-item-description">
                                {t('entity-file.listbox-item-description')}
                            </span>
                            <ul
                                ref={refFiles}
                                tabIndex="0"
                                role={'listbox'}
                                aria-label={t('global.files')}
                                aria-multiselectable="true"
                                aria-activedescendant={focused.guid}
                                onFocus={handleFocus}
                                onBlur={handleBlur}
                                onKeyDown={handleKeyDown}
                                style={{ outline: 'none' }}
                            >
                                {data?.files?.edges.map((entity) => {
                                    const isSelected = !!selected.find(
                                        (el) => el.guid === entity.guid,
                                    )
                                    return (
                                        <li
                                            key={entity.guid}
                                            id={entity.guid}
                                            role="option"
                                            aria-selected={isSelected}
                                            aria-describedby="listbox-item-description"
                                        >
                                            {getScreenReaderLabel(entity)}
                                        </li>
                                    )
                                })}
                            </ul>
                        </HideVisually>
                    )}

                    <Header>
                        {selectable === 'multiple' && selected.length ? (
                            <>
                                <SelectedActions
                                    count={selected.length}
                                    onClear={handleClear}
                                />
                                {canCreateAndEdit && (
                                    <Flexer
                                        divider="normal"
                                        style={{
                                            paddingTop: 4,
                                            marginLeft: '12px',
                                        }}
                                    >
                                        <div
                                            style={{
                                                display: 'flex',
                                            }}
                                        >
                                            {singleSelected && (
                                                <>
                                                    {singleSelected.subtype ===
                                                        'pad' &&
                                                        data?.site
                                                            .collabEditingEnabled &&
                                                        singleSelected.canEdit && (
                                                            <ActionButton
                                                                as={Link}
                                                                to={
                                                                    singleSelected.group
                                                                        ? `${singleSelected.group.url}/pad/edit/${singleSelected.guid}`
                                                                        : `/files/edit/${singleSelected.guid}`
                                                                }
                                                                state={{
                                                                    prevPathname:
                                                                        location.pathname,
                                                                }}
                                                                tooltip={t(
                                                                    'action.edit',
                                                                )}
                                                                aria-label={t(
                                                                    'entity-file.edit-selection-label',
                                                                    {
                                                                        title: singleSelected.title,
                                                                    },
                                                                )}
                                                            >
                                                                <EditIcon />
                                                            </ActionButton>
                                                        )}
                                                    {singleSelected.subtype !==
                                                        'pad' &&
                                                        singleSelected.canEdit && (
                                                            <ActionButton
                                                                tooltip={t(
                                                                    'action.edit',
                                                                )}
                                                                aria-label={t(
                                                                    'entity-file.edit-selection-label',
                                                                    {
                                                                        title: singleSelected.title,
                                                                    },
                                                                )}
                                                                onClick={
                                                                    toggleEditFileFolderModal
                                                                }
                                                                disabled={
                                                                    !singleSelected.canEdit
                                                                }
                                                            >
                                                                <EditIcon />
                                                            </ActionButton>
                                                        )}
                                                    {singleSelected.canEdit && (
                                                        <ActionButton
                                                            tooltip={t(
                                                                'global.access',
                                                            )}
                                                            aria-label={t(
                                                                'entity-file.access-selection',
                                                                {
                                                                    title: singleSelected.title,
                                                                },
                                                            )}
                                                            onClick={
                                                                toggleAccessFileFolderModal
                                                            }
                                                        >
                                                            <UsersIcon />
                                                        </ActionButton>
                                                    )}
                                                </>
                                            )}

                                            {canEditSelected && (
                                                <ActionButton
                                                    tooltip={t('action.move')}
                                                    aria-label={t(
                                                        'entity-file.move-selection',
                                                        {
                                                            count: selected.length,
                                                        },
                                                    )}
                                                    onClick={
                                                        toggleMoveFileFolderModal
                                                    }
                                                >
                                                    <MoveIcon />
                                                </ActionButton>
                                            )}

                                            {canArchiveAndDeleteSelected && (
                                                <ActionButton
                                                    tooltip={t('action.delete')}
                                                    aria-label={t(
                                                        'entity-file.delete-selection',
                                                        {
                                                            count: selected.length,
                                                        },
                                                    )}
                                                    onClick={
                                                        toggleDeleteFileFolderModal
                                                    }
                                                >
                                                    <DeleteIcon />
                                                </ActionButton>
                                            )}
                                        </div>

                                        <div style={{ display: 'flex' }}>
                                            {singleSelected &&
                                                data?.viewer?.loggedIn && (
                                                    <StarButton
                                                        entity={singleSelected}
                                                    />
                                                )}
                                            <ActionButton
                                                variant="secondary"
                                                tooltip={t('action.download')}
                                                aria-label={t(
                                                    'entity-file.download-selection',
                                                    {
                                                        count: selected.length,
                                                    },
                                                )}
                                                onClick={downloadFiles}
                                            >
                                                <DownloadIcon />
                                            </ActionButton>
                                        </div>
                                    </Flexer>
                                )}
                            </>
                        ) : (
                            <Breadcrumb
                                guid={containerGuid}
                                clickToSelect={clickToSelect}
                                setContainerGuid={setContainerGuid}
                                style={{ marginLeft: '8px', flexGrow: 1 }}
                            />
                        )}

                        <Flexer
                            style={{
                                paddingTop: 4,
                                marginLeft: 'auto',
                            }}
                            divider="normal"
                        >
                            {canCreateAndEdit &&
                                data?.viewer.canWriteToContainer && (
                                    <ul
                                        style={{
                                            display: 'flex',
                                        }}
                                    >
                                        <li>
                                            <ActionButton
                                                tooltip={t(
                                                    'entity-file.create-folder',
                                                )}
                                                onClick={
                                                    toggleCreateFolderModal
                                                }
                                            >
                                                <CreateFolderIcon />
                                            </ActionButton>
                                        </li>

                                        {data?.site.collabEditingEnabled && (
                                            <li>
                                                <ActionButton
                                                    tooltip={t(
                                                        'entity-file.create-pad',
                                                    )}
                                                    onClick={createPad}
                                                >
                                                    <CreateFileIcon />
                                                </ActionButton>
                                            </li>
                                        )}
                                        <li>
                                            <ActionButton
                                                tooltip={t(
                                                    'entity-file.upload-files',
                                                )}
                                                onClick={toggleUploadFileModal}
                                            >
                                                <UploadIcon />
                                            </ActionButton>
                                        </li>
                                    </ul>
                                )}

                            <Flexer gutter="none" as="ul">
                                {data?.viewer?.loggedIn && (
                                    <li>
                                        <ActionButton
                                            variant="secondary"
                                            onClick={toggleFilterBookmarks}
                                            tooltip={t(
                                                'entity-file.show-favorites',
                                            )}
                                            aria-pressed={filterBookmarks}
                                        >
                                            {filterBookmarks ? (
                                                <StarFillIcon />
                                            ) : (
                                                <StarIcon />
                                            )}
                                        </ActionButton>
                                    </li>
                                )}
                                <li>
                                    <ActionButton
                                        variant="secondary"
                                        onClick={toggleViewType}
                                        role="switch"
                                        aria-checked={viewType === 'list'}
                                        tooltip={t(
                                            viewType === 'list'
                                                ? 'entity-file.tile-view'
                                                : 'entity-file.list-view',
                                        )}
                                        aria-label={t(
                                            viewType === 'list'
                                                ? 'entity-file.tile-view-aria'
                                                : 'entity-file.list-view-aria',
                                        )}
                                    >
                                        {viewType === 'list' ? (
                                            <TileIcon />
                                        ) : (
                                            <ListIcon />
                                        )}
                                    </ActionButton>
                                </li>
                                <li>
                                    <ActionButton
                                        variant="secondary"
                                        onClick={() =>
                                            setShowDetails(!showDetails)
                                        }
                                        tooltip={
                                            showDetails
                                                ? t('entity-file.hide-details')
                                                : t('entity-file.show-details')
                                        }
                                        aria-pressed={showDetails}
                                    >
                                        <InfoIcon />
                                    </ActionButton>
                                </li>
                            </Flexer>
                        </Flexer>
                    </Header>
                </div>

                <Details
                    className="FilesDetails"
                    showDetails={showDetails}
                    setShowDetails={setShowDetails}
                    selected={selected}
                    folderEntity={
                        data?.containerEntity?.__typename === 'Folder' &&
                        data?.containerEntity
                    }
                    rootEntity={data?.rootEntity}
                    accessIds={data?.rootEntity?.readAccessIds}
                    site={data?.site}
                />
            </Wrapper>

            <Modal
                isVisible={showUploadFileModal}
                size="normal"
                title={t('entity-file.upload-files')}
                onClose={toggleUploadFileModal}
                showCloseButton
            >
                <UploadFile
                    onClose={toggleUploadFileModal}
                    containerGuid={containerGuid}
                    groupGuid={groupGuid}
                    defaultTags={data?.rootEntity?.defaultTags}
                    defaultTagCategories={
                        data?.rootEntity?.defaultTagCategories
                    }
                />
            </Modal>

            <Modal
                isVisible={showCreateFolderModal}
                title={t('entity-file.create-folder')}
                size="normal"
                onClose={toggleCreateFolderModal}
            >
                <CreateFolder
                    containerGuid={containerGuid}
                    groupGuid={groupGuid}
                    viewer={data?.viewer}
                    onClose={toggleCreateFolderModal}
                    onComplete={clearSelection}
                />
            </Modal>

            <Modal
                isVisible={showEditFileFolderModal}
                size="normal"
                title={
                    singleSelected?.subtype === 'file'
                        ? t('entity-file.edit-file')
                        : t('entity-file.edit-folder')
                }
                onClose={toggleEditFileFolderModal}
                showCloseButton
            >
                <EditFileFolder
                    onClose={toggleEditFileFolderModal}
                    entity={singleSelected}
                    onComplete={clearSelection}
                />
            </Modal>

            <Modal
                isVisible={showAccessFileFolderModal}
                size="normal"
                title={t('global.access')}
                onClose={toggleAccessFileFolderModal}
                showCloseButton
            >
                <AccessFileFolder
                    viewer={data?.viewer}
                    onClose={toggleAccessFileFolderModal}
                    entity={singleSelected}
                    onComplete={toggleAccessFileFolderModal}
                    containerGuid={containerGuid}
                />
            </Modal>

            <Modal
                isVisible={showMoveFileFolderModal}
                onClose={toggleMoveFileFolderModal}
                size="large"
                noPadding
                containHeight
                growHeight
            >
                <MoveFileFolder
                    onClose={toggleMoveFileFolderModal}
                    entities={selected}
                    containerGuid={containerGuid}
                    rootContainerGuid={rootContainerGuid}
                    onSuccess={() => setSelected([])}
                    showMyFiles={rootContainerGuid === data?.viewer?.user?.guid}
                />
            </Modal>

            <DeleteEntitiesModal
                title={t('entity-file.delete-selection', {
                    count: selected.length,
                })}
                isVisible={showDeleteFileFolderModal}
                onClose={toggleDeleteFileFolderModal}
                entities={selected}
                setSelected={setSelected}
                onComplete={() => {
                    toggleDeleteFileFolderModal()
                    clearSelection()
                }}
                refetchQueries={refetchQueries}
            />
        </FilesContext.Provider>
    )
}

FilesView.propTypes = {
    selectable: PropTypes.oneOf(['single', 'multiple']),
    selectableType: PropTypes.oneOf(['all', 'image', 'folder']),
}

export default FilesView
