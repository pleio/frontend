import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

import Button from 'js/components/Button/Button'
import FileFolderIcon from 'js/files/components/FileFolderIcon'
import getIconNameByMimetype from 'js/files/helpers/getIconNameByMimetype'
import PadView from 'js/pad/View'

import DownloadIcon from 'icons/download-small.svg'

const Wrapper = styled.div`
    position: relative;
    display: flex;
    align-items: center;
    justify-content: center;

    .FileViewFile {
        display: flex;
        flex-direction: column;
        align-items: center;
        padding: 20px;

        img {
            pointer-events: none;
            user-select: none;
        }
    }

    .FileViewImage {
        position: relative;
        width: auto;
        height: auto;
        max-width: 100%;
        max-height: calc(100vh - ${(p) => p.theme.headerBarHeight}px);
        user-select: none;
        opacity: 0;

        ${(p) =>
            p.$showImage &&
            css`
                transition: opacity ${(p) => p.theme.transition.normal};
                opacity: 1;
                z-index: 1;
            `};
    }

    .FileViewImageThumbnail {
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background-repeat: no-repeat;
        background-size: contain;
        background-position: center;
        filter: blur(5px);
    }
`

const FileViewContent = ({ entity, ...rest }) => {
    const { t } = useTranslation()

    const [showImage, setShowImage] = useState(false)

    useEffect(() => {
        setShowImage(false)
    }, [entity])

    if (!entity) return null

    const { subtype, title, mimeType, download, thumbnail } = entity

    if (mimeType && mimeType.indexOf('image/') !== -1) {
        const handleLoadImage = () => {
            setShowImage(true)
        }
        return (
            <Wrapper $showImage={showImage} {...rest}>
                <div style={{ overflow: 'hidden', position: 'relative' }}>
                    <img
                        src={download}
                        alt={title}
                        className="FileViewImage"
                        onLoad={handleLoadImage}
                    />
                    <div
                        className="FileViewImageThumbnail"
                        style={{
                            backgroundImage: `url(${thumbnail})`,
                        }}
                    />
                </div>
            </Wrapper>
        )
    } else if (subtype === 'pad') {
        return (
            <div {...rest}>
                <PadView entity={entity} />
            </div>
        )
    } else {
        const iconName = getIconNameByMimetype(mimeType)

        return (
            <Wrapper {...rest}>
                <div className="FileViewFile">
                    <FileFolderIcon
                        name={iconName}
                        style={{
                            width: '36px',
                            height: '48px',
                        }}
                    />
                    <span style={{ paddingTop: '8px' }}>{title}</span>
                    <Button
                        size="normal"
                        variant="tertiary"
                        as="a"
                        href={entity.download}
                        target="_blank"
                        rel="noopener noreferrer"
                        aria-label={`${t('entity-file.download-file', {
                            title,
                        })}${t('global.opens-in-new-window')}`}
                        style={{ marginTop: '8px' }}
                    >
                        <DownloadIcon style={{ marginRight: '6px' }} />
                        {t('action.download')}
                    </Button>
                </div>
            </Wrapper>
        )
    }
}

FileViewContent.propTypes = {
    entity: PropTypes.object,
}

export default FileViewContent
