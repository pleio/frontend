import React, { useEffect, useRef, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useLocation, useNavigate } from 'react-router-dom'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { media } from 'helpers'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import DropdownButton from 'js/components/DropdownButton/DropdownButton'
import Flexer from 'js/components/Flexer/Flexer'
import { Container } from 'js/components/Grid/Grid'
import HeaderBar from 'js/components/HeaderBar/HeaderBar'
import HideVisually from 'js/components/HideVisually/HideVisually'
import IconButton from 'js/components/IconButton/IconButton'
import DeleteEntitiesModal from 'js/components/Item/ItemActions/components/DeleteEntitiesModal'
import Modal from 'js/components/Modal/Modal'
import Tooltip from 'js/components/Tooltip/Tooltip'
import AccessFileFolder from 'js/files/components/AccessFileFolder'
import ActionButton from 'js/files/components/ActionButton'
import Breadcrumb from 'js/files/components/Breadcrumb'
import EditFileFolder from 'js/files/components/EditFileFolder'
import MoveFileFolder from 'js/files/components/MoveFileFolder'

import BackIcon from 'icons/arrow-left.svg'
import LeftIcon from 'icons/chevron-left-large.svg'
import RightIcon from 'icons/chevron-right-large.svg'
import DeleteIcon from 'icons/delete.svg'
import DownloadIcon from 'icons/download.svg'
import EditIcon from 'icons/edit.svg'
import InfoIcon from 'icons/info.svg'
import CopyIcon from 'icons/link.svg'
import MoveIcon from 'icons/move-to-folder.svg'
import UsersIcon from 'icons/users.svg'

const Wrapper = styled(HeaderBar)`
    position: relative;
    padding: 4px 0;
    z-index: 11;

    .FileViewContainer {
        display: flex;
        align-items: center;
        justify-content: space-between;
        height: 100%;

        ${media.mobileLandscapeDown`
            flex-wrap: wrap;
        `};
    }

    .FileViewColumn {
        display: flex;
        align-items: center;
        overflow: hidden;

        ${media.tabletUp`
            width: calc(50% - 80px);
        `};
    }

    .FileViewPager {
        display: flex;
        align-items: center;
        justify-content: center;

        > *[disabled] {
            opacity: 0;
        }

        ${media.tabletUp`
            width: 160px;
        `};
    }

    .FileViewPagerLabel {
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        color: ${(p) => p.theme.color.text.grey};
        margin: 1px -2px 0;
        user-select: none;
        white-space: nowrap;
    }

    .FileViewTitle {
        margin-bottom: 2px;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        font-weight: ${(p) => p.theme.font.weight.semibold};
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
    }
`

const PadDownloadOptions = ({ downloadLinks }) => {
    const { t } = useTranslation()

    const options = downloadLinks.map(({ type, url }) => {
        const title = `${t('action.download-as')} ${type}`

        return {
            name: title,
            label: title,
            href: url,
        }
    })

    return (
        <DropdownButton options={options}>
            <ActionButton
                variant="secondary"
                tooltip={t('action.download')}
                aria-label={t('action.show-download-options')}
            >
                <DownloadIcon />
            </ActionButton>
        </DropdownButton>
    )
}

const FileViewHeader = ({
    index,
    site,
    entity,
    orderBy = '',
    orderDirection = '',
    containerGuid,
    data,
    showDetails,
    setShowDetails,
    viewer,
}) => {
    const { t } = useTranslation()

    const navigate = useNavigate()
    const { pathname, state } = useLocation()

    const refFileAddress = useRef()

    const [isCopied, setIsCopied] = useState(false)

    const [currentIndex, setCurrentIndex] = useState()

    useEffect(() => {
        if (!index && !!entity && !!data?.files) {
            const fileIndex = data.files.edges.findIndex(
                (item) => item.guid === entity.guid,
            )
            setCurrentIndex(fileIndex)
        }

        window.addEventListener('keydown', handleKeyDown)

        return () => {
            window.removeEventListener('keydown', handleKeyDown)
        }
    })

    useEffect(() => {
        if (index) {
            setCurrentIndex(index)
        }
    }, [index])

    const handleKeyDown = (evt) => {
        if (
            showEditFileFolderModal ||
            showAccessFileFolderModal ||
            showMoveFileFolderModal ||
            showDeleteFileFolderModal
        )
            return

        switch (evt.key) {
            case 'ArrowLeft':
            case 'a':
                !!prevLink &&
                    navigate(prevLink, {
                        state: {
                            containerGuid,
                            index: currentIndex - 1,
                            orderBy,
                            orderDirection,
                        },
                    })
                break

            case 'ArrowRight':
            case 'd':
                !!nextLink &&
                    navigate(nextLink, {
                        state: {
                            containerGuid,
                            index: currentIndex + 1,
                            orderBy,
                            orderDirection,
                        },
                    })
                break
        }
    }

    const copyFileAddress = () => {
        refFileAddress.current.select()
        document.execCommand('copy')
        setIsCopied(true)
        setTimeout(() => setIsCopied(false), 3000)
    }

    const {
        title,
        guid,
        subtype,
        canEdit,
        rootContainer,
        canArchiveAndDelete,
        url,
        download,
        container,
        parentFolder,
    } = entity || {}

    const handleDeleteFileComplete = () => {
        navigate(containerLink, { replace: true })
    }

    const [showEditFileFolderModal, setShowEditFileFolderModal] =
        useState(false)
    const toggleEditFileFolderModal = () =>
        setShowEditFileFolderModal(!showEditFileFolderModal)

    const [showAccessFileFolderModal, setShowAccessFileFolderModal] =
        useState(false)
    const toggleAccessFileFolderModal = () =>
        setShowAccessFileFolderModal(!showAccessFileFolderModal)

    const [showMoveFileFolderModal, setShowMoveFileFolderModal] =
        useState(false)
    const toggleMoveFileFolderModal = () =>
        setShowMoveFileFolderModal(!showMoveFileFolderModal)

    const [showDeleteFileFolderModal, setShowDeleteFileFolderModal] =
        useState(false)
    const toggleDeleteFileFolderModal = () =>
        setShowDeleteFileFolderModal(!showDeleteFileFolderModal)

    const typeName = container?.__typename
    const containerLink =
        typeName === 'Group'
            ? `${container.url}/files`
            : typeName === 'User'
              ? `/user/${container.guid}/files`
              : typeName === 'Folder'
                ? container.url
                : ''

    const backLink = state?.prevPathname || containerLink
    const backLinkProps =
        state?.prevPathname && state?.prevPathname !== containerLink
            ? {
                  tooltip: t('action.go-back'),
                  'aria-label': t('entity-file.go-back-aria', {
                      link: state?.prevPathname,
                      interpolation: { escapeValue: false }, // Do not escape characters like '
                  }),
              }
            : {
                  tooltip: t('entity-file.go-back-to-folder'),
                  'aria-label': t('entity-file.go-back-to-folder-aria', {
                      title: parentFolder?.title || t('global.files'),
                      interpolation: { escapeValue: false }, // Do not escape characters like '
                  }),
              }

    const prevLink =
        currentIndex !== undefined
            ? data?.files?.edges[currentIndex - 1]?.url || ''
            : ''
    const nextLink =
        currentIndex !== undefined
            ? data?.files?.edges[currentIndex + 1]?.url || ''
            : ''

    const downloadLink = download || ''
    const downloadLinks =
        data?.files?.edges[currentIndex]?.downloadAsOptions || []

    return (
        <Wrapper $backgroundColor="white" as="header">
            <Container className="FileViewContainer">
                <div className="FileViewColumn">
                    {backLink && (
                        <IconButton
                            as={Link}
                            to={backLink}
                            size="large"
                            variant="secondary"
                            offset={[0, 4]}
                            {...backLinkProps}
                        >
                            <BackIcon />
                        </IconButton>
                    )}

                    <div style={{ overflow: 'hidden', marginTop: '-1px' }}>
                        {entity && (
                            <h1
                                className="FileViewTitle"
                                aria-current
                                title={title}
                                aria-describedby="file-heading"
                            >
                                {title}
                            </h1>
                        )}
                        <Breadcrumb guid={containerGuid} size="tiny" />
                    </div>
                </div>

                {data?.files?.edges?.length > 1 && (
                    <div className="FileViewPager">
                        <IconButton
                            as={Link}
                            to={prevLink}
                            state={{
                                containerGuid,
                                index: currentIndex - 1,
                                orderBy,
                                orderDirection,
                                prevPathname: state?.prevPathname,
                            }}
                            size="large"
                            hoverSize="normal"
                            variant="secondary"
                            disabled={!prevLink}
                            aria-label={t('entity-file.view-previous')}
                        >
                            <LeftIcon />
                        </IconButton>

                        <div className="FileViewPagerLabel">
                            {t('global.count-of-total', {
                                count:
                                    currentIndex !== undefined &&
                                    currentIndex + 1,
                                total: data.files.total,
                            })}
                        </div>

                        <IconButton
                            as={Link}
                            to={nextLink}
                            state={{
                                containerGuid,
                                index: currentIndex + 1,
                                orderBy,
                                orderDirection,
                                prevPathname: state?.prevPathname,
                            }}
                            size="large"
                            hoverSize="normal"
                            variant="secondary"
                            disabled={!nextLink}
                            aria-label={t('entity-file.view-next')}
                        >
                            <RightIcon />
                        </IconButton>
                    </div>
                )}

                <Flexer
                    className="FileViewColumn"
                    wrap
                    divider="normal"
                    style={{ justifyContent: 'flex-end' }}
                >
                    <div style={{ display: 'flex' }}>
                        <Flexer gutter="small">
                            {subtype === 'pad' &&
                                site?.collabEditingEnabled &&
                                canEdit && (
                                    <ActionButton
                                        as={Link}
                                        to={
                                            rootContainer?.url
                                                ? `${rootContainer.url}/pad/edit/${guid}`
                                                : `/files/edit/${guid}`
                                        }
                                        state={{
                                            prevPathname: pathname,
                                        }}
                                        tooltip={t('action.edit')}
                                        aria-label={t(
                                            'entity-file.edit-file-label',
                                            {
                                                title,
                                            },
                                        )}
                                    >
                                        <EditIcon />
                                    </ActionButton>
                                )}
                            {subtype !== 'pad' && canEdit && (
                                <>
                                    <ActionButton
                                        onClick={toggleEditFileFolderModal}
                                        tooltip={t('action.edit')}
                                        aria-label={t(
                                            'entity-file.edit-file-label',
                                            {
                                                title,
                                            },
                                        )}
                                    >
                                        <EditIcon />
                                    </ActionButton>
                                    <Modal
                                        isVisible={showEditFileFolderModal}
                                        size="normal"
                                        title={
                                            subtype === 'file'
                                                ? t('entity-file.edit-file')
                                                : t('entity-file.edit-folder')
                                        }
                                        onClose={toggleEditFileFolderModal}
                                        showCloseButton
                                    >
                                        <EditFileFolder
                                            onClose={toggleEditFileFolderModal}
                                            entity={entity}
                                            onComplete={
                                                toggleEditFileFolderModal
                                            }
                                        />
                                    </Modal>
                                </>
                            )}
                        </Flexer>

                        {canEdit && (
                            <>
                                <ActionButton
                                    onClick={toggleAccessFileFolderModal}
                                    tooltip={t('global.access')}
                                    aria-label={t('entity-file.edit-access', {
                                        title,
                                    })}
                                >
                                    <UsersIcon />
                                </ActionButton>
                                <Modal
                                    isVisible={showAccessFileFolderModal}
                                    size="normal"
                                    title={t('global.access')}
                                    onClose={toggleAccessFileFolderModal}
                                    showCloseButton
                                >
                                    <AccessFileFolder
                                        onClose={toggleAccessFileFolderModal}
                                        entity={entity}
                                        onComplete={toggleAccessFileFolderModal}
                                        viewer={viewer}
                                        containerGuid={containerGuid}
                                    />
                                </Modal>
                            </>
                        )}

                        {canEdit && (
                            <>
                                <ActionButton
                                    onClick={toggleMoveFileFolderModal}
                                    tooltip={t('action.move')}
                                    aria-label={t('entity-file.move-file', {
                                        title,
                                    })}
                                >
                                    <MoveIcon />
                                </ActionButton>
                                <Modal
                                    isVisible={showMoveFileFolderModal}
                                    size="large"
                                    onClose={toggleMoveFileFolderModal}
                                    noPadding
                                    containHeight
                                    growHeight
                                >
                                    <MoveFileFolder
                                        onClose={toggleMoveFileFolderModal}
                                        entities={[entity]}
                                        containerGuid={containerGuid}
                                        rootContainerGuid={rootContainer?.guid}
                                        showMyFiles={
                                            rootContainer?.guid ===
                                            viewer?.user?.guid
                                        }
                                    />
                                </Modal>
                            </>
                        )}

                        {canArchiveAndDelete && (
                            <>
                                <ActionButton
                                    onClick={toggleDeleteFileFolderModal}
                                    tooltip={t('action.delete')}
                                    aria-label={t('entity-file.delete-file', {
                                        title,
                                    })}
                                >
                                    <DeleteIcon />
                                </ActionButton>

                                <DeleteEntitiesModal
                                    isVisible={showDeleteFileFolderModal}
                                    onClose={toggleDeleteFileFolderModal}
                                    entities={[entity]}
                                    onComplete={handleDeleteFileComplete}
                                    refetchQueries={['FilesList']}
                                />
                            </>
                        )}
                    </div>

                    <div style={{ display: 'flex' }}>
                        <Tooltip
                            visible={isCopied}
                            content={t('entity-file.link-copied')}
                            offset={[0, 0]}
                        >
                            <ActionButton
                                variant="secondary"
                                onClick={copyFileAddress}
                                tooltip={t('entity-file.copy-link')}
                                aria-label={t('entity-file.copy-link-to', {
                                    title,
                                })}
                            >
                                <CopyIcon />
                            </ActionButton>
                        </Tooltip>
                        <HideVisually aria-hidden>
                            <input
                                tabIndex="-1"
                                type="text"
                                ref={refFileAddress}
                                value={`${window.location.origin}${url}`}
                                readOnly
                            />
                        </HideVisually>
                        {isCopied && (
                            <HideVisually role="alert">
                                {t('entity-file.link-copied')}
                            </HideVisually>
                        )}

                        {downloadLinks.length > 0 ? (
                            <PadDownloadOptions downloadLinks={downloadLinks} />
                        ) : (
                            <ActionButton
                                disabled={!downloadLink}
                                as="a"
                                variant={
                                    downloadLink ? 'secondary' : 'tertiary'
                                }
                                href={downloadLink}
                                target="_blank"
                                rel="noopener noreferrer"
                                tooltip={t('action.download')}
                                aria-label={`${t('entity-file.download-file', {
                                    title,
                                })}${t('global.opens-in-new-window')}`}
                            >
                                <DownloadIcon />
                            </ActionButton>
                        )}

                        <ActionButton
                            variant="secondary"
                            onClick={() => setShowDetails(!showDetails)}
                            tooltip={
                                showDetails
                                    ? t('entity-file.hide-details')
                                    : t('entity-file.show-details')
                            }
                            aria-pressed={showDetails}
                        >
                            <InfoIcon />
                        </ActionButton>
                    </div>
                </Flexer>
            </Container>
        </Wrapper>
    )
}

FileViewHeader.propTypes = {
    entity: PropTypes.object,
    containerGuid: PropTypes.string,
    orderBy: PropTypes.string,
    orderDirection: PropTypes.string,
}

const GET_BREADCRUMB = gql`
    query FileHeader(
        $containerGuid: String
        $orderBy: String
        $orderDirection: String
    ) {
        breadcrumb(guid: $containerGuid) {
            ... on Folder {
                guid
                title
                url
            }
        }
        files(
            containerGuid: $containerGuid
            typeFilter: ["file", "pad"]
            offset: 0
            limit: 999
            orderBy: $orderBy
            orderDirection: $orderDirection
        ) {
            total
            edges {
                guid
                ... on File {
                    title
                    url
                }
                ... on Pad {
                    title
                    url
                    downloadAsOptions {
                        type
                        url
                    }
                }
            }
        }
    }
`

export default graphql(GET_BREADCRUMB, {
    options: {
        fetchPolicy: 'cache-and-network',
    },
    skip: (props) => !props.containerGuid,
})(FileViewHeader)
