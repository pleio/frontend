import React, { useEffect } from 'react'
import { useLocation, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { media, useLocalStorage } from 'helpers'
import styled from 'styled-components'

import Document from 'js/components/Document'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import NotFound from 'js/core/NotFound'
import DetailsFileFolder from 'js/files/Details/components/DetailsFileFolder'

import FileViewContent from './FileViewContent'
import FileViewHeader from './FileViewHeader'

const Wrapper = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    display: flex;
    flex-direction: column;
    z-index: 12;
    background-color: white;

    .FileViewWrapper {
        flex-grow: 1;
        display: flex;
        overflow: hidden; // Allow scrollbar on .FileViewContent

        ${media.mobilePortrait`
            flex-direction: column;
        `}
    }

    .FileViewContent {
        flex-grow: 1;
        overflow: auto;

        ${media.mobilePortrait`
            height: 100%; // Evenly divide height with .FileViewDetails
        `}
    }

    .FileViewDetails {
        padding: 8px 16px 20px;
        overflow-y: auto;

        ${media.mobilePortrait`
            width: 100%;
            height: 100%; // Evenly divide height with .FileViewContent
            box-shadow: ${(p) => p.theme.shadow.hard.top};
        `}

        ${media.mobileLandscapeUp`
            box-shadow: ${(p) => p.theme.shadow.hard.left};
        `}

        ${media.mobileLandscape`
            width: 50%;
        `}

        ${media.tabletUp`
            width: 380px;
        `}
    }
`

const FileView = () => {
    const location = useLocation()
    const { guid } = useParams()

    // Coming from the edit pad page, we need to update the richDescription
    // because the server might not have the latest version yet
    const richDescription = location.state?.richDescription

    const { loading, data, client } = useQuery(GET_FILE_ITEM, {
        variables: {
            guid,
        },
        fetchPolicy: 'cache-and-network',
    })

    useEffect(() => {
        if (!richDescription || loading || !data) return

        const { site = {}, entity = {} } = data || {}

        client.writeQuery({
            query: GET_FILE_ITEM,
            data: {
                site,
                entity: {
                    ...entity,
                    richDescription,
                },
            },
            variables: {
                guid,
            },
        })
    }, [richDescription, data, client, loading, guid])

    const state = location?.state

    const [showDetails, setShowDetails] = useLocalStorage(
        'file-show-details',
        true,
    )

    return (
        <Wrapper>
            <Document title={data?.entity?.title} />
            <FileViewHeader
                index={state?.index}
                site={data?.site}
                entity={data?.entity}
                containerGuid={
                    state?.containerGuid ||
                    data?.entity?.container?.guid ||
                    null
                }
                viewer={data?.viewer}
                orderBy={state?.orderBy}
                orderDirection={state?.orderDirection}
                showDetails={showDetails}
                setShowDetails={setShowDetails}
                data={data}
            />
            {loading ? (
                <LoadingSpinner style={{ flexGrow: 1 }} />
            ) : data?.entity ? (
                <div className="FileViewWrapper">
                    <FileViewContent
                        entity={data.entity}
                        className="FileViewContent"
                    />
                    {showDetails && (
                        <div className="FileViewDetails">
                            <DetailsFileFolder
                                entity={data.entity}
                                site={data.site}
                                accessIds={
                                    data.entity.rootContainer?.readAccessIds ||
                                    window.__SETTINGS__.site.readAccessIds
                                }
                            />
                        </div>
                    )}
                </div>
            ) : (
                <NotFound />
            )}
        </Wrapper>
    )
}

const GET_FILE_ITEM = gql`
    query FileItem($guid: String!) {
        site {
            guid
            collabEditingEnabled
            showOriginalFileName
            showTagsInDetail
            showCustomTagsInDetail
        }
        viewer {
            guid
            canUpdateAccessLevel
            user {
                guid
            }
        }
        entity(guid: $guid, incrementViewCount: true) {
            guid
            ... on FileFolder {
                title
                description
                richDescription
                localRichDescription
                timePublished
                subtype
                timeUpdated
                url
                canEdit
                canArchiveAndDelete
                accessId
                writeAccessId
                tags
                owner {
                    guid
                    name
                }
                views
                parentFolder {
                    guid
                    title
                }
                rootContainer {
                    guid
                    ... on Group {
                        url
                        readAccessIds {
                            id
                            description
                        }
                    }
                }
                container {
                    guid
                    ... on User {
                        url
                    }
                    ... on Group {
                        url
                    }
                    ... on FileFolder {
                        url
                    }
                }
                referenceCount
                references {
                    guid
                    label
                    url
                    referenceType
                    entityType
                }
            }
            ... on File {
                size
                mimeType
                download
                thumbnail
                inputLanguage
                originalFileName
                tagCategories {
                    name
                    values
                }
            }
            ... on Pad {
                tagCategories {
                    name
                    values
                }
                isTranslationEnabled
            }
        }
    }
`

export default FileView
