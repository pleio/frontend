import React, { useContext, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import Avatar from 'js/components/Avatar/Avatar'
import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import { H3 } from 'js/components/Heading'
import IconButton from 'js/components/IconButton/IconButton'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import StickyProvider from 'js/components/Sticky/StickyProvider'
import { TiptapEditorContext } from 'js/components/Tiptap/TiptapEditor'
import FilesView from 'js/files/FilesView'
import { iconViewFragment } from 'js/lib/fragments/icon'

import CrossIcon from 'icons/cross-large.svg'
import FolderIcon from 'icons/folder.svg'

import MenuItem from './MenuItem'

const Wrapper = styled.div`
    flex-grow: 1;
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: stretch;
    overflow-y: hidden; // Needed for correct scroll behaviour

    .SelectFromFilesHeader {
        display: flex;
        align-items: center;
        justify-content: space-between;
        padding: 10px 8px 10px 20px;
        border-bottom: 1px solid ${(p) => p.theme.color.grey[30]};
    }

    .SelectFromFilesTitle {
        color: ${(p) => p.theme.color.primary.main};
    }

    .SelectFromFilesContainer {
        display: flex;
        flex-grow: 1;
        overflow-y: hidden; // Needed for correct scroll behaviour
    }

    .SelectFromFilesMenu {
        flex-shrink: 0;
        width: 220px;
        padding: 8px 4px;
        border-right: 1px solid ${(p) => p.theme.color.grey[30]};
        overflow-y: auto;
    }

    .SelectFromFilesActions {
        flex-shrink: 0;
        min-height: ${(p) => p.theme.headerBarHeight}px;
        border-top: 1px solid ${(p) => p.theme.color.grey[30]};
    }

    .SelectFromFilesListWrapper {
        flex-grow: 1;
        overflow-y: auto;
    }
`

const SelectFromFiles = ({
    multiple,
    selectableType,
    onClose,
    onAddFiles,
    title,
    disabledGuids = [],
    showMyFiles = true,
    isSubmitting,
    rootContainerGuid,
}) => {
    const { t } = useTranslation()

    const params = useParams()

    const isFolderSelect = selectableType === 'folder'

    const tiptapEditorContext = useContext(TiptapEditorContext) // PLEASE NOTE: This context is not always available!

    const { loading, data } = useQuery(GET_MY_GROUPS, {
        variables: {
            offset: 0,
            limit: 999,
        },
    })

    // Check wether currently in group chat or page and set the group menu item accordingly
    const [currentGroupGuid, setCurrentGroupGuid] = useState(
        tiptapEditorContext?.groupGuid ||
            rootContainerGuid ||
            params?.groupGuid ||
            null,
    )

    const [selected, setSelected] = useState([])

    const [destinationLabel, setDestinationLabel] = useState()

    const getCurrentGroup = (guid) => {
        return data?.groups.edges.find((g) => g.guid === guid) || { guid }
    }

    useEffect(() => {
        // Make sure group names are available
        if (!loading && isFolderSelect) {
            if (selected.length) {
                setDestinationLabel(selected[0].title)
            } else if (isMyFiles) {
                setDestinationLabel(t('user.my-files'))
            } else {
                setDestinationLabel(getCurrentGroup(currentGroupGuid)?.name)
            }
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [loading, selected, isFolderSelect, currentGroupGuid])

    useEffect(() => {
        setSelected([])
    }, [currentGroupGuid])

    const handleAddSelection = () => {
        if (isFolderSelect) {
            // If no folder is selected, the group root container is the target
            onAddFiles(
                selected.length
                    ? selected
                    : [getCurrentGroup(currentGroupGuid)],
            )
        } else {
            onAddFiles(selected)
        }
    }

    const isMyFiles =
        !currentGroupGuid || currentGroupGuid === data?.viewer?.user?.guid

    return (
        <Wrapper>
            <div className="SelectFromFilesHeader">
                <H3 as="h2" className="SelectFromFilesTitle" id="modal-title">
                    {title}
                </H3>
                <IconButton
                    variant="secondary"
                    size="large"
                    aria-label={t('action.close-modal')}
                    onClick={onClose}
                >
                    <CrossIcon />
                </IconButton>
            </div>
            <div className="SelectFromFilesContainer">
                <div className="SelectFromFilesMenu">
                    {loading ? (
                        <LoadingSpinner />
                    ) : (
                        <ul>
                            {showMyFiles && (
                                <MenuItem
                                    label={t('user.my-files')}
                                    icon={<FolderIcon />}
                                    active={isMyFiles} // If no group is selected, my files is active
                                    onClick={() =>
                                        setCurrentGroupGuid(
                                            data?.viewer?.user?.guid,
                                        )
                                    }
                                />
                            )}

                            {data.groups.edges.map(({ guid, name, icon }) => (
                                <MenuItem
                                    key={guid}
                                    label={name}
                                    icon={
                                        <Avatar
                                            disabled
                                            image={icon?.download}
                                            name={name}
                                            size="small"
                                            radiusStyle="rounded"
                                        />
                                    }
                                    active={currentGroupGuid === guid}
                                    onClick={() => setCurrentGroupGuid(guid)}
                                />
                            ))}
                        </ul>
                    )}
                </div>
                <StickyProvider className="SelectFromFilesListWrapper">
                    {currentGroupGuid ? (
                        <FilesView
                            className="SelectFromFilesList"
                            key={currentGroupGuid}
                            rootContainerGuid={currentGroupGuid}
                            selected={selected}
                            setSelected={setSelected}
                            selectable={multiple ? 'multiple' : 'single'}
                            selectableType={selectableType}
                            disabledGuids={disabledGuids}
                            clickToSelect
                        />
                    ) : !loading ? (
                        <FilesView
                            className="SelectFromFilesList"
                            key={data?.viewer?.user?.guid}
                            rootContainerGuid={data?.viewer?.user?.guid}
                            selected={selected}
                            setSelected={setSelected}
                            selectable={multiple ? 'multiple' : 'single'}
                            selectableType={selectableType}
                            disabledGuids={disabledGuids}
                            clickToSelect
                        />
                    ) : null}
                </StickyProvider>
            </div>
            <Flexer className="SelectFromFilesActions">
                <Button variant="tertiary" size="normal" onClick={onClose}>
                    {t('action.cancel')}
                </Button>
                <Button
                    size="normal"
                    variant="primary"
                    loading={isSubmitting}
                    disabled={
                        (!isFolderSelect && selected.length === 0) ||
                        (isFolderSelect && !destinationLabel)
                    }
                    onClick={handleAddSelection}
                >
                    {destinationLabel
                        ? `${t('entity-file.move-to')} '${destinationLabel}'`
                        : t('entity-file.add-selection')}
                </Button>
            </Flexer>
        </Wrapper>
    )
}

SelectFromFiles.propTypes = {
    selectableType: PropTypes.oneOf(['all', 'image', 'folder']),
}

const GET_MY_GROUPS = gql`
    query GroupsQuery($offset: Int!, $limit: Int!) {
        viewer {
            guid
            user {
                guid
            }
        }
        groups(filter: mine, offset: $offset, limit: $limit) {
            edges {
                guid
                name
                ${iconViewFragment}
                defaultReadAccessId
                defaultWriteAccessId
                readAccessIds {
                    id
                    description
                }
            }
        }
    }
`

export default SelectFromFiles
