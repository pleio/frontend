import React from 'react'
import styled from 'styled-components'

import Avatar from 'js/components/Avatar/Avatar'

const Wrapper = styled.button`
    position: relative;
    width: 100%;
    display: flex;
    padding: 4px 8px;
    font-size: ${(p) => p.theme.font.size.small};
    line-height: ${(p) => p.theme.font.lineHeight.small};
    border-radius: ${(p) => p.theme.radius.normal};

    &:hover {
        background-color: ${(p) => p.theme.color.hover};
    }

    &:active {
        background-color: ${(p) => p.theme.color.active};
    }

    &[aria-current='item'] {
        color: ${(p) => p.theme.color.primary.main};

        &:before {
            content: '';
            position: absolute;
            left: 0;
            top: 4px;
            width: 4px;
            height: 24px;
            border-radius: 2px;
            background-color: ${(p) => p.theme.color.primary.main};
        }
    }

    .MenuItemIcon {
        width: 24px;
        height: 24px;
        display: flex;
        align-items: center;
        justify-content: center;
        margin-right: 8px;
    }

    .MenuItemLabel {
        padding-top: 2px;
    }
`

const MenuItem = ({ active, label, icon, onClick }) => {
    return (
        <Wrapper
            aria-current={active ? 'item' : null}
            type="button"
            onClick={onClick}
        >
            <div className="MenuItemIcon">{icon}</div>
            <div className="MenuItemLabel">{label}</div>
        </Wrapper>
    )
}

export default MenuItem
