import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { media } from 'helpers'
import styled from 'styled-components'

import Card from 'js/components/Card/Card'
import Document from 'js/components/Document'
import { Container } from 'js/components/Grid/Grid'
import NotFound from 'js/core/NotFound'

import FilesView from './FilesView'

const Wrapper = styled(Container)`
    z-index: 1;

    ${media.mobileLandscapeDown`
        margin-top: -20px;
        padding: 0;
        display: flex;
        flex-grow: 1;

        .FilesView {
            border-radius: 0;
        }
    `};

    ${media.tabletUp`
        margin-bottom: 40px;
    `};
`

const GroupList = () => {
    const { groupGuid, containerGuid } = useParams()
    const { t } = useTranslation()

    const { data } = useQuery(GET_FILES_CONTAINER, {
        variables: {
            groupGuid,
            containerGuid,
        },
    })

    const [selected, setSelected] = useState([])

    if (containerGuid && data?.containerEntity === null) {
        return <NotFound />
    }

    return (
        <>
            <Document
                title={data?.containerEntity?.title || t('global.files')}
                containerTitle={data?.groupEntity?.name}
            />
            <Wrapper>
                <FilesView
                    as={Card}
                    className="FilesView"
                    rootContainerGuid={groupGuid}
                    groupGuid={groupGuid}
                    selected={selected}
                    setSelected={setSelected}
                    selectable="multiple"
                    canCreateAndEdit
                />
            </Wrapper>
        </>
    )
}

const GET_FILES_CONTAINER = gql`
    query FilesContainer($groupGuid: String!, $containerGuid: String) {
        groupEntity: entity(guid: $groupGuid) {
            guid
            ... on Group {
                name
            }
        }
        containerEntity: entity(guid: $containerGuid) {
            guid
            ... on Folder {
                title
                subtype
                size
            }
        }
    }
`

export default GroupList
