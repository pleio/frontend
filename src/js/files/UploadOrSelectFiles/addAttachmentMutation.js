import { gql } from '@apollo/client'

export default gql`
    mutation AddAttachment($input: addAttachmentInput!) {
        addAttachment(input: $input) {
            attachment {
                id
                name
                mimeType
                url
                downloadUrl
            }
        }
    }
`
