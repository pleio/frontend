import { sortByProperty } from 'helpers'
import { produce } from 'immer'

export default function addAttachments(
    mutate,
    files,
    setFiles,
    resolve,
    reject,
) {
    const insertFiles = []
    return Promise.all(
        files.map((file, i) => {
            if (file.guid) {
                // Existing file
                const { guid, title, subtype, mimeType, url, size, download } =
                    file

                if (files[i]) {
                    setFiles(
                        produce((newState) => {
                            newState[i].state = 'finished'
                        }),
                    )
                }

                return insertFiles.push({
                    index: i,
                    guid,
                    title,
                    subtype,
                    mimeType,
                    url,
                    size,
                    download,
                })
            } else {
                // Uploaded file
                return mutate({
                    variables: {
                        input: {
                            file,
                        },
                    },
                })
                    .then(({ data }) => {
                        const {
                            attachment: {
                                id,
                                name,
                                mimeType,
                                url,
                                downloadUrl,
                            },
                        } = data.addAttachment

                        let doesNotExist = false

                        setFiles(
                            produce((newState) => {
                                if (!newState[i]) {
                                    doesNotExist = true // If canceled during upload
                                    return false
                                }
                                newState[i].state = 'finished'
                            }),
                        )

                        if (doesNotExist) return false

                        insertFiles.push({
                            index: i,
                            guid: id,
                            title: name,
                            mimeType,
                            url,
                            download: downloadUrl,
                            size: file.size,
                        })
                    })
                    .catch((errors) => {
                        console.error(errors)
                    })
            }
        }),
    )
        .then(() => {
            const sortedInsertFiles = insertFiles.sort(sortByProperty('index'))
            resolve(sortedInsertFiles)
        })
        .catch((errors) => {
            console.error(errors)
            reject(errors)
        })
}
