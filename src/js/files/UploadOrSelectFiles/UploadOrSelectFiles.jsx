import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { produce } from 'immer'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import EntityList from 'js/components/EntityList/EntityList'
import EntityListItem from 'js/components/EntityList/EntityListItem'
import Loader from 'js/components/Form/Loader'
import { H4 } from 'js/components/Heading'
import IconButton from 'js/components/IconButton/IconButton'
import Modal from 'js/components/Modal/Modal'
import UploadBox from 'js/components/UploadBox/UploadBox'
import SelectFromFiles from 'js/files/SelectFromFiles/SelectFromFiles'
import { FILE_TYPE } from 'js/lib/constants'

import RemoveIcon from 'icons/cross.svg'

const Wrapper = styled.div`
    display: flex;
    flex-direction: column; // Center "Select from files" button

    .TypeDivider {
        position: relative;
        display: flex;
        justify-content: center;
        margin: 12px 0;

        &:before {
            content: '';
            position: absolute;
            top: 50%;
            left: 0;
            width: 100%;
            height: 1px;
            background-color: ${(p) => p.theme.color.grey[30]};
        }

        .TypeDividerText {
            position: relative;
            padding: 0 4px;
            background-color: white;
        }
    }

    .SelectedFiles {
        margin-top: 8px;
    }

    .SelectedFileLoader {
        flex-shrink: 0;
        position: relative;
        width: 32px;
        height: 32px;
    }
`

const UploadOrSelectFiles = ({
    multiple,
    accept = 'file',
    maxSize,
    uploadHelper,
    files,
    setFiles,
    isLoading,
}) => {
    const [showSelectFromFiles, setShowSelectFromFiles] = useState(false)

    const handleAddFiles = (value) => {
        if (multiple) {
            setFiles([...files, ...value])
        } else {
            setFiles(value)
        }
    }

    const { t } = useTranslation()

    const fileType = FILE_TYPE[accept]

    return (
        <>
            <Wrapper>
                <UploadBox
                    name="upload-file"
                    accept={fileType.accept}
                    multiple={multiple}
                    maxSize={maxSize || fileType.maxSize}
                    onSubmit={(file) =>
                        handleAddFiles(multiple ? file : [file])
                    }
                    aria-describedby="upload-file-helper"
                    helper={uploadHelper}
                />

                <div className="TypeDivider">
                    <div className="TypeDividerText">{t('global.or')}</div>
                </div>

                <Button
                    size="normal"
                    variant="secondary"
                    style={{ alignSelf: 'center' }}
                    onClick={() => setShowSelectFromFiles(true)}
                >
                    {t('action.select-from-files')}..
                </Button>

                {files.length > 0 && (
                    <>
                        <H4>
                            {t('entity-file.count-files', {
                                count: files.length,
                            })}
                        </H4>
                        <EntityList
                            as="ul"
                            className="SelectedFiles"
                            $showBorder
                        >
                            {files.map(
                                (
                                    {
                                        mimeType,
                                        type,
                                        subtype,
                                        title,
                                        name,
                                        hasChildren,
                                        size,
                                        state,
                                    },
                                    i,
                                ) => {
                                    // Combine differences between uploaded file and existing entity
                                    const entityObject = {
                                        mimeType: mimeType || type,
                                        subtype,
                                        name: title || name,
                                        hasChildren,
                                        size,
                                        state,
                                    }

                                    const removeSelectedFile = () => {
                                        setFiles(
                                            produce((newState) => {
                                                newState.splice(i, 1)
                                            }),
                                        )
                                    }

                                    return (
                                        <EntityListItem
                                            as="li"
                                            key={`selected-file-${i}`}
                                            entity={entityObject}
                                            type="file"
                                        >
                                            {isLoading ? (
                                                <div className="SelectedFileLoader">
                                                    <Loader
                                                        position="center"
                                                        isLoading={
                                                            state !== 'finished'
                                                        }
                                                        isSaved={
                                                            state === 'finished'
                                                        }
                                                    />
                                                </div>
                                            ) : (
                                                <IconButton
                                                    size="normal"
                                                    variant="secondary"
                                                    radiusStyle="rounded"
                                                    tooltip={t('action.remove')}
                                                    placement="right"
                                                    onClick={removeSelectedFile}
                                                >
                                                    <RemoveIcon />
                                                </IconButton>
                                            )}
                                        </EntityListItem>
                                    )
                                },
                            )}
                        </EntityList>
                    </>
                )}
            </Wrapper>
            <Modal
                isVisible={showSelectFromFiles}
                onClose={() => setShowSelectFromFiles(false)}
                size="large"
                noPadding
                containHeight
                growHeight
            >
                <SelectFromFiles
                    selectableType={accept === 'image' ? 'image' : 'all'}
                    multiple={multiple}
                    onAddFiles={(value) => {
                        handleAddFiles(value)
                        setShowSelectFromFiles(false)
                    }}
                    onClose={() => setShowSelectFromFiles(false)}
                    title={t('action.select-from-files')}
                />
            </Modal>
        </>
    )
}

UploadOrSelectFiles.propTypes = {
    accept: PropTypes.oneOf(['file', 'image']),
    files: PropTypes.array.isRequired,
    isLoading: PropTypes.bool,
    maxSize: PropTypes.number,
    multiple: PropTypes.bool,
    setFiles: PropTypes.func.isRequired,
}

export default UploadOrSelectFiles
