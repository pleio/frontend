import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useMutation } from '@apollo/client'
import PropTypes from 'prop-types'

import Button from 'js/components/Button/Button'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import Modal from 'js/components/Modal/Modal'

import addAttachmentMutation from './addAttachmentMutation'
import addAttachments from './addAttachments'
import UploadOrSelectFiles from './UploadOrSelectFiles'

const UploadOrSelectFilesModal = ({
    title,
    multiple,
    accept = 'file',
    maxSize,
    showModal,
    setShowModal,
    onComplete,
}) => {
    const [files, setFiles] = useState([])
    const [showError, setShowError] = useState('')

    const [mutate] = useMutation(addAttachmentMutation)

    const closeModal = () => {
        reset()
        setShowError('')
        setFiles([])
        setShowModal(false)
    }

    const handleComplete = (files) => {
        closeModal()
        onComplete(multiple ? files : files[0])
    }

    const submit = async () => {
        await addAttachments(
            mutate,
            files,
            setFiles,
            handleComplete,
            setShowError,
        )
    }

    const {
        reset,
        handleSubmit,
        formState: { isSubmitting },
    } = useForm()

    const { t } = useTranslation()

    return (
        <>
            <Modal
                isVisible={showModal}
                onClose={closeModal}
                title={title}
                size="small"
                containHeight
            >
                <UploadOrSelectFiles
                    multiple={multiple}
                    accept={accept}
                    maxSize={maxSize}
                    files={files}
                    setFiles={setFiles}
                    isLoading={isSubmitting}
                />

                <Errors errors={showError} />

                <Flexer mt>
                    <Button
                        size="normal"
                        variant="tertiary"
                        onClick={closeModal}
                    >
                        {t('action.cancel')}
                    </Button>
                    <Button
                        size="normal"
                        variant="primary"
                        disabled={files.length === 0}
                        loading={isSubmitting}
                        onClick={handleSubmit(submit)}
                    >
                        {t('action.insert')}
                    </Button>
                </Flexer>
            </Modal>
        </>
    )
}

UploadOrSelectFilesModal.propTypes = {
    title: PropTypes.string.isRequired,
    multiple: PropTypes.bool,
    accept: PropTypes.string,
    maxSize: PropTypes.number,
    showModal: PropTypes.bool.isRequired,
    setShowModal: PropTypes.func.isRequired,
    onComplete: PropTypes.func.isRequired,
}

UploadOrSelectFilesModal.propTypes = {
    accept: PropTypes.oneOf(['file', 'image']),
}

export default UploadOrSelectFilesModal
