import { gql } from '@apollo/client'

const GET_FILES_LIST = gql`
    query FilesList(
        $rootContainerGuid: String
        $containerGuid: String!
        $filterBookmarks: Boolean
        $offset: Int = 0
        $limit: Int = 50
        $orderBy: String
        $orderDirection: String
    ) {
        site {
            guid
            collabEditingEnabled
            readAccessIds {
                id
                description
            }
            showOriginalFileName
            showTagsInDetail
            showCustomTagsInDetail
        }
        viewer {
            guid
            loggedIn
            isAdmin
            canWriteToContainer(containerGuid: $containerGuid, subtype: "file")
            user {
                guid
                name
                icon
                url
            }
            canUpdateAccessLevel
        }
        containerEntity: entity(guid: $containerGuid) {
            guid
            ... on Folder {
                title
                canEdit
                subtype
                size
                owner {
                    guid
                    name
                }
                hasChildren
                timeUpdated
                timePublished
                accessId
                writeAccessId
                tags
                tagCategories {
                    name
                    values
                }
            }
        }
        rootEntity: entity(guid: $rootContainerGuid) {
            guid
            ... on User {
                canEdit
            }
            ... on Group {
                canEdit
                readAccessIds {
                    id
                    description
                }
                defaultTags
                defaultTagCategories {
                    name
                    values
                }
            }
        }
        files(
            containerGuid: $containerGuid
            offset: $offset
            limit: $limit
            orderBy: $orderBy
            orderDirection: $orderDirection
            filterBookmarks: $filterBookmarks
        ) {
            total
            edges {
                guid
                ... on File {
                    subtype
                    title
                    description
                    richDescription
                    localRichDescription
                    inputLanguage
                    isTranslationEnabled
                    url
                    size
                    isBookmarked
                    download
                    timeUpdated
                    timePublished
                    mimeType
                    canEdit
                    canArchiveAndDelete
                    thumbnail
                    accessId
                    writeAccessId
                    tags
                    tagCategories {
                        name
                        values
                    }
                    owner {
                        guid
                        name
                    }
                    referenceCount
                    references {
                        guid
                        label
                        url
                        referenceType
                        entityType
                    }
                    parentFolder {
                        guid
                    }
                    originalFileName
                }
                ... on Folder {
                    subtype
                    title
                    description
                    richDescription
                    localRichDescription
                    inputLanguage
                    isTranslationEnabled
                    url
                    hasChildren
                    isBookmarked
                    timeUpdated
                    timePublished
                    mimeType
                    canEdit
                    accessId
                    writeAccessId
                    tags
                    tagCategories {
                        name
                        values
                    }
                    owner {
                        guid
                        name
                    }
                    referenceCount
                    references {
                        guid
                        label
                        url
                        referenceType
                        entityType
                    }
                    parentFolder {
                        guid
                    }
                }
                ... on Pad {
                    subtype
                    title
                    richDescription
                    url
                    isBookmarked
                    timeUpdated
                    timePublished
                    canEdit
                    accessId
                    writeAccessId
                    owner {
                        guid
                        name
                    }
                    tags
                    tagCategories {
                        name
                        values
                    }
                    group {
                        guid
                        url
                    }
                    referenceCount
                    references {
                        guid
                        label
                        url
                        referenceType
                        entityType
                    }
                    parentFolder {
                        guid
                    }
                }
            }
        }
    }
`

export default GET_FILES_LIST
