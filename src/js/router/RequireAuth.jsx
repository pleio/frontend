import React from 'react'
import { gql, useQuery } from '@apollo/client'
import PropTypes from 'prop-types'

import NotFound from 'js/core/NotFound'

const RequireAuth = ({
    viewer,
    site,
    siteSettings,
    canWriteToContainerSubtype,
    children,
}) => {
    const { loading, data, error } = useQuery(GET_AUTH, {
        variables: {
            canWriteToContainerSubtype,
        },
    })

    if (loading) return null
    if (error) {
        console.error(error)
        return <NotFound />
    }

    if (site && data.site && !data.site[site]) {
        return <NotFound />
    }
    if (siteSettings && data.siteSettings && !data.siteSettings[siteSettings]) {
        return <NotFound />
    }
    if (viewer && data.viewer && !data.viewer[viewer]) {
        return <NotFound />
    }
    return children
}

const GET_AUTH = gql`
    query GetAuth($canWriteToContainerSubtype: String) {
        site {
            guid
            chatEnabled
        }
        siteSettings {
            contentModerationEnabled
            commentModerationEnabled
        }
        viewer {
            guid
            loggedIn
            isAdmin
            canAccessAdmin(permission: accessAdmin)
            hasFullControl: canAccessAdmin(permission: fullControl)
            hasUserManagement: canAccessAdmin(permission: userManagement)
            publicationRequestManagement: canAccessAdmin(
                permission: publicationRequestManagement
            )
            contentManagement: canAccessAdmin(permission: contentManagement)
            canWriteToContainer(subtype: $canWriteToContainerSubtype)
        }
    }
`

RequireAuth.propTypes = {
    site: PropTypes.string,
    siteSettings: PropTypes.string,
    viewer: PropTypes.string,
    canWriteToContainerSubtype: PropTypes.string,
}

export default RequireAuth
