import React, { useEffect, useRef } from 'react'
import { useLocation, useNavigationType } from 'react-router-dom'

export default function ScrollToTop() {
    const { pathname } = useLocation()

    const ref = useRef(null)

    const navigationType = useNavigationType()

    useEffect(() => {
        // do not scroll to top when browser's backbutton is clicked
        if (navigationType !== 'POP') {
            ref.current?.focus()
            window.scrollTo(0, 0)
        }
    }, [pathname])

    return <div ref={ref} tabIndex={-1} style={{ outline: 'none' }} />
}
