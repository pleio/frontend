import React, { Suspense } from 'react'
import {
    BrowserRouter,
    Navigate,
    Outlet,
    Route,
    Routes,
} from 'react-router-dom'

import NavigationFooter from 'js/admin/Navigation/components/NavigationFooter'
import BlogAdd from 'js/blog/Add'
import BlogEdit from 'js/blog/Edit'
import BlogRoutes from 'js/blog/Routes'
import VersionHistory from 'js/components/VersionHistory/VersionHistory'
import CoreRoutes from 'js/core/Routes'
import DiscussionsAdd from 'js/discussions/Add'
import DiscussionsEdit from 'js/discussions/Edit'
import DiscussionsRoutes from 'js/discussions/Routes'
import EventsAdd from 'js/events/Add'
import EventsEdit from 'js/events/Edit'
import EventGuestList from 'js/events/GuestList/GuestList'
import EventsRoutes from 'js/events/Routes'
import Files from 'js/files/Files'
import FileView from 'js/files/FileView/FileView'
import GroupAdd from 'js/group/Add'
import GroupEdit from 'js/group/Edit'
import Layout from 'js/layout/Layout'
import { useSiteStore } from 'js/lib/stores'
import MagazineAdd from 'js/magazine/AddEdit/Add'
import MagazineEdit from 'js/magazine/AddEdit/Edit'
import { MagazineRoutes, MagazinesRoutes } from 'js/magazine/Routes'
import MagazineIssueRoutes from 'js/magazine-issue/Routes'
import NewsAdd from 'js/news/Add'
import NewsEdit from 'js/news/Edit'
import NewsRoutes from 'js/news/Routes'
import PageAdd from 'js/page/Add'
import LegacyCmsPageRedirect from 'js/page/LegacyCmsPageRedirect'
import { PageRoutes } from 'js/page/Routes'
import { EpisodeAdd } from 'js/podcast/EpisodeAdd'
import { EpisodeEdit } from 'js/podcast/EpisodeEdit'
import { PodcastAdd } from 'js/podcast/PodcastAdd'
import { PodcastEdit } from 'js/podcast/PodcastEdit'
import PodcastRoutes from 'js/podcast/Routes'
import PollAdd from 'js/polls/Add'
import PollEdit from 'js/polls/Edit'
import PollsRoutes from 'js/polls/Routes'
import QuestionsAdd from 'js/questions/Add'
import QuestionsEdit from 'js/questions/Edit'
import QuestionsRoutes from 'js/questions/Routes'
import RequireAuth from 'js/router/RequireAuth'
import ScrollToTop from 'js/router/ScrollToTop'
import SearchRoutes from 'js/search/Routes'
import TaskAdd from 'js/tasks/Add'
import TaskEdit from 'js/tasks/Edit'
import ThemeProvider from 'js/theme/ThemeProvider'
import UsersRoutes from 'js/users/Routes'
import WikiAdd from 'js/wiki/Add'
import WikiEdit from 'js/wiki/Edit'
import WikiRoutes from 'js/wiki/Routes'

// Lazy load several routes to reduce initial bundle size
const Chat = React.lazy(
    () => import(/* webpackChunkName: "chat" */ 'js/chat/Chat'),
)

const PadEdit = React.lazy(
    () => import(/* webpackChunkName: "pad-edit" */ 'js/pad/Edit'),
)

const PageEdit = React.lazy(
    () => import(/* webpackChunkName: "page-edit" */ 'js/page/Edit'),
)

const EventCall = React.lazy(
    () => import(/* webpackChunkName: "event-call" */ 'js/videoCall/EventCall'),
)
const ChatCall = React.lazy(
    () => import(/* webpackChunkName: "chat-call" */ 'js/videoCall/ChatCall'),
)

const AdminRoutes = React.lazy(
    () => import(/* webpackChunkName: "admin-routes" */ 'js/admin/Routes'),
)

const GroupRoutes = React.lazy(
    () => import(/* webpackChunkName: "group-routes" */ 'js/group/Routes'),
)

const UserRoutes = React.lazy(
    () => import(/* webpackChunkName: "user-routes" */ 'js/user/Routes'),
)

const Router = () => {
    const { site } = useSiteStore()
    return (
        <BrowserRouter>
            <ScrollToTop />
            <Suspense>
                <Routes>
                    <Route
                        element={
                            <>
                                <ThemeProvider
                                    isRoot
                                    theme="site"
                                    bodyColor="#F5F5F5"
                                    siteStyle={site?.style}
                                >
                                    <Outlet />
                                </ThemeProvider>
                            </>
                        }
                    >
                        <Route
                            path="/admin/*"
                            element={
                                <Suspense
                                    fallback={<div>Loading Admin...</div>}
                                >
                                    <AdminRoutes />
                                </Suspense>
                            }
                        />

                        <Route
                            path="/videocall/:guid"
                            element={<EventCall />}
                        />
                        <Route
                            path="/videocall/chat/:guid"
                            element={<ChatCall />}
                        />

                        <Route
                            path="/chat"
                            element={
                                <RequireAuth
                                    viewer="loggedIn"
                                    site="chatEnabled"
                                >
                                    <Chat />
                                </RequireAuth>
                            }
                        />
                        <Route
                            path="/chat/:guid"
                            element={
                                <RequireAuth
                                    viewer="loggedIn"
                                    site="chatEnabled"
                                >
                                    <Chat />
                                </RequireAuth>
                            }
                        />

                        <Route
                            path="/:guid/version-history"
                            element={<VersionHistory />}
                        />
                        <Route
                            path="/:guid/version-history/:revisionGuid"
                            element={<VersionHistory />}
                        />

                        <Route
                            path="/news/add"
                            element={
                                <RequireAuth
                                    viewer="canWriteToContainer"
                                    canWriteToContainerSubtype="news"
                                >
                                    <NewsAdd />
                                </RequireAuth>
                            }
                        />
                        <Route path="/news/edit/:guid" element={<NewsEdit />} />

                        <Route
                            path="/blog/add"
                            element={
                                <RequireAuth
                                    viewer="canWriteToContainer"
                                    canWriteToContainerSubtype="blog"
                                >
                                    <BlogAdd />
                                </RequireAuth>
                            }
                        />
                        <Route path="/blog/edit/:guid" element={<BlogEdit />} />

                        <Route
                            path="/podcasts/add"
                            element={
                                <RequireAuth
                                    viewer="canWriteToContainer"
                                    canWriteToContainerSubtype="podcast"
                                >
                                    <PodcastAdd />
                                </RequireAuth>
                            }
                        />
                        <Route
                            path="/podcasts/edit/:guid"
                            element={<PodcastEdit />}
                        />
                        <Route
                            path="/podcasts/:podcastGuid/episodes/add"
                            element={<EpisodeAdd />}
                        />
                        <Route
                            path="/podcasts/episodes/edit/:episodeGuid"
                            element={<EpisodeEdit />}
                        />

                        <Route
                            path="/questions/add"
                            element={
                                <RequireAuth
                                    viewer="canWriteToContainer"
                                    canWriteToContainerSubtype="question"
                                >
                                    <QuestionsAdd />
                                </RequireAuth>
                            }
                        />
                        <Route
                            path="/questions/edit/:guid"
                            element={<QuestionsEdit />}
                        />

                        <Route
                            path="/discussion/add"
                            element={
                                <RequireAuth
                                    viewer="canWriteToContainer"
                                    canWriteToContainerSubtype="discussion"
                                >
                                    <DiscussionsAdd />
                                </RequireAuth>
                            }
                        />
                        <Route
                            path="/discussion/edit/:guid"
                            element={<DiscussionsEdit />}
                        />

                        <Route
                            path="/events/add"
                            element={
                                <RequireAuth
                                    viewer="canWriteToContainer"
                                    canWriteToContainerSubtype="event"
                                >
                                    <EventsAdd />
                                </RequireAuth>
                            }
                        />
                        <Route
                            path="/events/add/:containerGuid"
                            element={<EventsAdd />}
                        />
                        <Route
                            path="/events/edit/:guid"
                            element={<EventsEdit />}
                        />

                        <Route
                            path="/events/view/:guid/:slug/guest-list"
                            element={<EventGuestList />}
                        />
                        <Route
                            path="/groups/view/:groupGuid/:groupSlug/events/view/:guid/:slug/guest-list"
                            element={<EventGuestList />}
                        />

                        <Route
                            path="/events/edit/:guid"
                            element={<EventsEdit />}
                        />

                        <Route
                            path="/magazines/add"
                            element={
                                <RequireAuth
                                    viewer="canWriteToContainer"
                                    canWriteToContainerSubtype="magazine"
                                >
                                    <MagazineAdd />
                                </RequireAuth>
                            }
                        />
                        <Route
                            path="/magazine/edit/:guid"
                            element={<MagazineEdit />}
                        />

                        <Route
                            path="/magazine-issue/*"
                            element={<MagazineIssueRoutes />}
                        />

                        <Route path="/pages/add" element={<PageAdd />} />
                        <Route path="/page/edit/:guid" element={<PageEdit />} />
                        <Route
                            path="/page/edit/:guid/:slug"
                            element={<PageEdit />}
                        />
                        <Route
                            path="/admin/site/navigation/footer"
                            element={<NavigationFooter />}
                        />

                        <Route
                            path="/wiki/add"
                            element={
                                <RequireAuth
                                    viewer="canWriteToContainer"
                                    canWriteToContainerSubtype="wiki"
                                >
                                    <WikiAdd />
                                </RequireAuth>
                            }
                        />
                        <Route
                            path="/wiki/add/:containerGuid"
                            element={<WikiAdd />}
                        />
                        <Route path="/wiki/edit/:guid" element={<WikiEdit />} />

                        <Route path="/tasks/add" element={<TaskAdd />} />
                        <Route
                            path="/tasks/edit/:guid"
                            element={<TaskEdit />}
                        />

                        <Route
                            path="/polls/add"
                            element={
                                <RequireAuth
                                    viewer="canWriteToContainer"
                                    canWriteToContainerSubtype="poll"
                                >
                                    <PollAdd />
                                </RequireAuth>
                            }
                        />
                        <Route
                            path="/polls/edit/:guid"
                            element={<PollEdit />}
                        />

                        <Route
                            path="/files/view/:guid"
                            element={<FileView />}
                        />
                        <Route
                            path="/files/view/:guid/:slug"
                            element={<FileView />}
                        />

                        <Route path="/files/edit/:guid" element={<PadEdit />} />

                        <Route
                            path="/groups/add"
                            element={
                                <RequireAuth
                                    viewer="canWriteToContainer"
                                    canWriteToContainerSubtype="group"
                                >
                                    <GroupAdd />
                                </RequireAuth>
                            }
                        />
                        <Route
                            path="/groups/edit/:guid"
                            element={<GroupEdit />}
                        />
                        <Route
                            path="/groups/view/:groupGuid/:groupSlug/blog/add"
                            element={<BlogAdd />}
                        />
                        <Route
                            path="/groups/view/:groupGuid/:groupSlug/blog/edit/:guid"
                            element={<BlogEdit />}
                        />

                        {/* Group Podcasts */}
                        <Route
                            path="/groups/view/:groupGuid/:groupSlug/podcasts/add"
                            element={<PodcastAdd />}
                        />

                        <Route
                            path="/groups/view/:groupGuid/:groupSlug/podcasts/edit/:guid"
                            element={<PodcastEdit />}
                        />

                        <Route
                            path="/groups/view/:groupGuid/:groupSlug/podcasts/:podcastGuid/episodes/add"
                            element={<EpisodeAdd />}
                        />

                        <Route
                            path="/groups/view/:groupGuid/:groupSlug/podcasts/episodes/edit/:episodeGuid"
                            element={<EpisodeEdit />}
                        />
                        {/* End Group Podcasts */}

                        <Route
                            path="/groups/view/:groupGuid/:groupSlug/news/add"
                            element={<NewsAdd />}
                        />
                        <Route
                            path="/groups/view/:groupGuid/:groupSlug/news/edit/:guid"
                            element={<NewsEdit />}
                        />

                        <Route
                            path="/groups/view/:groupGuid/:groupSlug/questions/add"
                            element={<QuestionsAdd />}
                        />
                        <Route
                            path="/groups/view/:groupGuid/:groupSlug/questions/edit/:guid"
                            element={<QuestionsEdit />}
                        />
                        <Route
                            path="/groups/view/:groupGuid/:groupSlug/discussion/add"
                            element={<DiscussionsAdd />}
                        />
                        <Route
                            path="/groups/view/:groupGuid/:groupSlug/discussion/edit/:guid"
                            element={<DiscussionsEdit />}
                        />
                        <Route
                            path="/groups/view/:groupGuid/:groupSlug/events/add"
                            element={<EventsAdd />}
                        />
                        <Route
                            path="/groups/view/:groupGuid/:groupSlug/events/add/:containerGuid"
                            element={<EventsAdd />}
                        />
                        <Route
                            path="/groups/view/:groupGuid/:groupSlug/events/edit/:guid"
                            element={<EventsEdit />}
                        />
                        <Route
                            path="/groups/view/:groupGuid/:groupSlug/pad/edit/:guid"
                            element={<PadEdit />}
                        />
                        <Route
                            path="/groups/view/:groupGuid/:groupSlug/wiki/add"
                            element={<WikiAdd />}
                        />
                        <Route
                            path="/groups/view/:groupGuid/:groupSlug/wiki/add/:containerGuid"
                            element={<WikiAdd />}
                        />
                        <Route
                            path="/groups/view/:groupGuid/:groupSlug/wiki/edit/:guid"
                            element={<WikiEdit />}
                        />
                        <Route
                            path="/groups/view/:groupGuid/:groupSlug/tasks/add"
                            element={<TaskAdd />}
                        />
                        <Route
                            path="/groups/view/:groupGuid/:groupSlug/tasks/edit/:guid"
                            element={<TaskEdit />}
                        />
                        <Route
                            path="/groups/view/:groupGuid/:groupSlug/pages/add"
                            element={<PageAdd />}
                        />
                        <Route
                            path="/groups/view/:groupGuid/:groupSlug/page/edit/:guid"
                            element={<PageEdit />}
                        />
                        <Route
                            path="/groups/view/:groupGuid/:groupSlug/page/edit/:guid/:slug"
                            element={<PageEdit />}
                        />

                        <Route element={<Layout />}>
                            <Route path="/news/*" element={<NewsRoutes />} />
                            <Route path="/blog/*" element={<BlogRoutes />} />
                            <Route
                                path="/podcasts/*"
                                element={<PodcastRoutes />}
                            />
                            <Route
                                path="/questions/*"
                                element={<QuestionsRoutes />}
                            />
                            <Route
                                path="/discussion/*"
                                element={<DiscussionsRoutes />}
                            />
                            <Route
                                path="/events/*"
                                element={<EventsRoutes />}
                            />
                            <Route
                                path="/magazines/*"
                                element={<MagazinesRoutes />}
                            />
                            <Route
                                path="/magazine/*"
                                element={<MagazineRoutes />}
                            />
                            <Route path="/groups/*" element={<GroupRoutes />} />
                            <Route
                                path="/cms/*"
                                element={<LegacyCmsPageRedirect />}
                            />
                            <Route path="/page/*" element={<PageRoutes />} />
                            <Route path="/wiki/*" element={<WikiRoutes />} />
                            <Route path="/polls/*" element={<PollsRoutes />} />
                            <Route
                                path="/user/:userGuid/*"
                                element={<UserRoutes />}
                            />
                            <Route
                                path="/search/*"
                                element={<SearchRoutes />}
                            />
                            <Route
                                path="/members"
                                element={<Navigate to="/users" replace />}
                            />
                            <Route path="/users/*" element={<UsersRoutes />} />
                            <Route
                                path="/files/:containerGuid"
                                element={<Files />}
                            />
                            <Route path="/*" element={<CoreRoutes />} />
                        </Route>
                    </Route>
                </Routes>
            </Suspense>
        </BrowserRouter>
    )
}

export default Router
