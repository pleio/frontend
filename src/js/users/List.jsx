import React from 'react'
import { useTranslation } from 'react-i18next'

import Document from 'js/components/Document'
import Section from 'js/components/Section/Section'
import UserList from 'js/components/UserList/UserList'

const List = () => {
    const { t } = useTranslation()

    return (
        <>
            <Document title={t('user.title')} />
            <Section backgroundColor="white" grow>
                <UserList />
            </Section>
        </>
    )
}

export default List
