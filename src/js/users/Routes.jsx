import React from 'react'
import { Route, Routes } from 'react-router-dom'

import NotFound from 'js/core/NotFound'
import RequireAuth from 'js/router/RequireAuth'

import List from './List'

const Component = () => (
    <Routes>
        <Route
            path="/"
            element={
                <RequireAuth viewer="loggedIn">
                    <List />
                </RequireAuth>
            }
        />
        <Route element={<NotFound />} />
    </Routes>
)

export default Component
