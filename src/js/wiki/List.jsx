import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { useWindowScrollPosition } from 'helpers'

import { Container } from 'js/components/Grid/Grid'
import PageHeader from 'js/components/PageHeader/PageHeader'
import Section from 'js/components/Section/Section'

import Card from './components/Card'
import WikiList from './containers/WikiList'

const List = ({ data }) => {
    const { viewer } = data
    const { t } = useTranslation()

    useWindowScrollPosition('wiki')

    return (
        <>
            <PageHeader
                title={t('entity-wiki.title-list')}
                canCreate={viewer?.canWriteToContainer}
                createLink="/wiki/add"
                createLabel={t('entity-wiki.create')}
            />
            <Section>
                <Container size="tiny">
                    <WikiList
                        type="object"
                        subtype="wiki"
                        containerGuid="1"
                        childComponent={Card}
                        offset={0}
                        limit={20}
                    />
                </Container>
            </Section>
        </>
    )
}

const Query = gql`
    query WikiWrapper {
        viewer {
            guid
            loggedIn
            canWriteToContainer(subtype: "wiki")
        }
    }
`

export default graphql(Query)(List)
