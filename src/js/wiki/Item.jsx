import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { getGroupUrl, media } from 'helpers'
import { showDateTime, timeSince } from 'helpers/date/showDate'
import styled from 'styled-components'

import CoverImage from 'js/components/CoverImage'
import Document from 'js/components/Document'
import { Container } from 'js/components/Grid/Grid'
import { H1 } from 'js/components/Heading'
import HideVisually from 'js/components/HideVisually/HideVisually'
import ToggleTranslation from 'js/components/Item/components/ToggleTranslation'
import ItemActions from 'js/components/Item/ItemActions/ItemActions'
import Author from 'js/components/Item/ItemHeader/components/Author'
import ItemStatusTag from 'js/components/Item/ItemStatusTag'
import ItemSuggestedItems from 'js/components/Item/ItemSuggestedItems'
import ItemTags from 'js/components/ItemTags'
import Text from 'js/components/Text/Text'
import TiptapView from 'js/components/Tiptap/TiptapView'
import Tooltip from 'js/components/Tooltip/Tooltip'
import NotFound from 'js/core/NotFound'
import withGlobalState from 'js/lib/withGlobalState'
import ThemeProvider from 'js/theme/ThemeProvider'
import { wikiViewFragment } from 'js/wiki/fragments/entity'

import WikiNav from './components/WikiNav'
import getRefetchQueries from './helpers/getRefetchQueries'

const Wrapper = styled.div`
    display: flex;

    .WikiContainer {
        display: flex;
    }

    .ItemCoverContainer {
        ${media.tabletDown`
            padding: 0;
        `}
    }

    .WikiNavigation {
        z-index: 1; // Position shadow on top of .WikiArticle
    }

    .WikiArticle {
        flex-grow: 1;
        padding-bottom: 32px;
    }

    ${media.tabletDown`
        .WikiCoverContainer {
            padding: 0;
        }
    `}

    ${media.mobilePortrait`
        .WikiContainer {
            flex-direction: column;
        }

        .WikiNavigation {
            padding: 20px 0;
            box-shadow: ${(p) => p.theme.shadow.hard.bottom};
        }
    `};

    ${media.mobileLandscapeUp`
        .WikiNavigation {
            width: 260px;
            padding: 32px 0 20px;
            box-shadow: ${(p) => p.theme.shadow.hard.right};
        }
    `};
`

const Item = ({ globalState }) => {
    const params = useParams()
    const navigate = useNavigate()
    const { t } = useTranslation()

    const { data, loading } = useQuery(QUERY, {
        variables: {
            guid: params.guid,
        },
        fetchPolicy: 'cache-and-network',
    })

    const [isTranslated, setIsTranslated] = useState(null)
    useEffect(() => {
        if (!loading && data?.entity) {
            setIsTranslated(!!data.entity.localTitle)
        }
    }, [loading, data])

    if (loading) return null

    const { site, entity, viewer } = data

    if (!entity) return <NotFound />

    const refetchQueries = getRefetchQueries(true, !!entity.group)

    const {
        canEdit,
        localTitle,
        title,
        url,
        localRichDescription,
        richDescription,
        timeUpdated,
        featured,
        group,
        inGroup,
        suggestedItems,
        hasChildren,
        parent,
        tags,
        tagCategories,
        owner,
        showOwner,
        isTranslationEnabled,
    } = entity

    const hasTranslations = !!localTitle && isTranslationEnabled

    const translatedTitle = isTranslated ? localTitle : title
    const translatedRichDescription = isTranslated
        ? localRichDescription
        : richDescription

    // redirect if there is no groupGuid but entity is inGroup
    if (!params.groupGuid && inGroup) {
        navigate(url, { replace: true })
        return null
    }

    const editMode = globalState.editMode || inGroup

    const onEdit = `${getGroupUrl(params)}/wiki/edit/${entity.guid}`
    const onAfterDelete = parent?.url || `${getGroupUrl(params)}/wiki`

    const hasSuggestedItems = suggestedItems?.length > 0

    const showNav =
        (editMode && viewer.canWriteToContainer) || hasChildren || parent?.guid

    const showAuthor = showOwner && owner

    return (
        <ThemeProvider bodyColor="#FFFFFF">
            <Document title={translatedTitle} containerTitle={group?.name} />

            <Wrapper size={showNav ? 'large' : 'normal'}>
                <Container $noPadding className="WikiContainer">
                    {showNav && (
                        <WikiNav
                            viewer={viewer}
                            editMode={editMode}
                            className="WikiNavigation"
                        />
                    )}

                    <article className="WikiArticle">
                        <Container size="normal" className="WikiCoverContainer">
                            <CoverImage featured={featured} />
                        </Container>

                        <Container size="normal">
                            {showAuthor && (
                                <Author
                                    entity={entity}
                                    style={{ marginBottom: '16px' }}
                                />
                            )}

                            <div
                                style={{
                                    display: 'flex',
                                    alignItems: 'flex-start',
                                    marginTop: '32px',
                                }}
                            >
                                <H1>
                                    {translatedTitle}
                                    <ItemStatusTag
                                        entity={entity}
                                        style={{ marginBottom: '4px' }}
                                    />
                                </H1>

                                <ItemActions
                                    canDuplicate
                                    showEditButton
                                    entity={entity}
                                    onEdit={onEdit}
                                    moveEntity={parent && entity}
                                    onAfterDelete={onAfterDelete}
                                    refetchQueries={refetchQueries}
                                    style={{ marginLeft: 'auto' }}
                                />
                            </div>

                            <Text size="small" variant="grey">
                                {t('global.last-edited')}:{' '}
                                <Tooltip content={showDateTime(timeUpdated)}>
                                    <time
                                        dateTime={timeUpdated}
                                        aria-hidden={true}
                                    >
                                        {timeSince(timeUpdated)}
                                    </time>
                                </Tooltip>
                                <HideVisually>
                                    {showDateTime(timeUpdated)}
                                </HideVisually>
                            </Text>

                            <TiptapView
                                content={translatedRichDescription}
                                style={{ marginTop: '24px' }}
                            />

                            {hasTranslations && (
                                <ToggleTranslation
                                    isTranslated={isTranslated}
                                    setIsTranslated={setIsTranslated}
                                    style={{ marginTop: '12px' }}
                                />
                            )}

                            <ItemTags
                                showCustomTags={site.showCustomTagsInDetail}
                                showTags={site.showTagsInDetail}
                                customTags={tags}
                                tagCategories={tagCategories}
                                style={{ marginTop: '24px' }}
                            />
                        </Container>
                    </article>

                    {hasSuggestedItems && (
                        <ItemSuggestedItems
                            entity={entity}
                            style={{ marginTop: '32px' }}
                        />
                    )}
                </Container>
            </Wrapper>
        </ThemeProvider>
    )
}

const QUERY = gql`
    query WikiItem($guid: String!) {
        site {
            guid
            showTagsInDetail
            showCustomTagsInDetail
        }
        viewer {
            guid
            loggedIn
            user {
                guid
                name
                icon
                url
            }
            canWriteToContainer(
                containerGuid: $guid
                subtype: "wiki"
            )
            isSubEditor
        }
        entity(guid: $guid) {
            guid
            ${wikiViewFragment}
        }
    }
`
const MutationArchived = gql`
    mutation toggleEntityArchived($guid: String!) {
        toggleEntityArchived(guid: $guid) {
            success
        }
    }
`

export default withGlobalState(graphql(MutationArchived)(Item))
