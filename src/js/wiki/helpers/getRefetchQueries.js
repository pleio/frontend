/**
 * Load the correct query names, depending on if we need
 * to update the detail view and/or group lists.
 *
 * @param {boolean} isDetail
 * @param {boolean} isInGoup
 */
export default (isDetail, isInGoup) => {
    const refetchQueries = ['WikiItem', 'ActivityList', 'WikiList']
    if (isInGoup) {
        refetchQueries.push('GroupWiki')
    }
    return refetchQueries
}
