import React from 'react'
import { gql } from '@apollo/client'

import FetchMoreWrapper from 'js/components/FetchMoreWrapper'
import wikiListFragment from 'js/wiki/fragments/listFragment'

const GET_WIKI_LIST = gql`
    query WikiList(
        $subtype: String!
        $containerGuid: String
        $offset: Int!
        $limit: Int!
    ) {
        entities(
            subtype: $subtype
            containerGuid: $containerGuid
            offset: $offset
            limit: $limit
        ) {
            total
            edges {
                guid
                ...WikiListFragment
            }
        }
    }
    ${wikiListFragment}
`

const WikiList = (props) => {
    return <FetchMoreWrapper query={GET_WIKI_LIST} {...props} />
}

export default WikiList
