import {
    baseEntityFragment,
    entityEditFragment,
} from 'js/components/EntityActions/fragments/entity'
import { entityViewGroupFragment } from 'js/components/EntityActions/fragments/group'
import { entityViewSuggestedItemsFragment } from 'js/components/EntityActions/fragments/suggestedItems'
import { featuredViewFragment } from 'js/lib/fragments/featured'
import { ownerFragment } from 'js/lib/fragments/owner'

export const wikiEditFragment = `
... on Wiki {
    ${entityEditFragment}
    parent {
        guid
        title
        url
    }
    hasChildren
    children {
        guid
        title
        url
    }
}
`

export const wikiViewFragment = `
... on Wiki {
    ${baseEntityFragment}
    ${entityViewGroupFragment}
    ${entityViewSuggestedItemsFragment}
    ${featuredViewFragment}
    ${ownerFragment}
    timeUpdated
    localTitle
    localAbstract
    localRichDescription
    localDescription
    hasChildren
    parent {
        guid
        url
    }
    inGroup
    publishRequest {
        status
    }
}
`
