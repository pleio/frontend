import { featuredViewFragment } from 'js/lib/fragments/featured'
import { ownerFragment } from 'js/lib/fragments/owner'

const wikiListFragment = `
    fragment WikiListFragment on Wiki {
        guid
        localTitle
        localExcerpt
        title
        url
        excerpt
        subtype
        tagCategories {
            name
            values
        }
        tags
        ${featuredViewFragment}
        isBookmarked
        isPinned
        canBookmark
        canEdit
        canArchiveAndDelete,
        timePublished
        statusPublished
        isTranslationEnabled
        ${ownerFragment}
        group {
            guid
            ... on Group {
                isClosed
                name
                url
                membership
                isMembershipOnRequest
                isJoinButtonVisible
            }
        }
        publishRequest {
            status
        }
    }
`
export default wikiListFragment
