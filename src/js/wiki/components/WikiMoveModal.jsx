import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { Query } from '@apollo/client/react/components'
import { graphql } from '@apollo/client/react/hoc'
import styled, { css } from 'styled-components'

import Button from 'js/components/Button/Button'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import IconButton from 'js/components/IconButton/IconButton'
import Modal from 'js/components/Modal/Modal'
import Tooltip from 'js/components/Tooltip/Tooltip'

import CheckIcon from 'icons/check-small.svg'
import ArrowLeftIcon from 'icons/chevron-left.svg'
import ArrowRightIcon from 'icons/chevron-right.svg'

const WikiMoveItem = styled.div`
    position: relative;
    display: flex;
    align-items: center;
    border-bottom: 1px solid
        ${(p) => p.theme.color.grey[`${p.isHeader ? 30 : 20}`]};

    .WikiMoveItemName {
        width: 100%;
        display: flex;
        align-items: center;
        padding: 8px 0 8px 16px;
        min-height: 48px;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        color: ${(p) => p.theme.color.text.black};
        user-select: none;

        ${(p) =>
            p.isHeader &&
            css`
                padding-left: 0;
                font-size: ${(p) => p.theme.font.size.normal};
                line-height: ${(p) => p.theme.font.lineHeight.normal};
                font-weight: ${(p) => p.theme.font.weight.semibold};
            `};

        ${(p) =>
            p.isPlaceholder &&
            css`
                color: ${(p) => p.theme.color.primary.main};
            `};

        ${(p) =>
            p.isMoving &&
            css`
                color: ${(p) => p.theme.color.text.grey};
                background-color: ${(p) => p.theme.color.grey[10]};
            `};
    }

    ${(p) =>
        p.isTarget &&
        css`
            border: none;

            .WikiMoveItemName {
                background-color: white;
                border-radius: ${(p) => p.theme.radius.small};
                box-shadow: ${(p) => p.theme.shadow.soft.center};
            }
        `};

    button.WikiMoveItemName {
        &:hover {
            background-color: ${(p) => p.theme.color.hover};
        }

        &:active {
            background-color: ${(p) => p.theme.color.active};
        }

        &:not([disabled]) {
            padding-right: 32px;
        }

        &[disabled] {
            user-select: none;
            pointer-events: none;
        }
    }

    .WikiMoveItemSelect {
        flex-shrink: 0;
        display: flex;
        align-items: center;
        justify-content: center;
        width: 16px;
        height: 16px;
        border-radius: 50%;
        background-color: ${(p) => p.theme.color.secondary.main};
        color: white;
        margin-right: 8px;
    }

    .WikiMoveItemDownButton {
        position: absolute;
        top: 0;
        bottom: 0;
        right: 0;
        margin: auto 0;
        background-color: transparent;
    }
`

const WikiMoveModal = ({ mutate, isVisible, onClose, moveEntity }) => {
    const { t } = useTranslation()

    const [currentGuid, setCurrentGuid] = useState(moveEntity.parent.guid)
    const [selectedGuid, setSelectedGuid] = useState(null)
    const [errors, setErrors] = useState([])

    const toggleSelectedGuid = (guid) => {
        setSelectedGuid(guid !== selectedGuid ? guid : null)
    }

    const goToWiki = (guid) => {
        setCurrentGuid(guid)
        setSelectedGuid(null)
    }

    const handleSubmit = () => {
        return new Promise((resolve, reject) => {
            mutate({
                variables: {
                    input: {
                        guid: moveEntity.guid,
                        containerGuid: selectedGuid || currentGuid,
                    },
                },
                refetchQueries: ['WikiItem'],
            })
                .then(() => {
                    onClose()
                    resolve()
                })
                .catch((errors) => {
                    setErrors(errors)
                    reject(new Error(errors))
                })
        })
    }

    return (
        <Modal
            isVisible={isVisible}
            onClose={onClose}
            title={t('action.move-title', { title: moveEntity.title })}
            size="small"
        >
            <Query query={query} variables={{ guid: currentGuid }}>
                {({ loading, data }) => {
                    if (loading || !data) return null
                    const { entity } = data

                    const currentIsSameAsMovingParent =
                        entity.guid === moveEntity.parent.guid
                    const selectedIsSameAsMovingParent =
                        selectedGuid === moveEntity.parent.guid

                    const movingToCurrent =
                        !selectedGuid && !currentIsSameAsMovingParent

                    const moveDisabled = selectedGuid
                        ? selectedIsSameAsMovingParent
                        : currentIsSameAsMovingParent

                    return (
                        <>
                            <WikiMoveItem isHeader>
                                {entity.parent && (
                                    <IconButton
                                        size="normal"
                                        variant="secondary"
                                        aria-label={t(
                                            'entity-wiki.show-item-content',
                                            { title: entity.parent.title },
                                        )}
                                        onClick={() =>
                                            goToWiki(entity.parent.guid)
                                        }
                                    >
                                        <ArrowLeftIcon />
                                    </IconButton>
                                )}
                                <span className="WikiMoveItemName">
                                    {entity.title}
                                </span>
                            </WikiMoveItem>
                            {entity.children.map((child, i) => {
                                const isSelected = child.guid === selectedGuid
                                const isMoving = child.guid === moveEntity.guid
                                const isMovingParent =
                                    child.guid === moveEntity.parent.guid
                                const isDisabled = isMoving || isMovingParent

                                if (!moveDisabled && isMoving) return null

                                return (
                                    <WikiMoveItem key={i} isMoving={isMoving}>
                                        <Tooltip
                                            content={
                                                isMoving &&
                                                t(
                                                    'entity-wiki.current-location',
                                                )
                                            }
                                        >
                                            <div style={{ width: '100%' }}>
                                                <button
                                                    className="WikiMoveItemName"
                                                    aria-current={isSelected}
                                                    disabled={isDisabled}
                                                    aria-label={
                                                        isSelected
                                                            ? t(
                                                                  'entity-wiki.unselect-location',
                                                                  {
                                                                      title: child.title,
                                                                  },
                                                              )
                                                            : t(
                                                                  'entity-wiki.select-location',
                                                                  {
                                                                      title: child.title,
                                                                  },
                                                              )
                                                    }
                                                    onClick={() =>
                                                        toggleSelectedGuid(
                                                            child.guid,
                                                        )
                                                    }
                                                >
                                                    {isSelected && (
                                                        <div className="WikiMoveItemSelect">
                                                            <CheckIcon />
                                                        </div>
                                                    )}
                                                    {child.title}
                                                </button>
                                            </div>
                                        </Tooltip>
                                        {!isMoving && (
                                            <IconButton
                                                className="WikiMoveItemDownButton"
                                                size="normal"
                                                variant="secondary"
                                                aria-label={t(
                                                    'entity-wiki.show-item-content',
                                                    { title: child.title },
                                                )}
                                                onClick={() =>
                                                    goToWiki(child.guid)
                                                }
                                                style={{
                                                    marginLeft: 'auto',
                                                }}
                                            >
                                                <ArrowRightIcon />
                                            </IconButton>
                                        )}
                                    </WikiMoveItem>
                                )
                            })}

                            {movingToCurrent && (
                                <Tooltip
                                    content={t('entity-wiki.target-location')}
                                >
                                    <WikiMoveItem isTarget>
                                        <div className="WikiMoveItemName">
                                            {moveEntity.title}
                                        </div>
                                    </WikiMoveItem>
                                </Tooltip>
                            )}

                            <Errors errors={errors} />

                            <Flexer mt>
                                <Button
                                    size="normal"
                                    variant="tertiary"
                                    onClick={onClose}
                                >
                                    {t('action.cancel')}
                                </Button>
                                <Button
                                    size="normal"
                                    variant="primary"
                                    disabled={moveDisabled}
                                    onHandle={handleSubmit}
                                >
                                    {t('action.move')}
                                </Button>
                            </Flexer>
                        </>
                    )
                }}
            </Query>
        </Modal>
    )
}

const query = gql`
    query WikiMove($guid: String!) {
        entity(guid: $guid) {
            guid
            ... on Wiki {
                title
                parent {
                    guid
                    title
                }
                children {
                    guid
                    title
                }
            }
        }
    }
`

const Mutation = gql`
    mutation editEntity($input: editEntityInput!) {
        editEntity(input: $input) {
            entity {
                guid
            }
        }
    }
`

export default graphql(Mutation)(WikiMoveModal)
