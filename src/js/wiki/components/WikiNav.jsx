import React, { createContext } from 'react'
import { useTranslation } from 'react-i18next'
import { NavLink, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import GoUpArrow from 'icons/go-up-arrow.svg'

import WikiNavItem from './WikiNavItem'
import WikiNavList from './WikiNavList'

export const WikiNavContext = createContext()

const WikiNav = ({ mutate, viewer, editMode, ...rest }) => {
    const { t } = useTranslation()
    const params = useParams()
    const { guid } = params

    const { loading, data } = useQuery(Query, {
        variables: {
            guid,
        },
        fetchPolicy: 'cache-and-network',
    })

    if (loading) return null
    if (!data?.entity) return null

    const onDragEnd = (result) => {
        if (!result.destination) return

        const sourcePosition = result.source.index
        const destinationPosition = result.destination.index

        const newChildren = [...children]
        const [movedPage] = newChildren.splice(sourcePosition, 1)
        newChildren.splice(destinationPosition, 0, movedPage)

        mutate({
            variables: {
                input: {
                    guid: result.draggableId,
                    sourcePosition,
                    destinationPosition,
                },
            },

            optimisticResponse: {
                reorder: {
                    __typename: 'reorderPayload',
                    container: {
                        __typename: 'Wiki',
                        guid,
                        children: newChildren,
                    },
                },
            },
        })
    }

    const { entity } = data
    const { children, parent, rootParent } = entity

    const grandParent = parent?.parent
    const highestParent = grandParent || parent || entity

    const root = rootParent || highestParent

    return (
        <div {...rest}>
            {root && (
                <WikiNavItem isRoot as={NavLink} to={root.url} end>
                    {root.title}
                </WikiNavItem>
            )}

            {grandParent && grandParent?.guid !== root.guid && (
                <WikiNavItem as={NavLink} to={grandParent.url}>
                    <GoUpArrow style={{ marginRight: '6px' }} />
                    {t('entity-wiki.go-up')}
                </WikiNavItem>
            )}

            <WikiNavContext.Provider
                value={{
                    activeGuid: guid,
                    editMode,
                    canWriteToContainer: viewer.canWriteToContainer,
                    onDragEnd,
                }}
            >
                <WikiNavList entity={highestParent} indentLevel={0} />
            </WikiNavContext.Provider>
        </div>
    )
}

const MUTATION = gql`
    mutation WikiNavMutation($input: reorderInput!) {
        reorder(input: $input) {
            container {
                guid
                ... on Wiki {
                    children {
                        guid
                        title
                        url
                    }
                }
            }
        }
    }
`

const wikiNavFragment = `
    guid
    title
    url
    canEdit
    hasChildren
`

const Query = gql`
    query WikiItem($guid: String!) {
        entity(guid: $guid) {
            guid
            ... on Wiki {
                title
                canEdit
                url
                inGroup
                statusPublished
                rootParent {
                    ${wikiNavFragment}
                }
                parent {
                    ${wikiNavFragment}
                    parent {
                        ${wikiNavFragment}
                        children {
                            ${wikiNavFragment}
                            children {
                                ${wikiNavFragment}
                                children {
                                    ${wikiNavFragment}
                                }
                            }
                        }
                    }
                    children {
                        ${wikiNavFragment}
                        children {
                            ${wikiNavFragment}
                        }
                    }
                }
                children {
                    ${wikiNavFragment}
                }
            }
        }
    }
`

export default graphql(MUTATION)(WikiNav)
