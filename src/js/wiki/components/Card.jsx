import React from 'react'

import FeedItem from 'js/components/FeedItem/FeedItem'
import FeedItemContent from 'js/components/FeedItem/FeedItemContent'
import FeedItemImage from 'js/components/FeedItem/FeedItemImage'

import getRefetchQueries from '../helpers/getRefetchQueries'

const Card = ({
    'data-feed': dataFeed,
    entity,
    canPin,
    hideSubtype,
    hideGroup,
    hideExcerpt,
    hideActions,
    excerptMaxLines,
}) => {
    const onEdit = `${entity.group ? entity.group.url : ''}/wiki/edit/${
        entity.guid
    }`

    return (
        <FeedItem data-feed={dataFeed}>
            <FeedItemContent
                entity={entity}
                hideSubtype={hideSubtype}
                hideGroup={hideGroup}
                hideExcerpt={hideExcerpt}
                excerptMaxLines={excerptMaxLines}
                canPin={canPin}
                onEdit={onEdit}
                hideActions={hideActions}
                refetchQueries={getRefetchQueries(false, !!entity.group)}
            />
            <FeedItemImage entity={entity} />
        </FeedItem>
    )
}

export default Card
