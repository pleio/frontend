import React, { useContext, useState } from 'react'
import { Draggable } from 'react-beautiful-dnd'
import { NavLink } from 'react-router-dom'

import findNestedObject from '../helpers/findNestedObject'

import { WikiNavContext } from './WikiNav'
import WikiNavItem from './WikiNavItem'
import WikiNavList from './WikiNavList'

const WikiNavListItem = ({ index, entity, indentLevel, isDraggable }) => {
    const { activeGuid, editMode, canWriteToContainer } =
        useContext(WikiNavContext)

    const { guid, title, url, hasChildren, canEdit } = entity

    const isActive = guid === activeGuid
    const isActiveTree = findNestedObject(entity, 'guid', activeGuid)

    const [isExpanded, setIsExpanded] = useState(isActiveTree)

    const navLinkProps = {
        as: isActive ? 'button' : NavLink,
        to: isActive ? null : url,
        onClick: isActive ? () => setIsExpanded(!isExpanded) : null,
        isActive,
        indentLevel,
        showArrowIcon: hasChildren,
        isExpanded,
    }

    return (
        <>
            {isDraggable ? (
                <Draggable
                    draggableId={guid}
                    index={index}
                    isDragDisabled={!canEdit}
                >
                    {(provided) => (
                        <li
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                        >
                            <WikiNavItem
                                {...navLinkProps}
                                showMoveIcon={canEdit}
                                {...provided.dragHandleProps}
                            >
                                {title}
                            </WikiNavItem>
                        </li>
                    )}
                </Draggable>
            ) : (
                <li>
                    <WikiNavItem {...navLinkProps}>{title}</WikiNavItem>
                </li>
            )}

            {isExpanded && (
                <WikiNavList
                    entity={entity}
                    editMode={editMode}
                    canWriteToContainer={canWriteToContainer}
                    indentLevel={indentLevel + 1}
                />
            )}
        </>
    )
}

export default WikiNavListItem
