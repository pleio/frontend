import React, { useContext } from 'react'
import { DragDropContext, Droppable } from 'react-beautiful-dnd'
import { useTranslation } from 'react-i18next'
import { Link, useLocation, useParams } from 'react-router-dom'
import { getGroupUrl } from 'helpers'

import findNestedObject from '../helpers/findNestedObject'

import { WikiNavContext } from './WikiNav'
import WikiNavItem from './WikiNavItem'
import WikiNavListItem from './WikiNavListItem'

const WikiNavList = ({ entity, indentLevel }) => {
    const { activeGuid, editMode, canWriteToContainer, onDragEnd } =
        useContext(WikiNavContext)

    const { t } = useTranslation()
    const location = useLocation()
    const params = useParams()

    const { guid, canEdit, children } = entity

    const isActive = guid === activeGuid
    const isActiveTree = findNestedObject(entity, 'guid', activeGuid)

    const isDraggable = editMode && canEdit && isActive && children?.length > 1

    const childProps = {
        editMode,
        canWriteToContainer,
        indentLevel,
    }

    return (
        <>
            {isActiveTree && (
                <>
                    {isDraggable ? (
                        <DragDropContext onDragEnd={onDragEnd}>
                            <Droppable type="subnav" droppableId={guid}>
                                {(provided) => (
                                    <ul ref={provided.innerRef}>
                                        {children.map((child, index) => {
                                            return (
                                                <WikiNavListItem
                                                    key={child.guid}
                                                    index={index}
                                                    entity={child}
                                                    isDraggable
                                                    {...childProps}
                                                />
                                            )
                                        })}
                                        {provided.placeholder}
                                    </ul>
                                )}
                            </Droppable>
                        </DragDropContext>
                    ) : (
                        <ul>
                            {children.map((child) => {
                                return (
                                    <WikiNavListItem
                                        key={child.guid}
                                        entity={child}
                                        {...childProps}
                                    />
                                )
                            })}
                        </ul>
                    )}
                </>
            )}

            {editMode && canWriteToContainer && isActive && (
                <WikiNavItem
                    as={Link}
                    to={`${getGroupUrl(params)}/wiki/add/${guid}`}
                    state={{
                        prevPathname: location.pathname,
                    }}
                    indentLevel={indentLevel}
                >
                    + {t('entity-wiki.create')}
                </WikiNavItem>
            )}
        </>
    )
}

export default WikiNavList
