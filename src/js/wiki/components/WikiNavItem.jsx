import React from 'react'
import styled, { css } from 'styled-components'

import ListItem from 'js/components/ListItem/ListItem'

import ArrowDownIcon from 'icons/chevron-down-small.svg'
import MoveIcon from 'icons/move-vertical.svg'

const Wrapper = styled(ListItem)`
    padding-top: 6px;
    padding-bottom: 6px;

    ${(p) =>
        p.$isRoot &&
        css`
            font-size: ${(p) => p.theme.font.size.normal};
            line-height: ${(p) => p.theme.font.lineHeight.normal};
            font-weight: ${(p) => p.theme.font.weight.bold};
        `};

    ${(p) =>
        p.$indentLevel &&
        css`
            padding-left: calc((${p.$indentLevel} * 16px) + 16px);
        `};

    ${(p) =>
        p.$isActive &&
        css`
            color: ${(p) => p.theme.color.primary.main};
        `};
`

const WikiNavItem = ({
    children,
    showMoveIcon,
    showArrowIcon,
    isExpanded,
    isRoot,
    indentLevel,
    isActive,
    ...rest
}) => {
    return (
        <Wrapper
            $isRoot={isRoot}
            $indentLevel={indentLevel}
            $isActive={isActive}
            {...rest}
        >
            {showMoveIcon && (
                <MoveIcon
                    style={{
                        marginRight: '6px',
                    }}
                />
            )}
            {children}
            {showArrowIcon && (
                <ArrowDownIcon
                    style={{
                        marginLeft: 'auto',
                        transform: `scaleY(${isExpanded ? -1 : 1})`,
                    }}
                />
            )}
        </Wrapper>
    )
}

export default WikiNavItem
