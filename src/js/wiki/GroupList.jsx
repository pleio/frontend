import React from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { useWindowScrollPosition } from 'helpers'

import Document from 'js/components/Document'
import { Container } from 'js/components/Grid/Grid'

import Card from './components/Card'
import WikiList from './containers/WikiList'

const GroupList = () => {
    const { t } = useTranslation()
    const { groupGuid } = useParams()

    const { data } = useQuery(Query, {
        variables: {
            guid: groupGuid,
        },
    })

    useWindowScrollPosition(`${groupGuid}-wiki`)

    return (
        <>
            <Document
                title={t('entity-wiki.title-list')}
                containerTitle={data?.entity?.name}
            />

            <Container size="tiny">
                <WikiList
                    type="object"
                    subtype="wiki"
                    containerGuid={groupGuid}
                    childComponent={Card}
                    offset={0}
                    limit={20}
                />
            </Container>
        </>
    )
}

const Query = gql`
    query GroupWiki($guid: String!) {
        entity(guid: $guid) {
            guid
            ... on Group {
                name
            }
        }
    }
`

export default GroupList
