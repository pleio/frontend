import React from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { getGroupUrl } from 'helpers'

import EntityAddEditForm from 'js/components/EntityActions/EntityAddEditForm'
import NotFound from 'js/core/NotFound'
import { wikiEditFragment } from 'js/wiki/fragments/entity'

import getRefetchQueries from './helpers/getRefetchQueries'

const Edit = () => {
    const { t } = useTranslation()
    const navigate = useNavigate()
    const params = useParams()
    const { guid } = params

    const { loading, data } = useQuery(Query, {
        variables: {
            guid,
        },
    })

    if (loading) return null
    if (!data || !data.entity) return <NotFound />

    const { entity } = data

    const afterDelete = () => {
        navigate(entity.parent?.url || `${getGroupUrl(params)}/wiki`, {
            replace: true,
        })
    }

    const refetchQueries = getRefetchQueries(true, !!entity.group)

    return (
        <EntityAddEditForm
            title={t('entity-wiki.edit')}
            deleteTitle={t('entity-wiki.delete')}
            subtype="wiki"
            entity={entity}
            afterDelete={afterDelete}
            refetchQueries={refetchQueries}
        />
    )
}

const Query = gql`
    query EditWiki($guid: String!) {
        entity(guid: $guid) {
            guid
            ${wikiEditFragment}
        }
    }
`

export default Edit
