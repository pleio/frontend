import React from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'

import EntityAddEditForm from 'js/components/EntityActions/EntityAddEditForm'

import getRefetchQueries from './helpers/getRefetchQueries'

const Add = () => {
    const { t } = useTranslation()
    const { containerGuid, groupGuid } = useParams()

    const refetchQueries = getRefetchQueries(
        false,
        !containerGuid && !!groupGuid,
    )

    return (
        <EntityAddEditForm
            title={t('entity-wiki.create')}
            subtype="wiki"
            refetchQueries={refetchQueries}
        />
    )
}

export default Add
