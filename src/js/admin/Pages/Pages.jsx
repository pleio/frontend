import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useLocation, useParams } from 'react-router-dom'

import Button from 'js/components/Button/Button'
import Document from 'js/components/Document'
import Flexer from 'js/components/Flexer/Flexer'
import { Container } from 'js/components/Grid/Grid'
import { H3 } from 'js/components/Heading'
import Section from 'js/components/Section/Section'
import Spacer from 'js/components/Spacer/Spacer'
import PageList from 'js/page/PageList'

const Pages = () => {
    const { parentGuid } = useParams()
    const { t } = useTranslation()
    const location = useLocation()

    return (
        <>
            <Document
                title={t('admin.pages')}
                containerTitle={t('admin.title')}
            />
            <Container>
                <Section divider>
                    <Spacer spacing="normal">
                        {!parentGuid && (
                            <Flexer justifyContent="space-between">
                                <H3 as="h1">{t('admin.pages')}</H3>
                                <Button
                                    as={Link}
                                    to="/pages/add"
                                    state={{ prevPathname: location.pathname }}
                                    size="normal"
                                    variant="secondary"
                                >
                                    {t('entity-cms.create')}
                                </Button>
                            </Flexer>
                        )}

                        <PageList rootUrl="/admin/pages" />
                    </Spacer>
                </Section>
            </Container>
        </>
    )
}
export default Pages
