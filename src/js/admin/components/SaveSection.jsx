import React from 'react'
import { useTranslation } from 'react-i18next'

import AnimatePresence from 'js/components/AnimatePresence/AnimatePresence'
import Button from 'js/components/Button/Button'

const SaveSection = ({ isVisible, onSubmit }) => {
    const handleSubmit = () => {
        return new Promise((resolve, reject) => {
            onSubmit(resolve, reject)
        })
    }

    const { t } = useTranslation()

    return (
        <AnimatePresence visible={isVisible}>
            <div style={{ display: 'flex', justifyContent: 'center' }}>
                <Button size="normal" variant="primary" onHandle={handleSubmit}>
                    {t('action.save')}
                </Button>
            </div>
        </AnimatePresence>
    )
}

export default SaveSection
