import React from 'react'
import { media } from 'helpers'
import styled from 'styled-components'

import Card from 'js/components/Card/Card'
import { H3 } from 'js/components/Heading'
import Spacer from 'js/components/Spacer/Spacer'

const WidgetStatWrapper = styled.div`
    .StatTitle {
        font-size: ${(p) => (p.$titleSize === 'large' ? '28px' : '16px')};
        font-weight: ${(p) => p.theme.font.weight.bold};
        line-height: 28px;
        color: ${(p) => p.theme.color.primary.main};
        white-space: nowrap;
        margin-bottom: 4px;
    }

    .StatSubtitle {
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        color: ${(p) => p.theme.color.text.grey};
    }
`

export const WidgetStat = ({ title, titleSize = 'large', subtitle }) => {
    return (
        <WidgetStatWrapper $titleSize={titleSize}>
            <div className="StatTitle">{title}</div>
            {subtitle && <div className="StatSubtitle">{subtitle}</div>}
        </WidgetStatWrapper>
    )
}

const Wrapper = styled(Card)`
    display: flex;
    flex-direction: column;

    ${media.mobileLandscapeDown`
        padding: ${(p) =>
            `${p.theme.padding.vertical.small} ${p.theme.padding.horizontal.small}`};
    `}

    ${media.tabletUp`
        padding: ${(p) =>
            `${p.theme.padding.vertical.normal} ${p.theme.padding.horizontal.normal}`};
    `}

    .WidgetSpacer {
        flex-grow: 1;
        text-align: ${(p) => p.$textAlign};
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: space-between;
    }

    .WidgetTitle {
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .WidgetIcon {
        flex-shrink: 0;
        margin-right: 8px;
    }
`

const Widget = ({ title, icon, children, textAlign = 'center', ...rest }) => {
    return (
        <Wrapper radiusSize="large" $textAlign={textAlign} {...rest}>
            <Spacer spacing="normal" className="WidgetSpacer">
                <H3 as="h2" className="WidgetTitle">
                    {icon && <div className="WidgetIcon">{icon}</div>}
                    {title}
                </H3>
                {children}
            </Spacer>
        </Wrapper>
    )
}

export default Widget
