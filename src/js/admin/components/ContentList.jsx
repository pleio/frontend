import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useQuery } from '@apollo/client'
import { useDebounce, useLocalStorage } from 'helpers'

import useStatusOptions from 'js/admin/hooks/useStatusOptions'
import Button from 'js/components/Button/Button'
import UserField from 'js/components/EntityActions/Advanced/components/UserField.jsx'
import FetchMore from 'js/components/FetchMore'
import FilterWrapper from 'js/components/FilterWrapper'
import Flexer from 'js/components/Flexer/Flexer'
import { H4 } from 'js/components/Heading'
import ArchiveEntitiesModal from 'js/components/Item/ItemActions/components/ArchiveEntitiesModal'
import DeleteEntitiesModal from 'js/components/Item/ItemActions/components/DeleteEntitiesModal'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Select from 'js/components/Select/Select'
import Spacer from 'js/components/Spacer/Spacer'
import Table from 'js/components/Table'
import Text from 'js/components/Text/Text'
import Textfield from 'js/components/Textfield/Textfield'
import Toolbar from 'js/components/Toolbar/Toolbar'

import ContentItem from './ContentItem'
import GET_SITE_CONTENT from './ContentQuery'

const useTableHeaders = (showFiles) => {
    const { t } = useTranslation()

    return [
        {
            name: 'title',
            label: showFiles ? t('global.name') : t('form.title'),
            style: { minWidth: 200 },
        },

        ...(!showFiles
            ? [
                  {
                      name: 'contentType',
                      label: t('global.type'),
                      style: { minWidth: 60 },
                  },
              ]
            : []),
        {
            name: 'ownerName',
            label: t('global.owner'),
            style: { minWidth: 80 },
        },
        {
            name: 'groupName',
            label: t('global.group'),
            style: { minWidth: 100 },
        },
        {
            options: [
                {
                    name: 'timeUpdated',
                    label: t('global.last-edited'),
                    defaultOrderDirection: 'desc',
                },
                {
                    name: 'timePublished',
                    label: showFiles
                        ? t('global.created')
                        : t('global.published'),
                    defaultOrderDirection: 'desc',
                },
                ...(!showFiles
                    ? [
                          {
                              name: 'lastSeen',
                              label: t('admin.content-last-viewed'),
                              defaultOrderDirection: 'desc',
                          },
                      ]
                    : []),
            ],
            align: 'right',
            style: { minWidth: 100 },
        },
        ...(showFiles
            ? [
                  {
                      name: 'fileSize',
                      label: t('global.size'),
                      align: 'right',
                      style: { minWidth: 80 },
                  },
              ]
            : []),
        {
            label: t('permissions.read'),
            name: 'readAccess',
            align: 'right',
            style: {
                paddingRight: 0,
                width: 40,
            },
        },
        {
            style: { width: 40 },
        },
    ]
}

const ContentList = ({ id, subtypesMap, subtypeLabels, showFiles }) => {
    const { t } = useTranslation()
    const tableHeaders = useTableHeaders(showFiles)

    const allSubTypes = Object.values(subtypesMap).map(
        (subtype) => subtype.serverName,
    )

    const [searchInput, setSearchInput] = useState('')
    const q = useDebounce(searchInput, 200)

    const [queryLimit, setQueryLimit] = useState(50)
    const [subtypes, setSubtypes] = useState([])
    const [group, setGroup] = useState('')
    const [user, setUser] = useState('')
    const [statusPublished, setStatusPublished] = useState([])

    const [orderBy, setOrderBy] = useLocalStorage(
        `${id}-files-orderBy`,
        'timePublished',
    )
    const [orderDirection, setOrderDirection] = useLocalStorage(
        `${id}-files-orderDirection`,
        'desc',
    )

    const variables = {
        q,
        subtypes: subtypes.length === 0 ? allSubTypes : subtypes,
        containerGuid: group,
        offset: 0,
        limit: queryLimit,
        userGuid: user,
        ...(!showFiles ? { statusPublished } : {}),
        orderBy,
        orderDirection,
    }

    const { loading, data, fetchMore } = useQuery(GET_SITE_CONTENT, {
        variables,
    })

    const [selected, setSelected] = useState([])
    const clearSelected = () => {
        setSelected([])
    }

    const [showDeleteModal, setShowDeleteModal] = useState(false)
    const toggleDeleteModal = () => {
        setShowDeleteModal(!showDeleteModal)
    }

    const [showArchiveModal, setShowArchiveModal] = useState(false)
    const toggleArchiveModal = () => {
        setShowArchiveModal(!showArchiveModal)
    }

    const groups = data?.groups?.edges || []

    const statusOptions = useStatusOptions()

    return (
        <>
            <Spacer spacing="small">
                {selected.length > 0 && (
                    <Toolbar count={selected.length} onClear={clearSelected}>
                        <Flexer
                            divider="normal"
                            style={{
                                marginLeft: 'auto',
                            }}
                        >
                            <Button
                                size="normal"
                                variant="secondary"
                                aria-label={t('admin.block-selection', {
                                    count: selected.length,
                                })}
                                onClick={toggleArchiveModal}
                            >
                                {t('action.archive')}
                            </Button>
                            <ArchiveEntitiesModal
                                isVisible={showArchiveModal}
                                onClose={toggleArchiveModal}
                                entities={selected}
                                refetchQueries={['AdminContent']}
                                onComplete={() => {
                                    clearSelected()
                                    toggleArchiveModal()
                                }}
                            />
                            <Button
                                size="normal"
                                variant="secondary"
                                aria-label={t('admin.delete-selection', {
                                    count: selected.length,
                                })}
                                onClick={toggleDeleteModal}
                            >
                                {t('action.delete')}
                            </Button>
                            <DeleteEntitiesModal
                                isVisible={showDeleteModal}
                                onClose={toggleDeleteModal}
                                entities={selected}
                                refetchQueries={['AdminContent']}
                                onComplete={() => {
                                    clearSelected()
                                    toggleDeleteModal()
                                }}
                            />
                        </Flexer>
                    </Toolbar>
                )}

                <FilterWrapper style={{ padding: '4px 0' }}>
                    {/* div is needed for FilterWrapper */}
                    <div>
                        <Textfield
                            name="language"
                            label={t('form.search-by', {
                                attribute: showFiles
                                    ? t('global.name')
                                    : t('form.title'),
                            })}
                            value={searchInput}
                            onChange={(evt) => setSearchInput(evt.target.value)}
                            onClear={() => setSearchInput('')}
                        />
                    </div>
                    <Select
                        label={t('global.type')}
                        name="filter-by-type"
                        placeholder={t('filters.filter-by')}
                        options={subtypeLabels}
                        value={subtypes}
                        onChange={setSubtypes}
                        isMulti
                    />
                    <UserField
                        name="filter-by-owner"
                        label={t('global.owner')}
                        value={user}
                        onChange={(user) => setUser(user?.guid || '')}
                    />
                    <Select
                        label={t('global.group')}
                        name="filter-by-group"
                        placeholder={t('filters.filter-by')}
                        options={groups.map(({ guid, name }) => ({
                            value: guid,
                            label: name,
                        }))}
                        value={group}
                        onChange={setGroup}
                        isClearable
                    />
                    {!showFiles && (
                        <Select
                            label={t('global.status')}
                            name="filter-by-status"
                            placeholder={t('filters.filter-by')}
                            options={statusOptions}
                            value={statusPublished}
                            onChange={setStatusPublished}
                            isMulti
                        />
                    )}
                </FilterWrapper>
            </Spacer>

            {loading ? (
                <LoadingSpinner />
            ) : data.entities.edges.length === 0 ? (
                <Text textAlign="center">{t('admin.content-not-found')}</Text>
            ) : (
                <>
                    <H4 role="status" style={{ margin: '16px 0 4px' }}>
                        {showFiles
                            ? t('entity-file.result', {
                                  count: data.entities.total,
                              })
                            : t('global.count-items', {
                                  count: data.entities.total,
                              })}
                    </H4>

                    <Table
                        rowHeight="normal"
                        headers={tableHeaders}
                        orderBy={orderBy}
                        orderDirection={orderDirection}
                        setOrderBy={setOrderBy}
                        setOrderDirection={setOrderDirection}
                        tableWidth={showFiles ? 640 : 700}
                    >
                        <FetchMore
                            as="tbody"
                            edges={data.entities.edges}
                            getMoreResults={(data) => data.entities.edges}
                            fetchMore={fetchMore}
                            fetchCount={25}
                            setLimit={setQueryLimit}
                            maxLimit={data.entities.total}
                            resultsMessage={t('global.result', {
                                count: data.entities.total,
                            })}
                            fetchMoreButtonHeight={48}
                        >
                            {data.entities.edges.map((entity) => {
                                const selectedIndex = selected?.findIndex(
                                    (el) => el?.guid === entity.guid,
                                )
                                const isSelected = selectedIndex > -1

                                const toggleSelected = () => {
                                    if (!isSelected) {
                                        setSelected([...selected, entity])
                                    } else {
                                        const newSelected = [...selected]
                                        newSelected.splice(selectedIndex, 1)
                                        setSelected(newSelected)
                                    }
                                }

                                return (
                                    <ContentItem
                                        key={entity.guid}
                                        entity={entity}
                                        subtypes={subtypesMap}
                                        showFiles={showFiles}
                                        isSelected={isSelected}
                                        onToggle={toggleSelected}
                                        clearSelected={clearSelected}
                                        orderBy={orderBy}
                                    />
                                )
                            })}
                        </FetchMore>
                    </Table>
                </>
            )}
        </>
    )
}

export default ContentList
