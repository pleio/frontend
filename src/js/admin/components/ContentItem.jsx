import React from 'react'
import { useTranslation } from 'react-i18next'

import Checkbox from 'js/components/Checkbox/Checkbox'
import DisplayAccessLevel from 'js/components/DisplayAccessLevel'
import DisplayDate from 'js/components/DisplayDate'
import HideVisually from 'js/components/HideVisually/HideVisually'
import ItemActions from 'js/components/Item/ItemActions/ItemActions'
import FileFolderIcon from 'js/files/components/FileFolderIcon'
import getIconName from 'js/files/helpers/getIconName'
import { humanFileSize } from 'js/lib/helpers/fileSize'

const ContentItem = ({
    entity,
    subtypes,
    showFiles,
    isSelected,
    onToggle,
    clearSelected,
    orderBy,
}) => {
    const { t } = useTranslation()

    const {
        guid,
        title,
        url,
        __typename,
        owner,
        group,
        lastSeen,
        timePublished,
        timeUpdated,
        accessId,
        size,
        mimeType,
        hasChildren,
    } = entity

    const subtype = __typename.toLowerCase()

    const isFileFolder = ['file', 'folder', 'pad'].includes(subtype)

    const editUrl = isFileFolder
        ? url
        : subtypes[subtype]?.editUrl
          ? `/${subtypes[subtype].editUrl}/edit/${guid}`
          : ''

    const iconName = isFileFolder
        ? getIconName(subtype, mimeType, hasChildren)
        : null

    const selectedDateSorting =
        orderBy === 'lastSeen'
            ? lastSeen
            : orderBy === 'timePublished'
              ? timePublished
              : timeUpdated

    return (
        <tr>
            <td>
                <div style={{ height: '100%', display: 'flex' }}>
                    {!showFiles && (
                        <Checkbox
                            name={`file-${guid}`}
                            checked={isSelected}
                            size="small"
                            onChange={onToggle}
                            style={{
                                paddingRight: '8px',
                                display: 'flex',
                                alignItems: 'center',
                            }}
                        />
                    )}
                    <a
                        href={url}
                        target="_blank"
                        rel="noopener noreferrer"
                        className="TableLinkBlock"
                        style={{
                            wordBreak: 'break-word',
                        }}
                    >
                        {iconName && (
                            <FileFolderIcon
                                name={iconName}
                                style={{ marginRight: '8px' }}
                            />
                        )}
                        {title}
                        <HideVisually>
                            {t('global.opens-in-new-window')}
                        </HideVisually>
                    </a>
                </div>
            </td>
            {!showFiles && <td>{subtypes[subtype]?.contentName || '-'}</td>}
            <td>
                <a
                    href={owner.url}
                    target="_blank"
                    rel="noopener noreferrer"
                    className="TableLinkBlock"
                >
                    {owner.name}
                    <HideVisually>
                        {t('global.opens-in-new-window')}
                    </HideVisually>
                </a>
            </td>
            <td>
                {group?.name && (
                    <a
                        href={url}
                        target="_blank"
                        className="TableLinkBlock"
                        rel="noreferrer"
                    >
                        {group?.name}
                        <HideVisually>
                            {t('global.opens-in-new-window')}
                        </HideVisually>
                    </a>
                )}
            </td>
            <td style={{ textAlign: 'right' }}>
                <DisplayDate date={selectedDateSorting} />
            </td>
            {showFiles && (
                <td style={{ textAlign: 'right' }}>
                    {size ? humanFileSize(size) : null}
                </td>
            )}
            <td
                style={{
                    padding: 0,
                }}
            >
                <DisplayAccessLevel
                    accessId={accessId}
                    style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        height: '100%',
                    }}
                />
            </td>
            <td>
                <ItemActions
                    entity={entity}
                    onEdit={editUrl}
                    refetchQueries={['AdminContent']}
                    onAfterDelete={clearSelected}
                    onAfterArchive={clearSelected}
                />
            </td>
        </tr>
    )
}

export default ContentItem
