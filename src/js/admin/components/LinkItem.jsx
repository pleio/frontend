import React from 'react'
import styled from 'styled-components'

import IconButton from 'js/components/IconButton/IconButton'
import Textfield from 'js/components/Textfield/Textfield'

import RemoveIcon from 'icons/cross.svg'
import MoveIcon from 'icons/move-vertical.svg'

const Wrapper = styled.div`
    display: flex;

    .LinkItemFlexer {
        display: flex;
        flex-grow: 1;

        > :not(:last-child) {
            border-right: none;
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
        }

        > :not(:first-child) {
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
        }
    }
`

const LinkItem = ({
    index,
    parentIndex,
    name,
    fields,
    removeLabel,
    dragHandleProps,
    onClickRemove,
    onChange,
    ...rest
}) => {
    const handleClickRemove = () => {
        onClickRemove(index, parentIndex)
    }

    const handleChange = (evt, key) => {
        evt.persist()
        onChange(evt.target.value, key, index, parentIndex)
    }

    return (
        <Wrapper {...rest}>
            {!!dragHandleProps && (
                <IconButton
                    as="div"
                    size="large"
                    variant="secondary"
                    {...dragHandleProps}
                >
                    <MoveIcon />
                </IconButton>
            )}
            <div className="LinkItemFlexer">
                {fields.map((field) => (
                    <Textfield
                        key={`${name}-${field.key}`}
                        name={`${name}-${field.key}`}
                        label={field.label}
                        value={field.value || ''}
                        onChange={(evt) => handleChange(evt, field.key)}
                    />
                ))}
            </div>
            <IconButton
                size="large"
                variant="secondary"
                aria-label={removeLabel}
                onClick={handleClickRemove}
            >
                <RemoveIcon />
            </IconButton>
        </Wrapper>
    )
}

export default LinkItem
