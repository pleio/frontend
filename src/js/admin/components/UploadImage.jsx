import React, { useRef } from 'react'
import { useTranslation } from 'react-i18next'
import styled, { css } from 'styled-components'

import Button from 'js/components/Button/Button'
import FileInput from 'js/components/FileInput/FileInput'
import IconButton from 'js/components/IconButton/IconButton'
import { FILE_TYPE } from 'js/lib/constants'
import { media } from 'js/lib/helpers'

import RemoveIcon from 'icons/cross-small.svg'

const Wrapper = styled.div`
    display: flex;

    ${media.mobilePortrait`
        flex-direction: column;
        align-items: flex-end;
    `}

    ${media.mobileLandscapeUp`
        align-items: center;
    `}

    .UploadImage {
        position: relative;
        display: flex;
        align-items: center;
        background-color: ${(p) => p.theme.color.grey[20]};

        > img {
            max-height: 100%;
            user-select: none;

            ${(p) =>
                p.$stretch &&
                css`
                    height: 100%;
                `};
        }

        &:hover,
        &:focus {
            .UploadImageRemoveButton {
                opacity: 1;
            }
        }

        ${media.mobilePortrait`
            margin-bottom: 8px;
        `}

        ${media.mobileLandscapeUp`
            order: 1;
            margin-left: 16px;
        `}
    }

    .UploadImageRemoveButton {
        position: absolute;
        top: -12px;
        right: -12px;
        opacity: 0;
        transition: opacity ${(p) => p.theme.transition.fast};

        &:focus {
            opacity: 1;
        }
    }
`

const UploadImage = ({
    name,
    removeName,
    src,
    alt,
    stretch,
    onSubmit,
    ...rest
}) => {
    const refUploadInput = useRef()

    const handleClickUpload = () => {
        refUploadInput.current.value = ''
        refUploadInput.current.click()
    }

    const handleClickRemove = () => {
        onSubmit(removeName, true)
    }

    const handleSubmit = (file) => {
        const reader = new FileReader()
        reader.onloadend = () => {
            onSubmit(name, file)
        }
        reader.readAsDataURL(file)
    }

    const { t } = useTranslation()

    const { accept, maxSize } = FILE_TYPE.image

    return (
        <Wrapper $stretch={stretch}>
            {!!src && (
                <div className="UploadImage" {...rest}>
                    <img src={src} alt={alt} />
                    <IconButton
                        className="UploadImageRemoveButton"
                        size="small"
                        variant="secondary"
                        dropShadow
                        aria-label={t('form.delete-image')}
                        onClick={handleClickRemove}
                    >
                        <RemoveIcon />
                    </IconButton>
                </div>
            )}
            <FileInput
                ref={refUploadInput}
                name={name}
                accept={accept}
                maxSize={maxSize}
                onSubmit={handleSubmit}
                style={{ display: 'none' }}
            />
            <Button
                size="small"
                variant="secondary"
                onClick={handleClickUpload}
            >
                {t('form.upload-image')}
            </Button>
        </Wrapper>
    )
}

export default UploadImage
