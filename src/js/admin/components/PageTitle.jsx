import React, { Fragment } from 'react'
import { useMobileLandscapeUp } from 'helpers/breakpoints'
import styled from 'styled-components'

import { H3 } from 'js/components/Heading'
import HideVisually from 'js/components/HideVisually/HideVisually'

const Title = styled(H3)`
    > * {
        display: inline-block;

        &:not(:last-child):after {
            content: '  •  ';
            white-space: pre;
        }
    }
`

const PageTitle = ({ isVisible, titles, children, ...rest }) => {
    const Wrapper =
        useMobileLandscapeUp() && !isVisible ? HideVisually : Fragment

    return (
        <Wrapper>
            <Title as="h1" style={{ marginBottom: '16px' }} {...rest}>
                {titles?.map((title, i) => (
                    <span key={i}>{title}</span>
                ))}
                {children}
            </Title>
        </Wrapper>
    )
}

export default PageTitle
