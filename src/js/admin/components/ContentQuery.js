import { gql } from '@apollo/client'

const sharedFields = `
    accessId
    canEdit
    canArchiveAndDelete
    guid
    title
    url
    subtype
    timePublished
    owner {
        guid
        name
        url
    }
    group {
        guid
        name
        url
        __typename
    }
`

const sharedContentFields = `
    ${sharedFields}
    statusPublished
    timePublished
    timeUpdated
    lastSeen
`

const blogFragment = `fragment BlogFragment on Blog { ${sharedContentFields} }`
const discussionFragment = `fragment DiscussionFragment on Discussion { ${sharedContentFields} }`
const eventFragment = `fragment EventFragment on Event { ${sharedContentFields} hasChildren}`
const fileFragment = `fragment FileFragment on File { ${sharedFields} size mimeType }`
const folderFragment = `fragment FolderFragment on Folder { ${sharedFields} size hasChildren }`
const padFragment = `fragment PadFragment on Pad { ${sharedFields} }`
const newsFragment = `fragment NewsFragment on News { ${sharedContentFields} }`
const questionFragment = `fragment QuestionFragment on Question { ${sharedContentFields} canClose isClosed}`
const wikiFragment = `fragment WikiFragment on Wiki { ${sharedContentFields} hasChildren}`

export default gql`
    query AdminContent(
        $q: String!
        $limit: Int = 20
        $offset: Int = 0
        $subtypes: [String]
        $containerGuid: String
        $userGuid: String
        $statusPublished: [StatusPublished]
        $orderBy: OrderBy
        $orderDirection: OrderDirection
    ) {
        entities(
            q: $q
            limit: $limit
            orderBy: $orderBy
            orderDirection: $orderDirection
            subtypes: $subtypes
            containerGuid: $containerGuid
            offset: $offset
            userGuid: $userGuid
            statusPublished: $statusPublished
        ) {
            total
            edges {
                __typename
                ...BlogFragment
                ...DiscussionFragment
                ...EventFragment
                ...FileFragment
                ...FolderFragment
                ...PadFragment
                ...NewsFragment
                ...QuestionFragment
                ...WikiFragment
            }
        }
        groups(limit: 999) {
            edges {
                guid
                name
            }
        }
    }
    ${blogFragment}
    ${discussionFragment}
    ${eventFragment}
    ${fileFragment}
    ${folderFragment}
    ${padFragment}
    ${newsFragment}
    ${questionFragment}
    ${wikiFragment}
`
