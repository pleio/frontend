import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'

import Flexer from 'js/components/Flexer/Flexer'
import IconButton from 'js/components/IconButton/IconButton'
import Modal from 'js/components/Modal/Modal'
import Tag from 'js/components/Tag/Tag'

import AddIcon from 'icons/add.svg'

import AddSynonymForm from './AddSynonymForm'

const CustomTag = ({ tags, entity }) => {
    const { label, synonyms } = entity

    const [showSynonymModal, setShowSynonymModal] = useState(false)
    const toggleSynonymModal = () => {
        setShowSynonymModal(!showSynonymModal)
    }

    const [mutateExtractSynonym] = useMutation(
        gql`
            mutation ExtractSynonym($input: tagMergeInput!) {
                extractTagSynonym(input: $input) {
                    label
                    synonyms
                }
            }
        `,
        {
            refetchQueries: ['GetCustomTagsSiteSettings'],
        },
    )

    const { t } = useTranslation()

    return (
        <>
            <Flexer justifyContent="flex-start" gutter="tiny" wrap>
                <Tag>{label}</Tag>
                {synonyms.map((synonym) => {
                    const removeSynonym = () => {
                        mutateExtractSynonym({
                            variables: {
                                input: {
                                    tag: label,
                                    synonym,
                                },
                            },
                        })
                    }

                    return (
                        <Tag
                            key={`${label}-${synonym}`}
                            onRemove={removeSynonym}
                            tooltip={t('admin.remove-synonym')}
                        >
                            {synonym}
                        </Tag>
                    )
                })}
                <IconButton
                    size="small"
                    variant="secondary"
                    tooltip={t('admin.add-synonym')}
                    onClick={toggleSynonymModal}
                >
                    <AddIcon />
                </IconButton>
            </Flexer>
            <Modal
                isVisible={showSynonymModal}
                onClose={toggleSynonymModal}
                title={t('admin.add-synonym')}
                size="small"
            >
                <AddSynonymForm
                    entity={entity}
                    tags={tags}
                    onClose={toggleSynonymModal}
                />
            </Modal>
        </>
    )
}
export default CustomTag
