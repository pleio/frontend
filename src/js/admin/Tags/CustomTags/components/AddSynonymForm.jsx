import React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'

const AddSynonymForm = ({ entity, tags, onClose }) => {
    const { t } = useTranslation()

    const [mutateMergeTags] = useMutation(
        gql`
            mutation MergeTags($input: tagMergeInput!) {
                mergeTags(input: $input) {
                    label
                    synonyms
                }
            }
        `,
        {
            refetchQueries: ['GetCustomTagsSiteSettings'],
        },
    )

    const { label } = entity

    const submit = async ({ synonym }) => {
        await mutateMergeTags({
            variables: {
                input: {
                    tag: label,
                    synonym,
                },
            },
        })
            .then(() => {
                onClose()
            })
            .catch((error) => {
                console.error(error)
            })
    }

    const defaultValues = {
        synonym: '',
    }

    const {
        control,
        handleSubmit,
        watch,
        formState: { isSubmitting },
    } = useForm({
        defaultValues,
    })

    return (
        <form onSubmit={handleSubmit(submit)}>
            <FormItem
                type="select"
                control={control}
                options={tags
                    .filter((tag) => tag.label !== label)
                    .map((tag) => {
                        return {
                            label: tag.label,
                            value: tag.label,
                        }
                    })}
                name="synonym"
                label={t('form.tags')}
                isClearable
            />

            <Flexer mt>
                <Button
                    size="normal"
                    variant="tertiary"
                    type="button"
                    onClick={onClose}
                >
                    {t('action.cancel')}
                </Button>
                <Button
                    size="normal"
                    variant="primary"
                    type="submit"
                    disabled={!watch('synonym')}
                    loading={isSubmitting}
                >
                    {t('action.save')}
                </Button>
            </Flexer>
        </form>
    )
}

export default AddSynonymForm
