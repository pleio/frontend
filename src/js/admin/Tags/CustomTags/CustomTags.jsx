import React from 'react'
import { gql, useMutation, useQuery } from '@apollo/client'

import SettingContainer from 'js/admin/layout/SettingContainer'
import submitSetting from 'js/admin/lib/submitSetting'
import AnimatePresence from 'js/components/AnimatePresence/AnimatePresence'
import Switch from 'js/components/Form/FormSwitch'
import { Col, Container, Row } from 'js/components/Grid/Grid'
import Spacer from 'js/components/Spacer/Spacer'

import CustomTag from './components/CustomTag'

const CustomTags = () => {
    const { data } = useQuery(GET_SITE_SETTINGS)
    const [mutate] = useMutation(EDIT_SITE_SETTING)

    if (!data || !data.siteSettings) return null

    const {
        siteSettings: {
            customTagsAllowed,
            showCustomTagsInDetail,
            showCustomTagsInFeed,
        },
        tags,
    } = data

    const handleSubmitSetting = (key, value, resolve, reject) => {
        const refetchQueries = ['GetCustomTagsSiteSettings']
        submitSetting(mutate, key, value, refetchQueries, resolve, reject)
    }

    return (
        <Container size="normal">
            <Row>
                <Col tabletUp={1 / 2}>
                    <SettingContainer
                        title="global.custom-tags"
                        helper="admin.tags-custom-helper"
                        htmlFor="customTagsAllowed"
                    >
                        <Switch
                            name="customTagsAllowed"
                            checked={customTagsAllowed}
                            onChange={handleSubmitSetting}
                            releaseInputArea
                        />
                    </SettingContainer>
                </Col>

                <Col tabletUp={1 / 2}>
                    <AnimatePresence visible={customTagsAllowed}>
                        <SettingContainer
                            subtitle="admin.tags-show-in-feed"
                            htmlFor="showCustomTagsInFeed"
                            helper="admin.custom-tags-show-in-feed-helper"
                        >
                            <Switch
                                name="showCustomTagsInFeed"
                                checked={showCustomTagsInFeed}
                                onChange={handleSubmitSetting}
                                releaseInputArea
                                size="small"
                            />
                        </SettingContainer>

                        <SettingContainer
                            subtitle="admin.tags-show-in-item"
                            helper="admin.custom-tags-show-in-item-helper"
                            htmlFor="showCustomTagsInDetail"
                        >
                            <Switch
                                name="showCustomTagsInDetail"
                                checked={showCustomTagsInDetail}
                                onChange={handleSubmitSetting}
                                releaseInputArea
                                size="small"
                            />
                        </SettingContainer>
                    </AnimatePresence>
                </Col>
            </Row>

            {tags.length > 0 && (
                <AnimatePresence
                    visible={customTagsAllowed}
                    style={{ marginTop: 20 }}
                >
                    <SettingContainer
                        subtitle="admin.synonyms"
                        helper="admin.synonyms-helper"
                    />

                    <Spacer spacing="tiny">
                        {tags.map((tag) => (
                            <CustomTag
                                key={tag.label}
                                tags={tags}
                                entity={tag}
                            />
                        ))}
                    </Spacer>
                </AnimatePresence>
            )}
        </Container>
    )
}

const GET_SITE_SETTINGS = gql`
    query GetCustomTagsSiteSettings {
        siteSettings {
            customTagsAllowed
            showCustomTagsInFeed
            showCustomTagsInDetail
        }
        tags {
            label
            synonyms
        }
    }
`

const EDIT_SITE_SETTING = gql`
    mutation EditCustomTagsSiteSetting($input: editSiteSettingInput!) {
        editSiteSetting(input: $input) {
            siteSettings {
                customTagsAllowed
                showCustomTagsInFeed
                showCustomTagsInDetail
            }
        }
    }
`

export default CustomTags
