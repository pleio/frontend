import React, { useEffect, useRef, useState } from 'react'
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql, useMutation, useQuery } from '@apollo/client'
import { transparentize } from 'polished'
import styled from 'styled-components'

import SettingContainer from 'js/admin/layout/SettingContainer'
import submitSetting from 'js/admin/lib/submitSetting'
import Button from 'js/components/Button/Button'
import Errors from 'js/components/Errors'
import FormItem from 'js/components/Form/FormItem'
import Switch from 'js/components/Form/FormSwitch'
import { Col, Container, Row } from 'js/components/Grid/Grid'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import ConfirmationModal from 'js/components/Modal/ConfirmationModal'

import PageTagFilters from './components/PageTagFilters'
import TagCategory from './components/TagCategory'
import refetchQueries from './helpers/refetchQueries'

const Wrapper = styled.div`
    position: relative;
    overflow: hidden;
    margin: 0 -20px;

    .TagCategoriesLoading {
        position: absolute;
        left: 0;
        top: 0;
        bottom: 0;
        right: 0;
        background-color: ${transparentize(0.35, 'white')};
        display: flex;
        justify-content: center;
        align-items: center;
        z-index: 1;
    }

    .TagCategoriesOverflow {
        overflow-x: auto;
    }

    .TagCategoriesLeftShadow {
        position: absolute;
        left: -100px;
        top: 0;
        bottom: 0;
        width: 100px;
        z-index: 1;
        transition: box-shadow ${(p) => p.theme.transition.normal};
    }

    .TagCategoriesRightShadow {
        position: absolute;
        right: -100px;
        top: 0;
        bottom: 0;
        width: 100px;
        z-index: 1;
        transition: box-shadow ${(p) => p.theme.transition.normal};
    }
`

const CategoryTags = () => {
    const { t } = useTranslation()

    const refScrollWindow = useRef()
    const refLeftShadow = useRef()
    const refRightShadow = useRef()
    const refEnd = useRef()

    const setShadows = (scrollWindow) => {
        if (!scrollWindow) return

        const scrollDistanceLeft =
            scrollWindow.scrollWidth -
            (scrollWindow.scrollLeft + scrollWindow.offsetWidth)

        setLeftShadow(scrollWindow.scrollLeft)
        setRightShadow(scrollDistanceLeft)
    }

    const [editSiteSetting] = useMutation(EDIT_SITE_SETTING)
    const [addTagCategory, { error: addError }] = useMutation(ADD_TAG_CATEGORY)

    const [showAddTagCategoryModal, setShowAddTagCategoryModal] =
        useState(false)

    const {
        control,
        handleSubmit,
        reset,
        formState: { isValid, isSubmitting },
    } = useForm({ defaultValues: { name: '' } })

    useEffect(() => {
        setShadows(refScrollWindow.current)
    }, [])

    const [updateTagCategoryTag, { loading: loadingUpdateTag }] = useMutation(
        UPDATE_TAG_CATEGORY_TAG,
    )

    const [updateTagCategory, { loading: loadingUpdateTagCategory }] =
        useMutation(UPDATE_TAG_CATEGORY)

    const { data } = useQuery(GET_SITE_TAGS_SETTINGS)

    if (!data || !data.siteSettings) return null

    const {
        siteSettings: { tagCategorySettings, showTagsInDetail, showTagsInFeed },
    } = data

    const tagCategories = tagCategorySettings

    const handleAddTagCategory = async ({ name }) => {
        await addTagCategory({
            variables: {
                input: {
                    name,
                },
            },
            refetchQueries,
        })
            .then(() => {
                setShowAddTagCategoryModal(false)
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    const handleSubmitSetting = (key, value, resolve, reject) => {
        const refetchQueries = ['SiteTagsSettings']
        submitSetting(
            editSiteSetting,
            key,
            value,
            refetchQueries,
            resolve,
            reject,
        )
    }

    const onDragEnd = (result) => {
        if (!result.destination) return

        const sourceIndex = result.source.index
        const destinationIndex = result.destination.index

        const { type } = result

        if (type.indexOf('TAG') !== -1) {
            if (sourceIndex === destinationIndex) return

            const source = JSON.parse(result.source.droppableId)
            const category = tagCategories[source.index]
            const tag = tagCategories[source.index].values[sourceIndex]

            updateTagCategoryTag({
                variables: {
                    name: category.name,
                    value: tag.value,
                    input: {
                        value: tag.value,
                        synonyms: tag.synonyms, // Must be passed or else it will be removed
                        weight: destinationIndex,
                    },
                },
                refetchQueries,
            }).catch((errors) => {
                console.error(errors)
            })
        }

        if (type === 'CATEGORY') {
            if (sourceIndex === destinationIndex) return

            const category = tagCategories[sourceIndex]

            updateTagCategory({
                variables: {
                    name: category.name,
                    input: {
                        weight: destinationIndex,
                    },
                },
                refetchQueries,
            }).catch((errors) => {
                console.error(errors)
            })
        }
    }

    const setLeftShadow = (scrollLeft) => {
        if (scrollLeft > 0)
            refLeftShadow.current.style.boxShadow =
                '1px 0 6px 0 rgba(0, 0, 0, 0.075), 20px 0 20px -15px rgba(0, 0, 0, 0.075)'
        if (scrollLeft === 0) refLeftShadow.current.style.boxShadow = ''
    }

    const setRightShadow = (scrollDistanceLeft) => {
        if (scrollDistanceLeft >= 5)
            refRightShadow.current.style.boxShadow =
                '-1px 0 6px 0 rgba(0, 0, 0, 0.075), -20px 0 20px -15px rgba(0, 0, 0, 0.075)'
        if (scrollDistanceLeft < 5) refRightShadow.current.style.boxShadow = ''
    }

    const handleScroll = (evt) => {
        setShadows(evt.target)
    }

    const isLoading = loadingUpdateTag || loadingUpdateTagCategory

    return (
        <>
            <Container size="normal" style={{ marginBottom: '24px' }}>
                <Row>
                    <Col tabletUp={1 / 2}>
                        <SettingContainer
                            title="admin.category-tags"
                            helper="admin.tags-helper"
                        />
                    </Col>
                    <Col tabletUp={1 / 2}>
                        <SettingContainer
                            subtitle="admin.tags-show-in-feed"
                            helper="admin.tags-show-in-feed-helper"
                            htmlFor="showTagsInFeed"
                        >
                            <Switch
                                name="showTagsInFeed"
                                checked={showTagsInFeed}
                                onChange={handleSubmitSetting}
                                releaseInputArea
                                size="small"
                            />
                        </SettingContainer>
                        <SettingContainer
                            subtitle="admin.tags-show-in-item"
                            helper="admin.tags-show-in-item-helper"
                            htmlFor="showTagsInDetail"
                        >
                            <Switch
                                name="showTagsInDetail"
                                checked={showTagsInDetail}
                                onChange={handleSubmitSetting}
                                releaseInputArea
                                size="small"
                            />
                        </SettingContainer>
                    </Col>
                </Row>
            </Container>
            <Wrapper>
                {isLoading && (
                    <div className="TagCategoriesLoading">
                        <LoadingSpinner />
                    </div>
                )}
                <div ref={refLeftShadow} className="TagCategoriesLeftShadow" />
                <div
                    ref={refRightShadow}
                    className="TagCategoriesRightShadow"
                />
                <div
                    ref={refScrollWindow}
                    className="TagCategoriesOverflow"
                    onScroll={handleScroll}
                >
                    <Container
                        style={{
                            maxWidth: 'none',
                            padding: '16px 10px 12px',
                            display: 'flex',
                        }}
                    >
                        <DragDropContext onDragEnd={onDragEnd}>
                            <Droppable
                                droppableId="droppable"
                                type="CATEGORY"
                                direction="horizontal"
                            >
                                {(provided) => (
                                    <div
                                        ref={provided.innerRef}
                                        style={{
                                            display: 'inline-flex',
                                            margin: '0 auto',
                                        }}
                                        {...provided.droppableProps}
                                    >
                                        {tagCategories.map((entity, index) => (
                                            <Draggable
                                                key={`${entity.name}-${index}`}
                                                draggableId={`draggable-${index}`}
                                                index={index}
                                            >
                                                {(provided) => (
                                                    <div
                                                        ref={provided.innerRef}
                                                        {...provided.draggableProps}
                                                        style={{
                                                            width:
                                                                tagCategories.length >
                                                                2
                                                                    ? '280px'
                                                                    : '310px',
                                                            flexShrink: 0,
                                                            padding: '0 10px',
                                                            ...provided
                                                                .draggableProps
                                                                .style,
                                                        }}
                                                    >
                                                        <TagCategory
                                                            index={index}
                                                            entity={entity}
                                                            dragHandleProps={
                                                                provided.dragHandleProps
                                                            }
                                                        />
                                                    </div>
                                                )}
                                            </Draggable>
                                        ))}
                                        {provided.placeholder}
                                        <div ref={refEnd} />
                                    </div>
                                )}
                            </Droppable>
                        </DragDropContext>
                    </Container>
                </div>
            </Wrapper>

            <Container size="normal">
                <Button
                    size="small"
                    variant="tertiary"
                    disabled={tagCategories.length >= 9}
                    onClick={() => {
                        reset()
                        setShowAddTagCategoryModal(true)
                    }}
                    style={{ marginTop: '12px' }}
                >
                    {t('admin.add-tag-category')}
                </Button>

                <ConfirmationModal
                    title={t('admin.add-tag-category')}
                    isVisible={showAddTagCategoryModal}
                    cancelLabel={t('action.cancel')}
                    confirmLabel={t('action.confirm')}
                    onConfirm={() => handleSubmit(handleAddTagCategory)()}
                    onCancel={() => setShowAddTagCategoryModal(false)}
                    loading={isSubmitting}
                    disabled={!isValid}
                >
                    <FormItem
                        control={control}
                        type="text"
                        name="name"
                        label={t('global.name')}
                        required
                    />

                    <Errors errors={addError} />
                </ConfirmationModal>

                <PageTagFilters />
            </Container>
        </>
    )
}

const tagCategorySettingsTemplate = `
    tagCategorySettings {
        name
        values {
            value
            synonyms
        }
    }
`

const GET_SITE_TAGS_SETTINGS = gql`
    query SiteTagsSettings {
        siteSettings {
            showTagsInFeed
            showTagsInDetail
            ${tagCategorySettingsTemplate}
        }
    }
`
const EDIT_SITE_SETTING = gql`
    mutation EditSiteSetting($input: editSiteSettingInput!) {
        editSiteSetting(input: $input) {
            siteSettings {
                showTagsInFeed
                showTagsInDetail
                ${tagCategorySettingsTemplate}
            }
        }
    }
`

const ADD_TAG_CATEGORY = gql`
    mutation addTagCategory($input: MutateTagCategoryInput!) {
        addTagCategory(input: $input) {
            siteSettings {
                ${tagCategorySettingsTemplate}
            }
        }
    }
`

const UPDATE_TAG_CATEGORY_TAG = gql`
    mutation updateTagCategoryTag(
        $name: String!
        $value: String!
        $input: MutateTagCategoryTagInput!
    ) {
        updateTagCategoryTag(name: $name, value: $value, input: $input) {
            siteSettings {
                ${tagCategorySettingsTemplate}
            }
        }
    }
`

const UPDATE_TAG_CATEGORY = gql`
    mutation updateTagCategory(
        $name: String!
        $input: MutateTagCategoryInput!
    ) {
        updateTagCategory(name: $name, input: $input) {
            siteSettings {
                ${tagCategorySettingsTemplate}
            }
        }
    }
`

export default CategoryTags
