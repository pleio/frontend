import React, { useEffect, useState } from 'react'
import { useFieldArray, useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql, useMutation, useQuery } from '@apollo/client'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import Section from 'js/components/Section/Section'
import Setting from 'js/components/Setting'
import TabMenu from 'js/components/TabMenu'
import TabPage from 'js/components/TabPage'

const PageTagFilters = () => {
    const { t } = useTranslation()

    const [tab, setTab] = useState('news')

    const { data, loading } = useQuery(GET_SITE_PAGE_TAG_FILTERS)

    const {
        control,
        handleSubmit,
        watch,
        reset,
        formState: { isDirty, isSubmitting },
    } = useForm()

    useEffect(() => {
        if (!loading && data?.siteSettings?.pageTagFilters?.length > 0) {
            // Set initial form values
            reset({
                pageTagFilters: data?.siteSettings?.pageTagFilters.map(
                    ({ contentType, showTagFilter, showTagCategories }) => {
                        return {
                            contentType,
                            showTagFilter,
                            showTagCategories,
                        }
                    },
                ),
            })
        }
    }, [loading])

    const { fields } = useFieldArray({
        control,
        name: 'pageTagFilters',
    })

    const [mutate] = useMutation(EDIT_SITE_SETTING)

    const submit = async ({ pageTagFilters }) => {
        await mutate({
            variables: {
                input: {
                    pageTagFilters,
                },
            },
            refetchQueries: ['SitePageTagFilters'],
        })
            .then(() => {
                reset({}, { keepValues: true })
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    if (data?.site?.tagCategories?.length === 0) {
        return null
    }

    return (
        <Section divider>
            <Setting
                subtitle="admin.tag-filters"
                helper="admin.tag-filters-helper"
            />

            <TabMenu
                label={t('admin.tag-filters')}
                options={
                    data
                        ? data?.siteSettings?.pageTagFilters?.map(
                              ({ contentType }) => {
                                  return {
                                      onClick: () => setTab(contentType),
                                      isActive: tab === contentType,
                                      label: t(
                                          `entity-${contentType}.title-list`,
                                      ),
                                      id: `tab-${contentType}`,
                                      ariaControls: `panel-${contentType}`,
                                  }
                              },
                          )
                        : []
                }
                showBorder
                style={{ marginTop: '8px' }}
            />

            <form onSubmit={handleSubmit(submit)}>
                {fields.map(({ contentType }, index) => (
                    <TabPage
                        key={`${contentType}-${index}`}
                        id={`panel-${contentType}`}
                        ariaLabelledby={`tab-${contentType}`}
                        visible={tab === contentType}
                        style={{ padding: '8px 0' }}
                    >
                        <FormItem
                            control={control}
                            type="switch"
                            name={`pageTagFilters[${index}].showTagFilter`}
                            id={`pageTagFilters[${index}].showTagFilter`}
                            label={t('widget-feed.all-tag-categories')}
                            size="small"
                        />

                        <FormItem
                            type="checkboxes"
                            options={data.site.tagCategories.map((el) => ({
                                label: el.name,
                                value: el.name,
                                disabled: watch(
                                    `pageTagFilters[${index}].showTagFilter`,
                                ),
                                checked: watch(
                                    `pageTagFilters[${index}].showTagFilter`,
                                ),
                            }))}
                            control={control}
                            name={`pageTagFilters[${index}].showTagCategories`}
                            id={`pageTagFilters[${index}].showTagCategories`}
                            size="small"
                            style={{
                                marginLeft: '30px',
                            }}
                        />
                    </TabPage>
                ))}

                <Flexer>
                    <Button
                        size="normal"
                        variant="primary"
                        type="submit"
                        disabled={!isDirty}
                        loading={isSubmitting}
                    >
                        {t('action.save')}
                    </Button>
                </Flexer>
            </form>
        </Section>
    )
}

const EDIT_SITE_SETTING = gql`
    mutation EditSiteSetting($input: editSiteSettingInput!) {
        editSiteSetting(input: $input) {
            siteSettings {
                pageTagFilters {
                    contentType
                    showTagFilter
                    showTagCategories
                }
            }
        }
    }
`

const GET_SITE_PAGE_TAG_FILTERS = gql`
    query SitePageTagFilters {
        site {
            guid
            tagCategories {
                name
                values
            }
        }
        siteSettings {
            pageTagFilters {
                contentType
                showTagFilter
                showTagCategories
            }
        }
    }
`

export default PageTagFilters
