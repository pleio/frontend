import React, { useState } from 'react'
import { Draggable, Droppable } from 'react-beautiful-dnd'
import { useForm } from 'react-hook-form'
import { Trans, useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import Card from 'js/components/Card/Card'
import Errors from 'js/components/Errors'
import FormItem from 'js/components/Form/FormItem'
import IconButton from 'js/components/IconButton/IconButton'
import ConfirmationModal from 'js/components/Modal/ConfirmationModal'
import RadioFields from 'js/components/RadioField/RadioFields'
import Textfield from 'js/components/Textfield/Textfield'

import DeleteIcon from 'icons/delete.svg'
import MoveIcon from 'icons/move-horizontal.svg'

import refetchQueries from '../helpers/refetchQueries'

import CategoryTag from './CategoryTag'

const Wrapper = styled(Card)`
    padding: 20px 0 16px;
    background-color: white;

    .TagCategory {
        display: flex;
        margin-bottom: 20px;
    }

    .TagTags {
        border-top: 1px solid ${(p) => p.theme.color.grey[30]};
        padding-top: 20px;
    }
`

const TagCategory = ({ index, entity, dragHandleProps }) => {
    const { t } = useTranslation()

    const { name } = entity

    const {
        control,
        handleSubmit,
        reset,
        formState: { isValid, isSubmitting },
    } = useForm({ defaultValues: { value: '' } })

    const [extendTagCategory, { error: addError }] =
        useMutation(EXTEND_TAG_CATEGORY)

    const [showAddTagModal, setShowAddTagModal] = useState(false)

    const handleAddTag = async ({ value }) => {
        await extendTagCategory({
            variables: {
                name,
                input: {
                    value,
                },
            },
            refetchQueries,
        })
            .then(() => {
                setShowAddTagModal(false)
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    const [deleteTagCategory, { loading: deleteLoading, error: deleteError }] =
        useMutation(DELETE_TAG_CATEGORY)

    const [showDeleteTagCategoryModal, setShowDeleteTagCategoryModal] =
        useState(false)
    const [keepTags, setKeepTags] = useState(true)

    const handleClickRemove = () => {
        deleteTagCategory({
            variables: {
                name,
                keepTags,
            },
            refetchQueries,
        }).then(() => {
            setShowDeleteTagCategoryModal(false)
        })
    }

    const [nameInput, setNameInput] = useState(name)

    const handleChange = (evt) => setNameInput(evt.target.value)
    const [newName, setNewName] = useState() // Extra state to keep showing the new value while closing the rename modal
    const [showRenameModal, setShowRenameModal] = useState(false)
    const [updateTagCategory, { loading: renameLoading, error: renameError }] =
        useMutation(UPDATE_TAG_CATEGORY)
    const handleRename = () => {
        updateTagCategory({
            variables: {
                name,
                input: {
                    name: nameInput,
                },
            },
            refetchQueries,
        }).then(() => {
            setShowRenameModal(false)
        })
    }

    const defaultButtonProps = {
        size: 'large',
        hoverSize: 'normal',
        variant: 'secondary',
        radiusStyle: 'rounded',
    }

    return (
        <Wrapper>
            <div className="TagCategory">
                <IconButton
                    as="div"
                    {...defaultButtonProps}
                    {...dragHandleProps}
                >
                    <MoveIcon />
                </IconButton>
                <Textfield
                    name={`category-${index}`}
                    label={t('admin.tags-category')}
                    value={nameInput}
                    onChange={handleChange}
                    onBlur={() => {
                        if (!nameInput) {
                            setNameInput(name)
                        } else if (nameInput !== name) {
                            setNewName(nameInput)
                            setShowRenameModal(true)
                        }
                    }}
                />
                <IconButton
                    {...defaultButtonProps}
                    tooltip={t('admin.delete-tag-category')}
                    onClick={() => setShowDeleteTagCategoryModal(true)}
                >
                    <DeleteIcon />
                </IconButton>
            </div>
            <div className="TagTags">
                <Droppable
                    droppableId={JSON.stringify({
                        type: 'droppable',
                        index,
                    })}
                    type={`TAG-${index}`}
                >
                    {(provided) => (
                        <div ref={provided.innerRef}>
                            {entity.values.map((tag, childIndex) => {
                                if (!tag) return null
                                const { value } = tag

                                return (
                                    <Draggable
                                        key={`tags-${index}-${childIndex}-${value}`}
                                        draggableId={`draggable-${index}-${childIndex}`}
                                        index={childIndex}
                                    >
                                        {(provided) => (
                                            <div
                                                ref={provided.innerRef}
                                                {...provided.draggableProps}
                                                style={{
                                                    marginBottom: '16px',
                                                    ...provided.draggableProps
                                                        .style,
                                                }}
                                            >
                                                <CategoryTag
                                                    name={`tags-${index}-${childIndex}`}
                                                    categoryName={name}
                                                    entity={tag}
                                                    dragHandleProps={
                                                        provided.dragHandleProps
                                                    }
                                                />
                                            </div>
                                        )}
                                    </Draggable>
                                )
                            })}
                            {provided.placeholder}
                        </div>
                    )}
                </Droppable>

                <div style={{ margin: '0 40px' }}>
                    <Button
                        size="small"
                        variant="tertiary"
                        onClick={() => {
                            reset()
                            setShowAddTagModal(true)
                        }}
                    >
                        {t('admin.add-tag')}
                    </Button>
                </div>
            </div>

            <ConfirmationModal
                title={t('admin.add-tag')}
                isVisible={showAddTagModal}
                cancelLabel={t('action.cancel')}
                confirmLabel={t('action.confirm')}
                onConfirm={() => handleSubmit(handleAddTag)()}
                onCancel={() => setShowAddTagModal(false)}
                loading={isSubmitting}
                disabled={!isValid}
            >
                <FormItem
                    control={control}
                    type="text"
                    name="value"
                    label={t('global.name')}
                    required
                />

                <Errors errors={addError} />
            </ConfirmationModal>

            <ConfirmationModal
                title={t('admin.delete-tag-category')}
                isVisible={showDeleteTagCategoryModal}
                onConfirm={handleClickRemove}
                onCancel={() => setShowDeleteTagCategoryModal(false)}
                loading={deleteLoading}
            >
                <Trans i18nKey="admin.delete-tag-category-confirm">
                    Are you sure you want to delete the category{' '}
                    <strong>{{ name }}</strong>?<br />
                    <br />
                    <strong>Warning:</strong> All tags in this category will be
                    deleted.
                </Trans>
                Convert these tags to custom tags on all content where they are
                applied.
                <RadioFields
                    name="removeFromContent"
                    legend={t('global.type')}
                    options={[
                        {
                            value: 'convert',
                            label: t(
                                'admin.delete-tag-category-convert-to-custom',
                            ),
                        },
                        {
                            value: 'remove',
                            label: t(
                                'admin.delete-tag-category-remove-from-content',
                            ),
                        },
                    ]}
                    value={keepTags ? 'convert' : 'remove'}
                    onChange={({ target }) => {
                        setKeepTags(target.value === 'convert')
                    }}
                    size="small"
                    style={{ marginTop: '16px' }}
                />
                <Errors errors={deleteError} />
            </ConfirmationModal>

            <ConfirmationModal
                title={t('admin.rename-tag-category')}
                isVisible={showRenameModal}
                onConfirm={handleRename}
                onCancel={() => {
                    setNameInput(name)
                    setShowRenameModal(false)
                }}
                loading={renameLoading}
            >
                <Trans i18nKey="admin.rename-tag-category-confirm">
                    Are you sure you want to rename the category{' '}
                    <strong>{{ name }}</strong> to{' '}
                    <strong>{{ newName }}</strong>?
                </Trans>
                <Errors errors={renameError} />
            </ConfirmationModal>
        </Wrapper>
    )
}

const tagCategorySettingsTemplate = `
    tagCategorySettings {
        name
        values {
            value
            synonyms
        }
    }
`

const DELETE_TAG_CATEGORY = gql`
    mutation deleteTagCategory($name: String!, $keepTags: Boolean) {
        deleteTagCategory(name: $name, keepTags: $keepTags) {
            siteSettings {
                ${tagCategorySettingsTemplate}
            }
        }
    }
`

const EXTEND_TAG_CATEGORY = gql`
    mutation extendTagCategory(
        $name: String!
        $input: MutateTagCategoryTagInput!
    ) {
        extendTagCategory(name: $name, input: $input) {
            siteSettings {
                ${tagCategorySettingsTemplate}
            }
        }
    }
`

const UPDATE_TAG_CATEGORY = gql`
    mutation updateTagCategory(
        $name: String!
        $input: MutateTagCategoryInput!
    ) {
        updateTagCategory(name: $name, input: $input) {
            siteSettings {
                ${tagCategorySettingsTemplate}
            }
        }
    }
`

export default TagCategory
