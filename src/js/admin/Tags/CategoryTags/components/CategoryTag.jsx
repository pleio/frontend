import React, { useRef, useState } from 'react'
import { Trans, useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'
import styled from 'styled-components'

import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import IconButton from 'js/components/IconButton/IconButton'
import ConfirmationModal from 'js/components/Modal/ConfirmationModal'
import Popover from 'js/components/Popover/Popover'
import RadioFields from 'js/components/RadioField/RadioFields'
import Tag from 'js/components/Tag/Tag'
import Textfield from 'js/components/Textfield/Textfield'

import AddIcon from 'icons/add.svg'
import CheckIcon from 'icons/check.svg'
import DeleteIcon from 'icons/delete.svg'
import MoveIcon from 'icons/move-vertical.svg'

import refetchQueries from '../helpers/refetchQueries'

const Wrapper = styled.div`
    display: flex;
`

const CategoryTag = ({ name, entity, categoryName, dragHandleProps }) => {
    const { value, synonyms } = entity

    const refAddSynonymInput = useRef()

    const [instance, setInstance] = useState(null)

    const [
        deleteTagCategoryTag,
        { loading: deleteLoading, error: deleteError },
    ] = useMutation(DELETE_TAG_CATEGORY_TAG)

    const [showDeleteModal, setShowDeleteModal] = useState(false)

    const [keepTag, setKeepTag] = useState(true)

    const handleDelete = () => {
        deleteTagCategoryTag({
            variables: {
                name: categoryName,
                value,
                keepTag,
            },
            refetchQueries,
        }).then(() => {
            setShowDeleteModal(false)
        })
    }

    const [nameValue, setNameValue] = useState(value)
    const handleChangeName = (evt) => setNameValue(evt.target.value)
    const [showRenameModal, setShowRenameModal] = useState(false)

    const [
        updateTagCategoryTag,
        { loading: updateLoading, error: renameError },
    ] = useMutation(UPDATE_TAG_CATEGORY_TAG)

    const handleRename = () => {
        updateTagCategoryTag({
            variables: {
                name: categoryName,
                value,
                input: {
                    value: nameValue,
                    synonyms, // Must be passed or else it will be removed
                },
            },
            refetchQueries,
        }).then(() => {
            setShowRenameModal(false)
        })
    }

    const [addSynonymValue, setAddSynonymValue] = useState('')
    const handleChangeSynonym = (evt) => setAddSynonymValue(evt.target.value)

    const updateSynonyms = (synonyms) => {
        updateTagCategoryTag({
            variables: {
                name: categoryName,
                value,
                input: {
                    synonyms,
                },
            },
            refetchQueries,
        })
            .then(() => {
                setAddSynonymValue('')
                instance.hide()
            })
            .catch((error) => {
                console.error(error)
            })
    }

    const addSynonym = () => {
        const newSynonyms = [...synonyms, addSynonymValue]
        updateSynonyms(newSynonyms)
    }

    const removeSynonym = (i) => {
        const newSynonyms = [...synonyms]
        newSynonyms.splice(i, 1)
        updateSynonyms(newSynonyms)
    }

    const { t } = useTranslation()

    const defaultButtonProps = {
        size: 'large',
        hoverSize: 'normal',
        variant: 'secondary',
        radiusStyle: 'rounded',
    }

    return (
        <Wrapper>
            <IconButton as="div" {...defaultButtonProps} {...dragHandleProps}>
                <MoveIcon />
            </IconButton>
            <div style={{ flexGrow: 1 }}>
                <Textfield
                    name={`${name}-name`}
                    label={t('admin.tags-tag')}
                    value={nameValue}
                    onChange={handleChangeName}
                    onBlur={() => {
                        if (!nameValue) {
                            setNameValue(value)
                        } else if (nameValue !== value) {
                            setShowRenameModal(true)
                        }
                    }}
                    ElementAfter={
                        <Popover
                            onCreate={setInstance}
                            onShow={() => {
                                setTimeout(() => {
                                    refAddSynonymInput?.current?.focus()
                                }, 0)
                            }}
                            content={
                                <Flexer
                                    alignItems="stretch"
                                    gutter="none"
                                    divider="large"
                                    as="form"
                                    onSubmit={(e) => {
                                        e.preventDefault()
                                        addSynonym()
                                    }}
                                >
                                    <Textfield
                                        ref={refAddSynonymInput}
                                        name={`${name}-synonym`}
                                        value={addSynonymValue}
                                        onChange={handleChangeSynonym}
                                        placeholder={t('admin.synonym')}
                                        borderStyle="none"
                                        disabled={updateLoading}
                                    />
                                    <IconButton
                                        type="submit"
                                        size="large"
                                        variant={
                                            addSynonymValue
                                                ? 'primary'
                                                : 'tertiary'
                                        }
                                        radiusStyle="none"
                                        tooltip={t('action.confirm')}
                                        disabled={!addSynonymValue}
                                        loading={updateLoading}
                                    >
                                        <CheckIcon />
                                    </IconButton>
                                </Flexer>
                            }
                            overflowHidden
                            placement="top"
                            offset={[0, 0]}
                        >
                            <IconButton
                                {...defaultButtonProps}
                                tooltip={t('admin.add-synonym')}
                            >
                                <AddIcon />
                            </IconButton>
                        </Popover>
                    }
                />

                {synonyms.length > 0 && (
                    <Flexer
                        gutter="tiny"
                        justifyContent="flex-start"
                        wrap
                        style={{ marginTop: '8px' }}
                    >
                        {synonyms.map((synonym, i) => (
                            <Tag
                                key={`${name}-synonym-${i}`}
                                onRemove={() => removeSynonym(i)}
                                disabled={updateLoading}
                                tooltip={t('admin.remove-synonym')}
                            >
                                {synonym}
                            </Tag>
                        ))}
                    </Flexer>
                )}
            </div>
            <IconButton
                {...defaultButtonProps}
                tooltip={t('admin.delete-tag')}
                onClick={() => setShowDeleteModal(true)}
            >
                <DeleteIcon />
            </IconButton>

            <ConfirmationModal
                title={t('admin.delete-tag')}
                isVisible={showDeleteModal}
                onConfirm={handleDelete}
                onCancel={() => setShowDeleteModal(false)}
                loading={deleteLoading}
            >
                <Trans i18nKey="admin.delete-tag-confirm">
                    Are you sure you want to delete the tag{' '}
                    <strong>{{ value }}</strong>?
                </Trans>
                <RadioFields
                    name="removeFromContent"
                    legend={t('global.type')}
                    options={[
                        {
                            value: 'convert',
                            label: t('admin.delete-tag-convert-to-custom'),
                        },
                        {
                            value: 'remove',
                            label: t('admin.delete-tag-remove-from-content'),
                        },
                    ]}
                    value={keepTag ? 'convert' : 'remove'}
                    onChange={({ target }) => {
                        setKeepTag(target.value === 'convert')
                    }}
                    size="small"
                    style={{ marginTop: '16px' }}
                />
                <Errors errors={deleteError} />
            </ConfirmationModal>

            <ConfirmationModal
                title={t('admin.rename-tag')}
                isVisible={showRenameModal}
                onConfirm={handleRename}
                onCancel={() => {
                    setNameValue(value)
                    setShowRenameModal(false)
                }}
                loading={updateLoading}
            >
                <Trans i18nKey="admin.rename-tag-confirm">
                    Are you sure you want to rename the tag{' '}
                    <strong>{{ value }}</strong> to{' '}
                    <strong>{{ newValue: nameValue }}</strong>? The tag will be
                    updated everywhere it is used.
                </Trans>
                <Errors errors={renameError} />
            </ConfirmationModal>
        </Wrapper>
    )
}

const tagCategorySettingsTemplate = `
    tagCategorySettings {
        name
        values {
            value
            synonyms
        }
    }
`

const DELETE_TAG_CATEGORY_TAG = gql`
    mutation deleteTagCategoryTag(
        $name: String!
        $value: String!
        $keepTag: Boolean
    ) {
        deleteTagCategoryTag(name: $name, value: $value, keepTag: $keepTag) {
            siteSettings {
                ${tagCategorySettingsTemplate}
            }
        }
    }
`

const UPDATE_TAG_CATEGORY_TAG = gql`
    mutation updateTagCategoryTag(
        $name: String!
        $value: String!
        $input: MutateTagCategoryTagInput!
    ) {
        updateTagCategoryTag(name: $name, value: $value, input: $input) {
            siteSettings {
                ${tagCategorySettingsTemplate}
            }
        }
    }
`

export default CategoryTag
