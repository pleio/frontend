import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'

import PageTitle from 'js/admin/components/PageTitle'
import Document from 'js/components/Document'
import { Container } from 'js/components/Grid/Grid'
import Section from 'js/components/Section/Section'
import TabMenu from 'js/components/TabMenu'
import TabPage from 'js/components/TabPage'

import CategoryTags from './CategoryTags/CategoryTags'
import CustomTags from './CustomTags/CustomTags'

const Tags = () => {
    const { t } = useTranslation()

    const [tab, setTab] = useState('category')

    return (
        <>
            <Document
                title={t('admin.tags')}
                containerTitle={t('admin.title')}
            />
            <Section divider backgroundColor="white">
                <Container size="normal">
                    <PageTitle>{t('admin.tags')}</PageTitle>

                    <TabMenu
                        label={t('admin.tags')}
                        options={[
                            {
                                onClick: () => setTab('category'),
                                isActive: tab === 'category',
                                label: t('admin.category-tags'),
                                id: 'tab-category',
                                ariaControls: 'panel-category',
                            },
                            {
                                onClick: () => setTab('custom'),
                                isActive: tab === 'custom',
                                label: t('global.custom-tags'),
                                id: 'tab-custom',
                                ariaControls: 'panel-custom',
                            },
                        ]}
                        showBorder
                        sticky
                    />
                </Container>

                <TabPage
                    visible={tab === 'category'}
                    style={{ padding: '24px 0' }}
                    id="panel-category"
                    ariaLabelledby="tab-category"
                >
                    <CategoryTags />
                </TabPage>
                <TabPage
                    visible={tab === 'custom'}
                    style={{ padding: '24px 0' }}
                    id="panel-custom"
                    ariaLabelledby="tab-custom"
                >
                    <CustomTags />
                </TabPage>
            </Section>
        </>
    )
}

export default Tags
