import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { FastField, Form, Formik } from 'formik'
import compose from 'lodash.flowright'

import SettingContainer from 'js/admin/layout/SettingContainer'
import submitSetting from 'js/admin/lib/submitSetting'
import AnimatePresence from 'js/components/AnimatePresence/AnimatePresence'
import Textarea from 'js/components/Form/FormikTextarea'
import Input from 'js/components/Form/FormikTextfield'
import Switch from 'js/components/Form/FormSwitch'
import { Col, Container, Row } from 'js/components/Grid/Grid'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'

const PeriodicOverview = ({ data, mutate }) => {
    const { t } = useTranslation()

    if (data.loading) return <LoadingSpinner />
    if (!data?.siteSettings) return null
    const { siteSettings } = data

    const handleSubmitSetting = (key, value, resolve, reject) => {
        const refetchQueries = ['SitePeriodicOverviewSettings']

        submitSetting(mutate, key, value, refetchQueries, resolve, reject)
    }

    const initialValues = {
        emailOverviewSubject: siteSettings.emailOverviewSubject,
        emailOverviewTitle: siteSettings.emailOverviewTitle,
        emailOverviewIntro: siteSettings.emailOverviewIntro,
        emailOverviewFeaturedTitle: siteSettings.emailOverviewFeaturedTitle,
    }

    return (
        <Container size="small">
            <Formik initialValues={initialValues}>
                <Form>
                    <SettingContainer
                        title="admin.email-template"
                        inputWidth="full"
                    />
                    <div style={{ marginBottom: '24px' }}>
                        <Row $spacing={16}>
                            <Col mobileLandscapeUp={1 / 2}>
                                <FastField
                                    name="emailOverviewSubject"
                                    component={Input}
                                    label={t('admin.content-email-subject')}
                                    onSubmit={handleSubmitSetting}
                                />
                            </Col>
                            <Col mobileLandscapeUp={1 / 2}>
                                <FastField
                                    name="emailOverviewTitle"
                                    component={Input}
                                    label={t('form.title')}
                                    onSubmit={handleSubmitSetting}
                                />
                            </Col>
                            <Col mobileLandscapeUp={1 / 2}>
                                <FastField
                                    name="emailOverviewIntro"
                                    component={Textarea}
                                    label={t('form.introduction')}
                                    size="small"
                                    onSubmit={handleSubmitSetting}
                                />
                            </Col>
                        </Row>
                    </div>
                    <SettingContainer
                        subtitle="admin.content-email-featured"
                        helper="admin.content-email-featured-helper"
                        htmlFor="emailOverviewEnableFeatured"
                    >
                        <Switch
                            name="emailOverviewEnableFeatured"
                            checked={siteSettings.emailOverviewEnableFeatured}
                            onChange={handleSubmitSetting}
                            releaseInputArea
                        />
                    </SettingContainer>
                    <AnimatePresence
                        visible={siteSettings.emailOverviewEnableFeatured}
                    >
                        <Row>
                            <Col mobileLandscapeUp={1 / 2}>
                                <FastField
                                    name="emailOverviewFeaturedTitle"
                                    component={Input}
                                    label={t('form.title')}
                                    helper={t(
                                        'admin.content-email-featured-title-helper',
                                    )}
                                    onSubmit={handleSubmitSetting}
                                />
                            </Col>
                        </Row>
                    </AnimatePresence>
                </Form>
            </Formik>
        </Container>
    )
}

const Query = gql`
    query SitePeriodicOverviewSettings {
        siteSettings {
            emailOverviewSubject
            emailOverviewTitle
            emailOverviewIntro
            emailOverviewEnableFeatured
            emailOverviewFeaturedTitle
        }
    }
`

const Mutation = gql`
    mutation EditSiteSetting($input: editSiteSettingInput!) {
        editSiteSetting(input: $input) {
            siteSettings {
                emailOverviewSubject
                emailOverviewTitle
                emailOverviewIntro
                emailOverviewEnableFeatured
                emailOverviewFeaturedTitle
            }
        }
    }
`

export default compose(graphql(Query), graphql(Mutation))(PeriodicOverview)
