import React from 'react'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { Form, Formik } from 'formik'
import compose from 'lodash.flowright'

import SettingContainer from 'js/admin/layout/SettingContainer'
import submitSetting from 'js/admin/lib/submitSetting'
import Switch from 'js/components/Form/FormSwitch'
import { Container } from 'js/components/Grid/Grid'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'

const Notifications = ({ data, mutate }) => {
    if (data.loading) return <LoadingSpinner />
    if (!data?.siteSettings) return null
    const { siteSettings } = data

    const handleSubmitSetting = (key, value, resolve, reject) => {
        const refetchQueries = ['SiteNotificationsSettings']
        submitSetting(mutate, key, value, refetchQueries, resolve, reject)
    }

    return (
        <Container size="small">
            <Formik>
                <Form>
                    <SettingContainer
                        subtitle="admin.notifications-excerpt"
                        helper="admin.notifications-excerpt-helper"
                        htmlFor="emailNotificationShowExcerpt"
                    >
                        <Switch
                            name="emailNotificationShowExcerpt"
                            checked={siteSettings.emailNotificationShowExcerpt}
                            onChange={handleSubmitSetting}
                            releaseInputArea
                        />
                    </SettingContainer>
                </Form>
            </Formik>
        </Container>
    )
}

const Query = gql`
    query SiteNotificationsSettings {
        siteSettings {
            emailNotificationShowExcerpt
        }
    }
`

const Mutation = gql`
    mutation EditSiteSetting($input: editSiteSettingInput!) {
        editSiteSetting(input: $input) {
            siteSettings {
                emailNotificationShowExcerpt
            }
        }
    }
`

export default compose(graphql(Query), graphql(Mutation))(Notifications)
