import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'

import PageTitle from 'js/admin/components/PageTitle'
import Document from 'js/components/Document'
import { Container } from 'js/components/Grid/Grid'
import Section from 'js/components/Section/Section'
import TabMenu from 'js/components/TabMenu'
import TabPage from 'js/components/TabPage'

import Notifications from './components/Notifications'
import PeriodicOverview from './components/PeriodicOverview'

const Mailing = () => {
    const { t } = useTranslation()

    const [tab, setTab] = useState('notifications')

    return (
        <>
            <Document
                title={t('admin.mailing')}
                containerTitle={t('admin.title')}
            />
            <Section divider>
                <Container size="small">
                    <PageTitle>{t('admin.mailing')}</PageTitle>

                    <TabMenu
                        label={t('admin.mailing')}
                        options={[
                            {
                                onClick: () => setTab('notifications'),
                                isActive: tab === 'notifications',
                                label: t('admin.notifications'),
                                id: 'tab-notifications',
                                ariaControls: 'panel-notifications',
                            },
                            {
                                onClick: () => setTab('periodic-overview'),
                                isActive: tab === 'periodic-overview',
                                label: t('admin.content-email'),
                                id: 'tab-periodic-overview',
                                ariaControls: 'panel-periodic-overview',
                            },
                        ]}
                        showBorder
                        sticky
                    />
                </Container>

                <TabPage
                    visible={tab === 'notifications'}
                    style={{ padding: '24px 0' }}
                    id="panel-notifications"
                    ariaLabelledby="tab-notifications"
                >
                    <Notifications />
                </TabPage>
                <TabPage
                    visible={tab === 'periodic-overview'}
                    style={{ padding: '24px 0' }}
                    id="panel-periodic-overview"
                    ariaLabelledby="tab-periodic-overview"
                >
                    <PeriodicOverview />
                </TabPage>
            </Section>
        </>
    )
}

export default Mailing
