import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import SettingContainer from 'js/admin/layout/SettingContainer'
import Document from 'js/components/Document'
import FetchMore from 'js/components/FetchMore'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Section from 'js/components/Section/Section'
import Table from 'js/components/Table'
import Text from 'js/components/Text/Text'

const BrokenLinks = () => {
    const { t } = useTranslation()

    const [queryLimit, setQueryLimit] = useState(50)
    const { data, loading, fetchMore } = useQuery(GET_BROKEN_LINKS, {
        variables: {
            offset: 0,
            limit: queryLimit,
        },
    })

    return (
        <>
            <Document
                title={t('admin.broken-links')}
                containerTitle={t('admin.title')}
            />
            <Section divider>
                <SettingContainer title="admin.broken-links" />

                {loading ? (
                    <LoadingSpinner />
                ) : data.brokenLinks.edges.length === 0 ? (
                    <Text textAlign="center">
                        {t('admin.broken-links-no-results')}
                    </Text>
                ) : (
                    <Table wrap="word" rowHeight="small">
                        <thead>
                            <tr>
                                <th>{t('global.source')}</th>
                                <th>{t('form.link')}</th>
                                <th
                                    style={{
                                        textAlign: 'right',
                                    }}
                                >
                                    {t('admin.reason')}
                                </th>
                            </tr>
                        </thead>
                        <FetchMore
                            as="tbody"
                            edges={data.brokenLinks.edges}
                            getMoreResults={(data) => data.brokenLinks.edges}
                            fetchMore={fetchMore}
                            fetchCount={50}
                            setLimit={setQueryLimit}
                            maxLimit={data.brokenLinks.total}
                        >
                            {data.brokenLinks.edges.map(
                                ({ id, source, target, reason }) => (
                                    <tr key={id}>
                                        <td>
                                            <Link
                                                to={source}
                                                className="TableLinkBlock"
                                                style={{
                                                    padding: '8px 0',
                                                }}
                                            >
                                                {source}
                                            </Link>
                                        </td>
                                        <td>
                                            <Link
                                                to={target}
                                                className="TableLinkBlock"
                                                style={{
                                                    padding: '8px 0',
                                                }}
                                            >
                                                {target}
                                            </Link>
                                        </td>
                                        <td
                                            style={{
                                                whiteSpace: 'nowrap',
                                                textAlign: 'right',
                                            }}
                                        >
                                            {t(
                                                `admin.broken-links-reason-${reason}`,
                                            )}
                                        </td>
                                    </tr>
                                ),
                            )}
                        </FetchMore>
                    </Table>
                )}
            </Section>
        </>
    )
}

const GET_BROKEN_LINKS = gql`
    query SiteBrokenLinks($offset: Int, $limit: Int) {
        brokenLinks(offset: $offset, limit: $limit) {
            total
            edges {
                id
                source
                target
                reason
            }
        }
    }
`

export default BrokenLinks
