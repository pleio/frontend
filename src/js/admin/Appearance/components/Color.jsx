import React, { useState } from 'react'
import { ChromePicker } from 'react-color'
import { useTranslation } from 'react-i18next'
import { getReadableColor, textContrastIsAA } from 'helpers/getContrast'
import { getContrast, transparentize } from 'polished'
import styled, { useTheme } from 'styled-components'

import Badge from 'js/components/Badge/Badge'
import Popover from 'js/components/Popover/Popover'
import Tooltip from 'js/components/Tooltip/Tooltip'

import CheckIcon from 'icons/check-small.svg'
import CrossIcon from 'icons/cross-tiny.svg'
import TypographyIcon from 'icons/typography.svg'

const Wrapper = styled.div`
    user-select: none;

    .ColorTile {
        position: relative;
        flex-shrink: 0;
        display: flex;
        align-items: center;
        justify-content: center;
        width: 40px;
        height: 40px;
        background: ${(p) => p.$color};
        border-radius: 50%;
        border: 1px solid ${transparentize(0.9, 'black')};
    }

    ${Badge} {
        position: absolute;
        top: -3px;
        right: -3px;
        background-color: ${(p) =>
            p.$checkContrast ? p.theme.color.accept : p.theme.color.warn.main};
    }
`

const Color = ({ name, color, onChange }) => {
    const theme = useTheme()
    const { t } = useTranslation()

    const [finalColor, setFinalColor] = useState(color)
    const [pickColor, setPickColor] = useState(color)

    if (!color) return null

    const handleChangePicker = (color) => {
        setPickColor(color.hex)
    }

    const handleCompletePicker = (color) => {
        setFinalColor(color.hex)
    }

    const contrastBackround = textContrastIsAA(finalColor)
    const contrastText = getContrast(finalColor, theme.color.text.black) >= 3

    let checkContrast
    if (name === 'colorPrimary') {
        checkContrast = contrastBackround && contrastText
    } else if (name === 'colorSecondary') {
        checkContrast = contrastBackround
    }

    return (
        <Wrapper $color={finalColor} $checkContrast={checkContrast}>
            <Popover
                content={
                    <ChromePicker
                        disableAlpha
                        color={pickColor}
                        onChange={handleChangePicker}
                        onChangeComplete={handleCompletePicker}
                    />
                }
                onHide={() => onChange(name, finalColor)}
            >
                <button
                    type="button"
                    className="ColorTile"
                    aria-label={t('admin.color-show-picker')}
                >
                    {checkContrast !== undefined ? (
                        <Tooltip
                            theme={
                                checkContrast ? 'accept-white' : 'warn-white'
                            }
                            content={
                                checkContrast
                                    ? t('admin.color-pass')
                                    : t('admin.color-fail')
                            }
                        >
                            <Badge>
                                {checkContrast ? <CheckIcon /> : <CrossIcon />}
                            </Badge>
                        </Tooltip>
                    ) : (
                        <TypographyIcon
                            style={{
                                color: getReadableColor(finalColor),
                            }}
                        />
                    )}
                </button>
            </Popover>
        </Wrapper>
    )
}

export default Color
