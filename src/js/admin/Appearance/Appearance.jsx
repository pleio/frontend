import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { FastField, Form, Formik } from 'formik'
import compose from 'lodash.flowright'

import PageTitle from 'js/admin/components/PageTitle'
import UploadImage from 'js/admin/components/UploadImage'
import SettingContainer from 'js/admin/layout/SettingContainer'
import submitSetting from 'js/admin/lib/submitSetting'
import AnimatePresence from 'js/components/AnimatePresence/AnimatePresence'
import Document from 'js/components/Document'
import ErrorModal from 'js/components/ErrorModal'
import Flexer from 'js/components/Flexer/Flexer'
import Input from 'js/components/Form/FormikTextfield'
import Select from 'js/components/Form/FormSelect'
import { Container } from 'js/components/Grid/Grid'
import IconButton from 'js/components/IconButton/IconButton'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Section from 'js/components/Section/Section'

import HeartIcon from 'icons/heart.svg'
import ThumbsIcon from 'icons/thumbs.svg'

import Color from './components/Color'

const Appearance = ({ data, mutate }) => {
    const { t } = useTranslation()
    const [uploadError, setUploadError] = useState('')

    if (data.loading) return <LoadingSpinner />
    if (!data?.siteSettings) return null

    const { siteSettings } = data

    const handleSubmitSetting = (key, value, resolve, reject) => {
        const refetchQueries = ['SiteAppearanceSettings']
        const rejectSubmit = (error) => {
            setUploadError(error)
            reject(error)
        }

        let beforeResolve = resolve
        if (key === 'fontBody' || key === 'fontHeading') {
            beforeResolve = () => {
                window.location.reload()
            }
        }

        submitSetting(
            mutate,
            key,
            value,
            refetchQueries,
            beforeResolve,
            rejectSubmit,
        )
    }

    const handleSubmitLikeIcon = (value) => {
        return new Promise((resolve, reject) => {
            handleSubmitSetting('likeIcon', value, resolve, reject)
        })
    }
    const handleClickHeartButton = () => {
        return handleSubmitLikeIcon('heart')
    }
    const handleClickThumbsButton = () => {
        return handleSubmitLikeIcon('thumbs')
    }

    const initialValues = {
        logoAlt: siteSettings.logoAlt,
    }

    return (
        <>
            <Document
                title={t('global.appearance')}
                containerTitle={t('admin.title')}
            />
            <Formik initialValues={initialValues}>
                <Form>
                    <Container size="small">
                        <Section divider>
                            <PageTitle>{t('global.appearance')}</PageTitle>

                            <SettingContainer
                                title="admin.theme"
                                inputWidth="normal"
                                htmlFor="theme"
                            >
                                <Select
                                    name="theme"
                                    options={siteSettings.themeOptions}
                                    value={siteSettings.theme}
                                    onChange={handleSubmitSetting}
                                />
                            </SettingContainer>
                        </Section>

                        <Section divider>
                            <SettingContainer
                                title="admin.favicon"
                                htmlFor="favicon"
                            >
                                <UploadImage
                                    name="favicon"
                                    removeName="removeFavicon"
                                    src={siteSettings.favicon}
                                    alt={t('admin.favicon')}
                                    onSubmit={handleSubmitSetting}
                                    stretch
                                    style={{
                                        width: '32px',
                                        height: '32px',
                                    }}
                                />
                            </SettingContainer>
                        </Section>

                        <Section divider>
                            <SettingContainer
                                title="admin.fonts"
                                subtitle="admin.font-heading"
                                helper="admin.font-heading-helper"
                                inputWidth="normal"
                                htmlFor="fontHeading"
                            >
                                <Select
                                    name="fontHeading"
                                    options={siteSettings.fontOptions}
                                    value={siteSettings.fontHeading}
                                    onChange={handleSubmitSetting}
                                />
                            </SettingContainer>
                            <SettingContainer
                                subtitle="admin.font-default"
                                helper="admin.font-default-helper"
                                inputWidth="normal"
                                htmlFor="fontBody"
                            >
                                <Select
                                    name="fontBody"
                                    options={siteSettings.fontOptions}
                                    value={siteSettings.fontBody}
                                    onChange={handleSubmitSetting}
                                />
                            </SettingContainer>
                        </Section>

                        <Section divider>
                            <SettingContainer
                                title="admin.colors"
                                subtitle="admin.colors-header"
                                helper="admin.colors-header-helper"
                            >
                                <Color
                                    name="colorHeader"
                                    color={siteSettings.colorHeader}
                                    onChange={handleSubmitSetting}
                                />
                            </SettingContainer>
                            <SettingContainer
                                subtitle="admin.colors-primary"
                                helper="admin.colors-primary-helper"
                            >
                                <Color
                                    name="colorPrimary"
                                    color={siteSettings.colorPrimary}
                                    onChange={handleSubmitSetting}
                                />
                            </SettingContainer>
                            <SettingContainer
                                subtitle="admin.colors-secondary"
                                helper="admin.colors-secondary-helper"
                            >
                                <Color
                                    name="colorSecondary"
                                    color={siteSettings.colorSecondary}
                                    onChange={handleSubmitSetting}
                                />
                            </SettingContainer>
                        </Section>

                        <AnimatePresence
                            visible={siteSettings.theme === 'rijkshuisstijl'}
                        >
                            <>
                                <Section divider>
                                    <SettingContainer
                                        title="admin.header"
                                        subtitle="admin.header-image"
                                        helper="admin.header-image-helper"
                                    >
                                        <UploadImage
                                            name="logo"
                                            removeName="removeLogo"
                                            src={siteSettings.logo}
                                            alt={siteSettings.logoAlt}
                                            onSubmit={handleSubmitSetting}
                                            style={{
                                                height: '70px',
                                            }}
                                        />
                                    </SettingContainer>
                                    <SettingContainer
                                        subtitle="admin.header-description"
                                        helper="admin.header-description-helper"
                                        inputWidth="large"
                                        htmlFor="logoAlt"
                                    >
                                        <FastField
                                            name="logoAlt"
                                            component={Input}
                                            onSubmit={handleSubmitSetting}
                                        />
                                    </SettingContainer>
                                </Section>
                                <div />
                            </>
                        </AnimatePresence>

                        <Section divider>
                            <SettingContainer
                                subtitle="admin.like-icon"
                                helper="admin.like-icon-helper"
                            >
                                <Flexer gutter="none">
                                    <IconButton
                                        size="huge"
                                        variant={
                                            siteSettings.likeIcon === 'heart'
                                                ? 'primary'
                                                : 'secondary'
                                        }
                                        aria-label={t(
                                            'admin.like-icon-heart-label',
                                        )}
                                        disabled={
                                            siteSettings.likeIcon === 'heart'
                                        }
                                        onHandle={handleClickHeartButton}
                                        showOutline={
                                            siteSettings.likeIcon === 'heart'
                                        }
                                    >
                                        <HeartIcon />
                                    </IconButton>
                                    <IconButton
                                        size="huge"
                                        variant={
                                            siteSettings.likeIcon === 'thumbs'
                                                ? 'primary'
                                                : 'secondary'
                                        }
                                        aria-label={t(
                                            'admin.like-icon-thumbs-label',
                                        )}
                                        disabled={
                                            siteSettings.likeIcon === 'thumbs'
                                        }
                                        onHandle={handleClickThumbsButton}
                                        showOutline={
                                            siteSettings.likeIcon === 'thumbs'
                                        }
                                    >
                                        <ThumbsIcon />
                                    </IconButton>
                                </Flexer>
                            </SettingContainer>
                        </Section>
                    </Container>
                </Form>
            </Formik>

            {uploadError && (
                <ErrorModal
                    showModal={!!uploadError}
                    setShowModal={() => setUploadError('')}
                    error={uploadError}
                />
            )}
        </>
    )
}

const Query = gql`
    query SiteAppearanceSettings {
        siteSettings {
            themeOptions {
                value
                label
            }
            theme
            favicon
            fontOptions {
                value
                label
            }
            fontBody
            fontHeading
            colorHeader
            colorPrimary
            colorSecondary
            theme
            logo
            logoAlt
            likeIcon
        }
    }
`

const Mutation = gql`
    mutation EditSiteSetting($input: editSiteSettingInput!) {
        editSiteSetting(input: $input) {
            siteSettings {
                theme
                favicon
                fontBody
                fontHeading
                colorHeader
                colorPrimary
                colorSecondary
                theme
                logo
                logoAlt
                likeIcon
            }
        }
    }
`

export default compose(graphql(Query), graphql(Mutation))(Appearance)
