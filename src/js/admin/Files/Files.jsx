import React from 'react'
import { useTranslation } from 'react-i18next'

import ContentList from 'js/admin/components/ContentList'
import PageTitle from 'js/admin/components/PageTitle'
import Document from 'js/components/Document'
import { Container } from 'js/components/Grid/Grid'
import Section from 'js/components/Section/Section'
import useSubtypes from 'js/lib/hooks/useSubtypes'

const Files = () => {
    const { t } = useTranslation()

    const { subtypes, subtypeLabels } = useSubtypes(['file', 'folder', 'pad'])

    return (
        <>
            <Document
                title={t('global.files')}
                containerTitle={t('admin.title')}
            />
            <Container>
                <Section divider>
                    <PageTitle>{t('global.files')}</PageTitle>
                    <ContentList
                        id="admin-files"
                        subtypesMap={subtypes}
                        subtypeLabels={subtypeLabels}
                        showFiles
                    />
                </Section>
            </Container>
        </>
    )
}
export default Files
