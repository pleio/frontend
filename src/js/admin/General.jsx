import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { FastField, Formik } from 'formik'
import i18next from 'i18next'
import compose from 'lodash.flowright'

import PageTitle from 'js/admin/components/PageTitle'
import SettingContainer from 'js/admin/layout/SettingContainer'
import submitSetting from 'js/admin/lib/submitSetting'
import Document from 'js/components/Document'
import Textarea from 'js/components/Form/FormikTextarea'
import Input from 'js/components/Form/FormikTextfield'
import Select from 'js/components/Form/FormSelect'
import { Col, Container, Row } from 'js/components/Grid/Grid'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Section from 'js/components/Section/Section'

const General = ({ data, mutate }) => {
    const { t } = useTranslation()

    if (data.loading) return <LoadingSpinner />
    if (!data?.siteSettings) return null

    const { siteSettings } = data

    const handleSubmitSetting = (key, value, resolve, reject) => {
        const refetchQueries = ['SiteGeneralSettings']

        if (key === 'name') refetchQueries.push('Document')

        let beforeResolve = resolve
        if (key === 'language') {
            beforeResolve = () => {
                // using Promises
                i18next.changeLanguage(value).then((t) => {
                    t('key') // -> same as i18next.t
                })
                resolve()
            }
        }

        submitSetting(mutate, key, value, refetchQueries, beforeResolve, reject)
    }

    const initialValues = {
        name: siteSettings.name,
        description: siteSettings.description,
        piwikUrl: siteSettings.piwikUrl,
        piwikId: siteSettings.piwikId,
    }

    const extraLanguageOptions = [...siteSettings.languageOptions]
    extraLanguageOptions.splice(
        siteSettings.languageOptions.findIndex(
            (el) => el.value === siteSettings.language,
        ),
        1,
    )

    return (
        <>
            <Document
                title={t('global.general')}
                containerTitle={t('admin.title')}
            />

            <Formik initialValues={initialValues}>
                <Container size="small">
                    <Section divider>
                        <PageTitle>{t('global.site')}</PageTitle>

                        <SettingContainer
                            title="global.general"
                            subtitle="admin.language"
                            helper="admin.language-helper"
                            inputWidth="normal"
                            htmlFor="language"
                        >
                            <Select
                                name="language"
                                options={siteSettings.languageOptions}
                                value={siteSettings.language}
                                onChange={handleSubmitSetting}
                            />
                        </SettingContainer>
                        <SettingContainer
                            subtitle="admin.extra-languages"
                            helper="admin.extra-languages-helper"
                            inputWidth="normal"
                            htmlFor="extraLanguages"
                        >
                            <Select
                                name="extraLanguages"
                                isMulti
                                options={extraLanguageOptions}
                                value={siteSettings.extraLanguages}
                                onChange={handleSubmitSetting}
                            />
                        </SettingContainer>
                        <SettingContainer
                            subtitle="form.title"
                            helper="admin.site-title-helper"
                            inputWidth="large"
                            htmlFor="name"
                        >
                            <FastField
                                name="name"
                                component={Input}
                                onSubmit={handleSubmitSetting}
                            />
                        </SettingContainer>
                        <SettingContainer
                            subtitle="admin.site-description"
                            helper="admin.site-description-helper"
                            inputWidth="large"
                            htmlFor="description"
                        >
                            <FastField
                                name="description"
                                component={Textarea}
                                size="small"
                                onSubmit={handleSubmitSetting}
                            />
                        </SettingContainer>
                        <SettingContainer
                            subtitle="admin.analytics-matomo-piwik"
                            inputWidth="full"
                        >
                            <Row $spacing={16}>
                                <Col mobileLandscapeUp={1 / 2}>
                                    <FastField
                                        name="piwikUrl"
                                        component={Input}
                                        label={t('form.url')}
                                        onSubmit={handleSubmitSetting}
                                    />
                                </Col>
                                <Col mobileLandscapeUp={1 / 2}>
                                    <FastField
                                        name="piwikId"
                                        component={Input}
                                        label={t('form.id')}
                                        onSubmit={handleSubmitSetting}
                                    />
                                </Col>
                            </Row>
                        </SettingContainer>
                    </Section>
                </Container>
            </Formik>
        </>
    )
}

const Query = gql`
    query SiteGeneralSettings {
        siteSettings {
            languageOptions {
                value
                label
            }
            language
            name
            description
            extraLanguages
            piwikUrl
            piwikId
            contentModerationEnabled
        }
    }
`

const Mutation = gql`
    mutation EditSiteSetting($input: editSiteSettingInput!) {
        editSiteSetting(input: $input) {
            siteSettings {
                language
                name
                description
                extraLanguages
                piwikUrl
                piwikId
            }
        }
    }
`

export default compose(graphql(Query), graphql(Mutation))(General)
