import React from 'react'
import { useTranslation } from 'react-i18next'
import { Outlet } from 'react-router-dom'
import { media } from 'helpers'
import styled from 'styled-components'

import HideVisually from 'js/components/HideVisually/HideVisually'
import IconButton from 'js/components/IconButton/IconButton'
import { START_OF_CONTENT_ELEMENT_ID } from 'js/lib/constants'

import EyeIcon from 'icons/eye.svg'

import Navigation from './Navigation/Navigation'

const Wrapper = styled.div`
    flex-grow: 1;
    position: relative;
    width: 100%;
    max-width: 1480px;
    margin: 0 auto;
    overflow-x: clip;

    ${media.mobileLandscapeUp`
        padding-left: ${(p) => p.theme.dimensions.adminNavigationWidth};
    `};

    ${media.desktopLarge`
        padding-right: ${(p) => p.theme.dimensions.adminNavigationWidth};
    `};

    .LayoutMain {
        position: relative;
        width: 100%;
        padding-bottom: 60px;

        ${media.mobileLandscapeUp`
            padding-top: 40px;
        `};

        ${media.desktopUp`
            padding-left: 20px;
            padding-right: 20px;
        `};
    }

    .LayoutViewButton {
        position: fixed;
        bottom: 20px;
        right: 20px;
        z-index: 7;
    }

    .HeaderSkipToContent {
        &:focus,
        &:active {
            top: 4px;
            left: 4px;
            width: auto;
            height: 40px;

            margin: 0;
            padding: 0 12px;
            background-color: ${(p) => p.theme.color.pleio};

            font-size: ${(p) => p.theme.font.size.small};
            line-height: 40px;
            font-weight: ${(p) => p.theme.font.weight.semibold};
            color: white;

            clip: auto;
            z-index: 14;

            && {
                outline-offset: 2px;
            }
        }
    }
`

const Layout = ({ menu, ...rest }) => {
    const { t } = useTranslation()

    return (
        <Wrapper {...rest}>
            <HideVisually
                as="a"
                href={`#${START_OF_CONTENT_ELEMENT_ID}`}
                className="HeaderSkipToContent"
            >
                {t('action.skip-to-content')}
            </HideVisually>

            <Navigation menu={menu} />

            <main
                className="LayoutMain"
                id={START_OF_CONTENT_ELEMENT_ID}
                tabIndex="-1"
            >
                <IconButton
                    as="a"
                    href="/"
                    className="LayoutViewButton"
                    variant="primary"
                    size="huge"
                    tooltip={t('admin.view-site')}
                    dropShadow
                >
                    <EyeIcon />
                </IconButton>
                <Outlet />
            </main>
        </Wrapper>
    )
}

export default Layout
