import styled, { css } from 'styled-components'

import Setting from 'js/components/Setting'

export default styled(Setting)`
    ${(p) =>
        p.$mb &&
        css`
            margin-bottom: 24px;
        `};

    &:not(:last-child) {
        margin-bottom: 24px;
    }
`
