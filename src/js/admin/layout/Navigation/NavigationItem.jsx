import React from 'react'
import { NavLink, useMatch } from 'react-router-dom'
import { AnimatePresence, motion } from 'framer-motion'
import styled, { css } from 'styled-components'

import NavItem from './NavigationItem'

const Wrapper = styled.li`
    > .NavigationSubitems {
        position: relative;
        padding: 2px 0 2px 12px;
        overflow: hidden; // For animation

        &::before {
            content: '';
            position: absolute;
            left: 10px;
            top: 2px;
            bottom: 2px;
            width: 2px;
            background-color: ${(p) => p.theme.color.grey[20]};
        }
    }

    > .NavigationItem {
        position: relative;
        display: flex;
        align-items: center;
        border-radius: ${(p) => p.theme.radius.small};
        color: ${(p) => p.theme.color.text.grey};
        user-select: none;
        user-drag: none;

        &:hover {
            background-color: ${(p) => p.theme.color.hover};
            color: black;
        }

        &:active {
            background-color: ${(p) => p.theme.color.active};
            color: black;
        }

        ${(p) =>
            !p.$isSubitem &&
            css`
                padding: 8px 10px;
                font-size: ${(p) => p.theme.font.size.normal};
                line-height: ${(p) => p.theme.font.lineHeight.normal};
            `}

        ${(p) =>
            p.$isSubitem &&
            css`
                padding: 6px 14px;
                font-size: ${(p) => p.theme.font.size.small};
                line-height: ${(p) => p.theme.font.lineHeight.small};
                border-top-left-radius: 0;
                border-bottom-left-radius: 0;

                &::before {
                    content: '';
                    width: 2px;
                    height: 100%;
                    background-color: ${(p) => p.theme.color.primary.main};
                    position: absolute;
                    left: -2px;
                    opacity: 0;
                }

                &.active {
                    &::before {
                        opacity: 1;
                    }
                }
            `}

        &.active {
            pointer-events: none;
            color: ${(p) => p.theme.color.primary.main};
        }
    }

    .NavigationIcon {
        flex-shrink: 0;
        width: 24px;
        height: 24px;
        display: flex;
        justify-content: center;
        align-items: center;
        margin-right: 8px;
    }
`

const NavigationItem = ({ entity, isSubitem, hideNav }) => {
    const isActive = useMatch({
        path: entity.isActive || entity.link,
        end: false,
    })
    const hasSubitems = !!entity.children

    return (
        <Wrapper $isSubitem={!!isSubitem} $hasSubitems={hasSubitems}>
            <NavLink
                className={`NavigationItem${isActive ? ' active' : ''}`}
                to={entity.link}
                exact={entity.exact}
                onClick={!hasSubitems ? hideNav : null}
            >
                {entity.icon && (
                    <div className="NavigationIcon">{entity.icon}</div>
                )}
                <span>{entity.title}</span>
            </NavLink>
            <AnimatePresence>
                {hasSubitems && !!isActive && (
                    <motion.ul
                        className="NavigationSubitems"
                        initial="collapsed"
                        animate="open"
                        exit="collapsed"
                        variants={{
                            collapsed: { opacity: 0, height: 0 },
                            open: { opacity: 1, height: 'auto' },
                        }}
                        transition={{ duration: 0.15 }}
                    >
                        {entity.children.map((entity, i) => (
                            <NavItem
                                key={i}
                                entity={entity}
                                isSubitem
                                hideNav={hideNav}
                            />
                        ))}
                    </motion.ul>
                )}
            </AnimatePresence>
        </Wrapper>
    )
}

export default NavigationItem
