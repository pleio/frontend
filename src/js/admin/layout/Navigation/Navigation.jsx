import React, { useState } from 'react'
import { FocusOn } from 'react-focus-on'
import { useTranslation } from 'react-i18next'
import { AnimatePresence, motion } from 'framer-motion'
import { media } from 'helpers'
import { MobileLandscapeUp, MobilePortrait } from 'helpers/mediaWrapper'
import styled, { css } from 'styled-components'

import IconButton from 'js/components/IconButton/IconButton'

import MenuIcon from 'icons/menu.svg'

import NavItem from './NavigationItem'

const ToggleNavButton = styled(IconButton)`
    position: fixed;
    bottom: 20px;
    left: 20px;
    z-index: 7;
`

const Wrapper = styled.div`
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    z-index: 3;

    ${media.mobilePortrait`
        display: flex;
        position: fixed;
        right: 0;
        z-index: 8;

        ${(p) =>
            !p.$showNav &&
            css`
                pointer-events: none;
            `}
    `};

    .NavigationBackground {
        position: absolute;
        top: 0;
        bottom: 0;
        right: 0;
        left: 0;
        background-color: rgba(0, 0, 0, 0.1);
        cursor: pointer;
    }

    .NavigationMenuBackground {
        position: absolute;
        display: block;
        bottom: calc(-100vh + 52px);
        left: calc(-100vh + 52px);

        width: 200vh;
        height: 200vh;
        border-radius: 50%;
        background-color: white;
        transform-origin: center;
    }

    .NavigationMenu {
        height: 100%;

        ${media.mobilePortrait`
            position: relative;
            width: 250px;
            max-width: 100%;
            display: flex;
            align-items: center;
            overflow: hidden;
        `};

        ${media.mobileLandscapeUp`
            position: fixed;
            width: ${(p) => p.theme.dimensions.adminNavigationWidth};
            box-shadow: ${(p) => p.theme.shadow.hard.right};
        `};
    }

    .NavigationItems {
        position: relative;
        width: 100%;
        max-height: 100%;
        overflow-y: auto;

        ${media.mobilePortrait`
            padding: 40px 16px 80px;
        `};

        ${media.mobileLandscapeUp`
            padding: 80px 10px 40px;
        `};
    }
`

const Navigation = ({ menu }) => {
    const [showNav, setShowNav] = useState(false)

    const toggleShowNav = () => {
        setShowNav(!showNav)
    }

    const hideNav = () => {
        setShowNav(false)
    }

    const { t } = useTranslation()

    if (!menu) return null

    return (
        <>
            <MobilePortrait>
                <ToggleNavButton
                    variant="secondary"
                    size="huge"
                    dropShadow
                    aria-label={t('action.show-navigation')}
                    aria-expanded={showNav}
                    aria-haspopup="true"
                    aria-controls="menu-mobile"
                    onClick={toggleShowNav}
                >
                    <MenuIcon />
                </ToggleNavButton>
            </MobilePortrait>
            <Wrapper $showNav={showNav}>
                <MobilePortrait>
                    <AnimatePresence>
                        {showNav && (
                            <FocusOn onEscapeKey={toggleShowNav}>
                                <motion.div
                                    className="NavigationBackground"
                                    initial="hide"
                                    animate="show"
                                    exit="hide"
                                    variants={{
                                        hide: {
                                            opacity: 0,
                                            transition: {
                                                duration: 0.15,
                                            },
                                        },
                                        show: {
                                            opacity: 1,
                                            transition: {
                                                duration: 0.3,
                                            },
                                        },
                                    }}
                                    onClick={hideNav}
                                />
                                <div className="NavigationMenu">
                                    <motion.div
                                        className="NavigationMenuBackground"
                                        initial="hide"
                                        animate="show"
                                        exit="hide"
                                        variants={{
                                            hide: {
                                                transform: 'scale(0)',
                                                transition: {
                                                    duration: 0.15,
                                                },
                                            },
                                            show: {
                                                transform: 'scale(1)',
                                                opacity: 1,
                                                transition: {
                                                    duration: 0.3,
                                                },
                                            },
                                        }}
                                    />
                                    <ToggleNavButton
                                        variant="secondary"
                                        size="huge"
                                        showOutline
                                        aria-label={t('action.hide-navigation')}
                                        aria-expanded={showNav}
                                        aria-haspopup="true"
                                        aria-controls="menu-mobile"
                                        onClick={toggleShowNav}
                                    >
                                        <MenuIcon />
                                    </ToggleNavButton>
                                    <motion.nav
                                        className="NavigationItems"
                                        id="menu-mobile"
                                        initial="hide"
                                        animate="show"
                                        exit="hide"
                                        variants={{
                                            hide: {
                                                opacity: 0,
                                                transition: {
                                                    duration: 0.15,
                                                },
                                            },
                                            show: {
                                                opacity: 1,
                                                transition: {
                                                    duration: 0.25,
                                                    delay: 0.05,
                                                },
                                            },
                                        }}
                                    >
                                        <ul>
                                            {menu.map((entity, i) => (
                                                <NavItem
                                                    key={i}
                                                    entity={entity}
                                                    hideNav={hideNav}
                                                />
                                            ))}
                                        </ul>
                                    </motion.nav>
                                </div>
                            </FocusOn>
                        )}
                    </AnimatePresence>
                </MobilePortrait>
                <MobileLandscapeUp>
                    <nav className="NavigationMenu">
                        <ul className="NavigationItems">
                            {menu.map((entity, i) => (
                                <NavItem
                                    key={i}
                                    entity={entity}
                                    hideNav={hideNav}
                                />
                            ))}
                        </ul>
                    </nav>
                </MobileLandscapeUp>
            </Wrapper>
        </>
    )
}

export default Navigation
