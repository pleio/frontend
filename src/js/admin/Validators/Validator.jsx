import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'

import IconButton from 'js/components/IconButton/IconButton'
import Modal from 'js/components/Modal/Modal'

import DeleteIcon from 'icons/delete.svg'
import EditIcon from 'icons/edit.svg'

import AddEditValidator from './AddEditValidator'
import DeleteValidator from './DeleteValidator'

const Validator = ({ entity }) => {
    const [showAddEditValidatorModal, setShowAddEditValidatorModal] =
        useState(false)
    const toggleAddEditValidatorModal = () => {
        setShowAddEditValidatorModal(!showAddEditValidatorModal)
    }

    const [showDeleteValidatorModal, setShowDeleteValidatorModal] =
        useState(false)
    const toggleDeleteValidatorModal = () => {
        setShowDeleteValidatorModal(!showDeleteValidatorModal)
    }

    const { t } = useTranslation()

    return (
        <tr>
            <td style={{ width: '50%' }}>{entity.name}</td>
            <td style={{ width: '50%' }}>
                {t(`admin.validator-type-${entity.type}`)}
            </td>
            <td>
                <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                    <IconButton
                        size="large"
                        variant="secondary"
                        onClick={toggleAddEditValidatorModal}
                    >
                        <EditIcon />
                    </IconButton>
                    <Modal
                        isVisible={showAddEditValidatorModal}
                        title={t('admin.edit-validator', {
                            name: entity.name,
                        })}
                        size="small"
                        onClose={toggleAddEditValidatorModal}
                        containHeight
                    >
                        <AddEditValidator
                            entity={entity}
                            onClose={toggleAddEditValidatorModal}
                        />
                    </Modal>

                    <IconButton
                        size="large"
                        variant="secondary"
                        onClick={toggleDeleteValidatorModal}
                        aria-label={t('admin.delete-validator', {
                            name: entity.name,
                        })}
                    >
                        <DeleteIcon />
                    </IconButton>
                    <Modal
                        title={t('admin.delete-validator', {
                            name: entity.name,
                        })}
                        isVisible={showDeleteValidatorModal}
                        size="small"
                        onClose={toggleDeleteValidatorModal}
                    >
                        <DeleteValidator
                            entity={entity}
                            onClose={toggleDeleteValidatorModal}
                        />
                    </Modal>
                </div>
            </td>
        </tr>
    )
}

export default Validator
