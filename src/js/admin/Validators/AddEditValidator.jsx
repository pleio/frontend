import React, { useState } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import compose from 'lodash.flowright'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import { H4 } from 'js/components/Heading'
import Select from 'js/components/Select/Select'
import Text from 'js/components/Text/Text'
import Textfield from 'js/components/Textfield/Textfield'
import UploadBox from 'js/components/UploadBox/UploadBox'
import { MAX_FILE_SIZE } from 'js/lib/constants'

const List = styled.div`
    margin-bottom: 16px;
    border: 1px solid ${(p) => p.theme.color.grey[30]};
    overflow-y: auto;

    > * {
        padding: 4px 8px;

        &:not(:last-child) {
            border-bottom: 1px solid ${(p) => p.theme.color.grey[30]};
        }
    }
`

const AddEditValidator = ({ mutateAdd, mutateEdit, entity, onClose }) => {
    const { t } = useTranslation()

    const [errors, setErrors] = useState()

    const onSubmit = (data) => {
        const { type, name } = data

        if (entity) {
            return new Promise((resolve, reject) => {
                mutateEdit({
                    variables: {
                        input: {
                            id: entity.id,
                            name,
                            validationListFile,
                        },
                    },
                    refetchQueries: ['SiteValidatorsSettings'],
                })
                    .then(() => {
                        onClose()
                        resolve()
                    })
                    .catch((errors) => {
                        setErrors(errors)
                        reject(new Error(errors))
                    })
            })
        } else {
            return new Promise((resolve, reject) => {
                mutateAdd({
                    variables: {
                        input: {
                            type,
                            name,
                            validationListFile,
                        },
                    },
                    refetchQueries: ['SiteValidatorsSettings'],
                })
                    .then(() => {
                        onClose()
                        resolve()
                    })
                    .catch((errors) => {
                        setErrors(errors)
                        reject(new Error(errors))
                    })
            })
        }
    }

    const [validationListFile, setValidationListFile] = useState(null)
    const handleUploadFile = (file) => {
        setValidationListFile(file)
    }

    const defaultValues = {
        name: entity ? entity.name : '',
        type: entity ? entity.type : 'inList',
    }

    const { control, handleSubmit } = useForm({ defaultValues })

    const onError = (errors) => {
        return new Promise((resolve, reject) => {
            reject(new Error(errors))
        })
    }

    const submitForm = handleSubmit(onSubmit, onError)

    const typeOptions = [
        { value: 'inList', label: t('admin.validator-type-inList') },
    ]

    return (
        <>
            <Controller
                render={({ field }) => (
                    <Textfield
                        {...field}
                        label={t('form.title')}
                        style={{ marginBottom: '16px' }}
                    />
                )}
                name="name"
                control={control}
            />

            {!entity && (
                <Controller
                    render={({ field }) => (
                        <Select
                            {...field}
                            label={t('global.type')}
                            options={typeOptions}
                            style={{ marginBottom: '16px' }}
                        />
                    )}
                    name="type"
                    control={control}
                />
            )}

            {entity && entity.validationList && (
                <>
                    <H4 style={{ marginBottom: '4px' }}>
                        {t(`admin.validator-type-${entity.type}`)}
                    </H4>
                    <List>
                        {entity.validationList.map((el, i) => (
                            <div key={i}>{el}</div>
                        ))}
                    </List>
                </>
            )}

            <div>
                {entity && entity.validationList && (
                    <H4 style={{ marginBottom: '4px' }}>
                        {t(`admin.validator-inList-replace`)}
                    </H4>
                )}
                <UploadBox
                    name="validationListFile"
                    title={t('action.upload-csv')}
                    accept=".csv"
                    maxSize={MAX_FILE_SIZE.attachment}
                    onSubmit={handleUploadFile}
                >
                    {validationListFile && <div>{validationListFile.name}</div>}
                </UploadBox>
                <Text variant="grey" size="small" style={{ marginTop: '8px' }}>
                    {t(`admin.validator-inList-helper`)}
                </Text>
            </div>

            <Errors errors={errors} />

            <Flexer mt>
                <Button
                    size="normal"
                    variant="tertiary"
                    type="button"
                    onClick={onClose}
                >
                    {t('action.cancel')}
                </Button>
                <Button size="normal" variant="primary" onHandle={submitForm}>
                    {entity ? t('action.save') : t('action.create')}
                </Button>
            </Flexer>
        </>
    )
}

const ADD = gql`
    mutation AddSiteSettingProfileFieldValidator(
        $input: addSiteSettingProfileFieldValidatorInput!
    ) {
        addSiteSettingProfileFieldValidator(input: $input) {
            profileFieldValidator {
                id
                type
                name
                validationString
                validationList
            }
        }
    }
`

const EDIT = gql`
    mutation EditSiteSettingProfileFieldValidator(
        $input: editSiteSettingProfileFieldValidatorInput!
    ) {
        editSiteSettingProfileFieldValidator(input: $input) {
            profileFieldValidator {
                id
                type
                name
                validationString
                validationList
            }
        }
    }
`

export default compose(
    graphql(ADD, { name: 'mutateAdd' }),
    graphql(EDIT, { name: 'mutateEdit' }),
)(AddEditValidator)
