import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import Button from 'js/components/Button/Button'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'

const DeleteValidator = ({ mutate, entity, onClose }) => {
    const { t } = useTranslation()

    const [errors, setErrors] = useState()

    const handleSubmit = () => {
        return new Promise((resolve, reject) => {
            mutate({
                variables: {
                    input: {
                        id: entity.id,
                    },
                },
                refetchQueries: ['SiteValidatorsSettings'],
            })
                .then(() => {
                    onClose()
                    resolve()
                })
                .catch((errors) => {
                    setErrors(errors)
                    reject(new Error(errors))
                })
        })
    }

    return (
        <>
            {t('admin.delete-validator-confirm')}

            <Errors errors={errors} />

            <Flexer mt>
                <Button
                    size="normal"
                    variant="tertiary"
                    type="button"
                    onClick={onClose}
                >
                    {t('action.cancel')}
                </Button>
                <Button size="normal" variant="primary" onHandle={handleSubmit}>
                    {t('action.yes-delete')}
                </Button>
            </Flexer>
        </>
    )
}

const Mutation = gql`
    mutation DeleteSiteSettingProfileFieldValidator(
        $input: deleteSiteSettingProfileFieldValidatorInput!
    ) {
        deleteSiteSettingProfileFieldValidator(input: $input) {
            success
        }
    }
`

export default graphql(Mutation)(DeleteValidator)
