import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import PageTitle from 'js/admin/components/PageTitle'
import SettingContainer from 'js/admin/layout/SettingContainer'
import Button from 'js/components/Button/Button'
import Document from 'js/components/Document'
import { Container } from 'js/components/Grid/Grid'
import Modal from 'js/components/Modal/Modal'
import Section from 'js/components/Section/Section'
import Table from 'js/components/Table'

import AddEditValidator from './AddEditValidator'
import Validator from './Validator'

const Validators = ({ data }) => {
    if (!data || !data.siteSettings) return null

    const [showAddEditValidatorModal, setShowAddEditValidatorModal] =
        useState(false)
    const toggleAddEditValidatorModal = () => {
        setShowAddEditValidatorModal(!showAddEditValidatorModal)
    }

    const { t } = useTranslation()

    return (
        <>
            <Document
                title={t('admin.validators')}
                containerTitle={t('admin.title')}
            />
            <Container size="small">
                <Section divider>
                    <PageTitle>{t('admin.validators')}</PageTitle>

                    <SettingContainer
                        title="admin.validators"
                        helper="admin.validators-helper"
                    />

                    {data.siteSettings &&
                        data.siteSettings.profileFieldValidators.length > 0 && (
                            <Table style={{ marginBottom: '24px' }}>
                                <thead>
                                    <tr>
                                        <th>{t('global.name')}</th>
                                        <th>{t('global.type')}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {data.siteSettings.profileFieldValidators.map(
                                        (entity, index) => (
                                            <Validator
                                                key={index}
                                                entity={entity}
                                            />
                                        ),
                                    )}
                                </tbody>
                            </Table>
                        )}

                    <Modal
                        isVisible={showAddEditValidatorModal}
                        title={t('admin.create-validator')}
                        size="small"
                        onClose={toggleAddEditValidatorModal}
                    >
                        <AddEditValidator
                            isVisible={showAddEditValidatorModal}
                            onClose={toggleAddEditValidatorModal}
                        />
                    </Modal>

                    <Button
                        size="small"
                        variant="tertiary"
                        onClick={toggleAddEditValidatorModal}
                    >
                        {t('admin.create-validator')}
                    </Button>
                </Section>
            </Container>
        </>
    )
}

const Query = gql`
    query SiteValidatorsSettings {
        siteSettings {
            profileFieldValidators {
                id
                type
                name
                validationString
                validationList
            }
        }
    }
`

export default graphql(Query)(Validators)
