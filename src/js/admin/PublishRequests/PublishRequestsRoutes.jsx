import React from 'react'
import { Navigate, Route, Routes } from 'react-router-dom'

import RequireAuth from 'js/router/RequireAuth'

import CommentsContainer from './Comments/CommentsContainer'
import CommentsPublishRequestsList from './Comments/PublishRequestsList'
import ContentContainer from './Content/ContentContainer'
import ContentPublishRequestsList from './Content/PublishRequestsList'
import FilesContainer from './Files/FilesContainer'
import FilesPublishRequestsList from './Files/PublishRequestsList'

const PublishRequestsRoutes = () => (
    <Routes>
        <Route
            path="/content/*"
            element={
                <Routes>
                    <Route
                        element={
                            <RequireAuth siteSettings="contentModerationEnabled">
                                <ContentContainer />
                            </RequireAuth>
                        }
                    >
                        <Route
                            path="/"
                            element={<Navigate to="open" replace />}
                        />
                        <Route
                            path="/open"
                            element={
                                <ContentPublishRequestsList status="open" />
                            }
                        />
                        <Route
                            path="/assigned"
                            element={
                                <ContentPublishRequestsList status="assigned" />
                            }
                        />
                        <Route
                            path="/published"
                            element={
                                <ContentPublishRequestsList status="published" />
                            }
                        />
                    </Route>
                </Routes>
            }
        />

        <Route
            path="/files/*"
            element={
                <Routes>
                    <Route
                        element={
                            <RequireAuth siteSettings="contentModerationEnabled">
                                <FilesContainer />
                            </RequireAuth>
                        }
                    >
                        <Route
                            path="/"
                            element={<Navigate to="open" replace />}
                        />
                        <Route
                            path="/open"
                            element={<FilesPublishRequestsList status="open" />}
                        />
                        <Route
                            path="/assigned"
                            element={
                                <FilesPublishRequestsList status="assigned" />
                            }
                        />
                        <Route
                            path="/published"
                            element={
                                <FilesPublishRequestsList status="published" />
                            }
                        />
                    </Route>
                </Routes>
            }
        />

        <Route
            path="/comments/*"
            element={
                <RequireAuth siteSettings="commentModerationEnabled">
                    <Routes>
                        <Route element={<CommentsContainer />}>
                            <Route
                                path="/"
                                element={<Navigate to="open" replace />}
                            />
                            <Route
                                path="/open"
                                element={
                                    <CommentsPublishRequestsList status="open" />
                                }
                            />
                            <Route
                                path="/assigned"
                                element={
                                    <CommentsPublishRequestsList status="assigned" />
                                }
                            />
                            <Route
                                path="/closed"
                                element={
                                    <CommentsPublishRequestsList status="closed" />
                                }
                            />
                        </Route>
                    </Routes>
                </RequireAuth>
            }
        />
    </Routes>
)

export default PublishRequestsRoutes
