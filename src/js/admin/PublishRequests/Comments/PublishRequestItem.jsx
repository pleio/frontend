import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import HideVisually from 'js/components/HideVisually/HideVisually'
import Modal from 'js/components/Modal/Modal'
import Spacer from 'js/components/Spacer/Spacer'

import PublishRequestItemComment from './PublishRequestItemComment'

const PublishRequestItem = ({ entity, subtypes, status }) => {
    const { t } = useTranslation()

    const {
        guid,
        entity: { subtype, title, url },
        comments,
    } = entity

    const [showCommentsModal, setShowCommentsModal] = useState(false)
    const refetchQueries = [
        'GetPublishRequests',
        'GetPublishRequestTotals',
        'GetAdminAccessData',
    ]

    const [assignRequest, { loading: loadingAssign }] = useMutation(
        ASSIGN_REQUEST,
        {
            variables: { guid },
            refetchQueries,
        },
    )

    const [unassignRequest, { loading: loadingUnassign }] = useMutation(
        UNASSIGN_REQUEST,
        {
            variables: { guid },
            refetchQueries,
        },
    )

    return (
        <tr>
            <td>
                <div style={{ height: '100%', display: 'flex' }}>
                    <a
                        href={url}
                        target="_blank"
                        rel="noopener noreferrer"
                        className="TableLinkBlock"
                        style={{
                            wordBreak: 'break-word',
                        }}
                    >
                        {title}
                        <HideVisually>
                            {t('global.opens-in-new-window')}
                        </HideVisually>
                    </a>
                </div>
            </td>
            <td>{subtypes[subtype]?.contentName}</td>
            <td>
                <Flexer justifyContent="flex-end">
                    {status === 'assigned' && (
                        <Button
                            size="normal"
                            variant="tertiary"
                            onClick={unassignRequest}
                            loading={loadingUnassign}
                        >
                            {t('action.unassign')}
                        </Button>
                    )}

                    {['open', 'assigned'].includes(status) && (
                        <>
                            <Button
                                size="normal"
                                variant="secondary"
                                onClick={() => setShowCommentsModal(true)}
                            >
                                {t('comments.count-comments', {
                                    count: comments.length,
                                })}
                            </Button>
                            <Modal
                                title={t('comments.count-comments', {
                                    count: comments.length,
                                })}
                                size="normal"
                                isVisible={showCommentsModal}
                                onClose={() => setShowCommentsModal(false)}
                                showCloseButton
                            >
                                <Spacer as="ul" spacing="small">
                                    {comments.map((comment) => (
                                        <PublishRequestItemComment
                                            key={comment.guid}
                                            entity={comment}
                                        />
                                    ))}
                                </Spacer>
                            </Modal>
                        </>
                    )}

                    {status === 'open' && (
                        <Button
                            size="normal"
                            variant="primary"
                            onClick={assignRequest}
                            loading={loadingAssign}
                        >
                            {t('action.assign')}
                        </Button>
                    )}
                </Flexer>
            </td>
        </tr>
    )
}

const ASSIGN_REQUEST = gql`
    mutation AssignPublishRequest($guid: String!) {
        assignCommentModerationRequest(guid: $guid) {
            success
            node {
                status
            }
        }
    }
`

const UNASSIGN_REQUEST = gql`
    mutation ResetPublishRequest($guid: String!) {
        resetCommentModerationRequest(guid: $guid) {
            success
            node {
                status
            }
        }
    }
`

export default PublishRequestItem
