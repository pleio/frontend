import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'

import Button from 'js/components/Button/Button'
import Comment from 'js/components/Comment/Comment'
import Flexer from 'js/components/Flexer/Flexer'

const PublishRequestItemComment = ({ entity }) => {
    const { t } = useTranslation()

    const mutationOptions = {
        variables: { guid: entity.guid },
        refetchQueries: [
            'GetPublishRequests',
            'GetPublishRequestTotals',
            'GetAdminAccessData',
        ],
    }

    const [rejectRequest, { loading: loadingReject }] = useMutation(
        REJECT_REQUEST,
        mutationOptions,
    )
    const [publishRequest, { loading: loadingPublish }] = useMutation(
        PUBLISH_REQUEST,
        mutationOptions,
    )

    const disabled = loadingReject || loadingPublish

    return (
        <div>
            <Comment key={entity.guid} entity={entity} disabled />

            <Flexer justifyContent="flex-end">
                <Button
                    size="normal"
                    variant="warn"
                    onClick={rejectRequest}
                    loading={loadingReject}
                    disabled={disabled}
                >
                    {t('action.reject')}
                </Button>
                <Button
                    size="normal"
                    variant="primary"
                    onClick={publishRequest}
                    loading={loadingPublish}
                    disabled={disabled}
                >
                    {t('action.publish')}
                </Button>
            </Flexer>
        </div>
    )
}

const REJECT_REQUEST = gql`
    mutation RejectPublishRequest($guid: String!) {
        rejectComment(guid: $guid) {
            success
        }
    }
`

const PUBLISH_REQUEST = gql`
    mutation ConfirmPublishRequest($guid: String!) {
        confirmComment(guid: $guid) {
            success
        }
    }
`

export default PublishRequestItemComment
