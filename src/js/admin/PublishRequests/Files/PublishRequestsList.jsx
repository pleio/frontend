import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'

import FetchMore from 'js/components/FetchMore'
import { H4 } from 'js/components/Heading'
import HideVisually from 'js/components/HideVisually/HideVisually'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Table from 'js/components/Table'
import TabPage from 'js/components/TabPage'
import Text from 'js/components/Text/Text'
import useSubtypes from 'js/lib/hooks/useSubtypes'

import PublishRequestItem from './PublishRequestItem'

const PublishRequestList = ({ status }) => {
    const { t } = useTranslation()

    const { subtypes: subtypesMap } = useSubtypes([
        'blog',
        'discussion',
        'episode',
        'event',
        'news',
        'podcast',
        'question',
        'wiki',
    ])

    const [queryLimit, setQueryLimit] = useState(50)

    const variables = {
        offset: 0,
        limit: queryLimit,
        status,
    }

    const { loading, data, fetchMore } = useQuery(GET_PUBLISH_REQUESTS, {
        variables,
        fetchPolicy: 'cache-and-network',
    })

    const [selected, setSelected] = useState([])
    const clearSelected = () => {
        setSelected([])
    }

    const publishRequests = data?.publishRequests || {}

    return loading ? (
        <LoadingSpinner />
    ) : (
        <TabPage
            id={`panel-${status}`}
            ariaLabelledby={`tab-${status}`}
            visible
        >
            {publishRequests.edges.length === 0 ? (
                <Text textAlign="center" style={{ margin: 16 }}>
                    {t('admin.content-not-found')}
                </Text>
            ) : (
                <>
                    <H4 role="status" style={{ margin: '16px 0 4px' }}>
                        {t('global.count-items', {
                            count: publishRequests.total,
                        })}
                    </H4>
                    <Table rowHeight="normal">
                        <thead>
                            <tr>
                                <th>{t('global.name')}</th>
                                <th>{t('global.owner')}</th>
                                <th>{t('admin.requested')}</th>
                                <th>
                                    <HideVisually>
                                        {t('global.actions')}
                                    </HideVisually>
                                </th>
                            </tr>
                        </thead>
                        <FetchMore
                            as="tbody"
                            edges={publishRequests.edges}
                            getMoreResults={(data) =>
                                data.publishRequests.edges
                            }
                            fetchMore={fetchMore}
                            fetchCount={25}
                            setLimit={setQueryLimit}
                            maxLimit={publishRequests.total}
                            resultsMessage={t('global.result', {
                                count: publishRequests.total,
                            })}
                            fetchMoreButtonHeight={48}
                        >
                            {publishRequests.edges.map((entity) => {
                                const selectedIndex = selected?.findIndex(
                                    (el) => el?.guid === entity.guid,
                                )
                                const isSelected = selectedIndex > -1

                                const toggleSelected = () => {
                                    if (!isSelected) {
                                        setSelected([...selected, entity])
                                    } else {
                                        const newSelected = [...selected]
                                        newSelected.splice(selectedIndex, 1)
                                        setSelected(newSelected)
                                    }
                                }

                                return (
                                    <PublishRequestItem
                                        key={entity.guid}
                                        entity={entity}
                                        subtypes={subtypesMap}
                                        isSelected={isSelected}
                                        onToggle={toggleSelected}
                                        clearSelected={clearSelected}
                                        status={status}
                                    />
                                )
                            })}
                        </FetchMore>
                    </Table>
                </>
            )}
        </TabPage>
    )
}

const GET_PUBLISH_REQUESTS = gql`
    query GetPublishRequests(
        $offset: Int
        $limit: Int
        $status: PublishRequestStatusEnum
    ) {
        publishRequests(
            offset: $offset
            limit: $limit
            status: $status
            files: true
        ) {
            total
            edges {
                guid
                author {
                    guid
                    name
                    url
                }
                assignedTo {
                    guid
                }
                entity {
                    ... on File {
                        guid
                        subtype
                        title
                        url
                        publishRequest {
                            guid
                            publishedAt
                        }
                    }
                }
                publishedAt
                status
            }
        }
    }
`

export default PublishRequestList
