import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'

import Button from 'js/components/Button/Button'
import DisplayDate from 'js/components/DisplayDate'
import Flexer from 'js/components/Flexer/Flexer'
import HideVisually from 'js/components/HideVisually/HideVisually'

import ExternalIcon from 'icons/new-window-small.svg'

const refetchQueries = [
    'GetPublishRequests',
    'GetPublishRequestTotals',
    'GetAdminAccessData',
]

const OpenActions = ({ url, guid }) => {
    const { t } = useTranslation()

    const [assignRequest, { loading }] = useMutation(ASSIGN_REQUEST, {
        variables: { guid },
        refetchQueries,
    })

    return (
        <>
            <Button
                as="a"
                href={url}
                target="_blank"
                rel="noopener noreferrer"
                size="normal"
                variant="tertiary"
            >
                {t('action.view')}
                <HideVisually>{t('global.opens-in-new-window')}</HideVisually>
                <ExternalIcon
                    style={{
                        flexShrink: 0,
                        marginLeft: '6px',
                    }}
                />
            </Button>
            <Button
                size="normal"
                variant="primary"
                onClick={assignRequest}
                loading={loading}
            >
                {t('action.assign')}
            </Button>
        </>
    )
}

const AssignActions = ({ guid }) => {
    const { t } = useTranslation()

    const mutationOptions = {
        variables: { guid },
        refetchQueries,
    }

    const [unassignRequest, { loading: loadingUnassign }] = useMutation(
        UNASSIGN_REQUEST,
        mutationOptions,
    )
    const [rejectRequest, { loading: loadingReject }] = useMutation(
        REJECT_REQUEST,
        mutationOptions,
    )
    const [publishRequest, { loading: loadingPublish }] = useMutation(
        PUBLISH_REQUEST,
        mutationOptions,
    )

    const disabled = loadingUnassign || loadingReject || loadingPublish

    return (
        <>
            <Button
                size="normal"
                variant="tertiary"
                onClick={unassignRequest}
                loading={loadingUnassign}
                disabled={disabled}
            >
                {t('action.unassign')}
            </Button>
            <Button
                size="normal"
                variant="warn"
                onClick={rejectRequest}
                loading={loadingReject}
                disabled={disabled}
            >
                {t('action.reject')}
            </Button>
            <Button
                size="normal"
                variant="primary"
                onClick={publishRequest}
                loading={loadingPublish}
                disabled={disabled}
            >
                {t('action.publish')}
            </Button>
        </>
    )
}

const PublishRequestItem = ({ entity, subtypes, status }) => {
    const { t } = useTranslation()

    const {
        guid,
        author,
        entity: { subtype, title, url, publishRequest },
    } = entity

    return (
        <tr>
            <td>
                <div style={{ height: '100%', display: 'flex' }}>
                    <a
                        href={url}
                        target="_blank"
                        rel="noopener noreferrer"
                        className="TableLinkBlock"
                        style={{
                            wordBreak: 'break-word',
                        }}
                    >
                        {title}
                        <HideVisually>
                            {t('global.opens-in-new-window')}
                        </HideVisually>
                    </a>
                </div>
            </td>
            <td>{subtypes[subtype]?.contentName}</td>
            <td>
                <a
                    href={author.url}
                    target="_blank"
                    rel="noopener noreferrer"
                    className="TableLinkBlock"
                >
                    {author.name}
                    <HideVisually>
                        {t('global.opens-in-new-window')}
                    </HideVisually>
                </a>
            </td>
            <td>
                <DisplayDate date={publishRequest?.publishedAt} />
            </td>
            <td>
                <Flexer justifyContent="flex-end">
                    {status === 'open' ? (
                        <OpenActions url={url} guid={guid} />
                    ) : status === 'assigned' ? (
                        <AssignActions guid={guid} />
                    ) : null}
                </Flexer>
            </td>
        </tr>
    )
}

const ASSIGN_REQUEST = gql`
    mutation AssignPublishRequest($guid: String!) {
        assignPublishRequest(guid: $guid) {
            success
            node {
                publishedAt
                status
                assignedTo {
                    guid
                    name
                }
            }
        }
    }
`

const UNASSIGN_REQUEST = gql`
    mutation ResetPublishRequest($guid: String!) {
        resetPublishRequest(guid: $guid) {
            success
            node {
                publishedAt
                status
                assignedTo {
                    guid
                    name
                }
            }
        }
    }
`

const REJECT_REQUEST = gql`
    mutation RejectPublishRequest($guid: String!) {
        rejectPublishRequest(guid: $guid) {
            success
            node {
                publishedAt
                status
                assignedTo {
                    guid
                    name
                }
            }
        }
    }
`

const PUBLISH_REQUEST = gql`
    mutation ConfirmPublishRequest($guid: String!) {
        confirmPublishRequest(guid: $guid) {
            success
            node {
                publishedAt
                status
                assignedTo {
                    guid
                    name
                }
            }
        }
    }
`

export default PublishRequestItem
