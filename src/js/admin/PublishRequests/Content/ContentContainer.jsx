import React from 'react'
import { useTranslation } from 'react-i18next'
import { Outlet } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import PageTitle from 'js/admin/components/PageTitle'
import Document from 'js/components/Document'
import { Container } from 'js/components/Grid/Grid'
import Section from 'js/components/Section/Section'
import TabMenu from 'js/components/TabMenu'

const ContentContainer = ({ status }) => {
    const { t } = useTranslation()

    const { data } = useQuery(GET_PUBLISH_REQUEST_TOTALS)

    const openTotal = data?.open?.total || 0
    const assignedTotal = data?.assigned?.total || 0
    const publishedTotal = data?.published?.total || 0

    const titles = [
        t('content-moderation.publish-requests'),
        t('content-moderation.status.content'),
    ]

    return (
        <>
            <Document
                title={t('content-moderation.publish-requests')}
                containerTitle={t('admin.title')}
            />
            <Container>
                <Section divider>
                    <PageTitle titles={titles} isVisible />

                    <TabMenu
                        label={t('content-moderation.publish-requests')}
                        showBorder
                        options={[
                            {
                                to: 'open',
                                label: `${t('global.open')} (${openTotal})`,
                                isActive: status === 'open',
                                id: 'tab-open',
                                ariaControls: 'panel-open',
                            },
                            {
                                to: 'assigned',
                                label: `${t('admin.assigned')} (${assignedTotal})`,
                                isActive: status === 'assigned',
                                id: 'tab-assigned',
                                ariaControls: 'panel-assigned',
                            },
                            {
                                to: 'published',
                                label: `${t('global.published')} (${publishedTotal})`,
                                isActive: status === 'published',
                                id: 'tab-published',
                                ariaControls: 'panel-published',
                            },
                        ]}
                        style={{ marginBottom: 24 }}
                    />

                    <Outlet />
                </Section>
            </Container>
        </>
    )
}

export default ContentContainer

const GET_PUBLISH_REQUEST_TOTALS = gql`
    query GetPublishRequestTotals {
        open: publishRequests(status: open) {
            total
        }
        assigned: publishRequests(status: assigned) {
            total
        }
        published: publishRequests(status: published) {
            total
        }
    }
`
