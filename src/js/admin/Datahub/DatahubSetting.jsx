import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'

import Flexer from 'js/components/Flexer/Flexer'
import IconButton from 'js/components/IconButton/IconButton'
import ConfirmationModal from 'js/components/Modal/ConfirmationModal'

import DeleteIcon from 'icons/delete.svg'
import EditIcon from 'icons/edit.svg'

import DatahubForm from './DatahubForm'

const DatahubSetting = ({ setting }) => {
    const { key, name, apiUrl } = setting

    const [showEditDataHub, setShowEditDataHub] = useState(false)

    const [showDeleteConfirmationModal, setShowDeleteConfirmationModal] =
        useState(false)

    const [deleteExternalContentSource] = useMutation(
        DELETE_DATAHUB_EXTERNAL_CONTENT_SOURCE,
    )

    const deleteDatahubSetting = () => {
        deleteExternalContentSource({
            variables: {
                key,
            },
            refetchQueries: ['GetDatahubSettings'],
        })
            .then(() => {
                setShowDeleteConfirmationModal(false)
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    const { t } = useTranslation()

    return (
        <tr>
            <td>{name}</td>
            <td>{apiUrl}</td>
            <td>
                <Flexer
                    gutter="small"
                    divider="normal"
                    justifyContent="flex-end"
                    style={{ marginLeft: 'auto' }}
                >
                    <IconButton
                        size="large"
                        variant="secondary"
                        onClick={() => setShowEditDataHub(true)}
                        tooltip={t('action.edit')}
                    >
                        <EditIcon />
                    </IconButton>
                    <DatahubForm
                        setting={setting}
                        isVisible={showEditDataHub}
                        onClose={() => setShowEditDataHub(false)}
                    />

                    <IconButton
                        size="large"
                        variant="secondary"
                        onClick={() => setShowDeleteConfirmationModal(true)}
                        tooltip={t('action.delete')}
                    >
                        <DeleteIcon />
                    </IconButton>
                    <ConfirmationModal
                        title={t('admin.delete-source')}
                        isVisible={showDeleteConfirmationModal}
                        onConfirm={deleteDatahubSetting}
                        onCancel={() => setShowDeleteConfirmationModal(false)}
                    >
                        {t('admin.delete-source-confirm')}
                    </ConfirmationModal>
                </Flexer>
            </td>
        </tr>
    )
}

const DELETE_DATAHUB_EXTERNAL_CONTENT_SOURCE = gql`
    mutation DeleteDatahubExternalContentSource($key: String!) {
        deleteExternalContentSource(key: $key) {
            edges {
                key
                name
            }
        }
    }
`

export default DatahubSetting
