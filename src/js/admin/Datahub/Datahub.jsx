import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'

import PageTitle from 'js/admin/components/PageTitle'
import SettingContainer from 'js/admin/layout/SettingContainer'
import Button from 'js/components/Button/Button'
import Document from 'js/components/Document'
import { Container } from 'js/components/Grid/Grid'
import Section from 'js/components/Section/Section'
import Table from 'js/components/Table'

import DatahubForm from './DatahubForm'
import DatahubSetting from './DatahubSetting'

const Datahub = () => {
    const { t } = useTranslation()

    const [showAddDataHub, setShowAddDataHub] = useState(false)

    const { data } = useQuery(DATAHUB_SETTINGS)
    const datahubSettings = data?.externalContentSources?.edges || []

    if (!data) return null

    return (
        <>
            <Document
                title={t('admin.datahub')}
                containerTitle={t('admin.title')}
            />

            <Container size="small">
                <Section>
                    <PageTitle>{t('admin.datahub')}</PageTitle>

                    <SettingContainer
                        title="admin.external-sources"
                        helper="admin.external-sources-helper"
                    />

                    <Table rowHeight="normal">
                        <thead>
                            <tr>
                                <th>{t('global.name')}</th>
                                <th>{t('admin.api-url')}</th>
                            </tr>
                        </thead>
                        <tbody>
                            {datahubSettings.map((setting) => (
                                <DatahubSetting
                                    key={setting.key}
                                    setting={setting}
                                />
                            ))}
                        </tbody>
                    </Table>

                    <Button
                        size="small"
                        variant="tertiary"
                        onClick={() => setShowAddDataHub(true)}
                        style={{ marginTop: '1rem' }}
                    >
                        {t('admin.add-source')}
                    </Button>

                    <DatahubForm
                        isVisible={showAddDataHub}
                        onClose={() => setShowAddDataHub(false)}
                    />
                </Section>
            </Container>
        </>
    )
}

const DATAHUB_SETTINGS = gql`
    query GetDatahubSettings {
        externalContentSources(handler: datahub) {
            edges {
                key
                name
                pluralName
                ... on DatahubContentSource {
                    apiUrl
                    frontendUrl
                    batchSize
                }
            }
        }
    }
`

export default Datahub
