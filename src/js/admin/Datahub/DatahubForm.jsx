import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'

import Button from 'js/components/Button/Button'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import Modal from 'js/components/Modal/Modal'
import Spacer from 'js/components/Spacer/Spacer'

const DatahubForm = ({ setting, isVisible, onClose }) => {
    const { t } = useTranslation()

    const key = setting?.key

    const defaultValues = {
        name: setting?.name || '',
        pluralName: setting?.pluralName || '',
        apiUrl: setting?.apiUrl || '',
        frontendUrl: setting?.frontendUrl || '',
        batchSize: setting?.batchSize || 0,
    }

    const {
        control,
        handleSubmit,
        formState: { errors, isValid, isSubmitting },
    } = useForm({
        mode: 'onChange',
        defaultValues,
    })

    const [updateDatahubSettings] = useMutation(
        key
            ? UPDATE_DATAHUB_EXTERNAL_CONTENT_SOURCE
            : ADD_DATAHUB_EXTERNAL_CONTENT_SOURCE,
        { refetchQueries: ['GetDatahubSettings'] },
    )

    const [mutateErrors, setMutateErrors] = useState()

    const onSubmit = async ({
        name,
        pluralName,
        apiUrl,
        frontendUrl,
        batchSize,
    }) => {
        setMutateErrors()
        await updateDatahubSettings({
            variables: {
                input: {
                    key,
                    name,
                    pluralName,
                    apiUrl,
                    frontendUrl,
                    batchSize,
                },
            },
        })
            .then(() => {
                onClose()
            })
            .catch((errors) => {
                console.error(errors)
                setMutateErrors(errors)
            })
    }

    return (
        <Modal
            size="small"
            isVisible={isVisible}
            title={setting ? t('admin.edit-source') : t('admin.add-source')}
        >
            <form onSubmit={handleSubmit(onSubmit)}>
                <Spacer spacing="small">
                    <FormItem
                        control={control}
                        type="text"
                        name="name"
                        label={t('global.name')}
                        errors={errors}
                        required
                    />

                    <FormItem
                        control={control}
                        type="text"
                        name="pluralName"
                        label={t('global.plural-name')}
                        errors={errors}
                        required
                    />

                    <FormItem
                        control={control}
                        type="text"
                        name="apiUrl"
                        label={t('admin.api-url')}
                        errors={errors}
                        required
                    />

                    <FormItem
                        control={control}
                        type="text"
                        name="frontendUrl"
                        label={t('admin.frontend-url')}
                        errors={errors}
                        required
                    />

                    <FormItem
                        control={control}
                        type="number"
                        min={0}
                        name="batchSize"
                        label={t('admin.batch-size')}
                        errors={errors}
                        required
                    />

                    <Errors errors={mutateErrors} />
                </Spacer>

                <Flexer mt>
                    <Button size="normal" variant="tertiary" onClick={onClose}>
                        {t('action.cancel')}
                    </Button>
                    <Button
                        type="submit"
                        size="normal"
                        variant="primary"
                        disabled={!isValid}
                        loading={isSubmitting}
                    >
                        {t('action.save')}
                    </Button>
                </Flexer>
            </form>
        </Modal>
    )
}

const ADD_DATAHUB_EXTERNAL_CONTENT_SOURCE = gql`
    mutation AddDatahubExternalContentSource(
        $input: AddDatahubContentSourceInput!
    ) {
        addDatahubExternalContentSource(input: $input) {
            edges {
                name
            }
        }
    }
`

const UPDATE_DATAHUB_EXTERNAL_CONTENT_SOURCE = gql`
    mutation EditDatahubExternalContentSource(
        $input: UpdateDatahubContentSourceInput!
    ) {
        editDatahubExternalContentSource(input: $input) {
            edges {
                name
            }
        }
    }
`

export default DatahubForm
