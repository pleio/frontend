import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { subDays } from 'date-fns'
import { useIsMount, useLocalStorage } from 'helpers'

import FetchMore from 'js/components/FetchMore'
import FilterWrapper from 'js/components/FilterWrapper'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Select from 'js/components/Select/Select'
import Table from 'js/components/Table'

const today = new Date()
const dates = {
    day: {
        dateTimeFrom: subDays(today, 1),
    },
    week: {
        dateTimeFrom: subDays(today, 7),
    },
    month: {
        dateTimeFrom: subDays(today, 30),
    },
    all: {
        dateTimeFrom: null,
    },
}

const TopSearches = () => {
    const { t } = useTranslation()

    const [period, setPeriod] = useLocalStorage(
        'admin-dashboard-top-searches-period',
        'all',
    )

    const [queryLimit, setQueryLimit] = useState(5)
    const { loading, data, fetchMore, refetch } = useQuery(QUERY, {
        variables: {
            dateTimeFrom: dates[period]?.dateTimeFrom,
            offset: 0,
            limit: queryLimit,
        },
    })

    const isMount = useIsMount()

    useEffect(() => {
        if (isMount) {
            // Prevents the wrong height being set on the widget (because of caching). Use useEffect, not useLayoutEffect.
            refetch()
        }
    }, [])

    const options = [
        {
            value: 'day',
            label: t('date.past-24-hours'),
        },
        {
            value: 'week',
            label: t('date.past-days', { count: 7 }),
        },
        {
            value: 'month',
            label: t('date.past-days', { count: 30 }),
        },
        {
            value: 'all',
            label: t('date.all-time'),
        },
    ]

    return (
        <>
            <FilterWrapper style={{ padding: '0 20px', marginBottom: '8px' }}>
                <Select
                    name="exportableContentTypes"
                    label={t('date.period')}
                    options={options}
                    value={period}
                    onChange={setPeriod}
                />
            </FilterWrapper>
            {!data || loading ? (
                <LoadingSpinner />
            ) : (
                <FetchMore
                    edges={data.searchJournal.edges}
                    getMoreResults={(data) => data.searchJournal.edges}
                    fetchMore={fetchMore}
                    fetchType="click"
                    fetchCount={10}
                    setLimit={setQueryLimit}
                    maxLimit={data.searchJournal.total}
                >
                    <Table edgePadding wrap="word" rowHeight="small">
                        <thead>
                            <tr>
                                <th style={{ width: '100%' }}>
                                    {t('global.query')}
                                </th>
                                <th
                                    style={{
                                        textAlign: 'right',
                                    }}
                                >
                                    {t('global.count')}
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {data.searchJournal.edges.map((search) => (
                                <tr key={search.query}>
                                    <td>
                                        <Link
                                            className="TableLinkBlock"
                                            to={`/search?q=${search.query}`}
                                            style={{ padding: '8px 0' }}
                                        >
                                            {search.query}
                                        </Link>
                                    </td>
                                    <td
                                        style={{
                                            whiteSpace: 'nowrap',
                                            textAlign: 'right',
                                        }}
                                    >
                                        {search.count}
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </FetchMore>
            )}
        </>
    )
}

const QUERY = gql`
    query SiteDashboardTopSearches(
        $dateTimeFrom: DateTime
        $dateTimeTo: DateTime
        $offset: Int
        $limit: Int
    ) {
        searchJournal(
            dateTimeFrom: $dateTimeFrom
            dateTimeTo: $dateTimeTo
            offset: $offset
            limit: $limit
        ) {
            total
            edges {
                query
                count
            }
        }
    }
`

export default TopSearches
