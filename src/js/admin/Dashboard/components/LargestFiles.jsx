import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { humanFileSize, useIsMount } from 'helpers'

import FetchMore from 'js/components/FetchMore'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Table from 'js/components/Table'
import FileFolderIcon from 'js/files/components/FileFolderIcon'
import getIconNameByMimetype from 'js/files/helpers/getIconNameByMimetype'

const LargestFiles = () => {
    const { t } = useTranslation()

    const [queryLimit, setQueryLimit] = useState(5)
    const { loading, data, fetchMore, refetch } = useQuery(QUERY, {
        variables: {
            orderBy: 'size',
            orderDirection: 'desc',
            offset: 0,
            limit: queryLimit,
        },
    })

    const isMount = useIsMount()

    useEffect(() => {
        if (isMount) {
            // Prevents the wrong height being set on the widget (because of caching). Use useEffect, not useLayoutEffect.
            refetch()
        }
    }, [])

    return (
        <>
            {!data || loading ? (
                <LoadingSpinner />
            ) : (
                <FetchMore
                    edges={data.files.edges}
                    getMoreResults={(data) => data.files.edges}
                    fetchMore={fetchMore}
                    fetchType="click"
                    fetchCount={10}
                    setLimit={setQueryLimit}
                    maxLimit={data.files.total}
                >
                    <Table edgePadding wrap="word" rowHeight="small">
                        <thead>
                            <tr>
                                <th style={{ width: '100%' }}>
                                    {t('global.name')}
                                </th>
                                <th
                                    style={{
                                        textAlign: 'right',
                                    }}
                                >
                                    {t('global.size')}
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {data.files.edges.map((file, index) => (
                                <tr key={`${index}-${file.title}`}>
                                    <td>
                                        <Link
                                            className="TableLinkBlock"
                                            to={file.url}
                                        >
                                            <FileFolderIcon
                                                name={getIconNameByMimetype(
                                                    file.mimeType,
                                                )}
                                            />
                                            <div
                                                style={{
                                                    padding: '8px 0 8px 12px',
                                                }}
                                            >
                                                {file.title}
                                            </div>
                                        </Link>
                                    </td>
                                    <td
                                        style={{
                                            whiteSpace: 'nowrap',
                                            textAlign: 'right',
                                        }}
                                    >
                                        {humanFileSize(file.size)}
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </FetchMore>
            )}
        </>
    )
}

const QUERY = gql`
    query SiteDashboard(
        $offset: Int
        $limit: Int
        $orderBy: String
        $orderDirection: String
    ) {
        files(
            typeFilter: ["file"]
            offset: $offset
            limit: $limit
            orderBy: $orderBy
            orderDirection: $orderDirection
        ) {
            total
            edges {
                guid
                ... on File {
                    title
                    size
                    url
                    mimeType
                }
            }
        }
    }
`

export default LargestFiles
