import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'
import { useIsMount } from 'helpers'
import { showDateTime, showShortDate } from 'helpers/date/showDate'

import FetchMore from 'js/components/FetchMore'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Table from 'js/components/Table'
import Tooltip from 'js/components/Tooltip/Tooltip'
import UserLink from 'js/components/UserLink'

const NewestUsers = () => {
    const { t } = useTranslation()

    const [queryLimit, setQueryLimit] = useState(5)
    const { loading, data, fetchMore, refetch } = useQuery(QUERY, {
        variables: {
            orderBy: 'memberSince',
            orderDirection: 'desc',
            offset: 0,
            limit: queryLimit,
        },
    })

    const isMount = useIsMount()

    useEffect(() => {
        if (isMount) {
            // Prevents the wrong height being set on the widget (because of caching). Use useEffect, not useLayoutEffect.
            refetch()
        }
    }, [])

    return (
        <>
            {!data || loading ? (
                <LoadingSpinner />
            ) : (
                <FetchMore
                    edges={data.siteUsers.edges}
                    getMoreResults={(data) => data.siteUsers.edges}
                    fetchMore={fetchMore}
                    fetchType="click"
                    fetchCount={10}
                    setLimit={setQueryLimit}
                    maxLimit={data.siteUsers.total}
                >
                    <Table edgePadding wrap="word" rowHeight="small">
                        <thead>
                            <tr>
                                <th style={{ width: '100%' }}>
                                    {t('global.name')}
                                </th>
                                <th
                                    style={{
                                        textAlign: 'right',
                                    }}
                                >
                                    {t('global.date')}
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {data.siteUsers.edges.map((user) => (
                                <tr key={user.guid}>
                                    <td>
                                        <UserLink
                                            className="NewestUsersLink"
                                            entity={user}
                                            avatarSize="small"
                                        />
                                    </td>
                                    <td
                                        style={{
                                            textAlign: 'right',
                                            whiteSpace: 'nowrap',
                                        }}
                                    >
                                        <Tooltip
                                            content={showDateTime(
                                                user.memberSince,
                                            )}
                                        >
                                            <time
                                                dateTime={user.memberSince}
                                                aria-label={showDateTime(
                                                    user.memberSince,
                                                )}
                                            >
                                                {showShortDate(
                                                    user.memberSince,
                                                )}
                                            </time>
                                        </Tooltip>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </FetchMore>
            )}
        </>
    )
}

const QUERY = gql`
    query SiteDashboardNewestUsers(
        $offset: Int
        $limit: Int
        $orderBy: SiteUsersOrderBy
        $orderDirection: OrderDirection
    ) {
        siteUsers(
            offset: $offset
            limit: $limit
            orderBy: $orderBy
            orderDirection: $orderDirection
        ) {
            total
            edges {
                guid
                name
                icon
                url
                memberSince
            }
        }
    }
`

export default NewestUsers
