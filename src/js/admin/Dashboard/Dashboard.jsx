import React from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { startOfDay, subDays } from 'date-fns'
import { humanFileSize } from 'helpers'
import styled from 'styled-components'

import Widget, { WidgetStat } from 'js/admin/components/Widget'
import Card, { CardContent } from 'js/components/Card/Card'
import Document from 'js/components/Document'
import Flexer from 'js/components/Flexer/Flexer'
import { Col, Container, Row } from 'js/components/Grid/Grid'
import { H3 } from 'js/components/Heading'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Section from 'js/components/Section/Section'
import TabMenu from 'js/components/TabMenu'
import TabPage from 'js/components/TabPage'
import Tooltip from 'js/components/Tooltip/Tooltip'
import { START_OF_CONTENT_ELEMENT_ID } from 'js/lib/constants'

import CloudIcon from 'icons/cloud.svg'
import FaceIcon from 'icons/face.svg'
import StatisticsIcon from 'icons/statistics.svg'

import LargestFiles from './components/LargestFiles'
import NewestUsers from './components/NewestUsers'
import TopSearches from './components/TopSearches'

const Wrapper = styled(Section)`
    ${H3} {
        text-align: center;
    }

    ${Card} {
        margin-bottom: 20px;
    }

    .DashBoardCategory {
        display: flex;
        align-items: center;
        justify-content: center;

        svg {
            margin-right: 8px;
        }
    }

    .NewestUsersLink {
        /* Grow height to enlarge clickable area  */
        min-height: 100%; // height doesn't work properly
        padding: 8px 0;
    }
`

const Dashboard = () => {
    const { t } = useTranslation()

    const hash = useLocation().hash?.replace('#', '') || ''

    const { data } = useQuery(GET_SITE_DASHBOARD)

    const memberSince = subDays(startOfDay(new Date()), 31)

    const { data: dataNewUsers } = useQuery(GET_SITE_DASHBOARD_NEW_USERS, {
        variables: {
            memberSince,
        },
    })

    if (!data || !dataNewUsers) return <LoadingSpinner />

    const { site, siteStats, siteUsers } = data

    const startOfContentHash = `${START_OF_CONTENT_ELEMENT_ID}`

    return (
        <>
            <Document
                title={t('admin.dashboard')}
                containerTitle={t('admin.title')}
            />
            <Wrapper backgroundColor="white">
                <Container size="large">
                    <Row>
                        <Col mobileLandscapeUp={1 / 2}>
                            <Widget
                                icon={<CloudIcon />}
                                title={t('admin.storage')}
                            >
                                <WidgetStat
                                    title={humanFileSize(siteStats.dbUsage)}
                                    subtitle={t('admin.database')}
                                />
                                <WidgetStat
                                    title={humanFileSize(
                                        siteStats.fileDiskUsage,
                                    )}
                                    subtitle={t('global.files')}
                                />
                            </Widget>
                        </Col>
                        <Col mobileLandscapeUp={1 / 2}>
                            <Widget
                                icon={<FaceIcon />}
                                title={t('admin.users')}
                            >
                                <WidgetStat
                                    title={siteUsers.total}
                                    subtitle={t('admin.total')}
                                />
                                <Flexer
                                    justifyContent="space-evenly"
                                    style={{ width: '100%' }}
                                >
                                    <WidgetStat
                                        title={site.usersOnline}
                                        subtitle={t('admin.online')}
                                    />
                                    <Tooltip
                                        content={t('admin.new-users-helper')}
                                    >
                                        <div>
                                            <WidgetStat
                                                title={
                                                    dataNewUsers.siteUsers.total
                                                }
                                                subtitle={t('admin.new-users')}
                                            />
                                        </div>
                                    </Tooltip>
                                </Flexer>
                            </Widget>
                        </Col>
                    </Row>
                    <Card>
                        <CardContent style={{ paddingBottom: 0 }}>
                            <H3
                                as="h2"
                                className="DashBoardCategory"
                                style={{ marginBottom: '8px' }}
                            >
                                <StatisticsIcon />
                                {t('admin.statistics')}
                            </H3>
                        </CardContent>
                        <TabMenu
                            label={t('admin.statistics')}
                            options={[
                                {
                                    link: '#top-searches',
                                    isActive:
                                        !hash ||
                                        hash === startOfContentHash ||
                                        hash === 'top-searches',
                                    label: t('admin.top-searches'),
                                    id: 'tab-top-searches',
                                    ariaControls: 'top-searches',
                                },
                                {
                                    link: '#largest-files',
                                    isActive: hash === 'largest-files',
                                    label: t('admin.largest-files'),
                                    id: 'tab-largest-files',
                                    ariaControls: 'largest-files',
                                },
                                {
                                    link: '#newest-users',
                                    isActive: hash === 'newest-users',
                                    label: t('admin.newest-users'),
                                    id: 'tab-newest-users',
                                    ariaControls: 'newest-users',
                                },
                            ]}
                            edgePadding
                            showBorder
                            sticky
                        />
                        <TabPage
                            id={`tabpanel-${hash || 'top-searches'}`}
                            ariaLabelledby={`tab-${hash || 'top-searches'}`}
                            visible
                            style={{ padding: '16px 0 8px' }}
                        >
                            {(!hash ||
                                hash === startOfContentHash ||
                                hash === 'top-searches') && <TopSearches />}
                            {hash === 'largest-files' && <LargestFiles />}
                            {hash === 'newest-users' && <NewestUsers />}
                        </TabPage>
                    </Card>
                </Container>
            </Wrapper>
        </>
    )
}

const GET_SITE_DASHBOARD = gql`
    query SiteDashboard {
        site {
            guid
            usersOnline
        }
        siteStats {
            dbUsage
            fileDiskUsage
        }
        siteUsers {
            total
        }
    }
`

const GET_SITE_DASHBOARD_NEW_USERS = gql`
    query SiteDashboardNewUsers($memberSince: DateTime) {
        siteUsers(memberSince: $memberSince) {
            total
        }
    }
`

export default Dashboard
