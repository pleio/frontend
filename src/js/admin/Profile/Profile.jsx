import React, { useEffect, useState } from 'react'
import { DragDropContext, Droppable } from 'react-beautiful-dnd'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { produce } from 'immer'
import compose from 'lodash.flowright'

import PageTitle from 'js/admin/components/PageTitle'
import SaveSection from 'js/admin/components/SaveSection'
import SettingContainer from 'js/admin/layout/SettingContainer'
import Button from 'js/components/Button/Button'
import Document from 'js/components/Document'
import { Container } from 'js/components/Grid/Grid'
import HideVisually from 'js/components/HideVisually/HideVisually'
import Modal from 'js/components/Modal/Modal'
import Section from 'js/components/Section/Section'
import Table from 'js/components/Table'

import CreateFieldForm from './components/CreateFieldForm'
import ProfileField from './components/ProfileField'
import ProfileSection from './components/ProfileSection'

const Profile = ({ mutateEditSections, data }) => {
    const { t } = useTranslation()

    const [profileFields, setProfileFields] = useState()

    const [profileSections, setProfileSections] = useState()

    const [showAddFieldModal, setShowAddFieldModal] = useState(false)
    const toggleAddFieldModal = () => setShowAddFieldModal(!showAddFieldModal)

    const siteSettings = data?.siteSettings || {}

    useEffect(() => {
        if (siteSettings.profileSections?.length) {
            setProfileSections(
                siteSettings.profileSections.map((section) => {
                    return {
                        name: section.name,
                        profileFieldGuids: section.profileFieldGuids,
                    }
                }),
            )
        } else {
            // At least have one section to drag profile fields into
            setProfileSections([{ name: '', profileFieldGuids: [] }])
        }
    }, [siteSettings.profileFields, siteSettings.profileSections])

    useEffect(() => {
        // Update profile view
        if (siteSettings.profileFields) {
            setProfileFields(siteSettings.profileFields)
        }
    }, [siteSettings.profileFields])

    if (!data || !data.siteSettings) return null

    const handleSaveSections = (resolve, reject) => {
        mutateEditSections({
            variables: {
                input: {
                    profileSections,
                },
            },
        })
            .then(() => {
                resolve()
            })
            .catch((error) => {
                console.error(error)
                reject(new Error(error))
            })
    }

    const onDragEnd = (result) => {
        if (!result.destination) return

        if (result.type === 'SECTION') {
            const sourceIndex = result.source.index
            const destinationIndex = result.destination.index

            setProfileSections(
                produce((newState) => {
                    newState[destinationIndex] = profileSections[sourceIndex]
                    newState[sourceIndex] = profileSections[destinationIndex]
                }),
            )
        }

        if (result.type === 'FIELD') {
            const destinationIndex = result.destination.index
            const sourceSectionIndex = result.source.droppableId
            const destinationSectionIndex = result.destination.droppableId
            const fieldGuid = result.draggableId

            setProfileSections(
                produce((newState) => {
                    newState[sourceSectionIndex].profileFieldGuids.splice(
                        newState[sourceSectionIndex].profileFieldGuids.indexOf(
                            fieldGuid,
                        ),
                        1,
                    )
                    newState[destinationSectionIndex].profileFieldGuids.splice(
                        destinationIndex,
                        0,
                        fieldGuid,
                    )
                }),
            )
        }
    }

    const handleAddSection = () => {
        setProfileSections([
            ...profileSections,
            {
                name: '',
                profileFieldGuids: [],
            },
        ])
    }

    const handleEditSection = (value, index) => {
        setProfileSections(
            produce((newState) => {
                newState[index].name = value
            }),
        )
    }

    const handleRemoveSection = (index) => {
        setProfileFields(
            produce((newState) => {
                profileSections[index].profileFieldGuids.forEach(
                    (fieldGuid) => {
                        newState.find(
                            (el) => el.guid === fieldGuid,
                        ).showOnProfile = false
                    },
                )
            }),
        )

        setProfileSections(
            produce((newState) => {
                newState.splice(index, 1)
            }),
        )
    }

    const handleAddToProfile = (fieldGuid) => {
        setProfileFields(
            produce((newState) => {
                newState.find((el) => el.guid === fieldGuid).showOnProfile =
                    true
            }),
        )

        setProfileSections(
            produce((newState) => {
                newState[0].profileFieldGuids.splice(
                    newState[0].profileFieldGuids.length,
                    0,
                    fieldGuid,
                )
            }),
        )
    }

    const handleRemoveFromProfile = (fieldGuid) => {
        setProfileFields(
            produce((newState) => {
                newState.find((el) => el.guid === fieldGuid).showOnProfile =
                    false
            }),
        )

        setProfileSections(
            produce((newState) => {
                newState.forEach((section) => {
                    const fieldIndex =
                        section.profileFieldGuids.indexOf(fieldGuid)
                    fieldIndex !== -1 &&
                        section.profileFieldGuids.splice(fieldIndex, 1)
                })
            }),
        )
    }

    const handleCheckField = (fieldGuid) => {
        setProfileFields(
            produce((newState) => {
                newState.find((el) => el.guid === fieldGuid).showOnProfile =
                    true
            }),
        )
    }

    const typeOptions = [
        {
            value: 'text_field',
            label: t('profile.text'),
            fieldType: 'textField',
        },
        {
            value: 'email_field',
            label: t('profile.email'),
            fieldType: 'emailField',
        },
        {
            value: 'tel_field',
            label: t('profile.phone'),
            fieldType: 'charField',
        },
        {
            value: 'url_field',
            label: t('profile.url'),
            fieldType: 'urlField',
        },
        {
            value: 'html_field',
            label: t('profile.long-text'),
            fieldType: 'htmlField',
        },
        {
            value: 'select_field',
            label: t('profile.select'),
            fieldType: 'selectField',
        },
        {
            value: 'multi_select_field',
            label: t('profile.multi-select'),
            fieldType: 'multiSelectField',
        },
        {
            value: 'date_field',
            label: t('profile.date'),
            fieldType: 'dateField',
        },
    ]

    return (
        <>
            <Document
                title={t('admin.profile')}
                containerTitle={t('admin.title')}
            />
            <Container size="small">
                <Section divider>
                    <PageTitle>{t('admin.profile')}</PageTitle>

                    <SettingContainer
                        title={t('admin.profile-fields')}
                        helper={t('admin.profile-fields-helper')}
                        inputWidth="full"
                    >
                        {profileFields && (
                            <Table>
                                <thead>
                                    <tr>
                                        <th style={{ width: '44px' }}>
                                            <HideVisually>
                                                {t('admin.show-on-profile')}
                                            </HideVisually>
                                        </th>
                                        <th style={{ width: '50%' }}>
                                            {t('global.label')}
                                        </th>
                                        <th style={{ width: '50%' }}>
                                            {t('global.type')}
                                        </th>
                                        <th style={{ textAlign: 'center' }}>
                                            <HideVisually>
                                                {t('global.actions')}
                                            </HideVisually>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {profileFields.map((field) => (
                                        <ProfileField
                                            key={field.guid}
                                            entity={field}
                                            profileFieldValidators={
                                                siteSettings.profileFieldValidators
                                            }
                                            profileSyncEnabled={
                                                siteSettings.profileSyncEnabled
                                            }
                                            onAdd={handleAddToProfile}
                                            onRemove={handleRemoveFromProfile}
                                            typeOptions={typeOptions}
                                        />
                                    ))}
                                </tbody>
                            </Table>
                        )}
                    </SettingContainer>

                    <Button
                        size="small"
                        variant="tertiary"
                        onClick={toggleAddFieldModal}
                    >
                        {t('admin.create-field')}
                    </Button>
                </Section>
                <Section divider>
                    <SettingContainer
                        title={t('admin.profile-page')}
                        helper={t('admin.profile-page-helper')}
                    />

                    <DragDropContext onDragEnd={onDragEnd}>
                        <Droppable
                            droppableId="droppable-section"
                            type="SECTION"
                        >
                            {(provided) => (
                                <div
                                    ref={provided.innerRef}
                                    {...provided.droppableProps}
                                >
                                    {profileFields &&
                                        profileSections &&
                                        profileSections.map(
                                            (section, index) => (
                                                <ProfileSection
                                                    key={`${section}-${index}`}
                                                    index={index}
                                                    entity={section}
                                                    profileFields={
                                                        profileFields
                                                    }
                                                    onChange={handleEditSection}
                                                    onRemove={
                                                        handleRemoveSection
                                                    }
                                                    onCheckField={
                                                        handleCheckField
                                                    }
                                                    onRemoveField={
                                                        handleRemoveFromProfile
                                                    }
                                                />
                                            ),
                                        )}
                                    {provided.placeholder}
                                </div>
                            )}
                        </Droppable>
                    </DragDropContext>

                    <Button
                        size="small"
                        variant="tertiary"
                        onClick={handleAddSection}
                        style={{ marginBottom: '24px' }}
                    >
                        {t('admin.profile-section-add')}
                    </Button>
                    <SaveSection
                        isVisible={true}
                        onSubmit={handleSaveSections}
                    />
                </Section>
            </Container>

            <Modal
                isVisible={showAddFieldModal}
                title={t('admin.create-field')}
                size="small"
                onClose={toggleAddFieldModal}
            >
                <CreateFieldForm
                    onClose={toggleAddFieldModal}
                    typeOptions={typeOptions}
                />
            </Modal>
        </>
    )
}

const Query = gql`
    query SiteProfileSettings {
        siteSettings {
            profileFields {
                guid
                key
                name
                autocomplete
                isEditable
                isOnVcard
                isFilterable
                isFilter
                isInOverview
                isInAutoGroupMembership
                fieldType
                fieldOptions
                profileFieldValidator {
                    id
                }
            }
            profileSections {
                name
                profileFieldGuids
            }
            profileFieldValidators {
                id
                name
            }
            profileSyncEnabled
        }
    }
`

const EditSectionsMutation = gql`
    mutation EditSiteSetting($input: editSiteSettingInput!) {
        editSiteSetting(input: $input) {
            siteSettings {
                profileSections {
                    name
                    profileFieldGuids
                }
            }
        }
    }
`

export default compose(
    graphql(Query),
    graphql(EditSectionsMutation, { name: 'mutateEditSections' }),
)(Profile)
