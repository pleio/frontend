import React from 'react'
import { Draggable } from 'react-beautiful-dnd'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import IconButton from 'js/components/IconButton/IconButton'

import RemoveIcon from 'icons/cross.svg'
import MoveIcon from 'icons/move.svg'

const Wrapper = styled.div`
    display: flex;
    align-items: center;
    position: relative;
    width: 100%;
    background-color: white;
    box-shadow: 0 0 0 1px ${(p) => p.theme.color.grey[30]};
    overflow: hidden;

    .ProfileFieldName {
        flex-grow: 1;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        font-weight: ${(p) => p.theme.font.weight.semibold};
    }
`

const ProfileSectionField = ({ index, entity, onRemove }) => {
    if (!entity) return null

    const handleRemove = () => {
        onRemove(entity.guid)
    }

    const { t } = useTranslation()

    return (
        <Draggable draggableId={entity.guid} index={index}>
            {(provided) => (
                <>
                    <Wrapper
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                    >
                        <IconButton
                            as="div"
                            size="large"
                            variant="secondary"
                            radiusStyle="none"
                            aria-label={t('admin.profile-field-move', {
                                name: entity.name,
                            })}
                        >
                            <MoveIcon />
                        </IconButton>
                        <div className="ProfileFieldName">{entity.name}</div>
                        <IconButton
                            size="large"
                            variant="secondary"
                            radiusStyle="none"
                            aria-label={t('admin.profile-field-remove', {
                                name,
                            })}
                            onClick={handleRemove}
                        >
                            <RemoveIcon />
                        </IconButton>
                    </Wrapper>
                    {provided.placeholder}
                </>
            )}
        </Draggable>
    )
}

export default ProfileSectionField
