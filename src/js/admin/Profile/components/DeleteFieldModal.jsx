import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import Modal from 'js/components/Modal/Modal'

const DeleteFieldModal = ({ mutate, isVisible, onClose, entity }) => {
    const { t } = useTranslation()

    const handleSubmit = () => {
        return new Promise((resolve, reject) => {
            mutate({
                variables: {
                    input: {
                        guid: entity.guid,
                    },
                },
                refetchQueries: ['SiteProfileSettings'],
            })
                .then(() => {
                    onClose()
                    resolve()
                })
                .catch((errors) => {
                    reject(new Error(errors))
                })
        })
    }

    return (
        <Modal
            isVisible={isVisible}
            title={t('admin.profile-field-delete', {
                name: entity.name,
            })}
            size="small"
            onClose={onClose}
        >
            {t('admin.profile-field-delete-confirm')}
            <Flexer mt>
                <Button
                    size="normal"
                    variant="tertiary"
                    type="button"
                    onClick={onClose}
                >
                    {t('action.no-back')}
                </Button>
                <Button size="normal" variant="primary" onHandle={handleSubmit}>
                    {t('action.yes-delete')}
                </Button>
            </Flexer>
        </Modal>
    )
}

const Mutation = gql`
    mutation DeleteSiteSettingProfileField(
        $input: deleteSiteSettingProfileFieldInput!
    ) {
        deleteSiteSettingProfileField(input: $input) {
            success
        }
    }
`

export default graphql(Mutation)(DeleteFieldModal)
