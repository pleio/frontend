import React from 'react'
import { useForm } from 'react-hook-form'
import { Trans, useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import Spacer from 'js/components/Spacer/Spacer'

const EditFieldModal = ({
    mutate,
    onClose,
    entity,
    profileFieldValidators,
    profileSyncEnabled,
}) => {
    const { t } = useTranslation()

    const submit = async ({
        key,
        profileFieldValidatorId,
        fieldOptions,
        isEditable,
        isOnVcard,
        isFilter,
        isInOverview,
        isInAutoGroupMembership,
        name,
        autocomplete,
    }) => {
        await mutate({
            variables: {
                input: {
                    guid: entity.guid,
                    key,
                    name,
                    autocomplete,
                    profileFieldValidatorId,
                    fieldOptions: fieldOptions
                        ? fieldOptions.split(',').map((item) => item.trim())
                        : [],

                    isEditable: !isEditable,
                    isOnVcard,
                    isFilter,
                    isInOverview,
                    isInAutoGroupMembership,
                },
            },
            refetchQueries: ['SiteProfileSettings'],
        })
            .then(() => {
                onClose()
            })
            .catch((errors) => {
                console.error(errors)
                errors.graphQLErrors.forEach((error) => {
                    if (error.message === 'key_already_in_use') {
                        setError('key', {
                            message: t('error.key-in-use'),
                        })
                    }
                })
            })
    }

    const showOptions =
        entity.fieldType === 'selectField' ||
        entity.fieldType === 'multiSelectField'

    const validatorOptions =
        entity.fieldType === 'textField' &&
        profileFieldValidators.length > 0 &&
        profileFieldValidators.map((validator) => {
            return {
                value: validator.id,
                label: validator.name,
            }
        })

    const defaultValues = {
        key: entity.key,
        profileFieldValidatorId:
            entity.profileFieldValidator && entity.profileFieldValidator.id,
        fieldOptions: entity.fieldOptions.join(','),
        isEditable: !entity.isEditable,
        isOnVcard: entity.isOnVcard,
        isFilter: entity.isFilter,
        isInOverview: entity.isInOverview,
        isInAutoGroupMembership: entity.isInAutoGroupMembership,
        name: entity.name,
        autocomplete: entity.autocomplete,
    }

    const {
        control,
        handleSubmit,
        setError,
        formState: { errors, isDirty, isSubmitting, isSubmitSuccessful },
    } = useForm({ defaultValues })

    return (
        <Spacer as="form" spacing="small" onSubmit={handleSubmit(submit)}>
            <FormItem
                control={control}
                type="text"
                name="name"
                label={t('global.label')}
                errors={errors}
            />
            <FormItem
                control={control}
                type="text"
                name="autocomplete"
                label={t('settings.autocomplete-label')}
                helper={
                    <Trans i18nKey={'settings.autocomplete-helper'}>
                        <a
                            href={t('settings.autocomplete-resource')}
                            target="_blank"
                            rel="noreferrer"
                        >
                            Visit the w3c website
                        </a>{' '}
                        for more information about the autocomplete attribute.
                    </Trans>
                }
                errors={errors}
                style={{ marginBottom: 16 }}
            />

            {profileSyncEnabled && (
                <FormItem
                    control={control}
                    type="text"
                    name="key"
                    label={t('form.key')}
                    errors={errors}
                />
            )}

            {validatorOptions && (
                <FormItem
                    control={control}
                    type="select"
                    name="profileFieldValidatorId"
                    options={validatorOptions}
                    label={t('admin.profile-field-validator')}
                    isClearable
                />
            )}

            {showOptions && (
                <FormItem
                    control={control}
                    type="text"
                    name="fieldOptions"
                    label={t('admin.profile-field-options')}
                    helper={t('admin.profile-field-options-helper')}
                />
            )}

            <div>
                <FormItem
                    control={control}
                    type="checkbox"
                    name="isEditable"
                    label={t('admin.profile-field-lock')}
                    size="small"
                />

                {entity.fieldType !== 'htmlField' && (
                    <>
                        <FormItem
                            control={control}
                            type="checkbox"
                            name="isOnVcard"
                            label={t('admin.profile-field-card')}
                            size="small"
                        />
                        <FormItem
                            control={control}
                            type="checkbox"
                            name="isFilter"
                            label={t('admin.fields-show-as-filter')}
                            size="small"
                        />
                        <FormItem
                            control={control}
                            type="checkbox"
                            name="isInOverview"
                            label={t('admin.fields-show-as-column')}
                            size="small"
                        />
                        {showOptions && (
                            <FormItem
                                control={control}
                                type="checkbox"
                                name="isInAutoGroupMembership"
                                label={t('admin.auto-group-membership')}
                                size="small"
                            />
                        )}
                    </>
                )}
            </div>

            <Flexer mt>
                <Button
                    size="normal"
                    variant="tertiary"
                    type="button"
                    onClick={onClose}
                >
                    {t('action.cancel')}
                </Button>
                <Button
                    type="submit"
                    size="normal"
                    variant="primary"
                    disabled={!isDirty}
                    loading={isSubmitting}
                    isSubmitted={isSubmitSuccessful}
                >
                    {t('action.save')}
                </Button>
            </Flexer>
        </Spacer>
    )
}

const Mutation = gql`
    mutation EditSiteSettingProfileField(
        $input: editSiteSettingProfileFieldInput!
    ) {
        editSiteSettingProfileField(input: $input) {
            profileItem {
                guid
                name
                autocomplete
                isEditable
                isOnVcard
                isFilter
                isInOverview
                isInAutoGroupMembership
            }
        }
    }
`

export default graphql(Mutation)(EditFieldModal)
