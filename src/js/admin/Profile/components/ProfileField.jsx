import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import IconButton from 'js/components/IconButton/IconButton'
import Modal from 'js/components/Modal/Modal'
import Switch from 'js/components/Switch/Switch'

import DeleteIcon from 'icons/delete.svg'
import EditIcon from 'icons/edit.svg'

import DeleteFieldModal from './DeleteFieldModal'
import EditFieldModal from './EditFieldModal'

const ProfileField = ({
    entity,
    profileFieldValidators,
    profileSyncEnabled,
    onAdd,
    onRemove,
    typeOptions,
}) => {
    const { t } = useTranslation()

    const [showSettingsModal, setShowSettingsModal] = useState(false)
    const toggleSettingsModal = () => setShowSettingsModal(!showSettingsModal)

    const [showDeleteModal, setShowDeleteModal] = useState(false)
    const toggleDeleteModal = () => setShowDeleteModal(!showDeleteModal)

    if (!entity) return null

    const toggleShowOnProfile = (evt) => {
        const { checked } = evt.target
        if (checked) {
            onAdd(guid)
        } else {
            onRemove(guid)
        }
    }

    const { guid, name, fieldType, showOnProfile } = entity

    return (
        <>
            <tr>
                <td>
                    <Switch
                        name={`field-${guid}-showOnProfile`}
                        checked={!!showOnProfile}
                        size="small"
                        style={{ height: '100%' }}
                        onHandle={toggleShowOnProfile}
                    />
                </td>
                <td>{name}</td>
                <td>
                    {
                        typeOptions.filter(
                            (option) => option.fieldType === fieldType,
                        )[0].label
                    }
                </td>
                <td style={{ display: 'flex', alignItems: 'center' }}>
                    <IconButton
                        size="large"
                        variant="secondary"
                        aria-label={t('admin.profile-field-edit', {
                            name,
                        })}
                        onClick={toggleSettingsModal}
                        style={{ marginLeft: '4px' }}
                    >
                        <EditIcon />
                    </IconButton>
                    <IconButton
                        size="large"
                        variant="secondary"
                        aria-label={t('admin.profile-field-delete', {
                            name,
                        })}
                        onClick={toggleDeleteModal}
                        style={{ marginRight: '4px' }}
                    >
                        <DeleteIcon />
                    </IconButton>
                </td>
            </tr>
            <Modal
                isVisible={showSettingsModal}
                title={t('admin.profile-field-edit', {
                    name,
                })}
                size="small"
                onClose={toggleSettingsModal}
            >
                <EditFieldModal
                    onClose={toggleSettingsModal}
                    entity={entity}
                    profileFieldValidators={profileFieldValidators}
                    profileSyncEnabled={profileSyncEnabled}
                />
            </Modal>
            <DeleteFieldModal
                isVisible={showDeleteModal}
                onClose={toggleDeleteModal}
                entity={entity}
            />
        </>
    )
}

const Mutation = gql`
    mutation EditSiteSettingProfileField(
        $input: editSiteSettingProfileFieldInput!
    ) {
        editSiteSettingProfileField(input: $input) {
            profileItem {
                guid
                name
            }
        }
    }
`

export default graphql(Mutation)(ProfileField)
