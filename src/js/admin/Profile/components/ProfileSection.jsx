import React from 'react'
import { Draggable, Droppable } from 'react-beautiful-dnd'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import DashedBox from 'js/components/DashedBox/DashedBox'
import IconButton from 'js/components/IconButton/IconButton'
import Text from 'js/components/Text/Text'
import Textfield from 'js/components/Textfield/Textfield'

import RemoveIcon from 'icons/cross.svg'
import MoveVerticalIcon from 'icons/move-vertical.svg'

import ProfileSectionField from './ProfileSectionField'

const Wrapper = styled.div`
    position: relative;
    padding: 32px 0;

    &:before {
        content: '';
        position: absolute;
        top: 0;
        width: 80px;
        height: 1px;
        background-color: ${(p) => p.theme.color.grey[30]};
        left: 0;
        right: 0;
        margin: 0 auto;
    }
`

const FieldsWrapper = styled.div`
    margin: 0 40px;

    .FieldsWrapperBox {
        min-height: 40px;
    }

    .FieldsWrapperMessage {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        display: flex;
        align-items: center;
        justify-content: center;
    }
`

const ProfileSection = ({
    index,
    entity,
    profileFields,
    onChange,
    onRemove,
    onCheckField,
    onRemoveField,
}) => {
    const handleChange = (evt) => {
        onChange(evt.target.value, index)
    }

    const handleRemove = () => {
        onRemove(index)
    }

    const { t } = useTranslation()

    const SectionFields = (
        <FieldsWrapper>
            <Droppable droppableId={`${index}`} type="FIELD">
                {(provided) => (
                    <div ref={provided.innerRef} {...provided.droppableProps}>
                        <DashedBox className="FieldsWrapperBox">
                            {entity.profileFieldGuids.length > 0 ? (
                                entity.profileFieldGuids.map(
                                    (fieldGuid, index) => {
                                        const field = profileFields.find(
                                            (el) => el.guid === fieldGuid,
                                        )

                                        onCheckField(fieldGuid)

                                        return (
                                            <ProfileSectionField
                                                key={field.guid}
                                                index={index}
                                                entity={field}
                                                onRemove={onRemoveField}
                                            />
                                        )
                                    },
                                )
                            ) : (
                                <Text
                                    size="small"
                                    variant="grey"
                                    textAlign="center"
                                    className="FieldsWrapperMessage"
                                >
                                    {index === 0
                                        ? t(
                                              'admin.profile-section-default-helper',
                                          )
                                        : t('admin.profile-section-helper')}
                                </Text>
                            )}
                            {provided.placeholder}
                        </DashedBox>
                    </div>
                )}
            </Droppable>
        </FieldsWrapper>
    )

    if (index === 0) {
        return <div style={{ paddingBottom: '32px' }}>{SectionFields}</div>
    }

    return (
        <Draggable draggableId={`draggable-section-${index}`} index={index}>
            {(provided) => (
                <>
                    <Wrapper
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                    >
                        <div style={{ display: 'flex', marginBottom: '16px' }}>
                            <IconButton
                                as="div"
                                size="large"
                                variant="secondary"
                                {...provided.dragHandleProps}
                            >
                                <MoveVerticalIcon />
                            </IconButton>
                            <Textfield
                                name={`section-${index}`}
                                label={t('form.section')}
                                value={entity.name}
                                onChange={handleChange}
                            />
                            <IconButton
                                size="large"
                                variant="secondary"
                                aria-label={t('admin.profile-section-remove', {
                                    name: entity.name,
                                })}
                                onClick={handleRemove}
                            >
                                <RemoveIcon />
                            </IconButton>
                        </div>
                        {SectionFields}
                    </Wrapper>
                    {provided.placeholder}
                </>
            )}
        </Draggable>
    )
}

export default ProfileSection
