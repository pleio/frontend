import React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import Spacer from 'js/components/Spacer/Spacer'

const CreateFieldForm = ({ mutate, onClose, typeOptions }) => {
    const { t } = useTranslation()

    const submit = async ({ name, fieldType }) => {
        await mutate({
            variables: {
                input: {
                    name,
                    fieldType,
                },
            },
            refetchQueries: ['SiteProfileSettings'],
        })
            .then(() => {
                onClose()
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    const defaultValues = {
        name: '',
        fieldType: null,
    }

    const {
        control,
        handleSubmit,
        formState: { errors, isValid, isSubmitting },
    } = useForm({ mode: 'onChange', defaultValues })

    return (
        <Spacer as="form" spacing="small" onSubmit={handleSubmit(submit)}>
            <FormItem
                control={control}
                type="text"
                name="name"
                label={t('global.label')}
                errors={errors}
                style={{ marginBottom: 16 }}
                required
            />

            <FormItem
                control={control}
                type="select"
                name="fieldType"
                options={typeOptions}
                label={t('global.type')}
                errors={errors}
                helper={t('admin.create-field-helper')}
                required
            />

            <Flexer mt>
                <Button
                    size="normal"
                    variant="tertiary"
                    type="button"
                    onClick={onClose}
                >
                    {t('action.cancel')}
                </Button>
                <Button
                    type="submit"
                    size="normal"
                    variant="primary"
                    disabled={!isValid}
                    loading={isSubmitting}
                >
                    {t('action.create')}
                </Button>
            </Flexer>
        </Spacer>
    )
}

const Mutation = gql`
    mutation AddSiteSettingProfileField(
        $input: addSiteSettingProfileFieldInput!
    ) {
        addSiteSettingProfileField(input: $input) {
            profileItem {
                guid
                name
            }
        }
    }
`

export default graphql(Mutation)(CreateFieldForm)
