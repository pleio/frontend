import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { FastField, Formik } from 'formik'
import compose from 'lodash.flowright'

import PageTitle from 'js/admin/components/PageTitle'
import SettingContainer from 'js/admin/layout/SettingContainer'
import submitSetting from 'js/admin/lib/submitSetting'
import AnimatePresence from 'js/components/AnimatePresence/AnimatePresence'
import Document from 'js/components/Document'
import Input from 'js/components/Form/FormikTextfield'
import Select from 'js/components/Form/FormSelect'
import Switch from 'js/components/Form/FormSwitch'
import { Container } from 'js/components/Grid/Grid'
import Section from 'js/components/Section/Section'

const Flow = ({ data, mutate }) => {
    if (!data || !data.siteSettings) return null

    const { siteSettings } = data

    const { t } = useTranslation()

    const handleSubmitSetting = (key, val, resolve, reject) => {
        const refetchQueries = ['SiteFlowSettings']
        let value = val
        if (key === 'flowCaseId') value = parseInt(val)
        submitSetting(mutate, key, value, refetchQueries, resolve, reject)
    }

    const initialValues = {
        flowAppUrl: siteSettings.flowAppUrl,
        flowToken: siteSettings.flowToken,
        flowCaseId: siteSettings.flowCaseId,
        flowUserGuid: siteSettings.flowUserGuid,
    }

    return (
        <>
            <Document
                title={t('admin.flow')}
                containerTitle={t('admin.title')}
            />
            <Formik initialValues={initialValues}>
                <Container size="small">
                    <Section divider>
                        <PageTitle>{t('admin.flow')}</PageTitle>

                        <SettingContainer
                            title="admin.flow"
                            htmlFor="flowEnabled"
                        >
                            <Switch
                                name="flowEnabled"
                                releaseInputArea
                                checked={siteSettings.flowEnabled}
                                onChange={handleSubmitSetting}
                            />
                        </SettingContainer>

                        <AnimatePresence visible={siteSettings.flowEnabled}>
                            <SettingContainer
                                subtitle="admin.flow-subtypes"
                                helper="admin.flow-subtypes-helper"
                                inputWidth="large"
                                htmlFor="flowSubtypes"
                            >
                                <Select
                                    name="flowSubtypes"
                                    isMulti
                                    options={[
                                        {
                                            value: 'blog',
                                            label: t(
                                                'entity-blog.content-name',
                                            ),
                                        },
                                        {
                                            value: 'discussion',
                                            label: t(
                                                'entity-discussion.content-name',
                                            ),
                                        },
                                        {
                                            value: 'news',
                                            label: t(
                                                'entity-news.content-name',
                                            ),
                                        },
                                        {
                                            value: 'question',
                                            label: t(
                                                'entity-question.content-name',
                                            ),
                                        },
                                    ]}
                                    value={siteSettings.flowSubtypes}
                                    onChange={handleSubmitSetting}
                                />
                            </SettingContainer>

                            <SettingContainer
                                subtitle="admin.flow-app-url"
                                helper="admin.flow-app-url-helper"
                                inputWidth="large"
                                htmlFor="flowAppUrl"
                            >
                                <FastField
                                    name="flowAppUrl"
                                    component={Input}
                                    onSubmit={handleSubmitSetting}
                                />
                            </SettingContainer>

                            <SettingContainer
                                subtitle="admin.flow-token"
                                inputWidth="large"
                                htmlFor="flowToken"
                            >
                                <FastField
                                    name="flowToken"
                                    component={Input}
                                    onSubmit={handleSubmitSetting}
                                />
                            </SettingContainer>

                            <SettingContainer
                                subtitle="admin.flow-case-id"
                                inputWidth="large"
                                htmlFor="flowCaseId"
                            >
                                <FastField
                                    name="flowCaseId"
                                    component={Input}
                                    type="number"
                                    onSubmit={handleSubmitSetting}
                                />
                            </SettingContainer>

                            <SettingContainer
                                subtitle="admin.flow-user-guid"
                                helper="admin.flow-user-guid-helper"
                                inputWidth="large"
                                htmlFor="flowUserGuid"
                            >
                                <FastField
                                    name="flowUserGuid"
                                    component={Input}
                                    onSubmit={handleSubmitSetting}
                                />
                            </SettingContainer>
                        </AnimatePresence>
                    </Section>
                </Container>
            </Formik>
        </>
    )
}

const Query = gql`
    query SiteFlowSettings {
        siteSettings {
            flowEnabled
            flowSubtypes
            flowAppUrl
            flowToken
            flowCaseId
            flowUserGuid
        }
    }
`

const Mutation = gql`
    mutation EditSiteSetting($input: editSiteSettingInput!) {
        editSiteSetting(input: $input) {
            siteSettings {
                flowEnabled
                flowSubtypes
                flowAppUrl
                flowToken
                flowCaseId
                flowUserGuid
            }
        }
    }
`

export default compose(graphql(Query), graphql(Mutation))(Flow)
