import React from 'react'
import { useTranslation } from 'react-i18next'

import ContentList from 'js/admin/components/ContentList'
import PageTitle from 'js/admin/components/PageTitle'
import Document from 'js/components/Document'
import { Container } from 'js/components/Grid/Grid'
import Section from 'js/components/Section/Section'
import useSubtypes from 'js/lib/hooks/useSubtypes'

const Content = () => {
    const { t } = useTranslation()

    const { subtypes, subtypeLabels } = useSubtypes([
        'blog',
        'discussion',
        'event',
        'news',
        'question',
        'wiki',
    ])

    return (
        <>
            <Document
                title={t('global.content')}
                containerTitle={t('admin.title')}
            />
            <Container>
                <Section divider>
                    <PageTitle>{t('global.content')}</PageTitle>
                    <ContentList
                        id="admin-content"
                        subtypesMap={subtypes}
                        subtypeLabels={subtypeLabels}
                        showFiles={false}
                    />
                </Section>
            </Container>
        </>
    )
}

export default Content
