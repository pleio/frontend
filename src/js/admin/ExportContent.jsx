import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { sortByProperty } from 'helpers'
import compose from 'lodash.flowright'

import SettingContainer from 'js/admin/layout/SettingContainer'
import Button from 'js/components/Button/Button'
import Document from 'js/components/Document'
import Flexer from 'js/components/Flexer/Flexer'
import { Container } from 'js/components/Grid/Grid'
import Modal from 'js/components/Modal/Modal'
import Section from 'js/components/Section/Section'
import Select from 'js/components/Select/Select'

const ExportContent = ({ data, mutate }) => {
    const { t } = useTranslation()

    const [showExportAvatarsModal, setShowExportAvatarsModal] = useState(false)
    const toggleExportAvatarsModal = () => {
        setShowExportAvatarsModal(!showExportAvatarsModal)
    }

    const handleChange = (val) => {
        window.location = `/exporting/content/${val}`
    }

    const handleClickExportGroupOwners = () => {
        window.location = `/exporting/group-owners`
    }

    const handleClickExportAvatars = () => {
        mutate()
            .then(() => {
                toggleExportAvatarsModal()
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    if (!data?.siteSettings) return null

    const { siteSettings } = data
    const options = [...siteSettings.exportableContentTypes].sort(
        sortByProperty('label'),
    )

    return (
        <>
            <Document
                title={t('admin.export-content')}
                containerTitle={t('admin.title')}
            />

            <Container size="small">
                <Section divider>
                    <SettingContainer
                        title="admin.export-content"
                        subtitle="admin.export-content-type"
                        helper="admin.export-content-type-helper"
                        htmlFor="exportableContentTypes"
                        inputWidth="normal"
                    >
                        <Select
                            name="exportableContentTypes"
                            label={t('global.type')}
                            options={options}
                            onChange={handleChange}
                        />
                    </SettingContainer>
                    <SettingContainer
                        subtitle="admin.export-group-owners"
                        helper="admin.export-group-owners-helper"
                    >
                        <Button
                            size="small"
                            variant="tertiary"
                            onClick={handleClickExportGroupOwners}
                        >
                            {t('admin.export-group-owners')}
                        </Button>
                    </SettingContainer>

                    <SettingContainer
                        subtitle="admin.export-profile-images"
                        helper="admin.export-profile-images-helper"
                    >
                        <Button
                            size="small"
                            variant="tertiary"
                            onClick={handleClickExportAvatars}
                        >
                            {t('admin.export-profile-images')}
                        </Button>
                    </SettingContainer>

                    <Modal
                        size="small"
                        isVisible={showExportAvatarsModal}
                        title={t('admin.export-profile-images')}
                    >
                        {t('admin.export-profile-images-confirm')}
                        <Flexer mt>
                            <Button
                                size="normal"
                                variant="primary"
                                onClick={toggleExportAvatarsModal}
                            >
                                {t('action.ok')}
                            </Button>
                        </Flexer>
                    </Modal>
                </Section>
            </Container>
        </>
    )
}

const Query = gql`
    query SiteFlowSettings {
        siteSettings {
            exportableContentTypes {
                value
                label
            }
        }
    }
`

const Mutation = gql`
    mutation Avatars {
        exportAvatars {
            guid
        }
    }
`

export default compose(graphql(Query), graphql(Mutation))(ExportContent)
