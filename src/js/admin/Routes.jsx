import React from 'react'
import { Navigate, Outlet, Route, Routes } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import useMenu from 'js/admin/lib/useMenu'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import NotFound from 'js/core/NotFound'
import RequireAuth from 'js/router/RequireAuth'
import ThemeProvider from 'js/theme/ThemeProvider'

import Access from './Access/Access'
import Onboarding from './Access/Onboarding'
import Advanced from './Advanced/Advanced'
import Agreements from './Agreements/Agreements'
import Appearance from './Appearance/Appearance'
import BrokenLinks from './BrokenLinks/BrokenLinks'
import Content from './Content/Content'
import Dashboard from './Dashboard/Dashboard'
import Datahub from './Datahub/Datahub'
import ExportContent from './ExportContent'
import Files from './Files/Files'
import Flow from './Flow'
import Site from './General'
import Layout from './layout/Layout'
import Mailing from './Mailing/Mailing'
import Navigation from './Navigation/Navigation'
import Pages from './Pages/Pages'
import Profile from './Profile/Profile'
import ProfileSync from './ProfileSync/ProfileSync'
import PublishRequestsRoutes from './PublishRequests/PublishRequestsRoutes'
import Redirects from './Redirects'
import Subscription from './Subscription/Subscription'
import Tags from './Tags/Tags'
import AccessRequests from './Users/AccessRequests/AccessRequests'
import BlockedAndScheduled from './Users/BlockedAndScheduled/BlockedAndScheduled'
import DeleteRequests from './Users/DeleteRequests/DeleteRequests'
import Invitations from './Users/Invitations/Invitations'
import Users from './Users/Users'
import Validators from './Validators/Validators'

const RoutesProvider = () => (
    <Routes>
        <Route
            element={
                <ThemeProvider theme="pleio" bodyColor="#FFFFFF">
                    <RequireAuth viewer="canAccessAdmin">
                        <Outlet />
                    </RequireAuth>
                </ThemeProvider>
            }
        >
            <Route path="*" element={<RoutesAccess />} />
        </Route>
    </Routes>
)

const RoutesAccess = () => {
    const { data, loading } = useQuery(GET_ADMIN_ACCESS)

    if (loading) return <LoadingSpinner />

    return <AdminRoutes data={data} />
}

const AdminRoutes = ({ data }) => {
    const { site, siteSettings, viewer } = data

    const menu = useMenu(site, siteSettings, viewer)

    return (
        <Routes>
            <Route element={<Layout menu={menu} />}>
                <Route
                    path="/"
                    element={<Navigate to={menu[0]?.link} replace />}
                />

                <Route
                    path="/*"
                    element={
                        <RequireAuth viewer="hasFullControl">
                            <Routes>
                                <Route
                                    path="/general"
                                    element={
                                        <Navigate
                                            to="/admin/dashboard"
                                            replace
                                        />
                                    }
                                />
                                <Route
                                    path="/dashboard"
                                    element={<Dashboard />}
                                />

                                <Route
                                    path="/site"
                                    element={<Navigate to="general" replace />}
                                />
                                <Route
                                    path="/site/general"
                                    element={<Site />}
                                />
                                <Route
                                    path="/site/access"
                                    element={<Access />}
                                />
                                <Route
                                    path="/site/appearance"
                                    element={<Appearance />}
                                />
                                <Route
                                    path="/site/onboarding"
                                    element={<Onboarding />}
                                />
                                <Route path="/site/tags" element={<Tags />} />
                                <Route
                                    path="/site/mailing"
                                    element={<Mailing />}
                                />
                                <Route
                                    path="/site/advanced"
                                    element={<Advanced />}
                                />

                                <Route
                                    path="/site/navigation"
                                    element={<Navigation />}
                                />

                                <Route path="/content" element={<Content />} />

                                <Route path="/files" element={<Files />} />

                                <Route path="/profile" element={<Profile />} />

                                <Route
                                    path="/tools/agreements"
                                    element={<Agreements />}
                                />
                                <Route
                                    path="/tools/datahub"
                                    element={<Datahub />}
                                />
                                <Route
                                    path="/tools/broken-links"
                                    element={<BrokenLinks />}
                                />
                                <Route
                                    path="/tools/export-content"
                                    element={<ExportContent />}
                                />
                                <Route path="/tools/flow" element={<Flow />} />
                                <Route
                                    path="/tools/profile-sync"
                                    element={<ProfileSync />}
                                />
                                <Route
                                    path="/tools/redirects"
                                    element={<Redirects />}
                                />
                                <Route
                                    path="/tools/validators"
                                    element={<Validators />}
                                />

                                <Route
                                    path="/subscription"
                                    element={<Subscription />}
                                />
                            </Routes>
                        </RequireAuth>
                    }
                />

                <Route
                    path="/publish-requests/*"
                    element={
                        <RequireAuth viewer="publicationRequestManagement">
                            <PublishRequestsRoutes />
                        </RequireAuth>
                    }
                />

                <Route
                    path="/pages"
                    element={
                        <RequireAuth viewer="contentManagement">
                            <Pages />
                        </RequireAuth>
                    }
                />
                <Route
                    path="/pages/:parentGuid"
                    element={
                        <RequireAuth viewer="contentManagement">
                            <Pages />
                        </RequireAuth>
                    }
                />

                <Route
                    path="/users/*"
                    element={
                        <RequireAuth viewer="hasUserManagement">
                            <Routes>
                                <Route
                                    path="/"
                                    element={<Navigate to="manage" replace />}
                                />
                                <Route path="/manage" element={<Users />} />
                                <Route
                                    path="/blocked"
                                    element={<BlockedAndScheduled />}
                                />
                                <Route
                                    path="/invitations"
                                    element={<Invitations />}
                                />
                                <Route
                                    path="/access-requests"
                                    element={<AccessRequests />}
                                />
                                <Route
                                    path="/delete-requests"
                                    element={<DeleteRequests />}
                                />
                            </Routes>
                        </RequireAuth>
                    }
                />
                <Route element={<NotFound />} />
            </Route>
        </Routes>
    )
}

const GET_ADMIN_ACCESS = gql`
    query GetAdminAccessData {
        site {
            guid
            datahubExternalContentEnabled
        }
        siteSettings {
            contentModerationEnabled
            commentModerationEnabled
        }
        viewer {
            guid
            canAccessAdmin: canAccessAdmin(permission: accessAdmin)
            hasFullControl: canAccessAdmin(permission: fullControl)
            hasUserManagement: canAccessAdmin(permission: userManagement)
            publicationRequestManagement: canAccessAdmin(
                permission: publicationRequestManagement
            )
            hasContentManagement: canAccessAdmin(permission: contentManagement)
            accessRequestsCount: countAdminTasks(kind: siteAccessRequests)
            deleteRequestsCount: countAdminTasks(kind: deleteAccountRequests)
            contentPublicationRequestsCount: countAdminTasks(
                kind: contentPublicationRequests
            )
            commentPublicationRequestsCount: countAdminTasks(
                kind: commentPublicationRequests
            )
            filePublicationRequestsCount: countAdminTasks(
                kind: filePublicationRequests
            )
        }
    }
`

export default RoutesProvider
