import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { useIsMount } from 'helpers'
import { produce } from 'immer'
import compose from 'lodash.flowright'

import LinkItem from 'js/admin/components/LinkItem'
import PageTitle from 'js/admin/components/PageTitle'
import SaveSection from 'js/admin/components/SaveSection'
import SettingContainer from 'js/admin/layout/SettingContainer'
import submitSetting from 'js/admin/lib/submitSetting'
import Button from 'js/components/Button/Button'
import Document from 'js/components/Document'
import Errors from 'js/components/Errors'
import { Container } from 'js/components/Grid/Grid'
import Section from 'js/components/Section/Section'

const Redirects = ({ data, mutate }) => {
    if (!data || !data.siteSettings) return null

    const [errors, setErrors] = useState()

    const handleSubmitSetting = (resolve, reject) => {
        const handleReject = (errors) => {
            setErrors(errors)
            reject()
        }

        const handleResolve = () => {
            setErrors()
            resolve()
        }

        const newRedirects = redirects.map((entity) => {
            return {
                source: entity.source,
                destination: entity.destination,
            }
        })

        const refetchQueries = ['SiteRedirectsSettings']
        submitSetting(
            mutate,
            'redirects',
            newRedirects,
            refetchQueries,
            handleResolve,
            handleReject,
        )
    }

    const [redirects, setRedirects] = useState(data.siteSettings.redirects)

    const isMount = useIsMount()
    const [hasChanged, setHasChanged] = useState(false)

    useEffect(() => {
        if (!isMount && !hasChanged) setHasChanged(true)
    }, [redirects])

    const handleClickRemove = (i) => {
        const newLinks = [...redirects]
        newLinks.splice(i, 1)
        setRedirects(newLinks)
    }

    const handleChange = (value, field, index) => {
        setRedirects(
            produce((newState) => {
                newState[index][field] = value
            }),
        )
    }

    const handleClickAdd = () => {
        setRedirects([
            ...redirects,
            {
                source: '',
                destination: '',
            },
        ])
    }

    const { t } = useTranslation()

    return (
        <>
            <Document
                title={t('admin.redirects')}
                containerTitle={t('admin.title')}
            />
            <Container size="small">
                <Section divider>
                    <PageTitle>{t('admin.redirects')}</PageTitle>

                    <SettingContainer
                        title="admin.redirects"
                        helper="admin.redirects-helper"
                    />

                    {redirects.map((entity, index) => (
                        <LinkItem
                            key={index}
                            index={index}
                            name={`directlinks-${index}`}
                            fields={[
                                {
                                    key: 'source',
                                    value: entity.source,
                                    label: t('global.source'),
                                },
                                {
                                    key: 'destination',
                                    value: entity.destination,
                                    label: t('admin.destination'),
                                },
                            ]}
                            removeLabel={t('admin.remove-redirect')}
                            onClickRemove={handleClickRemove}
                            onChange={handleChange}
                            style={{ marginBottom: '16px' }}
                        />
                    ))}

                    <Button
                        size="small"
                        variant="tertiary"
                        onClick={handleClickAdd}
                    >
                        {t('admin.add-redirect')}
                    </Button>

                    <Errors errors={errors} />

                    <SaveSection
                        isVisible={hasChanged}
                        onSubmit={handleSubmitSetting}
                    />
                </Section>
            </Container>
        </>
    )
}

const Query = gql`
    query SiteRedirectsSettings {
        siteSettings {
            redirects {
                source
                destination
            }
        }
    }
`

const Mutation = gql`
    mutation EditSiteSetting($input: editSiteSettingInput!) {
        editSiteSetting(input: $input) {
            siteSettings {
                redirects {
                    source
                    destination
                }
            }
        }
    }
`

export default compose(graphql(Query), graphql(Mutation))(Redirects)
