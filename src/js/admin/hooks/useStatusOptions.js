import { useTranslation } from 'react-i18next'

export const useStatusOptions = () => {
    const { t } = useTranslation()

    return [
        {
            value: 'draft',
            label: t('global.draft'),
        },
        {
            value: 'published',
            label: t('global.published'),
        },
        {
            value: 'archived',
            label: t('global.archived'),
        },
    ]
}

export default useStatusOptions
