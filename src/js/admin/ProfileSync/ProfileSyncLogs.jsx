import React from 'react'
import { useTranslation } from 'react-i18next'
import { showDateTime, timeSince } from 'helpers/date/showDate'
import styled from 'styled-components'

import FetchMore from 'js/components/FetchMore'
import H3 from 'js/components/Heading/H3'
import H4 from 'js/components/Heading/H4'
import Spacer from 'js/components/Spacer/Spacer'
import Tooltip from 'js/components/Tooltip/Tooltip'

const Wrapper = styled.div`
    margin-bottom: 40px;

    .ProfileSyncLogs__inset {
        margin-left: 16px;

        & *:not(:last-child) {
            margin-bottom: 16px;
        }
    }

    tt {
        background: #f5f5f5;
        border: 1px solid #ddd;
        border-radius: ${(p) => p.theme.radius.small};
        display: inline-block;
        padding: 12px 16px;
    }
`

const LogRows = ({ logs }) => {
    return logs.map(({ uuid, content, timeCreated }) => (
        <Wrapper key={uuid}>
            <Spacer spacing="tiny">
                <H4>{uuid}</H4>

                <div className="ProfileSyncLogs__inset">
                    <div>
                        <Tooltip content={showDateTime(timeCreated)}>
                            <span aria-hidden>{timeSince(timeCreated)}</span>
                        </Tooltip>
                    </div>

                    <tt>
                        <small>{content}</small>
                    </tt>
                </div>
            </Spacer>
        </Wrapper>
    ))
}

const ProfileSyncLogs = ({ logs, fetchMore, setQueryLimit }) => {
    const { t } = useTranslation()

    return (
        <Spacer>
            <H3>{t('admin.profile-sync-logs')}</H3>

            {logs.edges.length > 0 ? (
                <FetchMore
                    edges={logs.edges}
                    getMoreResults={() => logs.edges}
                    fetchMore={fetchMore}
                    fetchCount={10}
                    setLimit={setQueryLimit}
                    maxLimit={logs?.total || 0}
                >
                    <LogRows logs={logs.edges} />
                </FetchMore>
            ) : (
                <p>{t('admin.profile-sync-logs-not-found')}</p>
            )}
        </Spacer>
    )
}

export default ProfileSyncLogs
