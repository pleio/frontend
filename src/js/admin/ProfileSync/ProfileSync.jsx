import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql, useMutation, useQuery } from '@apollo/client'
import { useIsMount } from 'helpers'

import PageTitle from 'js/admin/components/PageTitle'
import SettingContainer from 'js/admin/layout/SettingContainer'
import AnimatePresence from 'js/components/AnimatePresence/AnimatePresence'
import Document from 'js/components/Document'
import FormItem from 'js/components/Form/FormItem'
import { Container } from 'js/components/Grid/Grid'
import Section from 'js/components/Section/Section'

import ProfileSyncLogs from './ProfileSyncLogs'

const ProfileSync = () => {
    const isMount = useIsMount()

    const [queryLimit, setQueryLimit] = useState(10)

    const { loading, data, error, refetch, fetchMore } = useQuery(
        GET_PROFILE_SYNC_SETTINGS,
        {
            variables: {
                offset: 0,
                limit: queryLimit,
            },
        },
    )

    useEffect(() => {
        if (isMount) {
            refetch()
        }
    }, [isMount, refetch])

    if (!data || !data.siteSettings || loading) return null

    if (error) console.error(error)

    return (
        <ShowProfileSync
            profileSyncLogs={data.profileSyncLogs}
            siteSettings={data.siteSettings}
            fetchMore={fetchMore}
            setQueryLimit={setQueryLimit}
        />
    )
}

const ShowProfileSync = ({
    profileSyncLogs = [],
    siteSettings,
    fetchMore,
    setQueryLimit,
}) => {
    const { t } = useTranslation()

    const [mutate] = useMutation(EDIT_SITE_SETTING)

    const defaultValues = {
        profileSyncEnabled: siteSettings.profileSyncEnabled,
        profileSyncToken: siteSettings.profileSyncToken,
    }

    const {
        control,
        handleSubmit,
        formState: { isDirty, errors },
        watch,
    } = useForm({ defaultValues })

    const watchProfileSyncEnabled = watch('profileSyncEnabled')

    const onSubmit = (data) => {
        isDirty &&
            mutate({
                variables: { input: { ...data } },
                refetchQueries: ['SiteFlowSettings'],
            })
    }

    return (
        <>
            <Document
                title={t('admin.profile-sync')}
                containerTitle={t('admin.title')}
            />

            <form onSubmit={handleSubmit(onSubmit)}>
                <Container size="small">
                    <Section divider>
                        <PageTitle>{t('admin.profile-sync')}</PageTitle>

                        <SettingContainer
                            title="admin.profile-sync"
                            htmlFor="profileSyncEnabled"
                        >
                            <FormItem
                                name="profileSyncEnabled"
                                control={control}
                                type="switch"
                                onBlur={handleSubmit(onSubmit)}
                            />
                        </SettingContainer>

                        <AnimatePresence visible={watchProfileSyncEnabled}>
                            <SettingContainer
                                subtitle="admin.profile-sync-token"
                                helper="admin.profile-sync-token-helper"
                                inputWidth="large"
                                htmlFor="profileSyncToken"
                            >
                                <FormItem
                                    name="profileSyncToken"
                                    control={control}
                                    rules={{
                                        minLength: {
                                            value: 20,
                                            message: t(
                                                'admin.profile-sync-token-error',
                                            ),
                                        },
                                    }}
                                    type="text"
                                    errors={errors}
                                    onBlur={handleSubmit(onSubmit)}
                                />
                            </SettingContainer>

                            <ProfileSyncLogs
                                fetchMore={fetchMore}
                                setQueryLimit={setQueryLimit}
                                logs={profileSyncLogs}
                            />
                        </AnimatePresence>
                    </Section>
                </Container>
            </form>
        </>
    )
}

const GET_PROFILE_SYNC_SETTINGS = gql`
    query SiteFlowSettings($offset: Int, $limit: Int) {
        siteSettings {
            profileSyncEnabled
            profileSyncToken
        }
        profileSyncLogs(offset: $offset, limit: $limit) {
            total
            edges {
                uuid
                content
                timeCreated
            }
        }
    }
`

const EDIT_SITE_SETTING = gql`
    mutation EditSiteSetting($input: editSiteSettingInput!) {
        editSiteSetting(input: $input) {
            siteSettings {
                profileSyncEnabled
                profileSyncToken
            }
        }
    }
`

export default ProfileSync
