import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { FastField, Formik } from 'formik'

import SettingContainer from 'js/admin/layout/SettingContainer'
import submitSetting from 'js/admin/lib/submitSetting'
import AnimatePresence from 'js/components/AnimatePresence/AnimatePresence'
import Input from 'js/components/Form/FormikTextfield'
import Select from 'js/components/Form/FormSelect'
import Switch from 'js/components/Form/FormSwitch'
import FormTiptap from 'js/components/Form/FormTiptap'
import { Col, Row } from 'js/components/Grid/Grid'
import Section from 'js/components/Section/Section'
import Setting from 'js/components/Setting'

import AccessRequests from './components/AccessRequests'
import Activate2FAModal from './components/Activate2FAModal'
import DirectRegistrationDomains from './components/DirectRegistrationDomains'
import WhitelistedIpRanges from './components/WhitelistedIpRanges'

const General = ({ data, editSiteSetting }) => {
    const { t } = useTranslation()

    const [showActivate2FAModal, setShowActivate2FAModal] = useState(false)
    const toggleActivate2FAModal = () => {
        setShowActivate2FAModal(!showActivate2FAModal)
    }

    const { siteSettings, viewer } = data

    const {
        allowRegistration,
        autoApproveSSO,
        blockedUserIntroMessage,
        directRegistrationDomains,
        idpId,
        idpName,
        isClosed,
        loginIntro,
        require2FA,
        searchEngineIndexingEnabled,
        walledGardenByIpEnabled,
        whitelistedIpRanges,
    } = siteSettings

    const handleSubmitSetting = (key, value, resolve, reject) => {
        const refetchQueries = ['SiteAccessSettings']

        if (key === 'require2FA' && !viewer.has2faEnabled && value !== 'none') {
            // viewer needs to enable 2FA before forcing 2FA for the site
            toggleActivate2FAModal()
            reject()
            return
        }

        submitSetting(
            editSiteSetting,
            key,
            value,
            refetchQueries,
            resolve,
            reject,
        )
    }

    const submitOpposite = (name, checked, resolve, reject) =>
        handleSubmitSetting(name, !checked, resolve, reject)

    const initialValues = {
        autoApproveSSO,
        idpId,
        idpName,
    }

    return (
        <Formik initialValues={initialValues}>
            {({ values }) => (
                <>
                    <Section divider noTopPadding>
                        <SettingContainer
                            title="admin.closed"
                            helper="admin.closed-helper"
                        >
                            <Switch
                                name="isClosed"
                                releaseInputArea
                                checked={isClosed}
                                onChange={handleSubmitSetting}
                                size="small"
                            />
                        </SettingContainer>

                        <AnimatePresence visible={!isClosed}>
                            <div style={{ paddingBottom: '24px' }}>
                                <SettingContainer
                                    subtitle="admin.closed-whitelist"
                                    helper="admin.closed-whitelist-helper"
                                    htmlFor="walledGardenByIpEnabled"
                                    style={{ marginBottom: 0 }}
                                >
                                    <Switch
                                        name="walledGardenByIpEnabled"
                                        releaseInputArea
                                        checked={walledGardenByIpEnabled}
                                        onChange={handleSubmitSetting}
                                        size="small"
                                    />
                                </SettingContainer>
                                <SettingContainer inputWidth="large">
                                    <AnimatePresence
                                        visible={walledGardenByIpEnabled}
                                    >
                                        <WhitelistedIpRanges
                                            whitelistedIpRanges={
                                                whitelistedIpRanges
                                            }
                                            onSubmitSetting={
                                                handleSubmitSetting
                                            }
                                        />
                                    </AnimatePresence>
                                </SettingContainer>
                            </div>
                        </AnimatePresence>

                        <AnimatePresence visible={isClosed}>
                            <SettingContainer
                                subtitle="admin.closed-message"
                                helper="admin.closed-message-helper"
                                inputWidth="full"
                                style={{ paddingBottom: '24px' }}
                            >
                                <FormTiptap
                                    name="loginIntro"
                                    content={loginIntro}
                                    options={{
                                        textStyle: true,
                                        textLink: true,
                                        textList: true,
                                    }}
                                    onSubmit={handleSubmitSetting}
                                />
                            </SettingContainer>
                        </AnimatePresence>

                        <AnimatePresence
                            visible={!isClosed && !walledGardenByIpEnabled}
                        >
                            <SettingContainer
                                subtitle="admin.indexing"
                                helper="admin.indexing-helper"
                                htmlFor="searchEngineIndexingEnabled"
                            >
                                <Switch
                                    name="searchEngineIndexingEnabled"
                                    releaseInputArea
                                    checked={searchEngineIndexingEnabled}
                                    onChange={handleSubmitSetting}
                                    size="small"
                                />
                            </SettingContainer>
                        </AnimatePresence>
                    </Section>

                    <Section divider>
                        <Setting
                            title="admin.request-access"
                            helper="admin.request-access-helper"
                            htmlFor="allowRegistration"
                        >
                            <Switch
                                name="allowRegistration"
                                releaseInputArea
                                checked={!allowRegistration}
                                onChange={submitOpposite}
                                size="small"
                            />
                        </Setting>

                        <AnimatePresence visible={!allowRegistration}>
                            <div style={{ paddingTop: 24 }}>
                                <DirectRegistrationDomains
                                    directRegistrationDomains={
                                        directRegistrationDomains
                                    }
                                    onSubmitSetting={handleSubmitSetting}
                                />

                                <AnimatePresence visible={!!values.idpId}>
                                    <SettingContainer
                                        subtitle="admin.autoApproveSSO"
                                        helper="admin.autoApproveSSO-helper"
                                        htmlFor="autoApproveSSO"
                                        style={{ paddingBottom: 24 }}
                                    >
                                        <Switch
                                            name="autoApproveSSO"
                                            releaseInputArea
                                            checked={autoApproveSSO}
                                            onChange={handleSubmitSetting}
                                            size="small"
                                        />
                                    </SettingContainer>
                                </AnimatePresence>

                                <AccessRequests />
                            </div>
                        </AnimatePresence>
                    </Section>

                    <Section divider>
                        <SettingContainer title="admin.sso" inputWidth="full">
                            <Row $spacing={16}>
                                <Col mobileLandscapeUp={1 / 2}>
                                    <FastField
                                        name="idpId"
                                        component={Input}
                                        label={t('form.id')}
                                        onSubmit={handleSubmitSetting}
                                    />
                                </Col>
                                <Col mobileLandscapeUp={1 / 2}>
                                    <FastField
                                        name="idpName"
                                        component={Input}
                                        label={t('global.name')}
                                        onSubmit={handleSubmitSetting}
                                    />
                                </Col>
                            </Row>
                        </SettingContainer>
                    </Section>

                    <Section divider>
                        <SettingContainer
                            title="admin.2fa"
                            helper="admin.2fa-helper"
                            htmlFor="require2FA"
                            inputWidth="normal"
                        >
                            <Select
                                name="require2FA"
                                options={[
                                    {
                                        value: 'none',
                                        label: t('admin.2fa-option-none'),
                                    },
                                    {
                                        value: 'admin',
                                        label: t('admin.2fa-option-admin'),
                                    },
                                    {
                                        value: 'all',
                                        label: t('admin.2fa-option-all'),
                                    },
                                ]}
                                value={require2FA}
                                onChange={handleSubmitSetting}
                            />
                        </SettingContainer>
                        <Activate2FAModal
                            isVisible={showActivate2FAModal}
                            onClose={toggleActivate2FAModal}
                        />
                    </Section>

                    <Section divider>
                        <SettingContainer
                            title="admin.blocked-user-intro-message-title"
                            helper="admin.blocked-user-intro-message-description"
                            inputWidth="full"
                        >
                            <FormTiptap
                                name="blockedUserIntroMessage"
                                content={blockedUserIntroMessage}
                                options={{
                                    textStyle: true,
                                    textLink: true,
                                    textList: true,
                                }}
                                onSubmit={handleSubmitSetting}
                            />
                        </SettingContainer>
                    </Section>
                </>
            )}
        </Formik>
    )
}

export default General
