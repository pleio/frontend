import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useMutation, useQuery } from '@apollo/client'

import PageTitle from 'js/admin/components/PageTitle'
import Document from 'js/components/Document'
import { Container } from 'js/components/Grid/Grid'
import { H2 } from 'js/components/Heading'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Section from 'js/components/Section/Section'
import TabMenu from 'js/components/TabMenu'
import TabPage from 'js/components/TabPage'

import General from './General'
import Onboarding from './Onboarding'

const Access = () => {
    const { t } = useTranslation()

    const [tab, setTab] = useState('general')

    const { data, loading } = useQuery(GET_SITE_ACCESS_SETTINGS)
    const [editSiteSetting] = useMutation(EDIT_SITE_SETTING)

    if (loading) return <LoadingSpinner />
    if (!data?.siteSettings) return null

    const tabOptions = [
        {
            id: 'general-tab',
            ariaControls: 'general',
            onClick: () => setTab('general'),
            isActive: tab === 'general',
            label: t('global.general'),
        },
        {
            id: 'onboarding-tab',
            ariaControls: 'onboarding',
            onClick: () => setTab('onboarding'),
            isActive: tab === 'onboarding',
            label: t('admin.onboarding'),
        },
    ]

    return (
        <>
            <Document
                title={t('global.access')}
                containerTitle={t('admin.title')}
            />

            <Section>
                <Container size="small">
                    <PageTitle>{t('global.site')}</PageTitle>

                    <H2 style={{ marginBottom: 24 }}>{t('global.access')}</H2>

                    <TabMenu
                        options={tabOptions}
                        label={t('admin.access-settings')}
                        showBorder
                        sticky
                    ></TabMenu>
                </Container>

                <Container size="small" style={{ paddingTop: 24 }}>
                    <TabPage
                        visible={tab === 'general'}
                        id="general"
                        aria-labelledby="general-tab"
                    >
                        <General
                            data={data}
                            editSiteSetting={editSiteSetting}
                        />
                    </TabPage>
                    <TabPage
                        visible={tab === 'onboarding'}
                        id="onboarding"
                        aria-labelledby="onboarding-tab"
                    >
                        <Onboarding />
                    </TabPage>
                </Container>
            </Section>
        </>
    )
}

const GET_SITE_ACCESS_SETTINGS = gql`
    query SiteAccessSettings {
        siteSettings {
            isClosed
            allowRegistration
            loginIntro
            walledGardenByIpEnabled
            whitelistedIpRanges
            directRegistrationDomains
            idpId
            idpName
            autoApproveSSO
            searchEngineIndexingEnabled
            require2FA
            blockedUserIntroMessage
            showLoginRegister
        }
        viewer {
            guid
            has2faEnabled
        }
    }
`

const EDIT_SITE_SETTING = gql`
    mutation EditSiteSetting($input: editSiteSettingInput!) {
        editSiteSetting(input: $input) {
            siteSettings {
                isClosed
                allowRegistration
                loginIntro
                walledGardenByIpEnabled
                whitelistedIpRanges
                directRegistrationDomains
                idpId
                idpName
                autoApproveSSO
                searchEngineIndexingEnabled
                require2FA
                blockedUserIntroMessage
                showLoginRegister
            }
        }
    }
`

export default Access
