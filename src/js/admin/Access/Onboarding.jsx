import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import compose from 'lodash.flowright'

import PageTitle from 'js/admin/components/PageTitle'
import submitSetting from 'js/admin/lib/submitSetting'
import AnimatePresence from 'js/components/AnimatePresence/AnimatePresence'
import Checkbox from 'js/components/Checkbox/Checkbox'
import Switch from 'js/components/Form/FormSwitch'
import FormTiptap from 'js/components/Form/FormTiptap'
import HideVisually from 'js/components/HideVisually/HideVisually'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Setting from 'js/components/Setting'
import Spacer from 'js/components/Spacer/Spacer'
import Table from 'js/components/Table'

import OnboardingField from './components/OnboardingField'

const Onboarding = ({ data, mutate }) => {
    const { t } = useTranslation()

    if (data.loading) return <LoadingSpinner />
    if (!data?.siteSettings) return null
    const { siteSettings } = data

    const handleSubmitSetting = (key, value, resolve, reject) => {
        const refetchQueries = ['SiteOnboardingSettings']

        submitSetting(mutate, key, value, refetchQueries, resolve, reject)
    }

    return (
        <>
            <PageTitle>{t('admin.onboarding')}</PageTitle>

            <Setting
                title="admin.onboarding"
                helper="admin.show-onboarding-helper"
                htmlFor="onboardingEnabled"
            >
                <Switch
                    name="onboardingEnabled"
                    checked={siteSettings.onboardingEnabled}
                    onChange={handleSubmitSetting}
                    releaseInputArea
                    size="small"
                />
            </Setting>

            <AnimatePresence visible={siteSettings.onboardingEnabled}>
                <Spacer style={{ paddingTop: 24 }}>
                    <Setting
                        subtitle="form.message"
                        helper="admin.onboarding-introduction-helper"
                        inputWidth="full"
                    >
                        <FormTiptap
                            name="onboardingIntro"
                            content={siteSettings.onboardingIntro}
                            options={{
                                textStyle: true,
                                textLink: true,
                                textList: true,
                            }}
                            onSubmit={handleSubmitSetting}
                        />
                    </Setting>

                    <Setting
                        subtitle="admin.onboarding-fields"
                        helper="admin.onboarding-fields-helper"
                        inputWidth="full"
                    >
                        <Checkbox
                            label={t('admin.onboarding-existing-helper')}
                            name="onboardingForceExistingUsers"
                            size="small"
                            checked={siteSettings.onboardingForceExistingUsers}
                            onChange={(evt) =>
                                handleSubmitSetting(
                                    'onboardingForceExistingUsers',
                                    evt.target.checked,
                                )
                            }
                            style={{ marginBottom: 24 }}
                        />

                        <Table>
                            <thead>
                                <tr>
                                    <th style={{ width: '44px' }}>
                                        <HideVisually>
                                            {t('admin.show-on-onboarding')}
                                        </HideVisually>
                                    </th>
                                    <th style={{ width: '50%' }}>
                                        {t('global.label')}
                                    </th>
                                    <th style={{ width: '50%' }}>
                                        {t('global.type')}
                                    </th>
                                    <th style={{ textAlign: 'center' }}>
                                        {t('global.required')}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                {siteSettings.profileFields.map((field) => (
                                    <OnboardingField
                                        key={field.guid}
                                        entity={field}
                                    />
                                ))}
                            </tbody>
                        </Table>
                    </Setting>
                </Spacer>
            </AnimatePresence>
        </>
    )
}

const Query = gql`
    query SiteOnboardingSettings {
        siteSettings {
            onboardingEnabled
            onboardingForceExistingUsers
            onboardingIntro
            profileSections {
                name
                profileFieldGuids
            }
            profileFields {
                guid
                name
                fieldType
                isEditable
                isInOnboarding
                isMandatory
            }
        }
    }
`

const Mutation = gql`
    mutation EditSiteSetting($input: editSiteSettingInput!) {
        editSiteSetting(input: $input) {
            siteSettings {
                onboardingEnabled
                onboardingForceExistingUsers
                onboardingIntro
            }
        }
    }
`

export default compose(graphql(Query), graphql(Mutation))(Onboarding)
