import React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql, useMutation, useQuery } from '@apollo/client'

import SettingContainer from 'js/admin/layout/SettingContainer'
import submitSetting from 'js/admin/lib/submitSetting'
import FormItem from 'js/components/Form/FormItem'
import useSaveState from 'js/components/Form/hooks/useSaveState'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Spacer from 'js/components/Spacer/Spacer'

const LoadAccessRequests = () => {
    const { data, loading } = useQuery(GET_SITE_ACCESS_REQUESTS_SETTINGS)

    if (loading) return <LoadingSpinner />

    return <AccessRequests data={data} />
}

const AccessRequests = ({ data }) => {
    const { t } = useTranslation()

    const { successState, setLoadingState, setSavedState, resetState } =
        useSaveState([
            'siteMembershipAcceptedIntro',
            'siteMembershipDeniedIntro',
        ])

    const [mutate] = useMutation(EDIT_SITE_SETTING)

    const { siteMembershipAcceptedIntro, siteMembershipDeniedIntro } =
        data?.siteSettings || {}

    const handleSubmitSetting = (fields, { target }) => {
        const key = target.id
        const refetchQueries = ['SiteAccessRequestsSettings']

        setLoadingState(key)

        submitSetting(mutate, key, fields[key], refetchQueries, () => {
            setSavedState(key)
            setTimeout(() => resetState(key), 2000)
        })
    }

    const defaultValues = {
        siteMembershipAcceptedIntro,
        siteMembershipDeniedIntro,
    }

    const { control, handleSubmit } = useForm({
        mode: 'onChange',
        defaultValues,
    })

    return (
        <form>
            <SettingContainer
                subtitle="form.message"
                helper="admin.access-request-message-helper"
                inputWidth="large"
            >
                <Spacer>
                    <FormItem
                        control={control}
                        label={t('global.accepted')}
                        type="textarea"
                        name="siteMembershipAcceptedIntro"
                        size="small"
                        onBlur={handleSubmit(handleSubmitSetting)}
                        successState={successState.siteMembershipAcceptedIntro}
                    />
                    <FormItem
                        control={control}
                        label={t('global.rejected')}
                        type="textarea"
                        name="siteMembershipDeniedIntro"
                        size="small"
                        onBlur={handleSubmit(handleSubmitSetting)}
                        successState={successState.siteMembershipDeniedIntro}
                    />
                </Spacer>
            </SettingContainer>
        </form>
    )
}

const GET_SITE_ACCESS_REQUESTS_SETTINGS = gql`
    query SiteAccessRequestsSettings {
        siteSettings {
            siteMembershipAcceptedIntro
            siteMembershipDeniedIntro
        }
    }
`

const EDIT_SITE_SETTING = gql`
    mutation EditSiteSetting($input: editSiteSettingInput!) {
        editSiteSetting(input: $input) {
            siteSettings {
                siteMembershipAcceptedIntro
                siteMembershipDeniedIntro
            }
        }
    }
`

export default LoadAccessRequests
