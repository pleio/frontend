import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import Checkbox from 'js/components/Checkbox/Checkbox'
import Switch from 'js/components/Switch/Switch'

const OnboardingField = ({ mutate, entity }) => {
    const handleChangeFieldSetting = (name, checked) => {
        return new Promise((resolve, reject) => {
            mutate({
                variables: {
                    input: {
                        guid: entity.guid,
                        [name]: checked,
                    },
                },
                refetchQueries: ['SiteOnboardingSettings'],
            })
                .then(() => {
                    resolve()
                })
                .catch((error) => {
                    console.error(error)
                    reject(new Error(error))
                })
        })
    }

    const handleChangeIsInOnboarding = (evt) =>
        handleChangeFieldSetting('isInOnboarding', evt.target.checked)

    const handleChangeIsMandatory = (evt) =>
        handleChangeFieldSetting('isMandatory', evt.target.checked)

    const { t } = useTranslation()

    return (
        <tr>
            <td>
                <Switch
                    name={`${entity.guid}-isInOnboarding`}
                    checked={entity.isInOnboarding}
                    size="small"
                    style={{ height: '100%' }}
                    onHandle={handleChangeIsInOnboarding}
                />
            </td>
            <td>{entity.name}</td>
            <td>{entity.fieldType}</td>
            <td>
                <Checkbox
                    name={`${entity.guid}-isMandatory`}
                    aria-label={t('global.required')}
                    checked={entity.isMandatory}
                    disabled={!entity.isInOnboarding}
                    size="small"
                    style={{
                        height: '100%',
                        display: 'flex',
                        justifyContent: 'center',
                    }}
                    onHandle={handleChangeIsMandatory}
                />
            </td>
        </tr>
    )
}

const Mutation = gql`
    mutation EditSiteSettingProfileField(
        $input: editSiteSettingProfileFieldInput!
    ) {
        editSiteSettingProfileField(input: $input) {
            profileItem {
                guid
                name
                isInOnboarding
                isMandatory
            }
        }
    }
`

export default graphql(Mutation)(OnboardingField)
