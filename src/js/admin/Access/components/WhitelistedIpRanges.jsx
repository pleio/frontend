import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import HideVisually from 'js/components/HideVisually/HideVisually'
import IconButton from 'js/components/IconButton/IconButton'
import Tag from 'js/components/Tag/Tag'
import Textfield from 'js/components/Textfield/Textfield'

import AddIcon from 'icons/add.svg'

const AddButton = styled(IconButton)`
    background-color: transparent;
    height: 100%;
    border-left: 1px solid transparent;

    &:not([disabled]) {
        border-left-color: ${(p) => p.theme.color.grey[30]};
    }
`

const WhitelistedIpRanges = ({ whitelistedIpRanges, onSubmitSetting }) => {
    const handleAddDomain = () => {
        const rangeArray = inputValue
            .replace(/\s+/g, '')
            .split(',')
            .filter((item) => item)
        setInputValue('')
        onSubmitSetting(
            'whitelistedIpRanges',
            Array.from(new Set([...whitelistedIpRanges, ...rangeArray])),
            handleResolve,
            handleReject,
        )
    }

    const [errors, setErrors] = useState()

    const handleReject = (errors) => {
        setErrors(errors)
    }

    const handleResolve = () => {
        setErrors()
    }

    const handleRemoveDomain = (range) => {
        const newWhitelistedIpRanges = [...whitelistedIpRanges]
        newWhitelistedIpRanges.splice(whitelistedIpRanges.indexOf(range), 1)
        onSubmitSetting(
            'whitelistedIpRanges',
            newWhitelistedIpRanges,
            handleResolve,
            handleReject,
        )
    }

    const [inputValue, setInputValue] = useState('')

    const handleChangeInput = (evt) => setInputValue(evt.target.value)

    const handleKeyPressInput = (evt) => {
        if (evt.key === 'Enter') handleAddDomain()
    }

    const { t } = useTranslation()

    if (!whitelistedIpRanges) return null

    return (
        <>
            <HideVisually as="label" htmlFor="whitelistedIpRanges">
                {t('admin.closed-whitelist-input')}
            </HideVisually>
            <Textfield
                name="whitelistedIpRanges"
                value={inputValue}
                onChange={handleChangeInput}
                onKeyPress={handleKeyPressInput}
                ElementAfter={
                    <AddButton
                        variant="secondary"
                        size="large"
                        radiusStyle="none"
                        disabled={!inputValue}
                        onClick={handleAddDomain}
                        aria-label={t('admin.closed-whitelist-add')}
                    >
                        <AddIcon />
                    </AddButton>
                }
            />

            {whitelistedIpRanges.length > 0 && (
                <Flexer
                    justifyContent="flex-start"
                    gutter="tiny"
                    style={{
                        flexWrap: 'wrap',
                        paddingTop: '8px',
                    }}
                >
                    {whitelistedIpRanges.map((range, index) => {
                        const handleRemove = () => handleRemoveDomain(range)
                        return (
                            <Tag
                                key={index}
                                onRemove={handleRemove}
                                textRemoveLabel={t(
                                    'admin.closed-whitelist-remove',
                                    {
                                        ip: range,
                                    },
                                )}
                                style={{ marginTop: '4px' }}
                            >
                                {range}
                            </Tag>
                        )
                    })}
                </Flexer>
            )}

            <Errors errors={errors} />
        </>
    )
}

export default WhitelistedIpRanges
