import React from 'react'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import HideVisually from 'js/components/HideVisually/HideVisually'
import Modal from 'js/components/Modal/Modal'

import ExternalIcon from 'icons/new-window-small.svg'

const Activate2FAModal = ({ isVisible, onClose }) => {
    const { t } = useTranslation()

    return (
        <Modal
            size="small"
            isVisible={isVisible}
            onClose={onClose}
            title={t('admin.2fa')}
        >
            <p>{t('admin.2fa-confirm')}</p>
            <Flexer mt>
                <Button variant="tertiary" size="normal" onClick={onClose}>
                    {t('action.close')}
                </Button>
                <Button
                    size="normal"
                    variant="secondary"
                    as="a"
                    target="_blank"
                    rel="noopener noreferrer"
                    href="https://account.pleio.nl/securitypages/"
                >
                    {t('admin.enable-2fa')}
                    <HideVisually>
                        {t('global.opens-in-new-window')}
                    </HideVisually>
                    <ExternalIcon style={{ marginLeft: 6 }} />
                </Button>
            </Flexer>
        </Modal>
    )
}

export default Activate2FAModal
