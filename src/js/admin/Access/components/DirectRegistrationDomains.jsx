import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import SettingContainer from 'js/admin/layout/SettingContainer'
import AnimatePresence from 'js/components/AnimatePresence/AnimatePresence'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import IconButton from 'js/components/IconButton/IconButton'
import Tag from 'js/components/Tag/Tag'
import Textfield from 'js/components/Textfield/Textfield'

import AddIcon from 'icons/add.svg'

const AddButton = styled(IconButton)`
    background-color: transparent;
    height: 100%;
    border-left: 1px solid transparent;

    &:not([disabled]) {
        border-left-color: ${(p) => p.theme.color.grey[30]};
    }
`

const DirectRegistrationDomains = ({
    directRegistrationDomains,
    onSubmitSetting,
}) => {
    const handleAddDomain = () => {
        const domainArray = inputValue
            .replace(/\s+/g, '')
            .split(',')
            .filter((item) => item)
        setInputValue('')
        onSubmitSetting(
            'directRegistrationDomains',
            Array.from(new Set([...directRegistrationDomains, ...domainArray])),
            handleResolve,
            handleReject,
        )
    }

    const [errors, setErrors] = useState()

    const handleReject = (errors) => {
        setErrors(errors)
    }

    const handleResolve = () => {
        setErrors()
    }

    const handleRemoveDomain = (domain) => {
        const newDirectRegistrationDomains = [...directRegistrationDomains]
        newDirectRegistrationDomains.splice(
            directRegistrationDomains.indexOf(domain),
            1,
        )
        onSubmitSetting(
            'directRegistrationDomains',
            newDirectRegistrationDomains,
            handleResolve,
            handleReject,
        )
    }

    const [inputValue, setInputValue] = useState('')

    const handleChangeInput = (evt) => setInputValue(evt.target.value)

    const handleKeyPressInput = (evt) => {
        if (evt.key === 'Enter') handleAddDomain()
    }

    const { t } = useTranslation()

    if (!directRegistrationDomains) return null

    return (
        <SettingContainer
            subtitle="admin.registration-whitelist"
            helper="admin.registration-whitelist-helper"
            inputWidth="large"
            htmlFor="directRegistrationDomains"
        >
            <Textfield
                name="directRegistrationDomains"
                value={inputValue}
                onChange={handleChangeInput}
                onKeyPress={handleKeyPressInput}
                ElementAfter={
                    <AddButton
                        variant="secondary"
                        size="large"
                        radiusStyle="none"
                        disabled={!inputValue}
                        onClick={handleAddDomain}
                        aria-label={t('admin.registration-whitelist-add')}
                    >
                        <AddIcon />
                    </AddButton>
                }
            />

            <AnimatePresence
                visible={directRegistrationDomains.length > 0 || false}
            >
                <Flexer
                    justifyContent="flex-start"
                    gutter="tiny"
                    style={{
                        flexWrap: 'wrap',
                        paddingTop: '8px',
                    }}
                >
                    {directRegistrationDomains.map((domain, index) => {
                        const handleRemove = () => handleRemoveDomain(domain)
                        return (
                            <Tag
                                key={index}
                                onRemove={handleRemove}
                                textRemoveLabel={t(
                                    'admin.registration-whitelist-remove',
                                    { domain },
                                )}
                                style={{ marginTop: '4px' }}
                            >
                                {domain}
                            </Tag>
                        )
                    })}
                </Flexer>
            </AnimatePresence>

            <Errors errors={errors} />
        </SettingContainer>
    )
}

export default DirectRegistrationDomains
