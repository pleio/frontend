import React from 'react'
import { useTranslation } from 'react-i18next'
import { FastField } from 'formik'
import { sortByProperty } from 'helpers'

import SettingContainer from 'js/admin/layout/SettingContainer'
import AnimatePresence from 'js/components/AnimatePresence/AnimatePresence'
import Input from 'js/components/Form/FormikTextfield'
import Select from 'js/components/Form/FormSelect'
import Switch from 'js/components/Form/FormSwitch'
import Section from 'js/components/Section/Section'
import Setting from 'js/components/Setting'

const hideAuthorTypes = [
    ['event', 'entity-event.content-name'],
    ['blog', 'entity-blog.content-name'],
    ['discussion', 'entity-discussion.content-name'],
    ['news', 'entity-news.content-name'],
    ['task', 'entity-task.content-name'],
    ['page', 'entity-cms.content-name'],
    ['wiki', 'entity-wiki.content-name'],
    ['question', 'entity-question.content-name'],
    ['podcast', 'entity-podcast.content-name'],
]

const GeneralSettings = ({ siteSettings, handleSubmitSetting }) => {
    const { t } = useTranslation()

    const submitOpposite = (name, checked, resolve, reject) =>
        handleSubmitSetting(name, !checked, resolve, reject)

    return (
        <>
            <Section divider>
                <SettingContainer
                    subtitle="admin.login-register"
                    helper="admin.login-register-helper"
                    htmlFor="showLoginRegister"
                >
                    <Switch
                        name="showLoginRegister"
                        releaseInputArea
                        checked={!siteSettings.showLoginRegister}
                        onChange={submitOpposite}
                        size="small"
                    />
                </SettingContainer>

                <SettingContainer
                    subtitle="admin.views-count"
                    helper="admin.views-count-helper"
                    htmlFor="showViewsCount"
                >
                    <Switch
                        name="showViewsCount"
                        checked={siteSettings.showViewsCount}
                        onChange={handleSubmitSetting}
                        releaseInputArea
                        size="small"
                    />
                </SettingContainer>
                <SettingContainer
                    subtitle="admin.hide-author"
                    helper="admin.hide-author-helper"
                    inputWidth="normal"
                >
                    <Select
                        name="hideContentOwner"
                        isMulti
                        label={t('global.types')}
                        options={hideAuthorTypes
                            .map(([typeName, translationKey]) => ({
                                value: typeName,
                                label: t(translationKey),
                            }))
                            .sort(sortByProperty('label'))}
                        value={siteSettings.hideContentOwner}
                        onChange={handleSubmitSetting}
                    />
                </SettingContainer>
                <SettingContainer
                    subtitle="admin.social-sharing"
                    helper="admin.social-sharing-helper"
                    htmlFor="enableSharing"
                >
                    <Switch
                        name="enableSharing"
                        checked={siteSettings.enableSharing}
                        onChange={handleSubmitSetting}
                        releaseInputArea
                        size="small"
                    />
                </SettingContainer>
                <SettingContainer
                    subtitle="admin.voting"
                    helper="admin.voting-helper"
                    htmlFor="showUpDownVoting"
                >
                    <Switch
                        name="showUpDownVoting"
                        checked={siteSettings.showUpDownVoting}
                        onChange={handleSubmitSetting}
                        releaseInputArea
                        size="small"
                    />
                </SettingContainer>
                <SettingContainer
                    subtitle="admin.suggested"
                    helper="admin.suggested-helper"
                    htmlFor="showSuggestedItems"
                >
                    <Switch
                        name="showSuggestedItems"
                        checked={siteSettings.showSuggestedItems}
                        onChange={handleSubmitSetting}
                        releaseInputArea
                        size="small"
                    />
                </SettingContainer>
                <SettingContainer
                    subtitle="admin.reply-without-account"
                    helper="admin.reply-without-account-helper"
                    htmlFor="commentWithoutAccountEnabled"
                >
                    <Switch
                        name="commentWithoutAccountEnabled"
                        checked={siteSettings.commentWithoutAccountEnabled}
                        onChange={handleSubmitSetting}
                        releaseInputArea
                        size="small"
                    />
                </SettingContainer>
                <SettingContainer
                    subtitle="admin.cookie-consent"
                    helper="admin.cookie-consent-helper"
                    htmlFor="cookieConsent"
                >
                    <Switch
                        name="cookieConsent"
                        checked={siteSettings.cookieConsent}
                        onChange={handleSubmitSetting}
                        releaseInputArea
                        size="small"
                    />
                </SettingContainer>
                <SettingContainer
                    subtitle="form.abstract"
                    helper="admin.abstract-helper"
                    htmlFor="maxCharactersInAbstract"
                    inputWidth="small"
                >
                    <FastField
                        name="maxCharactersInAbstract"
                        component={Input}
                        onSubmit={handleSubmitSetting}
                        type="number"
                        min={1}
                        max={1000}
                    />
                </SettingContainer>
            </Section>
            <Section divider>
                <SettingContainer
                    title="admin.news"
                    subtitle="admin.news-excerpt"
                    helper="admin.news-excerpt-helper"
                    htmlFor="showExcerptInNewsCard"
                >
                    <Switch
                        name="showExcerptInNewsCard"
                        checked={siteSettings.showExcerptInNewsCard}
                        onChange={handleSubmitSetting}
                        releaseInputArea
                        size="small"
                    />
                </SettingContainer>

                <SettingContainer
                    subtitle="admin.news-comments"
                    helper="admin.news-comments-helper"
                    htmlFor="commentsOnNews"
                >
                    <Switch
                        name="commentsOnNews"
                        checked={siteSettings.commentsOnNews}
                        onChange={handleSubmitSetting}
                        releaseInputArea
                        size="small"
                    />
                </SettingContainer>
            </Section>

            <Section divider>
                <SettingContainer
                    title="admin.event"
                    subtitle="admin.event-export-attendees"
                    helper="admin.event-export-attendees-helper"
                    htmlFor="eventExport"
                >
                    <Switch
                        name="eventExport"
                        checked={siteSettings.eventExport}
                        onChange={handleSubmitSetting}
                        releaseInputArea
                        size="small"
                    />
                </SettingContainer>

                <SettingContainer
                    subtitle="admin.event-tile-view"
                    helper="admin.event-tile-view-helper"
                    htmlFor="eventTiles"
                >
                    <Switch
                        name="eventTiles"
                        checked={siteSettings.eventTiles}
                        onChange={handleSubmitSetting}
                        releaseInputArea
                        size="small"
                    />
                </SettingContainer>
            </Section>

            <Section divider>
                <SettingContainer
                    title="admin.question"
                    subtitle="admin.question-best-answer"
                    helper="admin.question-best-answer-helper"
                    htmlFor="questionerCanChooseBestAnswer"
                >
                    <Switch
                        name="questionerCanChooseBestAnswer"
                        checked={siteSettings.questionerCanChooseBestAnswer}
                        onChange={handleSubmitSetting}
                        releaseInputArea
                        size="small"
                    />
                </SettingContainer>

                <Setting
                    subtitle="admin.question-lock-after-activity"
                    helper="admin.question-lock-after-activity-helper"
                    htmlFor="questionLockAfterActivity"
                    inputWidth="large"
                >
                    <div
                        style={{
                            position: 'relative',
                            display: 'flex',
                            justifyContent: 'flex-end',
                            paddingBottom: '24px',
                        }}
                    >
                        <Switch
                            name="questionLockAfterActivity"
                            checked={siteSettings.questionLockAfterActivity}
                            onChange={handleSubmitSetting}
                            releaseInputArea
                            size="small"
                        />
                    </div>

                    <AnimatePresence
                        visible={siteSettings.questionLockAfterActivity}
                    >
                        <FastField
                            label={t('form.link')}
                            name="questionLockAfterActivityLink"
                            component={Input}
                            onSubmit={handleSubmitSetting}
                        />
                    </AnimatePresence>
                </Setting>
            </Section>

            <Section divider>
                <SettingContainer title="entity-file.content-name" />

                <SettingContainer
                    subtitle="form.description"
                    helper="admin.file-description-helper"
                    htmlFor="fileDescriptionFieldEnabled"
                >
                    <Switch
                        name="fileDescriptionFieldEnabled"
                        checked={siteSettings.fileDescriptionFieldEnabled}
                        onChange={handleSubmitSetting}
                        releaseInputArea
                        size="small"
                    />
                </SettingContainer>

                <SettingContainer
                    subtitle="admin.original-filename"
                    helper="admin.original-filename-helper"
                    htmlFor="showOriginalFileName"
                >
                    <Switch
                        name="showOriginalFileName"
                        checked={siteSettings.showOriginalFileName}
                        onChange={handleSubmitSetting}
                        releaseInputArea
                        size="small"
                    />
                </SettingContainer>

                <SettingContainer
                    subtitle="admin.file-meta-data"
                    helper="admin.file-meta-data-helper"
                    htmlFor="preserveFileExif"
                >
                    <Switch
                        name="preserveFileExif"
                        checked={siteSettings.preserveFileExif}
                        onChange={handleSubmitSetting}
                        releaseInputArea
                        size="small"
                    />
                </SettingContainer>

                <SettingContainer
                    subtitle="entity-file.pad"
                    helper="admin.enable-pad-helper"
                    htmlFor="collabEditingEnabled"
                >
                    <Switch
                        name="collabEditingEnabled"
                        checked={siteSettings.collabEditingEnabled}
                        onChange={handleSubmitSetting}
                        releaseInputArea
                        size="small"
                    />
                </SettingContainer>
            </Section>
        </>
    )
}

export default GeneralSettings
