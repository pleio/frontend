import React from 'react'
import { useTranslation } from 'react-i18next'

import normalizeContentTypes from 'js/activity/utils/normalizeContentTypes'
import SettingContainer from 'js/admin/layout/SettingContainer'
import Select from 'js/components/Form/FormSelect'
import Switch from 'js/components/Form/FormSwitch'
import Section from 'js/components/Section/Section'

const SearchSettings = ({ siteSettings, handleSubmitSetting }) => {
    const { t } = useTranslation()

    const searchTypeOptions = normalizeContentTypes(
        siteSettings.searchContentTypes.contentTypes,
    )

    return (
        <Section divider>
            <SettingContainer
                subtitle="admin.date-filters"
                helper="admin.date-filters-helper"
                htmlFor="searchPublishedFilterEnabled"
            >
                <Switch
                    name="searchPublishedFilterEnabled"
                    checked={siteSettings.searchPublishedFilterEnabled}
                    onChange={handleSubmitSetting}
                    releaseInputArea
                    size="small"
                />
            </SettingContainer>

            <SettingContainer
                subtitle="admin.search-exclude-types"
                helper="admin.search-exclude-types-helper"
                inputWidth="normal"
                htmlFor="searchExcludedContentTypes"
            >
                <Select
                    name="searchExcludedContentTypes"
                    label={t('global.types')}
                    options={searchTypeOptions}
                    value={siteSettings.searchExcludedContentTypes}
                    isMulti
                    onChange={handleSubmitSetting}
                />
            </SettingContainer>

            <SettingContainer
                subtitle="admin.highlighted-search-results"
                helper="admin.highlighted-search-results-helper"
                inputWidth="normal"
                htmlFor="recommendedType"
            >
                <Select
                    name="recommendedType"
                    options={[
                        {
                            value: 'recommended',
                            label: t('search.recommended'),
                        },
                        {
                            value: 'certified',
                            label: t('search.certified'),
                        },
                    ]}
                    value={siteSettings.recommendedType}
                    label={t('global.label')}
                    onChange={handleSubmitSetting}
                />
            </SettingContainer>
        </Section>
    )
}

export default SearchSettings
