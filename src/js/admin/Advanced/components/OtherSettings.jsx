import React from 'react'
import { useTranslation } from 'react-i18next'
import { FastField } from 'formik'

import SettingContainer from 'js/admin/layout/SettingContainer'
import AnimatePresence from 'js/components/AnimatePresence/AnimatePresence'
import Textarea from 'js/components/Form/FormikTextarea'
import Input from 'js/components/Form/FormikTextfield'
import FormItem from 'js/components/Form/FormItem'
import Switch from 'js/components/Form/FormSwitch'
import { Col, Row } from 'js/components/Grid/Grid'
import Section from 'js/components/Section/Section'
import Setting from 'js/components/Setting'

const OtherSettings = ({
    control,
    siteSettings,
    errors,
    handleSubmitSetting,
    loadingSiteSettingUpdate,
    handleSettingSubmit,
}) => {
    const { t } = useTranslation()

    return (
        <>
            <Section divider>
                <Setting title="admin.kaltura" helper="admin.allow-kaltura">
                    <Switch
                        name="kalturaVideoEnabled"
                        releaseInputArea
                        checked={siteSettings.kalturaVideoEnabled}
                        onChange={handleSubmitSetting}
                        size="small"
                    />
                </Setting>
                <AnimatePresence visible={siteSettings.kalturaVideoEnabled}>
                    <Row
                        $spacing={16}
                        style={{
                            paddingTop: '24px',
                        }}
                    >
                        <Col mobileLandscapeUp={1 / 2}>
                            <FastField
                                name="kalturaVideoPartnerId"
                                component={Input}
                                label={t('form.partner-id-kaltura')}
                                onSubmit={handleSubmitSetting}
                            />
                        </Col>
                        <Col mobileLandscapeUp={1 / 2}>
                            <FastField
                                name="kalturaVideoPlayerId"
                                component={Input}
                                label={t('form.player-id-kaltura')}
                                onSubmit={handleSubmitSetting}
                            />
                        </Col>
                    </Row>
                </AnimatePresence>
            </Section>

            <Section divider>
                <SettingContainer
                    title="admin.custom-css"
                    helper="admin.custom-css-helper"
                    inputWidth="full"
                >
                    <FastField
                        name="customCss"
                        component={Textarea}
                        label={t('admin.css')}
                        size="small"
                        minHeight="200px"
                        onSubmit={handleSubmitSetting}
                    />
                </SettingContainer>
            </Section>

            <Section divider>
                <Setting
                    title="admin.security-txt"
                    helper="admin.security-txt-helper"
                >
                    <Switch
                        name="securityTextEnabled"
                        releaseInputArea
                        checked={siteSettings.securityTextEnabled}
                        onChange={handleSubmitSetting}
                        size="small"
                    />
                </Setting>

                <AnimatePresence visible={siteSettings.securityTextEnabled}>
                    <FormItem
                        control={control}
                        type="checkbox"
                        name="securityTextRedirectEnabled"
                        label={t('admin.security-txt-redirect')}
                        onChange={({ target }) => {
                            handleSettingSubmit(
                                'securityTextRedirectEnabled',
                                target.checked,
                            )
                        }}
                        disabled={loadingSiteSettingUpdate}
                        style={{ paddingTop: 8 }}
                    />
                    <AnimatePresence
                        visible={siteSettings.securityTextRedirectEnabled}
                    >
                        <FormItem
                            control={control}
                            type="text"
                            name="securityTextRedirectUrl"
                            label={t('admin.security-txt-url')}
                            onBlur={({ target }) => {
                                handleSettingSubmit(
                                    'securityTextRedirectUrl',
                                    target.value,
                                )
                            }}
                            disabled={loadingSiteSettingUpdate}
                            errors={errors}
                            style={{ paddingTop: 24 }}
                        />
                    </AnimatePresence>
                    <AnimatePresence
                        visible={!siteSettings.securityTextRedirectEnabled}
                    >
                        <Row $spacing={16} style={{ paddingTop: 24 }}>
                            <Col mobileLandscapeUp={1 / 2}>
                                <FastField
                                    name="securityText"
                                    component={Textarea}
                                    label={t('admin.security-txt')}
                                    onSubmit={handleSubmitSetting}
                                />
                            </Col>
                            <Col mobileLandscapeUp={1 / 2}>
                                <FastField
                                    name="securityTextPGP"
                                    component={Textarea}
                                    label={t('admin.security-txt-pgp')}
                                    onSubmit={handleSubmitSetting}
                                />
                            </Col>
                        </Row>
                    </AnimatePresence>
                </AnimatePresence>
            </Section>
        </>
    )
}

export default OtherSettings
