import React from 'react'
import { useTranslation } from 'react-i18next'

import SettingContainer from 'js/admin/layout/SettingContainer'
import Select from 'js/components/Form/FormSelect'
import Switch from 'js/components/Form/FormSwitch'
import Section from 'js/components/Section/Section'

const UserSettings = ({ siteSettings, handleSubmitSetting }) => {
    const { t } = useTranslation()

    return (
        <Section divider>
            <SettingContainer
                subtitle="admin.newsletter"
                helper="admin.newsletter-helper"
                htmlFor="newsletter"
            >
                <Switch
                    name="newsletter"
                    checked={siteSettings.newsletter}
                    onChange={handleSubmitSetting}
                    releaseInputArea
                    size="small"
                />
            </SettingContainer>
            <SettingContainer
                subtitle="admin.content-email"
                helper="admin.content-email-helper"
                inputWidth="normal"
                htmlFor="defaultEmailOverviewFrequency"
            >
                <Select
                    name="defaultEmailOverviewFrequency"
                    label={t('date.period')}
                    options={siteSettings.defaultEmailOverviewFrequencyOptions}
                    value={siteSettings.defaultEmailOverviewFrequency}
                    onChange={handleSubmitSetting}
                />
            </SettingContainer>

            <SettingContainer
                subtitle="admin.delete-account"
                helper="admin.delete-account-helper"
                htmlFor="cancelMembershipEnabled"
            >
                <Switch
                    name="cancelMembershipEnabled"
                    checked={siteSettings.cancelMembershipEnabled}
                    onChange={handleSubmitSetting}
                    releaseInputArea
                    size="small"
                />
            </SettingContainer>
        </Section>
    )
}

export default UserSettings
