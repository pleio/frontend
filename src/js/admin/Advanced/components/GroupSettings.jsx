import React from 'react'

import SettingContainer from 'js/admin/layout/SettingContainer'
import Switch from 'js/components/Form/FormSwitch'
import Section from 'js/components/Section/Section'

const GroupSettings = ({ siteSettings, handleSubmitSetting }) => {
    const submitOpposite = (name, checked, resolve, reject) =>
        handleSubmitSetting(name, !checked, resolve, reject)

    return (
        <Section divider>
            <SettingContainer
                subtitle="admin.group-create"
                helper="admin.group-create-helper"
                htmlFor="limitedGroupAdd"
            >
                <Switch
                    name="limitedGroupAdd"
                    checked={!siteSettings.limitedGroupAdd}
                    onChange={submitOpposite}
                    releaseInputArea
                    size="small"
                />
            </SettingContainer>

            <SettingContainer
                subtitle="global.status-updates"
                helper="admin.status-updates-helper"
                htmlFor="statusUpdateGroups"
            >
                <Switch
                    name="statusUpdateGroups"
                    checked={siteSettings.statusUpdateGroups}
                    onChange={handleSubmitSetting}
                    releaseInputArea
                    size="small"
                />
            </SettingContainer>

            <SettingContainer
                subtitle="admin.group-subgroups"
                helper="admin.group-subgroups-helper"
                htmlFor="subgroups"
            >
                <Switch
                    name="subgroups"
                    checked={siteSettings.subgroups}
                    onChange={handleSubmitSetting}
                    releaseInputArea
                    size="small"
                />
            </SettingContainer>

            <SettingContainer
                subtitle="admin.group-export-members"
                helper="admin.group-export-members-helper"
                htmlFor="groupMemberExport"
            >
                <Switch
                    name="groupMemberExport"
                    checked={siteSettings.groupMemberExport}
                    onChange={handleSubmitSetting}
                    releaseInputArea
                    size="small"
                />
            </SettingContainer>
        </Section>
    )
}

export default GroupSettings
