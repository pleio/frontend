import React from 'react'
import { useTranslation } from 'react-i18next'
import { sortByProperty } from 'helpers'

import SettingContainer from 'js/admin/layout/SettingContainer'
import Select from 'js/components/Form/FormSelect'
import Switch from 'js/components/Form/FormSwitch'
import Section from 'js/components/Section/Section'

const canCreateTypes = [
    ['blog', 'entity-blog.content-name'],
    ['discussion', 'entity-discussion.content-name'],
    ['event', 'entity-event.content-name'],
    ['question', 'entity-question.content-name'],
    ['podcast', 'entity-podcast.content-name'],
    ['poll', 'entity-poll.content-name'],
    ['wiki', 'entity-wiki.content-name'],
]

const contentModerationTypes = [
    ['blog', 'entity-blog.content-name'],
    ['discussion', 'entity-discussion.content-name'],
    ['episode', 'entity-episode.content-name'],
    ['event', 'entity-event.content-name'],
    ['news', 'entity-news.content-name'],
    ['podcast', 'entity-podcast.content-name'],
    ['question', 'entity-question.content-name'],
    ['wiki', 'entity-wiki.content-name'],
    ['file', 'entity-file.content-name'],
]

const commentModerationTypes = [
    ['blog', 'entity-blog.content-name'],
    ['discussion', 'entity-discussion.content-name'],
    ['event', 'entity-event.content-name'],
    ['news', 'entity-news.content-name'],
    ['question', 'entity-question.content-name'],
]

const ContentSettings = ({ siteSettings, handleSubmitSetting }) => {
    const { t } = useTranslation()

    return (
        <>
            <Section divider>
                <SettingContainer
                    subtitle="admin.default-read-access"
                    helper="admin.default-read-access-helper"
                    inputWidth="normal"
                    htmlFor="defaultReadAccessId"
                >
                    <Select
                        name="defaultReadAccessId"
                        options={siteSettings.defaultReadAccessIdOptions}
                        value={siteSettings.defaultReadAccessId}
                        onChange={handleSubmitSetting}
                    />
                </SettingContainer>
                <SettingContainer
                    subtitle="admin.hide-access-select"
                    helper="admin.hide-access-select-helper"
                    htmlFor="hideAccessLevelSelect"
                >
                    <Switch
                        name="hideAccessLevelSelect"
                        checked={siteSettings.hideAccessLevelSelect}
                        size="small"
                        onChange={handleSubmitSetting}
                    />
                </SettingContainer>
                <SettingContainer
                    subtitle="admin.create-content"
                    helper="admin.create-content-helper"
                    inputWidth="normal"
                >
                    <Select
                        name="openForCreateContentTypes"
                        isMulti
                        label={t('global.types')}
                        options={canCreateTypes
                            .map(([typeName, translationKey]) => ({
                                value: typeName,
                                label: t(translationKey),
                            }))
                            .sort(sortByProperty('label'))}
                        value={siteSettings.openForCreateContentTypes}
                        onChange={handleSubmitSetting}
                    />
                </SettingContainer>
                {siteSettings.contentModerationEnabled && (
                    <>
                        <SettingContainer
                            subtitle="admin.content-moderation"
                            helper="admin.content-moderation-helper"
                            inputWidth="normal"
                        >
                            <Select
                                name="requireContentModerationFor"
                                isMulti
                                label={t('global.types')}
                                options={contentModerationTypes
                                    .map(([typeName, translationKey]) => ({
                                        value: typeName,
                                        label: t(translationKey),
                                    }))
                                    .sort(sortByProperty('label'))}
                                value={siteSettings.requireContentModerationFor}
                                onChange={handleSubmitSetting}
                            />
                        </SettingContainer>
                        <SettingContainer
                            subtitle="admin.comment-moderation"
                            helper="admin.comment-moderation-helper"
                            inputWidth="normal"
                        >
                            <Select
                                name="requireCommentModerationFor"
                                isMulti
                                label={t('global.types')}
                                options={commentModerationTypes
                                    .map(([typeName, translationKey]) => ({
                                        value: typeName,
                                        label: t(translationKey),
                                    }))
                                    .sort(sortByProperty('label'))}
                                value={siteSettings.requireCommentModerationFor}
                                onChange={handleSubmitSetting}
                            />
                        </SettingContainer>
                    </>
                )}
            </Section>
        </>
    )
}

export default ContentSettings
