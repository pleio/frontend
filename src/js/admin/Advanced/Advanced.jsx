import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql, useMutation, useQuery } from '@apollo/client'
import { Formik } from 'formik'

import PageTitle from 'js/admin/components/PageTitle'
import submitSetting from 'js/admin/lib/submitSetting'
import Document from 'js/components/Document'
import { Container } from 'js/components/Grid/Grid'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import TabMenu from 'js/components/TabMenu'
import TabPage from 'js/components/TabPage'

import ContentSettings from './components/ContentSettings'
import GeneralSettings from './components/GeneralSettings'
import GroupSettings from './components/GroupSettings'
import OtherSettings from './components/OtherSettings'
import SearchSettings from './components/SearchSettings'
import UserSettings from './components/UserSettings'

const tabOptions = [
    {
        name: 'general',
        label: 'global.general',
        id: 'general-tab',
        ariaControls: 'advanced-general',
    },
    {
        name: 'content',
        label: 'global.content',
        id: 'content-tab',
        ariaControls: 'advanced-content',
    },
    {
        name: 'group',
        label: 'global.group',
        id: 'group-tab',
        ariaControls: 'advanced-group',
    },
    {
        name: 'user',
        label: 'admin.user',
        id: 'user-tab',
        ariaControls: 'advanced-user',
    },
    {
        name: 'search',
        label: 'global.search',
        id: 'search-tab',
        ariaControls: 'advanced-search',
    },
    {
        name: 'other',
        label: 'global.other',
        id: 'other-tab',
        ariaControls: 'advanced-other',
    },
]

const Advanced = () => {
    const { data, loading } = useQuery(GET_SITE_ADVANCED_SETTINGS)

    if (loading) return <LoadingSpinner />
    if (!data?.siteSettings) return null

    return <AdvancedSettings siteSettings={data.siteSettings} />
}

/*
    This component still uses Formik
    We gradually migrate to react-hook-form
*/
const AdvancedSettings = ({ siteSettings }) => {
    const { t } = useTranslation()

    const [tab, setTab] = useState('general')

    const refetchQueries = ['SiteAdvancedSettings']
    const [mutate] = useMutation(EDIT_SITE_SETTING)

    const handleSubmitSetting = (key, value, resolve, reject) => {
        submitSetting(mutate, key, value, refetchQueries, resolve, reject)
    }

    // Formik default values
    const initialValues = {
        maxCharactersInAbstract: siteSettings.maxCharactersInAbstract,
        questionLockAfterActivityLink:
            siteSettings.questionLockAfterActivityLink,
        preserveFileExif: siteSettings.preserveFileExif,
        kalturaVideoPartnerId: siteSettings.kalturaVideoPartnerId,
        kalturaVideoPlayerId: siteSettings.kalturaVideoPlayerId,
        customCss: siteSettings.customCss,
        securityText: siteSettings.securityText,
        securityTextPGP: siteSettings.securityTextPGP,
        requireContentModerationFor: siteSettings.requireContentModerationFor,
        requireCommentModerationFor: siteSettings.requireCommentModerationFor,
    }

    // React Hook Form default values
    const defaultValues = {
        securityTextRedirectEnabled: siteSettings.securityTextRedirectEnabled,
        securityTextRedirectUrl: siteSettings.securityTextRedirectUrl,
    }

    const {
        control,
        setValue,
        setError,
        formState: { errors },
    } = useForm({
        mode: 'onChange',
        defaultValues,
    })

    const [editSiteSetting, { loading: loadingSiteSettingUpdate }] =
        useMutation(EDIT_SITE_SETTING, {
            refetchQueries,
        })

    // Handle setting submit for form fields controlled by react-hook-form
    const handleSettingSubmit = (fieldName, value) => {
        setValue(fieldName, value)
        editSiteSetting({
            variables: {
                input: { [fieldName]: value },
            },
        }).catch((errorResponse) => {
            setError(fieldName, errorResponse)
        })
    }

    return (
        <>
            <Document
                title={t('global.advanced')}
                containerTitle={t('admin.title')}
            />

            <Formik initialValues={initialValues}>
                <Container size="small">
                    <PageTitle>{t('global.advanced')}</PageTitle>

                    <TabMenu
                        options={tabOptions.map(({ name, label }) => ({
                            onClick: () => setTab(name),
                            isActive: tab === name,
                            label: t(label),
                            id: `advanced-tab-${name}`,
                            ariaControls: `advanced-${name}`,
                        }))}
                        showBorder
                        label={t('form.advanced-settings')}
                    />

                    <TabPage
                        visible={tab === 'general'}
                        id="general"
                        ariaLabelledby="general-tab"
                    >
                        <GeneralSettings
                            siteSettings={siteSettings}
                            handleSubmitSetting={handleSubmitSetting}
                        />
                    </TabPage>

                    <TabPage
                        visible={tab === 'search'}
                        id="search"
                        ariaLabelledby="search-tab"
                    >
                        <SearchSettings
                            siteSettings={siteSettings}
                            handleSubmitSetting={handleSubmitSetting}
                        />
                    </TabPage>

                    <TabPage
                        visible={tab === 'content'}
                        id="content"
                        ariaLabelledby="content-tab"
                    >
                        <ContentSettings
                            siteSettings={siteSettings}
                            handleSubmitSetting={handleSubmitSetting}
                        />
                    </TabPage>

                    <TabPage
                        visible={tab === 'group'}
                        id="group"
                        ariaLabelledby="group-tab"
                    >
                        <GroupSettings
                            siteSettings={siteSettings}
                            handleSubmitSetting={handleSubmitSetting}
                        />
                    </TabPage>

                    <TabPage
                        visible={tab === 'user'}
                        id="user"
                        ariaLabelledby="user-tab"
                    >
                        <UserSettings
                            siteSettings={siteSettings}
                            handleSubmitSetting={handleSubmitSetting}
                        />
                    </TabPage>

                    <TabPage
                        visible={tab === 'other'}
                        id="other"
                        ariaLabelledby="other-tab"
                    >
                        <OtherSettings
                            control={control}
                            errors={errors}
                            siteSettings={siteSettings}
                            handleSubmitSetting={handleSubmitSetting}
                            loadingSiteSettingUpdate={loadingSiteSettingUpdate}
                            handleSettingSubmit={handleSettingSubmit}
                        />
                    </TabPage>
                </Container>
            </Formik>
        </>
    )
}

const GET_SITE_ADVANCED_SETTINGS = gql`
    query SiteAdvancedSettings {
        siteSettings {
            showLoginRegister
            maxCharactersInAbstract
            showUpDownVoting
            enableSharing
            showViewsCount
            newsletter
            defaultEmailOverviewFrequencyOptions {
                value
                label
            }
            defaultEmailOverviewFrequency
            cancelMembershipEnabled
            showExcerptInNewsCard
            commentsOnNews
            hideContentOwner
            openForCreateContentTypes
            eventExport
            questionerCanChooseBestAnswer
            statusUpdateGroups
            subgroups
            groupMemberExport
            limitedGroupAdd
            showSuggestedItems
            cookieConsent
            customCss
            eventTiles
            commentWithoutAccountEnabled
            questionLockAfterActivity
            questionLockAfterActivityLink
            kalturaVideoEnabled
            kalturaVideoPartnerId
            kalturaVideoPlayerId
            fileDescriptionFieldEnabled
            showOriginalFileName
            preserveFileExif
            collabEditingEnabled
            searchPublishedFilterEnabled
            recommendedType
            securityTextEnabled
            securityText
            securityTextPGP
            securityTextRedirectEnabled
            securityTextRedirectUrl
            contentModerationEnabled
            requireContentModerationFor
            commentModerationEnabled
            requireCommentModerationFor
            defaultReadAccessId
            defaultReadAccessIdOptions {
                value
                label
            }
            hideAccessLevelSelect
            searchContentTypes {
                contentTypes {
                    key
                    value
                }
            }
            searchExcludedContentTypes
        }
    }
`

const EDIT_SITE_SETTING = gql`
    mutation EditSiteSetting($input: editSiteSettingInput!) {
        editSiteSetting(input: $input) {
            siteSettings {
                name
            }
        }
    }
`

export default Advanced
