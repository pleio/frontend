import React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import Spacer from 'js/components/Spacer/Spacer'

const ChangeOwnerForm = ({
    siteOwnerName,
    siteOwnerEmail,
    setShowChangeOwner,
}) => {
    const { t } = useTranslation()

    const {
        control,
        handleSubmit,
        formState: { errors, isValid, isSubmitting },
    } = useForm({
        mode: 'onChange',
        defaultValues: {
            siteOwnerName,
            siteOwnerEmail,
        },
    })

    const [overwriteSiteOwner] = useMutation(OVERWRITE_SITE_OWNER, {
        refetchQueries: ['SiteSubscription'],
    })

    const onSubmit = async ({ siteOwnerName, siteOwnerEmail }) => {
        await overwriteSiteOwner({
            variables: {
                input: {
                    siteOwnerName,
                    siteOwnerEmail,
                },
            },
        })
            .then(() => {
                setShowChangeOwner(false)
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    return (
        <Spacer spacing="small" as="form" onSubmit={handleSubmit(onSubmit)}>
            <FormItem
                control={control}
                type="text"
                name="siteOwnerName"
                label={t('global.name')}
                errors={errors}
            />
            <FormItem
                control={control}
                type="email"
                name="siteOwnerEmail"
                label={t('form.email-address')}
                errors={errors}
            />
            <Flexer mt>
                <Button
                    size="normal"
                    variant="tertiary"
                    onClick={() => setShowChangeOwner(false)}
                >
                    {t('action.cancel')}
                </Button>
                <Button
                    size="normal"
                    variant="primary"
                    type="submit"
                    disabled={!isValid}
                    loading={isSubmitting}
                >
                    {t('action.save')}
                </Button>
            </Flexer>
        </Spacer>
    )
}

const OVERWRITE_SITE_OWNER = gql`
    mutation OverwriteSiteOwner($input: OverwriteSiteOwnerInput!) {
        overwriteSiteOwner(input: $input) {
            siteOwnerName
            siteOwnerEmail
        }
    }
`

export default ChangeOwnerForm
