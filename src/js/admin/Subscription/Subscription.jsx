import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import Widget, { WidgetStat } from 'js/admin/components/Widget'
import Button from 'js/components/Button/Button'
import Document from 'js/components/Document'
import { Col, Container, Row } from 'js/components/Grid/Grid'
import HideVisually from 'js/components/HideVisually/HideVisually'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Modal from 'js/components/Modal/Modal'
import Section from 'js/components/Section/Section'
import Text from 'js/components/Text/Text'

import ExternalIcon from 'icons/new-window-small.svg'
import PackageIcon from 'icons/package-label.svg'
import SupportIcon from 'icons/support.svg'
import OwnerIcon from 'icons/user-card.svg'

import ChangeOwnerForm from './components/ChangeOwnerForm'

function LineBreakComponent({ text }) {
    return (
        <>
            {text.split('\n').map((line, index) => (
                <React.Fragment key={index}>
                    {line}
                    {index !== text.split('\n').length - 1 && <br />}
                </React.Fragment>
            ))}
        </>
    )
}

const Subscription = ({ data }) => {
    const { t } = useTranslation()

    const [showChangeOwner, setShowChangeOwner] = useState(false)

    if (data.loading) return <LoadingSpinner />
    if (!data?.siteSettings) return null

    const { siteSettings } = data
    const {
        sitePlanName,
        siteCategory,
        supportContractEnabled,
        supportContractHoursRemaining,
        siteOwnerName,
        siteOwnerEmail,
        siteNotes,
    } = siteSettings

    const hasSiteInfo = sitePlanName || siteCategory

    const hasOwner = siteOwnerName || siteOwnerEmail

    const normalizeReturns = /(\r\n|\n|\r)/gm // Handle all types of line breaks
    const normalizedSiteNotes = siteNotes.replace(normalizeReturns, '\n')

    const colSize = hasSiteInfo ? 1 / 3 : 1 / 2

    return (
        <>
            <Document
                title={t('admin.subscription')}
                containerTitle={t('admin.title')}
            />
            <Container size="large">
                <Section backgroundColor="white">
                    <Row $spacing={20} $growContent>
                        <Col mobileLandscapeUp={colSize}>
                            <Widget
                                icon={<OwnerIcon />}
                                title={t('global.owner')}
                            >
                                {hasOwner ? (
                                    <WidgetStat
                                        title={siteOwnerName}
                                        titleSize="normal"
                                        subtitle={siteOwnerEmail}
                                    />
                                ) : (
                                    <Text size="small">
                                        {t('admin.no-owner')}
                                    </Text>
                                )}
                                <Button
                                    size="normal"
                                    variant="secondary"
                                    onClick={() => setShowChangeOwner(true)}
                                >
                                    {hasOwner
                                        ? t('admin.change-owner')
                                        : t('admin.set-owner')}
                                </Button>
                                <Modal
                                    size="small"
                                    isVisible={showChangeOwner}
                                    title={
                                        hasOwner
                                            ? t('admin.change-owner')
                                            : t('admin.set-owner')
                                    }
                                    onClose={() => setShowChangeOwner(false)}
                                >
                                    <ChangeOwnerForm
                                        siteOwnerName={siteOwnerName}
                                        siteOwnerEmail={siteOwnerEmail}
                                        setShowChangeOwner={setShowChangeOwner}
                                    />
                                </Modal>
                            </Widget>
                        </Col>

                        <Col mobileLandscapeUp={colSize}>
                            <Widget
                                icon={<SupportIcon />}
                                title={t('admin.support')}
                            >
                                {supportContractEnabled ? (
                                    <WidgetStat
                                        title={supportContractHoursRemaining}
                                        subtitle={t('admin.hours', {
                                            count: supportContractHoursRemaining,
                                        })}
                                    />
                                ) : (
                                    <Text size="small">
                                        {t('admin.no-support-contract')}
                                    </Text>
                                )}
                                <Button
                                    as="a"
                                    href="https://support.pleio.nl/"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    size="normal"
                                    variant="primary"
                                >
                                    {t('admin.ask-help')}
                                    <HideVisually>
                                        {t('global.opens-in-new-window')}
                                    </HideVisually>
                                    <ExternalIcon
                                        style={{
                                            flexShrink: 0,
                                            marginLeft: '6px',
                                        }}
                                    />
                                </Button>
                            </Widget>
                        </Col>

                        {hasSiteInfo && (
                            <Col mobileLandscapeUp={colSize}>
                                <Widget
                                    icon={<PackageIcon />}
                                    title={t('admin.package')}
                                >
                                    <WidgetStat
                                        title={sitePlanName}
                                        subtitle={siteCategory}
                                    />
                                    <Button
                                        as="a"
                                        href="https://support.pleio.nl/page/view/445826d5-0e5f-412f-a892-d0935836b2f2/tarieven"
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        size="normal"
                                        variant="primary"
                                    >
                                        {t('admin.view-packages')}
                                        <HideVisually>
                                            {t('global.opens-in-new-window')}
                                        </HideVisually>
                                        <ExternalIcon
                                            style={{
                                                flexShrink: 0,
                                                marginLeft: '6px',
                                            }}
                                        />
                                    </Button>
                                </Widget>
                            </Col>
                        )}
                    </Row>
                    {siteNotes && (
                        <Row>
                            <Col>
                                <Widget
                                    title={t('admin.notes')}
                                    style={{
                                        alignItems: 'start',
                                        marginTop: 40,
                                    }}
                                    textAlign="left"
                                >
                                    <Text>
                                        <LineBreakComponent
                                            text={normalizedSiteNotes}
                                        />
                                    </Text>
                                </Widget>
                            </Col>
                        </Row>
                    )}
                </Section>
            </Container>
        </>
    )
}

const GET_SITE_SUBSCRIPTION = gql`
    query SiteSubscription {
        siteSettings {
            sitePlanName
            siteCategory
            supportContractEnabled
            supportContractHoursRemaining
            siteOwnerName
            siteOwnerEmail
            siteNotes
        }
    }
`

export default graphql(GET_SITE_SUBSCRIPTION)(Subscription)
