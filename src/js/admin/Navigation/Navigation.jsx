import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { gql, useMutation, useQuery } from '@apollo/client'
import { FastField, Formik } from 'formik'
import { useTheme } from 'styled-components'

import PageTitle from 'js/admin/components/PageTitle'
import UploadImage from 'js/admin/components/UploadImage'
import SettingContainer from 'js/admin/layout/SettingContainer'
import submitSetting from 'js/admin/lib/submitSetting'
import AnimatePresence from 'js/components/AnimatePresence/AnimatePresence'
import Button from 'js/components/Button/Button'
import Document from 'js/components/Document'
import ErrorModal from 'js/components/ErrorModal'
import Input from 'js/components/Form/FormikTextfield'
import Select from 'js/components/Form/FormSelect'
import Switch from 'js/components/Form/FormSwitch'
import { Container } from 'js/components/Grid/Grid'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Section from 'js/components/Section/Section'

import ActivityStream from './components/ActivityStream'
import NavigationMenu from './components/NavigationMenu'

const Navigation = () => {
    const [uploadError, setUploadError] = useState('')
    const { data, loading } = useQuery(GET_SITE_NAVIGATION_SETTINGS)
    const [mutate] = useMutation(EDIT_SITE_SETTING_MUTATION)

    const handleSubmitSetting = (key, value, resolve, reject) => {
        if (key === 'menuState') {
            value = value ? 'compact' : 'normal'
        }
        const refetchQueries = ['SiteNavigationSettings']
        const rejectSubmit = (error) => {
            setUploadError(error)
            reject(error)
        }

        submitSetting(mutate, key, value, refetchQueries, resolve, rejectSubmit)
    }

    const theme = useTheme()
    const { t } = useTranslation()

    if (loading) return <LoadingSpinner />
    if (!data?.siteSettings) return null

    const { siteSettings } = data

    const {
        startPageOptions,
        startPage,
        startPageCmsOptions,
        startPageCms,
        anonymousStartPage,
        anonymousStartPageCms,
        showIcon,
        icon,
        iconAlt,
        menu,
        languageOptions,
        extraLanguages,
        menuState,
        footerPageEnabled,
    } = siteSettings

    const initialValues = {
        iconAlt,
    }

    return (
        <>
            <Document
                title={t('global.navigation')}
                containerTitle={t('admin.title')}
            />
            <Container size="small">
                <Section divider>
                    <PageTitle>{t('global.navigation')}</PageTitle>

                    <SettingContainer
                        title="global.navigation"
                        subtitle="admin.home-page"
                        helper="admin.home-page-helper"
                        inputWidth="normal"
                        htmlFor="startPage"
                    >
                        <>
                            <Select
                                name="startPage"
                                options={startPageOptions}
                                value={startPage}
                                onChange={handleSubmitSetting}
                            />
                            <AnimatePresence visible={startPage === 'cms'}>
                                <div style={{ paddingTop: '8px' }}>
                                    <Select
                                        name="startPageCms"
                                        options={startPageCmsOptions}
                                        value={startPageCms}
                                        onChange={handleSubmitSetting}
                                    />
                                </div>
                            </AnimatePresence>
                        </>
                    </SettingContainer>

                    <SettingContainer
                        subtitle="admin.home-page-logged-out"
                        helper="admin.home-page-logged-out-helper"
                        inputWidth="normal"
                        htmlFor="anonymousStartPage"
                    >
                        <>
                            <Select
                                name="anonymousStartPage"
                                options={startPageOptions}
                                value={anonymousStartPage}
                                isClearable
                                onChange={handleSubmitSetting}
                            />
                            <AnimatePresence
                                visible={anonymousStartPage === 'cms'}
                            >
                                <div style={{ paddingTop: '8px' }}>
                                    <Select
                                        name="anonymousStartPageCms"
                                        options={startPageCmsOptions}
                                        value={anonymousStartPageCms}
                                        onChange={handleSubmitSetting}
                                    />
                                </div>
                            </AnimatePresence>
                        </>
                    </SettingContainer>

                    <SettingContainer
                        subtitle="admin.home-icon"
                        helper="admin.home-icon-helper"
                        htmlFor="showIcon"
                    >
                        <>
                            <Switch
                                name="showIcon"
                                checked={showIcon}
                                onChange={handleSubmitSetting}
                            />
                            <AnimatePresence visible={showIcon}>
                                <div style={{ paddingTop: '16px' }}>
                                    <UploadImage
                                        name="icon"
                                        removeName="removeIcon"
                                        src={icon}
                                        alt={t('admin.home-icon')}
                                        onSubmit={handleSubmitSetting}
                                        style={{
                                            height: `${theme.headerBarHeight}px`,
                                            padding: '0 16px',
                                            backgroundColor:
                                                theme.color.header.main,
                                        }}
                                    />
                                    <Formik initialValues={initialValues}>
                                        <FastField
                                            name="iconAlt"
                                            component={Input}
                                            label={t('admin.home-icon-alt')}
                                            onSubmit={handleSubmitSetting}
                                            style={{ marginTop: '24px' }}
                                        />
                                    </Formik>
                                </div>
                            </AnimatePresence>
                        </>
                    </SettingContainer>
                </Section>

                <Section divider>
                    <SettingContainer
                        title="admin.footer"
                        helper="admin.footer-helper"
                        htmlFor="footerPageEnabled"
                    >
                        <div
                            style={{
                                display: 'flex',
                                alignItems: 'flex-end',
                                flexDirection: 'column',
                                gap: 16,
                            }}
                        >
                            <Switch
                                name="footerPageEnabled"
                                checked={footerPageEnabled}
                                onChange={handleSubmitSetting}
                            />
                            <Button
                                variant="secondary"
                                as={Link}
                                to="/admin/site/navigation/footer"
                            >
                                {t('admin.edit-footer')}
                            </Button>
                        </div>
                    </SettingContainer>
                </Section>

                <NavigationMenu
                    menu={menu}
                    languageOptions={languageOptions}
                    extraLanguages={extraLanguages}
                    onSubmitSetting={handleSubmitSetting}
                />

                <Section divider>
                    <SettingContainer
                        subtitle="admin.menu-compact"
                        helper="admin.menu-compact-helper"
                        htmlFor="menuState"
                    >
                        <Switch
                            name="menuState"
                            checked={menuState === 'compact'}
                            onChange={handleSubmitSetting}
                        />
                    </SettingContainer>
                </Section>

                {startPage === 'activity' && <ActivityStream />}
            </Container>

            {uploadError && (
                <ErrorModal
                    showModal={!!uploadError}
                    setShowModal={() => setUploadError('')}
                    error={uploadError}
                />
            )}
        </>
    )
}

const menuItemTemplate = `
    label
    link
    translations {
        __typename @include(if: false)
        label
        language
    }
    access
`

const menuTemplate = `
    menu {
        ${menuItemTemplate}
        children {
            ${menuItemTemplate}
            children {
                ${menuItemTemplate}
            }
        }
        access
    }
`

const GET_SITE_NAVIGATION_SETTINGS = gql`
    query SiteNavigationSettings {
        siteSettings {
            startPageOptions {
                value
                label
            }
            startPage
            startPageCmsOptions {
                value
                label
            }
            startPageCms
            anonymousStartPage
            anonymousStartPageCms
            showIcon
            icon
            iconAlt
            menuState
            ${menuTemplate}
            languageOptions {
                value
                label
            }
            extraLanguages
            footerPageEnabled
        }
    }
`

const EDIT_SITE_SETTING_MUTATION = gql`
    mutation EditSiteSetting($input: editSiteSettingInput!) {
        editSiteSetting(input: $input) {
            siteSettings {
                startPage
                startPageCms
                showIcon
                icon
                iconAlt
                menuState
                ${menuTemplate}
            }
        }
    }
`

export default Navigation
