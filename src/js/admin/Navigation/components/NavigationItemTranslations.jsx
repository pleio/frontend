import React from 'react'
import { useFieldArray, useForm } from 'react-hook-form'
import { Trans, useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import Spacer from 'js/components/Spacer/Spacer'

const NavigationItemTranslations = ({
    defaultLabel,
    translations,
    availableLanguages,
    onClose,
    onSubmit,
}) => {
    const { t } = useTranslation()

    // React Hook Form default values
    const defaultValues = {
        translations: availableLanguages.map((l) => {
            return {
                language: l.value ?? '',
                label:
                    translations.find((t) => t.language === l.value)?.label ??
                    '',
            }
        }),
    }

    const {
        control,
        formState: { isSubmitting },
        handleSubmit,
    } = useForm({
        defaultValues,
    })

    const { fields } = useFieldArray({
        control,
        name: 'translations',
    })

    return (
        <>
            <Spacer spacing="normal">
                <p>
                    <Trans i18nKey="admin.menu-item-translations-helper">
                        Translate menu item{' '}
                        <strong>{{ label: defaultLabel }}</strong> into one or
                        more of the enabled languages. Leave empty to show the
                        default label.
                    </Trans>
                </p>
                {fields.map((field, i) => (
                    <FormItem
                        key={field.id}
                        control={control}
                        type="text"
                        name={`translations.${i}.label`}
                        label={
                            availableLanguages.find(
                                (l) => l.value === field.language,
                            )?.label
                        }
                        placeholder={t('global.label')}
                    />
                ))}
            </Spacer>
            <Flexer mt>
                <Button size="normal" variant="tertiary" onClick={onClose}>
                    {t('action.cancel')}
                </Button>
                <Button
                    size="normal"
                    variant="primary"
                    loading={isSubmitting}
                    onClick={handleSubmit(onSubmit)}
                >
                    {t('action.confirm')}
                </Button>
            </Flexer>
        </>
    )
}

export default NavigationItemTranslations
