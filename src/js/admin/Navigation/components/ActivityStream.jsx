import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { FastField, Form, Formik } from 'formik'
import compose from 'lodash.flowright'

import SettingContainer from 'js/admin/layout/SettingContainer'
import submitSetting from 'js/admin/lib/submitSetting'
import Input from 'js/components/Form/FormikTextfield'
import Select from 'js/components/Form/FormSelect'
import Switch from 'js/components/Form/FormSwitch'
import Section from 'js/components/Section/Section'

import DirectLinks from './ActivityStreamDirectLinks'
import Footer from './ActivityStreamFooter'

const ActivityStream = ({ data, mutate }) => {
    const { t } = useTranslation()

    if (!data || !data.siteSettings) return null

    const siteSettings = data.siteSettings

    const handleSubmitSetting = (key, value, resolve, reject) => {
        const refetchQueries = ['SiteActivitySettings']
        submitSetting(mutate, key, value, refetchQueries, resolve, reject)
    }

    const featuredOptions = [
        {
            value: 0,
            label: '0',
        },
        {
            value: 1,
            label: '1',
        },
        {
            value: 2,
            label: '2',
        },
        {
            value: 3,
            label: '3',
        },
        {
            value: 4,
            label: '4',
        },
        {
            value: 5,
            label: '5',
        },
        {
            value: 6,
            label: '6',
        },
    ]

    const initialValues = {
        subtitle: siteSettings.subtitle,
        leaderImage: siteSettings.leaderImage,
        showInitiative: siteSettings.showInitiative,
        initiativeTitle: siteSettings.initiativeTitle,
        initiativeDescription: siteSettings.initiativeDescription,
        initiativeImage: siteSettings.initiativeImage,
        initiativeImageAlt: siteSettings.initiativeImageAlt,
        initiativeLink: siteSettings.initiativeLink,
    }

    return (
        <Formik initialValues={initialValues}>
            <Form>
                <Section divider>
                    <SettingContainer
                        title="admin.activity"
                        subtitle="admin.activity-featured-items"
                        inputWidth="small"
                        htmlFor="numberOfFeaturedItems"
                    >
                        <Select
                            name="numberOfFeaturedItems"
                            options={featuredOptions}
                            value={siteSettings.numberOfFeaturedItems}
                            onChange={handleSubmitSetting}
                        />
                    </SettingContainer>
                    <SettingContainer
                        subtitle="admin.activity-feed-sorting"
                        htmlFor="enableFeedSorting"
                    >
                        <Switch
                            name="enableFeedSorting"
                            releaseInputArea
                            checked={siteSettings.enableFeedSorting}
                            onChange={handleSubmitSetting}
                        />
                    </SettingContainer>
                    <SettingContainer
                        subtitle="admin.activity-feed-filters"
                        htmlFor="showExtraHomepageFilters"
                    >
                        <Switch
                            name="showExtraHomepageFilters"
                            releaseInputArea
                            checked={siteSettings.showExtraHomepageFilters}
                            onChange={handleSubmitSetting}
                        />
                    </SettingContainer>
                </Section>

                <Section divider>
                    <SettingContainer
                        title="admin.activity-leader"
                        subtitle="admin.activity-leader-show"
                        htmlFor="showLeader"
                    >
                        <Switch
                            name="showLeader"
                            releaseInputArea
                            checked={siteSettings.showLeader}
                            onChange={handleSubmitSetting}
                        />
                    </SettingContainer>
                    <SettingContainer
                        subtitle="admin.activity-leader-buttons"
                        htmlFor="showLeaderButtons"
                    >
                        <Switch
                            name="showLeaderButtons"
                            releaseInputArea
                            checked={siteSettings.showLeaderButtons}
                            onChange={handleSubmitSetting}
                        />
                    </SettingContainer>
                    <SettingContainer
                        subtitle="admin.activity-leader-subtitle"
                        inputWidth="large"
                    >
                        <FastField
                            name="subtitle"
                            component={Input}
                            label={t('admin.activity-leader-subtitle')}
                            onSubmit={handleSubmitSetting}
                        />
                    </SettingContainer>
                    <SettingContainer
                        subtitle="admin.activity-leader-image"
                        inputWidth="large"
                    >
                        <FastField
                            name="leaderImage"
                            component={Input}
                            label={t('form.url')}
                            onSubmit={handleSubmitSetting}
                        />
                    </SettingContainer>
                </Section>

                <Section divider>
                    <SettingContainer
                        title="admin.activity-initiative"
                        subtitle="admin.activity-initiative-show"
                        htmlFor="showInitiative"
                    >
                        <Switch
                            name="showInitiative"
                            releaseInputArea
                            checked={siteSettings.showInitiative}
                            onChange={handleSubmitSetting}
                        />
                    </SettingContainer>
                    <SettingContainer subtitle="form.title" inputWidth="large">
                        <FastField
                            name="initiativeTitle"
                            component={Input}
                            label={t('form.title')}
                            onSubmit={handleSubmitSetting}
                        />
                    </SettingContainer>
                    <SettingContainer
                        subtitle="admin.activity-initiative-description"
                        inputWidth="large"
                    >
                        <FastField
                            name="initiativeDescription"
                            component={Input}
                            label={t('form.description')}
                            onSubmit={handleSubmitSetting}
                        />
                    </SettingContainer>
                    <SettingContainer
                        subtitle="admin.activity-initiative-image"
                        inputWidth="large"
                    >
                        <FastField
                            name="initiativeImage"
                            component={Input}
                            label={t('form.url')}
                            onSubmit={handleSubmitSetting}
                        />
                    </SettingContainer>
                    <SettingContainer
                        subtitle="admin.activity-initiative-image-description"
                        inputWidth="large"
                    >
                        <FastField
                            name="initiativeImageAlt"
                            component={Input}
                            label={t('form.description')}
                            onSubmit={handleSubmitSetting}
                        />
                    </SettingContainer>
                    <SettingContainer
                        subtitle="admin.activity-initiative-link"
                        inputWidth="large"
                    >
                        <FastField
                            name="initiativeLink"
                            component={Input}
                            label={t('form.url')}
                            onSubmit={handleSubmitSetting}
                        />
                    </SettingContainer>
                </Section>

                <DirectLinks
                    entity={siteSettings.directLinks}
                    submitSetting={handleSubmitSetting}
                />

                <Footer
                    entity={siteSettings.footer}
                    submitSetting={handleSubmitSetting}
                />
            </Form>
        </Formik>
    )
}

const Query = gql`
    query SiteActivitySettings {
        siteSettings {
            numberOfFeaturedItems
            enableFeedSorting
            showExtraHomepageFilters
            showLeader
            showLeaderButtons
            subtitle
            leaderImage
            showInitiative
            initiativeTitle
            initiativeDescription
            initiativeImage
            initiativeImageAlt
            initiativeLink
            directLinks {
                title
                link
            }
            footer {
                title
                link
            }
        }
    }
`

const Mutation = gql`
    mutation EditSiteSetting($input: editSiteSettingInput!) {
        editSiteSetting(input: $input) {
            siteSettings {
                numberOfFeaturedItems
                enableFeedSorting
                showExtraHomepageFilters
                showLeader
                showLeaderButtons
                subtitle
                leaderImage
                showInitiative
                initiativeTitle
                initiativeDescription
                initiativeImage
                initiativeImageAlt
                initiativeLink
                directLinks {
                    title
                    link
                }
                footer {
                    title
                    link
                }
            }
        }
    }
`

export default compose(graphql(Query), graphql(Mutation))(ActivityStream)
