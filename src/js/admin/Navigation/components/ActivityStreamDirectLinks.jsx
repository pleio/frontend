import React, { useEffect, useState } from 'react'
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd'
import { useTranslation } from 'react-i18next'
import { useIsMount } from 'helpers'
import { produce } from 'immer'

import LinkItem from 'js/admin/components/LinkItem'
import SaveSection from 'js/admin/components/SaveSection'
import SettingContainer from 'js/admin/layout/SettingContainer'
import Button from 'js/components/Button/Button'
import Section from 'js/components/Section/Section'

const DirectLinks = ({ entity, submitSetting }) => {
    if (!entity) return null

    const [directLinks, setDirectLinks] = useState(entity)

    const isMount = useIsMount()
    const [hasChanged, setHasChanged] = useState(false)

    useEffect(() => {
        if (!isMount && !hasChanged) setHasChanged(true)
    }, [directLinks])

    const handleSubmitSetting = (resolve, reject) => {
        const newDirectLinks = directLinks.map((entity) => {
            return {
                title: entity.title,
                link: entity.link,
            }
        })
        submitSetting('directLinks', newDirectLinks, resolve, reject)
    }

    const onDragEnd = (result) => {
        if (!result.destination) return
        const sourceIndex = result.source.index
        const destinationIndex = result.destination.index
        const newPages = [...directLinks]
        const [movedPage] = newPages.splice(sourceIndex, 1)
        newPages.splice(destinationIndex, 0, movedPage)
        setDirectLinks(newPages)
    }

    const handleClickAdd = () => {
        setDirectLinks([
            ...directLinks,
            {
                title: '',
                link: '',
            },
        ])
    }

    const handleClickRemove = (i) => {
        const newLinks = [...directLinks]
        newLinks.splice(i, 1)
        setDirectLinks(newLinks)
    }

    const handleChange = (value, field, index) => {
        setDirectLinks(
            produce((newState) => {
                newState[index][field] = value
            }),
        )
    }

    const { t } = useTranslation()

    return (
        <Section divider>
            <SettingContainer title="admin.activity-direct-links" />
            <DragDropContext onDragEnd={onDragEnd}>
                <Droppable droppableId="droppable" type="ITEM">
                    {(provided) => (
                        <div
                            ref={provided.innerRef}
                            {...provided.droppableProps}
                        >
                            {directLinks.map((entity, index) => (
                                <Draggable
                                    key={index}
                                    draggableId={`draggable-${index}`}
                                    index={index}
                                >
                                    {(provided) => (
                                        <div
                                            ref={provided.innerRef}
                                            {...provided.draggableProps}
                                            style={{
                                                marginBottom: '16px',
                                                ...provided.draggableProps
                                                    .style,
                                            }}
                                        >
                                            <LinkItem
                                                index={index}
                                                name={`directlinks-${index}`}
                                                fields={[
                                                    {
                                                        key: 'title',
                                                        value: entity.title,
                                                        label: t('form.title'),
                                                    },
                                                    {
                                                        key: 'link',
                                                        value: entity.link,
                                                        label: t('form.link'),
                                                    },
                                                ]}
                                                removeLabel={t(
                                                    'admin.remove-link',
                                                )}
                                                dragHandleProps={
                                                    provided.dragHandleProps
                                                }
                                                onClickRemove={
                                                    handleClickRemove
                                                }
                                                onChange={handleChange}
                                            />
                                        </div>
                                    )}
                                </Draggable>
                            ))}
                            {provided.placeholder}
                        </div>
                    )}
                </Droppable>
            </DragDropContext>
            <Button size="small" variant="tertiary" onClick={handleClickAdd}>
                {t('admin.add-link')}
            </Button>
            <SaveSection
                isVisible={hasChanged}
                onSubmit={handleSubmitSetting}
            />
        </Section>
    )
}

export default DirectLinks
