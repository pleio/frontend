import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'

import accessControl from 'js/components/EntityActions/fragments/accessControl'
import NotFound from 'js/core/NotFound'
import { rowsEditFragment } from 'js/page/Page/rowsFragment'
import PageEditor from 'js/page/PageEditor/PageEditor'

const NavigationFooter = () => {
    const { t } = useTranslation()

    const { loading, data } = useQuery(GET_SITE_FOOTER, {
        fetchPolicy: 'cache-and-network',
    })

    if (loading) return null

    const { site, viewer, siteSettings } = data || {}

    if (!siteSettings) return <NotFound />

    // Custom settings, specifically for the footer
    const title = t('admin.footer')
    const url = '/admin/site/navigation'

    return (
        <PageEditor
            entity={{
                ...siteSettings.footerPage,
                title,
                url,
            }}
            site={site}
            viewer={viewer}
            isFooter
        />
    )
}

const GET_SITE_FOOTER = gql`
    query SiteFooter {
        viewer {
            guid
            isAdmin
        }
        site {
            guid
        }
        siteSettings {
            footerPage {
                guid
                pageType
                statusPublished
                title
                ${accessControl}
                rows {
                    ${rowsEditFragment}
                }
            }
        }
    }
`

export default NavigationFooter
