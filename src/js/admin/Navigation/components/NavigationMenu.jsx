import React, { useEffect, useState } from 'react'
import { DragDropContext, Droppable } from 'react-beautiful-dnd'
import { useTranslation } from 'react-i18next'
import { useIsMount } from 'helpers'
import { produce } from 'immer'

import SaveSection from 'js/admin/components/SaveSection'
import SettingContainer from 'js/admin/layout/SettingContainer'
import Button from 'js/components/Button/Button'
import Section from 'js/components/Section/Section'

import NavigationItem from './NavigationItem'

const NavigationMenu = ({
    menu,
    languageOptions,
    extraLanguages,
    onSubmitSetting,
}) => {
    const [pages, setPages] = useState(menu)

    const isMount = useIsMount()
    const [hasChanged, setHasChanged] = useState(false)

    const availableLanguages = languageOptions.filter((lang) =>
        extraLanguages.includes(lang.value),
    )

    useEffect(() => {
        if (!isMount && !hasChanged) setHasChanged(true)
    }, [pages])

    const submitPages = (resolve, reject) => {
        const menu = []
        let index = 0
        pages.forEach(({ label, link, access, translations, children }) => {
            index++
            menu.push({
                label,
                link,
                translations,
                access,
                id: index,
                parentId: null,
            })

            if (children.length > 0) {
                const parentId = index
                children.forEach(
                    ({ label, link, access, translations, children }) => {
                        index++
                        menu.push({
                            label,
                            link,
                            translations,
                            access,
                            id: index,
                            parentId,
                        })

                        if (children.length > 0) {
                            const parentId = index
                            children.forEach(
                                ({ label, link, access, translations }) => {
                                    index++
                                    menu.push({
                                        label,
                                        link,
                                        translations,
                                        access,
                                        id: index,
                                        parentId,
                                    })
                                },
                            )
                        }
                    },
                )
            }
        })

        onSubmitSetting('menu', menu, resolve, reject)
    }

    const onDragEnd = (result) => {
        if (!result.destination) return

        const sourceIndex = result.source.index
        const destinationIndex = result.destination.index

        if (result.type === 'SUBITEM') {
            const source = JSON.parse(result.source.droppableId)
            const destination = JSON.parse(result.destination.droppableId)

            const newSource = [...pages[source.index].children]
            const [movedPage] = newSource.splice(sourceIndex, 1)

            if (source.index === destination.index) {
                newSource.splice(destinationIndex, 0, movedPage)
            }

            setPages(
                produce((newState) => {
                    newState[source.index].children = newSource
                }),
            )

            if (source.index !== destination.index) {
                const newDestination = [...pages[destination.index].children]
                newDestination.splice(destinationIndex, 0, movedPage)

                setPages(
                    produce((newState) => {
                        newState[destination.index].children = newDestination
                    }),
                )
            }
        } else if (result.type === 'SUBITEM2') {
            const source = JSON.parse(result.source.droppableId)
            const destination = JSON.parse(result.destination.droppableId)

            const newSource = [
                ...pages[result.parentIndex].children[source.childIndex]
                    .children,
            ]
            const [movedPage] = newSource.splice(sourceIndex, 1)

            if (source.index === destination.index) {
                newSource.splice(destinationIndex, 0, movedPage)
            }

            setPages(
                produce((newState) => {
                    newState[result.parentIndex].children[
                        source.childIndex
                    ].children = newSource
                }),
            )
        } else {
            const newPages = [...pages]
            const [movedPage] = newPages.splice(sourceIndex, 1)
            newPages.splice(destinationIndex, 0, movedPage)
            setPages(newPages)
        }
    }

    const defaultAccessLevel = 'public'

    const handleClickAdd = (evt, index, childIndex) => {
        if (index !== undefined) {
            let newPages
            if (childIndex !== undefined) {
                newPages = [
                    ...(pages[index].children[childIndex].children || []),
                    {
                        label: '',
                        link: '/',
                        translations: [],
                        access:
                            pages[index].children[childIndex].access ||
                            defaultAccessLevel,
                    },
                ]
                setPages(
                    produce((newState) => {
                        newState[index].children[childIndex].children = newPages
                    }),
                )
            } else {
                newPages = [
                    ...pages[index].children,
                    {
                        label: '',
                        link: '/',
                        translations: [],
                        children: [],
                        access: defaultAccessLevel,
                    },
                ]
                setPages(
                    produce((newState) => {
                        newState[index].children = newPages
                    }),
                )
            }
        } else {
            setPages([
                ...pages,
                {
                    label: '',
                    link: '/',
                    translations: [],
                    children: [],
                    access: defaultAccessLevel,
                },
            ])
        }
    }

    const handleClickRemove = (index, parentIndex, grandparentIndex) => {
        if (parentIndex !== undefined && grandparentIndex === undefined) {
            const newPages = [...pages[parentIndex].children]
            newPages.splice(index, 1)
            setPages(
                produce((newState) => {
                    newState[parentIndex].children = newPages
                }),
            )
        } else if (
            parentIndex !== undefined &&
            grandparentIndex !== undefined
        ) {
            const newPages = [
                ...pages[grandparentIndex].children[parentIndex].children,
            ]
            newPages.splice(index, 1)
            setPages(
                produce((newState) => {
                    newState[grandparentIndex].children[parentIndex].children =
                        newPages
                }),
            )
        } else {
            const newPages = [...pages]
            newPages.splice(index, 1)
            setPages(newPages)
        }
    }

    const handleChange = (
        value,
        field,
        index,
        parentIndex,
        grandparentIndex,
    ) => {
        if (parentIndex !== undefined && grandparentIndex === undefined) {
            setPages(
                produce((newState) => {
                    newState[parentIndex].children[index][field] = value
                }),
            )
        } else if (
            parentIndex !== undefined &&
            grandparentIndex !== undefined
        ) {
            setPages(
                produce((newState) => {
                    newState[grandparentIndex].children[parentIndex].children[
                        index
                    ][field] = value
                }),
            )
        } else {
            setPages(
                produce((newState) => {
                    newState[index][field] = value
                }),
            )
        }
    }

    const { t } = useTranslation()

    return (
        <Section divider>
            <SettingContainer title="admin.menu" helper="admin.menu-helper" />
            <DragDropContext onDragEnd={onDragEnd}>
                <Droppable droppableId="droppable" type="ITEM">
                    {(provided) => (
                        <div ref={provided.innerRef}>
                            {pages.map((entity, index) => {
                                return (
                                    <NavigationItem
                                        key={`menu-${index}`}
                                        index={index}
                                        entity={entity}
                                        availableLanguages={availableLanguages}
                                        onClickAdd={handleClickAdd}
                                        onClickRemove={handleClickRemove}
                                        onChange={handleChange}
                                        onDragEnd={onDragEnd}
                                    />
                                )
                            })}
                            {provided.placeholder}
                        </div>
                    )}
                </Droppable>
            </DragDropContext>
            <Button size="small" variant="tertiary" onClick={handleClickAdd}>
                {t('action.add-item')}
            </Button>
            <SaveSection isVisible={hasChanged} onSubmit={submitPages} />
        </Section>
    )
}

export default NavigationMenu
