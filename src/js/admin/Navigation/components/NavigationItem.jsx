import React from 'react'
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'

import NavigationItemLink from './NavigationItemLink'

const NavigationItem = ({
    index,
    entity,
    availableLanguages,
    onClickAdd,
    onClickRemove,
    onChange,
    onDragEnd,
}) => {
    const handleClickAdd = (evt, index, childIndex) => {
        onClickAdd(evt, index, childIndex)
    }

    const { t } = useTranslation()

    return (
        <Draggable draggableId={`draggable-${index}`} index={index}>
            {(provided) => (
                <div ref={provided.innerRef} {...provided.draggableProps}>
                    <NavigationItemLink
                        index={index}
                        name={`menu-${index}`}
                        fields={[
                            {
                                key: 'label',
                                value: entity.label,
                                label: t('global.label'),
                            },
                            ...(!entity.children.length
                                ? [
                                      {
                                          key: 'link',
                                          value: entity.link,
                                          label: t('form.link'),
                                      },
                                  ]
                                : []),
                        ]}
                        availableLanguages={availableLanguages}
                        removeLabel={t('admin.remove-item')}
                        dragHandleProps={provided.dragHandleProps}
                        onClickRemove={onClickRemove}
                        onChange={onChange}
                        entity={entity}
                    />
                    <div
                        style={{ margin: '0 0 0 40px', paddingBottom: '20px' }}
                    >
                        <Droppable
                            droppableId={JSON.stringify({
                                type: 'droppable',
                                index,
                            })}
                            type="SUBITEM"
                        >
                            {(provided) => (
                                <div ref={provided.innerRef}>
                                    {entity.children.map(
                                        (entity, childIndex) => (
                                            <Draggable
                                                key={`menu-${index}-${childIndex}`}
                                                draggableId={`draggable-${index}-${childIndex}`}
                                                index={childIndex}
                                            >
                                                {(provided) => (
                                                    <div
                                                        ref={provided.innerRef}
                                                        {...provided.draggableProps}
                                                        style={{
                                                            paddingTop: '16px',
                                                            ...provided
                                                                .draggableProps
                                                                .style,
                                                        }}
                                                    >
                                                        <NavigationItemLink
                                                            index={childIndex}
                                                            parentIndex={index}
                                                            name={`menu-${index}-${childIndex}`}
                                                            fields={[
                                                                {
                                                                    key: 'label',
                                                                    value: entity.label,
                                                                    label: t(
                                                                        'global.label',
                                                                    ),
                                                                },
                                                                ...(!entity
                                                                    .children
                                                                    .length
                                                                    ? [
                                                                          {
                                                                              key: 'link',
                                                                              value: entity.link,
                                                                              label: t(
                                                                                  'form.link',
                                                                              ),
                                                                          },
                                                                      ]
                                                                    : []),
                                                            ]}
                                                            availableLanguages={
                                                                availableLanguages
                                                            }
                                                            removeLabel={t(
                                                                'admin.remove-item',
                                                            )}
                                                            dragHandleProps={
                                                                provided.dragHandleProps
                                                            }
                                                            onClickRemove={
                                                                onClickRemove
                                                            }
                                                            onChange={onChange}
                                                            entity={entity}
                                                        />
                                                        <div
                                                            style={{
                                                                margin: '0 0 0 40px',
                                                            }}
                                                        >
                                                            <DragDropContext
                                                                onDragEnd={(
                                                                    result,
                                                                ) =>
                                                                    onDragEnd({
                                                                        ...result,
                                                                        parentIndex:
                                                                            index,
                                                                    })
                                                                }
                                                            >
                                                                <Droppable
                                                                    droppableId={JSON.stringify(
                                                                        {
                                                                            type: 'droppable2',
                                                                            childIndex,
                                                                        },
                                                                    )}
                                                                    type="SUBITEM2"
                                                                >
                                                                    {(
                                                                        provided,
                                                                    ) => (
                                                                        <div
                                                                            ref={
                                                                                provided.innerRef
                                                                            }
                                                                        >
                                                                            {entity.children.map(
                                                                                (
                                                                                    entity,
                                                                                    grandChildIndex,
                                                                                ) => (
                                                                                    <Draggable
                                                                                        key={`menu-${childIndex}-${grandChildIndex}`}
                                                                                        draggableId={`draggable-${childIndex}-${grandChildIndex}`}
                                                                                        index={
                                                                                            grandChildIndex
                                                                                        }
                                                                                    >
                                                                                        {(
                                                                                            provided,
                                                                                        ) => (
                                                                                            <div
                                                                                                ref={
                                                                                                    provided.innerRef
                                                                                                }
                                                                                                {...provided.draggableProps}
                                                                                                style={{
                                                                                                    paddingTop:
                                                                                                        '16px',
                                                                                                    ...provided
                                                                                                        .draggableProps
                                                                                                        .style,
                                                                                                }}
                                                                                            >
                                                                                                <NavigationItemLink
                                                                                                    index={
                                                                                                        grandChildIndex
                                                                                                    }
                                                                                                    parentIndex={
                                                                                                        childIndex
                                                                                                    }
                                                                                                    grandparentIndex={
                                                                                                        index
                                                                                                    }
                                                                                                    name={`menu-${childIndex}-${grandChildIndex}`}
                                                                                                    fields={[
                                                                                                        {
                                                                                                            key: 'label',
                                                                                                            value: entity.label,
                                                                                                            label: t(
                                                                                                                'global.label',
                                                                                                            ),
                                                                                                        },
                                                                                                        {
                                                                                                            key: 'link',
                                                                                                            value: entity.link,
                                                                                                            label: t(
                                                                                                                'form.link',
                                                                                                            ),
                                                                                                        },
                                                                                                    ]}
                                                                                                    availableLanguages={
                                                                                                        availableLanguages
                                                                                                    }
                                                                                                    removeLabel={t(
                                                                                                        'admin.remove-item',
                                                                                                    )}
                                                                                                    dragHandleProps={
                                                                                                        provided.dragHandleProps
                                                                                                    }
                                                                                                    onClickRemove={
                                                                                                        onClickRemove
                                                                                                    }
                                                                                                    onChange={
                                                                                                        onChange
                                                                                                    }
                                                                                                    entity={
                                                                                                        entity
                                                                                                    }
                                                                                                />
                                                                                            </div>
                                                                                        )}
                                                                                    </Draggable>
                                                                                ),
                                                                            )}
                                                                            {
                                                                                provided.placeholder
                                                                            }
                                                                        </div>
                                                                    )}
                                                                </Droppable>
                                                            </DragDropContext>
                                                            <Button
                                                                size="small"
                                                                variant="tertiary"
                                                                style={{
                                                                    marginTop:
                                                                        '12px',
                                                                }}
                                                                onClick={(
                                                                    evt,
                                                                ) =>
                                                                    handleClickAdd(
                                                                        evt,
                                                                        index,
                                                                        childIndex,
                                                                    )
                                                                }
                                                            >
                                                                {t(
                                                                    'admin.add-subitem',
                                                                )}
                                                            </Button>
                                                        </div>
                                                    </div>
                                                )}
                                            </Draggable>
                                        ),
                                    )}
                                    {provided.placeholder}
                                </div>
                            )}
                        </Droppable>
                        <Button
                            size="small"
                            variant="tertiary"
                            style={{
                                marginTop: '12px',
                            }}
                            onClick={(evt) => handleClickAdd(evt, index)}
                        >
                            {t('admin.add-subitem')}
                        </Button>
                    </div>
                </div>
            )}
        </Draggable>
    )
}

export default NavigationItem
