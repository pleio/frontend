import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import { AccessIcon, useAccessTitle } from 'js/components/AccessIcon/AccessIcon'
import Badge from 'js/components/Badge/Badge'
import DropdownButton from 'js/components/DropdownButton/DropdownButton'
import Flexer from 'js/components/Flexer/Flexer'
import IconButton from 'js/components/IconButton/IconButton'
import Modal from 'js/components/Modal/Modal'
import Textfield from 'js/components/Textfield/Textfield'

import RemoveIcon from 'icons/cross.svg'
import MoveIcon from 'icons/move-vertical.svg'
import TranslateIcon from 'icons/translate.svg'

import NavigationItemTranslations from './NavigationItemTranslations'

const Wrapper = styled.div`
    display: flex;

    .LinkItemFlexer {
        display: flex;
        flex-grow: 1;

        > :not(:last-child) {
            border-right: none;
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
        }

        > :not(:first-child) {
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
        }
    }

    .LinkItemTranslationsBadge {
        position: absolute;
        right: 2px;
        top: 0;
    }
`

const NavigationItemLink = ({
    index,
    parentIndex,
    grandparentIndex,
    name,
    fields,
    availableLanguages,
    removeLabel,
    dragHandleProps,
    onClickRemove,
    onChange,
    entity,
    ...rest
}) => {
    const { access, translations } = entity
    const currentVisibilityLevel = useAccessTitle(access)

    const [showTranslationsModal, setShowTranslationsModal] = useState(false)

    const handleClickRemove = () => {
        onClickRemove(index, parentIndex, grandparentIndex)
    }

    const handleChange = (evt, key) => {
        evt.persist()
        onChange(evt.target.value, key, index, parentIndex, grandparentIndex)
    }

    const handleAccessIdChange = (accessId) => {
        onChange(accessId, 'access', index, parentIndex, grandparentIndex)
    }

    const { t } = useTranslation()

    const visibilityOptions = [
        {
            onClick: () => handleAccessIdChange('administrator'),
            name: t('permissions.administrators'),
            active: access === 'administrator',
        },
        {
            onClick: () => handleAccessIdChange('loggedIn'),
            name: t('permissions.logged-in-users'),
            active: access === 'loggedIn',
        },
        ...(!window.__SETTINGS__.site.isClosed
            ? [
                  {
                      onClick: () => handleAccessIdChange('public'),
                      name: t('permissions.public'),
                      active: access === 'public' || access === null,
                  },
                  {
                      onClick: () => handleAccessIdChange('publicOnly'),
                      name: t('permissions.logged-out'),
                      active: access === 'publicOnly' || access === null,
                  },
              ]
            : []),
    ]

    const translationsCount = translations.filter((t) => t.label).length

    return (
        <Wrapper {...rest}>
            {!!dragHandleProps && (
                <IconButton
                    as="div"
                    size="large"
                    variant="secondary"
                    {...dragHandleProps}
                >
                    <MoveIcon />
                </IconButton>
            )}
            <div className="LinkItemFlexer">
                {fields.map(({ key, label, value }) => (
                    <Textfield
                        key={`${name}-${key}`}
                        name={`${name}-${key}`}
                        label={label}
                        value={value || ''}
                        onChange={(evt) => handleChange(evt, key)}
                        ElementAfter={
                            key === 'label' &&
                            availableLanguages.length > 0 && (
                                <div style={{ position: 'relative' }}>
                                    <IconButton
                                        variant="secondary"
                                        size="normal"
                                        radiusStyle="rounded"
                                        tooltip={t(
                                            'admin.menu-item-translations',
                                        )}
                                        style={{ marginRight: 6 }}
                                        onClick={() =>
                                            setShowTranslationsModal(true)
                                        }
                                        badge={translationsCount}
                                    >
                                        <TranslateIcon />
                                    </IconButton>
                                    {translationsCount ? (
                                        <Badge className="LinkItemTranslationsBadge">
                                            {translationsCount}
                                        </Badge>
                                    ) : null}
                                </div>
                            )
                        }
                    />
                ))}
            </div>
            <Flexer gutter="small" divider="normal">
                <DropdownButton
                    options={visibilityOptions}
                    placement="bottom-start"
                >
                    <IconButton
                        variant="secondary"
                        size="large"
                        style={{ marginLeft: '8px' }}
                        aria-label={t('profile.field-visibility-action', {
                            name,
                            visibilityLevel: currentVisibilityLevel,
                        })}
                        tooltip={currentVisibilityLevel}
                    >
                        <AccessIcon access={access} />
                    </IconButton>
                </DropdownButton>

                <IconButton
                    size="large"
                    variant="secondary"
                    aria-label={removeLabel}
                    onClick={handleClickRemove}
                >
                    <RemoveIcon />
                </IconButton>
            </Flexer>

            <Modal
                size="small"
                isVisible={showTranslationsModal}
                title={t('admin.menu-item-translations')}
                onClose={() => setShowTranslationsModal(false)}
            >
                <NavigationItemTranslations
                    defaultLabel={
                        fields.find((field) => field.key === 'label').value
                    }
                    translations={translations}
                    availableLanguages={availableLanguages}
                    onClose={() => setShowTranslationsModal(false)}
                    onSubmit={({ translations }) => {
                        onChange(
                            translations,
                            'translations',
                            index,
                            parentIndex,
                            grandparentIndex,
                        )
                        setShowTranslationsModal(false)
                    }}
                />
            </Modal>
        </Wrapper>
    )
}

export default NavigationItemLink
