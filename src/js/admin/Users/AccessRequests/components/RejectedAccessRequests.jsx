import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'

import DisplayDate from 'js/components/DisplayDate'
import Table from 'js/components/Table'

const RejectedAccessRequests = ({ rejectedAccessRequests = [] }) => {
    const { t } = useTranslation()

    return (
        <Table>
            <thead>
                <tr>
                    <th>{t('global.name')}</th>
                    <th>{t('form.email-address')}</th>
                    <th style={{ textAlign: 'right' }}>
                        {t('admin.requested')}
                    </th>
                    <th style={{ textAlign: 'right' }}>
                        {t('global.rejected')}
                    </th>
                    <th>{t('global.by')}</th>
                    <th>{t('admin.reason')}</th>
                </tr>
            </thead>
            <tbody>
                {rejectedAccessRequests.map(
                    (
                        {
                            name,
                            email,
                            timeCreated,
                            timeUpdated,
                            reason,
                            processedBy,
                        },
                        index,
                    ) => (
                        <tr key={index}>
                            <td>{name}</td>
                            <td>
                                <a
                                    href={`mailto:${email}`}
                                    className="TableLink"
                                >
                                    {email}
                                </a>
                            </td>
                            <td
                                style={{
                                    textAlign: 'right',
                                }}
                            >
                                <DisplayDate date={timeCreated} />
                            </td>
                            <td
                                style={{
                                    textAlign: 'right',
                                }}
                            >
                                <DisplayDate date={timeUpdated} />
                            </td>
                            <td>
                                {processedBy && (
                                    <Link
                                        to={`/user/${processedBy?.guid}`}
                                        className="TableLink"
                                    >
                                        {processedBy?.name}
                                    </Link>
                                )}
                            </td>
                            <td>{reason}</td>
                        </tr>
                    ),
                )}
            </tbody>
        </Table>
    )
}

export default RejectedAccessRequests
