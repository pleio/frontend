import React from 'react'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import Modal from 'js/components/Modal/Modal'
import Spacer from 'js/components/Spacer/Spacer'
import Text from 'js/components/Text/Text'
import ProfileSections from 'js/user/Profile/components/ProfileSections'

const ProfileModal = ({
    profileFields,
    name,
    email,
    isVisible,
    onClose,
    children,
}) => {
    const { t } = useTranslation()

    if (profileFields.length === 0) return null

    return (
        <Modal
            isVisible={isVisible}
            title={t('profile.see-profile-action')}
            size="normal"
            onClose={onClose}
        >
            <Spacer>
                <Text size="small">
                    {name} ({email})
                </Text>

                <ProfileSections
                    user={{
                        canEdit: false,
                    }}
                    fields={profileFields}
                    isEditingDefault={false}
                    allFieldsRequired={false}
                />

                <Flexer mt>
                    <Button
                        size="normal"
                        variant="tertiary"
                        onClick={onClose}
                        type="button"
                    >
                        {t('action.cancel')}
                    </Button>

                    {children}
                </Flexer>
            </Spacer>
        </Modal>
    )
}

export default ProfileModal
