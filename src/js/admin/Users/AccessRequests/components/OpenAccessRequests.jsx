import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import DisplayDate from 'js/components/DisplayDate'
import Flexer from 'js/components/Flexer/Flexer'
import Table from 'js/components/Table'

import ProfileModal from './ProfileModal'

const OpenAccessRequests = ({
    toggleAccessRequest,
    openAccessRequests = [],
}) => {
    const { t } = useTranslation()

    // showProfile = index of the openAccessRequests array
    const [showProfile, setShowProfile] = useState(-1)

    return (
        <>
            <Table>
                <thead>
                    <tr>
                        <th>{t('global.name')}</th>
                        <th>{t('form.email-address')}</th>
                        <th style={{ textAlign: 'right' }}>
                            {t('admin.requested')}
                        </th>
                        <th />
                    </tr>
                </thead>
                <tbody>
                    {openAccessRequests.map(
                        ({ name, email, timeUpdated }, index) => {
                            return (
                                <tr key={index}>
                                    <td>{name}</td>
                                    <td>
                                        <a
                                            href={`mailto:${email}`}
                                            className="TableLink"
                                        >
                                            {email}
                                        </a>
                                    </td>
                                    <td
                                        style={{
                                            textAlign: 'right',
                                        }}
                                    >
                                        <DisplayDate date={timeUpdated} />
                                    </td>
                                    <td>
                                        <Flexer justifyContent="flex-end">
                                            <Button
                                                size="normal"
                                                variant="tertiary"
                                                onClick={() =>
                                                    setShowProfile(index)
                                                }
                                            >
                                                {t(
                                                    'profile.see-profile-action',
                                                )}
                                            </Button>

                                            <Button
                                                size="normal"
                                                variant="warn-secondary"
                                                onClick={() => {
                                                    toggleAccessRequest(
                                                        email,
                                                        'reject',
                                                    )
                                                }}
                                            >
                                                {t('action.reject')}
                                            </Button>

                                            <Button
                                                size="normal"
                                                variant="secondary"
                                                onClick={() => {
                                                    toggleAccessRequest(
                                                        email,
                                                        'accept',
                                                    )
                                                }}
                                            >
                                                {t('action.approve')}
                                            </Button>
                                        </Flexer>
                                    </td>
                                </tr>
                            )
                        },
                    )}
                </tbody>
            </Table>

            <ProfileModal
                profileFields={
                    showProfile > -1
                        ? openAccessRequests[showProfile]?.profileFields
                        : []
                }
                name={openAccessRequests[showProfile]?.name}
                email={openAccessRequests[showProfile]?.email}
                isVisible={showProfile > -1}
                onClose={() => setShowProfile(-1)}
            >
                <Button
                    size="normal"
                    variant="warn-secondary"
                    onClick={() =>
                        toggleAccessRequest(
                            openAccessRequests[showProfile]?.email,
                            'reject',
                        )
                    }
                >
                    {t('action.reject')}
                </Button>

                <Button
                    size="normal"
                    variant="secondary"
                    onClick={() =>
                        toggleAccessRequest(
                            openAccessRequests[showProfile]?.email,
                            'accept',
                        )
                    }
                >
                    {t('action.approve')}
                </Button>
            </ProfileModal>
        </>
    )
}

export default OpenAccessRequests
