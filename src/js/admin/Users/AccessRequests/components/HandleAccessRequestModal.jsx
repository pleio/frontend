import React from 'react'
import { useForm } from 'react-hook-form'
import { Trans, useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import Modal from 'js/components/Modal/Modal'
import Spacer from 'js/components/Spacer/Spacer'

const HandleAccessRequestModal = ({ mutate, entity, isVisible, onClose }) => {
    const { t } = useTranslation()

    const defaultValues = {
        message: null,
    }

    const {
        control,
        formState: { isSubmitting },
        handleSubmit,
    } = useForm({ defaultValues })

    const onSubmit = async ({ message }) => {
        await mutate({
            variables: {
                input: {
                    email,
                    accept: type !== 'reject',
                    message,
                },
            },
            refetchQueries: [
                'SiteUsers',
                'SiteAccessRequests',
                'GetAdminAccessData',
            ],
        })
            .then(() => {
                onClose()
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    if (!entity) return null

    const { email, type } = entity

    return (
        <Modal
            isVisible={isVisible}
            title={
                type === 'accept'
                    ? t('admin.approve-access-request')
                    : type === 'reject'
                      ? t('admin.reject-access-request')
                      : t('admin.resend-access-request')
            }
            size="small"
            onClose={onClose}
        >
            <form onSubmit={handleSubmit(onSubmit)}>
                <div>
                    <Spacer>
                        {type === 'accept' ? (
                            <Trans i18nKey="admin.approve-access-request-confirm">
                                Are you sure you want to give access to{' '}
                                <strong>{{ email }}</strong>? Users have to log
                                in again to definitively join the site.
                            </Trans>
                        ) : type === 'reject' ? (
                            <Trans i18nKey="admin.reject-access-request-confirm">
                                Are you sure you want to reject the access
                                request for <strong>{{ email }}</strong>?
                            </Trans>
                        ) : (
                            <Trans i18nKey="admin.resend-access-request-confirm">
                                Are you sure you want to give access to{' '}
                                <strong>{{ email }}</strong>? Users then still
                                have to log in to definitively join the site.
                            </Trans>
                        )}

                        {type === 'reject' && (
                            <FormItem
                                control={control}
                                type="textarea"
                                name="message"
                                label={t('admin.reject-access-request-message')}
                            />
                        )}

                        <Flexer mt>
                            <Button
                                size="normal"
                                variant="tertiary"
                                type="button"
                                onClick={onClose}
                            >
                                {t('action.cancel')}
                            </Button>
                            <Button
                                size="normal"
                                variant={type === 'reject' ? 'warn' : 'primary'}
                                type="submit"
                                loading={isSubmitting}
                            >
                                {type === 'reject'
                                    ? t('action.reject')
                                    : t('action.approve')}
                            </Button>
                        </Flexer>
                    </Spacer>
                </div>
            </form>
        </Modal>
    )
}

const SITE_ACCESS_REQUEST = gql`
    mutation HandleSiteAccessRequest($input: handleSiteAccessRequestInput!) {
        handleSiteAccessRequest(input: $input) {
            success
        }
    }
`
export default graphql(SITE_ACCESS_REQUEST)(HandleAccessRequestModal)
