import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'

import DisplayDate from 'js/components/DisplayDate'
import Table from 'js/components/Table'

const ApprovedAccessRequests = ({ approvedAccessRequests = [] }) => {
    const { t } = useTranslation()

    return (
        <Table>
            <thead>
                <tr>
                    <th>{t('global.name')}</th>
                    <th>{t('form.email-address')}</th>
                    <th style={{ textAlign: 'right' }}>
                        {t('admin.requested')}
                    </th>
                    <th style={{ textAlign: 'right' }}>
                        {t('global.approved')}
                    </th>
                    <th>{t('global.by')}</th>
                </tr>
            </thead>
            <tbody>
                {approvedAccessRequests.map(
                    (
                        { name, email, timeCreated, timeUpdated, processedBy },
                        index,
                    ) => (
                        <tr key={index}>
                            <td>{name}</td>
                            <td>
                                <a
                                    href={`mailto:${email}`}
                                    className="TableLink"
                                >
                                    {email}
                                </a>
                            </td>
                            <td
                                style={{
                                    textAlign: 'right',
                                }}
                            >
                                <DisplayDate date={timeCreated} />
                            </td>
                            <td
                                style={{
                                    textAlign: 'right',
                                }}
                            >
                                <DisplayDate date={timeUpdated} />
                            </td>
                            <td>
                                {processedBy && (
                                    <Link
                                        to={`/user/${processedBy?.guid}`}
                                        className="TableLink"
                                    >
                                        {processedBy?.name}
                                    </Link>
                                )}
                            </td>
                        </tr>
                    ),
                )}
            </tbody>
        </Table>
    )
}

export default ApprovedAccessRequests
