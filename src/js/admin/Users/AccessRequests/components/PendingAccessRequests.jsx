import React from 'react'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import DisplayDate from 'js/components/DisplayDate'
import Flexer from 'js/components/Flexer/Flexer'
import Table from 'js/components/Table'

const PendingAccessRequests = ({
    toggleAccessRequest,
    pendingAccessRequests = [],
}) => {
    const { t } = useTranslation()

    return (
        <Table>
            <thead>
                <tr>
                    <th>{t('global.name')}</th>
                    <th>{t('form.email-address')}</th>
                    <th style={{ textAlign: 'right' }}>
                        {t('admin.requested')}
                    </th>
                    <th />
                </tr>
            </thead>
            <tbody>
                {pendingAccessRequests.map(
                    (
                        { name, email, timeCreated, timeUpdated, processedBy },
                        index,
                    ) => (
                        <tr key={index}>
                            <td>{name}</td>
                            <td>
                                <a
                                    href={`mailto:${email}`}
                                    className="TableLink"
                                >
                                    {email}
                                </a>
                            </td>
                            <td
                                style={{
                                    textAlign: 'right',
                                }}
                            >
                                <DisplayDate date={timeCreated} />
                            </td>
                            <td>
                                <Flexer justifyContent="flex-end">
                                    <Button
                                        size="normal"
                                        variant="warn-secondary"
                                        onClick={() =>
                                            toggleAccessRequest(email, 'reject')
                                        }
                                    >
                                        {t('action.reject')}
                                    </Button>
                                    <Button
                                        size="normal"
                                        variant="secondary"
                                        onClick={() =>
                                            toggleAccessRequest(email, 'resend')
                                        }
                                    >
                                        {t('action.resend')}
                                    </Button>
                                </Flexer>
                            </td>
                        </tr>
                    ),
                )}
            </tbody>
        </Table>
    )
}

export default PendingAccessRequests
