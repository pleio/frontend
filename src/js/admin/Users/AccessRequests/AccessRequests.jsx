import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import PageTitle from 'js/admin/components/PageTitle'
import Document from 'js/components/Document'
import { Container } from 'js/components/Grid/Grid'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Section from 'js/components/Section/Section'
import TabMenu from 'js/components/TabMenu'
import TabPage from 'js/components/TabPage'
import Text from 'js/components/Text/Text'
import { START_OF_CONTENT_ELEMENT_ID } from 'js/lib/constants'

import ApprovedAccessRequests from './components/ApprovedAccessRequests'
import HandleAccessRequestModal from './components/HandleAccessRequestModal'
import OpenAccessRequests from './components/OpenAccessRequests'
import PendingAccessRequests from './components/PendingAccessRequests'
import RejectedAccessRequests from './components/RejectedAccessRequests'

const NoResults = () => {
    const { t } = useTranslation()

    return (
        <div
            style={{
                margin: '0 auto',
                textAlign: 'center',
            }}
        >
            <Text as="h2">{t('admin.access-requests-no-results')}</Text>
        </div>
    )
}

const accessRequestFieldsFragment = `
    name
    email
    status
    timeUpdated
    timeCreated
    reason
    processedBy {
        guid
        name
    }
`

const GET_SITE_ACCESS_REQUESTS = gql`
    query SiteAccessRequests {
        siteSettings {
            siteAccessOpen: siteAccessRequests(status: unprocessed) {
                edges {
                    name
                    email
                    status
                    timeUpdated
                    profileFields {
                        key
                        name
                        value
                        fieldType
                        category
                    }
                }
            }
            siteAccessPending: siteAccessRequests(status: pending) {
                edges {
                    ${accessRequestFieldsFragment}
                }
            }
            siteAccessApproved: siteAccessRequests(status: accepted) {
                edges {
                    ${accessRequestFieldsFragment}
                }
            }
            siteAccessRejected: siteAccessRequests(status: rejected) {
                edges {
                    ${accessRequestFieldsFragment}
                }
            }
        }
    }
`

const AccessRequests = () => {
    const { t } = useTranslation()

    const { hash } = useLocation()

    const { data } = useQuery(GET_SITE_ACCESS_REQUESTS)

    const [showHandleAccessRequestModal, setShowHandleAccessRequestModal] =
        useState(false)

    const toggleHandleAccessRequestModal = () =>
        setShowHandleAccessRequestModal(!showHandleAccessRequestModal)

    const hideHandleAccessRequestModal = () => {
        setAccessRequest()
        setShowHandleAccessRequestModal(false)
    }

    const [accessRequest, setAccessRequest] = useState()

    const toggleAccessRequest = (email, type) => {
        setAccessRequest({
            email,
            type,
        })
        toggleHandleAccessRequestModal()
    }

    if (data?.loading) return <LoadingSpinner />
    if (!data?.siteSettings) return null

    const { siteSettings } = data

    const openAccessRequests = siteSettings.siteAccessOpen.edges
    const pendingAccessRequests = siteSettings.siteAccessPending.edges
    const approvedAccessRequests = siteSettings.siteAccessApproved.edges
    const rejectedAccessRequests = siteSettings.siteAccessRejected.edges

    const startOfContentHash = `#${START_OF_CONTENT_ELEMENT_ID}`

    const tabMenuOptions = [
        {
            link: '',
            isActive: !hash || hash === startOfContentHash,
            label: `${t('global.open')} (${openAccessRequests.length})`,
            id: 'tab-open',
            ariaControls: 'panel-open',
        },
        {
            link: '#pending',
            isActive: hash === '#pending',
            label: `${t('global.in-progress')} (${pendingAccessRequests.length})`,
            id: 'tab-pending',
            ariaControls: 'panel-pending',
        },
        {
            link: '#approved',
            isActive: hash === '#approved',
            label: `${t('global.approved')}`,
            id: 'tab-approved',
            ariaControls: 'panel-approved',
        },
        {
            link: '#rejected',
            isActive: hash === '#rejected',
            label: `${t('global.rejected')}`,
            id: 'tab-rejected',
            ariaControls: 'panel-rejected',
        },
    ]

    return (
        <>
            <Document
                title={t('admin.users')}
                containerTitle={t('admin.title')}
            />

            <Section divider>
                <Container size="large">
                    <PageTitle>{t('admin.access-requests')}</PageTitle>

                    <TabMenu
                        label={t('admin.access-requests')}
                        options={tabMenuOptions}
                        showBorder
                        sticky
                    />

                    <TabPage
                        visible={!hash || hash === startOfContentHash}
                        style={{ padding: '24px 0' }}
                        id="panel-open"
                        ariaLabelledby="tab-open"
                    >
                        {openAccessRequests.length > 0 ? (
                            <OpenAccessRequests
                                openAccessRequests={openAccessRequests}
                                toggleAccessRequest={toggleAccessRequest}
                            />
                        ) : (
                            <NoResults />
                        )}
                    </TabPage>

                    <TabPage
                        visible={hash === '#pending'}
                        style={{ padding: '24px 0' }}
                        id="panel-pending"
                        ariaLabelledby="tab-pending"
                    >
                        {pendingAccessRequests.length > 0 ? (
                            <PendingAccessRequests
                                pendingAccessRequests={pendingAccessRequests}
                                toggleAccessRequest={toggleAccessRequest}
                            />
                        ) : (
                            <NoResults />
                        )}
                    </TabPage>

                    <TabPage
                        visible={hash === '#approved'}
                        style={{ padding: '24px 0' }}
                        id="panel-approved"
                        ariaLabelledby="tab-approved"
                    >
                        {approvedAccessRequests.length > 0 ? (
                            <ApprovedAccessRequests
                                approvedAccessRequests={approvedAccessRequests}
                            />
                        ) : (
                            <NoResults />
                        )}
                    </TabPage>

                    <TabPage
                        visible={hash === '#rejected'}
                        style={{ padding: '24px 0' }}
                        id="panel-rejected"
                        ariaLabelledby="tab-rejected"
                    >
                        {rejectedAccessRequests.length > 0 ? (
                            <RejectedAccessRequests
                                rejectedAccessRequests={rejectedAccessRequests}
                            />
                        ) : (
                            <NoResults />
                        )}
                    </TabPage>

                    <HandleAccessRequestModal
                        entity={accessRequest}
                        isVisible={showHandleAccessRequestModal}
                        onClose={hideHandleAccessRequestModal}
                    />
                </Container>
            </Section>
        </>
    )
}

export default AccessRequests
