import React from 'react'
import { useForm } from 'react-hook-form'
import { Trans, useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import Modal from 'js/components/Modal/Modal'

const HandleDeleteRequestModal = ({ mutate, entity, isVisible, onClose }) => {
    const { t } = useTranslation()

    const {
        formState: { isSubmitting },
        handleSubmit,
    } = useForm()

    const onSubmit = async () => {
        await mutate({
            variables: {
                input: {
                    guid,
                    accept,
                },
            },
            refetchQueries: ['SiteUsers', 'GetAdminAccessData'],
        })
            .then(() => {
                onClose()
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    if (!entity) return null

    const { guid, name, accept } = entity

    return (
        <Modal
            isVisible={isVisible}
            title={
                accept
                    ? t('admin.delete-users', { count: 1 })
                    : t('admin.reject-delete-request')
            }
            size="small"
            onClose={onClose}
        >
            <form onSubmit={handleSubmit(onSubmit)}>
                {accept ? (
                    <Trans i18nKey="admin.delete-user-confirm">
                        Are you sure you want to delete{' '}
                        <strong>{{ name }}</strong>?
                    </Trans>
                ) : (
                    <Trans i18nKey="admin.reject-delete-request-confirm">
                        Are you sure you want to reject the delete request for{' '}
                        <strong>{{ name }}</strong>?
                    </Trans>
                )}
                <Flexer mt>
                    <Button
                        size="normal"
                        variant="tertiary"
                        type="button"
                        onClick={onClose}
                    >
                        {t('action.no-back')}
                    </Button>
                    <Button
                        size="normal"
                        variant="primary"
                        type="submit"
                        loading={isSubmitting}
                    >
                        {accept
                            ? t('action.yes-delete')
                            : t('action.yes-confirm')}
                    </Button>
                </Flexer>
            </form>
        </Modal>
    )
}

const DELETE_ACCOUNT_REQUEST = gql`
    mutation HandleDeleteAccountRequest(
        $input: handleDeleteAccountRequestInput!
    ) {
        handleDeleteAccountRequest(input: $input) {
            success
        }
    }
`

export default graphql(DELETE_ACCOUNT_REQUEST)(HandleDeleteRequestModal)
