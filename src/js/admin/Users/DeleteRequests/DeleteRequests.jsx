import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import PageTitle from 'js/admin/components/PageTitle'
import Button from 'js/components/Button/Button'
import Document from 'js/components/Document'
import Flexer from 'js/components/Flexer/Flexer'
import { Container } from 'js/components/Grid/Grid'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import NoResultsMessage from 'js/components/NoResultsMessage/NoResultsMessage'
import Section from 'js/components/Section/Section'
import Table from 'js/components/Table'

import HandleDeleteRequestModal from './components/HandleDeleteRequestModal'

const DeleteRequests = ({ data }) => {
    const [showHandleDeleteRequestModal, setShowHandleDeleteRequestModal] =
        useState(false)

    const toggleHandleDeleteRequestModal = () =>
        setShowHandleDeleteRequestModal(!showHandleDeleteRequestModal)

    const hideHandleDeleteRequestModal = () => {
        setDeleteRequest()
        setShowHandleDeleteRequestModal(false)
    }

    const [deleteRequest, setDeleteRequest] = useState()

    const { t } = useTranslation()

    if (data.loading) return <LoadingSpinner />
    if (!data?.siteSettings) return null

    const { siteSettings } = data

    return (
        <>
            <Document
                title={t('admin.users')}
                containerTitle={t('admin.title')}
            />
            <Section divider>
                <Container size="small">
                    <PageTitle>{t('admin.delete-requests')}</PageTitle>

                    {siteSettings.deleteAccountRequests.edges.length > 0 ? (
                        <Table>
                            <thead>
                                <tr>
                                    <th>{t('global.name')}</th>
                                    <th>{t('form.email-address')}</th>
                                    <th />
                                </tr>
                            </thead>
                            <tbody>
                                {siteSettings.deleteAccountRequests.edges.map(
                                    (entity, index) => {
                                        const rejectAccessRequest = () => {
                                            setDeleteRequest({
                                                guid: entity.guid,
                                                name: entity.name,
                                                accept: false,
                                            })
                                            toggleHandleDeleteRequestModal()
                                        }

                                        const acceptAccessRequest = () => {
                                            setDeleteRequest({
                                                guid: entity.guid,
                                                name: entity.name,
                                                accept: true,
                                            })
                                            toggleHandleDeleteRequestModal()
                                        }

                                        return (
                                            <tr key={index}>
                                                <td>{entity.name}</td>
                                                <td>{entity.email}</td>
                                                <td>
                                                    <Flexer justifyContent="flex-end">
                                                        <Button
                                                            size="normal"
                                                            variant="tertiary"
                                                            onClick={
                                                                rejectAccessRequest
                                                            }
                                                        >
                                                            {t('action.reject')}
                                                        </Button>

                                                        <Button
                                                            size="normal"
                                                            variant="primary"
                                                            onClick={
                                                                acceptAccessRequest
                                                            }
                                                        >
                                                            {t('action.delete')}
                                                        </Button>

                                                        <HandleDeleteRequestModal
                                                            entity={
                                                                deleteRequest
                                                            }
                                                            isVisible={
                                                                showHandleDeleteRequestModal
                                                            }
                                                            onClose={
                                                                hideHandleDeleteRequestModal
                                                            }
                                                        />
                                                    </Flexer>
                                                </td>
                                            </tr>
                                        )
                                    },
                                )}
                            </tbody>
                        </Table>
                    ) : (
                        <NoResultsMessage
                            title={t('admin.delete-requests-no-results')}
                        />
                    )}
                </Container>
            </Section>
        </>
    )
}

const Query = gql`
    query SiteUsers {
        viewer {
            guid
            user {
                guid
            }
        }
        siteSettings {
            roleOptions {
                value
                label
            }
            deleteAccountRequests {
                edges {
                    guid
                    name
                    icon
                    url
                    roles
                    email
                }
            }
        }
    }
`

export default graphql(Query)(DeleteRequests)
