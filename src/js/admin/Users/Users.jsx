import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { formatISO, startOfDay } from 'date-fns'
import { useDebounce } from 'helpers'

import PageTitle from 'js/admin/components/PageTitle'
import BanUsers from 'js/admin/Users/components/BanUsers'
import DeleteUsers from 'js/admin/Users/components/DeleteUsers'
import UserList from 'js/admin/Users/components/UserList'
import Button from 'js/components/Button/Button'
import DateField from 'js/components/DateField/DateField'
import Document from 'js/components/Document'
import FilterWrapper from 'js/components/FilterWrapper'
import Flexer from 'js/components/Flexer/Flexer'
import { Container } from 'js/components/Grid/Grid'
import Modal from 'js/components/Modal/Modal'
import Section from 'js/components/Section/Section'
import Select from 'js/components/Select/Select'
import Spacer from 'js/components/Spacer/Spacer'
import Textfield from 'js/components/Textfield/Textfield'
import Toolbar from 'js/components/Toolbar/Toolbar'

import ExportUsers from './components/ExportUsers'
import ImportUsersModal from './components/ImportUsersModal'

const Users = ({ data }) => {
    const { t } = useTranslation()

    const [searchInput, setSearchInput] = useState('')

    const [roleFilter, setRoleFilter] = useState('')
    const handleChangeRole = (value) => setRoleFilter(value)

    const [lastOnlineFilter, setLastOnlineFilter] = useState()

    const handleChangeLastOnline = (val) => {
        setLastOnlineFilter(val ? formatISO(startOfDay(new Date(val))) : null)
    }

    const [showImportUsersModal, setShowImportUsersModal] = useState(false)
    const toggleImportUsersModal = () =>
        setShowImportUsersModal(!showImportUsersModal)

    const [showExportUsersModal, setShowExportUsersModal] = useState(false)
    const toggleExportUsersModal = () =>
        setShowExportUsersModal(!showExportUsersModal)

    const [showBanUsersModal, setBanUsersModal] = useState(false)
    const toggleBanUsersModal = () => setBanUsersModal(!showBanUsersModal)

    const [showDeleteUsersModal, setShowDeleteUsersModal] = useState(false)
    const toggleDeleteUsersModal = () =>
        setShowDeleteUsersModal(!showDeleteUsersModal)

    const [selected, setSelected] = useState([])

    const q = useDebounce(searchInput, 200)

    const handleClear = () => {
        setSelected([])
    }

    if (!data.siteSettings) return null

    return (
        <>
            <Document
                title={t('admin.users')}
                containerTitle={t('admin.title')}
            />
            <Section divider>
                <Container size="large">
                    <PageTitle>{t('admin.users')}</PageTitle>

                    <Spacer spacing="small">
                        {selected.length > 0 ? (
                            <div>
                                <Toolbar
                                    count={selected.length}
                                    onClear={handleClear}
                                >
                                    <Flexer
                                        divider="normal"
                                        style={{
                                            marginLeft: 'auto',
                                        }}
                                    >
                                        <Button
                                            size="normal"
                                            variant="secondary"
                                            aria-label={t(
                                                'admin.block-selection',
                                                {
                                                    count: selected.length,
                                                },
                                            )}
                                            onClick={toggleBanUsersModal}
                                        >
                                            {t('action.block')}
                                        </Button>
                                        <Button
                                            size="normal"
                                            variant="secondary"
                                            aria-label={t(
                                                'admin.delete-selection',
                                                {
                                                    count: selected.length,
                                                },
                                            )}
                                            onClick={toggleDeleteUsersModal}
                                        >
                                            {t('action.delete')}
                                        </Button>
                                    </Flexer>
                                </Toolbar>
                            </div>
                        ) : (
                            <Flexer
                                justifyContent="flex-end"
                                style={{
                                    minHeight: '48px',
                                    padding: '4px 0',
                                }}
                            >
                                <Button
                                    size="normal"
                                    variant="secondary"
                                    onClick={toggleImportUsersModal}
                                >
                                    {t('admin.import-users')}
                                </Button>
                                <Button
                                    size="normal"
                                    variant="secondary"
                                    onClick={toggleExportUsersModal}
                                >
                                    {t('admin.export-users')}
                                </Button>
                            </Flexer>
                        )}

                        <FilterWrapper style={{ padding: '4px 0' }}>
                            <div>
                                <Textfield
                                    name="language"
                                    label={t('global.search-user')}
                                    value={searchInput}
                                    onChange={(evt) =>
                                        setSearchInput(evt.target.value)
                                    }
                                    onClear={() => setSearchInput('')}
                                />
                            </div>
                            <Select
                                name="role"
                                label={t('global.role')}
                                options={data.siteSettings.roleOptions}
                                value={roleFilter}
                                onChange={handleChangeRole}
                                isClearable
                            />
                            <DateField
                                name="lastOnlineBefore"
                                label={t('admin.inactive-since')}
                                value={lastOnlineFilter}
                                toDate={formatISO(new Date())}
                                onChange={handleChangeLastOnline}
                                clearLabel={t('admin.clear-inactive-since')}
                                isClearable
                                style={{
                                    marginLeft: 'auto',
                                }}
                            />
                        </FilterWrapper>
                    </Spacer>

                    <UserList
                        name="admin-users-manage"
                        q={q}
                        role={roleFilter}
                        lastOnlineBefore={lastOnlineFilter || null}
                        selected={selected}
                        setSelected={setSelected}
                    />
                </Container>
            </Section>

            <ImportUsersModal
                isVisible={showImportUsersModal}
                onClose={toggleImportUsersModal}
            />

            <Modal
                isVisible={showExportUsersModal}
                title={t('admin.export-users')}
                size="small"
                onClose={toggleExportUsersModal}
            >
                <ExportUsers onClose={toggleExportUsersModal} />
            </Modal>

            <Modal
                isVisible={showBanUsersModal}
                title={t('admin.block-users', { count: selected.length })}
                size="small"
                onClose={toggleBanUsersModal}
            >
                <BanUsers
                    onClose={toggleBanUsersModal}
                    users={selected}
                    afterSubmit={() => setSelected([])}
                />
            </Modal>

            <Modal
                isVisible={showDeleteUsersModal}
                title={t('admin.delete-users', { count: selected.length })}
                size="small"
                onClose={toggleDeleteUsersModal}
            >
                <DeleteUsers
                    users={selected}
                    onClose={toggleDeleteUsersModal}
                    afterSubmit={() => setSelected([])}
                />
            </Modal>
        </>
    )
}

const Query = gql`
    query SiteUsersRoleOptions {
        siteSettings {
            roleOptions {
                value
                label
            }
        }
    }
`

export default graphql(Query)(Users)
