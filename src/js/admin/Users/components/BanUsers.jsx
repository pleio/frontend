import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { Trans, useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'
import { addDays, startOfDay } from 'date-fns'

import AnimatePresence from 'js/components/AnimatePresence/AnimatePresence'
import Button from 'js/components/Button/Button'
import Checkbox from 'js/components/Checkbox/Checkbox'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import Spacer from 'js/components/Spacer/Spacer'

const tomorrow = startOfDay(addDays(new Date(), 1))

const BanUsers = ({ users, isBanned, onClose, afterSubmit }) => {
    const { t } = useTranslation()

    const [doScheduleBanUsers, setDoScheduleBanUsers] = useState(false)

    const [blockUsers] = useMutation(BLOCK_USERS)
    const [scheduleBanUsers] = useMutation(SCHEDULE_BAN_USERS)

    const defaultValues = {
        banReason: '',
        banAt: tomorrow,
    }

    const {
        control,
        errors,
        formState: { isValid, isSubmitting },
        handleSubmit,
    } = useForm({ defaultValues })

    const onAfterSubmit = () => {
        afterSubmit?.()
        onClose()
    }

    const onError = (errors) => console.error(errors)

    const onSubmit = async ({ banReason, banAt }) => {
        if (doScheduleBanUsers) {
            await scheduleBanUsers({
                variables: {
                    input: {
                        guids: users.map((user) => user.guid),
                        banReason,
                        banAt,
                    },
                },
                refetchQueries: ['SiteUsers'],
            })
                .then(onAfterSubmit)
                .catch(onError)
        } else {
            await blockUsers({
                variables: {
                    input: {
                        guids: users.map((user) => user.guid),
                        action: isBanned ? 'unban' : 'ban',
                        banReason: banReason || null,
                    },
                },
                refetchQueries: ['SiteUsers'],
            })
                .then(onAfterSubmit)
                .catch(onError)
        }
    }

    const names = users.map((user) => user.name).join(', ')

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Spacer spacing="small">
                {isBanned ? (
                    <Trans i18nKey="admin.unblock-users-confirm">
                        Are you sure you want to give{' '}
                        <strong>{{ name: names }}</strong> access back to the
                        site?
                    </Trans>
                ) : (
                    <Trans i18nKey="admin.block-users-confirm">
                        Are you sure you want to block{' '}
                        <strong>{{ name: names }}</strong> from accessing the
                        site?
                    </Trans>
                )}

                {!isBanned && (
                    <>
                        <FormItem
                            label={t('admin.reason')}
                            control={control}
                            type="text"
                            name="banReason"
                            errors={errors}
                            required={doScheduleBanUsers}
                        />

                        <Checkbox
                            type="checkbox"
                            size="small"
                            label={t('admin.schedule-blocking')}
                            checked={doScheduleBanUsers}
                            onChange={() =>
                                setDoScheduleBanUsers(!doScheduleBanUsers)
                            }
                        />
                    </>
                )}

                <AnimatePresence visible={doScheduleBanUsers}>
                    <FormItem
                        control={control}
                        type="datetime"
                        name="banAt"
                        fromDateTime={tomorrow}
                        required={doScheduleBanUsers}
                    />
                </AnimatePresence>

                <Flexer mt>
                    <Button
                        size="normal"
                        variant="tertiary"
                        type="button"
                        onClick={onClose}
                    >
                        {t('action.no-back')}
                    </Button>
                    <Button
                        size="normal"
                        variant="primary"
                        type="submit"
                        disabled={!isValid}
                        loading={isSubmitting}
                    >
                        {t('action.yes-confirm')}
                    </Button>
                </Flexer>
            </Spacer>
        </form>
    )
}

const BLOCK_USERS = gql`
    mutation BlockUsers($input: editUsersInput!) {
        editUsers(input: $input) {
            success
        }
    }
`

const SCHEDULE_BAN_USERS = gql`
    mutation ($input: scheduleBanUsersInput!) {
        scheduleBanUsers(input: $input) {
            users {
                guid
            }
        }
    }
`

export default BanUsers
