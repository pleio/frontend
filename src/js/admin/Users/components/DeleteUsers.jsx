import React from 'react'
import { useForm } from 'react-hook-form'
import { Trans, useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'

const DeleteUsers = ({ mutate, users, onClose, afterSubmit }) => {
    const { t } = useTranslation()

    const {
        formState: { isSubmitting },
        handleSubmit,
    } = useForm()

    const onSubmit = async () => {
        await mutate({
            variables: {
                input: {
                    guids: users.map((user) => user.guid),
                },
            },
            refetchQueries: ['SiteUsers'],
        })
            .then(() => {
                afterSubmit?.()
                onClose()
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    const names = users.map((user) => user.name).join(', ')

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Trans i18nKey="admin.delete-user-confirm">
                Are you sure you want to delete{' '}
                <strong>{{ name: names }}</strong>?
            </Trans>
            <Flexer mt>
                <Button
                    size="normal"
                    variant="tertiary"
                    type="button"
                    onClick={onClose}
                >
                    {t('action.no-back')}
                </Button>
                <Button
                    size="normal"
                    variant="primary"
                    type="submit"
                    loading={isSubmitting}
                >
                    {t('action.yes-delete')}
                </Button>
            </Flexer>
        </form>
    )
}

const DELETE_USERS = gql`
    mutation DeleteUsers($input: deleteUsersInput!) {
        deleteUsers(input: $input) {
            success
        }
    }
`
export default graphql(DELETE_USERS)(DeleteUsers)
