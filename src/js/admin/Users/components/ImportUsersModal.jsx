import React, { useEffect, useRef, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { Form, Formik } from 'formik'
import compose from 'lodash.flowright'
import * as yup from 'yup'

import Button from 'js/components/Button/Button'
import Errors from 'js/components/Errors'
import FileInput from 'js/components/FileInput/FileInput'
import Flexer from 'js/components/Flexer/Flexer'
import Modal from 'js/components/Modal/Modal'
import Table from 'js/components/Table'
import Text from 'js/components/Text/Text'

import ImportUsersField from './ImportUsersField'

const ImportUsersModal = ({
    mutateStep1,
    mutateStep2,
    data,
    isVisible,
    onClose,
}) => {
    const { t } = useTranslation()

    useEffect(() => {
        setFormData()
        setStep(1)
    }, [isVisible])

    const [error, setError] = useState()
    const [step, setStep] = useState(1)
    const [formData, setFormData] = useState()

    const refUploadInput = useRef()

    if (!data.siteSettings) return null

    const handleClickUpload = () => {
        refUploadInput.current.value = ''
        refUploadInput.current.click()
    }

    const handleUpload = (file) => {
        const reader = new FileReader()
        reader.onloadend = () => {
            mutateStep1({
                variables: {
                    input: {
                        usersCsv: file,
                    },
                },
            })
                .then(({ data }) => {
                    setError()
                    const {
                        importId,
                        accessIdOptions,
                        csvColumns,
                        userFields,
                    } = data.importUsersStep1
                    setFormData({
                        importId,
                        accessIdOptions,
                        csvColumns,
                        userFields,
                    })
                    setStep(2)
                })
                .catch((error) => {
                    console.error(error)
                    setError(error)
                })
        }
        reader.readAsDataURL(file)
    }

    const initialValues = !!formData && {
        fields: formData.csvColumns.map((column) => ({
            csvColumn: column,
            userField: '',
            accessId: 0,
            forceAccess: false,
        })),
    }

    const validationSchema = yup.object().shape({
        fields: yup.array().of(
            yup.object().shape({
                csvColumn: yup.string(),
                userField: yup.string(),
                accessId: yup.number(),
                forceAccess: yup.boolean(),
            }),
        ),
    })

    const handleBack = () => {
        setStep(1)
        setFormData()
    }

    const handleSubmit = (values) => {
        const fields = values.fields.filter((field) => !!field.userField)

        mutateStep2({
            variables: {
                input: {
                    importId: formData.importId,
                    fields,
                },
            },
        })
            .then(() => {
                setError()
                setStep(3)
            })
            .catch((error) => {
                console.error(error)
                setError(error)
            })
    }

    return (
        <Modal
            isVisible={isVisible}
            title={t('admin.import-users')}
            size={step === 2 ? 'normal' : 'small'}
        >
            {step === 1 && (
                <>
                    <Text size="small">{t('admin.import-users-step1')}</Text>
                    <Flexer mt>
                        <Button
                            size="normal"
                            variant="tertiary"
                            onClick={onClose}
                        >
                            {t('action.cancel')}
                        </Button>
                        <Button
                            size="normal"
                            variant="secondary"
                            onClick={handleClickUpload}
                        >
                            {t('action.upload-csv')}
                        </Button>
                        <FileInput
                            ref={refUploadInput}
                            name="ImportUsersCsv"
                            accept=".csv"
                            maxSize={10}
                            onSubmit={handleUpload}
                            style={{ display: 'none' }}
                        />
                    </Flexer>
                </>
            )}
            {step === 2 && (
                <Formik
                    validateOnBlur={false}
                    validateOnChange={false}
                    initialValues={initialValues}
                    validationSchema={validationSchema}
                    onSubmit={handleSubmit}
                >
                    {({ isSubmitting, values, setFieldValue }) => (
                        <Form>
                            <Text size="small" style={{ marginBottom: '16px' }}>
                                {t('admin.import-users-step2')}
                            </Text>
                            <Table style={{ overflowX: 'visible' }}>
                                <thead>
                                    <tr>
                                        <th />
                                        <th style={{ width: '50%' }}>
                                            {t('admin.source-field')}
                                        </th>
                                        <th
                                            style={{
                                                width: '50%',
                                                minWidth: '130px',
                                            }}
                                        >
                                            {t('admin.target-field')}
                                        </th>
                                        <th
                                            style={{
                                                textAlign: 'center',
                                            }}
                                        >
                                            {t('global.access')}
                                        </th>
                                        <th style={{ textAlign: 'center' }}>
                                            {t('global.force')}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {values.fields.map((field, i) => (
                                        <ImportUsersField
                                            key={i}
                                            index={i}
                                            entity={field}
                                            userFields={formData.userFields}
                                            accessIdOptions={
                                                formData.accessIdOptions
                                            }
                                            setFieldValue={setFieldValue}
                                        />
                                    ))}
                                </tbody>
                            </Table>
                            <Errors errors={error} />
                            <Flexer mt>
                                <Button
                                    size="normal"
                                    variant="tertiary"
                                    onClick={handleBack}
                                >
                                    {t('action.back')}
                                </Button>
                                <Button
                                    type="submit"
                                    size="normal"
                                    variant="primary"
                                    isLoading={isSubmitting}
                                    disabled={
                                        !values.fields.find(
                                            (field) =>
                                                field.userField === 'id' ||
                                                field.userField === 'email',
                                        )
                                    }
                                >
                                    {t('action.import')}
                                </Button>
                            </Flexer>
                        </Form>
                    )}
                </Formik>
            )}
            {step === 3 && (
                <div>
                    <Text size="small">{t('admin.import-users-step3')}</Text>
                    <Flexer mt>
                        <Button
                            size="normal"
                            variant="primary"
                            onClick={onClose}
                        >
                            {t('action.ok')}
                        </Button>
                    </Flexer>
                </div>
            )}
        </Modal>
    )
}

const Query = gql`
    query SiteAdvancedSettings {
        siteSettings {
            exportableUserFields {
                field_type
                field
                label
            }
            profileFields {
                guid
                name
            }
        }
    }
`

const Step1Mutation = gql`
    mutation ImportUsersStep1($input: importUsersStep1Input!) {
        importUsersStep1(input: $input) {
            importId
            csvColumns
            userFields {
                value
                label
            }
            accessIdOptions {
                value
                label
            }
        }
    }
`
const Step2Mutation = gql`
    mutation ImportUsersStep2($input: importUsersStep2Input!) {
        importUsersStep2(input: $input) {
            success
        }
    }
`

export default compose(
    graphql(Query),
    graphql(Step1Mutation, { name: 'mutateStep1' }),
    graphql(Step2Mutation, { name: 'mutateStep2' }),
)(ImportUsersModal)
