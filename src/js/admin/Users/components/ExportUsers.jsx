import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { produce } from 'immer'

import Button from 'js/components/Button/Button'
import Checkbox from 'js/components/Checkbox/Checkbox'
import Flexer from 'js/components/Flexer/Flexer'
import { Col, Row } from 'js/components/Grid/Grid'
import { H4 } from 'js/components/Heading'

const ExportUsers = ({ data, onClose }) => {
    if (!data.siteSettings) return null

    const [userFields, setUserFields] = useState([])
    const [profileFields, setProfileFields] = useState([])

    const handleClose = () => {
        setUserFields([])
        setProfileFields([])
        onClose()
    }

    const handleSubmit = () => {
        const params = []
        userFields.forEach((field) => params.push(`user_fields[]=${field}`))
        profileFields.forEach((field) =>
            params.push(`profile_field_guids[]=${field}`),
        )
        window.location = `/exporting/users?${params.join('&')}`
    }

    const { t } = useTranslation()

    return (
        <>
            <Row>
                <Col mobileLandscapeUp={1 / 2}>
                    <H4 style={{ marginBottom: '2px' }}>Account</H4>
                    {data.siteSettings.exportableUserFields.map((field) => {
                        const setValue = (evt) => {
                            const fieldIndex = userFields.indexOf(field.field)
                            const fieldExists = fieldIndex > -1

                            if (evt.target.checked) {
                                !fieldExists &&
                                    setUserFields([...userFields, field.field])
                            } else {
                                fieldExists &&
                                    setUserFields(
                                        produce((newState) => {
                                            newState.splice(fieldIndex, 1)
                                        }),
                                    )
                            }
                        }

                        return (
                            <Checkbox
                                key={field.field}
                                name={field.field}
                                label={field.label}
                                size="small"
                                onChange={setValue}
                            />
                        )
                    })}
                </Col>
                <Col mobileLandscapeUp={1 / 2}>
                    <H4 style={{ marginBottom: '2px' }}>Profile</H4>
                    {data.siteSettings.profileFields.map((field) => {
                        const setValue = (evt) => {
                            const fieldIndex = profileFields.indexOf(field.guid)
                            const fieldExists = fieldIndex > -1

                            if (evt.target.checked) {
                                !fieldExists &&
                                    setProfileFields([
                                        ...profileFields,
                                        field.guid,
                                    ])
                            } else {
                                fieldExists &&
                                    setProfileFields(
                                        produce((newState) => {
                                            newState.splice(fieldIndex, 1)
                                        }),
                                    )
                            }
                        }

                        return (
                            <Checkbox
                                key={field.guid}
                                name={field.name}
                                label={field.name}
                                size="small"
                                onChange={setValue}
                            />
                        )
                    })}
                </Col>
            </Row>

            <Flexer mt>
                <Button
                    size="normal"
                    variant="tertiary"
                    type="button"
                    onClick={handleClose}
                >
                    {t('action.cancel')}
                </Button>
                <Button size="normal" variant="primary" onClick={handleSubmit}>
                    {t('action.export')}
                </Button>
            </Flexer>
        </>
    )
}

const Query = gql`
    query SiteAdvancedSettings {
        siteSettings {
            exportableUserFields {
                field_type
                field
                label
            }
            profileFields {
                guid
                name
            }
        }
    }
`

export default graphql(Query)(ExportUsers)
