import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'
import { useIsMount, useLocalStorage } from 'helpers'

import User from 'js/admin/Users/User/User'
import FetchMore from 'js/components/FetchMore'
import { H4 } from 'js/components/Heading'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Table from 'js/components/Table'
import Text from 'js/components/Text/Text'

const UserList = ({
    q,
    role,
    name,
    lastOnlineBefore,
    isBanned,
    isScheduledForBan,
    selected,
    setSelected,
}) => {
    const { t } = useTranslation()

    const [orderBy, setOrderBy] = useLocalStorage(`${name}-orderBy`, 'name')

    const [orderDirection, setOrderDirection] = useLocalStorage(
        `${name}-orderDirection`,
        'asc',
    )

    const [queryLimit, setQueryLimit] = useState(50)
    const { loading, data, fetchMore, refetch } = useQuery(Query, {
        variables: {
            q,
            role,
            lastOnlineBefore,
            isBanned,
            offset: 0,
            limit: queryLimit,
            orderBy,
            orderDirection,
            filterScheduledForBan: isScheduledForBan ? 'isScheduled' : 'any',
        },
    })

    const isMount = useIsMount()

    useEffect(() => {
        if (isMount) {
            refetch()
        }
    })

    const toggleSelected = (user, isSelected) => {
        if (!isSelected) {
            setSelected([...selected, user])
        } else {
            const index = selected.findIndex((el) => el.guid === user.guid)
            const newSelected = [...selected]
            newSelected.splice(index, 1)
            setSelected(newSelected)
        }
    }

    return (
        <>
            {loading ? (
                <LoadingSpinner />
            ) : (
                <>
                    <H4 role="status" style={{ margin: '16px 0 4px' }}>
                        {t('user.result', {
                            count: data?.siteUsers?.total ?? 0,
                        })}
                    </H4>
                    <Table
                        rowHeight="small"
                        headers={[
                            {
                                name: 'name',
                                label: t('global.name'),
                                style: { minWidth: '200px' },
                            },
                            ...(isBanned || isScheduledForBan
                                ? [
                                      {
                                          name: 'banReason',
                                          label: t('admin.reason'),
                                      },
                                  ]
                                : []),
                            {
                                name: 'email',
                                label: t('form.email-address'),
                            },
                            {
                                options: [
                                    ...(isBanned
                                        ? [
                                              {
                                                  name: 'bannedSince',
                                                  label: t(
                                                      'admin.blocked-since',
                                                  ),
                                                  defaultOrderDirection: 'desc',
                                              },
                                          ]
                                        : []),
                                    ...(isScheduledForBan
                                        ? [
                                              {
                                                  name: 'banScheduledAt',
                                                  label: t('admin.block-on'),
                                              },
                                          ]
                                        : []),
                                    {
                                        name: 'lastOnline',
                                        label: t('admin.last-online'),
                                        defaultOrderDirection: 'desc',
                                    },
                                ],
                                align: 'right',
                            },
                            {
                                style: { width: '40px' },
                            },
                        ]}
                        orderBy={orderBy}
                        orderDirection={orderDirection}
                        setOrderBy={setOrderBy}
                        setOrderDirection={setOrderDirection}
                    >
                        <FetchMore
                            as="tbody"
                            edges={data?.siteUsers?.edges}
                            getMoreResults={(data) => data.siteUsers.edges}
                            fetchMore={fetchMore}
                            fetchType="scroll"
                            fetchCount={50}
                            setLimit={setQueryLimit}
                            maxLimit={data?.siteUsers?.total}
                            noResults={
                                <Text
                                    textAlign="center"
                                    style={{ padding: '20px' }}
                                >
                                    {t('user.no-results')}
                                </Text>
                            }
                        >
                            {data?.siteUsers?.edges.map((entity) => {
                                const isSelected =
                                    selected.findIndex(
                                        (el) => el.guid === entity.guid,
                                    ) !== -1

                                const handleToggle = () => {
                                    toggleSelected(entity, isSelected)
                                }

                                return (
                                    <User
                                        key={entity.guid}
                                        entity={entity}
                                        isBanned={isBanned}
                                        isScheduledForBan={isScheduledForBan}
                                        viewer={data.viewer}
                                        isSelected={isSelected}
                                        siteSettings={data.siteSettings}
                                        onToggle={handleToggle}
                                        orderBy={orderBy}
                                        clearSelected={() => setSelected([])}
                                    />
                                )
                            })}
                        </FetchMore>
                    </Table>
                </>
            )}
        </>
    )
}

const Query = gql`
    query SiteUsers(
        $q: String!
        $offset: Int!
        $limit: Int!
        $role: String
        $isBanned: Boolean
        $filterScheduledForBan: UserBanScheduledFilter
        $lastOnlineBefore: String
        $orderBy: SiteUsersOrderBy
        $orderDirection: OrderDirection
    ) {
        viewer {
            guid
            user {
                guid
            }
        }
        siteSettings {
            roleOptions {
                value
                label
            }
            profileSets {
                id
                field {
                    name
                }
            }
        }
        siteUsers(
            q: $q
            offset: $offset
            limit: $limit
            role: $role
            isBanned: $isBanned
            filterScheduledForBan: $filterScheduledForBan
            lastOnlineBefore: $lastOnlineBefore
            orderBy: $orderBy
            orderDirection: $orderDirection
        ) {
            total
            edges {
                guid
                name
                icon
                url
                email
                roles
                lastOnline
                isSuperadmin
                canDelete
                canBan
                profileSetManager {
                    id
                }
                banReason
                bannedSince
                banScheduledAt
            }
        }
    }
`

export default UserList
