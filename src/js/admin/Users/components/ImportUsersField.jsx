import React from 'react'
import { useTranslation } from 'react-i18next'
import { Field } from 'formik'

import Checkbox from 'js/components/Form/FormikCheckbox'
import FormikDropdown from 'js/components/Form/FormikDropdown'
import FormikSelect from 'js/components/Form/FormikSelect'
import IconButton from 'js/components/IconButton/IconButton'

import IconPublic from 'icons/earth.svg'
import IconPrivate from 'icons/eye-off.svg'
import IconUsers from 'icons/users.svg'

const ImportUsersField = ({ index, entity, userFields, accessIdOptions }) => {
    const disableAccessId =
        !entity.userField ||
        ['id', 'email', 'name'].indexOf(entity.userField) !== -1

    const accessIdIcons = [
        <IconPrivate key={0} />,
        <IconUsers key={1} />,
        <IconPublic key={3} />,
    ]

    const { t } = useTranslation()

    return (
        <tr>
            <td>{index}</td>
            <td>{entity.csvColumn}</td>
            <td>
                <FormikSelect
                    name={`fields[${index}].userField`}
                    options={userFields}
                    isClearable
                />
            </td>
            <td>
                <FormikDropdown
                    name={`fields[${index}].accessId`}
                    options={accessIdOptions}
                >
                    <IconButton
                        variant="secondary"
                        size="large"
                        disabled={disableAccessId}
                        style={disableAccessId ? { opacity: 0 } : {}}
                        aria-label={t('form.access-level')}
                    >
                        {accessIdIcons[entity.accessId]}
                    </IconButton>
                </FormikDropdown>
            </td>
            <td>
                <Field
                    name={`fields[${index}].forceAccess`}
                    component={Checkbox}
                    size="small"
                    disabled={disableAccessId}
                    style={{
                        height: '100%',
                        display: 'flex',
                        justifyContent: 'center',
                        opacity: disableAccessId && 0,
                    }}
                />
            </td>
        </tr>
    )
}

export default ImportUsersField
