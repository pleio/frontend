import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'

import PageTitle from 'js/admin/components/PageTitle'
import Document from 'js/components/Document'
import { Container } from 'js/components/Grid/Grid'
import Section from 'js/components/Section/Section'
import Spacer from 'js/components/Spacer/Spacer'
import TabMenu from 'js/components/TabMenu'
import TabPage from 'js/components/TabPage'

import Blocked from './Blocked'
import Scheduled from './Scheduled'

const BlockedAndScheduled = () => {
    const { t } = useTranslation()

    const [tab, setTab] = useState('blocked')

    return (
        <>
            <Document
                title={t('admin.users')}
                containerTitle={t('admin.title')}
            />
            <Section divider>
                <Container size="large">
                    <PageTitle>{t('admin.blocked')}</PageTitle>

                    <Spacer spacing="small">
                        <TabMenu
                            label={t('admin.blocked')}
                            showBorder
                            options={[
                                {
                                    label: t('admin.blocked'),
                                    onClick: () => setTab('blocked'),
                                    isActive: tab === 'blocked',
                                    id: 'tab-blocked',
                                    ariaControls: 'panel-blocked',
                                },
                                {
                                    label: t('admin.scheduled'),
                                    onClick: () => setTab('scheduled'),
                                    isActive: tab === 'scheduled',
                                    id: 'tab-scheduled',
                                    ariaControls: 'panel-scheduled',
                                },
                            ]}
                        />

                        <TabPage
                            id="panel-blocked"
                            visible={tab === 'blocked'}
                            ariaLabelledby="tab-blocked"
                        >
                            <Blocked />
                        </TabPage>

                        <TabPage
                            id="panel-scheduled"
                            visible={tab === 'scheduled'}
                            ariaLabelledby="tab-scheduled"
                        >
                            <Scheduled />
                        </TabPage>
                    </Spacer>
                </Container>
            </Section>
        </>
    )
}

export default BlockedAndScheduled
