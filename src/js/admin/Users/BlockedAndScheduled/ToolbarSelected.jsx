import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'

import BanUsers from 'js/admin/Users/components/BanUsers'
import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import Modal from 'js/components/Modal/Modal'
import Toolbar from 'js/components/Toolbar/Toolbar'

const ToolbarSelected = ({ selected, onClear }) => {
    const { t } = useTranslation()

    const [showBanUsersModal, setBanUsersModal] = useState(false)
    const toggleBanUsersModal = () => setBanUsersModal(!showBanUsersModal)

    if (selected.length === 0) return null

    return (
        <>
            <Toolbar count={selected.length} onClear={onClear}>
                <Flexer
                    divider="normal"
                    style={{
                        marginLeft: 'auto',
                    }}
                >
                    <Button
                        size="normal"
                        variant="secondary"
                        aria-label={t('admin.unblock-selection', {
                            count: selected.length,
                        })}
                        onClick={toggleBanUsersModal}
                    >
                        {t('action.unblock')}
                    </Button>
                </Flexer>
            </Toolbar>

            <Modal
                isVisible={showBanUsersModal}
                title={t('admin.unblock-users', { count: selected.length })}
                size="small"
                onClose={toggleBanUsersModal}
            >
                <BanUsers
                    onClose={toggleBanUsersModal}
                    users={selected}
                    afterSubmit={onClear}
                    isBanned
                />
            </Modal>
        </>
    )
}

export default ToolbarSelected
