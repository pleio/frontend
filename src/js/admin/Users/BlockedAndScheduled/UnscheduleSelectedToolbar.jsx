import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import Toolbar from 'js/components/Toolbar/Toolbar'

import UnscheduleModal from './UnscheduleModal'

const UnscheduleSelectedToolbar = ({ selected, onClear }) => {
    const { t } = useTranslation()

    const [showModal, setShowModal] = useState(false)
    const toggleModal = () => setShowModal(!showModal)

    if (selected.length === 0) return null

    return (
        <>
            <Toolbar count={selected.length} onClear={onClear}>
                <Flexer
                    divider="normal"
                    style={{
                        marginLeft: 'auto',
                    }}
                >
                    <Button
                        size="normal"
                        variant="secondary"
                        onClick={toggleModal}
                        aria-label={t('admin.unschedule-selection', {
                            count: selected.length,
                        })}
                    >
                        {t('action.unschedule-block')}
                    </Button>
                </Flexer>
            </Toolbar>

            <UnscheduleModal
                isVisible={showModal}
                users={selected}
                onClose={toggleModal}
                onClear={onClear}
            />
        </>
    )
}

export default UnscheduleSelectedToolbar
