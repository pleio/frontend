import React, { useState } from 'react'
import { useDebounce } from 'helpers'

import UserList from 'js/admin/Users/components/UserList'

import Filters from './Filters'
import { useAdmin } from './hooks/useAdmin'
import UnscheduleSelectedToolbar from './UnscheduleSelectedToolbar'

const Blocked = () => {
    const [searchInput, setSearchInput] = useState('')

    const q = useDebounce(searchInput, 200)

    const [roleFilter, setRoleFilter] = useState('')

    const [selected, setSelected] = useState([])

    const isAdmin = useAdmin(roleFilter)

    return (
        <>
            <UnscheduleSelectedToolbar
                onClear={() => setSelected([])}
                selected={selected}
            />

            <Filters
                searchInput={searchInput}
                setSearchInput={setSearchInput}
                roleFilter={roleFilter}
                setRoleFilter={setRoleFilter}
                canExportUsers={false}
            />

            <UserList
                q={q}
                isAdmin={isAdmin}
                role={roleFilter}
                selected={selected}
                setSelected={setSelected}
                name="admin-users-scheduled"
                isScheduledForBan
            />
        </>
    )
}

export default Blocked
