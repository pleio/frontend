import React, { useState } from 'react'
import { Trans, useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import Modal from 'js/components/Modal/Modal'

const UnscheduleModal = ({ users, isVisible, onClose, onClear }) => {
    const { t } = useTranslation()

    const [processing, setProcessing] = useState(false)

    const [unscheduleUsers] = useMutation(UNSCHEDULE_BAN_USERS)

    const handleUnschedule = async () => {
        setProcessing(true)

        await unscheduleUsers({
            variables: {
                input: {
                    guids: users.map((user) => user.guid),
                    banReason: null,
                    banAt: null,
                },
            },
            refetchQueries: ['SiteUsers'],
        })
            .then(() => {
                setProcessing(false)
                onClose()
                onClear()
            })
            .catch((errors) => {
                setProcessing(false)
                console.error(errors)
            })
    }

    const names = users.map((user) => user.name).join(', ')

    return (
        <>
            <Modal
                isVisible={isVisible}
                title={t('admin.unschedule-selection', { count: users.length })}
                size="small"
                onClose={onClose}
            >
                <Trans i18nKey="admin.unschedule-users-confirm">
                    Are you sure you want to unschedule blocking of{' '}
                    <strong>{{ name: names }}</strong>?
                </Trans>

                <Flexer mt>
                    <Button
                        size="normal"
                        variant="tertiary"
                        type="button"
                        onClick={onClose}
                    >
                        {t('action.cancel')}
                    </Button>
                    <Button
                        size="normal"
                        variant="primary"
                        type="submit"
                        loading={processing}
                        onClick={handleUnschedule}
                    >
                        {t('action.ok')}
                    </Button>
                </Flexer>
            </Modal>
        </>
    )
}

export default UnscheduleModal

const UNSCHEDULE_BAN_USERS = gql`
    mutation ($input: scheduleBanUsersInput!) {
        scheduleBanUsers(input: $input) {
            users {
                guid
            }
        }
    }
`
