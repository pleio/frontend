import React, { useState } from 'react'
import { useDebounce } from 'helpers'

import UserList from 'js/admin/Users/components/UserList'

import Filters from './Filters'
import { useAdmin } from './hooks/useAdmin'
import ToolbarSelected from './ToolbarSelected'

const Blocked = () => {
    const [searchInput, setSearchInput] = useState('')

    const q = useDebounce(searchInput, 200)

    const [roleFilter, setRoleFilter] = useState('')

    const [selected, setSelected] = useState([])

    const isAdmin = useAdmin(roleFilter)

    return (
        <>
            <ToolbarSelected
                onClear={() => setSelected([])}
                selected={selected}
            />

            <Filters
                searchInput={searchInput}
                setSearchInput={setSearchInput}
                roleFilter={roleFilter}
                setRoleFilter={setRoleFilter}
                canExportUsers={!selected.length}
            />

            <UserList
                q={q}
                isAdmin={isAdmin}
                role={roleFilter}
                selected={selected}
                setSelected={setSelected}
                isBanned
                name="admin-users-blocked"
            />
        </>
    )
}

export default Blocked
