import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'

import Button from 'js/components/Button/Button'
import FilterWrapper from 'js/components/FilterWrapper'
import Flexer from 'js/components/Flexer/Flexer'
import Select from 'js/components/Select/Select'
import Textfield from 'js/components/Textfield/Textfield'

function onClickExportUsers() {
    window.location = `/exporting/banned-users`
}

const Filters = ({
    searchInput,
    setSearchInput,
    roleFilter,
    setRoleFilter,
    canExportUsers = false,
}) => {
    const { t } = useTranslation()

    const { data } = useQuery(GET_USER_ROLES)

    if (!data?.siteSettings) return null

    return (
        <Flexer
            justifyContent="space-between"
            style={{
                padding: '4px 0',
            }}
        >
            <FilterWrapper style={{ padding: '4px 0', flexGrow: 1 }}>
                <div>
                    <Textfield
                        name="language"
                        label={t('global.search-user')}
                        value={searchInput}
                        onChange={(evt) => setSearchInput(evt.target.value)}
                        onClear={() => setSearchInput('')}
                    />
                </div>
                <Select
                    name="role"
                    label={t('global.role')}
                    options={data.siteSettings.roleOptions}
                    value={roleFilter}
                    onChange={setRoleFilter}
                    isClearable
                />
            </FilterWrapper>

            {canExportUsers && (
                <Button
                    size="normal"
                    variant="secondary"
                    onClick={onClickExportUsers}
                >
                    {t('admin.export-users')}
                </Button>
            )}
        </Flexer>
    )
}

const GET_USER_ROLES = gql`
    query SiteUsersRoleOptions {
        siteSettings {
            roleOptions {
                value
                label
            }
        }
    }
`

export default Filters
