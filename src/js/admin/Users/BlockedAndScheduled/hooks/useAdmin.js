export const useAdmin = (roleFilter) =>
    roleFilter === 'admin' ? true : roleFilter === 'user' ? false : undefined
