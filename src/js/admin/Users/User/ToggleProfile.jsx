import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { Trans, useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'

export const useProfileOptions = ({ profileSets, userProfileSets }) => {
    const { t } = useTranslation()

    const [showProfileConfirmation, setShowProfileConfirmation] =
        useState(false)

    const [activeProfile, setActiveProfile] = useState(null)

    const userProfileIds = userProfileSets.map(({ id }) => id)

    const profileOptions = profileSets.map(({ id, field }) => ({
        name: userProfileIds.includes(id)
            ? t('admin.remove-profile-set', { profile: field.name })
            : t('admin.add-profile-set', { profile: field.name }),
        onClick: () => {
            setShowProfileConfirmation(true)
            setActiveProfile({ id, field })
        },
    }))

    return {
        profileOptions,
        showProfileConfirmation,
        setShowProfileConfirmation,
        activeProfile,
    }
}

const ToggleProfile = ({ entity, activeProfile, onClose }) => {
    const { t } = useTranslation()
    const {
        formState: { isSubmitting },
        handleSubmit,
    } = useForm()

    const { guid, name, profileSetManager } = entity

    const hasCurrentProfile = !!profileSetManager.find(
        (profile) => profile.id === activeProfile.id,
    )

    const mutation = hasCurrentProfile
        ? REMOVE_PROFILE_SET_MANAGER
        : ADD_PROFILE_SET_MANAGER

    const [mutateFunction, { error }] = useMutation(mutation, {
        refetchQueries: ['SiteUsers'],
    })

    const onSubmit = async () => {
        await mutateFunction({
            variables: {
                userGuid: guid,
                profileSetGuid: activeProfile.id,
            },
        })
            .then(() => {
                onClose()
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    if (error) console.error(error)

    return (
        <>
            {hasCurrentProfile ? (
                <Trans i18nKey="profile.remove-profile-set-confirm">
                    Are you sure you want to remove the profile{' '}
                    {{ profile: activeProfile.field.name }} from{' '}
                    <strong>{{ name }}</strong>?
                </Trans>
            ) : (
                <Trans i18nKey="profile.add-profile-set-confirm">
                    Are you sure you want to give the profile{' '}
                    {{ profile: activeProfile.field.name }} to{' '}
                    <strong>{{ name }}</strong>?
                </Trans>
            )}
            <form onSubmit={handleSubmit(onSubmit)}>
                <Flexer mt>
                    <Button
                        size="normal"
                        variant="tertiary"
                        type="button"
                        onClick={onClose}
                    >
                        {t('action.no-back')}
                    </Button>
                    <Button
                        size="normal"
                        variant="primary"
                        type="submit"
                        loading={isSubmitting}
                    >
                        {t('action.yes-confirm')}
                    </Button>
                </Flexer>
            </form>
        </>
    )
}

const ADD_PROFILE_SET_MANAGER = gql`
    mutation AddProfileSetManager(
        $userGuid: String!
        $profileSetGuid: String!
    ) {
        addProfileSetManager(
            userGuid: $userGuid
            profileSetGuid: $profileSetGuid
        ) {
            user {
                guid
            }
        }
    }
`

const REMOVE_PROFILE_SET_MANAGER = gql`
    mutation RemoveProfileSetManager(
        $userGuid: String!
        $profileSetGuid: String!
    ) {
        removeProfileSetManager(
            userGuid: $userGuid
            profileSetGuid: $profileSetGuid
        ) {
            user {
                guid
            }
        }
    }
`

export default ToggleProfile
