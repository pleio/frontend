import React from 'react'
import { useForm } from 'react-hook-form'
import { Trans, useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'

const ToggleRole = ({ mutate, togglingRole, guid, name, onClose }) => {
    const { t } = useTranslation()

    const { role, hasRole, roleLabel } = togglingRole

    const {
        formState: { isSubmitting },
        handleSubmit,
    } = useForm()

    const onSubmit = async () => {
        await mutate({
            variables: {
                input: {
                    guid,
                    role,
                },
            },
            refetchQueries: ['SiteUsers'],
        })
            .then(() => {
                onClose()
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            {hasRole ? (
                <Trans i18nKey="role.remove-role-confirm">
                    Are you sure you want to remove the role{' '}
                    {{ role: roleLabel }} of <strong>{{ name }}</strong>?
                </Trans>
            ) : (
                <Trans i18nKey="role.add-role-confirm">
                    Are you sure you want to add the role {{ role: roleLabel }}{' '}
                    to <strong>{{ name }}</strong> ?
                </Trans>
            )}
            <Flexer mt>
                <Button
                    size="normal"
                    variant="tertiary"
                    type="button"
                    onClick={onClose}
                >
                    {t('action.no-back')}
                </Button>
                <Button
                    size="normal"
                    variant="primary"
                    type="submit"
                    loading={isSubmitting}
                >
                    {t('action.yes-confirm')}
                </Button>
            </Flexer>
        </form>
    )
}

const Mutation = gql`
    mutation ToggleUserRole($input: toggleUserRoleInput!) {
        toggleUserRole(input: $input) {
            success
        }
    }
`
export default graphql(Mutation)(ToggleRole)
