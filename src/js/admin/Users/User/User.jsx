import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import useRoleLabel from 'helpers/useRoleLabel'
import UnscheduleModal from 'src/js/admin/Users/BlockedAndScheduled/UnscheduleModal'
import styled from 'styled-components'

import BanUsers from 'js/admin/Users/components/BanUsers'
import DeleteUsers from 'js/admin/Users/components/DeleteUsers'
import Avatar from 'js/components/Avatar/Avatar'
import Checkbox from 'js/components/Checkbox/Checkbox'
import DisplayDate from 'js/components/DisplayDate'
import DropdownButton from 'js/components/DropdownButton/DropdownButton'
import Flexer from 'js/components/Flexer/Flexer'
import IconButton from 'js/components/IconButton/IconButton'
import Modal from 'js/components/Modal/Modal'

import OptionsIcon from 'icons/options.svg'

import ToggleProfile, { useProfileOptions } from './ToggleProfile'
import ToggleRole from './ToggleRole'

const Wrapper = styled.div`
    height: 100%;
    display: flex;
    align-items: center;
    position: relative;

    .AvatarRole {
        z-index: 2;
    }
`

const User = ({
    entity,
    viewer,
    siteSettings,
    isSelected,
    onToggle,
    orderBy,
    clearSelected,
    isBanned = false,
    isScheduledForBan = false,
}) => {
    const { roleOptions, profileSets } = siteSettings
    const [togglingRole, setTogglingRole] = useState()

    const [showToggleRoleModal, setShowToggleRoleModal] = useState(false)
    const toggleRoleModal = () => setShowToggleRoleModal(!showToggleRoleModal)

    const [showBanUsersModal, setBanUsersModal] = useState(false)
    const toggleBanUsersModal = () => setBanUsersModal(!showBanUsersModal)

    const [showUnscheduleUserModal, setUnscheduleUserModal] = useState(false)
    const toggleUnscheduleUserModal = () =>
        setUnscheduleUserModal(!showUnscheduleUserModal)

    const [showDeleteUserModal, setShowDeleteUserModal] = useState(false)
    const toggleDeleteUserModal = () =>
        setShowDeleteUserModal(!showDeleteUserModal)

    const hideToggleRoleModal = () => {
        setTogglingRole()
        setShowToggleRoleModal(false)
    }

    const handleToggleRole = (role, hasRole, roleLabel) => {
        setTogglingRole({
            role,
            hasRole,
            roleLabel,
        })
        toggleRoleModal()
    }

    const { t } = useTranslation()

    const {
        guid,
        name,
        icon,
        url,
        email,
        roles,
        lastOnline,
        isSuperadmin,
        canDelete,
        canBan,
        banReason,
        bannedSince,
        banScheduledAt,
    } = entity

    const isViewer = viewer.user && viewer.user.guid === guid

    let isAdmin, isEditor, isQuestionManager, isNewsEditor, isUserAdmin

    if (roles.length) {
        isAdmin = roles.indexOf('ADMIN') !== -1
        isEditor = roles.indexOf('EDITOR') !== -1
        isQuestionManager = roles.indexOf('QUESTION_MANAGER') !== -1
        isNewsEditor = roles.indexOf('NEWS_EDITOR') !== -1
        isUserAdmin = roles.indexOf('USER_ADMIN') !== -1
    }

    const adminLabel = roleOptions.find((el) => el.value === 'ADMIN').label
    const editorLabel = roleOptions.find((el) => el.value === 'EDITOR').label
    const questionManagerLabel = roleOptions.find(
        (el) => el.value === 'QUESTION_MANAGER',
    ).label
    const newsEditorLabel = roleOptions.find(
        (el) => el.value === 'NEWS_EDITOR',
    ).label
    const userAdminLabel = roleOptions.find(
        (el) => el.value === 'USER_ADMIN',
    ).label

    const {
        profileOptions,
        showProfileConfirmation,
        setShowProfileConfirmation,
        activeProfile,
    } = useProfileOptions({
        profileSets,
        userGuid: guid,
        userProfileSets: entity.profileSetManager,
    })

    const profileSetIds = entity.profileSetManager.map(({ id }) => id)
    const hasProfileSet = profileSetIds.includes(activeProfile?.id)
    const roleLabel = useRoleLabel(roles)

    const options = [
        [
            {
                name: t('profile.see-profile-action'),
                href: url,
                target: '_blank',
            },
            ...(!isViewer && canBan
                ? [
                      {
                          name: isBanned
                              ? t('action.unblock')
                              : t('action.block'),
                          onClick: toggleBanUsersModal,
                      },
                  ]
                : []),
            ...(!isViewer && canBan && isScheduledForBan
                ? [
                      {
                          name: t('action.unschedule-block'),
                          onClick: toggleUnscheduleUserModal,
                      },
                  ]
                : []),
            ...(!isViewer && canDelete
                ? [
                      {
                          name: t('action.delete'),
                          onClick: toggleDeleteUserModal,
                      },
                  ]
                : []),
        ],
        ...(!isViewer && !isSuperadmin
            ? [
                  [
                      {
                          name: isAdmin
                              ? t('role.remove-role', {
                                    role: adminLabel,
                                })
                              : t('role.add-role', {
                                    role: adminLabel,
                                }),
                          onClick: () =>
                              handleToggleRole('ADMIN', isAdmin, adminLabel),
                      },
                      {
                          name: isEditor
                              ? t('role.remove-role', {
                                    role: editorLabel,
                                })
                              : t('role.add-role', {
                                    role: editorLabel,
                                }),
                          onClick: () =>
                              handleToggleRole('EDITOR', isEditor, editorLabel),
                      },
                      {
                          name: isQuestionManager
                              ? t('role.remove-role', {
                                    role: questionManagerLabel,
                                })
                              : t('role.add-role', {
                                    role: questionManagerLabel,
                                }),
                          onClick: () =>
                              handleToggleRole(
                                  'QUESTION_MANAGER',
                                  isQuestionManager,
                                  questionManagerLabel,
                              ),
                      },
                      {
                          name: isNewsEditor
                              ? t('role.remove-role', {
                                    role: newsEditorLabel,
                                })
                              : t('role.add-role', {
                                    role: newsEditorLabel,
                                }),
                          onClick: () =>
                              handleToggleRole(
                                  'NEWS_EDITOR',
                                  isNewsEditor,
                                  newsEditorLabel,
                              ),
                      },
                      {
                          name: isUserAdmin
                              ? t('role.remove-role', {
                                    role: userAdminLabel,
                                })
                              : t('role.add-role', {
                                    role: userAdminLabel,
                                }),
                          onClick: () =>
                              handleToggleRole(
                                  'USER_ADMIN',
                                  isUserAdmin,
                                  userAdminLabel,
                              ),
                      },
                  ],
              ]
            : []),
        [...profileOptions],
    ]

    return (
        <tr>
            <td>
                <Wrapper>
                    <Checkbox
                        name={`file-${guid}`}
                        checked={isSelected}
                        size="small"
                        onChange={onToggle}
                        releaseInputArea
                        disabled={isViewer}
                    />
                    <Avatar
                        name={name}
                        image={icon}
                        disabled
                        role={roleLabel}
                        size="small"
                        style={{ margin: '0 8px' }}
                    />
                    <div style={{ padding: '8px 0' }}>{name}</div>
                </Wrapper>
            </td>
            {(isBanned || isScheduledForBan) && (
                <td style={{ paddingTop: '8px', paddingBottom: '8px' }}>
                    {banReason}
                </td>
            )}
            <td style={{ paddingTop: '8px', paddingBottom: '8px' }}>{email}</td>
            <td
                style={{
                    textAlign: 'right',
                    whiteSpace: 'nowrap',
                    paddingRight: 0,
                }}
            >
                <DisplayDate
                    date={
                        orderBy === 'bannedSince'
                            ? bannedSince
                            : orderBy === 'banScheduledAt'
                              ? banScheduledAt
                              : isBanned
                                ? bannedSince
                                : isScheduledForBan
                                  ? banScheduledAt
                                  : lastOnline
                    }
                />
            </td>
            <td>
                <Flexer justifyContent="flex-end">
                    <DropdownButton options={options}>
                        <IconButton
                            variant="secondary"
                            size="large"
                            tooltip={t('global.options')}
                            aria-label={t('action.show-options')}
                        >
                            <OptionsIcon />
                        </IconButton>
                    </DropdownButton>
                    <Modal
                        isVisible={showBanUsersModal}
                        title={
                            isBanned
                                ? t('admin.unblock-users', { count: 1 })
                                : t('admin.block-users', { count: 1 })
                        }
                        size="small"
                        onClose={toggleBanUsersModal}
                    >
                        <BanUsers
                            onClose={toggleBanUsersModal}
                            users={[entity]}
                            isBanned={isBanned}
                            afterSubmit={clearSelected}
                        />
                    </Modal>
                    <UnscheduleModal
                        isVisible={showUnscheduleUserModal}
                        users={[entity]}
                        onClose={toggleUnscheduleUserModal}
                        onClear={clearSelected}
                    />
                    <Modal
                        isVisible={showDeleteUserModal}
                        title={t('admin.delete-users', { count: 1 })}
                        size="small"
                        onClose={toggleDeleteUserModal}
                    >
                        <DeleteUsers
                            users={[entity]}
                            onClose={toggleDeleteUserModal}
                            afterSubmit={clearSelected}
                        />
                    </Modal>
                    <Modal
                        isVisible={showProfileConfirmation}
                        title={t(
                            hasProfileSet
                                ? `admin.remove-profile-set`
                                : `admin.add-profile-set`,
                            { profile: activeProfile?.field?.name },
                        )}
                        size="small"
                        onClose={() => setShowProfileConfirmation(false)}
                    >
                        <ToggleProfile
                            onClose={() => setShowProfileConfirmation(false)}
                            entity={entity}
                            activeProfile={activeProfile}
                            profileSets={profileSets}
                        />
                    </Modal>
                    {!!togglingRole && (
                        <Modal
                            isVisible={showToggleRoleModal}
                            title={
                                togglingRole.hasRole
                                    ? t('role.remove-role', {
                                          role: togglingRole.roleLabel,
                                      })
                                    : t('role.add-role', {
                                          role: togglingRole.roleLabel,
                                      })
                            }
                            size="small"
                            onClose={hideToggleRoleModal}
                        >
                            <ToggleRole
                                onClose={hideToggleRoleModal}
                                togglingRole={togglingRole}
                                guid={guid}
                                name={name}
                            />
                        </Modal>
                    )}
                </Flexer>
            </td>
        </tr>
    )
}

export default User
