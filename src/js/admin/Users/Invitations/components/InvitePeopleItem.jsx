import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import styled, { css } from 'styled-components'
import * as yup from 'yup'

import IconButton from 'js/components/IconButton/IconButton'

import CrossIcon from 'icons/cross-small.svg'

const Wrapper = styled.div`
    padding-left: 8px;
    display: flex;
    align-items: center;
    font-size: ${(p) => p.theme.font.size.small};
    line-height: ${(p) => p.theme.font.lineHeight.small};

    ${(p) =>
        p.$isInvalid &&
        css`
            color: ${(p) => p.theme.color.warn.main};
        `};

    &:not(:last-child) {
        border-bottom: 1px solid ${(p) => p.theme.color.grey[30]};
    }

    .InvitePeopleAddressNumber {
        flex-shrink: 0;
        margin-right: 4px;
    }
`

const InvitePeopleItem = ({ index, address, onRemove, ...rest }) => {
    const [isInvalid, setIsInvalid] = useState(false)

    useEffect(() => {
        const schema = yup.string().email()

        schema.isValid(address).then(function (valid) {
            if (!valid) setIsInvalid(true)
        })
    }, [address])

    const { t } = useTranslation()

    if (!address) return null

    const handleRemove = () => onRemove(address)

    return (
        <Wrapper $isInvalid={isInvalid} {...rest}>
            <div style={{ display: 'flex', padding: '4px 0' }}>
                <span className="InvitePeopleAddressNumber">{index + 1}.</span>
                {address}
                {isInvalid && ' (invalid)'}
            </div>
            <IconButton
                size="normal"
                variant="secondary"
                aria-label={t('admin.remove-from-send')}
                onClick={handleRemove}
                style={{
                    marginLeft: 'auto',
                    backgroundColor: 'transparent',
                }}
            >
                <CrossIcon />
            </IconButton>
        </Wrapper>
    )
}

export default InvitePeopleItem
