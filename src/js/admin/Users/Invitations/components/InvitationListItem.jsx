import React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import Button from 'js/components/Button/Button'

const InvitationListItem = ({ mutate, entity }) => {
    const {
        formState: { isSubmitting },
        handleSubmit,
    } = useForm()

    const onSubmit = async () => {
        await mutate({
            variables: {
                input: {
                    emailAddresses: [entity.email],
                },
            },
            refetchQueries: ['SiteInvitations'],
        }).catch((errors) => {
            console.error(errors)
        })
    }

    const { t } = useTranslation()

    return (
        <tr>
            <td>{entity.email}</td>
            <td>
                <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                    <Button
                        size="normal"
                        variant="tertiary"
                        type="submit"
                        onClick={handleSubmit(onSubmit)}
                        loading={isSubmitting}
                    >
                        {t('action.revoke')}
                    </Button>
                </div>
            </td>
        </tr>
    )
}

const Mutation = gql`
    mutation RevokeInviteToSite($input: revokeInviteToSiteInput!) {
        revokeInviteToSite(input: $input) {
            success
        }
    }
`

export default graphql(Mutation)(InvitationListItem)
