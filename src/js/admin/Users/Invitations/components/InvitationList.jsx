import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Table from 'js/components/Table'
import Text from 'js/components/Text/Text'

import InvitationListItem from './InvitationListItem'

const InvitationList = ({ data }) => {
    const { t } = useTranslation()

    if (data.loading) return <LoadingSpinner />

    const { edges } = data.siteSettings.siteInvites

    if (edges.length) {
        return (
            <Table>
                <tbody>
                    {edges.map((invitation, index) => (
                        <InvitationListItem
                            key={`${invitation.email}-${index}`}
                            entity={invitation}
                        />
                    ))}
                </tbody>
            </Table>
        )
    } else {
        return (
            <div style={{ margin: '0 auto', textAlign: 'center' }}>
                <Text as="h2">{t('admin.invitations-no-results')}</Text>
            </div>
        )
    }
}

const Query = gql`
    query SiteInvitations {
        siteSettings {
            siteInvites {
                edges {
                    email
                }
            }
        }
    }
`

export default graphql(Query)(InvitationList)
