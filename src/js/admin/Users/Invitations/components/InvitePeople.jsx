import React, { useRef, useState } from 'react'
import { useForm } from 'react-hook-form'
import { Trans, useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { produce } from 'immer'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import IconButton from 'js/components/IconButton/IconButton'
import Textfield from 'js/components/Textfield/Textfield'
import Tiptap from 'js/components/Tiptap/TiptapEditor'

import AddIcon from 'icons/add.svg'

import InvitePeopleItem from './InvitePeopleItem'

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    overflow: hidden;

    .EmailAddressList {
        margin-bottom: 16px;
        border: 1px solid ${(p) => p.theme.color.grey[30]};
        overflow-y: auto;
    }

    .EmailAddressAddButton {
        background-color: transparent;
        height: 100%;
        border-left: 1px solid ${(p) => p.theme.color.grey[30]};
    }
`

const InvitePeople = ({ mutate, onClose }) => {
    const refEditor = useRef()

    const [emailAddresses, setEmailAddresses] = useState(new Set())

    const handleAddEmailAddress = () => {
        const addressArray = inputValue
            .replace(/\s+/g, '')
            .split(',')
            .filter((item) => item)
        setInputValue('')
        setEmailAddresses(new Set([...emailAddresses, ...addressArray]))
    }

    const handleRemoveEmailAddress = (address) => {
        setErrors()

        setEmailAddresses(
            produce((newState) => {
                newState.delete(address)
            }),
        )
    }

    const [inputValue, setInputValue] = useState('')

    const handleChangeInput = (evt) => setInputValue(evt.target.value)

    const handleKeyPressInput = (evt) => {
        if (evt.key === 'Enter') handleAddEmailAddress()
    }

    const handleClose = () => {
        setEmailAddresses(new Set())
        setInputValue('')
        onClose()
    }

    const [errors, setErrors] = useState()

    const {
        formState: { isSubmitting },
        handleSubmit,
    } = useForm()

    const onSubmit = async () => {
        await mutate({
            variables: {
                input: {
                    emailAddresses: Array.from(emailAddresses),
                    message: refEditor.current.getValue(),
                },
            },
            refetchQueries: ['SiteInvitations'],
        })
            .then(() => {
                handleClose()
            })
            .catch((errors) => {
                setErrors(errors)
                console.error(errors)
            })
    }

    const { t } = useTranslation()

    return (
        <Wrapper>
            {emailAddresses.size > 0 && (
                <ul
                    className="EmailAddressList"
                    style={{ marginBottom: '8px' }}
                >
                    {Array.from(emailAddresses).map((address, index) => (
                        <InvitePeopleItem
                            key={index}
                            as="li"
                            index={index}
                            address={address}
                            onRemove={handleRemoveEmailAddress}
                        />
                    ))}
                </ul>
            )}

            <div style={{ margin: '8px 0 16px' }}>
                <Textfield
                    name="invitationEmail"
                    label={t('form.email-address')}
                    value={inputValue}
                    onChange={handleChangeInput}
                    onKeyPress={handleKeyPressInput}
                    ElementAfter={
                        <IconButton
                            className="EmailAddressAddButton"
                            variant={inputValue ? 'secondary' : 'tertiary'}
                            size="large"
                            radiusStyle="none"
                            disabled={!inputValue}
                            onClick={handleAddEmailAddress}
                            aria-label={t('admin.add-to-send')}
                        >
                            <AddIcon />
                        </IconButton>
                    }
                    helper={
                        <Trans i18nKey="admin.invite-people-helper">
                            Add email addresses by uploading .csv files or
                            typing them in. When typing, separate addresses with
                            a comma and press <kbd>Enter</kbd> to add.
                        </Trans>
                    }
                />
            </div>

            <Tiptap
                ref={refEditor}
                name="invitationMessage"
                placeholder={t('form.write-message')}
                options={{
                    textStyle: true,
                }}
                contentType="html"
                style={{ overflowY: 'auto' }}
            />

            <Errors errors={errors} />

            <Flexer mt>
                <Button
                    size="normal"
                    variant="tertiary"
                    type="button"
                    onClick={handleClose}
                >
                    {t('action.cancel')}
                </Button>
                <Button
                    size="normal"
                    variant="primary"
                    disabled={emailAddresses.size === 0}
                    onClick={handleSubmit(onSubmit)}
                    loading={isSubmitting}
                >
                    {emailAddresses.size === 0
                        ? t('action.send')
                        : t('admin.send-invites', {
                              count: emailAddresses.size,
                          })}
                </Button>
            </Flexer>
        </Wrapper>
    )
}

const Mutation = gql`
    mutation InviteToSite($input: inviteToSiteInput!) {
        inviteToSite(input: $input) {
            success
        }
    }
`

export default graphql(Mutation)(InvitePeople)
