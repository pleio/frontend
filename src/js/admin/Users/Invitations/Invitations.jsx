import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'

import PageTitle from 'js/admin/components/PageTitle'
import Button from 'js/components/Button/Button'
import Document from 'js/components/Document'
import { Container } from 'js/components/Grid/Grid'
import Modal from 'js/components/Modal/Modal'
import Section from 'js/components/Section/Section'

import InvitationList from './components/InvitationList'
import InvitePeople from './components/InvitePeople'

const Invitations = () => {
    const [showInvitePeopleModal, setShowInvitePeopleModal] = useState(false)
    const toggleInvitePeopleModal = () =>
        setShowInvitePeopleModal(!showInvitePeopleModal)

    const { t } = useTranslation()

    return (
        <>
            <Document
                title={t('admin.users')}
                containerTitle={t('admin.title')}
            />
            <Section divider>
                <Container size="small">
                    <PageTitle>{t('admin.invitations')}</PageTitle>

                    <div
                        style={{
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'flex-end',
                            marginBottom: '24px',
                        }}
                    >
                        <Button
                            size="normal"
                            variant="secondary"
                            onClick={toggleInvitePeopleModal}
                        >
                            {t('global.invite-people')}
                        </Button>
                    </div>

                    <InvitationList offset={0} limit={100} />

                    <Modal
                        isVisible={showInvitePeopleModal}
                        title={t('global.invite-people')}
                        size="small"
                        onClose={toggleInvitePeopleModal}
                        containHeight
                    >
                        <InvitePeople onClose={toggleInvitePeopleModal} />
                    </Modal>
                </Container>
            </Section>
        </>
    )
}

export default Invitations
