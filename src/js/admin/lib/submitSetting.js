export default function submitSetting(
    mutate,
    key,
    value,
    refetchQueries,
    resolve,
    reject,
) {
    mutate({
        variables: {
            input: {
                [key]: value,
            },
        },
        refetchQueries,
    })
        .then(() => {
            resolve && resolve()
        })
        .catch((errors) => {
            console.error(errors)
            reject && reject(errors)
        })
}
