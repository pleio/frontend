import React from 'react'
import { useTranslation } from 'react-i18next'
import { sortByProperty } from 'helpers'
import styled from 'styled-components'

import ComputerIcon from 'icons/computer.svg'
import ContentIcon from 'icons/content.svg'
import DashboardIcon from 'icons/dashboard.svg'
import FilesIcon from 'icons/files.svg'
import PagesIcon from 'icons/pages.svg'
import ProfileIcon from 'icons/profile.svg'
import PublishRequestsIcon from 'icons/published-with-changes.svg'
import SubscriptionIcon from 'icons/subscription.svg'
import ToolsIcon from 'icons/tools.svg'
import UsersIcon from 'icons/users.svg'

const Notification = styled.span`
    display: inline-flex;
    gap: 8px;

    .NotificationBullet {
        font-size: 1.6rem;
        color: ${(p) => p.theme.color.primary.main};
    }
`

const NotificationBullet = ({ show }) =>
    show ? <span className="NotificationBullet"> •</span> : null

const useMenu = (site, siteSettings, viewer) => {
    const { t } = useTranslation()

    const { datahubExternalContentEnabled } = site
    const { contentModerationEnabled, commentModerationEnabled } = siteSettings
    const {
        hasFullControl,
        hasUserManagement,
        publicationRequestManagement,
        hasContentManagement,
        accessRequestsCount,
        deleteRequestsCount,
        contentPublicationRequestsCount,
        commentPublicationRequestsCount,
        filePublicationRequestsCount,
    } = viewer

    const toolsMenu = getToolsMenu(t, datahubExternalContentEnabled)

    const showUserMenuNotification =
        accessRequestsCount + deleteRequestsCount > 0
    const showPublicationMenuNotification =
        contentPublicationRequestsCount +
            commentPublicationRequestsCount +
            filePublicationRequestsCount >
        0

    return [
        ...(hasFullControl
            ? [
                  {
                      link: '/admin/dashboard',
                      title: t('admin.dashboard'),
                      icon: <DashboardIcon />,
                  },
              ]
            : []),
        ...(hasFullControl
            ? [
                  {
                      link: '/admin/site',
                      title: t('global.site'),
                      icon: <ComputerIcon />,
                      children: [
                          {
                              link: '/admin/site/general',
                              title: t('global.general'),
                          },
                          {
                              link: '/admin/site/access',
                              title: t('global.access'),
                          },
                          {
                              link: '/admin/site/appearance',
                              title: t('global.appearance'),
                          },
                          {
                              link: '/admin/site/navigation',
                              title: t('global.navigation'),
                          },
                          {
                              link: '/admin/site/tags',
                              title: t('admin.tags'),
                          },
                          {
                              link: '/admin/site/mailing',
                              title: t('admin.mailing'),
                          },
                          {
                              link: '/admin/site/advanced',
                              title: t('global.advanced'),
                          },
                      ],
                  },
              ]
            : []),
        ...(hasContentManagement
            ? [
                  {
                      link: '/admin/pages',
                      title: t('admin.pages'),
                      icon: <PagesIcon />,
                  },
              ]
            : []),
        ...(hasFullControl
            ? [
                  {
                      link: '/admin/content',
                      title: t('global.content'),
                      icon: <ContentIcon />,
                  },
              ]
            : []),
        ...(hasFullControl
            ? [
                  {
                      link: '/admin/files',
                      title: t('global.files'),
                      icon: <FilesIcon />,
                  },
              ]
            : []),
        ...(hasUserManagement
            ? [
                  {
                      link: '/admin/users',
                      title: (
                          <Notification>
                              {t('admin.users')}
                              <NotificationBullet
                                  show={showUserMenuNotification}
                              />
                          </Notification>
                      ),
                      icon: <UsersIcon />,
                      children: [
                          {
                              link: '/admin/users/manage',
                              title: t('action.manage'),
                          },
                          {
                              link: '/admin/users/blocked',
                              title: t('admin.blocked'),
                          },
                          {
                              link: '/admin/users/invitations',
                              title: t('admin.invitations'),
                          },
                          {
                              link: '/admin/users/access-requests',
                              title: `${t('admin.access-requests')}${
                                  accessRequestsCount > 0
                                      ? ` (${accessRequestsCount})`
                                      : ''
                              }`,
                          },
                          {
                              link: '/admin/users/delete-requests',
                              title: `${t('admin.delete-requests')}${
                                  deleteRequestsCount > 0
                                      ? ` (${deleteRequestsCount})`
                                      : ''
                              }`,
                          },
                      ],
                  },
              ]
            : []),
        ...(hasFullControl
            ? [
                  {
                      link: '/admin/profile',
                      title: t('admin.profile'),
                      icon: <ProfileIcon />,
                  },
              ]
            : []),
        ...(publicationRequestManagement &&
        (contentModerationEnabled || commentModerationEnabled)
            ? [
                  {
                      link: `/admin/publish-requests/${contentModerationEnabled ? 'content' : 'comments'}`,
                      isActive: '/admin/publish-requests',
                      title: (
                          <Notification>
                              {t('content-moderation.publish-requests')}
                              <NotificationBullet
                                  show={showPublicationMenuNotification}
                              />
                          </Notification>
                      ),

                      icon: <PublishRequestsIcon />,
                      children: [
                          ...(contentModerationEnabled
                              ? [
                                    {
                                        link: '/admin/publish-requests/content',
                                        title: `${t('content-moderation.status.content')}${
                                            contentPublicationRequestsCount > 0
                                                ? ` (${contentPublicationRequestsCount})`
                                                : ''
                                        }`,
                                    },
                                ]
                              : []),
                          ...(contentModerationEnabled
                              ? [
                                    {
                                        link: '/admin/publish-requests/files',
                                        title: `${t('content-moderation.status.files')}${
                                            filePublicationRequestsCount > 0
                                                ? ` (${filePublicationRequestsCount})`
                                                : ''
                                        }`,
                                    },
                                ]
                              : []),
                          ...(commentModerationEnabled
                              ? [
                                    {
                                        link: '/admin/publish-requests/comments',
                                        title: `${t('content-moderation.status.comments')}${
                                            commentPublicationRequestsCount > 0
                                                ? ` (${commentPublicationRequestsCount})`
                                                : ''
                                        }`,
                                    },
                                ]
                              : []),
                      ],
                  },
              ]
            : []),
        ...(hasFullControl
            ? [
                  {
                      link: toolsMenu[0].link,
                      isActive: '/admin/tools',
                      title: t('admin.tools'),
                      icon: <ToolsIcon />,
                      children: toolsMenu,
                  },
              ]
            : []),
        ...(hasFullControl
            ? [
                  {
                      link: '/admin/subscription',
                      title: t('admin.subscription'),
                      icon: <SubscriptionIcon />,
                  },
              ]
            : []),
    ]
}

const getToolsMenu = (t, datahubExternalContentEnabled) => {
    const menu = [
        {
            link: '/admin/tools/agreements',
            title: t('admin.agreements'),
        },
        {
            link: '/admin/tools/export-content',
            title: t('admin.export-content'),
        },
        {
            link: '/admin/tools/flow',
            title: t('admin.flow'),
        },
        {
            link: '/admin/tools/profile-sync',
            title: t('admin.profile-sync'),
        },
        {
            link: '/admin/tools/redirects',
            title: t('admin.redirects'),
        },
        {
            link: '/admin/tools/validators',
            title: t('admin.validators'),
        },
        {
            link: '/admin/tools/broken-links',
            title: t('admin.broken-links'),
        },
        ...(datahubExternalContentEnabled
            ? [
                  {
                      link: '/admin/tools/datahub',
                      title: t('admin.datahub'),
                  },
              ]
            : []),
    ]

    menu.sort(sortByProperty('title'))

    return menu
}

export default useMenu
