import React from 'react'
import { useTranslation } from 'react-i18next'

import Table from 'js/components/Table'
import Text from 'js/components/Text/Text'

const CustomAgreements = ({ siteCustomAgreements }) => {
    const { t } = useTranslation()

    return (
        <Table wrap="word" rowHeight="small">
            <thead>
                <tr>
                    <th>{t('global.name')}</th>
                    <th>{t('admin.document')}</th>
                </tr>
            </thead>
            <tbody>
                {siteCustomAgreements.map((agreement) => {
                    return (
                        <tr key={agreement.name}>
                            <td>{agreement.name}</td>
                            <td>
                                <Text size="small">
                                    <a href={agreement.url}>
                                        {agreement.fileName}
                                    </a>
                                </Text>
                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </Table>
    )
}

export default CustomAgreements
