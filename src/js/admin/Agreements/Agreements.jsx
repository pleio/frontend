import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import PageTitle from 'js/admin/components/PageTitle'
import Document from 'js/components/Document'
import { Container } from 'js/components/Grid/Grid'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Section from 'js/components/Section/Section'
import TabMenu from 'js/components/TabMenu'
import TabPage from 'js/components/TabPage'
import Text from 'js/components/Text/Text'

import Agreement from './components/Agreement'
import CustomAgreements from './CustomAgreements'

const Agreements = ({ data }) => {
    const { t } = useTranslation()

    const [tab, setTab] = useState()

    if (data.loading) return <LoadingSpinner />

    const hasCustomAgreements = data.siteCustomAgreements.length > 0

    return (
        <>
            <Document
                title={t('admin.agreements')}
                containerTitle={t('admin.title')}
            />
            <Container size="small">
                <Section divider>
                    <PageTitle>{t('admin.agreements')}</PageTitle>

                    {hasCustomAgreements || data.siteAgreements.length > 0 ? (
                        <>
                            <TabMenu
                                label={t('admin.agreements')}
                                options={[
                                    ...(hasCustomAgreements
                                        ? [
                                              {
                                                  onClick: () =>
                                                      setTab('custom'),
                                                  isActive: tab
                                                      ? tab === 'custom'
                                                      : hasCustomAgreements,
                                                  id: 'custom-agreements-tab',
                                                  label: t(
                                                      'admin.custom-agreements',
                                                  ),
                                                  ariaControls:
                                                      'custom-agreements',
                                              },
                                          ]
                                        : []),
                                    ...data.siteAgreements.map(
                                        ({ id, name }, i) => {
                                            return {
                                                onClick: () => setTab(id),
                                                isActive: tab
                                                    ? tab === id
                                                    : hasCustomAgreements
                                                      ? false
                                                      : i === 0,
                                                id: `agreement-${id}-tab`,
                                                label: name,
                                                ariaControls: `agreement-${id}`,
                                            }
                                        },
                                    ),
                                ]}
                                showBorder
                                sticky
                            />

                            {hasCustomAgreements && (
                                <TabPage
                                    visible={
                                        tab
                                            ? tab === 'custom'
                                            : hasCustomAgreements
                                    }
                                    id="custom-agreements"
                                    ariaLabelledby="custom-agreements-tab"
                                    style={{ padding: '24px 0' }}
                                >
                                    <CustomAgreements
                                        siteCustomAgreements={
                                            data.siteCustomAgreements
                                        }
                                    />
                                </TabPage>
                            )}

                            {data.siteAgreements.map((agreement, i) => (
                                <TabPage
                                    visible={
                                        tab
                                            ? tab === agreement.id
                                            : hasCustomAgreements
                                              ? false
                                              : i === 0
                                    }
                                    key={agreement.id}
                                    id={`agreement-${agreement.id}`}
                                    ariaLabelledby={`agreement-${agreement.id}-tab`}
                                    style={{ padding: '24px 0' }}
                                >
                                    <Agreement
                                        viewer={data.viewer}
                                        entity={agreement}
                                    />
                                </TabPage>
                            ))}
                        </>
                    ) : (
                        <Text textAlign="center">
                            {t('admin.no-agreements')}
                        </Text>
                    )}
                </Section>
            </Container>
        </>
    )
}

const GET_SITE_AGREEMENTS = gql`
    query SiteAgreements {
        viewer {
            guid
            user {
                guid
                name
            }
        }
        siteAgreements {
            id
            name
            description
            accepted
            versions {
                id
                version
                document
                accepted
                acceptedBy
                acceptedDate
            }
        }
        siteCustomAgreements {
            name
            url
            fileName
        }
    }
`

export default graphql(GET_SITE_AGREEMENTS)(Agreements)
