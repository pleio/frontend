import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { Trans, useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import { H3 } from 'js/components/Heading'
import Modal from 'js/components/Modal/Modal'
import Spacer from 'js/components/Spacer/Spacer'
import Table from 'js/components/Table'
import Text from 'js/components/Text/Text'

import Version from './Version'

const Agreement = ({ viewer, entity }) => {
    const [showConfirmModal, setShowConfirmModal] = useState(false)
    const toggleConfirmModal = () => setShowConfirmModal(!showConfirmModal)

    const { t } = useTranslation()
    const { id, name, description, versions } = entity
    const latestVersion = versions[0]

    const [mutate] = useMutation(gql`
        mutation SignSiteAgreementVersion(
            $input: signSiteAgreementVersionInput!
        ) {
            signSiteAgreementVersion(input: $input) {
                siteAgreementVersion {
                    id
                    accepted
                    acceptedBy
                    acceptedDate
                }
            }
        }
    `)

    const signVersion = async () => {
        await mutate({
            variables: {
                input: {
                    id: latestVersion.id,
                    accept: true,
                },
            },
        })
            .then(() => {
                toggleConfirmModal()
            })
            .catch((error) => {
                console.error(error)
            })
    }

    const defaultValues = {
        accept: false,
    }

    const {
        control,
        handleSubmit,
        formState: { isValid, isSubmitting },
    } = useForm({ mode: 'onChange', defaultValues })

    return (
        <Spacer spacing="normal">
            {description && <div>{description}</div>}

            {versions.length > 0 && (
                <>
                    {!latestVersion.accepted && (
                        <Button
                            size="normal"
                            variant="secondary"
                            onClick={toggleConfirmModal}
                        >
                            {t('admin.sign-latest-version')}
                        </Button>
                    )}

                    <div>
                        <H3>{t('admin.versions')}</H3>
                        <Table
                            wrap="word"
                            rowHeight="small"
                            style={{ marginTop: '8px' }}
                        >
                            <thead>
                                <tr>
                                    <th>{t('admin.document')}</th>
                                    <th
                                        style={{
                                            textAlign: 'right',
                                        }}
                                    >
                                        {t('admin.signed-by')}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                {versions.map((version) => (
                                    <Version
                                        key={`${id}-${version.id}`}
                                        entity={version}
                                    />
                                ))}
                            </tbody>
                        </Table>
                    </div>
                    <Modal
                        size="small"
                        isVisible={showConfirmModal}
                        onClose={toggleConfirmModal}
                        title={name}
                    >
                        <form onSubmit={handleSubmit(signVersion)}>
                            <Text size="small">
                                <a href={latestVersion.document}>
                                    {t('admin.download-agreement', {
                                        version: latestVersion.version,
                                    })}
                                </a>
                            </Text>

                            <FormItem
                                control={control}
                                required
                                type="checkbox"
                                name="accept"
                                label={
                                    <Trans i18nKey="admin.confirm-agreement-description">
                                        <strong>
                                            {{ userName: viewer.user.name }}
                                        </strong>{' '}
                                        is authorized to sign for{' '}
                                        <strong>
                                            {{
                                                domain: window.location
                                                    .hostname,
                                            }}
                                        </strong>{' '}
                                        and agrees to{' '}
                                        <a href={latestVersion.document}>
                                            {{ version: latestVersion.version }}
                                        </a>
                                    </Trans>
                                }
                                size="small"
                                style={{ marginTop: '16px' }}
                            />
                            <Flexer mt>
                                <Button
                                    size="normal"
                                    variant="tertiary"
                                    onClick={toggleConfirmModal}
                                >
                                    {t('action.cancel')}
                                </Button>
                                <Button
                                    size="normal"
                                    variant="primary"
                                    type="submit"
                                    disabled={!isValid}
                                    loading={isSubmitting}
                                >
                                    {t('action.sign')}
                                </Button>
                            </Flexer>
                        </form>
                    </Modal>
                </>
            )}
        </Spacer>
    )
}

export default Agreement
