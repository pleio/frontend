import React from 'react'
import { useTranslation } from 'react-i18next'
import { showDateTime, showShortDate } from 'helpers/date/showDate'

import Text from 'js/components/Text/Text'
import Tooltip from 'js/components/Tooltip/Tooltip'

const Agreement = ({ entity }) => {
    const { version, document, accepted, acceptedBy, acceptedDate } = entity
    const { t } = useTranslation()

    return (
        <tr>
            <td>
                <Text size="small">
                    <a href={document}>
                        {t('admin.version')} {version}
                    </a>
                </Text>
            </td>
            <td
                style={{
                    textAlign: 'right',
                    whiteSpace: 'nowrap',
                }}
            >
                {accepted && (
                    <div>
                        {t('admin.name-at', {
                            name: acceptedBy,
                        })}{' '}
                        <Tooltip content={showDateTime(acceptedDate)}>
                            <time
                                dateTime={acceptedDate}
                                aria-label={showDateTime(acceptedDate)}
                            >
                                {showShortDate(acceptedDate)}
                            </time>
                        </Tooltip>
                    </div>
                )}
            </td>
        </tr>
    )
}

export default Agreement
