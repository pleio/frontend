import React from 'react'
import { createRoot } from 'react-dom/client'
import { ApolloProvider } from '@apollo/client'

import { AriaLiveProvider } from 'js/components/AriaLive'
import client from 'js/lib/client'
import { GlobalStateProvider } from 'js/lib/withGlobalState'
import SearchProvider from 'js/search/providers/SearchProvider'

import '../less/style.less'
import App from './App'
import insertDomElement from './lib/insertDomElement'

const container = document.querySelector('#react-root')

const render = (Component) => {
    const root = createRoot(container) // createRoot(container!) if you use TypeScript
    root.render(
        <ApolloProvider client={client}>
            <GlobalStateProvider>
                <AriaLiveProvider>
                    <SearchProvider>
                        <Component />
                    </SearchProvider>
                </AriaLiveProvider>
            </GlobalStateProvider>
        </ApolloProvider>,
    )
}

insertDomElement({
    attr: { id: 'modal-root' },
    after: container,
})

render(App)
