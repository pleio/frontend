import React, { Suspense } from 'react'
import { Outlet } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import styled from 'styled-components'

import Header from 'js/layout/Header/Header'
import { START_OF_CONTENT_ELEMENT_ID } from 'js/lib/constants'
import withGlobalState from 'js/lib/withGlobalState'
import PageRows from 'js/page/Page/PageRows'
import { rowsViewFragment } from 'js/page/Page/rowsFragment'

import AdminOverlay from './AdminOverlay'
import CookieNotification from './CookieNotification'

const ChatOverlay = React.lazy(
    () =>
        import(
            /* webpackChunkName: "chat-overlay" */ './ChatOverlay/ChatOverlay'
        ),
)

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    height: 100vh;

    .LayoutMain {
        flex: 1 1 auto;
        position: relative;
        display: flex;
        flex-direction: column;

        > * {
            flex-shrink: 0;
        }

        .Section + .Section {
            padding-top: 20px;
        }
    }
`

const Layout = ({ globalState, setGlobalState }) => {
    const { data, loading } = useQuery(GET_TOP_MENU)

    if (loading) return null

    const { site, viewer, siteSettings } = data
    const { chatEnabled, footerPageEnabled, footerRows } = site || {}

    const toggleEditMode = () => {
        setGlobalState((newState) => {
            newState.editMode = !globalState.editMode
        })
    }

    let cookieNotification
    if (window.__SETTINGS__.site.cookieConsent && viewer && !viewer.loggedIn) {
        cookieNotification = <CookieNotification />
    }

    const hasFooter = footerPageEnabled && footerRows?.length > 0

    return (
        <Wrapper>
            <Header site={site} siteSettings={siteSettings} viewer={viewer} />
            <main
                className="LayoutMain"
                id={START_OF_CONTENT_ELEMENT_ID}
                tabIndex="-1"
            >
                <AdminOverlay
                    viewer={viewer}
                    editMode={globalState.editMode}
                    toggleEditMode={toggleEditMode}
                    positionLeft={!!globalState.showChatOverlay}
                />
                {chatEnabled && viewer?.loggedIn && (
                    <Suspense>
                        <ChatOverlay />
                    </Suspense>
                )}
                {cookieNotification}
                <Outlet />
            </main>

            {hasFooter && (
                <footer>
                    <PageRows site={site} guid={site.guid} rows={footerRows} />
                </footer>
            )}
        </Wrapper>
    )
}

const GET_TOP_MENU = gql`
    query TopMenu {
        site {
            guid
            logo
            logoAlt
            icon
            iconAlt
            showIcon
            theme
            menuState
            menu {
                label
                localLabel
                link
                children {
                    label
                    localLabel
                    link
                    children {
                        label
                        localLabel
                        link
                    }
                }
            }
            blockedUserIntroMessage
            footer {
                title
                link
            }
            chatEnabled
            contentModerationEnabled
            requireContentModerationFor
            commentModerationEnabled
            requireCommentModerationFor
            footerPageEnabled
            footerRows {
                ${rowsViewFragment}
            }
        }
        viewer {
            guid
            loggedIn
            isAdmin
            isSubEditor
            isBanned
            updateAccountUrl
            showSuspendAdminPrivilegesToggle
            isSuspendAdminPrivileges
            user {
                guid
                username
                name
                icon
                isChatUsersNotificationPushEnabled
                isChatGroupNotificationPushEnabled
            }
            canAccessAdmin(permission: accessAdmin)
        }
    }
`

export default withGlobalState(Layout)
