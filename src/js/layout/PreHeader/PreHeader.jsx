import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import { Container } from 'js/components/Grid/Grid'
import HideVisually from 'js/components/HideVisually/HideVisually'

const Wrapper = styled.div`
    position: relative;
    width: 100%;
    height: 80px;
    background-color: white;
    z-index: 12;

    .PreHeaderLogo {
        display: flex;
        align-content: center;
        justify-content: center;

        a {
            display: block;
        }

        img {
            display: block;
            width: auto;
            height: 70px;
        }
    }
`

const PreHeader = ({ site }) => {
    const { t } = useTranslation()

    if (!site) return null

    return (
        <Wrapper>
            <Container>
                {site.logo && (
                    <div className="PreHeaderLogo">
                        <Link to="/">
                            <HideVisually>{t('global.home-page')}</HideVisually>
                            <img
                                src={site.logo}
                                alt={site.logoAlt}
                                aria-hidden={!site.logoAlt}
                            />
                        </Link>
                    </div>
                )}
            </Container>
        </Wrapper>
    )
}

export default PreHeader
