import React, { useContext } from 'react'
import { media } from 'helpers'
import styled, { css } from 'styled-components'

import ChatList from 'js/chat/components/ChatList'
import Messages from 'js/chat/components/Messages'
import NewThreadForm from 'js/chat/components/NewThreadForm'
import SendMessage from 'js/chat/components/SendMessage'
import Subthread from 'js/chat/components/Subthread'
import globalStateContext from 'js/lib/withGlobalState/globalStateContext'

import ChatOverlayHeader from './ChatOverlayHeader'

const Wrapper = styled.div`
    position: fixed;
    bottom: 0;
    z-index: 12;
    background: white;
    overflow: hidden;
    display: flex;
    flex-direction: column;
    box-shadow: ${(p) => p.theme.shadow.overlay};

    ${media.mobilePortrait`
        width: 100%;
        height: ${(p) => (p.$minimized ? 'auto' : '100%')};
        left: 0;
    `};

    ${media.mobileLandscapeUp`
        right: 20px;
        width: ${(p) => (p.$minimized ? '200px' : '400px')};
        height: ${(p) => (p.$minimized ? 'auto' : '560px')};
        border-radius: ${(p) =>
            `${p.theme.radius.large} ${p.theme.radius.large} 0 0`};
    `};

    .ChatOverlaySendMessage {
        order: 1;
    }

    .ChatOverlaySubthread {
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        background-color: white;
        z-index: 4;
    }

    ${(p) =>
        p.$minimized &&
        css`
            .ChatOverlayMenu,
            .ChatOverlayNewThread,
            .ChatOverlaySendMessage,
            .ChatOverlayMessages {
                // Prevent focus inside these elements
                display: none;
            }
        `};

    ${(p) =>
        p.$showMainThread &&
        css`
            .ChatOverlayMenu,
            .ChatOverlayNewThread {
                // Prevent focus inside these elements
                display: none;
            }
        `};

    ${(p) =>
        p.$showSubthread &&
        css`
            .ChatOverlayMenu,
            .ChatOverlayHeader,
            .ChatOverlayNewThread,
            .ChatOverlaySendMessage,
            .ChatOverlayMessages {
                // Prevent focus inside these elements
                display: none;
            }
        `};

    ${(p) =>
        p.$showNewThread &&
        css`
            .ChatOverlayMenu,
            .ChatOverlaySendMessage,
            .ChatOverlayMessages {
                // Prevent focus inside these elements
                display: none;
            }
        `};
`

const ChatOverlay = () => {
    const {
        globalState: {
            showChatOverlay,
            chatOverlayExpanded,
            chatNewThread,
            activeMainThread,
            subthreadGuid,
        },
        setGlobalState,
    } = useContext(globalStateContext)

    if (!showChatOverlay) return null

    const openChat = (chat) => {
        setGlobalState((newState) => {
            newState.activeMainThread = chat
            newState.chatOverlayExpanded = true
        })
    }

    return (
        <Wrapper
            $minimized={!chatOverlayExpanded}
            $showMainThread={!!activeMainThread}
            $showSubthread={!!subthreadGuid}
            $showNewThread={!!chatNewThread}
        >
            <ChatOverlayHeader className="ChatOverlayHeader" />
            <ChatList onClickItem={openChat} className="ChatOverlayMenu" />
            {chatNewThread ? (
                <NewThreadForm
                    className="ChatOverlayNewThread"
                    onComplete={(chat) =>
                        setGlobalState((newState) => {
                            newState.activeMainThread = chat
                        })
                    }
                />
            ) : activeMainThread ? (
                <>
                    <Subthread
                        className="ChatOverlaySubthread"
                        guid={subthreadGuid}
                        group={activeMainThread?.container.group}
                    />
                    <SendMessage
                        chatGuid={activeMainThread?.guid}
                        group={activeMainThread?.container.group}
                        className="ChatOverlaySendMessage"
                    />
                    <Messages
                        chatGuid={activeMainThread?.guid}
                        className="ChatOverlayMessages"
                    />
                </>
            ) : null}
        </Wrapper>
    )
}

export default ChatOverlay
