import React, { useContext } from 'react'
import { useTranslation } from 'react-i18next'
import styled, { css } from 'styled-components'

import ChatAvatar from 'js/chat/components/ChatAvatar'
import ChatContainer from 'js/chat/components/ChatContainer'
import ChatHeaderTitle from 'js/chat/components/ChatHeaderTitle'
import MenuTitleButton from 'js/chat/components/MenuTitleButton'
import getChatContainer from 'js/chat/helpers/getChatContainer'
import Flexer from 'js/components/Flexer/Flexer'
import IconButton from 'js/components/IconButton/IconButton'
import globalStateContext from 'js/lib/withGlobalState/globalStateContext'

import CrossIcon from 'icons/cross.svg'
import EditSquareSmallIcon from 'icons/edit-square-small.svg'
import MenuIcon from 'icons/menu.svg'
import MinimizeIcon from 'icons/minimize.svg'
import NewWindowIcon from 'icons/new-window.svg'

const Wrapper = styled.div`
    display: flex;
    align-items: center;
    background-color: white;
    z-index: 3; // Position on top of <MessageDivider />

    .ChatOverlayHeaderButton {
        overflow: hidden; // Needed for ellipsis
        display: flex;
        align-items: center;
        padding: 4px 8px;
        border-radius: ${(p) => p.theme.radius.normal};

        &:hover {
            background-color: ${(p) => p.theme.color.hover};

            .ChatAvatarMultipleIndicator {
                border-color: ${(p) => p.theme.color.grey[10]};
            }
        }

        &:active {
            background-color: ${(p) => p.theme.color.active};
        }
    }

    ${(p) =>
        !p.$showThread &&
        css`
            border-bottom: 1px solid ${(p) => p.theme.color.grey[30]};
        `}

    ${(p) =>
        p.$minimized
            ? css`
                  padding: 4px;

                  .ChatOverlayHeaderButton {
                      flex-grow: 1;
                  }
              `
            : css`
                  padding: 8px;
              `}
`

const ChatOverlayHeader = ({ ...rest }) => {
    const { t } = useTranslation()

    const {
        globalState: { chatNewThread, chatOverlayExpanded, activeMainThread },
        setGlobalState,
    } = useContext(globalStateContext)

    const setExpanded = (bool) => {
        setGlobalState((newState) => {
            newState.chatOverlayExpanded = bool
        })
    }

    const container = getChatContainer(activeMainThread?.container)

    // Chat overlay is minimized
    if (!chatOverlayExpanded) {
        return (
            <Wrapper $minimized {...rest}>
                <button
                    type="button"
                    className="ChatOverlayHeaderButton"
                    onClick={() => setExpanded(true)}
                >
                    {chatNewThread ? (
                        <ChatHeaderTitle>
                            {t('chat.new-message')}
                        </ChatHeaderTitle>
                    ) : container ? (
                        <>
                            <ChatAvatar
                                container={container}
                                style={{ marginRight: '10px' }}
                            />
                            <ChatHeaderTitle>{container.title}</ChatHeaderTitle>
                        </>
                    ) : (
                        <ChatHeaderTitle>{t('chat.title')}</ChatHeaderTitle>
                    )}
                </button>
                <IconButton
                    size="normal"
                    variant="secondary"
                    radiusStyle="rounded"
                    onClick={() => {
                        setGlobalState((newState) => {
                            newState.showChatOverlay = false
                            newState.activeMainThread = null
                        })
                    }}
                    tooltip={t('action.close')}
                    placement="top"
                >
                    <CrossIcon />
                </IconButton>
            </Wrapper>
        )
    } else {
        // Chat overlay is expanded
        return (
            <Wrapper
                $showThread={!!container}
                {...rest}
                $muted={!activeMainThread?.isNotificationPushEnabled}
            >
                <Flexer
                    gutter="none"
                    justifyContent="flex-start"
                    style={{ flexGrow: 1, overflow: 'hidden' }} // Needed for ellipsis
                >
                    {(chatNewThread || container) && (
                        <IconButton
                            size="normal"
                            variant="secondary"
                            radiusStyle="rounded"
                            tooltip={t('chat.open-menu')}
                            placement="top"
                            onClick={() => {
                                setGlobalState((newState) => {
                                    newState.chatNewThread = false
                                    newState.activeMainThread = null
                                })
                            }}
                        >
                            <MenuIcon />
                        </IconButton>
                    )}

                    {chatNewThread ? (
                        <ChatHeaderTitle>
                            {t('chat.new-message')}
                        </ChatHeaderTitle>
                    ) : container ? (
                        <ChatContainer
                            chat={activeMainThread}
                            buttonClass="ChatOverlayHeaderButton"
                            isOverlay
                        />
                    ) : (
                        <>
                            <MenuTitleButton
                                className="ChatOverlayHeaderButton"
                                size="small"
                            />
                            <IconButton
                                size="normal"
                                variant="primary"
                                radiusStyle="rounded"
                                onClick={() => {
                                    setGlobalState((newState) => {
                                        newState.chatNewThread = true
                                    })
                                }}
                                tooltip={t('chat.new-message')}
                                placement="top"
                            >
                                <EditSquareSmallIcon />
                            </IconButton>
                        </>
                    )}
                </Flexer>
                <Flexer divider="normal" gutter="tiny">
                    <IconButton
                        size="normal"
                        variant="secondary"
                        radiusStyle="rounded"
                        onClick={() => setExpanded(false)}
                        tooltip={t('chat.minimize')}
                        placement="top"
                    >
                        <MinimizeIcon />
                    </IconButton>
                    <IconButton
                        size="normal"
                        variant="secondary"
                        radiusStyle="rounded"
                        tooltip={t('action.open-in-new-tab')}
                        placement="top"
                        as="a"
                        href={`/chat${
                            activeMainThread ? `/${activeMainThread.guid}` : ''
                        }`}
                        target="_blank"
                        rel="noopener noreferrer"
                        onClick={() => setExpanded(false)}
                    >
                        <NewWindowIcon />
                    </IconButton>
                </Flexer>
            </Wrapper>
        )
    }
}

export default ChatOverlayHeader
