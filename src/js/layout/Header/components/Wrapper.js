import { getReadableColor } from 'helpers/getContrast'
import styled from 'styled-components'

export default styled.header`
    flex: 0 0 auto;
    position: relative;

    .HeaderSkipToContent {
        &:focus,
        &:active {
            top: 4px;
            left: 4px;
            width: auto;
            height: 40px;

            margin: 0;
            padding: 0 12px;
            background-color: ${(p) => p.theme.color.pleio};

            font-size: ${(p) => p.theme.font.size.small};
            line-height: 40px;
            font-weight: ${(p) => p.theme.font.weight.semibold};
            color: white;

            clip: auto;
            z-index: 14;

            outline-color: ${(p) =>
                getReadableColor(p.theme.color.header.main)} !important;
        }
    }
`
