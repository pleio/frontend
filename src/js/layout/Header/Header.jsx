import React, { useState } from 'react'
import { Trans, useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import HideVisually from 'js/components/HideVisually/HideVisually'
import Modal from 'js/components/Modal/Modal'
import Text from 'js/components/Text/Text'
import TiptapView from 'js/components/Tiptap/TiptapView'
import Navigation from 'js/layout/Navigation/Navigation'
import PreHeader from 'js/layout/PreHeader/PreHeader'
import { START_OF_CONTENT_ELEMENT_ID } from 'js/lib/constants'

import Wrapper from './components/Wrapper'

const Header = React.memo(({ site, siteSettings, viewer }) => {
    const { t } = useTranslation()

    const [modalVisible, setModalVisible] = useState(true)
    const hideModal = () => setModalVisible(false)

    const themeIsRijkshuisstijl = site?.theme === 'rijkshuisstijl'

    if (!site || !viewer) return null

    return (
        <Wrapper>
            <HideVisually
                as="a"
                href={`#${START_OF_CONTENT_ELEMENT_ID}`}
                className="HeaderSkipToContent"
            >
                {t('action.skip-to-content')}
            </HideVisually>

            {themeIsRijkshuisstijl && <PreHeader site={site} />}

            <Navigation
                site={site}
                siteSettings={siteSettings}
                viewer={viewer}
            />

            {viewer.isBanned && (
                <Modal
                    isVisible={modalVisible}
                    title={t('account.banned-title')}
                    size="small"
                >
                    {site.blockedUserIntroMessage ? (
                        <TiptapView content={site.blockedUserIntroMessage} />
                    ) : (
                        <Text size="small" as="p">
                            <Trans i18nKey="account.banned-description">
                                Contact the site administrator or go to
                                <a href="https://account.pleio.nl/profile/">
                                    {{ url: 'account.pleio.nl' }}
                                </a>
                                and log in with a different account.
                            </Trans>
                        </Text>
                    )}

                    <Button
                        size="normal"
                        variant="primary"
                        style={{ margin: '24px auto 0' }}
                        onClick={hideModal}
                    >
                        {t('action.ok')}
                    </Button>
                </Modal>
            )}
        </Wrapper>
    )
})

Header.displayName = 'Header'

export default Header
