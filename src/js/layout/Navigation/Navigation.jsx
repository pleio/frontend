import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { getReadableColor } from 'helpers/getContrast'
import { DesktopUp, MobileLandscapeUp, TabletDown } from 'helpers/mediaWrapper'
import styled from 'styled-components'

import { Container } from 'js/components/Grid/Grid'
import NavigationChat from 'js/components/NavigationChat/NavigationChat'
import NavigationMenu from 'js/components/NavigationMenu/NavigationMenu'
import NavigationNotifications from 'js/components/NavigationNotifications/NavigationNotifications'
import NavigationPortal from 'js/components/NavigationPortal/NavigationPortal'
import NavigationSearch from 'js/components/NavigationSearch/NavigationSearch'
import NavigationUserMenu from 'js/components/NavigationUserMenu/NavigationUserMenu'
import Sticky from 'js/components/Sticky/Sticky'
import MobileNavigation from 'js/layout/MobileNavigation/MobileNavigation'
import NavigationAction from 'js/layout/Navigation/components/NavigationAction'

import MenuIcon from 'icons/menu.svg'

const Wrapper = styled(Sticky)`
    position: relative;
    display: flex;
    align-items: center;
    background-color: ${(p) => p.theme.color.header.main};

    &[data-sticky='true'] {
        box-shadow: ${(p) => p.theme.shadow.soft.bottom};
        z-index: 11;
    }

    .NavigationContainer {
        display: flex;
        justify-content: space-between;
    }

    .MobileNavigationButton {
        background-color: transparent;
        color: ${(p) => getReadableColor(p.theme.color.header.main)};
        margin-left: -10px;
    }

    .NavigationActions {
        flex-shrink: 0;
        display: flex;
        align-items: center;
    }
`

const Navigation = ({ site, viewer }) => {
    const [mobileNavigationVisible, setMobileNavigationVisible] =
        useState(false)

    const toggleMobileNavigation = () => {
        setMobileNavigationVisible(!mobileNavigationVisible)
    }

    const { t } = useTranslation()

    if (!site) return null

    return (
        <>
            <Wrapper>
                <Container className="NavigationContainer">
                    {site.menuState === 'compact' ? (
                        <NavigationAction
                            onClick={toggleMobileNavigation}
                            aria-label={t('action.show-navigation')}
                            aria-expanded={mobileNavigationVisible}
                            aria-haspopup="true"
                            aria-controls="menu-mobile"
                            style={{ marginLeft: '-8px' }}
                            menuState={site.menuState}
                        >
                            <MenuIcon />
                        </NavigationAction>
                    ) : (
                        <>
                            <TabletDown>
                                <NavigationAction
                                    onClick={toggleMobileNavigation}
                                    aria-label={t('action.show-navigation')}
                                    aria-expanded={mobileNavigationVisible}
                                    aria-haspopup="true"
                                    aria-controls="menu-mobile"
                                    style={{ marginLeft: '-8px' }}
                                >
                                    <MenuIcon />
                                </NavigationAction>
                            </TabletDown>

                            <DesktopUp>
                                <NavigationMenu site={site} />
                            </DesktopUp>
                        </>
                    )}
                    <div className="NavigationActions">
                        <MobileLandscapeUp>
                            <NavigationSearch />
                        </MobileLandscapeUp>
                        {viewer?.loggedIn ? (
                            <>
                                {site?.chatEnabled && (
                                    <NavigationChat user={viewer?.user} />
                                )}
                                <NavigationNotifications viewer={viewer} />
                                <NavigationUserMenu
                                    site={site}
                                    viewer={viewer}
                                />
                            </>
                        ) : (
                            <NavigationPortal />
                        )}
                    </div>
                </Container>
            </Wrapper>
            {site.menuState === 'compact' ? (
                <MobileNavigation
                    site={site}
                    showMobileNavigation={mobileNavigationVisible}
                    toggleMobileNavigation={toggleMobileNavigation}
                />
            ) : (
                <TabletDown>
                    <MobileNavigation
                        site={site}
                        showMobileNavigation={mobileNavigationVisible}
                        toggleMobileNavigation={toggleMobileNavigation}
                    />
                </TabletDown>
            )}
        </>
    )
}

export default Navigation
