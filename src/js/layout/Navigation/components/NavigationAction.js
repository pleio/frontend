import { getReadableColor } from 'helpers/getContrast'
import styled from 'styled-components'

export default styled.button`
    width: ${(p) => p.theme.headerBarHeight}px;
    height: ${(p) => p.theme.headerBarHeight}px;

    outline-color: ${(p) =>
        getReadableColor(p.theme.color.header.main)} !important;
    display: flex;
    align-items: center;
    justify-content: center;

    position: relative;
    background-color: transparent;
    color: ${(p) => getReadableColor(p.theme.color.header.main)};

    &:before {
        content: '';
        position: absolute;
        width: 40px;
        height: 40px;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        margin: auto;
        border-radius: 50%;
    }

    &:hover:before {
        background-color: ${(p) => p.theme.color.header.hover};
    }

    &:active:before {
        background-color: ${(p) => p.theme.color.header.active};
    }

    > * {
        position: relative;
    }

    svg #bg {
        fill: none; // Remove possible background color from icon
    }
`
