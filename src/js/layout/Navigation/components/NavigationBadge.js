import styled, { css } from 'styled-components'

import {
    getReadableColor,
    nonTextContrastIsAA,
} from 'js/lib/helpers/getContrast'

export default styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    min-width: 18px;
    height: 18px;
    border-radius: 9px;
    border: 2px solid white;
    border-color: ${(p) => p.theme.color.header.main};
    padding-left: 3px;
    padding-right: 3px;
    font-size: ${(p) => p.theme.font.size.tiny};
    line-height: ${(p) => p.theme.font.lineHeight.tiny};
    font-weight: ${(p) => p.theme.font.weight.bold};
    white-space: nowrap;
    position: absolute;
    top: 8px;
    left: 26px;

    ${(p) =>
        nonTextContrastIsAA(p.theme.color.warn.main, p.theme.color.header.main)
            ? css`
                  background-color: ${(p) => p.theme.color.warn.main};
                  color: ${(p) => getReadableColor(p.theme.color.warn.main)};
              `
            : css`
                  background-color: ${(p) =>
                      getReadableColor(p.theme.color.header.main)};
                  color: ${(p) =>
                      getReadableColor(
                          getReadableColor(p.theme.color.header.main),
                      )};
              `};
`
