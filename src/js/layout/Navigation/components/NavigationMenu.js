import { media } from 'helpers'
import styled from 'styled-components'

export default styled.div`
    width: 100vw;
    max-width: 100%;
    display: flex;
    flex-direction: column;

    ${media.mobilePortrait`
        max-height: ${(p) =>
            p.$maxHeight
                ? `calc(${p.$maxHeight}px - 53px)`
                : 'calc(100vh - 53px)'};
    `};

    ${media.mobileLandscapeUp`
        width: 440px;
        max-height: 560px;
    `};

    .NavigationMenuHeader {
        flex-shrink: 0;
        position: relative;
        display: flex;
        align-items: center;
        justify-content: space-between;
        padding: 16px 24px;
    }

    .NavigationMenuEmpty {
        padding: 0 24px 16px;
        margin-top: -8px;
    }

    .NavigationMenuFooter {
        flex-shrink: 0;
        display: flex;
        align-items: center;
        justify-content: center;
        height: 40px;
        padding-bottom: 1px;
        border-top: 1px solid ${(p) => p.theme.color.grey[30]};
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        color: ${(p) => p.theme.color.primary.main};

        &:hover {
            text-decoration: underline;
        }
    }
`
