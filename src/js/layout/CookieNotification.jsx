import React, { useState } from 'react'
import { Trans, useTranslation } from 'react-i18next'
import styled from 'styled-components'

import Flexer from 'js/components/Flexer/Flexer'
import { Container } from 'js/components/Grid/Grid'
import IconButton from 'js/components/IconButton/IconButton'
import Text from 'js/components/Text/Text'
import { createCookie, readCookie } from 'js/lib/cookies'

import CloseIcon from 'icons/cross-large.svg'

const Wrapper = styled.div`
    width: 100%;
    padding: 10px 0 12px;
    background-color: white;
    box-shadow: ${(p) => p.theme.shadow.soft.top};
    z-index: 1;
`

const CookieNotification = () => {
    const [isHidden, setIsHidden] = useState(readCookie('cookieConsent'))

    const hideNotification = () => {
        createCookie('cookieConsent', true, 365)
        setIsHidden(true)
    }

    const { t } = useTranslation()

    if (isHidden) return null

    return (
        <Wrapper>
            <Container size="large">
                <Flexer>
                    <Text size="small">
                        <Trans i18nKey="global.cookie-notification">
                            Cookieverklaring
                            <a
                                href="https://support.pleio.nl/page/view/b416f2af-2009-481e-b3c2-c269e7fddd85/info-contact/22738c9c-d591-40e2-a5a1-0a7d930db0fa"
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                link
                            </a>
                        </Trans>
                    </Text>
                    <IconButton
                        size="large"
                        variant="secondary"
                        onClick={hideNotification}
                        aria-label={t('global.cookie-notification-close')}
                    >
                        <CloseIcon />
                    </IconButton>
                </Flexer>
            </Container>
        </Wrapper>
    )
}

export default CookieNotification
