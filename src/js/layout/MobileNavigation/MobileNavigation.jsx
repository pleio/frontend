import React from 'react'
import { FocusOn } from 'react-focus-on'
import { useTranslation } from 'react-i18next'
import { AnimatePresence } from 'framer-motion'
import { MobilePortrait } from 'helpers/mediaWrapper'

import HeaderBar from 'js/components/HeaderBar/HeaderBar'
import Menu from 'js/components/MobileNavigationMenu/MobileNavigationMenu'
import NavigationAction from 'js/layout/Navigation/components/NavigationAction'

import MenuOpenIcon from 'icons/menu-open.svg'

import Backdrop from './components/Backdrop'
import Wrapper from './components/Wrapper'
import MobileNavigationSearch from './MobileNavigationSearch'

const MobileNavigation = ({
    site,
    showMobileNavigation,
    toggleMobileNavigation,
}) => {
    const { t } = useTranslation()
    return (
        <AnimatePresence>
            {showMobileNavigation && (
                <FocusOn onEscapeKey={toggleMobileNavigation}>
                    <Wrapper
                        initial="hide"
                        animate="show"
                        exit="hide"
                        variants={{
                            hide: {
                                x: '-100%',
                                transition: {
                                    type: 'spring',
                                    stiffness: 100,
                                    mass: 0.1,
                                },
                            },
                            show: {
                                x: '0%',
                                transition: {
                                    type: 'spring',
                                    stiffness: 75,
                                    mass: 0.2,
                                },
                            },
                        }}
                    >
                        <div className="MobileNavDrawer">
                            <HeaderBar className="MobileNavHeader">
                                <NavigationAction
                                    onClick={toggleMobileNavigation}
                                    aria-label={t('action.hide-navigation')}
                                    aria-expanded={showMobileNavigation}
                                    aria-haspopup="true"
                                    aria-controls="menu-mobile"
                                    style={
                                        site?.menuState === 'compact'
                                            ? {
                                                  marginLeft:
                                                      'calc(100% - 220px)',
                                              }
                                            : { marginLeft: '-8px' }
                                    }
                                >
                                    <MenuOpenIcon />
                                </NavigationAction>
                            </HeaderBar>
                            <div className="MobileNavContent" id="menu-mobile">
                                <MobilePortrait>
                                    <MobileNavigationSearch
                                        hideSidebar={toggleMobileNavigation}
                                    />
                                </MobilePortrait>
                                <Menu
                                    site={site}
                                    toggleMobileNavigation={
                                        toggleMobileNavigation
                                    }
                                />
                            </div>
                        </div>
                    </Wrapper>
                    <Backdrop
                        initial="hide"
                        animate="show"
                        exit="hide"
                        variants={{
                            hide: {
                                opacity: 0,
                                transition: {
                                    duration: 0.15,
                                },
                            },
                            show: {
                                opacity: 1,
                                transition: {
                                    duration: 0.35,
                                },
                            },
                        }}
                        onClick={toggleMobileNavigation}
                    />
                </FocusOn>
            )}
        </AnimatePresence>
    )
}

export default MobileNavigation
