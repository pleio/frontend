import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import { getSearchLink } from 'helpers'
import styled from 'styled-components'

import HideVisually from 'js/components/HideVisually/HideVisually'
import IconButton from 'js/components/IconButton/IconButton'

import SearchIcon from 'icons/search.svg'

const Wrapper = styled.form`
    display: flex;
    border-bottom: 1px solid ${(p) => p.theme.color.grey[30]};

    .Input {
        background-color: transparent;
        border: none;
        width: 100%;
        height: ${(p) => p.theme.headerBarHeight}px;
        padding: 0 0 0 20px;
    }

    .SubmitButton {
        background-color: transparent;
        width: ${(p) => p.theme.headerBarHeight}px;
        height: ${(p) => p.theme.headerBarHeight}px;
    }
`

const MobileNavigationSearch = ({ hideSidebar }) => {
    const navigate = useNavigate()
    const { t } = useTranslation()

    const [value, setValue] = useState('')

    const onChange = (evt) => {
        setValue(evt.target.value)
    }

    const onSubmit = (evt) => {
        evt.preventDefault()
        navigate(
            getSearchLink({
                q: value.trim(),
            }),
        )
        hideSidebar()
    }

    return (
        <Wrapper onSubmit={onSubmit}>
            <HideVisually as="label" htmlFor="mobile-nav-search">
                {t('global.search')}
            </HideVisually>
            <input
                id="mobile-nav-search"
                className="Input"
                placeholder={t('global.search')}
                value={value}
                onChange={onChange}
            />
            <IconButton
                variant="secondary"
                radiusStyle="none"
                className="SubmitButton"
                type="submit"
                disabled={value === ''}
                aria-label={t('global.search')}
            >
                <SearchIcon />
            </IconButton>
        </Wrapper>
    )
}

export default MobileNavigationSearch
