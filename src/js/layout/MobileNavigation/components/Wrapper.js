import { motion } from 'framer-motion'
import styled from 'styled-components'

export default styled(motion.div)`
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    width: calc(50% - 260px);
    min-width: 250px;
    max-width: 100%;
    z-index: 13;

    .MobileNavHeader {
        flex-shrink: 0;
        display: flex;
        align-items: center;
        height: ${(p) => p.theme.headerBarHeight}px;
        padding: 0 20px;
        transition: box-shadow ${(p) => p.theme.transition.normal};
    }

    .MobileNavDrawer {
        display: flex;
        flex-direction: column;
        height: 100%;
        background-color: white;
        box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.04);
    }

    .MobileNavContent {
        flex-grow: 1;
        overflow-y: auto;
        display: flex;
        flex-direction: column;

        > * {
            flex-shrink: 0;
            margin-left: calc(100% - 250px);
        }
    }
`
