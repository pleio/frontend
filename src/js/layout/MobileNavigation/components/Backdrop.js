import { motion } from 'framer-motion'
import styled from 'styled-components'

export default styled(motion.div)`
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background-color: rgba(0, 0, 0, 0.15);
    cursor: pointer;
    z-index: 12;
`
