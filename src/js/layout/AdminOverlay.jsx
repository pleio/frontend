import React, { useContext, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { gql, useMutation } from '@apollo/client'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

import IconButton from 'js/components/IconButton/IconButton'
import globalStateContext from 'js/lib/withGlobalState/globalStateContext'

import EditIcon from 'icons/edit.svg'
import CrossIcon from 'icons/edit-cross.svg'
import UserIcon from 'icons/face.svg'
import UserCrossIcon from 'icons/face-cross.svg'
import SettingsIcon from 'icons/settings.svg'

const Wrapper = styled.div`
    position: fixed;
    bottom: 20px;
    z-index: 12;

    ${(p) =>
        p.$positionLeft
            ? css`
                  left: 20px;
              `
            : css`
                  right: 20px;
              `}

    &.active, &:hover, &:focus, &:focus-within {
        .AdminEditModeButton {
            margin-top: 88px; // Expand hover area
        }
        .AdminSettingsButton {
            opacity: 1;
            transform: translateY(-72px);
        }
        .ToggleUserButton {
            opacity: 1;
            transform: translateY(-144px);
        }
    }

    .AdminEditModeButton {
        z-index: 1;
    }

    .AdminSettingsButton,
    .ToggleUserButton {
        position: absolute;
        bottom: 0;
        right: 0;
        opacity: 0;
        transition:
            opacity ${(p) => p.theme.transition.normal},
            transform ${(p) => p.theme.transition.materialNormal};

        &.active {
            opacity: 1;
            transform: translateY(0px) !important;
        }
    }

    // Settings for keyboard navigation
    .AdminSettingsButton:focus {
        transform: translateY(-72px);
    }
    .ToggleUserButton:focus {
        transform: translateY(-144px);
    }
`

const AdminOverlay = ({ viewer, editMode, toggleEditMode, positionLeft }) => {
    const { globalState } = useContext(globalStateContext)

    const { t } = useTranslation()

    const [
        toggleAdminMode,
        { loading: toggleAdminModeLoading, error: toggleAdminModeError },
    ] = useMutation(TOGGLE_ADMIN_MODE)

    toggleAdminModeError && console.error(toggleAdminModeError)

    const platformIsMac = window.navigator.platform.match('Mac')
    const shortKey = platformIsMac ? '⌘ + e' : 'Ctrl + e'

    const handleKeyDown = (evt) => {
        if ((platformIsMac ? evt.metaKey : evt.ctrlKey) && evt.key === 'e') {
            evt.preventDefault()
            toggleEditMode()
        }
    }

    useEffect(() => {
        window.addEventListener('keydown', handleKeyDown)

        return () => window.removeEventListener('keydown', handleKeyDown)
    })

    if (
        !viewer?.isSubEditor &&
        !globalState?.canEditPage &&
        !viewer?.isSuspendAdminPrivileges
    )
        return null

    return (
        <Wrapper
            $positionLeft={positionLeft}
            className={toggleAdminModeLoading ? 'active' : ''}
        >
            {!viewer.isSuspendAdminPrivileges && (
                <IconButton
                    className="AdminEditModeButton"
                    variant={editMode ? 'secondary' : 'pleio'}
                    size="huge"
                    dropShadow
                    onClick={toggleEditMode}
                    tooltip={
                        editMode
                            ? t('admin.edit-mode-off', {
                                  shortKey,
                              })
                            : t('admin.edit-mode-on', {
                                  shortKey,
                              })
                    }
                    placement="left"
                >
                    {editMode ? <CrossIcon /> : <EditIcon />}
                </IconButton>
            )}

            {viewer.canAccessAdmin && (
                <IconButton
                    className="AdminSettingsButton"
                    as={Link}
                    to="/admin"
                    variant="pleio"
                    size="huge"
                    tooltip={t('admin.goto-admin')}
                    placement="left"
                    dropShadow
                >
                    <SettingsIcon />
                </IconButton>
            )}

            {viewer.showSuspendAdminPrivilegesToggle && (
                <IconButton
                    className={`ToggleUserButton ${
                        viewer.isSuspendAdminPrivileges ? 'active' : ''
                    }`}
                    onClick={() => {
                        toggleAdminMode()
                        window.location.reload()
                    }}
                    variant={
                        viewer.isSuspendAdminPrivileges ? 'secondary' : 'pleio'
                    }
                    size="huge"
                    tooltip={
                        viewer.isSuspendAdminPrivileges
                            ? t('admin.disable-user-mode')
                            : t('admin.user-mode')
                    }
                    placement="left"
                    dropShadow
                    loading={toggleAdminModeLoading}
                >
                    {viewer.isSuspendAdminPrivileges ? (
                        <UserCrossIcon />
                    ) : (
                        <UserIcon />
                    )}
                </IconButton>
            )}
        </Wrapper>
    )
}

AdminOverlay.propTypes = {
    viewer: PropTypes.object.isRequired,
    editMode: PropTypes.bool.isRequired,
    toggleEditMode: PropTypes.func.isRequired,
    positionLeft: PropTypes.bool,
}

export default AdminOverlay

const TOGGLE_ADMIN_MODE = gql`
    mutation ToggleSuspendAdminPrivileges {
        toggleSuspendAdminPrivileges {
            guid
            showSuspendAdminPrivilegesToggle
            isSuspendAdminPrivileges
        }
    }
`
