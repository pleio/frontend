import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import Button from 'js/components/Button/Button'
import Card, { CardContent } from 'js/components/Card/Card'
import Document from 'js/components/Document'
import Flexer from 'js/components/Flexer/Flexer'
import { Container } from 'js/components/Grid/Grid'
import { H1 } from 'js/components/Heading'
import NotFound from 'js/core/NotFound'
import PageList from 'js/page/PageList'

import AddIcon from 'icons/add.svg'

const Pages = () => {
    const { t } = useTranslation()

    const { groupGuid } = useParams()

    const { data, loading } = useQuery(GET_GROUP_PAGES, {
        variables: {
            guid: groupGuid,
        },
    })

    if (loading) return null
    if (data?.canEdit) return <NotFound />

    const { entity } = data

    const title = t('admin.pages')

    return (
        <Container>
            <Document title={title} containerTitle={entity?.name} />
            <Card>
                <CardContent>
                    <Flexer
                        justifyContent="space-between"
                        style={{ marginBottom: '24px' }}
                    >
                        <H1>{title}</H1>
                        <Button
                            as={Link}
                            to="add"
                            state={{ prevPathname: location.pathname }}
                            size="normal"
                            variant="secondary"
                        >
                            <AddIcon style={{ marginRight: '6px' }} />
                            {t('entity-cms.create')}
                        </Button>
                    </Flexer>
                    <PageList />
                </CardContent>
            </Card>
        </Container>
    )
}

const GET_GROUP_PAGES = gql`
    query GroupPages($guid: String!) {
        entity(guid: $guid) {
            guid
            ... on Group {
                name
                url
                canEdit
            }
        }
    }
`

export default Pages
