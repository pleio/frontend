import { iconViewFragment } from 'js/lib/fragments/icon'

const groupListFragment = `
    guid
    name
    description
    richDescription
    canEdit
    excerpt
    isMembershipOnRequest
    isClosed
    isFeatured
    membership
    memberCount
    ${iconViewFragment}
    url
`

export default groupListFragment
