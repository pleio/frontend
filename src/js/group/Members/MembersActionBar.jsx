import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'
import styled from 'styled-components'

import IconButton from 'js/components/IconButton/IconButton'
import ConfirmationModal from 'js/components/Modal/ConfirmationModal'
import Toolbar from 'js/components/Toolbar/Toolbar'

import DeleteIcon from 'icons/delete.svg'

const ActionBar = styled(Toolbar)`
    justify-content: space-between;
`

const MembersActionBar = ({ reset, groupGuid, selectedMembers }) => {
    const { t } = useTranslation()
    const [showConfirmationModal, setShowConfirmationModal] = useState(false)

    const [removeMembersFromGroup, { loading }] = useMutation(
        REMOVE_MEMBERS_FROM_GROUP,
        { refetchQueries: ['GroupContainer', 'GroupMembers'] },
    )

    const handleRemove = async () => {
        await removeMembersFromGroup({
            variables: {
                input: {
                    guid: groupGuid,
                    userGuids: selectedMembers,
                },
            },
        }).catch((e) => {
            console.error(e)
        })

        setShowConfirmationModal(false)
        reset()
    }

    return (
        <>
            <ActionBar count={selectedMembers.length} onClear={reset}>
                <IconButton
                    onClick={() => setShowConfirmationModal(true)}
                    size="large"
                    tooltip={t('action.remove')}
                    variant="primary"
                >
                    <DeleteIcon />
                </IconButton>
            </ActionBar>

            <ConfirmationModal
                isVisible={showConfirmationModal}
                onCancel={() => setShowConfirmationModal(false)}
                onConfirm={handleRemove}
                title={t('entity-group.remove-from-group')}
                loading={loading}
            >
                {t('entity-group.remove-members-confirm', {
                    count: selectedMembers.length,
                })}
            </ConfirmationModal>
        </>
    )
}

const REMOVE_MEMBERS_FROM_GROUP = gql`
    mutation RemoveGroupMembers($input: removeGroupMembersInput!) {
        removeGroupMembers(input: $input) {
            group {
                guid
            }
        }
    }
`

export default MembersActionBar
