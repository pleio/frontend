import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'
import { useRoleLabel } from 'helpers'

import Checkbox from 'js/components/Checkbox/Checkbox'
import DisplayDate from 'js/components/DisplayDate'
import DropdownButton from 'js/components/DropdownButton/DropdownButton'
import IconButton from 'js/components/IconButton/IconButton'
import ConfirmationModal from 'js/components/Modal/ConfirmationModal'
import UserLink from 'js/components/UserLink'
import { showShortDate } from 'js/lib/helpers/date/showDate'

import OptionsIcon from 'icons/options.svg'

const Member = ({
    entity,
    group,
    checked,
    onSelectMember,
    onDeselectMember,
}) => {
    const { t } = useTranslation()

    const { guid, canEdit, canChangeOwnership } = group
    const { roles, isOwner, user } = entity

    const refetchQueries = ['GroupContainer', 'GroupMembers']

    const [showRemoveMemberModal, setShowRemoveMemberModal] = useState(false)
    const [removeMember, { loading: removeMemberLoading }] = useMutation(
        REMOVE_MEMBERS_FROM_GROUP,
        {
            refetchQueries,
        },
    )
    const handleRemoveMember = () => {
        removeMember({
            variables: {
                input: {
                    guid,
                    userGuids: [entity.user.guid],
                },
            },
        })
            .catch((e) => {
                console.error(e)
            })
            .then(() => {
                onDeselectMember()
                setShowRemoveMemberModal(false)
            })
    }

    const [showMakeOwnerModal, setShowMakeOwnerModal] = useState(false)
    const [makeOwner, { loading: makeOwnerLoading }] = useMutation(
        CHANGE_GROUP_OWNER,
        {
            refetchQueries,
        },
    )
    const handleMakeOwner = () => {
        makeOwner({
            variables: {
                input: {
                    guid,
                    userGuid: entity.user.guid,
                },
            },
        })
            .catch((errors) => {
                console.error(errors)
            })
            .then(() => {
                setShowMakeOwnerModal(false)
            })
    }

    const [toggleMemberRole, { loading: toggleMemberRoleLoading }] =
        useMutation(TOGGLE_GROUP_MEMBER_ROLE, {
            refetchQueries,
        })
    const handleToggleRole = (role) => {
        toggleMemberRole({
            variables: {
                input: {
                    guid,
                    userGuid: entity.user.guid,
                    role,
                },
            },
        }).catch((errors) => {
            console.error(errors)
        })
    }

    const isRole = (role) => roles.indexOf(role) !== -1
    const avatarRole = useRoleLabel(roles, isOwner)

    return (
        <tr>
            {canEdit && (
                <td style={{ position: 'relative', paddingRight: 0 }}>
                    <Checkbox
                        name={user.guid}
                        defaultChecked={checked}
                        onChange={({ target }) =>
                            target.checked
                                ? onSelectMember()
                                : onDeselectMember()
                        }
                        releaseInputArea
                        size="small"
                        disabled={isOwner}
                    />
                </td>
            )}
            <td>
                <UserLink
                    entity={user}
                    role={avatarRole}
                    avatarSize="small"
                    style={{ height: '100%' }}
                />
            </td>

            {user.fieldsInOverview.map(({ key, value, fieldType }) => {
                return (
                    <td key={key}>
                        {value && fieldType === 'dateField'
                            ? showShortDate(value)
                            : value}
                    </td>
                )
            })}

            {canEdit && (
                <td style={{ textAlign: 'right' }}>
                    <DisplayDate date={entity.memberSince} />
                </td>
            )}

            {canEdit && (
                <td>
                    {!isOwner && (
                        <>
                            <DropdownButton
                                options={[
                                    [
                                        {
                                            name: t(
                                                'entity-group.remove-from-group',
                                            ),
                                            onClick: () =>
                                                setShowRemoveMemberModal(true),
                                        },
                                    ],
                                    [
                                        ...(canChangeOwnership
                                            ? [
                                                  {
                                                      name: t(
                                                          'entity-group.make-group-owner',
                                                      ),
                                                      onClick: () =>
                                                          setShowMakeOwnerModal(
                                                              true,
                                                          ),
                                                  },
                                              ]
                                            : []),
                                        {
                                            name: isRole('admin')
                                                ? t('role.remove-role', {
                                                      role: t('role.admin'),
                                                  })
                                                : t('role.add-role', {
                                                      role: t('role.admin'),
                                                  }),
                                            onClick: () =>
                                                handleToggleRole('admin'),
                                        },
                                        {
                                            name: isRole('newsEditor')
                                                ? t('role.remove-role', {
                                                      role: t(
                                                          'role.news-editor',
                                                      ),
                                                  })
                                                : t('role.add-role', {
                                                      role: t(
                                                          'role.news-editor',
                                                      ),
                                                  }),
                                            onClick: () =>
                                                handleToggleRole('newsEditor'),
                                        },
                                    ],
                                ]}
                            >
                                <IconButton
                                    variant="secondary"
                                    size="large"
                                    tooltip={t('global.options')}
                                    aria-label={t('action.show-options')}
                                    loading={
                                        makeOwnerLoading ||
                                        removeMemberLoading ||
                                        toggleMemberRoleLoading
                                    }
                                >
                                    <OptionsIcon />
                                </IconButton>
                            </DropdownButton>

                            <ConfirmationModal
                                title={t('entity-group.remove-from-group')}
                                isVisible={showRemoveMemberModal}
                                onCancel={() => setShowRemoveMemberModal(false)}
                                onConfirm={handleRemoveMember}
                                loading={removeMemberLoading}
                            >
                                {t('entity-group.remove-members-confirm', {
                                    count: 1,
                                })}
                            </ConfirmationModal>

                            <ConfirmationModal
                                title={t('entity-group.make-group-owner')}
                                isVisible={showMakeOwnerModal}
                                onCancel={() => setShowMakeOwnerModal(false)}
                                onConfirm={handleMakeOwner}
                                loading={makeOwnerLoading}
                            >
                                {t('entity-group.make-group-owner-confirm')}
                            </ConfirmationModal>
                        </>
                    )}
                </td>
            )}
        </tr>
    )
}

const TOGGLE_GROUP_MEMBER_ROLE = gql`
    mutation toggleMemberRole($input: toggleGroupMemberRoleInput!) {
        toggleGroupMemberRole(input: $input) {
            group {
                ... on Group {
                    guid
                    membership
                }
            }
        }
    }
`

const CHANGE_GROUP_OWNER = gql`
    mutation makeOwner($input: changeGroupOwnerInput!) {
        changeGroupOwner(input: $input) {
            group {
                ... on Group {
                    guid
                    membership
                }
            }
        }
    }
`

const REMOVE_MEMBERS_FROM_GROUP = gql`
    mutation removeMember($input: removeGroupMembersInput!) {
        removeGroupMembers(input: $input) {
            group {
                guid
            }
        }
    }
`

export default Member
