import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { media, useLocalStorage } from 'helpers'
import styled from 'styled-components'

import AnimatePresence from 'js/components/AnimatePresence/AnimatePresence'
import Card from 'js/components/Card/Card'
import Document from 'js/components/Document'
import FetchMore from 'js/components/FetchMore'
import FilterWrapper from 'js/components/FilterWrapper'
import { Container } from 'js/components/Grid/Grid'
import HideVisually from 'js/components/HideVisually/HideVisually'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import SearchBar from 'js/components/SearchBar/SearchBar'
import Select from 'js/components/Select/Select'
import Table from 'js/components/Table'
import TableSortableHeader from 'js/components/TableSortableHeader/TableSortableHeader'
import Filters from 'js/components/UserList/Filters'

import Member from './Member'
import MembersActionBar from './MembersActionBar'

const Wrapper = styled(Container)`
    z-index: 1;

    ${media.mobileLandscapeDown`
        margin-top: -24px;
        padding: 0;
        flex-grow: 1;

        ${Card} {
            border-radius: 0;
            box-shadow: none;
            border-top: 1px solid ${(p) => p.theme.color.grey[20]};
        }
    `};

    ${media.tabletUp`
        margin-bottom: 40px;
    `};

    ${Card} {
        min-height: 100%;
        padding: 8px 0 16px;
    }
`

const membersReducer = (members, action) => {
    const { type, memberGuid } = action

    let newMembers = []

    switch (type) {
        case 'ADD_MEMBER':
            newMembers = [...members, memberGuid]
            break
        case 'REMOVE_MEMBER':
            newMembers = members.filter((guid) => guid !== memberGuid)
            break
        case 'RESET':
            newMembers = []
            break
    }

    return newMembers
}

const useGroupRoleOptions = () => {
    const { t } = useTranslation()

    return [
        {
            label: t('role.admin'),
            value: 'admin',
        },
        {
            label: t('role.owner'),
            value: 'owner',
        },
        {
            label: t('role.news-editor'),
            value: 'newsEditor',
        },
    ]
}

const Members = () => {
    const { t } = useTranslation()
    const { groupGuid } = useParams()
    const [selectedMembers, dispatchMember] = React.useReducer(
        membersReducer,
        [],
    )

    const [isFilterPanelVisible, setFilterPanelVisible] = useState(true)
    const toggleFilterArea = () => setFilterPanelVisible(!isFilterPanelVisible)

    const [filterValue, setFilterValue] = useState({})
    const handleUpdateFilter = (selected, name) => {
        const newfilters = { ...filterValue }
        if (Array.isArray(selected) ? selected.length > 0 : !!selected) {
            newfilters[name] = selected
        } else {
            delete newfilters[name]
        }
        setQ(searchInput)
        setFilterValue(newfilters)
    }

    const [searchInput, setSearchInput] = useState('')
    const onChangeSearchInput = (evt) => {
        setSearchInput(evt.target.value)
    }

    const [q, setQ] = useState('')
    const onSubmitSearchInput = (evt) => {
        evt.preventDefault()
        setQ(searchInput)
    }

    const [orderBy, setOrderBy] = useLocalStorage(
        'group-members-order-by',
        'name',
    )
    const [orderDirection, setOrderDirection] = useLocalStorage(
        'group-members-order-direction',
        'asc',
    )

    const groupRoles = useGroupRoleOptions()
    const [roleFilter, setRoleFilter] = useLocalStorage(
        'group-members-role-filter',
        null,
    )

    const [queryLimit, setQueryLimit] = useState(50)
    const { loading, data, fetchMore } = useQuery(GET_GROUP_MEMBERS, {
        variables: {
            groupGuid,
            q,
            offset: 0,
            limit: queryLimit,
            filters: Object.entries(filterValue).map(([key, value]) => ({
                name: key,
                values: value,
            })),
            roleFilter,
            orderBy,
            orderDirection,
        },
    })

    const { entity: group, filters, members } = data || {}

    const availableFilters = filters?.users || []
    const hasAvailableFilters = availableFilters.length > 0
    const fieldsInOverview = members?.fieldsInOverview || []

    if (!groupGuid) return null

    return (
        <>
            <Document
                title={t('entity-group.members')}
                containerTitle={group?.name}
            />
            <Wrapper>
                <Card style={{ padding: '24px 0 16px' }}>
                    <Container>
                        <form method="GET" onSubmit={onSubmitSearchInput}>
                            <SearchBar
                                name="user-list-search"
                                value={searchInput}
                                label={t('entity-group.search-placeholder')}
                                onChange={onChangeSearchInput}
                                showFilters={isFilterPanelVisible}
                                onToggleFilters={
                                    hasAvailableFilters && toggleFilterArea
                                }
                                size="large"
                                style={{
                                    width: '100%',
                                    maxWidth: '300px',
                                    margin: '0 auto 8px',
                                }}
                            />
                        </form>

                        {hasAvailableFilters && (
                            <AnimatePresence visible={isFilterPanelVisible}>
                                <Filters
                                    onUpdateFilter={handleUpdateFilter}
                                    availableFilters={availableFilters}
                                    selectedFilters={filterValue}
                                />
                            </AnimatePresence>
                        )}

                        <FilterWrapper>
                            <Select
                                name="role-filter"
                                label={t('global.role')}
                                value={roleFilter}
                                onChange={(filter) =>
                                    setRoleFilter(filter || null)
                                }
                                isClearable
                                options={groupRoles}
                            />
                        </FilterWrapper>
                    </Container>

                    {selectedMembers.length > 0 && (
                        <MembersActionBar
                            groupGuid={groupGuid}
                            selectedMembers={selectedMembers}
                            totalMembers={members?.total || 0}
                            reset={() => dispatchMember({ type: 'RESET' })}
                        />
                    )}

                    {loading ? (
                        <LoadingSpinner />
                    ) : members ? (
                        <Table edgePadding rowHeight="small">
                            <thead>
                                <tr>
                                    {group.canEdit && (
                                        <th
                                            style={{
                                                width: '42px',
                                                paddingRight: 0,
                                            }}
                                        ></th>
                                    )}
                                    <TableSortableHeader
                                        name="name"
                                        label={t('global.name')}
                                        orderBy={orderBy}
                                        setOrderBy={setOrderBy}
                                        orderDirection={orderDirection}
                                        setOrderDirection={setOrderDirection}
                                    />
                                    {fieldsInOverview.map(({ key, name }) => (
                                        <th key={key}>{name}</th>
                                    ))}
                                    {group.canEdit && (
                                        <TableSortableHeader
                                            name="memberSince"
                                            label={t(
                                                'entity-group.member-since',
                                            )}
                                            orderBy={orderBy}
                                            setOrderBy={setOrderBy}
                                            orderDirection={orderDirection}
                                            setOrderDirection={
                                                setOrderDirection
                                            }
                                            align="right"
                                        />
                                    )}
                                    {group.canEdit && (
                                        <th
                                            style={{
                                                width: '60px',
                                                height:
                                                    fieldsInOverview.length ===
                                                    0
                                                        ? 0
                                                        : null,
                                            }}
                                        >
                                            <HideVisually>
                                                {t('global.actions')}
                                            </HideVisually>
                                        </th>
                                    )}
                                </tr>
                            </thead>
                            <FetchMore
                                as="tbody"
                                edges={members.edges}
                                getMoreResults={(data) => data.members.edges}
                                fetchMore={fetchMore}
                                fetchType="scroll"
                                fetchCount={50}
                                setLimit={setQueryLimit}
                                maxLimit={members.total}
                                noResults={t('entity-group.members-no-results')}
                            >
                                {members.edges.map((entity) => (
                                    <Member
                                        key={`${entity.user.guid}-${String(
                                            selectedMembers.includes(
                                                entity.user.guid,
                                            ),
                                        )}`}
                                        entity={entity}
                                        group={group}
                                        checked={selectedMembers.includes(
                                            entity.user.guid,
                                        )}
                                        onSelectMember={() =>
                                            dispatchMember({
                                                type: 'ADD_MEMBER',
                                                memberGuid: entity.user.guid,
                                            })
                                        }
                                        onDeselectMember={() =>
                                            dispatchMember({
                                                type: 'REMOVE_MEMBER',
                                                memberGuid: entity.user.guid,
                                            })
                                        }
                                    />
                                ))}
                            </FetchMore>
                        </Table>
                    ) : null}
                </Card>
            </Wrapper>
        </>
    )
}

const GET_GROUP_MEMBERS = gql`
    query GroupMembers(
        $groupGuid: String!
        $q: String
        $offset: Int
        $limit: Int
        $filters: [FilterInput]
        $roleFilter: GroupRoleFilter
        $orderBy: GroupMemberOrderBy
        $orderDirection: OrderDirection
    ) {
        entity(guid: $groupGuid) {
            ... on Group {
                guid
                name
                canEdit
                canChangeOwnership
            }
        }
        filters {
            users(groupGuid: $groupGuid) {
                name
                label
                fieldType
                keys
            }
        }
        members(
            groupGuid: $groupGuid
            q: $q
            offset: $offset
            limit: $limit
            filters: $filters
            roleFilter: $roleFilter
            orderBy: $orderBy
            orderDirection: $orderDirection
        ) {
            total
            edges {
                roles
                isOwner
                email
                memberSince
                user {
                    guid
                    username
                    url
                    name
                    icon
                    fieldsInOverview(groupGuid: $groupGuid) {
                        key
                        name
                        value
                        fieldType
                    }
                }
            }
            fieldsInOverview {
                key
                name
            }
        }
    }
`

export default Members
