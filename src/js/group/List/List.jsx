import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { useWindowScrollPosition } from 'helpers'
import PropTypes from 'prop-types'

import FetchMore from 'js/components/FetchMore'
import { Col, Container, Row } from 'js/components/Grid/Grid'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import PageHeader from 'js/components/PageHeader/PageHeader'
import SearchBar from 'js/components/SearchBar/SearchBar'
import Section from 'js/components/Section/Section'
import TabMenu from 'js/components/TabMenu'
import TabPage from 'js/components/TabPage'
import listFragment from 'js/group/fragments/listFragment'

import Card from './components/Card'

const GroupCard = (props) => (
    <Col mobileLandscapeUp={1 / 2}>
        <Card {...props} />
    </Col>
)

const List = ({
    limit,
    offset,
    filter,
    q,
    searchValue,
    onUpdateSearch,
    viewer,
}) => {
    const { t } = useTranslation()
    const { pathname } = useLocation()
    const [queryLimit, setQueryLimit] = useState(limit)

    useWindowScrollPosition('groups')

    const { loading, data, fetchMore } = useQuery(GET_GROUPS, {
        variables: { offset, limit: queryLimit, filter, q },
        options: {
            fetchPolicy: 'cache-and-network',
        },
    })

    const options = [
        {
            to: '/groups',
            label: t('filters.group-all'),
            end: true,
            id: 'tab-all',
            ariaControls: 'tabpanel-all',
        },
        {
            to: '/groups/mine',
            label: t('filters.group-mine'),
            end: true,
            id: 'tab-mine',
            ariaControls: 'tabpanel-mine',
        },
    ]

    const activeTab = pathname.includes('mine') ? 'mine' : 'all'

    return (
        <>
            <PageHeader
                title={t('entity-group.title-list')}
                canCreate={viewer?.canWriteToContainer}
                createLink="/groups/add"
                createLabel={t('entity-group.create')}
                style={viewer?.loggedIn ? { paddingBottom: 0 } : {}}
            >
                {viewer?.loggedIn && (
                    <TabMenu
                        label={t('entity-group.title-list')}
                        options={options}
                    />
                )}
            </PageHeader>

            <TabPage
                id={`tabpanel-${activeTab}`}
                aria-labelledby={`tab-${activeTab}`}
                visible
            >
                <Section>
                    <Container size="tiny" style={{ marginBottom: '24px' }}>
                        <SearchBar
                            name="group-list-search"
                            value={searchValue}
                            onChange={onUpdateSearch}
                            label={t('entity-group.search')}
                        />
                    </Container>
                    <Container>
                        {loading ? (
                            <LoadingSpinner />
                        ) : (
                            <FetchMore
                                edges={data.groups.edges}
                                getMoreResults={(data) => data.groups.edges}
                                fetchMore={fetchMore}
                                fetchCount={limit}
                                setLimit={setQueryLimit}
                                maxLimit={data.groups.total}
                                resultsMessage={t('global.result', {
                                    count: data.groups.total,
                                })}
                                fetchMoreButtonHeight={48}
                            >
                                <Row $growContent>
                                    {data.groups.edges.map((entity) => (
                                        <GroupCard
                                            key={entity.guid}
                                            entity={entity}
                                            viewer={viewer}
                                        />
                                    ))}
                                </Row>
                            </FetchMore>
                        )}
                    </Container>
                </Section>
            </TabPage>
        </>
    )
}

List.propTypes = {
    onUpdateSearch: PropTypes.func.isRequired,
}

const GET_GROUPS = gql`
    query GroupsQuery(
        $filter: GroupFilter
        $offset: Int!
        $limit: Int!
        $q: String!
    ) {
        groups(filter: $filter, offset: $offset, limit: $limit, q: $q) {
            total
            edges {
                ${listFragment}
            }
        }
    }
`

export default List
