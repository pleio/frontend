import styled, { css } from 'styled-components'

import Card from 'js/components/Card/Card'

export default styled(Card)`
    margin-bottom: 20px;

    .GroupCardContent {
        display: flex;
        height: 100%;
    }

    .GroupCardAvatar {
        margin-top: 4px;
        margin-right: 20px;
    }

    .GroupCardContentWrapper {
        flex-grow: 1;
        display: flex;
        flex-direction: column;
        flex-basis: 100%;
    }

    .GroupCardTitle {
        margin-bottom: 4px;
        color: ${(p) => p.theme.color.primary.main};
        cursor: pointer;
    }

    .GroupCardDescription {
        margin-bottom: 16px;
    }

    .GroupCardInfo {
        margin-top: auto;
        margin-bottom: -6px;
    }

    ${(p) =>
        p.$isFeatured &&
        css`
            &:after {
                content: '';
                position: absolute;
                left: 0;
                top: 0;
                bottom: 0;
                width: 4px;
                background-color: ${p.theme.color.secondary.main};
            }
        `}
`
