import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'

import Avatar from 'js/components/Avatar/Avatar'
import { CardContent } from 'js/components/Card/Card'
import { H3 } from 'js/components/Heading'
import Modal from 'js/components/Modal/Modal'
import Truncate from 'js/components/Truncate/Truncate'
import GroupDetails from 'js/group/components/GroupDetails'
import GroupInfo from 'js/group/components/GroupInfo'

import Wrapper from './CardWrapper'

const Card = ({ entity, 'data-feed': dataFeed }) => {
    const {
        isClosed,
        isFeatured,
        canEdit,
        url,
        membership,
        icon,
        name,
        excerpt,
    } = entity

    const [infoModalVisible, setInfoModalVisible] = useState(false)

    const toggleInfoModal = () => setInfoModalVisible(!infoModalVisible)

    const noAccess = isClosed && !canEdit && membership !== 'joined'

    const { t } = useTranslation()

    return (
        <>
            <Wrapper as="article" $isFeatured={isFeatured} data-feed={dataFeed}>
                <CardContent className="GroupCardContent">
                    <Avatar
                        tabIndex="-1"
                        size="large"
                        image={icon?.download}
                        name={name}
                        title={t('entity-group.icon-alt')}
                        aria-label={t('entity-group.go-to', { name })}
                        className="GroupCardAvatar"
                        as={noAccess ? 'button' : Link}
                        to={noAccess ? null : url}
                        onClick={noAccess ? toggleInfoModal : null}
                    />

                    <div className="GroupCardContentWrapper">
                        <H3 as="h2">
                            {noAccess ? (
                                <button
                                    className="GroupCardTitle"
                                    onClick={toggleInfoModal}
                                >
                                    {name}
                                </button>
                            ) : (
                                <Link className="GroupCardTitle" to={url}>
                                    {name}
                                </Link>
                            )}
                        </H3>
                        {excerpt && (
                            <div className="GroupCardDescription">
                                <Truncate>{excerpt}</Truncate>
                            </div>
                        )}
                        <GroupInfo entity={entity} className="GroupCardInfo" />
                    </div>
                </CardContent>
            </Wrapper>
            <Modal
                size="normal"
                showCloseButton
                isVisible={infoModalVisible}
                onClose={toggleInfoModal}
            >
                <GroupDetails guid={entity.guid} />
            </Modal>
        </>
    )
}

export default Card
