import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'
import { useDebounce } from 'helpers'

import List from './List'

const GroupsList = ({ filter }) => {
    const { data } = useQuery(Query)

    const [searchValue, setSearchValue] = useState('')

    const updateSearch = (evt) => {
        setSearchValue(evt.target.value)
    }

    const { t } = useTranslation()

    const title =
        filter === 'all'
            ? t('entity-group.title-list')
            : t('filters.group-mine')

    return (
        <List
            q={useDebounce(searchValue, 200)}
            offset={0}
            limit={40}
            filter={filter}
            title={title}
            onUpdateSearch={updateSearch}
            searchValue={searchValue}
            viewer={data && data.viewer}
        />
    )
}

const Query = gql`
    query GroupsViewer {
        viewer {
            guid
            loggedIn
            canWriteToContainer(subtype: "group")
            isAdmin
        }
    }
`

export default GroupsList
