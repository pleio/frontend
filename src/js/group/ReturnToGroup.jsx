import React from 'react'
import { Link, useLocation } from 'react-router-dom'
import styled from 'styled-components'

import Avatar from 'js/components/Avatar/Avatar'
import { Container } from 'js/components/Grid/Grid'
import { H3 } from 'js/components/Heading'
import Sticky from 'js/components/Sticky/Sticky'

const Wrapper = styled(Sticky)`
    background-color: white;
    height: ${(p) => p.theme.headerBarHeight}px;
    box-shadow: ${(p) => p.theme.shadow.hard.bottom};
    z-index: 2; // Position on top of next Section and .Meta

    &[data-sticky='true'] {
        box-shadow: ${(p) => p.theme.shadow.soft.bottom};
    }

    .ReturnToGroupLink {
        height: 100%;
        display: inline-flex;
        align-items: center;
        color: ${(p) => p.theme.color.primary.main};
    }
`

const ReturnToGroup = ({ entity }) => {
    const location = useLocation()

    if (!entity) return null

    const prevPathname = location?.state?.prevPathname
    const prevPathnameIsInGroup = prevPathname
        ? prevPathname.indexOf(entity.url) !== -1
        : false

    return (
        <Wrapper>
            <Container style={{ height: '100%' }}>
                <H3
                    as={Link}
                    to={prevPathnameIsInGroup ? prevPathname : entity.url}
                    className="ReturnToGroupLink"
                >
                    {entity.icon && (
                        <Avatar
                            size="small"
                            image={entity.icon}
                            disabled
                            style={{ marginRight: '8px' }}
                        />
                    )}
                    {entity.name}
                </H3>
            </Container>
        </Wrapper>
    )
}

export default ReturnToGroup
