import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import AddEditForm from './AddEditForm/AddEditForm'

const Add = ({ data }) => {
    const { loading, viewer, site } = data

    const { t } = useTranslation()

    if (loading) return null

    return (
        <AddEditForm
            title={t('entity-group.create')}
            viewer={viewer}
            site={site}
        />
    )
}

const Query = gql`
    query AddGroup {
        viewer {
            guid
            isAdmin
        }
        site {
            guid
            customTagsAllowed
            tagCategories {
                name
                values
            }
            profileFields {
                guid
                name
            }
            chatEnabled
            autoMembershipProfileFields {
                guid
                name
                fieldOptions
            }
        }
    }
`

export default graphql(Query)(Add)
