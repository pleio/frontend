import React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import Button from 'js/components/Button/Button'
import InputLanguage from 'js/components/EntityActions/General/components/InputLanguage'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import Spacer from 'js/components/Spacer/Spacer'

const Edit = ({ mutate, entity, toggleEdit }) => {
    const {
        guid,
        richDescription,
        group,
        inputLanguage,
        isTranslationEnabled,
    } = entity
    const { t } = useTranslation()

    const refetchQueries = ['ActivityList', 'StatusUpdateItem']

    const submit = async ({
        richDescription,
        inputLanguage,
        isTranslationEnabled,
    }) => {
        await mutate({
            variables: {
                input: {
                    guid,
                    richDescription,
                    inputLanguage,
                    isTranslationEnabled,
                },
            },
            refetchQueries,
        })
            .then(() => {
                toggleEdit()
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    const defaultValues = {
        richDescription,
        inputLanguage,
        isTranslationEnabled,
    }

    const {
        control,
        handleSubmit,
        watch,
        formState: { isValid, isSubmitting },
    } = useForm({ mode: 'onChange', defaultValues })

    const { data } = useQuery(GET_SITE_SETTINGS)
    const showInputLanuage =
        data?.site?.contentTranslation &&
        data?.site?.languageOptions?.length > 1

    const watchIsTranslationEnabled = watch('isTranslationEnabled')

    return (
        <form onSubmit={handleSubmit(submit)}>
            <Spacer spacing="small">
                <FormItem
                    control={control}
                    type="rich"
                    name="richDescription"
                    options={{
                        textStyle: true,
                        textLink: true,
                        textList: true,
                        insertMedia: true,
                        insertMention: true,
                    }}
                    required
                    placeholder={t('entity-group.status-placeholder')}
                    group={group}
                />

                {showInputLanuage && (
                    <InputLanguage
                        control={control}
                        languageOptions={data?.site?.languageOptions}
                        showInputLanguage={watchIsTranslationEnabled}
                    />
                )}
            </Spacer>

            <Flexer mt justifyContent="flex-end">
                <Button size="normal" variant="tertiary" onClick={toggleEdit}>
                    {t('action.cancel')}
                </Button>
                <Button
                    size="normal"
                    variant="primary"
                    type="submit"
                    disabled={!isValid}
                    loading={isSubmitting}
                >
                    {t('action.save')}
                </Button>
            </Flexer>
        </form>
    )
}

const Mutation = gql`
    mutation StatusUpdateCard($input: editEntityInput!) {
        editEntity(input: $input) {
            entity {
                guid
                ... on StatusUpdate {
                    title
                    description
                    richDescription
                    url
                    accessId
                    writeAccessId
                    isBookmarked
                    canBookmark
                    tags
                }
            }
        }
    }
`

const GET_SITE_SETTINGS = gql`
    query GetSiteSettings {
        site {
            guid
            contentTranslation
            languageOptions {
                value
                label
            }
        }
    }
`

export default graphql(Mutation)(Edit)
