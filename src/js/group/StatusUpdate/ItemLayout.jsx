import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'

import CommentList from 'js/components/CommentList'
import Document from 'js/components/Document'
import { Container } from 'js/components/Grid/Grid'
import { H4 } from 'js/components/Heading'
import ToggleTranslation from 'js/components/Item/components/ToggleTranslation'
import ItemHeader from 'js/components/Item/ItemHeader/ItemHeader'
import LikeAndBookmark from 'js/components/LikeAndBookmark'
import Section from 'js/components/Section/Section'
import SocialShare from 'js/components/SocialShare/SocialShare'
import TiptapView from 'js/components/Tiptap/TiptapView'
import ThemeProvider from 'js/theme/ThemeProvider'

import Edit from './Edit'

const Item = ({ entity, viewer }) => {
    const { t } = useTranslation()

    const [editing, setEditing] = useState(false)
    const toggleEdit = () => setEditing(!editing)

    const {
        title,
        group,
        statusPublished,
        isTranslationEnabled,
        richDescription,
        localRichDescription,
        commentCount,
    } = entity

    const containerTitle = group?.name
    const onAfterDelete = group?.url
    const refetchQueries = ['StatusUpdateItem']

    const isPublished = statusPublished === 'published'

    const hasTranslations = !!localRichDescription && isTranslationEnabled
    const [isTranslated, setIsTranslated] = useState(hasTranslations)

    const translatedRichDescription =
        (isTranslated && localRichDescription) || richDescription

    return (
        <ThemeProvider bodyColor="#FFFFFF">
            <Document title={title} containerTitle={containerTitle} />

            <Section backgroundColor="white" grow>
                <Container size="normal">
                    <article
                        style={{
                            position: 'relative',
                        }}
                    >
                        <ItemHeader
                            entity={entity}
                            onEdit={toggleEdit}
                            onAfterDelete={onAfterDelete}
                        />

                        {editing ? (
                            <Edit entity={entity} toggleEdit={toggleEdit} />
                        ) : (
                            <>
                                <TiptapView
                                    content={translatedRichDescription}
                                />
                                {hasTranslations && (
                                    <ToggleTranslation
                                        isTranslated={isTranslated}
                                        setIsTranslated={setIsTranslated}
                                        style={{ marginTop: '4px' }}
                                    />
                                )}
                            </>
                        )}

                        {isPublished && viewer.loggedIn && (
                            <LikeAndBookmark entity={entity} />
                        )}

                        {isPublished && window.__SETTINGS__.enableSharing && (
                            <SocialShare />
                        )}

                        <H4
                            variant="grey"
                            style={{
                                marginTop: '24px',
                            }}
                        >
                            {t('comments.count-comments', {
                                count: commentCount,
                            })}
                        </H4>
                        <CommentList
                            entity={entity}
                            refetchQueries={refetchQueries}
                            viewer={viewer}
                        />
                    </article>
                </Container>
            </Section>
        </ThemeProvider>
    )
}

export default Item
