import React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import Card from 'js/components/Card/Card'
import FormItem from 'js/components/Form/FormItem'

const Wrapper = styled(Card)`
    overflow: hidden;
    margin-bottom: 20px;

    .ProseMirror {
        padding-bottom: 56px;
    }
`

const CreateStatusUpdate = ({ mutate, group }) => {
    const { data } = useQuery(GET_VIEWER, {
        variables: {
            guid: group?.guid,
        },
        skip: !group?.guid,
    })

    const submit = async ({ richDescription }) => {
        await mutate({
            variables: {
                input: {
                    type: 'object',
                    subtype: 'statusupdate',
                    richDescription,
                    groupGuid: group?.guid,
                },
            },
            refetchQueries: ['ActivityList'],
        })
            .then(() => {
                reset()
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    const defaultValues = {
        richDescription: '',
    }

    const {
        control,
        handleSubmit,
        reset,
        formState: { isValid, isSubmitting },
    } = useForm({ mode: 'onChange', defaultValues })

    const { t } = useTranslation()

    if (!data?.viewer || (data?.viewer && !data?.viewer.canWriteToContainer))
        return null

    return (
        <Wrapper as="form" onSubmit={handleSubmit(submit)}>
            <FormItem
                control={control}
                type="rich"
                name="richDescription"
                label={t('entity-group.status-placeholder')}
                options={{
                    textStyle: true,
                    textLink: true,
                    textList: true,
                    insertMedia: true,
                    insertMention: true,
                }}
                required
                borderStyle="none"
                group={group}
            />
            <div
                style={{
                    position: 'absolute',
                    bottom: '12px',
                    right: '16px',
                }}
            >
                <Button
                    size="normal"
                    variant="primary"
                    type="submit"
                    disabled={!isValid}
                    loading={isSubmitting}
                >
                    {t('entity-group.status-set')}
                </Button>
            </div>
        </Wrapper>
    )
}

const GET_VIEWER = gql`
    query GroupStatusUpdate($guid: String!) {
        viewer {
            guid
            canWriteToContainer(containerGuid: $guid, subtype: "statusupdate")
        }
    }
`

const Mutation = gql`
    mutation addStatusUpdate($input: addEntityInput!) {
        addEntity(input: $input) {
            entity {
                guid
                ... on StatusUpdate {
                    description
                }
            }
        }
    }
`

export default graphql(Mutation)(CreateStatusUpdate)
