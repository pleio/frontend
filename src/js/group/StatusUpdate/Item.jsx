import React from 'react'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import NotFound from 'js/core/NotFound'
import { iconViewFragment } from 'js/lib/fragments/icon'
import { commentOwnerFragment, ownerFragment } from 'js/lib/fragments/owner'

import ItemLayout from './ItemLayout'

const Item = () => {
    const params = useParams()
    const { guid } = params

    const { loading, data } = useQuery(GET_STATUS_UPDATE_ITEM, {
        variables: {
            guid,
        },
    })

    if (loading) return null
    if (!data || !data.entity) return <NotFound />

    const { entity, viewer } = data

    return <ItemLayout entity={entity} viewer={viewer} />
}

const GET_STATUS_UPDATE_ITEM = gql`
    query StatusUpdateItem($guid: String!) {
        viewer {
            guid
            loggedIn
            user {
                guid
                name
                icon
            }
        }
        entity(guid: $guid) {
            guid
            ... on StatusUpdate {
                title
                description
                richDescription
                localRichDescription
                isTranslationEnabled
                inputLanguage
                accessId
                timePublished
                statusPublished
                canEdit
                canArchiveAndDelete
                tags
                url
                views
                votes
                hasVoted
                isBookmarked
                isFollowing
                canBookmark
                ${ownerFragment}
                commentCount
                comments {
                    guid
                    description
                    excerpt
                    richDescription
                    timeCreated
                    canEdit
                    votes
                    hasVoted
                    ${commentOwnerFragment}
                    canComment
                    commentCount
                    comments {
                        guid
                        excerpt
                        richDescription
                        timeCreated
                        canEdit
                        votes
                        hasVoted
                        ${commentOwnerFragment}
                        canComment
                    }
                }
                group {
                    guid
                    ... on Group {
                        isClosed
                        url
                        name
                        ${iconViewFragment}
                        membership
                    }
                }
            }
        }
    }
`

export default Item
