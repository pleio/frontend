import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'

import FeedItem from 'js/components/FeedItem/FeedItem'
import FeedItemContent from 'js/components/FeedItem/FeedItemContent'
import FeedItemFooter from 'js/components/FeedItem/FeedItemFooter'
import HideVisually from 'js/components/HideVisually/HideVisually'
import ToggleTranslation from 'js/components/Item/components/ToggleTranslation'
import ShowMore from 'js/components/ShowMore'
import TiptapView from 'js/components/Tiptap/TiptapView'

import Edit from './Edit'

const StatusUpdateCard = ({
    'data-feed': dataFeed,
    entity,
    canPin,
    hideGroup,
    hideComments,
    hideLikes,
}) => {
    const { t } = useTranslation()

    const refetchQueries = ['ActivityList']

    const [editing, setEditing] = useState(false)
    const toggleEdit = () => setEditing(!editing)

    const { isTranslationEnabled, richDescription, localRichDescription } =
        entity
    const hasTranslations = !!localRichDescription && isTranslationEnabled
    const [isTranslated, setIsTranslated] = useState(hasTranslations)

    const translatedRichDescription =
        (isTranslated && localRichDescription) || richDescription

    return (
        <FeedItem data-feed={dataFeed}>
            <FeedItemContent
                entity={entity}
                hideGroup={hideGroup}
                hideExcerpt
                canPin={canPin}
                onEdit={toggleEdit}
                refetchQueries={refetchQueries}
            >
                {editing ? (
                    <Edit entity={entity} toggleEdit={toggleEdit} />
                ) : (
                    <>
                        <ShowMore>
                            <HideVisually>
                                <h3>{t('entity-statusupdate.content-name')}</h3>
                            </HideVisually>
                            <TiptapView content={translatedRichDescription} />
                        </ShowMore>
                        {hasTranslations && (
                            <ToggleTranslation
                                isTranslated={isTranslated}
                                setIsTranslated={setIsTranslated}
                                style={{ marginTop: '4px' }}
                            />
                        )}
                    </>
                )}
            </FeedItemContent>
            <FeedItemFooter
                entity={entity}
                hideComments={hideComments}
                hideLikes={hideLikes}
            />
        </FeedItem>
    )
}

export default StatusUpdateCard
