import { ownerFragment } from 'js/lib/fragments/owner'

const statusUpdateListFragment = `
    fragment StatusUpdateListFragment on StatusUpdate {
        url
        excerpt
        description
        richDescription
        localRichDescription
        isTranslationEnabled
        inputLanguage,
        timePublished
        statusPublished
        isBookmarked
        isPinned
        canBookmark
        canEdit
        canArchiveAndDelete
        subtype
        commentCount
        canComment
        hasVoted
        votes
        ${ownerFragment}
        group {
            guid
            ... on Group {
                isClosed
                name
                url
                membership
                isMembershipOnRequest
                isJoinButtonVisible
            }
        }
    }
`
export default statusUpdateListFragment
