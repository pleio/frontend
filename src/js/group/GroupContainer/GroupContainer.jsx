import React, { useContext } from 'react'
import { useTranslation } from 'react-i18next'
import { Outlet, useLocation, useNavigate, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { media } from 'helpers'
import styled from 'styled-components'

import chatFragment from 'js/chat/fragments/chat'
import Avatar from 'js/components/Avatar/Avatar'
import CoverImage from 'js/components/CoverImage'
import Flexer from 'js/components/Flexer/Flexer'
import { Container } from 'js/components/Grid/Grid'
import { H3 } from 'js/components/Heading'
import IconButton from 'js/components/IconButton/IconButton'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Modal from 'js/components/Modal/Modal'
import NotFound from 'js/core/NotFound'
import GroupDetails from 'js/group/components/GroupDetails'
import JoinGroupButton from 'js/group/components/JoinGroupButton'
import ReturnToGroup from 'js/group/ReturnToGroup'
import { featuredViewFragment } from 'js/lib/fragments/featured'
import { iconViewFragment } from 'js/lib/fragments/icon'
import globalStateContext from 'js/lib/withGlobalState/globalStateContext'
import Page from 'js/page/Item'
import ProfileSections from 'js/user/Profile/components/ProfileSections'

import ChatIcon from 'icons/chat.svg'

import CreateItemButton from './components/CreateItemButton'
import GroupNavigation from './components/GroupNavigation'
import GroupSearch from './components/GroupSearch'
import GroupActions from './GroupActions/GroupActions'

const Wrapper = styled.div`
    margin-bottom: 20px;
    background-color: white;
    overflow: hidden;
    box-shadow: ${(p) =>
        p.$isDetailPage ? p.theme.shadow.hard.outside : p.theme.card.shadow};

    ${media.mobileLandscapeDown`
        margin-left: -20px;
        margin-right: -20px;
    `};

    ${media.tabletUp`
        border-bottom-left-radius: ${(p) => p.theme.radius.normal};
        border-bottom-right-radius: ${(p) => p.theme.radius.normal};
    `};

    .GroupCover {
        display: ${(p) => !p.$isHomePage && 'none'};
    }

    .GroupContentWrapper {
        ${media.mobileLandscapeDown`
            padding-left: ${(p) => p.theme.padding.horizontal.small};
            padding-right: ${(p) => p.theme.padding.horizontal.small};
        `};

        ${media.tabletUp`
            padding-left: ${(p) => p.theme.padding.horizontal.normal};
            padding-right: ${(p) => p.theme.padding.horizontal.normal};
        `};
    }

    .GroupContentTop {
        display: flex;

        ${media.mobileLandscapeDown`
            flex-wrap: wrap;
        `};

        ${media.mobilePortrait`
            margin-bottom: 8px;
        `};

        ${media.tabletUp`
            margin-bottom: -8px;
        `};
    }

    .GroupInfo {
        display: flex;
        align-items: center;
        padding: 8px 0;
    }

    .GroupAvatar {
        width: 24px;
        height: 24px;
    }

    .GroupTools {
        flex-shrink: 0;

        ${media.mobilePortrait`
            width: 100%;

            .GroupToolsAlign {
                flex-grow: 1;
            }
        `};

        ${media.mobileLandscapeUp`
            margin-left: auto;
            padding: 4px 0 4px 12px;
        `};
    }

    .GroupChatButton {
        position: relative;
    }

    .GroupChatBadge {
        position: absolute;
        top: 8px;
        right: 7px;
        width: 8px;
        height: 8px;
        border-radius: 50%;
        background-color: ${(p) => p.theme.color.warn.main};
    }
`

const GroupContainer = ({ isHomePage, isDetailPage }) => {
    const { t } = useTranslation()
    const location = useLocation()
    const navigate = useNavigate()

    const { groupGuid } = useParams()

    // Check if user has access to content before rendering page
    const { loading, error, data } = useQuery(GET_GROUP_CONTAINER, {
        variables: { guid: groupGuid },
        fetchPolicy: location.state?.reload ? 'network-only' : 'cache-first',
    })

    const { setGlobalState } = useContext(globalStateContext)

    if (loading) return <LoadingSpinner />
    if (error || !data.entity) return <NotFound />

    const { entity, viewer, site } = data

    const {
        isClosed,
        canEdit,
        membership,
        requiredProfileFieldsMessage,
        name,
        url,
        icon,
        featured,
        plugins,
        isChatEnabled,
        chat,
        startPage,
        isMenuAlwaysVisible,
    } = entity
    const { canCreateNews } = viewer

    if (isClosed && !canEdit && membership !== 'joined') {
        const navigateToGroups = () => navigate('/groups')

        return (
            <Modal
                size="normal"
                showCloseButton
                isVisible
                onClose={navigateToGroups}
            >
                <GroupDetails guid={groupGuid} />
            </Modal>
        )
    }

    if (viewer?.user?.missingProfileFields?.length > 0) {
        const navigateToGroups = () => navigate('/groups')

        return (
            <Modal
                size="normal"
                isVisible={true}
                onClose={navigateToGroups}
                title={name}
                showCloseButton
            >
                {requiredProfileFieldsMessage && (
                    <p style={{ marginBottom: '24px' }}>
                        {requiredProfileFieldsMessage}
                    </p>
                )}

                <ProfileSections
                    user={viewer.user}
                    fields={viewer.user.missingProfileFields}
                    isEditingDefault
                    allFieldsRequired
                />
            </Modal>
        )
    }

    const openChat = () => {
        setGlobalState((newState) => {
            newState.activeMainThread = chat
        })
    }

    const startPageGuid = startPage?.guid

    if (isDetailPage && !isMenuAlwaysVisible) {
        // In this case we don't show the full group menu in the header
        // but just a link to the group homepage
        return (
            <>
                <ReturnToGroup entity={entity} />
                <Outlet />
            </>
        )
    }

    return (
        <>
            <Container>
                <Wrapper $isHomePage={isHomePage} $isDetailPage={isDetailPage}>
                    <CoverImage
                        className="GroupCover"
                        featured={featured}
                        size="landscape-small"
                    />

                    <div className="GroupContentWrapper">
                        <div className="GroupContentTop">
                            <div className="GroupInfo">
                                {icon?.download && (
                                    <Avatar
                                        size="small"
                                        image={icon.download}
                                        disabled
                                        style={{
                                            marginRight: '8px',
                                        }}
                                    />
                                )}
                                <H3 as="h1">{name}</H3>
                                <GroupActions entity={entity} viewer={viewer} />
                            </div>
                            <Flexer gutter="small" className="GroupTools">
                                <JoinGroupButton
                                    entity={entity}
                                    loggedIn={viewer.loggedIn}
                                    showModal
                                />
                                <Flexer
                                    gutter="tiny"
                                    justifyContent="flex-start"
                                    className="GroupToolsAlign"
                                >
                                    <GroupSearch />
                                    {site.chatEnabled &&
                                        isChatEnabled &&
                                        membership === 'joined' && (
                                            <div className="GroupChatButton">
                                                <IconButton
                                                    size="large"
                                                    variant="secondary"
                                                    tooltip={t('chat.title')}
                                                    onClick={openChat}
                                                >
                                                    <ChatIcon />
                                                </IconButton>
                                                {chat.unread > 0 && (
                                                    <div className="GroupChatBadge" />
                                                )}
                                            </div>
                                        )}
                                </Flexer>
                                {membership === 'joined' && (
                                    <CreateItemButton
                                        canCreateNews={canCreateNews}
                                        plugins={plugins}
                                        url={url}
                                    />
                                )}
                            </Flexer>
                        </div>
                    </div>
                    <GroupNavigation entity={entity} />
                </Wrapper>
            </Container>

            {isHomePage && startPageGuid ? (
                <Page guid={startPageGuid} isHome />
            ) : (
                <Outlet />
            )}
        </>
    )
}

export default GroupContainer

const GET_GROUP_CONTAINER = gql`
    query GroupContainer($guid: String!) {
        site {
            guid
            chatEnabled
        }
        entity(guid: $guid) {
            guid
            ... on Group {
                name
                url
                canEdit
                isClosed
                isMembershipOnRequest
                membership
                isLeavingGroupDisabled
                plugins
                description
                richDescription
                introduction
                ${iconViewFragment}
                isChatEnabled
                isJoinButtonVisible
                isMenuAlwaysVisible
                chat {
                    ${chatFragment}
                }
                ${featuredViewFragment}
                requiredProfileFields {
                    guid
                    name
                }
                requiredProfileFieldsMessage
                subgroups {
                    edges {
                        id
                        name
                    }
                }
                menu {
                    type
                    ... on GroupMenuPluginItem {
                        label
                        plugin
                    }
                    ... on GroupMenuLinkItem {
                        label
                        link
                    }
                    ... on GroupMenuPageItem {
                        page {
                            guid
                            title
                        }
                    }
                    ... on GroupSubmenuItem {
                        label
                        submenu {
                            type
                            ... on GroupMenuPluginItem {
                                label
                                plugin
                            }
                            ... on GroupMenuLinkItem {
                                label
                                link
                            }
                            ... on GroupMenuPageItem {
                                page {
                                    guid
                                    title
                                }
                            }
                        }
                    }
                }
                startPage {
                    guid
                    title
                }
            }
        }
        viewer {
            guid
            loggedIn
            isAdmin
            canCreateNews: canWriteToContainer(
                containerGuid: $guid
                subtype: "news"
            )
            user {
                guid
                name
                icon
                url
                canEdit
                missingProfileFields(groupGuid: $guid) {
                    key
                    name
                    category
                    accessId
                    fieldType
                    isEditable
                    fieldOptions
                }
            }
        }
    }
`
