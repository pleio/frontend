import React from 'react'
import { media } from 'helpers'
import styled from 'styled-components'

import Sticky from 'js/components/Sticky/Sticky'
import TabMenu from 'js/components/TabMenu'

const Wrapper = styled(Sticky)`
    &[data-sticky='true'] {
        background: white;
        box-shadow: ${(p) => p.theme.shadow.soft.bottom};
        z-index: 10;
    }

    ${media.mobileLandscapeDown`
        padding-left: ${(p) => p.theme.padding.horizontal.small};
        padding-right: ${(p) => p.theme.padding.horizontal.small};
    `};

    ${media.tabletUp`
        padding-left: ${(p) => p.theme.padding.horizontal.normal};
        padding-right: ${(p) => p.theme.padding.horizontal.normal};
    `};
`

const GroupNavigation = ({ entity }) => {
    const { canEdit, membership, menu, startPage, url } = entity
    const showMembersMenuItem = canEdit || membership === 'joined'

    const getMenuObject = ({ type, plugin, page, label, link, submenu }) => {
        if (plugin === 'members' && !showMembersMenuItem) return null

        switch (type) {
            case 'page': {
                const { guid, title } = page || {}

                if (!guid && !title) return

                return {
                    label: title,
                    to: `${url}/page/view/${guid}/${title}`,
                }
            }

            case 'plugin':
                return {
                    label,
                    to: `${url}/${plugin}`,
                }

            case 'link':
                return {
                    label,
                    url: link,
                }

            case 'submenu':
                return {
                    label,
                    options: submenu.map(getMenuObject),
                }

            default:
        }
    }

    const tabOptions = menu
        .filter(
            (item) => item.plugin || item.page || item.submenu || item.link, // Only plugins and pages that the viewer has access to
        )
        .map((item) => getMenuObject(item))
        .filter(Boolean)

    if (startPage?.title) {
        tabOptions.unshift({
            to: url,
            label: startPage?.title,
            end: true,
        })
    }

    return (
        <Wrapper>
            <TabMenu options={tabOptions} />
        </Wrapper>
    )
}

export default GroupNavigation
