import React from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation, useMatch } from 'react-router-dom'

import Button from 'js/components/Button/Button'
import DropdownButton from 'js/components/DropdownButton/DropdownButton'
import useSubtypes from 'js/lib/hooks/useSubtypes'

const CreateItemButton = ({ plugins, canCreateNews, url }) => {
    const { t } = useTranslation()
    const location = useLocation()
    const { subtypes } = useSubtypes()

    const match = useMatch('groups/view/:groupGuid/:groupSlug/:page')
    const pageParam = match?.params?.page

    const isActiveType = (type) => {
        return pageParam === type
    }
    const canCreateType = (type) => {
        return plugins.includes(type)
    }
    const isActiveAndCanCreateType = (type) => {
        return isActiveType(type) && canCreateType(type)
    }

    const getCreateLink = (type, urlPart, translationKey) => {
        return {
            default: isActiveType(type),
            to: `${url}/${urlPart}/add`,
            name: t(translationKey),
            state: {
                prevPathname: location.pathname,
            },
        }
    }

    const getDropdownOptions = () => {
        const types = [
            ['events', 'event'],
            ['blog', 'blog'],
            ['discussion', 'discussion'],
            ['questions', 'question'],
            ['news', 'news'],
            ['wiki', 'wiki'],
            ['podcasts', 'podcast'],
            ['tasks', 'task'],
        ]
        return types
            .map(([type, translationKey]) => {
                if (type === 'news') {
                    return canCreateNews
                        ? getCreateLink(
                              'news',
                              'news',
                              `entity-news.content-name`,
                          )
                        : null
                }
                return canCreateType(type)
                    ? getCreateLink(
                          type,
                          type,
                          subtypes[translationKey].contentName,
                      )
                    : null
            })
            .filter(Boolean)
    }

    return (
        <DropdownButton options={getDropdownOptions()} showArrow>
            {isActiveAndCanCreateType('events') ? (
                t('entity-event.create')
            ) : isActiveAndCanCreateType('blog') ? (
                t('entity-blog.create')
            ) : isActiveAndCanCreateType('discussion') ? (
                t('entity-discussion.create')
            ) : isActiveAndCanCreateType('questions') ? (
                t('entity-question.create')
            ) : canCreateNews && isActiveAndCanCreateType('news') ? (
                t('entity-news.create')
            ) : isActiveAndCanCreateType('wiki') ? (
                t('entity-wiki.create')
            ) : isActiveAndCanCreateType('podcasts') ? (
                t('entity-podcast.create-title')
            ) : isActiveAndCanCreateType('tasks') ? (
                t('entity-task.create')
            ) : (
                <Button size="normal" variant="primary">
                    <span style={{ marginRight: 6 }}>{t('action.create')}</span>
                </Button>
            )}
        </DropdownButton>
    )
}

export default CreateItemButton
