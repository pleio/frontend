import React, { useRef, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation, useNavigate, useParams } from 'react-router-dom'
import { media } from 'helpers'
import styled, { css } from 'styled-components'

import HideVisually from 'js/components/HideVisually/HideVisually'
import IconButton from 'js/components/IconButton/IconButton'

import SearchIcon from 'icons/search.svg'

const visibleStyle = css`
    border-color: ${(p) => p.theme.color.grey[40]};

    .GroupSearchInput {
        opacity: 1;
    }
`

const Wrapper = styled.div`
    position: relative;
    width: 40px;
    max-width: 260px;
    height: 40px;
    border-radius: ${(p) => p.theme.radius.small};
    border: 1px solid transparent;
    overflow: hidden;
    transition:
        width ${(p) => p.theme.transition.materialFast},
        border-color ${(p) => p.theme.transition.fast};

    .GroupSearchInput {
        position: absolute;
        left: 0;
        top: 0;
        bottom: 0;
        height: 100%; /* IE */
        border: none;
        padding: 0 0 0 16px;
        width: calc(100% - 38px);
        outline-offset: -1px;
        background: none;
        font-size: ${(p) => p.theme.font.size.small};
        opacity: 0;
        transition: opacity ${(p) => p.theme.transition.fast};
    }

    .GroupSearchButton {
        width: 38px;
        height: 38px;
        position: absolute;
        right: 0;
        top: 0;
        bottom: 0;
        margin: auto 0;

        &:before {
            width: 38px;
            height: 38px;
        }
    }

    ${media.mobilePortrait`
        width: 100%;
        ${visibleStyle};
    `};

    ${media.mobileLandscapeUp`
        ${(p) =>
            p.$isVisible &&
            css`
                width: 220px;
                ${visibleStyle};
            `};
    `};
`

const GroupSearch = () => {
    const location = useLocation()
    const navigate = useNavigate()
    const params = useParams()

    const [isFocused, setIsFocused] = useState(false)
    const handleFocus = () => setIsFocused(true)
    const handleBlur = () => setIsFocused(false)

    const [searchValue, setSearchValue] = useState('')

    const onChange = (evt) => setSearchValue(evt.target.value)

    const onKeyDown = (evt) => {
        if (evt.key === 'Enter') handleSearch()
        if (evt.key === 'Escape') evt.target.blur()
    }

    const refInput = useRef()
    const focusInput = () => refInput.current.focus()

    const handleSearch = () => {
        const getLink = (type) =>
            `/groups/view/${params.groupGuid}/${params.groupSlug}/${type}`

        let subtype
        switch (location.pathname) {
            case getLink('events'):
                subtype = 'event'
                break

            case getLink('blog'):
                subtype = 'blog'
                break

            case getLink('questions'):
                subtype = 'question'
                break

            case getLink('discussion'):
                subtype = 'discussion'
                break

            case getLink('news'):
                subtype = 'news'
                break

            case getLink('files'):
                subtype = 'file'
                break

            case getLink('wiki'):
                subtype = 'wiki'
                break
        }

        if (subtype) {
            navigate(
                `/groups/view/${params.groupGuid}/${
                    params.groupSlug
                }/search?q=${searchValue.trim()}&type=object&subtype=${subtype}`,
            )
        } else {
            navigate(
                `/groups/view/${params.groupGuid}/${
                    params.groupSlug
                }/search?q=${searchValue.trim()}`,
            )
        }
    }

    const { t } = useTranslation()

    return (
        <Wrapper $isVisible={isFocused || searchValue}>
            <HideVisually as="label" htmlFor="group-search">
                {t('entity-group.search-in')}
            </HideVisually>
            <input
                ref={refInput}
                className="GroupSearchInput"
                name="group-search"
                id="group-search"
                onFocus={handleFocus}
                onBlur={handleBlur}
                onKeyDown={onKeyDown}
                onChange={onChange}
                value={searchValue}
                placeholder={t('entity-group.search-in')}
            />
            <IconButton
                className="GroupSearchButton"
                size="large"
                variant="secondary"
                radiusStyle={isFocused || searchValue ? 'none' : 'round'}
                onClick={searchValue ? handleSearch : focusInput}
                disabled={isFocused && !searchValue}
                aria-label={t('entity-group.search-in')}
                tabIndex="-1"
                tooltip={t('global.search')}
            >
                <SearchIcon />
            </IconButton>
        </Wrapper>
    )
}

export default GroupSearch
