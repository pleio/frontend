import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation } from 'react-router-dom'
import { media } from 'helpers'
import styled, { css } from 'styled-components'

import DropdownButton from 'js/components/DropdownButton/DropdownButton'
import IconButton from 'js/components/IconButton/IconButton'
import Modal from 'js/components/Modal/Modal'
import GroupDetails from 'js/group/components/GroupDetails'

import InfoIcon from 'icons/info.svg'
import OptionsIcon from 'icons/options-small.svg'

import InviteMembers from './components/InviteMembers'
import LeaveGroup from './components/LeaveGroup'
import MembershipRequests from './components/MembershipRequests'
import SendMessage from './components/SendMessage'
import Subgroups from './components/Subgroups'

const OptionsButton = styled(IconButton)`
    margin-left: 8px;

    ${(p) =>
        p.$canEdit &&
        css`
            padding: 0 12px;
            border-radius: 16px;

            .IconButtonBackground {
                width: 100%;
                border-radius: 16px;
            }

            ${media.mobilePortrait`
                span {
                    display: none;
                }
            `};

            ${media.mobileLandscapeUp`
                width: auto;

                span {
                    white-space: nowrap;
                    font-size: ${(p) => p.theme.font.size.small};
                    line-height: ${(p) => p.theme.font.lineHeight.small};
                    font-weight: ${(p) => p.theme.font.weight.semibold};
                }

                svg {
                    display: none;
                }
            `};
        `};
`

const GroupActions = ({ entity, viewer }) => {
    const location = useLocation()

    const [showGroupDetailsModal, setShowGroupDetailsModal] = useState(false)
    const toggleGroupDetailsModal = () =>
        setShowGroupDetailsModal(!showGroupDetailsModal)

    const [showLeaveGroupModal, setShowLeaveGroupModal] = useState(false)
    const toggleLeaveGroupModal = () =>
        setShowLeaveGroupModal(!showLeaveGroupModal)

    const [showInviteMembersModal, setShowInviteMembersModal] = useState(false)
    const toggleInviteMembersModal = () =>
        setShowInviteMembersModal(!showInviteMembersModal)

    const [showMembershipRequestsModal, setShowMembershipRequestsModal] =
        useState(false)
    const toggleMembershipRequestsModal = () =>
        setShowMembershipRequestsModal(!showMembershipRequestsModal)

    const [showSendMessageModal, setShowSendMessageModal] = useState(false)
    const toggleSendMessageModal = () =>
        setShowSendMessageModal(!showSendMessageModal)

    const [showSubgroupsModal, setShowSubgroupsModal] = useState(false)
    const toggleSubgroupsModal = () =>
        setShowSubgroupsModal(!showSubgroupsModal)

    const { t } = useTranslation()

    const { guid, canEdit, membership, url, isLeavingGroupDisabled } = entity

    if (!canEdit && membership !== 'joined') {
        return (
            <>
                <IconButton
                    size="normal"
                    variant="tertiary"
                    onClick={toggleGroupDetailsModal}
                    tooltip={t('entity-group.group-details')}
                    placement="right"
                >
                    <InfoIcon />
                </IconButton>
                <Modal
                    size="normal"
                    showCloseButton
                    isVisible={showGroupDetailsModal}
                    onClose={toggleGroupDetailsModal}
                >
                    <GroupDetails guid={guid} />
                </Modal>
            </>
        )
    } else {
        return (
            <>
                <DropdownButton
                    placement="bottom-start"
                    options={[
                        [
                            {
                                onClick: toggleGroupDetailsModal,
                                name: t('entity-group.about'),
                            },
                            ...(canEdit
                                ? [
                                      {
                                          to: `/groups/edit/${guid}`,
                                          name: t('entity-group.edit-action'),
                                          state: {
                                              prevPathname: location.pathname,
                                          },
                                      },
                                  ]
                                : []),
                        ],
                        canEdit
                            ? [
                                  {
                                      to: `${url}/pages`,
                                      name: t('admin.pages'),
                                  },
                                  {
                                      to: `${url}/members`,
                                      name: t('entity-group.members'),
                                  },
                                  ...(window.__SETTINGS__.subgroups
                                      ? [
                                            {
                                                onClick: toggleSubgroupsModal,
                                                name: t(
                                                    'entity-group.subgroups',
                                                ),
                                            },
                                        ]
                                      : []),
                              ]
                            : [],
                        canEdit
                            ? [
                                  {
                                      onClick: toggleSendMessageModal,
                                      name: t('global.send-message'),
                                  },
                                  {
                                      onClick: toggleInviteMembersModal,
                                      name: t('global.invite-people'),
                                  },
                                  {
                                      onClick: toggleMembershipRequestsModal,
                                      name: t('entity-group.requests'),
                                  },
                                  ...(window.__SETTINGS__.groupMemberExport
                                      ? [
                                            {
                                                href: `/exporting/group/${guid}`,
                                                name: t(
                                                    'entity-group.export-members',
                                                ),
                                            },
                                        ]
                                      : []),
                              ]
                            : [],
                        membership === 'joined'
                            ? [
                                  ...(!isLeavingGroupDisabled
                                      ? [
                                            {
                                                onClick: toggleLeaveGroupModal,
                                                name: t(
                                                    'entity-group.leave-group',
                                                ),
                                            },
                                        ]
                                      : []),
                              ]
                            : [],
                    ]}
                >
                    <OptionsButton
                        size="normal"
                        variant="secondary"
                        $canEdit={canEdit}
                        showOutline
                        aria-label={
                            canEdit ? null : t('entity-group.group-options')
                        }
                        tooltip={canEdit ? null : t('global.options')}
                        placement="right"
                    >
                        <OptionsIcon />
                        {canEdit && <span>{t('action.manage')}</span>}
                    </OptionsButton>
                </DropdownButton>

                <Modal
                    size="normal"
                    showCloseButton
                    isVisible={showGroupDetailsModal}
                    onClose={toggleGroupDetailsModal}
                >
                    <GroupDetails guid={guid} />
                </Modal>

                <Modal
                    size="small"
                    title={t('entity-group.leave-group')}
                    isVisible={showLeaveGroupModal}
                    onClose={toggleLeaveGroupModal}
                >
                    <LeaveGroup
                        entity={entity}
                        onClose={toggleLeaveGroupModal}
                    />
                </Modal>

                <Modal
                    size="normal"
                    isVisible={showInviteMembersModal}
                    onClose={toggleInviteMembersModal}
                    title={t('global.invite-people')}
                    showCloseButton
                >
                    <InviteMembers guid={guid} viewer={viewer} />
                </Modal>

                <Modal
                    size="normal"
                    title={t('entity-group.requests')}
                    showCloseButton
                    isVisible={showMembershipRequestsModal}
                    onClose={toggleMembershipRequestsModal}
                >
                    <MembershipRequests guid={guid} />
                </Modal>

                <Modal
                    size="normal"
                    title={t('global.send-message')}
                    showCloseButton
                    isVisible={showSendMessageModal}
                    onClose={toggleSendMessageModal}
                    containHeight
                >
                    <SendMessage
                        entity={entity}
                        onClose={toggleSendMessageModal}
                    />
                </Modal>

                {window.__SETTINGS__.subgroups && (
                    <Modal
                        size="normal"
                        title={t('entity-group.subgroups')}
                        showCloseButton
                        isVisible={showSubgroupsModal}
                        onClose={toggleSubgroupsModal}
                        containHeight
                    >
                        <Subgroups containerGuid={guid} />
                    </Modal>
                )}
            </>
        )
    }
}

export default GroupActions
