import React from 'react'
import { useRoleLabel } from 'helpers'
import styled from 'styled-components'

import Avatar from 'js/components/Avatar/Avatar'
import Checkbox from 'js/components/Checkbox/Checkbox'

const Wrapper = styled.div`
    display: flex;
    align-items: center;
    position: relative;
    padding: 4px 0;
    font-size: ${(p) => p.theme.font.size.small};
    line-height: ${(p) => p.theme.font.lineHeight.small};
`

const SubgroupMember = ({ member, selected, selectable, onSelect }) => {
    const { roles, isOwner, user } = member

    const avatarRole = useRoleLabel(roles, isOwner)

    return (
        <Wrapper>
            {selectable && (
                <Checkbox
                    onChange={() => onSelect(member.user.guid)}
                    defaultChecked={selected}
                    releaseInputArea
                    style={{ marginRight: '8px' }}
                />
            )}
            <Avatar
                image={user.icon}
                name={user.name}
                role={avatarRole}
                disabled
                style={{ marginRight: '12px' }}
            />
            {member.user.name}
        </Wrapper>
    )
}

export default SubgroupMember
