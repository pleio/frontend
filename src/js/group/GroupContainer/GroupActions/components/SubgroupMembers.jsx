import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { produce } from 'immer'

import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'

import SubgroupMember from './SubgroupMember'

const SubgroupMembers = ({ data, onSelect }) => {
    const { t } = useTranslation()

    if (data?.loading) {
        return <LoadingSpinner style={{ margin: '16px' }} />
    }

    const { inSubgroup, notInSubgroup } = data

    if (!inSubgroup && !notInSubgroup) return null

    let mergedItems = inSubgroup.members.edges.map((item) => {
        return produce(item, (itemDraft) => {
            itemDraft.selected = true
        })
    })

    if (mergedItems.length >= inSubgroup.members.total) {
        mergedItems = mergedItems.concat(notInSubgroup.members.edges)
    }

    const userList = [].concat(mergedItems).map((member) => {
        return (
            <SubgroupMember
                key={member.user.guid}
                group={inSubgroup}
                member={member}
                editable={inSubgroup.canEdit}
                selectable
                onSelect={onSelect}
                selected={member.selected}
            />
        )
    })

    let placeholder
    if (userList.length === 0) {
        placeholder = t('entity-group.members-notfound')
    }

    return (
        <div
            style={{
                overflowY: 'auto',
                paddingTop: '8px',
            }}
        >
            {placeholder}
            {userList}
        </div>
    )
}

const Query = gql`
    query SubgroupMembers($guid: String!, $subgroupId: Int, $q: String) {
        inSubgroup: entity(guid: $guid) {
            ... on Group {
                guid
                canEdit
                members(
                    q: $q
                    offset: 0
                    limit: 99999
                    inSubgroupId: $subgroupId
                ) {
                    total
                    edges {
                        isOwner
                        roles
                        email
                        user {
                            guid
                            username
                            url
                            name
                            icon
                        }
                    }
                }
            }
        }
        notInSubgroup: entity(guid: $guid) {
            ... on Group {
                guid
                canEdit
                members(
                    q: $q
                    offset: 0
                    limit: 99999
                    notInSubgroupId: $subgroupId
                ) {
                    total
                    edges {
                        isOwner
                        roles
                        email
                        user {
                            guid
                            username
                            url
                            name
                            icon
                        }
                    }
                }
            }
        }
    }
`

const Settings = {
    options: ({ containerGuid, subgroup: { id: subgroupId }, q }) => {
        return {
            variables: {
                guid: containerGuid,
                subgroupId,
                q,
            },
        }
    },
}

export default graphql(Query, Settings)(SubgroupMembers)
