import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { Set } from 'immutable'

import Button from 'js/components/Button/Button'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import { H4 } from 'js/components/Heading'
import RadioFields from 'js/components/RadioField/RadioFields'
import Select from 'js/components/Select/Select'
import Spacer from 'js/components/Spacer/Spacer'
import Text from 'js/components/Text/Text'
import MembersListSearch from 'js/group/components/MembersListSearch'

const SendMessage = ({ mutate, entity, onClose }) => {
    const { t } = useTranslation()

    const [page, setPage] = useState('receiver')
    const [receiver, setReceiver] = useState(null)
    const handleChangeReceiver = (evt) => {
        setReceiver(evt.target.value)
    }
    const [recipients, setRecipients] = useState(new Set())
    const [subGroup, setSubGroup] = useState(null)

    const [errorList, setErrorList] = useState()

    const onSelect = (guid) => {
        if (!recipients.has(guid)) {
            setRecipients(recipients.add(guid))
        } else {
            setRecipients(recipients.delete(guid))
        }
    }

    const availableSubGroups = entity?.subgroups?.edges

    const handleSubmitReceiver = () => {
        if (receiver === 'sendToSubGroup') {
            setPage('subGroup')
        } else if (receiver === 'sendToSelectedMembers') {
            setPage('members')
        } else {
            setPage('message')
        }
    }

    const submit = async ({ subject, message, sendCopyToSender }) => {
        if (receiver === 'sendToSelectedMembers' && recipients.size === 0) {
            setErrorList({ message: ['group_mail_choose_members'] })
            return false
        }

        setErrorList(null)

        const input = {
            guid: entity.guid,
            message,
            subject:
                receiver === 'isTest'
                    ? `${t('global.test')}: ${subject}`
                    : subject,
            ...(receiver === 'sendToSelectedMembers'
                ? {
                      recipients,
                  }
                : {}),
            ...(receiver === 'sendToSubGroup'
                ? {
                      subGroup,
                  }
                : {}),
            sendToAllMembers: receiver === 'sendToAllMembers',
            sendCopyToSender,
        }

        await mutate({
            variables: {
                input,
            },
        })
            .then(() => {
                setPage('completed')
            })
            .catch((errors) => {
                console.error(errors)
                setErrorList(errors)
            })
    }

    const defaultValues = {
        subject: '',
        message: '',
        sendCopyToSender: true,
    }

    const {
        control,
        handleSubmit,
        formState: { errors, isValid, isSubmitting },
    } = useForm({ mode: 'onChange', defaultValues })

    if (page === 'completed') {
        return (
            <>
                <Text as="p">{t('global.message-sent')}</Text>
                <Flexer mt>
                    <Button
                        size="normal"
                        variant="primary"
                        onClick={() =>
                            receiver !== 'isTest'
                                ? onClose()
                                : setPage('receiver')
                        }
                    >
                        {t('action.ok')}
                    </Button>
                </Flexer>
            </>
        )
    } else if (page === 'receiver') {
        return (
            <>
                <Text as="p" style={{ marginBottom: '8px' }}>
                    {t('entity-group.message-members')}
                </Text>
                <RadioFields
                    name="setReceiver"
                    size="normal"
                    legend={t('global.type')}
                    options={[
                        {
                            label: t('entity-group.send-message-all-members'),
                            value: 'sendToAllMembers',
                        },
                        {
                            label: t('entity-group.specific-members'),
                            value: 'sendToSelectedMembers',
                        },
                        ...(entity?.subgroups?.edges.length > 0
                            ? [
                                  {
                                      label: t('entity-group.subgroup'),
                                      value: 'sendToSubGroup',
                                  },
                              ]
                            : []),
                        {
                            label: t('global.send-message-as-test'),
                            value: 'isTest',
                        },
                    ]}
                    value={receiver}
                    onChange={handleChangeReceiver}
                />
                <Flexer mt>
                    <Button size="normal" variant="tertiary" onClick={onClose}>
                        {t('action.cancel')}
                    </Button>
                    <Button
                        variant="primary"
                        size="normal"
                        type="button"
                        disabled={!receiver}
                        onClick={handleSubmitReceiver}
                    >
                        {t('action.next')}
                    </Button>
                </Flexer>
            </>
        )
    } else if (page === 'members') {
        return (
            <>
                <H4
                    style={{
                        marginBottom: '8px',
                    }}
                >
                    {t('entity-group.specific-members')}
                    {recipients.size > 0 ? ` (${recipients.size})` : null}
                </H4>
                <MembersListSearch
                    containerGuid={entity.guid}
                    onSelect={onSelect}
                    selected={recipients}
                />
                <Flexer mt>
                    <Button
                        size="normal"
                        variant="tertiary"
                        onClick={() => setPage('receiver')}
                    >
                        {t('action.back')}
                    </Button>
                    <Button
                        variant="primary"
                        size="normal"
                        type="button"
                        disabled={recipients.size === 0}
                        onClick={() => setPage('message')}
                    >
                        {t('action.next')}
                    </Button>
                </Flexer>
            </>
        )
    } else if (page === 'subGroup') {
        return (
            <>
                <H4
                    id="subGroup-label"
                    style={{
                        marginBottom: '8px',
                    }}
                >
                    {t('entity-group.subgroup')}
                </H4>
                <Select
                    name="subGroup"
                    options={availableSubGroups.map((subGroup) => ({
                        label: subGroup.name,
                        value: subGroup.id,
                    }))}
                    size="small"
                    value={subGroup}
                    onChange={setSubGroup}
                />
                <Flexer mt>
                    <Button
                        size="normal"
                        variant="tertiary"
                        onClick={() => setPage('receiver')}
                    >
                        {t('action.back')}
                    </Button>
                    <Button
                        variant="primary"
                        size="normal"
                        type="button"
                        disabled={!subGroup}
                        onClick={() => setPage('message')}
                    >
                        {t('action.next')}
                    </Button>
                </Flexer>
            </>
        )
    } else if (page === 'message') {
        let sendMessageHelper = ''
        switch (receiver) {
            case 'sendToAllMembers':
                sendMessageHelper = t('entity-group.message-all-members')
                break
            case 'sendToSelectedMembers':
                sendMessageHelper = t('entity-group.message-nr-members', {
                    count: recipients.size,
                })
                break
            case 'sendToSubGroup':
                sendMessageHelper = t('entity-group.message-subgroup', {
                    group: availableSubGroups?.find(
                        (sub) => sub.id === subGroup,
                    ).name,
                })
                break
            case 'isTest':
                sendMessageHelper = t('entity-group.message-test')
                break
        }

        return (
            <>
                <Spacer
                    as="form"
                    spacing="small"
                    onSubmit={handleSubmit(submit)}
                    style={{
                        display: 'flex',
                        flexDirection: 'column',
                        overflow: 'hidden',
                        paddingTop: '6px',
                    }}
                >
                    <Text as="p">{sendMessageHelper}</Text>
                    <FormItem
                        control={control}
                        type="text"
                        name="subject"
                        label={t('global.subject')}
                        required
                        errors={errors}
                    />
                    <FormItem
                        control={control}
                        type="rich"
                        name="message"
                        placeholder={t('form.write-message')}
                        options={{
                            textStyle: true,
                        }}
                        required
                        errors={errors}
                        contentType="html"
                        style={{ overflowY: 'auto' }}
                    />

                    {receiver !== 'isTest' && (
                        <FormItem
                            type="checkbox"
                            control={control}
                            name="sendCopyToSender"
                            label={t('global.send-message-copy')}
                        />
                    )}

                    <Errors errors={errorList} />

                    <Flexer>
                        <Button
                            variant="tertiary"
                            size="normal"
                            type="button"
                            onClick={() =>
                                receiver === 'sendToSelectedMembers'
                                    ? setPage('members')
                                    : receiver === 'sendToSubGroup'
                                      ? setPage('subGroup')
                                      : setPage('receiver')
                            }
                        >
                            {t('action.back')}
                        </Button>
                        <Button
                            variant="primary"
                            size="normal"
                            type="submit"
                            disabled={!isValid}
                            loading={isSubmitting}
                        >
                            {t('action.send')}
                        </Button>
                    </Flexer>
                </Spacer>
            </>
        )
    }
}

const Mutation = gql`
    mutation SendMessageModal($input: sendMessageToGroupInput!) {
        sendMessageToGroup(input: $input) {
            group {
                ... on Group {
                    guid
                    subgroups {
                        edges {
                            id
                            name
                        }
                    }
                }
            }
        }
    }
`

export default graphql(Mutation)(SendMessage)
