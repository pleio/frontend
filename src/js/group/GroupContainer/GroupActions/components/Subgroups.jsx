import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'

import SubgroupAdd from './SubgroupAdd'
import SubgroupItem from './SubgroupItem'

const Subgroups = ({ data, containerGuid }) => {
    const { t } = useTranslation()

    return (
        <>
            <SubgroupAdd groupGuid={containerGuid} />
            {data.loading ? (
                <LoadingSpinner />
            ) : (
                <div style={{ overflowY: 'auto' }}>
                    {data?.entity.subgroups.length === 0
                        ? t('entity-group.subgroup-notfound')
                        : data?.entity.subgroups.edges.map((subgroup) => (
                              <SubgroupItem
                                  key={subgroup.id}
                                  entity={subgroup}
                                  groupGuid={data?.entity.guid}
                              />
                          ))}
                </div>
            )}
        </>
    )
}

const QUERY = gql`
    query GroupSubgroups($containerGuid: String!) {
        entity(guid: $containerGuid) {
            guid
            ... on Group {
                subgroups {
                    total
                    edges {
                        id
                        name
                        members {
                            guid
                        }
                    }
                }
            }
        }
    }
`
export default graphql(QUERY)(Subgroups)
