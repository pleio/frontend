import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import compose from 'lodash.flowright'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import UserLink from 'js/components/UserLink'

const Wrapper = styled.ul`
    .MembershipRequestUser {
        display: flex;
        align-items: center;

        &:not(:last-child) {
            border-bottom: 1px solid ${(p) => p.theme.color.grey[20]};
        }
    }
`

const MembershipRequests = ({ mutateAccept, mutateReject, data }) => {
    const { t } = useTranslation()
    const [errors, setErrors] = useState()

    const onAccept = (user) => {
        setErrors()

        mutateAccept({
            variables: {
                input: {
                    userGuid: user.guid,
                    groupGuid: entity.guid,
                },
            },
            refetchQueries: ['GroupMembers', 'MembersList'],
        }).catch((errors) => {
            setErrors(errors)
            console.error(errors)
        })
    }

    const onReject = (user) => {
        if (!confirm(t('entity-group.remove-request-confirm'))) {
            return
        }

        setErrors()

        mutateReject({
            variables: {
                input: {
                    userGuid: user.guid,
                    groupGuid: entity.guid,
                },
            },
        }).catch((errors) => {
            setErrors(errors)
            console.error(errors)
        })
    }

    if (!data.entity) return null
    const { entity } = data

    return (
        <Wrapper>
            <Errors errors={errors} />
            {entity.membershipRequests.edges.length === 0
                ? t('entity-group.requests-notfound')
                : entity.membershipRequests.edges.map((user) => {
                      const accept = () => onAccept(user)
                      const reject = () => onReject(user)

                      return (
                          <li className="MembershipRequestUser" key={user.guid}>
                              <UserLink entity={user} />
                              <Flexer style={{ marginLeft: 'auto' }}>
                                  <Button
                                      size="normal"
                                      variant="primary"
                                      onClick={accept}
                                  >
                                      {t('action.accept')}
                                  </Button>
                                  <Button
                                      size="normal"
                                      variant="tertiary"
                                      onClick={reject}
                                  >
                                      {t('action.reject')}
                                  </Button>
                              </Flexer>
                          </li>
                      )
                  })}
        </Wrapper>
    )
}

const Query = gql`
    query MembershipRequestsList($guid: String!) {
        entity(guid: $guid) {
            guid
            ... on Group {
                membershipRequests {
                    total
                    edges {
                        guid
                        username
                        name
                        icon
                        url
                    }
                }
            }
        }
    }
`

const Settings = {
    options: (ownProps) => {
        return {
            variables: {
                guid: ownProps.guid,
                q: ownProps.q,
            },
        }
    },
}

const AcceptMutation = gql`
    mutation MembershipRequestsList($input: acceptMembershipRequestInput!) {
        acceptMembershipRequest(input: $input) {
            group {
                guid
                name
                membershipRequests {
                    total
                    edges {
                        guid
                        username
                        name
                        icon
                    }
                }
            }
        }
    }
`

const RejectMutation = gql`
    mutation MembershipRequestsList($input: rejectMembershipRequestInput!) {
        rejectMembershipRequest(input: $input) {
            group {
                guid
                name
                membershipRequests {
                    total
                    edges {
                        guid
                        username
                        name
                        icon
                    }
                }
            }
        }
    }
`

export default compose(
    graphql(Query, Settings),
    graphql(AcceptMutation, { name: 'mutateAccept' }),
    graphql(RejectMutation, { name: 'mutateReject' }),
)(MembershipRequests)
