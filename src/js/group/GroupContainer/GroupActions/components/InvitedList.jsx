import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import styled from 'styled-components'

import InvitedItem from './InvitedItem'

const UserListWrapper = styled.div`
    margin-top: 8px;

    > * {
        padding: 8px 0;

        &:not(:last-child) {
            border-bottom: 1px solid ${(p) => p.theme.color.grey[20]};
        }
    }
`

const InvitedList = ({ data }) => {
    const { t } = useTranslation()

    if (!data.entity) return null

    return (
        <UserListWrapper>
            {data.entity.invited.edges.length === 0
                ? t('entity-group.invite-notfound')
                : data.entity.invited.edges.map((invite) => (
                      <InvitedItem
                          key={invite.id}
                          id={invite.id}
                          entity={
                              invite.user || {
                                  name: invite.email,
                                  email: invite.email,
                              }
                          }
                          timeCreated={invite.timeCreated}
                      />
                  ))}
        </UserListWrapper>
    )
}

const Query = gql`
    query InvitedList($guid: String!) {
        entity(guid: $guid) {
            guid
            ... on Group {
                invited(offset: 0, limit: 999) {
                    total
                    edges {
                        id
                        invited
                        timeCreated
                        email
                        user {
                            guid
                            url
                            username
                            name
                            icon
                        }
                    }
                }
            }
        }
    }
`

const Settings = {
    options: (ownProps) => {
        return {
            variables: {
                guid: ownProps.guid,
                q: ownProps.q,
            },
        }
    },
}

export default graphql(Query, Settings)(InvitedList)
