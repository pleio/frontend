import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { showShortDate } from 'helpers/date/showDate'
import compose from 'lodash.flowright'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import Text from 'js/components/Text/Text'
import UserLink from 'js/components/UserLink'

const InvitedItem = ({
    mutateResend,
    mutateDelete,
    id,
    entity,
    timeCreated,
}) => {
    const [isSent, setIsSent] = useState(false)

    const handleResend = () => {
        return new Promise((resolve, reject) => {
            mutateResend({
                variables: {
                    input: { id },
                },
            })
                .then(() => {
                    resolve()
                    setIsSent(true)
                })
                .catch((error) => {
                    console.error(error)
                    reject(new Error(error))
                })
        })
    }

    const handleDelete = () => {
        return new Promise((resolve, reject) => {
            mutateDelete({
                variables: {
                    input: { id },
                },
                refetchQueries: ['InvitedList'],
            })
                .then(() => {
                    resolve()
                })
                .catch((error) => {
                    console.error(error)
                    reject(new Error(error))
                })
        })
    }

    const { t } = useTranslation()

    return (
        <div style={{ display: 'flex', alignItems: 'center' }}>
            <UserLink entity={entity} style={{ flexGrow: 1 }}>
                <Text size="small" variant="grey">
                    {t('entity-group.last-invited', {
                        date: showShortDate(timeCreated),
                    })}
                </Text>
            </UserLink>

            <Flexer style={{ marginLeft: 'auto' }}>
                {isSent ? (
                    <Text size="small">{t('global.invitation-sent')}</Text>
                ) : (
                    <Button
                        size="normal"
                        variant="primary"
                        onHandle={handleResend}
                    >
                        {t('entity-group.resend')}
                    </Button>
                )}
                <Button
                    size="normal"
                    variant="tertiary"
                    onHandle={handleDelete}
                >
                    {t('entity-group.withdraw-invitation')}
                </Button>
            </Flexer>
        </div>
    )
}

const ResendMutation = gql`
    mutation InvitedList($input: resendGroupInvitationInput!) {
        resendGroupInvitation(input: $input) {
            group {
                guid
                name
                invited {
                    total
                    edges {
                        id
                        invited
                        timeCreated
                        email
                        user {
                            guid
                            username
                            name
                            icon
                        }
                    }
                }
            }
        }
    }
`

const DeleteMutation = gql`
    mutation InvitedList($input: deleteGroupInvitationInput!) {
        deleteGroupInvitation(input: $input) {
            group {
                guid
                name
                invited {
                    total
                    edges {
                        id
                        invited
                        timeCreated
                        email
                        user {
                            guid
                            username
                            name
                            icon
                        }
                    }
                }
            }
        }
    }
`

export default compose(
    graphql(ResendMutation, { name: 'mutateResend' }),
    graphql(DeleteMutation, { name: 'mutateDelete' }),
)(InvitedItem)
