import React, { useState } from 'react'
import { Trans, useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import Button from 'js/components/Button/Button'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'

const LeaveGroup = ({ mutate, onClose, entity }) => {
    const navigate = useNavigate()
    const [errors, setErrors] = useState([])

    const handleSubmit = () => {
        const input = {
            guid: entity.guid,
        }

        mutate({
            variables: {
                input,
            },
        })
            .then(({ data }) => {
                onClose()
                navigate(data.leaveGroup.group.url)
            })
            .catch((errors) => {
                setErrors(errors)
            })
    }

    const { t } = useTranslation()

    if (entity.isOwner) {
        return (
            <>
                {t('entity-group.leave-group-owner')}
                <Flexer mt>
                    <Button
                        size="normal"
                        variant="primary"
                        type="button"
                        onClick={onClose}
                    >
                        {t('action.ok')}
                    </Button>
                </Flexer>
            </>
        )
    } else {
        return (
            <>
                <Trans i18nKey="entity-group.leave-confirm">
                    Really leave group
                    <strong>{{ name: entity.name }}</strong>?
                </Trans>
                <Errors errors={errors} />
                <Flexer mt>
                    <Button
                        size="normal"
                        variant="tertiary"
                        type="button"
                        onClick={onClose}
                    >
                        {t('action.no-back')}
                    </Button>
                    <Button
                        size="normal"
                        variant="primary"
                        onClick={handleSubmit}
                    >
                        {t('action.yes-confirm')}
                    </Button>
                </Flexer>
            </>
        )
    }
}

const Mutation = gql`
    mutation LeaveGroupModal($input: leaveGroupInput!) {
        leaveGroup(input: $input) {
            group {
                guid
                ... on Group {
                    canEdit
                    url
                    membership
                    isClosed
                    memberCount
                    members(limit: 3) {
                        edges {
                            user {
                                guid
                                username
                                url
                                name
                                icon
                            }
                        }
                    }
                }
            }
        }
    }
`

export default graphql(Mutation)(LeaveGroup)
