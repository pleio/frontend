import React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import Button from 'js/components/Button/Button'
import FormItem from 'js/components/Form/FormItem'
import Setting from 'js/components/Setting'
import Spacer from 'js/components/Spacer/Spacer'

const SubgroupAdd = ({ groupGuid, mutate }) => {
    const submit = async ({ name }) => {
        await mutate({
            variables: {
                input: {
                    name,
                    members: [],
                    groupGuid,
                },
            },
            refetchQueries: ['GroupSubgroups'],
        })
            .then(() => {
                reset()
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    const { t } = useTranslation()

    const {
        control,
        handleSubmit,
        reset,
        formState: { isValid, isSubmitting },
    } = useForm({ mode: 'onChange' })

    return (
        <Spacer as="form" spacing="small" onSubmit={handleSubmit(submit)}>
            <Setting subtitle="entity-group.subgroup-create" inputWidth="full">
                <FormItem
                    control={control}
                    type="text"
                    name="name"
                    label={t('global.name')}
                    required
                />
            </Setting>
            <Button
                size="normal"
                variant="primary"
                type="submit"
                disabled={!isValid}
                loading={isSubmitting}
            >
                {t('action.create')}
            </Button>
        </Spacer>
    )
}

const Mutation = gql`
    mutation SubgroupsModal($input: addSubgroupInput!) {
        addSubgroup(input: $input) {
            success
        }
    }
`

export default graphql(Mutation)(SubgroupAdd)
