import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { useDebounce } from 'helpers'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import SearchBar from 'js/components/SearchBar/SearchBar'

import SubgroupMembers from './SubgroupMembers'

const SubgroupEdit = ({ containerGuid, subgroup, mutate, onClose }) => {
    const { t } = useTranslation()

    const [search, setSearch] = useState('')
    const [selected, setSelected] = useState(
        subgroup.members.map((member) => member.guid),
    )

    const defaultValues = {
        name: subgroup.name,
    }

    const {
        control,
        handleSubmit,
        formState: { errors, isValid, isSubmitting },
    } = useForm({ mode: 'onChange', defaultValues })

    if (!subgroup) return null

    const onChange = (e) => {
        setSearch(e.target.value)
    }

    const onSelect = (guid) => {
        if (selected.indexOf(guid) === -1) {
            setSelected((arr) => [...arr, guid])
        } else {
            setSelected((arr) => arr.filter((val) => val !== guid))
        }
    }

    const submit = ({ name }) => {
        setSearch('')

        mutate({
            variables: {
                input: {
                    id: subgroup.id,
                    name,
                    members: [...selected],
                },
            },
            refetchQueries: [
                'GroupSubgroups',
                'SubgroupMembers',
                'AccessField',
            ],
        })
            .then(() => {
                onClose()
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    return (
        <>
            <FormItem
                control={control}
                type="text"
                name="name"
                label={t('global.name')}
                required
                errors={errors}
                style={{ marginBottom: '16px' }}
            />
            <SearchBar
                name="user-list-search-subgroup"
                value={search}
                label={t('entity-group.search-placeholder')}
                onChange={onChange}
            />
            {subgroup && (
                <SubgroupMembers
                    containerGuid={containerGuid}
                    subgroup={subgroup}
                    q={useDebounce(search, 200)}
                    onSelect={onSelect}
                />
            )}
            <Flexer>
                <Button size="normal" variant="tertiary" onClick={onClose}>
                    {t('action.cancel')}
                </Button>
                <Button
                    size="normal"
                    variant="primary"
                    disabled={!isValid}
                    loading={isSubmitting}
                    onClick={handleSubmit(submit)}
                >
                    {t('action.save')}
                </Button>
            </Flexer>
        </>
    )
}

const Mutation = gql`
    mutation SubgroupEdit($input: editSubgroupInput!) {
        editSubgroup(input: $input) {
            success
        }
    }
`

export default graphql(Mutation)(SubgroupEdit)
