import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'
import { enableMapSet, produce } from 'immer'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import Checkbox from 'js/components/Checkbox/Checkbox'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import IconButton from 'js/components/IconButton/IconButton'
import Spacer from 'js/components/Spacer/Spacer'
import Text from 'js/components/Text/Text'
import UserLink from 'js/components/UserLink'

import RemoveIcon from 'icons/cross.svg'

import InviteAutoComplete from './InviteAutoComplete'

const UserListWrapper = styled.div`
    margin-top: 8px;

    > * {
        padding: 8px 0;

        &:not(:last-child) {
            border-bottom: 1px solid ${(p) => p.theme.color.grey[20]};
        }
    }
`

enableMapSet()

const InviteForm = ({ guid, viewer, setIsInvited }) => {
    const { t } = useTranslation()

    const [users, setUsers] = useState([])
    const [directAdd, setDirectAdd] = useState(false)
    const [hasEmail, setHasEmail] = useState(false)
    const [mutateErrors, setMutateErrors] = useState()

    const [mutate] = useMutation(INVITE_TO_GROUP)

    const select = (user) => {
        setUsers(
            produce((newState) => {
                newState.push(user)
            }),
        )
    }

    const updateUser = (user, index, role) => {
        setUsers(
            produce((newState) => {
                newState[index] = { ...user, role }
            }),
        )
    }

    const deselect = (user) => {
        setUsers(
            produce((newState) => {
                if (hasEmail && !newState.some((user) => !user.guid)) {
                    setHasEmail(false)
                }

                return newState.filter((u) => u.email !== user.email)
            }),
        )
    }

    const submit = () => {
        return new Promise((resolve, reject) => {
            const input = {
                guid,
                directAdd,
                users: [...users].map(({ guid, email, role }) => ({
                    guid,
                    email,
                    ...(role && { role }),
                })),
            }

            mutate({
                variables: {
                    input,
                },
                refetchQueries: ['GroupContainer', 'GroupMembers'],
            })
                .then(() => {
                    resolve()
                    setIsInvited(true)
                })
                .catch((errors) => {
                    console.error(errors)
                    setMutateErrors(errors)
                    reject(errors)
                })
        })
    }

    const updateDirectAdd = (evt) => {
        setDirectAdd(evt.target.checked)
    }

    return (
        <div style={{ paddingTop: '16px' }}>
            <Spacer>
                <div>
                    <InviteAutoComplete
                        guid={guid}
                        users={users}
                        onSelect={select}
                    />
                    {users.length > 0 && (
                        <UserListWrapper>
                            {[...users].map((user, i) => {
                                const isUser = !!user.guid

                                return (
                                    <Flexer
                                        key={i}
                                        style={{
                                            alignItems: 'center',
                                            justifyContent: 'space-between',
                                        }}
                                    >
                                        <UserLink
                                            entity={user}
                                            isEmail={!isUser}
                                        >
                                            {isUser && (
                                                <Text
                                                    size="small"
                                                    variant="grey"
                                                >
                                                    {user.email}
                                                </Text>
                                            )}
                                        </UserLink>

                                        <Flexer>
                                            {viewer.isAdmin && (
                                                <Checkbox
                                                    name="role"
                                                    label={t(
                                                        'entity-group.group-admin',
                                                    )}
                                                    checked={
                                                        user.role === 'admin'
                                                    }
                                                    onChange={({ target }) => {
                                                        updateUser(
                                                            user,
                                                            i,
                                                            target.checked
                                                                ? 'admin'
                                                                : '',
                                                        )
                                                    }}
                                                    size="small"
                                                />
                                            )}

                                            <IconButton
                                                size="large"
                                                variant="secondary"
                                                tooltip={t('action.remove')}
                                                onClick={() => deselect(user)}
                                            >
                                                <RemoveIcon />
                                            </IconButton>
                                        </Flexer>
                                    </Flexer>
                                )
                            })}
                        </UserListWrapper>
                    )}
                    <div style={{ marginTop: '8px' }} />
                    {viewer.isAdmin && (
                        <Checkbox
                            name="directAdd"
                            label={t('entity-group.auto-add-user')}
                            checked={directAdd}
                            onChange={updateDirectAdd}
                            size="small"
                        />
                    )}
                </div>

                {mutateErrors && <Errors errors={mutateErrors} />}

                <Flexer>
                    <Button
                        size="normal"
                        variant="primary"
                        disabled={users.length === 0}
                        onHandle={submit}
                    >
                        {t('action.invite')}
                    </Button>
                </Flexer>
            </Spacer>
        </div>
    )
}

const INVITE_TO_GROUP = gql`
    mutation InviteItem($input: inviteToGroupInput!) {
        inviteToGroup(input: $input) {
            group {
                ... on Group {
                    guid
                    invited(offset: 0, limit: 999) {
                        total
                        edges {
                            id
                            invited
                            timeCreated
                            email
                            user {
                                guid
                                url
                                username
                                name
                                icon
                            }
                        }
                    }
                }
            }
        }
    }
`

export default InviteForm
