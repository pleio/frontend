import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import TabMenu from 'js/components/TabMenu'
import TabPage from 'js/components/TabPage'

import InvitedList from './InvitedList'
import InviteForm from './InviteForm'

const InviteMembers = ({ guid, viewer }) => {
    const { t } = useTranslation()

    const [tab, setTab] = useState('invite')

    const [isInvited, setIsInvited] = useState(false)

    const tabOptions = [
        {
            label: t('action.invite'),
            onClick: () => setTab('invite'),
            isActive: tab === 'invite',
            id: 'tab-invite',
            ariaControls: 'tabpanel-invite',
        },
        {
            label: t('global.invited'),
            onClick: () => setTab('invited'),
            isActive: tab === 'invited',
            id: 'tab-invited',
            ariaControls: 'tabpanel-invited',
        },
    ]

    if (isInvited) {
        return (
            <div>
                {t('entity-group.invites-sent')}
                <Flexer mt>
                    <Button
                        size="normal"
                        variant="primary"
                        onClick={() => {
                            setIsInvited(false)
                            setTab('invite')
                        }}
                    >
                        {t('action.ok')}
                    </Button>
                </Flexer>
            </div>
        )
    }

    return (
        <>
            <TabMenu
                label={t('global.invite-people')}
                options={tabOptions}
                showBorder
                edgePadding
                edgeMargin
            />
            <TabPage
                id={`tabpanel-${tab}`}
                aria-labelledby={`tab-${tab}`}
                visible
            >
                {tab === 'invite' && (
                    <InviteForm
                        guid={guid}
                        viewer={viewer}
                        setIsInvited={setIsInvited}
                    />
                )}
                {tab === 'invited' && <InvitedList guid={guid} />}
            </TabPage>
        </>
    )
}

export default InviteMembers
