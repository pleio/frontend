import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import DropdownButton from 'js/components/DropdownButton/DropdownButton'
import Errors from 'js/components/Errors'
import Modal from 'js/components/Modal/Modal'

import SubgroupEdit from './SubgroupEdit'

const Wrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 8px 0;
    font-weight: ${(p) => p.theme.font.size.normal};
    line-height: ${(p) => p.theme.font.lineHeight.normal};

    &:not(:last-child) {
        border-bottom: 1px solid ${(p) => p.theme.color.grey[20]};
    }

    .SubgroupItem__name {
        font-weight: ${(p) => p.theme.font.weight.semibold};
    }
`

const SubgroupItem = ({ mutate, entity, groupGuid }) => {
    const [showSubgroupEditModal, setShowSubgroupEditModal] = useState(false)
    const toggleSubgroupEditModal = () =>
        setShowSubgroupEditModal(!showSubgroupEditModal)

    const [errors, setErrors] = useState([])
    const { t } = useTranslation()

    if (!entity) return null

    const handleDelete = () => {
        if (!confirm(t('entity-group.subgroup-delete'))) return

        const input = {
            id: entity.id,
        }

        setErrors([])

        mutate({
            variables: {
                input,
            },
            refetchQueries: ['GroupSubgroups', 'AccessField'],
        }).catch((errors) => {
            setErrors(errors)
        })
    }

    const options = [
        {
            onClick: toggleSubgroupEditModal,
            name: t('action.edit'),
        },
        {
            onClick: handleDelete,
            name: t('action.delete'),
        },
    ]

    return (
        <>
            <Wrapper>
                <Errors errors={errors} />
                <div>
                    <span className="SubgroupItem__name">{entity.name}</span> (
                    {entity.members.length})
                </div>
                <DropdownButton options={options} showArrow>
                    <Button size="normal" variant="secondary">
                        <span
                            style={{
                                marginRight: '6px',
                            }}
                        >
                            {t('global.actions')}
                        </span>
                    </Button>
                </DropdownButton>
            </Wrapper>

            <Modal
                size="normal"
                title={entity.name}
                showCloseButton
                isVisible={showSubgroupEditModal}
                onClose={toggleSubgroupEditModal}
                containHeight
            >
                <SubgroupEdit
                    containerGuid={groupGuid}
                    onClose={toggleSubgroupEditModal}
                    subgroup={entity}
                />
            </Modal>
        </>
    )
}

const Mutation = gql`
    mutation SubgroupItem($input: deleteSubgroupInput!) {
        deleteSubgroup(input: $input) {
            success
        }
    }
`

export default graphql(Mutation)(SubgroupItem)
