import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useLazyQuery } from '@apollo/client'
import { useDebounce } from 'helpers'
import styled from 'styled-components'

import IconButton from 'js/components/IconButton/IconButton'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Popover from 'js/components/Popover/Popover'
import SearchBar from 'js/components/SearchBar/SearchBar'
import Text from 'js/components/Text/Text'
import UserLink from 'js/components/UserLink'
import { REGEX } from 'js/lib/constants'

import AddIcon from 'icons/add.svg'

const UserListWrapper = styled.div`
    > * {
        padding: 8px 0;

        &:not(:last-child) {
            border-bottom: 1px solid ${(p) => p.theme.color.grey[20]};
        }
    }
`

const Item = ({ entity, onSelect }) => {
    const { t } = useTranslation()

    return (
        <div
            style={{
                display: 'flex',
                alignItems: 'center',
                padding: '8px',
            }}
        >
            <UserLink entity={entity} isEmail={!entity?.guid}>
                {entity.guid && (
                    <Text size="small" variant="grey">
                        {entity.email}
                    </Text>
                )}
            </UserLink>
            <IconButton
                size="large"
                variant="secondary"
                aria-label={t('action.add')}
                onClick={() => onSelect(entity)}
                style={{ marginLeft: 'auto' }}
            >
                <AddIcon />
            </IconButton>
        </div>
    )
}

const InviteAutoComplete = ({ guid, users, onSelect }) => {
    const { t } = useTranslation()

    const [getUsers, { loading, data }] = useLazyQuery(GET_INVITE_USERS)

    const [inputValue, setInputValue] = useState('')
    const q = useDebounce(inputValue, 200)

    useEffect(() => {
        getUsers({
            variables: {
                q,
                guid,
            },
        })
    }, [guid, q, getUsers])

    const onChange = (evt) => {
        setInputValue(evt.target.value)
    }

    const onKeyDown = (evt) => {
        if (evt.key === 'Escape') {
            evt.stopPropagation()
            setInputValue('')
        }
    }

    const invites = data?.entity.invite.edges.filter(
        (invite) => !users.find((u) => u.guid === invite.user.guid),
    )

    let email
    if (q && REGEX.email.test(q) && !users.find((u) => u.email === q)) {
        email = <Item entity={{ name: q, email: q }} onSelect={onSelect} />
    }

    const handleShow = ({ popper, reference }) => {
        popper.style.width = reference.getBoundingClientRect().width + 'px'
    }

    return (
        <div style={{ position: 'relative' }}>
            <Popover
                content={
                    loading ? (
                        <LoadingSpinner style={{ margin: '16px' }} />
                    ) : !loading && data ? (
                        <div style={{ maxHeight: '500px', overflowY: 'auto' }}>
                            {invites.length === 0 && !email ? (
                                <Text
                                    size="small"
                                    variant="grey"
                                    textAlign="center"
                                    style={{ padding: '16px' }}
                                >
                                    {t('global.no-results')}
                                </Text>
                            ) : (
                                <UserListWrapper>
                                    {invites.map((invite) => (
                                        <Item
                                            key={`${invite.user.guid}-invite`}
                                            entity={invite.user}
                                            onSelect={onSelect}
                                        />
                                    ))}
                                    {email}
                                </UserListWrapper>
                            )}
                        </div>
                    ) : null
                }
                trigger="manual"
                visible={!!q}
                onShow={handleShow}
                maxWidth="none"
                placement="bottom"
                offset={0}
                arrow={false}
            >
                <SearchBar
                    name="group-invite-autocomplete"
                    value={inputValue}
                    onChange={onChange}
                    onKeyDown={onKeyDown}
                    label={`${t('global.search-user-or-email')}..`}
                />
            </Popover>
        </div>
    )
}

const GET_INVITE_USERS = gql`
    query InviteAutoCompleteList($q: String, $guid: String!) {
        entity(guid: $guid) {
            guid
            ... on Group {
                invite(q: $q, offset: 0, limit: 9999) {
                    edges {
                        invited
                        user {
                            guid
                            url
                            username
                            name
                            icon
                            email
                        }
                    }
                }
            }
        }
    }
`

export default InviteAutoComplete
