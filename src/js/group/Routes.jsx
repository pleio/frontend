import React from 'react'
import { Route, Routes } from 'react-router-dom'

import BlogGroupList from 'js/blog/GroupList'
import BlogGroupItem from 'js/blog/Item'
import NotFound from 'js/core/NotFound'
import DiscussionsGroupList from 'js/discussions/GroupList'
import DiscussionsGroupItem from 'js/discussions/Item'
import EventsGroupList from 'js/events/GroupList'
import EventsGroupItem from 'js/events/Item'
import Members from 'js/group/Members/Members'
import Pages from 'js/group/Pages'
import StatusUpdateItem from 'js/group/StatusUpdate/Item'
import NewsGroupList from 'js/news/GroupList'
import NewsGroupItem from 'js/news/Item'
import Page from 'js/page/Item'
import LegacyCmsPageRedirect from 'js/page/LegacyCmsPageRedirect'
import PodcastGroupEpisode from 'js/podcast/Episode'
import PodcastGroupItem from 'js/podcast/Item'
import PodcastGroupList from 'js/podcast/PodcastGroupList'
import QuestionsGroupList from 'js/questions/GroupList'
import QuestionsGroupItem from 'js/questions/Item'
import Search from 'js/search/Search'
import WikiGroupList from 'js/wiki/GroupList'
import WikiGroupItem from 'js/wiki/Item'

import FilesGroupList from '../files/GroupList'
import TasksGroupList from '../tasks/GroupList'

import GroupContainer from './GroupContainer/GroupContainer'
import Invitations from './Invitations'
import ListCreator from './List/ListCreator'

const Component = () => {
    return (
        <Routes>
            <Route path="/" element={<ListCreator filter="all" />} />
            <Route path="/mine" element={<ListCreator filter="mine" />} />
            <Route path="/invitations" element={<Invitations />} />

            <Route
                path="/view/:groupGuid/:groupSlug"
                element={<GroupContainer isHomePage />}
            />

            <Route element={<GroupContainer />}>
                <Route
                    path="/view/:groupGuid/:groupSlug/blog"
                    element={<BlogGroupList />}
                />
                <Route
                    path="/view/:groupGuid/:groupSlug/questions"
                    element={<QuestionsGroupList />}
                />
                <Route
                    path="/view/:groupGuid/:groupSlug/discussion"
                    element={<DiscussionsGroupList />}
                />
                <Route
                    path="/view/:groupGuid/:groupSlug/news"
                    element={<NewsGroupList />}
                />
                <Route
                    path="/view/:groupGuid/:groupSlug/events"
                    element={<EventsGroupList />}
                />
                <Route
                    path="/view/:groupGuid/:groupSlug/files"
                    element={<FilesGroupList />}
                />
                <Route
                    path="/view/:groupGuid/:groupSlug/files/:containerGuid"
                    element={<FilesGroupList />}
                />
                <Route
                    path="/view/:groupGuid/:groupSlug/wiki"
                    element={<WikiGroupList />}
                />
                <Route
                    path="/view/:groupGuid/:groupSlug/podcasts"
                    element={<PodcastGroupList />}
                />
                <Route
                    path="/view/:groupGuid/:groupSlug/tasks"
                    element={<TasksGroupList />}
                />
                <Route
                    path="/view/:groupGuid/:groupSlug/members"
                    element={<Members />}
                />
                <Route
                    path="/view/:groupGuid/:groupSlug/pages"
                    element={<Pages />}
                />
                <Route
                    path="/view/:groupGuid/:groupSlug/page/view/:guid/:slug"
                    element={<Page />}
                />
                <Route
                    path="/view/:groupGuid/:groupSlug/cms/view/*"
                    element={<LegacyCmsPageRedirect />}
                />
            </Route>

            <Route element={<GroupContainer isDetailPage />}>
                <Route
                    path="/view/:groupGuid/:groupSlug/update/view/:guid"
                    element={<StatusUpdateItem />}
                />
                <Route
                    path="/view/:groupGuid/:groupSlug/blog/view/:guid/:slug"
                    element={<BlogGroupItem />}
                />
                <Route
                    path="/view/:groupGuid/:groupSlug/events/view/:guid/:slug"
                    element={<EventsGroupItem />}
                />
                <Route
                    path="/view/:groupGuid/:groupSlug/discussion/view/:guid/:slug"
                    element={<DiscussionsGroupItem />}
                />
                <Route
                    path="/view/:groupGuid/:groupSlug/news/view/:guid/:slug"
                    element={<NewsGroupItem />}
                />
                <Route
                    path="/view/:groupGuid/:groupSlug/podcasts/view/:guid/:slug"
                    element={<PodcastGroupItem />}
                />
                <Route
                    path="/view/:groupGuid/:groupSlug/podcasts/episodes/view/:guid/:slug"
                    element={<PodcastGroupEpisode />}
                />
                <Route
                    path="/view/:groupGuid/:groupSlug/questions/view/:guid/:slug"
                    element={<QuestionsGroupItem />}
                />
                <Route
                    path="/view/:groupGuid/:groupSlug/wiki/view/:guid/:slug"
                    element={<WikiGroupItem />}
                />
                <Route
                    path="/view/:groupGuid/:groupSlug/wiki/view/:containerGuid/:containerSlug/:guid"
                    element={<WikiGroupItem />}
                />
                <Route
                    path="/view/:groupGuid/:groupSlug/search"
                    element={<Search />}
                />
            </Route>

            <Route path="*" element={<NotFound />} />
        </Routes>
    )
}
export default Component
