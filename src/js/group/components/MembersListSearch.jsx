import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'

import FetchMore from 'js/components/FetchMore'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import SearchBar from 'js/components/SearchBar/SearchBar'

import MemberItem from './MemberItem'

const MembersListSearch = ({ containerGuid, selected, onSelect }) => {
    const [searchInput, setSearchInput] = useState('')
    const onChangeSearchInput = (evt) => {
        setSearchInput(evt.target.value)
    }

    const [q, setQ] = useState('')
    const onSubmitSearchInput = (evt) => {
        evt.preventDefault()
        setQ(searchInput)
    }

    const [queryLimit, setQueryLimit] = useState(50)
    const { loading, data, fetchMore } = useQuery(Query, {
        variables: {
            groupGuid: containerGuid,
            q,
            offset: 0,
            limit: queryLimit,
        },
    })

    const { t } = useTranslation()

    return (
        <>
            <form method="GET" onSubmit={onSubmitSearchInput}>
                <SearchBar
                    name="send-message-members-search"
                    value={searchInput}
                    label={t('entity-group.search-placeholder')}
                    onChange={onChangeSearchInput}
                    size="large"
                />
            </form>

            {loading ? (
                <LoadingSpinner />
            ) : data.members ? (
                <FetchMore
                    edges={data.members.edges}
                    getMoreResults={(data) => data.members.edges}
                    fetchMore={fetchMore}
                    fetchType="scroll"
                    isScrollableBox
                    fetchCount={50}
                    setLimit={setQueryLimit}
                    maxLimit={data.members.total}
                >
                    {data.members.edges.map((entity) => (
                        <MemberItem
                            key={entity.user.guid}
                            entity={entity}
                            onSelect={onSelect}
                            selected={selected}
                        />
                    ))}
                </FetchMore>
            ) : null}
        </>
    )
}

const Query = gql`
    query GroupMembers(
        $groupGuid: String!
        $q: String
        $offset: Int
        $limit: Int
    ) {
        members(groupGuid: $groupGuid, q: $q, offset: $offset, limit: $limit) {
            edges {
                roles
                isOwner
                email
                user {
                    guid
                    username
                    url
                    name
                    icon
                    roles
                }
            }
            total
        }
    }
`

export default MembersListSearch
