import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import Card, { CardContent } from 'js/components/Card/Card'
import IconButton from 'js/components/IconButton/IconButton'
import TiptapView from 'js/components/Tiptap/TiptapView'
import EditWidgetPlaceholder from 'js/page/Widget/components/components/EditWidgetPlaceholder'

import CrossIcon from 'icons/cross.svg'

const Wrapper = styled(Card)`
    position: relative;

    .IntroductionCloseButton {
        position: absolute;
        top: 12px;
        right: 16px;
        background-color: transparent;
    }
`

const GroupIntroduction = ({ editMode, group }) => {
    const [isVisible, setIsVisible] = useState(true)

    const { t } = useTranslation()

    const introduction = group?.introduction

    const handleClose = () => setIsVisible(!isVisible)

    if (!introduction || !isVisible) {
        if (!editMode) return null
        return (
            <EditWidgetPlaceholder
                title={t('widget-introduction.title')}
                helper={t('widget-introduction.helper')}
            />
        )
    }

    return (
        <Wrapper>
            <CardContent>
                <TiptapView content={introduction} />
            </CardContent>
            {!editMode && (
                <IconButton
                    size="normal"
                    variant="secondary"
                    onClick={handleClose}
                    className="IntroductionCloseButton"
                    aria-label={t('entity-group.hide-introduction')}
                >
                    <CrossIcon />
                </IconButton>
            )}
        </Wrapper>
    )
}

export default GroupIntroduction
