import React from 'react'
import { useRoleLabel } from 'helpers'
import styled from 'styled-components'

import Checkbox from 'js/components/Checkbox/Checkbox'
import UserLink from 'js/components/UserLink'

const Wrapper = styled.div`
    display: flex;
    align-items: center;
    padding: 8px 0;
`

const MemberItem = ({ entity, selected, onSelect }) => {
    const { roles, isOwner, user } = entity

    // roles is member roles (group), not user roles (site)
    const role = useRoleLabel(roles, isOwner)

    return (
        <Wrapper>
            <Checkbox
                checked={selected.has(user.guid)}
                onChange={() => onSelect(user.guid)}
                style={{ marginRight: '8px' }}
            />
            <UserLink entity={user} role={role} style={{ flexGrow: 1 }} />
        </Wrapper>
    )
}

export default MemberItem
