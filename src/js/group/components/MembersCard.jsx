import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import filterViewerFromAttendees from 'helpers/filterViewerFromAttendees'

import Card, { CardContent } from 'js/components/Card/Card'
import { H3 } from 'js/components/Heading'
import People from 'js/components/People'

const MembersCard = ({ group, viewer }) => {
    const { t } = useTranslation()

    if (!group) return null

    const { canEdit, membership, members, memberCount, menu } = group

    const canViewMembers =
        canEdit ||
        (menu.find((el) => el.id === 'members') && membership === 'joined')

    const membersList =
        members?.edges.map((membership) => membership.user) || []

    return (
        <Card
            className="GroupMembersCard" // Being used for custom CSS?
            as={canViewMembers ? Link : null}
            to={canViewMembers ? `${group.url}/members` : null}
        >
            <CardContent>
                <People
                    users={
                        membership === 'joined'
                            ? filterViewerFromAttendees(
                                  membersList,
                                  viewer?.guid,
                              )
                            : membersList
                    }
                    showCheck={membership === 'joined'}
                    checkLabel={t('entity-group.you-are-member')}
                    total={memberCount}
                    size="large"
                    style={{ marginBottom: '8px' }}
                />
                <H3 as="div" style={{ textAlign: 'center' }}>
                    {t('entity-group.nr-members', {
                        count: memberCount,
                    })}
                </H3>
            </CardContent>
        </Card>
    )
}

export default MembersCard
