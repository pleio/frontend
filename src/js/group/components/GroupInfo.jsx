import React from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import Text from 'js/components/Text/Text'
import Tooltip from 'js/components/Tooltip/Tooltip'
import humanFileSize from 'js/lib/helpers/fileSize/humanFileSize'

import PrivateIcon from 'icons/eye-off.svg'
import LockIcon from 'icons/lock.svg'

const Wrapper = styled.div`
    height: 32px;
    display: flex;
    align-items: center;
    justify-content: space-between;

    .GroupInfoTypes {
        display: flex;
        align-items: center;
    }

    .GroupInfoType {
        display: flex;
        align-items: center;
        justify-content: center;
        width: 32px;
        height: 32px;
        color: ${(p) => p.theme.color.icon.grey};
    }
`

const GroupInfo = ({ entity, ...rest }) => {
    const { t } = useTranslation()
    const { isMembershipOnRequest, isClosed, memberCount, fileDiskUsage } =
        entity

    return (
        <Wrapper {...rest}>
            <Text size="small" variant="grey">
                {t('entity-group.nr-members', {
                    count: memberCount,
                })}
            </Text>

            {fileDiskUsage > 0 && (
                <Text size="small" variant="grey">
                    {t('entity-group.file-disk-usage')}
                    {`: ${humanFileSize(fileDiskUsage)}`}
                </Text>
            )}

            <div className="GroupInfoTypes">
                {isMembershipOnRequest && (
                    <Tooltip
                        content={t(
                            'entity-group.type-semi-open-features.membership',
                        )}
                    >
                        <div className="GroupInfoType">
                            <LockIcon
                                aria-label={t(
                                    'entity-group.type-semi-open-features.membership',
                                )}
                            />
                        </div>
                    </Tooltip>
                )}
                {isClosed && (
                    <Tooltip
                        content={t(
                            'entity-group.type-private-features.content',
                        )}
                    >
                        <div className="GroupInfoType">
                            <PrivateIcon
                                aria-label={t(
                                    'entity-group.type-private-features.content',
                                )}
                            />
                        </div>
                    </Tooltip>
                )}
            </div>
        </Wrapper>
    )
}

export default GroupInfo
