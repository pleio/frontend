import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import styled from 'styled-components'

import Avatar from 'js/components/Avatar/Avatar'
import { H2 } from 'js/components/Heading'
import Text from 'js/components/Text/Text'
import TiptapView from 'js/components/Tiptap/TiptapView'
import Tooltip from 'js/components/Tooltip/Tooltip'
import GroupInfo from 'js/group/components/GroupInfo'
import { iconViewFragment } from 'js/lib/fragments/icon'

import JoinGroupButton from './JoinGroupButton'

const Wrapper = styled.div`
    display: flex;

    .GroupDetailsAvatar {
        margin-top: 4px;
        margin-right: 20px;
    }

    .GroupDetailsTitle {
        color: ${(p) => p.theme.color.primary.main};
    }

    .GroupDetailsInfo {
        margin-top: 16px;
    }

    .GroupDetailsAccess {
        margin-top: 16px;
        display: flex;
        justify-content: center;
    }
`

const GroupDetails = ({ data }) => {
    const { t } = useTranslation()

    const { entity, viewer } = data || {}

    if (!entity) return null

    const { icon, name, isLeavingGroupDisabled, membership, richDescription } =
        entity

    return (
        <Wrapper>
            <Avatar
                size="large"
                image={icon?.download}
                name={name}
                title={t('entity-group.icon-alt')}
                className="GroupDetailsAvatar"
                disabled
            />
            <div style={{ flexGrow: 1 }}>
                <H2 as="h1" className="GroupDetailsTitle">
                    {name}
                </H2>

                {isLeavingGroupDisabled && (
                    <Tooltip
                        content={t('entity-group.mandatory-group-helper')}
                        disabled={membership !== 'joined'}
                    >
                        <Text
                            size="small"
                            variant="grey"
                            style={{
                                display: 'inline-flex',
                                marginBottom: '8px',
                            }}
                        >
                            {t('entity-group.mandatory-group')}
                        </Text>
                    </Tooltip>
                )}

                <TiptapView content={richDescription} />

                <GroupInfo entity={entity} className="GroupDetailsInfo" />

                <JoinGroupButton
                    entity={entity}
                    loggedIn={viewer.loggedIn}
                    className="GroupDetailsAccess"
                />
            </div>
        </Wrapper>
    )
}

const Query = gql`
    query GroupInfo($guid: String!) {
        viewer {
            guid
            loggedIn
            user {
                guid
                name
                icon
                url
            }
        }
        entity(guid: $guid) {
            guid
            ... on Group {
                name
                ${iconViewFragment}
                richDescription
                description
                isMembershipOnRequest
                isLeavingGroupDisabled
                isClosed
                memberCount
                membership
                requiredProfileFields {
                    guid
                }
                isJoinButtonVisible
                fileDiskUsage
            }
        }
    }
`

export default graphql(Query)(GroupDetails)
