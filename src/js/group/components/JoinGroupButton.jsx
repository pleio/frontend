import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'

import Button from 'js/components/Button/Button'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import IsLoggedInButton from 'js/components/IsLoggedInButton'
import Modal from 'js/components/Modal/Modal'
import Text from 'js/components/Text/Text'

const JoinGroupButton = ({
    entity,
    loggedIn,
    size,
    showModal,
    className,
    style,
}) => {
    const { t } = useTranslation()
    const [joinGroup, { error, loading }] = useMutation(JOIN_GROUP)

    const [modalVisible, setModalVisible] = useState(false)

    if (!entity || entity.membership === 'joined') return null
    if (!loggedIn && !window.__SETTINGS__.showLoginRegister) return null
    if (!entity.isJoinButtonVisible) return null
    const toggleModal = () => {
        setModalVisible(!modalVisible)
    }

    const requestAccess = () => {
        joinGroup({
            variables: {
                input: {
                    guid: entity.guid,
                },
            },
            refetchQueries: ['GroupContainer'],
        })
            .then(() => {
                if (entity.isMembershipOnRequest) setModalVisible(true)
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    const buttonLabel = entity.isMembershipOnRequest
        ? t('entity-group.membership-request')
        : t('entity-group.join')

    let content

    if (entity.membership === 'not_joined')
        content = (
            <IsLoggedInButton>
                <Button
                    className="JoinGroupButton" // Created for bdcommunicatie.pleio.nl
                    size={size || 'normal'}
                    variant={
                        entity.isMembershipOnRequest ? 'secondary' : 'primary'
                    }
                    loading={loading}
                    onClick={requestAccess}
                >
                    {loggedIn
                        ? buttonLabel
                        : t('entity-group.membership-login')}
                </Button>
            </IsLoggedInButton>
        )
    else if (entity.membership === 'requested') {
        if (showModal) {
            content = (
                <>
                    <Button
                        size="normal"
                        variant={'secondary'}
                        onClick={toggleModal}
                    >
                        {t('entity-group.membership-requested')}
                    </Button>
                    <Modal
                        size="small"
                        isVisible={modalVisible}
                        title={t('entity-group.membership-requested')}
                        onClose={toggleModal}
                        showCloseButton={false}
                    >
                        {t('entity-group.membership-requested-helper')}
                        <Flexer mt>
                            <Button
                                size="normal"
                                variant="primary"
                                onClick={toggleModal}
                            >
                                {t('action.ok')}
                            </Button>
                        </Flexer>
                    </Modal>
                </>
            )
        } else {
            content = (
                <Text size="small" textAlign="center">
                    {t('entity-group.membership-requested-helper')}
                </Text>
            )
        }
    }

    return (
        <div className={className} style={style}>
            <Errors errors={error} />
            {content}
        </div>
    )
}

const JOIN_GROUP = gql`
    mutation JoinGroup($input: joinGroupInput!) {
        joinGroup(input: $input) {
            group {
                guid
                ... on Group {
                    canEdit
                    membership
                    memberCount
                    members(limit: 3) {
                        edges {
                            user {
                                guid
                                username
                                url
                                name
                                icon
                            }
                        }
                    }
                }
            }
        }
    }
`
export default JoinGroupButton
