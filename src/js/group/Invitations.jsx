import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation, useNavigate } from 'react-router-dom'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { getQueryVariable } from 'helpers'

import Button from 'js/components/Button/Button'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import Modal from 'js/components/Modal/Modal'
import { iconViewFragment } from 'js/lib/fragments/icon'

const Invitations = ({ mutate }) => {
    const [errors, setErrors] = useState([])

    const navigate = useNavigate()
    const location = useLocation()

    const handleClose = () => {
        navigate(location?.state?.prevPathname || '/groups')
    }

    const handleSubmit = () => {
        const input = {
            code: getQueryVariable('invitecode'),
        }

        mutate({
            variables: {
                input,
            },
        })
            .then(({ data }) => {
                const { group } = data.acceptGroupInvitation
                navigate(group.url)
            })
            .catch((errors) => {
                setErrors(errors)
            })
    }

    const { t } = useTranslation()

    return (
        <Modal
            isVisible
            title={t('entity-group.invitation-title')}
            size="small"
            onClose={handleClose}
        >
            {t('entity-group.invitation-accept')}
            <Errors errors={errors} />
            <Flexer mt>
                <Button size="normal" variant="tertiary" onClick={handleClose}>
                    {t('action.cancel')}
                </Button>
                <Button size="normal" variant="primary" onClick={handleSubmit}>
                    {t('action.accept')}
                </Button>
            </Flexer>
        </Modal>
    )
}

const Mutation = gql`
    mutation Invitations($input: acceptGroupInvitationInput!) {
        acceptGroupInvitation(input: $input) {
            group {
                guid
                ... on Group {
                    name
                    plugins
                    description
                    ${iconViewFragment}
                    isClosed
                    url
                    canEdit
                    membership
                    members(limit: 5) {
                        total
                        edges {
                            roles
                            email
                            user {
                                guid
                                username
                                url
                                name
                                icon
                            }
                        }
                    }
                }
            }
        }
    }
`

export default graphql(Mutation)(Invitations)
