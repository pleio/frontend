import React from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import NotFound from 'js/core/NotFound'
import { featuredEditFragment } from 'js/lib/fragments/featured'
import { iconEditFragment } from 'js/lib/fragments/icon'

import AddEditForm from './AddEditForm/AddEditForm'

const Edit = () => {
    const { t } = useTranslation()
    const { guid } = useParams()

    const { loading, data } = useQuery(EDIT_GROUP, {
        variables: {
            guid,
        },
        fetchPolicy: 'cache-and-network', // Always show latest group pages
    })

    if (loading) return null

    const { entity, groupPages, viewer, site } = data || {}

    if (!entity || !entity?.canEdit) return <NotFound />

    return (
        <AddEditForm
            title={t('entity-group.edit')}
            entity={entity}
            viewer={viewer}
            site={site}
            groupPages={groupPages}
        />
    )
}

const EDIT_GROUP = gql`
    query EditGroup($guid: String!) {
        viewer {
            guid
            isAdmin
        }
        site {
            guid
            customTagsAllowed
            tagCategories {
                name
                values
            }
            profileFields {
                guid
                name
                fieldType
            }
            chatEnabled
        }
        entity(guid: $guid) {
            guid
            ... on Group {
                name
                description
                richDescription
                introduction
                welcomeMessage
                ${iconEditFragment}
                ${featuredEditFragment}
                isClosed
                isMembershipOnRequest
                isFeatured
                autoNotification
                fileNotification
                plugins
                canEdit
                url
                isChatEnabled
                tags
                tagCategories {
                    name
                    values
                }
                defaultTags
                defaultTagCategories {
                    name
                    values
                }
                isJoinButtonVisible
                isLeavingGroupDisabled
                isHidden
                isAutoMembershipEnabled
                autoMembershipFields {
                    field {
                        guid
                        name
                        fieldOptions
                    }
                    value
                }
                isIntroductionPublic
                showMemberProfileFields {
                    guid
                    name
                }
                requiredProfileFields {
                    guid
                    name
                }
                requiredProfileFieldsMessage
                startPage {
                    guid
                }
                isMenuAlwaysVisible
                menu {
                    id
                    type
                    access
                    ... on GroupMenuPluginItem {
                        plugin
                    }
                    ... on GroupMenuLinkItem {
                        label
                        link
                    }
                    ... on GroupMenuPageItem {
                        page {
                            guid
                            title
                        }
                    }
                    ... on GroupSubmenuItem {
                        label
                        submenu {
                            type
                            access
                            ... on GroupMenuPluginItem {
                                label
                                plugin
                            }
                            ... on GroupMenuLinkItem {
                                label
                                link
                            }
                            ... on GroupMenuPageItem {
                                page {
                                    guid
                                    title
                                }
                            }
                        }
                    }
                }
            }
        }
        groupPages: entities(
            subtypes: ["landingpage", "page"]
            containerGuid: $guid
        ) {
            edges {
                ... on Page {
                    guid
                    title
                }
            }
        }
    }
`

export default Edit
