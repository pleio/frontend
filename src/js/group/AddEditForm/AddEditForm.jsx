import React, { useRef, useState } from 'react'
import { FormProvider, useFieldArray, useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useLocation, useNavigate } from 'react-router-dom'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { sanitizeTagCategories } from 'helpers'
import { Set } from 'immutable'
import compose from 'lodash.flowright'
import styled from 'styled-components'

import ActionContainer from 'js/components/ActionContainer/ActionContainer'
import DeleteModal from 'js/components/EntityActions/DeleteModal'
import Errors from 'js/components/Errors'
import FormItem from 'js/components/Form/FormItem'
import { Container } from 'js/components/Grid/Grid'
import Sticky from 'js/components/Sticky/Sticky'
import TabMenu from 'js/components/TabMenu'
import TabPage from 'js/components/TabPage'
import { MAX_LENGTH } from 'js/lib/constants'
import { featuredEditFragment } from 'js/lib/fragments/featured'
import { iconViewFragment } from 'js/lib/fragments/icon'

import General from './components/General'
import Navigation from './components/Navigation'
import Settings from './components/Settings'

const TabMenuWrapper = styled(Sticky)`
    position: relative;

    &[data-sticky='true'] {
        background: white;
        box-shadow: ${(p) => p.theme.shadow.soft.bottom};
        z-index: 1;
    }
`

// BE only needs the guid and the value of a profile field
function formatProfileFields(profileFields) {
    return profileFields.map(({ field, value }) => ({
        guid: field.guid,
        value,
    }))
}

function getAutoMembershipFields(entity, site) {
    return (
        // Groups with selected auto membership fields
        entity?.autoMembershipFields ||
        // New groups or groups without selected auto membership fields
        site?.autoMembershipProfileFields?.map((autoMembershipProfileField) => {
            return {
                field: autoMembershipProfileField,
                value: [],
            }
        }) ||
        []
    )
}

const AddEditForm = ({
    mutateAdd,
    mutateEdit,
    title,
    entity,
    groupPages,
    viewer,
    site,
}) => {
    const navigate = useNavigate()
    const location = useLocation()

    const refFeatured = useRef()

    const [showDeleteModal, setShowDeleteModal] = useState(false)
    const toggleDeleteModal = () => setShowDeleteModal(!showDeleteModal)

    const [tab, setTab] = useState('general')

    const [mutationErrors, setErrors] = useState()

    const { t } = useTranslation()

    const handleClose = () => {
        navigate(
            location?.state?.prevPathname ||
                (entity && entity.url) ||
                '/groups',
            { replace: true },
        )
    }

    const afterDelete = () => {
        navigate('/groups', { replace: true })
    }

    const [categoryTags, setCategoryTags] = useState(
        entity?.tagCategories || [],
    )
    const [defaultTagCategories, setDefaultTagCategories] = useState(
        entity?.defaultTagCategories || [],
    )
    const [customTags, setCustomTags] = useState(
        new Set((entity && entity.tags) || null),
    )
    const [defaultTags, setDefaultTags] = useState(
        new Set((entity && entity.defaultTags) || null),
    )

    const [isMembershipOnRequest, setIsMembershipOnRequest] = useState(
        entity?.isMembershipOnRequest || false,
    )
    const [isClosed, setIsClosed] = useState(entity?.isClosed || false)

    const [plugins, setPlugins] = useState(entity?.plugins || [])

    const [showMemberProfileFieldGuids, setShowMemberProfileFieldGuids] =
        useState(
            entity && entity.showMemberProfileFields.length > 0
                ? entity.showMemberProfileFields.map((el) => el.guid)
                : [],
        )

    const [requiredProfileFieldGuids, setRequiredProfileFieldGuids] = useState(
        entity && entity.requiredProfileFields.length > 0
            ? entity.requiredProfileFields.map((el) => el.guid)
            : [],
    )

    const [startPage, setStartPage] = useState(entity?.startPage || '')
    const [menu, setMenu] = useState(entity?.menu || [])

    const onSubmit = async ({
        name,
        icon,
        richDescription,
        introduction,
        isIntroductionPublic,
        welcomeMessage,
        autoNotification,
        fileNotification,
        isFeatured,
        isAutoMembershipEnabled,
        isLeavingGroupDisabled,
        isHidden,
        requiredProfileFieldsMessage,
        showDefaultTags,
        isJoinButtonVisible,
        isChatEnabled,
        startPage,
        autoMembershipFields,
        isMenuAlwaysVisible,
    }) => {
        const getMenuObject = (item) => {
            if (typeof item === 'string') return null // Page type added, but no page selected
            const { label, plugin, page, type, access, submenu } = item
            const id = plugin || page?.guid || item.id || item.link
            return {
                type,
                label,
                access,
                ...(type === 'submenu'
                    ? { submenu: submenu.map(getMenuObject) }
                    : {
                          id,
                      }),
            }
        }

        const input = {
            ...(entity ? { guid: entity.guid } : {}),
            name,
            featured: refFeatured.current && refFeatured.current.getValue(),
            iconGuid: icon?.guid || null,
            richDescription,
            introduction,
            isIntroductionPublic,
            welcomeMessage,
            plugins,
            showMemberProfileFieldGuids,
            // clean required fields list from fields that are no longer checked in showMemberProfileFieldGuids
            requiredProfileFieldGuids: requiredProfileFieldGuids.filter(
                (guid) => showMemberProfileFieldGuids.indexOf(guid) !== -1,
            ),
            requiredProfileFieldsMessage,
            autoNotification,
            fileNotification,
            isFeatured,
            isAutoMembershipEnabled,
            isLeavingGroupDisabled,
            isHidden,
            tagCategories: sanitizeTagCategories(
                site.tagCategories,
                categoryTags,
            ),
            tags: customTags.toJS(),
            isMembershipOnRequest,
            isClosed,
            defaultTagCategories: showDefaultTags
                ? sanitizeTagCategories(
                      site.tagCategories,
                      defaultTagCategories,
                  )
                : null,
            defaultTags: showDefaultTags ? defaultTags : null,
            isChatEnabled,
            ...(entity && { startPageGuid: startPage || '' }), // We cannot set a start page as long as the group does not exist
            menu: menu.map(getMenuObject).filter(Boolean),
            isJoinButtonVisible,
            autoMembershipFields: formatProfileFields(autoMembershipFields),
            isMenuAlwaysVisible,
        }

        setErrors()

        const mutate = entity ? mutateEdit : mutateAdd
        await mutate({
            variables: {
                input,
            },
            refetchQueries: ['GroupsQuery', 'GetEntity', 'AdminPages'],
        })
            .then(({ data }) => {
                if (entity) {
                    navigate(
                        location?.state?.prevPathname ||
                            data.editGroup.group.url,
                        {
                            replace: true,
                            state: { reload: true },
                        },
                    )
                } else {
                    navigate(data.addGroup.group.url, { replace: true })
                }
            })
            .catch((errors) => {
                console.error(errors)
                setErrors(errors)
            })
    }

    const defaultValues = {
        name: entity?.name || null,
        icon: entity?.icon || null,
        richDescription: entity?.richDescription || null,
        introduction: entity?.introduction || null,
        isIntroductionPublic: entity?.isIntroductionPublic || false,
        welcomeMessage: entity?.welcomeMessage || null,
        autoNotification: entity ? entity.autoNotification : true,
        fileNotification: entity ? entity.fileNotification : false,
        isFeatured: entity?.isFeatured || false,
        isJoinButtonVisible: entity?.isJoinButtonVisible ?? true,
        isAutoMembershipEnabled: entity?.isAutoMembershipEnabled || false,
        isLeavingGroupDisabled: entity?.isLeavingGroupDisabled || false,
        isHidden: entity?.isHidden || false,
        requiredProfileFieldsMessage:
            entity?.requiredProfileFieldsMessage || '',
        showDefaultTags:
            entity?.defaultTags?.length > 0 ||
            entity?.defaultTagCategories?.length > 0,
        isChatEnabled: entity?.isChatEnabled || false,
        ...(entity && { startPage: entity?.startPage?.guid || '' }), // We cannot set a start page as long as the group does not exist
        autoMembershipFields: getAutoMembershipFields(entity, site),
        isMenuAlwaysVisible: entity?.isMenuAlwaysVisible || true,
    }

    const formMethods = useForm({
        mode: 'onChange',
        defaultValues,
    })

    const {
        control,
        handleSubmit,
        formState: { errors, isValid, isSubmitting },
        setValue,
    } = formMethods

    const autoMemberShipFieldArray = useFieldArray({
        control,
        name: 'autoMembershipFields',
    })

    const onError = (errors) => {
        return new Promise((resolve, reject) => {
            reject(new Error(errors))
        })
    }

    const submitForm = handleSubmit(onSubmit, onError)

    return (
        <ActionContainer
            title={title}
            publish={submitForm}
            onDelete={entity ? toggleDeleteModal : null}
            onClose={handleClose}
            isValid={isValid}
            isPublished={!!entity}
            isPublishing={isSubmitting}
        >
            <FormProvider {...formMethods}>
                <form>
                    <Container size="small">
                        <FormItem
                            control={control}
                            type="text"
                            name="name"
                            label={t('global.name')}
                            required
                            maxLength={MAX_LENGTH.groupName}
                            errors={errors}
                            size="large"
                            style={{ marginBottom: 16 }}
                        />
                        <Errors errors={mutationErrors} />
                    </Container>

                    <TabMenuWrapper>
                        <Container size="small">
                            <TabMenu
                                label={t('entity-group.group-options')}
                                options={[
                                    {
                                        onClick: () => setTab('general'),
                                        label: t('global.general'),
                                        isActive: tab === 'general',
                                        id: 'tab-general',
                                        ariaControls: 'tabpanel-general',
                                    },
                                    {
                                        onClick: () => setTab('settings'),
                                        label: t('global.settings'),
                                        isActive: tab === 'settings',
                                        id: 'tab-settings',
                                        ariaControls: 'tabpanel-settings',
                                    },
                                    {
                                        onClick: () => setTab('navigation'),
                                        label: t('global.navigation'),
                                        isActive: tab === 'navigation',
                                        id: 'tab-navigation',
                                        ariaControls: 'tabpanel-navigation',
                                    },
                                ]}
                                edgePadding
                                edgeMargin
                                showBorder
                                style={{ position: 'static' }}
                            />
                        </Container>
                    </TabMenuWrapper>

                    <TabPage
                        id={`tabpanel-general`}
                        aria-labelledby={`tab-general`}
                        visible={tab === 'general'}
                    >
                        <General
                            ref={refFeatured}
                            site={site}
                            entity={entity}
                            control={control}
                            tags={categoryTags}
                            setTags={setCategoryTags}
                            customTags={customTags}
                            setCustomTags={setCustomTags}
                            isMembershipOnRequest={isMembershipOnRequest}
                            setIsMembershipOnRequest={setIsMembershipOnRequest}
                            isClosed={isClosed}
                            setIsClosed={setIsClosed}
                        />
                    </TabPage>

                    <TabPage
                        id={`tabpanel-settings`}
                        aria-labelledby={`tab-settings`}
                        visible={tab === 'settings'}
                    >
                        <Settings
                            site={site}
                            viewer={viewer}
                            autoMemberShipFieldArray={autoMemberShipFieldArray}
                            entity={entity}
                            plugins={plugins}
                            setPlugins={setPlugins}
                            isAutoMembershipEnabled={
                                defaultValues.isAutoMembershipEnabled
                            }
                            showMemberProfileFieldGuids={
                                showMemberProfileFieldGuids
                            }
                            setShowMemberProfileFieldGuids={
                                setShowMemberProfileFieldGuids
                            }
                            requiredProfileFieldGuids={
                                requiredProfileFieldGuids
                            }
                            setRequiredProfileFieldGuids={
                                setRequiredProfileFieldGuids
                            }
                            defaultTags={defaultTags}
                            setDefaultTags={setDefaultTags}
                            defaultTagCategories={defaultTagCategories}
                            setDefaultTagCategories={setDefaultTagCategories}
                            menu={menu}
                            setMenu={setMenu}
                        />
                    </TabPage>

                    <TabPage
                        id={`tabpanel-navigation`}
                        aria-labelledby={`tab-navigation`}
                        visible={tab === 'navigation'}
                    >
                        <Navigation
                            entity={entity}
                            groupPages={groupPages?.edges || []}
                            startPage={startPage}
                            setStartPage={({ id }) => {
                                setValue('startPage', id)
                                setStartPage(id)
                            }}
                            menu={menu}
                            setMenu={setMenu}
                        />
                    </TabPage>
                </form>
            </FormProvider>

            <DeleteModal
                isVisible={showDeleteModal}
                onClose={toggleDeleteModal}
                title={t('entity-group.delete')}
                entity={entity}
                afterDelete={afterDelete}
                refetchQueries={['GroupsQuery']}
            />
        </ActionContainer>
    )
}

const ADDMUTATION = gql`
    mutation addGroup($input: addGroupInput!) {
        addGroup(input: $input) {
            group {
                guid
                url
            }
        }
    }
`

const EDITMUTATION = gql`
    mutation editGroup($input: editGroupInput!) {
        editGroup(input: $input) {
            group {
                guid
                name
                url
                description
                richDescription
                introduction
                welcomeMessage
                autoNotification
                fileNotification
                plugins
                ${iconViewFragment}
                isClosed
                isMembershipOnRequest
                tags
                tagCategories {
                    name
                    values
                }
                chat {
                    guid
                    unread
                }
                isChatEnabled
                isLeavingGroupDisabled
                isJoinButtonVisible
                isHidden
                isAutoMembershipEnabled
                isIntroductionPublic
                requiredProfileFields {
                    guid
                    name
                }
                requiredProfileFieldsMessage
                ${featuredEditFragment}
                showMemberProfileFields {
                    guid
                    name
                }
                defaultReadAccessId
            }
        }
    }
`

export default compose(
    graphql(ADDMUTATION, { name: 'mutateAdd' }),
    graphql(EDITMUTATION, { name: 'mutateEdit' }),
)(AddEditForm)
