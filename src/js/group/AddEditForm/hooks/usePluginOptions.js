import { useTranslation } from 'react-i18next'

const usePluginOptions = () => {
    const { t } = useTranslation()

    return [
        {
            value: 'events',
            label: t('entity-event.title-list'),
        },
        {
            value: 'blog',
            label: t('entity-blog.title-list'),
        },
        {
            value: 'discussion',
            label: t('entity-discussion.title-list'),
        },
        {
            value: 'questions',
            label: t('entity-question.title-list'),
        },
        {
            value: 'wiki',
            label: t('entity-wiki.title-list'),
        },
        {
            value: 'files',
            label: t('entity-file.title-list'),
        },
        {
            value: 'tasks',
            label: t('entity-task.title-list'),
        },
        {
            value: 'members',
            label: t('entity-group.members'),
        },
        {
            value: 'news',
            label: t('entity-news.title-list'),
        },
        {
            value: 'podcasts',
            label: t('entity-podcast.title-list'),
        },
    ]
}

export default usePluginOptions
