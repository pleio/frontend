import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import PropTypes from 'prop-types'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import { Row } from 'js/components/Grid/Grid'
import Modal from 'js/components/Modal/Modal'

import Option from './Option/Option'

const GroupTypePicker = ({
    isMembershipOnRequest,
    onChangeIsMembershipOnRequest,
    isClosed,
    onChangeIsClosed,
}) => {
    const { t } = useTranslation()

    const [showAccessWarningModal, setShowAccessWarningModal] = useState(false)

    const toggleAccessWarningModal = () => {
        setShowAccessWarningModal(!showAccessWarningModal)
    }

    const confirmAccessWarning = () => {
        setOption(2)
        toggleAccessWarningModal()
    }

    const setOption = (groupType) => {
        let isMembershipOnRequest, isClosed

        switch (groupType) {
            case 0:
                isMembershipOnRequest = false
                isClosed = false
                break
            case 1:
                isMembershipOnRequest = true
                isClosed = false
                break
            case 2:
                isMembershipOnRequest = true
                isClosed = true
        }

        onChangeIsMembershipOnRequest(isMembershipOnRequest)
        onChangeIsClosed(isClosed)
    }

    const getGroupType = () => {
        if (isMembershipOnRequest !== null && isClosed !== null) {
            let groupType
            if (!isMembershipOnRequest && !isClosed) {
                groupType = 0
            } else if (isMembershipOnRequest && !isClosed) {
                groupType = 1
            } else if (isMembershipOnRequest && isClosed) {
                groupType = 2
            }
            return groupType
        }
    }

    const groupType = getGroupType()

    return (
        <>
            <Row $growContent $gutter={0}>
                <Option
                    title={t('entity-group.type-open')}
                    value={0}
                    features={t('entity-group.type-open-features', {
                        returnObjects: true,
                    })}
                    checked={groupType === 0}
                    setOption={setOption}
                />
                <Option
                    title={t('entity-group.type-semi-open')}
                    value={1}
                    features={t('entity-group.type-semi-open-features', {
                        returnObjects: true,
                    })}
                    checked={groupType === 1}
                    setOption={setOption}
                />
                <Option
                    title={t('entity-group.type-private')}
                    value={2}
                    features={t('entity-group.type-private-features', {
                        returnObjects: true,
                    })}
                    checked={groupType === 2}
                    setOption={
                        isClosed !== undefined // If changing type of an existing group to closed
                            ? toggleAccessWarningModal
                            : setOption
                    }
                />
            </Row>
            <Modal
                isVisible={showAccessWarningModal}
                title={t('form.access-level')}
                size="small"
                onClose={toggleAccessWarningModal}
            >
                {t('entity-group.type-private-warning')}
                <Flexer mt>
                    <Button
                        size="normal"
                        variant="tertiary"
                        type="button"
                        onClick={toggleAccessWarningModal}
                    >
                        {t('action.no-back')}
                    </Button>
                    <Button
                        size="normal"
                        variant="primary"
                        onClick={confirmAccessWarning}
                    >
                        {t('action.yes-confirm')}
                    </Button>
                </Flexer>
            </Modal>
        </>
    )
}

GroupTypePicker.propTypes = {
    isMembershipOnRequest: PropTypes.bool,
    isClosed: PropTypes.bool,
}

export default GroupTypePicker
