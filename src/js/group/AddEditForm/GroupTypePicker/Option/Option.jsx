import React from 'react'
import PropTypes from 'prop-types'

import RadioField from 'js/components/RadioField/RadioField'

import Column from './components/Column'
import Wrapper from './components/Wrapper'

const Option = ({ title, value, checked, features, setOption }) => {
    const handleChange = () => {
        setOption(value)
    }

    return (
        <Column mobileLandscapeUp={1 / 3}>
            <Wrapper checked={checked}>
                <div className="OptionCard">
                    <div>
                        <label className="OptionTitle" htmlFor={value}>
                            {title}
                        </label>
                        <ul className="OptionList">
                            {Object.entries(features).map(([key, feature]) => (
                                <li key={key} className="OptionListItem">
                                    {feature}
                                </li>
                            ))}
                        </ul>
                    </div>
                </div>
                <RadioField
                    name="groupType"
                    id={value}
                    checked={checked}
                    style={{
                        position: 'static',
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}
                    onChange={handleChange}
                />
            </Wrapper>
        </Column>
    )
}

Option.propTypes = {
    title: PropTypes.string,
    value: PropTypes.number,
    checked: PropTypes.bool,
    features: PropTypes.object,
}

export default Option
