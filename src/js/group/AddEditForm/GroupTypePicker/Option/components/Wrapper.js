import { media } from 'helpers'
import styled, { css } from 'styled-components'

export default styled.div`
    position: relative;
    display: flex;

    .OptionTitle {
        display: block;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        font-weight: ${(p) => p.theme.font.weight.semibold};
        color: ${(p) => p.theme.color.text.black};
    }

    ${media.mobilePortrait`
        display: flex;

       .OptionCard {
            order: 1;
            margin-left: 12px;
        }
    `};

    ${media.mobileLandscapeUp`
        flex-direction: column;
    `};

    .OptionCard {
        flex-grow: 1;
        position: relative;
        border: 1px solid ${(p) => p.theme.color.grey[30]};

        ${(p) =>
            p.checked &&
            css`
                z-index: 1;
                border-color: ${(p) => p.theme.color.secondary.main};
            `};

        ${media.mobilePortrait`
            padding: 24px 20px 28px 32px;
        `};

        ${media.mobileLandscapeUp`
            padding: 24px 24px 30px 36px;
        `};
    }

    .OptionList {
        list-style: none;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
    }

    .OptionListItem {
        position: relative;

        &:before {
            content: '';
            display: inline-block;
            width: 4px;
            height: 4px;
            background-color: ${(p) => p.theme.color.grey[40]};
            border-radius: 50%;
            position: absolute;
            top: 8px;
            left: -12px;
        }
    }
`
