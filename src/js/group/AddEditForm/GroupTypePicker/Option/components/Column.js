import { media } from 'helpers'
import styled from 'styled-components'

import { Col } from 'js/components/Grid/Grid'

export default styled(Col)`
    ${media.mobilePortrait`
        &:first-child {
            margin-bottom: -1px;

            .OptionCard {
                border-top-left-radius: ${(p) => p.theme.radius.small};
                border-top-right-radius: ${(p) => p.theme.radius.small};
            }
        }

        &:last-child {
            margin-top: -1px;

            .OptionCard {
                border-bottom-left-radius: ${(p) => p.theme.radius.small};
                border-bottom-right-radius: ${(p) => p.theme.radius.small};
            }
        }
    `}

    ${media.mobileLandscapeUp`
        &:first-child {
            margin-right: -1px;

            .OptionCard {
                border-top-left-radius:  ${(p) => p.theme.radius.small};
                border-bottom-left-radius:  ${(p) => p.theme.radius.small};
            }
        }

        &:last-child {
            margin-left: -1px;

            .OptionCard {
                border-top-right-radius:  ${(p) => p.theme.radius.small};
                border-bottom-right-radius: ${(p) => p.theme.radius.small};
            }
        }
    `}
`
