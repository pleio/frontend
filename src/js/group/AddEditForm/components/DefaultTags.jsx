import React from 'react'
import { useTranslation } from 'react-i18next'

import AnimatePresence from 'js/components/AnimatePresence/AnimatePresence'
import CustomTagsField from 'js/components/CustomTagsField'
import FormItem from 'js/components/Form/FormItem'
import { Col, Row } from 'js/components/Grid/Grid'
import TagCategoriesField from 'js/components/TagCategories/TagCategoriesField'

const DefaultTags = ({
    control,
    tagCategories,
    showDefaultTags,
    defaultTags,
    setDefaultTags,
    defaultTagCategories,
    setDefaultTagCategories,
    customTagsAllowed,
}) => {
    const { t } = useTranslation()

    const handleAddTags = (tag) => {
        setDefaultTags(defaultTags.add(tag))
    }
    const handleRemoveTags = (tag) => {
        setDefaultTags(defaultTags.delete(tag))
    }

    return (
        <Row>
            <Col>
                <FormItem
                    control={control}
                    type="switch"
                    prependField
                    name="showDefaultTags"
                    size="small"
                    title={t('entity-group.default-tags')}
                    helper={t('entity-group.default-tags-helper')}
                />
                <AnimatePresence visible={showDefaultTags}>
                    <TagCategoriesField
                        name="filters"
                        tagCategories={tagCategories}
                        value={defaultTagCategories}
                        setTags={setDefaultTagCategories}
                    />
                    {customTagsAllowed && (
                        <CustomTagsField
                            name="defaultTags"
                            value={defaultTags}
                            onAddTag={handleAddTags}
                            onRemoveTag={handleRemoveTags}
                            style={{ margin: '16px 0 8px' }}
                        />
                    )}
                </AnimatePresence>
            </Col>
        </Row>
    )
}

export default DefaultTags
