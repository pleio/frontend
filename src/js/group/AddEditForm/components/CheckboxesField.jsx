import React from 'react'

import Checkbox from 'js/components/Checkbox/Checkbox'
import { H4 } from 'js/components/Heading'
import Text from 'js/components/Text/Text'

const CheckboxesField = ({
    name,
    title,
    helper,
    options,
    value,
    onChange,
    ...rest
}) => (
    <div {...rest}>
        <H4>{title}</H4>
        <Text size="small" variant="grey">
            {helper}
        </Text>
        {options.map((option) => {
            const handleChange = (evt) => {
                if (evt.target.checked) {
                    onChange([...value, option.value])
                } else {
                    const newValue = [...value]
                    newValue.splice(value.indexOf(option.value), 1)
                    onChange(newValue)
                }
            }

            return (
                <Checkbox
                    key={`${name}-${option.value}`}
                    name={`${name}-${option.value}`}
                    checked={value.indexOf(option.value) !== -1}
                    label={option.label}
                    size="small"
                    onChange={handleChange}
                    disabled={option.disabled}
                    title={option.title}
                />
            )
        })}
    </div>
)

export default CheckboxesField
