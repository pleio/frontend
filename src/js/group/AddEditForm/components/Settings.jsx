import React, { useState } from 'react'
import { useFormContext } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { sortByProperty } from 'helpers'
import { produce } from 'immer'

import Button from 'js/components/Button/Button'
import Checkbox from 'js/components/Checkbox/Checkbox'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import { Col, Container, Row } from 'js/components/Grid/Grid'
import Popover from 'js/components/Popover/Popover'
import Setting from 'js/components/Setting'
import Spacer from 'js/components/Spacer/Spacer'
import Text from 'js/components/Text/Text'

import Automembers from './Automembers'
import CheckboxesField from './CheckboxesField'
import ConfirmFieldModal from './ConfirmFieldModal'
import DefaultTags from './DefaultTags'

const PluginsOption = ({
    option: { value, label },
    plugins,
    setPlugins,
    menu,
    setMenu,
}) => {
    const { t } = useTranslation()

    const [showAddToMenuPrompt, setShowAddToMenuPrompt] = useState(false)

    const optionExistsInMenu = menu.find((el) => el.id === value)

    const handleChange = ({ target: { checked } }) => {
        if (
            (optionExistsInMenu && !checked) ||
            (!optionExistsInMenu && checked)
        ) {
            setShowAddToMenuPrompt(true)
        }

        if (checked) {
            setPlugins([...plugins, value])
        } else {
            setPlugins(
                produce((newState) => {
                    newState.splice(plugins.indexOf(value), 1)
                }),
            )
        }
    }

    const handleAddToMenu = () => {
        if (optionExistsInMenu) {
            setMenu(
                produce((newState) => {
                    newState.splice(
                        menu.findIndex((el) => el.id === value),
                        1,
                    )
                }),
            )
        } else {
            setMenu([
                ...menu,
                {
                    id: value,
                    type: 'plugin',
                },
            ])
        }

        setShowAddToMenuPrompt(false)
    }

    const message = optionExistsInMenu ? (
        <span>
            {t('entity-group.remove-type-from-navigation-prompt')}
            {value === 'files' && (
                <>
                    <br />
                    {t('entity-group.remove-type-from-navigation-prompt-files')}
                </>
            )}
        </span>
    ) : (
        t('entity-group.add-type-to-navigation-prompt')
    )

    return (
        <div>
            <Popover
                visible={showAddToMenuPrompt}
                onClickOutside={() => setShowAddToMenuPrompt(false)}
                placement="top-start"
                content={
                    <Spacer spacing="tiny" style={{ padding: '8px 12px 12px' }}>
                        <Text size="small" textAlign="center">
                            {message}
                        </Text>
                        <Flexer>
                            <Button
                                size="small"
                                variant="tertiary"
                                onClick={() => {
                                    setShowAddToMenuPrompt(false)
                                }}
                            >
                                {t('action.no-skip')}
                            </Button>
                            <Button
                                size="small"
                                variant="primary"
                                onClick={handleAddToMenu}
                            >
                                {t('action.yes-confirm')}
                            </Button>
                        </Flexer>
                    </Spacer>
                }
            >
                <div>
                    <Checkbox
                        key={`plugins-${value}`}
                        name={`plugins-${value}`}
                        onChange={handleChange}
                        label={label}
                        checked={plugins.includes(value)}
                        size="small"
                    />
                </div>
            </Popover>
        </div>
    )
}

const Wrapper = ({ children }) => {
    return (
        <Container size="small">
            <Row $gutter={40} $spacing={16} style={{ marginTop: '24px' }}>
                {children}
            </Row>
        </Container>
    )
}

const Settings = ({
    site,
    viewer,
    autoMemberShipFieldArray,
    plugins,
    setPlugins,
    showMemberProfileFieldGuids,
    setShowMemberProfileFieldGuids,
    requiredProfileFieldGuids,
    setRequiredProfileFieldGuids,
    defaultTags,
    setDefaultTags,
    defaultTagCategories,
    setDefaultTagCategories,
    menu,
    setMenu,
}) => {
    const { t } = useTranslation()

    const { control, watch } = useFormContext()

    const pluginsOptions = [
        {
            value: 'events',
            label: t('entity-event.title-list'),
        },
        {
            value: 'blog',
            label: t('entity-blog.title-list'),
        },
        {
            value: 'discussion',
            label: t('entity-discussion.title-list'),
        },
        {
            value: 'questions',
            label: t('entity-question.title-list'),
        },
        {
            value: 'news',
            label: t('entity-news.title-list'),
        },
        {
            value: 'wiki',
            label: t('entity-wiki.title-list'),
        },
        {
            value: 'podcasts',
            label: t('entity-podcast.title-list'),
        },
        {
            value: 'tasks',
            label: t('entity-task.title-list'),
        },
        {
            value: 'files',
            label: t('entity-file.title-list'),
        },
    ]

    const profileFieldOptions = site.profileFields.map(({ guid, name }) => ({
        value: guid,
        label: name,
    }))

    // Long text fields cannot be displayed in the members page
    const membersPageProfileFieldOptions = site.profileFields.map(
        ({ guid, name, fieldType }) => ({
            value: guid,
            label: name,
            disabled: fieldType === 'htmlField',
            title:
                fieldType === 'htmlField'
                    ? t('entity-group.html-field-helper')
                    : null,
        }),
    )

    return (
        <Wrapper>
            <Col mobileLandscapeUp={1 / 2}>
                <Setting
                    subtitle={t('global.types')}
                    helper={t('entity-group.types-helper')}
                    htmlFor="plugins"
                    inputWidth="full"
                >
                    {pluginsOptions
                        .sort(sortByProperty('label'))
                        .map((option, i) => (
                            <PluginsOption
                                key={`plugins-${i}`}
                                option={option}
                                plugins={plugins}
                                setPlugins={setPlugins}
                                menu={menu}
                                setMenu={setMenu}
                            />
                        ))}
                </Setting>

                {profileFieldOptions.length > 0 && (
                    <>
                        <CheckboxesField
                            name="showMemberProfileFieldGuids"
                            title={t('entity-group.members')}
                            helper={t('entity-group.members-helper')}
                            options={membersPageProfileFieldOptions}
                            value={showMemberProfileFieldGuids}
                            onChange={setShowMemberProfileFieldGuids}
                            style={{ marginTop: '16px' }}
                        />

                        <CheckboxesField
                            name="requiredProfileField"
                            title={t('profile.required-fields')}
                            helper={t('profile.required-fields-helper')}
                            options={profileFieldOptions}
                            value={requiredProfileFieldGuids}
                            onChange={setRequiredProfileFieldGuids}
                            style={{ marginTop: '16px' }}
                        />

                        <FormItem
                            control={control}
                            type="textarea"
                            name="requiredProfileFieldsMessage"
                            label={t('form.message')}
                            helper={t('profile.required-fields-message-helper')}
                            style={{ marginTop: '16px' }}
                        />
                    </>
                )}
            </Col>
            <Col mobileLandscapeUp={1 / 2}>
                <Spacer spacing="small">
                    {site.chatEnabled && (
                        <FormItem
                            control={control}
                            type="switch"
                            prependField
                            name="isChatEnabled"
                            size="small"
                            title={t('chat.title')}
                            helper={t('entity-group.chat-helper')}
                        />
                    )}
                    <FormItem
                        control={control}
                        type="switch"
                        prependField
                        name="autoNotification"
                        size="small"
                        title={t('entity-group.field-notifications')}
                        helper={t('entity-group.field-notifications-helper')}
                    />
                    <FormItem
                        control={control}
                        type="switch"
                        prependField
                        name="fileNotification"
                        size="small"
                        title={t('entity-group.file-notifications')}
                        helper={t('entity-group.file-notifications-helper')}
                    />
                    {viewer?.isAdmin && (
                        <FormItem
                            control={control}
                            type="switch"
                            prependField
                            name="isFeatured"
                            size="small"
                            title={t('entity-group.field-featured')}
                            helper={t('entity-group.field-featured-helper')}
                        />
                    )}
                    {viewer?.isAdmin && (
                        <FormItem
                            control={control}
                            type="switch"
                            prependField
                            name="isHidden"
                            size="small"
                            title={t('entity-group.hidden')}
                            helper={t('entity-group.hidden-helper')}
                        />
                    )}
                    <FormItem
                        control={control}
                        type="switch"
                        prependField
                        name="isJoinButtonVisible"
                        size="small"
                        title={t('entity-group.allow-join')}
                        helper={t('entity-group.allow-join-helper')}
                    />

                    <FormItem
                        control={control}
                        type="switch"
                        prependField
                        name="isMenuAlwaysVisible"
                        size="small"
                        title={t('entity-group.menu-always-visible')}
                        helper={t('entity-group.menu-always-visible-helper')}
                    />

                    {viewer?.isAdmin && (
                        <>
                            <FormItem
                                control={control}
                                type="switch"
                                prependField
                                name="isAutoMembershipEnabled"
                                size="small"
                                title={t('entity-group.field-auto-join')}
                                helper={t(
                                    'entity-group.field-auto-join-helper',
                                )}
                            />
                            <ConfirmFieldModal
                                title={t('entity-group.field-auto-join')}
                                fieldName="isAutoMembershipEnabled"
                            >
                                {t('entity-group.field-auto-join-warning')}
                            </ConfirmFieldModal>
                        </>
                    )}

                    <Automembers
                        autoMemberShipFieldArray={autoMemberShipFieldArray}
                    />

                    {viewer?.isAdmin && (
                        <FormItem
                            control={control}
                            type="switch"
                            prependField
                            name="isLeavingGroupDisabled"
                            size="small"
                            title={t('entity-group.field-mandatory')}
                            helper={t('entity-group.field-mandatory-helper')}
                        />
                    )}
                    <DefaultTags
                        control={control}
                        tagCategories={site.tagCategories}
                        showDefaultTags={watch('showDefaultTags')}
                        defaultTags={defaultTags}
                        setDefaultTags={setDefaultTags}
                        defaultTagCategories={defaultTagCategories}
                        setDefaultTagCategories={setDefaultTagCategories}
                        customTagsAllowed={site.customTagsAllowed}
                    />
                </Spacer>
            </Col>
        </Wrapper>
    )
}

export default Settings
