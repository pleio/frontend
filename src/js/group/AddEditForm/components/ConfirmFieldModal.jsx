// Note: if you want to use this component, make sure your <form> is wrapped
// in <FormProvider> of "react-hook-form"
import React, { useEffect, useState } from 'react'
import { useFormContext } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import Modal from 'js/components/Modal/Modal'

const ConfirmFieldModal = ({ title, fieldName, children }) => {
    const { t } = useTranslation()
    const { watch, setValue, formState } = useFormContext()
    const [showConfirmation, setShowConfirmation] = useState(false)
    const isFieldDirty = formState.dirtyFields[fieldName]
    const watchedField = watch(fieldName)

    useEffect(() => {
        if (isFieldDirty && watchedField) {
            setShowConfirmation(true)
        } else {
            setShowConfirmation(false)
        }
    }, [watchedField, isFieldDirty])

    return (
        <Modal title={title} isVisible={showConfirmation} size="small">
            {children}
            <Flexer mt>
                <Button
                    size="normal"
                    variant="secondary"
                    onClick={() => {
                        setValue(fieldName, false)
                        setShowConfirmation(false)
                    }}
                >
                    {t('action.cancel')}
                </Button>
                <Button
                    size="normal"
                    variant="primary"
                    onClick={() => {
                        setShowConfirmation(false)
                    }}
                >
                    {t('action.yes-confirm')}
                </Button>
            </Flexer>
        </Modal>
    )
}

export default ConfirmFieldModal
