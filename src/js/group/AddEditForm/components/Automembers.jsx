import React, { useState } from 'react'
import { useFormContext } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import IconButton from 'js/components/IconButton/IconButton'
import Modal from 'js/components/Modal/Modal'
import Select from 'js/components/Select/Select'
import Spacer from 'js/components/Spacer/Spacer'
import Text from 'js/components/Text/Text'

import SettingsIcon from 'icons/settings-small.svg'

function hasSelectedAutoMemberProfileFields(autoMembershipFields = []) {
    return autoMembershipFields.some(
        (profileField) => profileField.value?.length > 0,
    )
}

const Automembers = ({ autoMemberShipFieldArray }) => {
    const { t } = useTranslation()

    const { control, setValue } = useFormContext()

    const { fields, update } = autoMemberShipFieldArray

    const [originalSelectedProfileFields] = useState(fields) // Save the original selected fields to reset if user cancels

    const [showSettingsModal, setShowSettingsModal] = useState(false)

    if (!fields.length) return null

    return (
        <div>
            <FormItem
                control={control}
                type="switch"
                prependField
                name="autoMemberOnProfileFields"
                checked={hasSelectedAutoMemberProfileFields(fields)}
                size="small"
                title={t('entity-group.profile-field-auto-join')}
                helper={t('entity-group.profile-field-auto-join-helper')}
                onChange={({ target }) => {
                    setValue(
                        'autoMembershipFields',
                        originalSelectedProfileFields.map((profileField) => {
                            return {
                                ...profileField,
                                value: [],
                            }
                        }),
                    )

                    setShowSettingsModal(target.checked)
                }}
                titleAppend={
                    <IconButton
                        size="small"
                        variant="secondary"
                        style={{
                            marginLeft: '2px',
                        }}
                        onClick={() => {
                            setShowSettingsModal(true)
                        }}
                    >
                        <SettingsIcon />
                    </IconButton>
                }
            />

            <Modal
                size="small"
                isVisible={showSettingsModal}
                title={t('entity-group.profile-field-auto-join')}
                onClose={() => setShowSettingsModal(false)}
                showCloseButton
            >
                <Spacer spacing="small">
                    <Text size="small">
                        {t('entity-group.profile-field-auto-join-explainer')}
                    </Text>

                    {fields.map((item, index) => {
                        const { field, value } = item
                        return (
                            <Select
                                key={field.guid}
                                label={field.name}
                                options={field.fieldOptions.map((value) => ({
                                    value,
                                    label: value,
                                }))}
                                value={value}
                                isMulti
                                onChange={(value) =>
                                    update(index, { ...item, value })
                                }
                            />
                        )
                    })}
                </Spacer>

                <Flexer mt>
                    <Button
                        variant="tertiary"
                        size="normal"
                        onClick={() => {
                            setValue(
                                'autoMembershipFields',
                                originalSelectedProfileFields,
                            )
                            setShowSettingsModal(false)
                        }}
                    >
                        {t('action.cancel')}
                    </Button>
                    <Button
                        size="normal"
                        variant="primary"
                        onClick={() => setShowSettingsModal(false)}
                    >
                        {t('action.save')}
                    </Button>
                </Flexer>
            </Modal>
        </div>
    )
}

export default Automembers
