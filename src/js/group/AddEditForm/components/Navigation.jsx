import React from 'react'
import { DragDropContext } from 'react-beautiful-dnd'
import { useTranslation } from 'react-i18next'
import { produce } from 'immer'

import Button from 'js/components/Button/Button'
import { Container } from 'js/components/Grid/Grid'
import Setting from 'js/components/Setting'
import Spacer from 'js/components/Spacer/Spacer'

import MenuItem from './MenuItem'
import NavigationMenu from './NavigationMenu'

const Navigation = ({
    entity,
    menu,
    groupPages,
    setMenu,
    startPage,
    setStartPage,
}) => {
    const { t } = useTranslation()

    const newPageTemplate =
        groupPages?.length > 0
            ? {
                  type: 'page',
                  id: groupPages[0].guid,
                  access: 'public',
              }
            : {
                  type: 'plugin',
                  id: 'blog',
                  access: 'public',
              }

    const onDragEnd = (result) => {
        if (!result.destination) return

        const sourceIndex = result.source.index
        const destinationIndex = result.destination.index

        if (result.type === 'SUBMENU') {
            const source = JSON.parse(result.source.droppableId)
            const destination = JSON.parse(result.destination.droppableId)

            const newSource = [...menu[source.index].submenu]
            const [movedPage] = newSource.splice(sourceIndex, 1)

            if (newSource.length === 0) {
                setMenu(
                    produce((newState) => {
                        newState[source.index] = newPageTemplate
                    }),
                )
            }

            if (source.index === destination.index) {
                newSource.splice(destinationIndex, 0, movedPage)
            }

            setMenu(
                produce((newState) => {
                    newState[source.index].submenu = newSource
                }),
            )

            if (source.index !== destination.index) {
                const newDestination = [...menu[destination.index].submenu]
                newDestination.splice(destinationIndex, 0, movedPage)

                setMenu(
                    produce((newState) => {
                        newState[destination.index].submenu = newDestination
                    }),
                )
            }
        } else {
            const newPages = [...menu]
            const [movedPage] = newPages.splice(sourceIndex, 1)
            newPages.splice(destinationIndex, 0, movedPage)
            setMenu(newPages)
        }
    }

    const handleClickAdd = (evt, index) => {
        if (index !== undefined) {
            const newSubmenu = [...(menu[index].submenu || []), newPageTemplate]
            setMenu(
                produce((newState) => {
                    newState[index] = {
                        label: newState[index].label || '',
                        type: 'submenu',
                        access: newState[index].access,
                        submenu: newSubmenu,
                    }
                }),
            )
        } else {
            setMenu([...menu, newPageTemplate])
        }
    }

    const handleRemove = (index, parentIndex) => {
        if (parentIndex !== undefined) {
            setMenu(
                produce((newState) => {
                    const parent = newState[parentIndex]
                    const submenu = parent.submenu
                    submenu.splice(index, 1)
                    if (submenu.length === 0) {
                        const newParent = newPageTemplate
                        newParent.access = parent.access
                        newState[parentIndex] = newParent
                    }
                }),
            )
        } else {
            setMenu(
                produce((newState) => {
                    newState.splice(index, 1)
                }),
            )
        }
    }

    const handleChangePage = (val, index, parentIndex) => {
        if (parentIndex !== undefined) {
            setMenu(
                produce((newState) => {
                    newState[parentIndex].submenu[index] = val
                }),
            )
        } else {
            setMenu(
                produce((newState) => {
                    newState[index] = val
                }),
            )
        }
    }

    const handleChangeAccess = (val, index, parentIndex) => {
        if (parentIndex !== undefined) {
            setMenu(
                produce((newState) => {
                    newState[parentIndex].submenu[index].access = val
                }),
            )
        } else {
            setMenu(
                produce((newState) => {
                    newState[index].access = val
                }),
            )
        }
    }

    const handleChangeLabel = (val, i) => {
        setMenu(
            produce((newState) => {
                newState[i].label = val
            }),
        )
    }

    return (
        <Container size="small" style={{ marginTop: '24px' }}>
            <Spacer>
                {/* Only show start page setting if group exists */}
                {entity && (
                    <Spacer spacing="small">
                        <Setting
                            subtitle="entity-group.home-page"
                            helper="entity-group.home-page-helper"
                        />
                        <MenuItem
                            name="startPage"
                            id="startPage"
                            entity={startPage}
                            groupPages={groupPages}
                            onChangePage={setStartPage}
                        />
                    </Spacer>
                )}

                <div>
                    <Spacer spacing="small">
                        <Setting
                            subtitle="entity-group.menu"
                            helper="entity-group.menu-helper"
                        />
                        <DragDropContext onDragEnd={onDragEnd}>
                            <NavigationMenu
                                menu={menu}
                                groupPages={groupPages}
                                onChangePage={handleChangePage}
                                onChangeLabel={handleChangeLabel}
                                onChangeAccess={handleChangeAccess}
                                onRemovePage={handleRemove}
                                onAddPage={handleClickAdd}
                                onDragEnd={onDragEnd}
                                id="menu"
                                droppableId="droppable"
                                type="MENU"
                                style={{
                                    marginLeft: '-20px',
                                    marginRight: '-20px',
                                }}
                            />
                        </DragDropContext>
                    </Spacer>

                    <Button
                        size="normal"
                        variant="secondary"
                        onClick={handleClickAdd}
                        style={{ marginTop: '16px' }}
                    >
                        {t('action.add-item')}
                    </Button>
                </div>
            </Spacer>
        </Container>
    )
}

export default Navigation
