import React from 'react'
import { Draggable, Droppable } from 'react-beautiful-dnd'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'

import MenuItem from './MenuItem'

const NavigationMenu = ({
    id,
    type,
    droppableId,
    menu,
    groupPages,
    onDragEnd,
    onChangePage,
    onChangeLabel,
    onChangeAccess,
    onRemovePage,
    onAddPage,
    ...rest
}) => {
    const { t } = useTranslation()

    if (!menu || menu?.length === 0) return null

    return (
        <Droppable droppableId={droppableId} type={type}>
            {(provided) => (
                <ul ref={provided.innerRef} {...rest}>
                    {menu.map((entity, i) => {
                        const newId = `${id}-${i}`
                        return (
                            <Draggable
                                key={newId}
                                draggableId={newId}
                                index={i}
                            >
                                {(provided) => (
                                    <li
                                        ref={provided.innerRef}
                                        {...provided.draggableProps}
                                    >
                                        <MenuItem
                                            id={`menu-${i}`}
                                            dragHandleProps={
                                                provided.dragHandleProps
                                            }
                                            entity={entity}
                                            onChangePage={(val) =>
                                                onChangePage(val, i)
                                            }
                                            onChangeAccess={(val) =>
                                                onChangeAccess(val, i)
                                            }
                                            onChangeLabel={(val) =>
                                                onChangeLabel(val, i)
                                            }
                                            onRemove={() => onRemovePage(i)}
                                            style={{
                                                paddingBottom: '16px',
                                            }}
                                            groupPages={groupPages}
                                        />
                                        <NavigationMenu
                                            menu={entity.submenu}
                                            groupPages={groupPages}
                                            onChangePage={(val, childIndex) =>
                                                onChangePage(val, childIndex, i)
                                            }
                                            onChangeAccess={(val, childIndex) =>
                                                onChangeAccess(
                                                    val,
                                                    childIndex,
                                                    i,
                                                )
                                            }
                                            onChangeLabel={onChangeLabel}
                                            onRemovePage={(childIndex) =>
                                                onRemovePage(childIndex, i)
                                            }
                                            onAddPage={onAddPage}
                                            onDragEnd={onDragEnd}
                                            id={newId}
                                            droppableId={JSON.stringify({
                                                type: 'droppable',
                                                index: i,
                                            })}
                                            type="SUBMENU"
                                            style={{
                                                marginLeft: '40px',
                                            }}
                                        />
                                        {type === 'MENU' && (
                                            <Button
                                                size="small"
                                                variant="tertiary"
                                                style={{
                                                    margin: '0 0 16px 80px',
                                                }}
                                                onClick={(evt) =>
                                                    onAddPage(evt, i)
                                                }
                                            >
                                                {t('admin.add-subitem')}
                                            </Button>
                                        )}
                                    </li>
                                )}
                            </Draggable>
                        )
                    })}
                    {provided.placeholder}
                </ul>
            )}
        </Droppable>
    )
}

export default NavigationMenu
