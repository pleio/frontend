import React, { forwardRef } from 'react'
import { useTranslation } from 'react-i18next'

import CustomTagsField from 'js/components/CustomTagsField'
import IconField from 'js/components/EntityActions/components/IconField'
import CoverField from 'js/components/Form/CoverField/CoverField'
import FormItem from 'js/components/Form/FormItem'
import { Container } from 'js/components/Grid/Grid'
import Spacer from 'js/components/Spacer/Spacer'
import TagCategoriesField from 'js/components/TagCategories/TagCategoriesField'

import GroupTypePicker from '../GroupTypePicker/GroupTypePicker'

const General = forwardRef(
    (
        {
            site,
            entity,
            control,
            tags,
            setTags,
            customTags,
            setCustomTags,
            isMembershipOnRequest,
            setIsMembershipOnRequest,
            isClosed,
            setIsClosed,
        },
        ref,
    ) => {
        const { t } = useTranslation()

        const messageHelper = (
            <>
                {t('entity-group.field-welcome-helper')}
                <br />
                <span style={{ display: 'inline-block', width: '100px' }}>
                    [name]
                </span>
                {t('entity-group.field-welcome-helper-username')}
                <br />
                <span style={{ display: 'inline-block', width: '100px' }}>
                    [group_name]
                </span>
                {t('entity-group.field-welcome-helper-groupname')}
                <br />
                <span style={{ display: 'inline-block', width: '100px' }}>
                    [group_url]
                </span>
                {t('entity-group.field-welcome-helper-groupurl')}
            </>
        )

        const handleAddTags = (tag) => {
            setCustomTags(customTags.add(tag))
        }
        const handleRemoveTags = (tag) => {
            setCustomTags(customTags.delete(tag))
        }

        return (
            <>
                <Container size="small" style={{ paddingTop: '24px' }}>
                    <Spacer>
                        <CoverField
                            ref={ref}
                            featured={entity && entity.featured}
                            size="landscape-small"
                            disableCaption
                        />

                        <IconField
                            iconProps={{
                                objectFit: 'cover',
                                style: {
                                    width: '40px',
                                    height: '40px',
                                    borderRadius: '50%',
                                    overflow: 'hidden',
                                },
                            }}
                        />

                        <FormItem
                            control={control}
                            type="rich"
                            name="richDescription"
                            title={t('entity-group.field-description')}
                            helper={t('entity-group.field-description-helper')}
                            options={{
                                textStyle: true,
                                textLink: true,
                                textQuote: true,
                                textList: true,
                                insertMedia: true,
                            }}
                            height="small"
                        />

                        <div>
                            <FormItem
                                control={control}
                                type="rich"
                                name="introduction"
                                title={t('form.introduction')}
                                helper={t('entity-group.field-intro-helper')}
                                height="small"
                                options={{
                                    textFormat: true,
                                    textStyle: true,
                                    textLink: true,
                                    textQuote: true,
                                    textList: true,
                                    insertMedia: true,
                                }}
                            />
                            <FormItem
                                control={control}
                                type="switch"
                                name="isIntroductionPublic"
                                label={t(
                                    'entity-group.field-intro-visible-non-members',
                                )}
                                size="small"
                            />
                        </div>

                        <div>
                            <FormItem
                                control={control}
                                type="rich"
                                name="welcomeMessage"
                                title={t('form.welcome-message')}
                                helper={messageHelper}
                                options={{
                                    textFormat: true,
                                    textStyle: true,
                                }}
                                height="small"
                                contentType="html"
                            />
                        </div>

                        <TagCategoriesField
                            name="filters"
                            tagCategories={site.tagCategories}
                            value={tags}
                            setTags={setTags}
                        />
                        {site.customTagsAllowed && (
                            <CustomTagsField
                                name="customTags"
                                value={customTags}
                                onAddTag={handleAddTags}
                                onRemoveTag={handleRemoveTags}
                            />
                        )}
                    </Spacer>
                </Container>
                <Container size="large" style={{ marginTop: '24px' }}>
                    <GroupTypePicker
                        isMembershipOnRequest={isMembershipOnRequest}
                        onChangeIsMembershipOnRequest={setIsMembershipOnRequest}
                        isClosed={isClosed}
                        onChangeIsClosed={setIsClosed}
                    />
                </Container>
            </>
        )
    },
)

General.displayName = 'General'

export default General
