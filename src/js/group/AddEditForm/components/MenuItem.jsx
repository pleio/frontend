import React from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import { AccessIcon, useAccessTitle } from 'js/components/AccessIcon/AccessIcon'
import DropdownButton from 'js/components/DropdownButton/DropdownButton'
import Flexer from 'js/components/Flexer/Flexer'
import IconButton from 'js/components/IconButton/IconButton'
import Select from 'js/components/Select/Select'
import Textfield from 'js/components/Textfield/Textfield'
import usePluginOptions from 'js/group/AddEditForm/hooks/usePluginOptions'

import RemoveIcon from 'icons/cross.svg'
import MoveIcon from 'icons/move-vertical.svg'

const Wrapper = styled.div`
    display: flex;

    .LinkItemFlexer {
        display: flex;
        flex-grow: 1;

        > * {
            flex-grow: 1;
        }

        > :nth-child(2) {
            .SelectContainer:before {
                border-left: none;
            }
        }
    }
`

const sortAlphabetically = (a, b) => (a.label > b.label ? 1 : -1)

const MenuItem = ({
    id,
    dragHandleProps,
    entity,
    groupPages = [],
    onChangePage,
    onChangeLabel,
    onChangeAccess,
    onRemove,
    ...rest
}) => {
    const { t } = useTranslation()

    const { type, plugin, access } = entity

    const isStartPage = id === 'startPage'

    const pluginOptions = usePluginOptions().sort(sortAlphabetically)

    const typeOptions = [
        ...(groupPages?.length > 0
            ? [
                  {
                      value: 'page',
                      label: t('global.page'),
                  },
              ]
            : []),
        {
            value: 'plugin',
            label: t('entity-group.template'),
        },
        {
            value: 'link',
            label: t('form.link'),
        },
    ]

    const visibilityOptions = [
        {
            onClick: () => onChangeAccess('public'),
            name: t('permissions.public'),
            active: access === 'public',
        },
        {
            onClick: () => onChangeAccess('loggedIn'),
            name: t('permissions.logged-in-users'),
            active: access === 'loggedIn',
        },
        {
            onClick: () => onChangeAccess('members'),
            name: t('permissions.members'),
            active: access === 'members',
        },
    ]

    const currentVisibilityLevel = useAccessTitle(access)

    return (
        <Wrapper {...rest}>
            {!!dragHandleProps && (
                <IconButton
                    as="div"
                    size="large"
                    variant="secondary"
                    {...dragHandleProps}
                >
                    <MoveIcon />
                </IconButton>
            )}

            <div className="LinkItemFlexer">
                {entity.type === 'submenu' ? (
                    <Textfield
                        name={`${id}-name`}
                        label={t('form.title')}
                        value={entity.label}
                        required
                        onChange={(evt) => onChangeLabel(evt.target.value)}
                    />
                ) : (
                    <>
                        {!isStartPage && (
                            <Select
                                name={`${id}-type`}
                                label={t('global.type')}
                                options={typeOptions}
                                borderStyle="rounded-left"
                                value={entity.type}
                                onChange={(type) =>
                                    onChangePage({
                                        type,
                                        id:
                                            type === 'page'
                                                ? groupPages[0].guid
                                                : type === 'plugin'
                                                  ? pluginOptions[0].value
                                                  : null,
                                        access,
                                    })
                                }
                                style={{ maxWidth: '120px' }}
                            />
                        )}

                        {/* Type: page */}
                        {(type === 'page' || isStartPage) && (
                            <>
                                {/* No access */}
                                {entity.id && entity.page === null ? (
                                    <Select
                                        name={`page-${id}`}
                                        label={t('global.page')}
                                        options={[
                                            {
                                                value: entity.id,
                                                label: t(
                                                    'error.no-access-to-this-page',
                                                ),
                                            },
                                        ]}
                                        value={entity.id}
                                        borderStyle="rounded-right"
                                        disabled
                                    />
                                ) : (
                                    <Select
                                        name={`page-${id}`}
                                        label={t('global.page')}
                                        options={groupPages.map((page) => {
                                            return {
                                                value: page.guid,
                                                label: page.title,
                                            }
                                        })}
                                        value={
                                            isStartPage
                                                ? entity.guid || entity
                                                : entity.page?.guid || entity.id
                                        }
                                        borderStyle={
                                            isStartPage
                                                ? 'rounded'
                                                : 'rounded-right'
                                        }
                                        onChange={(id) =>
                                            onChangePage({
                                                id,
                                                type: 'page',
                                                access,
                                            })
                                        }
                                    />
                                )}
                            </>
                        )}

                        {/* Type: plugin */}
                        {type === 'plugin' && (
                            <Select
                                name={`plugin-${id}`}
                                label={t('entity-group.template')}
                                options={pluginOptions}
                                value={plugin || entity.id}
                                borderStyle={
                                    isStartPage ? 'rounded' : 'rounded-right'
                                }
                                onChange={(id) =>
                                    onChangePage({
                                        id,
                                        type: 'plugin',
                                        access,
                                    })
                                }
                            />
                        )}

                        {/* Type: link */}
                        {type === 'link' && (
                            <>
                                <Textfield
                                    name={`${id}-label`}
                                    label={t('form.title')}
                                    value={entity.label}
                                    onChange={(evt) =>
                                        onChangePage({
                                            type: 'link',
                                            label: evt.target.value,
                                            link: entity.link,
                                            id: entity.id,
                                            access,
                                        })
                                    }
                                    borderStyle="square"
                                    style={{
                                        marginLeft: '-1px',
                                        marginRight: '-1px',
                                    }}
                                />
                                <Textfield
                                    name={`${id}-link`}
                                    label={t('form.link')}
                                    value={entity.link}
                                    onChange={(evt) =>
                                        onChangePage({
                                            type: 'link',
                                            label: entity.label,
                                            link: evt.target.value,
                                            id: evt.target.value,
                                            access,
                                        })
                                    }
                                    borderStyle="rounded-right"
                                />
                            </>
                        )}
                    </>
                )}
            </div>

            <Flexer gutter="small" divider="normal">
                {onChangeAccess && (
                    <DropdownButton
                        options={visibilityOptions}
                        placement="bottom-start"
                    >
                        <IconButton
                            variant="secondary"
                            size="large"
                            tooltip={currentVisibilityLevel}
                        >
                            <AccessIcon access={access} />
                        </IconButton>
                    </DropdownButton>
                )}

                {onRemove && (
                    <IconButton
                        size="large"
                        variant="secondary"
                        onClick={onRemove}
                    >
                        <RemoveIcon />
                    </IconButton>
                )}
            </Flexer>
        </Wrapper>
    )
}

export default MenuItem
