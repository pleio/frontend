import React from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { getGroupUrl } from 'helpers'

import EntityAddEditForm from 'js/components/EntityActions/EntityAddEditForm'
import NotFound from 'js/core/NotFound'
import { discussionEditFragment } from 'js/discussions/fragments/entity'

import getRefetchQueries from './helpers/getRefetchQueries'

const Edit = () => {
    const { t } = useTranslation()
    const navigate = useNavigate()
    const params = useParams()
    const { guid } = params

    const { loading, data } = useQuery(Query, {
        variables: {
            guid,
        },
    })

    if (loading) return null
    if (!data || !data.entity) return <NotFound />

    const { entity } = data

    const afterDelete = () => {
        navigate(`${getGroupUrl(params)}/discussion`, {
            replace: true,
        })
    }

    const refetchQueries = getRefetchQueries(true, !!entity.group)

    return (
        <EntityAddEditForm
            title={t('entity-discussion.edit-title')}
            deleteTitle={t('entity-discussion.delete-title')}
            subtype="discussion"
            entity={entity}
            afterDelete={afterDelete}
            refetchQueries={refetchQueries}
        />
    )
}

const Query = gql`
    query EditDiscussion($guid: String!) {
        entity(guid: $guid) {
            guid
            ${discussionEditFragment}
        }
    }
`

export default Edit
