import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'
import { useWindowScrollPosition } from 'helpers'

import { Container } from 'js/components/Grid/Grid'
import PageHeader from 'js/components/PageHeader/PageHeader'
import Section from 'js/components/Section/Section'
import FeedView from 'js/page/Widget/components/Feed/FeedView'

// First load data, otherwise useWindowScrollPosition and FeedView won't work properly
const ListWrapper = () => {
    const { data, loading } = useQuery(GET_LIST_SETTINGS)

    if (loading) return null
    return <List data={data} />
}

const List = ({ data }) => {
    const { viewer, site } = data

    const { t } = useTranslation()

    const settings = {
        showTagFilter: site?.pageTagFilters.showTagFilter,
        showTagCategories: site?.pageTagFilters.showTagCategories,
        sortingOptions: ['timePublished', 'lastAction'],
        typeFilter: ['discussion'],
        sortBy: 'lastAction',
        sortDirection: 'desc',
        itemView: 'list',
        itemCount: 10,
    }

    useWindowScrollPosition('discussions')

    return (
        <>
            <PageHeader
                title={t('entity-discussion.title-list')}
                canCreate={viewer && viewer.canWriteToContainer}
                createLink="/discussion/add"
                createLabel={t('entity-discussion.create')}
            />
            <Section>
                <Container size="tiny">
                    <FeedView guid="discussions" settings={settings} />
                </Container>
            </Section>
        </>
    )
}

const GET_LIST_SETTINGS = gql`
    query Discussions {
        viewer {
            guid
            canWriteToContainer(subtype: "discussion")
        }
        site {
            guid
            pageTagFilters(contentType: "discussion") {
                showTagFilter
                showTagCategories
            }
        }
    }
`

export default ListWrapper
