import React from 'react'

import FeedItem from 'js/components/FeedItem/FeedItem'
import FeedItemContent from 'js/components/FeedItem/FeedItemContent'
import FeedItemFooter from 'js/components/FeedItem/FeedItemFooter'
import FeedItemImage from 'js/components/FeedItem/FeedItemImage'

import getRefetchQueries from '../helpers/getRefetchQueries'

const Card = ({
    'data-feed': dataFeed,
    entity,
    canPin,
    hideSubtype,
    hideGroup,
    hideExcerpt,
    hideComments,
    hideLikes,
    hideActions,
    excerptMaxLines,
}) => {
    const onEdit = `${entity.group ? entity.group.url : ''}/discussion/edit/${
        entity.guid
    }`

    return (
        <FeedItem data-feed={dataFeed}>
            <FeedItemContent
                entity={entity}
                hideGroup={hideGroup}
                hideSubtype={hideSubtype}
                hideExcerpt={hideExcerpt}
                excerptMaxLines={excerptMaxLines}
                canPin={canPin}
                onEdit={onEdit}
                hideActions={hideActions}
                refetchQueries={getRefetchQueries(false, !!entity.group)}
            />
            <FeedItemFooter
                entity={entity}
                hideComments={hideComments}
                hideLikes={hideLikes}
                canUpvote
            />
            <FeedItemImage entity={entity} />
        </FeedItem>
    )
}

export default Card
