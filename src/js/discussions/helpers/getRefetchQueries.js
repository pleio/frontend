/**
 * Load the correct query names, depending on if we need
 * to update the detail view and/or group lists.
 *
 * @param {boolean} isDetail
 * @param {boolean} isInGoup
 */
export default (isDetail, isInGoup) => {
    const refetchQueries = ['ActivityList', 'Discussions']
    if (isDetail) {
        refetchQueries.push('DiscussionItem')
    }
    if (isInGoup) {
        refetchQueries.push('GroupDiscussions')
    }
    return refetchQueries
}
