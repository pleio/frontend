import React from 'react'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { getGroupUrl } from 'helpers'

import ItemLayout from 'js/components/Item/ItemLayout'
import NotFound from 'js/core/NotFound'
import { discussionViewFragment } from 'js/discussions/fragments/entity'

import getRefetchQueries from './helpers/getRefetchQueries'

const Item = () => {
    const params = useParams()
    const { guid, groupGuid } = params

    const { loading, data } = useQuery(GET_DISCUSSION_ITEM, {
        variables: {
            guid,
            groupGuid,
        },
    })

    if (loading) return null
    if (!data || !data.entity) return <NotFound />

    const { entity, viewer } = data
    const onEdit = `${getGroupUrl(params)}/discussion/edit/${entity.guid}`
    const onAfterDelete = `${getGroupUrl(params)}/discussion`
    const refetchQueries = getRefetchQueries(true, !!entity.group)

    return (
        <ItemLayout
            viewer={viewer}
            entity={entity}
            canUpvote
            onEdit={onEdit}
            onAfterDelete={onAfterDelete}
            refetchQueries={refetchQueries}
        />
    )
}

const GET_DISCUSSION_ITEM = gql`
    query DiscussionItem($guid: String!, $groupGuid: String) {
        viewer {
            guid
            loggedIn
            user {
                guid
                name
                icon
            }
            requiresCommentModeration(subtype: "discussion", groupGuid: $groupGuid)
        }
        entity(guid: $guid, incrementViewCount: true) {
            guid
            ... on Discussion {
                ${discussionViewFragment}
            }
        }
    }
`

export default Item
