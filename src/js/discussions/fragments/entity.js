import {
    entityEditFragment,
    entityViewFragment,
} from 'js/components/EntityActions/fragments/entity'

export const discussionEditFragment = `
... on Discussion {
    ${entityEditFragment}
}
`

export const discussionViewFragment = `
... on Discussion {
    ${entityViewFragment}
    publishRequest {
        status
    }
}
`
