import { featuredViewFragment } from 'js/lib/fragments/featured'
import { ownerFragment } from 'js/lib/fragments/owner'

const discussionListFragment = `
    fragment DiscussionListFragment on Discussion {
        guid
        localTitle
        localExcerpt
        title
        url
        excerpt
        ${featuredViewFragment}
        canEdit
        canArchiveAndDelete
        subtype
        votes
        hasVoted
        isBookmarked
        isPinned
        canBookmark
        tagCategories {
            name
            values
        }
        tags
        timePublished
        statusPublished
        views
        commentCount
        canComment
        isTranslationEnabled
        ${ownerFragment}
        group {
            guid
            ... on Group {
                isClosed
                name
                url
                membership
                isMembershipOnRequest
                isJoinButtonVisible
            }
        }
        publishRequest {
            status
        }
    }
`
export default discussionListFragment
