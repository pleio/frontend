import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigationType } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { useIsMount, useWindowScrollPosition } from 'helpers'

import blogListFragment from 'js/blog/fragments/listFragment'
import FetchMore from 'js/components/FetchMore'
import FilterWrapper from 'js/components/FilterWrapper'
import { Container } from 'js/components/Grid/Grid'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import PageHeader from 'js/components/PageHeader/PageHeader'
import Section from 'js/components/Section/Section'
import Select from 'js/components/Select/Select'
import discussionListFragment from 'js/discussions/fragments/listFragment'
import eventListFragment from 'js/events/fragments/listFragment'
import statusUpdateListFragment from 'js/group/StatusUpdate/fragments/listFragment.js'
import newsListFragment from 'js/news/fragments/listFragment'
import { podcastListFragment } from 'js/podcast/fragments/list'
import questionListFragment from 'js/questions/fragments/listFragment'
import wikiListFragment from 'js/wiki/fragments/listFragment'

import Card from './components/Card'

const List = () => {
    const { t } = useTranslation()
    const [subtype, setSubtype] = useState('all')

    const [queryLimit, setQueryLimit] = useState(5)
    const { loading, data, fetchMore, refetch } = useQuery(QUERY, {
        variables: {
            offset: 0,
            limit: queryLimit,
            subtype,
        },
    })

    useWindowScrollPosition('saved')

    const isMount = useIsMount()
    const navigationType = useNavigationType()

    useEffect(() => {
        if (isMount && navigationType !== 'POP') {
            refetch()
        }
    })

    return (
        <>
            <PageHeader title={t('saved.title')} />

            <Section>
                <Container size="tiny">
                    <FilterWrapper style={{ marginBottom: '16px' }}>
                        <Select
                            name="subtype"
                            label={t('global.type')}
                            options={[
                                {
                                    value: 'all',
                                    label: t('global.all'),
                                },
                                {
                                    value: 'blog',
                                    label: t('entity-blog.content-name'),
                                },
                                {
                                    value: 'news',
                                    label: t('entity-news.title-list'),
                                },
                                {
                                    value: 'discussion',
                                    label: t('entity-discussion.title-list'),
                                },
                                {
                                    value: 'question',
                                    label: t('entity-question.title-list'),
                                },
                                {
                                    value: 'event',
                                    label: t('entity-event.title-list'),
                                },
                                {
                                    value: 'podcast',
                                    label: t('entity-podcast.title-list'),
                                },
                                {
                                    value: 'statusupdate',
                                    label: t('global.status-updates'),
                                },
                                {
                                    value: 'wiki',
                                    label: t('entity-wiki.title-list'),
                                },
                            ]}
                            onChange={setSubtype}
                            value={subtype}
                        />
                    </FilterWrapper>

                    {loading ? (
                        <LoadingSpinner />
                    ) : (
                        data?.bookmarks && (
                            <FetchMore
                                edges={data.bookmarks.edges}
                                getMoreResults={(data) => data.bookmarks.edges}
                                fetchMore={fetchMore}
                                fetchType="click"
                                fetchCount={10}
                                setLimit={setQueryLimit}
                                maxLimit={data.bookmarks.total}
                            >
                                {data.bookmarks.edges.map((entity, i) => (
                                    <Card
                                        key={entity.guid}
                                        data-feed={i}
                                        entity={entity}
                                        viewer={data.viewer}
                                    />
                                ))}
                            </FetchMore>
                        )
                    )}
                </Container>
            </Section>
        </>
    )
}

const QUERY = gql`
    query BookmarkList($offset: Int!, $limit: Int!, $subtype: String) {
        viewer {
            guid
            loggedIn
            user {
                guid
                email
            }
        }
        bookmarks(offset: $offset, limit: $limit, subtype: $subtype) {
            total
            edges {
                guid
                ...StatusUpdateListFragment
                ...BlogListFragment
                ...DiscussionListFragment
                ...EventListFragment
                ...NewsListFragment
                ...QuestionListFragment
                ...WikiListFragment
                ${podcastListFragment}
            }
        }
    }
    ${statusUpdateListFragment}
    ${blogListFragment}
    ${discussionListFragment}
    ${eventListFragment}
    ${newsListFragment}
    ${questionListFragment}
    ${wikiListFragment}
`

export default List
