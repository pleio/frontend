import React from 'react'
import PropTypes from 'prop-types'

import BlogCard from 'js/blog/components/Card'
import DiscussionCard from 'js/discussions/components/Card'
import EventCard from 'js/events/components/Card'
import StatusUpdateCard from 'js/group/StatusUpdate/Card'
import NewsCard from 'js/news/components/Card'
import PodcastCard from 'js/podcast/components/PodcastCard'
import QuestionCard from 'js/questions/components/Card'
import WikiCard from 'js/wiki/components/Card'

const Card = ({ entity, ...rest }) => {
    switch (entity.__typename) {
        case 'Wiki':
            return <WikiCard entity={entity} {...rest} />

        case 'Blog':
            return <BlogCard entity={entity} {...rest} />

        case 'Discussion':
            return <DiscussionCard entity={entity} {...rest} />

        case 'Podcast':
            return <PodcastCard entity={entity} {...rest} />

        case 'Event':
            return <EventCard entity={entity} {...rest} />

        case 'News':
            return <NewsCard entity={entity} {...rest} />

        case 'StatusUpdate':
            return <StatusUpdateCard entity={entity} {...rest} />

        case 'Question':
            return <QuestionCard entity={entity} {...rest} />

        default:
            return null
    }
}

Card.propTypes = {
    entity: PropTypes.object.isRequired,
}

export default Card
