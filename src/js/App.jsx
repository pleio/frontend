import React, { useEffect } from 'react'

import withGlobalState from 'js/lib/withGlobalState'
import Router from 'js/router/Router'

import 'js/i18n/i18n'

const App = ({ globalState, setGlobalState }) => {
    const handleVisibilityChange = () => {
        if (globalState.inactiveWindow !== document.hidden) {
            // TODO: re-renders whole app (causes FeedView to reset filters)
            setGlobalState((newState) => {
                newState.inactiveWindow = document.hidden
            })
        }
    }

    const timeInterval = setTimeout(() => {
        const lastAcivity = localStorage.getItem('lastActvity')
        const diffMs = Math.abs(new Date(lastAcivity) - new Date()) // milliseconds between now & last activity
        const seconds = Math.floor(diffMs / 1000)
        const minutes = Math.floor(seconds / 60)
        if (minutes >= 10) {
            clearInterval(timeInterval)
            if (globalState.inactiveUser !== true) {
                setGlobalState((newState) => {
                    newState.inactiveUser = true
                })
            }
        }
    }, 1000)

    const setLastActivty = () => {
        if (globalState.inactiveUser !== false) {
            setGlobalState((newState) => {
                newState.inactiveUser = false
            })
        }
        localStorage.setItem('lastActvity', new Date())
    }

    const mouseClass = 'mouse-user'

    const handleKeyDown = (evt) => {
        if (evt.key === 'Tab') {
            document.body.classList.remove(mouseClass)
            window.removeEventListener('keydown', handleKeyDown)
            window.addEventListener('mousedown', handleMouseDown)
        }
    }

    const handleMouseDown = () => {
        document.body.classList.add(mouseClass)
        window.removeEventListener('mousedown', handleMouseDown)
        window.addEventListener('keydown', handleKeyDown)
    }

    const registerServiceWorker = () => {
        if ('serviceWorker' in navigator) {
            const serviceWorker = document.querySelector(
                'meta[name="service-worker-js"]',
            )
            if (serviceWorker)
                navigator.serviceWorker.register(serviceWorker.content)
        }
    }

    useEffect(() => {
        window.addEventListener('visibilitychange', handleVisibilityChange)
        window.addEventListener('mousemove', setLastActivty)
        window.addEventListener('keypress', setLastActivty)
        window.addEventListener('mousedown', handleMouseDown)
        registerServiceWorker()

        return () => {
            window.removeEventListener(
                'visibilitychange',
                handleVisibilityChange,
            )
            window.removeEventListener('mousemove', setLastActivty)
            window.removeEventListener('keypress', setLastActivty)
            clearInterval(timeInterval)
            window.removeEventListener('keydown', handleKeyDown)
            window.removeEventListener('mousedown', handleMouseDown)
        }
    })

    return <Router />
}

export default withGlobalState(App)
