import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'
import { sanitizeTagCategories } from 'helpers'

import Button from 'js/components/Button/Button'
import CustomTagsField from 'js/components/CustomTagsField'
import InputLanguage from 'js/components/EntityActions/General/components/InputLanguage'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import Spacer from 'js/components/Spacer/Spacer'
import TagCategoriesField from 'js/components/TagCategories/TagCategoriesField'
import useCustomTags from 'js/page/Widget/hooks/useCustomTags'

const Settings = ({ site, entity, onClose }) => {
    const { t } = useTranslation()

    const isTranslationEnabled = entity ? entity.isTranslationEnabled : true

    const [editPad, { error: mutationErrors }] = useMutation(gql`
        mutation EditPad($input: editPadInput!) {
            editPad(input: $input) {
                entity {
                    guid
                    ... on Pad {
                        title
                        inputLanguage
                        tags
                        tagCategories {
                            name
                            values
                        }
                    }
                }
            }
        }
    `)

    const [categoryTags, setCategoryTags] = useState(
        entity?.tagCategories || [],
    )
    const { customTags, handleAddTags, handleRemoveTags } = useCustomTags(
        entity?.tags,
    )

    const defaultValues = {
        title: entity?.title || null,
        inputLanguage: entity?.inputLanguage || null,
        isTranslationEnabled,
    }

    const {
        control,
        handleSubmit,
        watch,
        formState: { errors, isValid, isSubmitting },
    } = useForm({
        mode: 'onChange',
        defaultValues,
    })

    const submit = async ({ title, inputLanguage, isTranslationEnabled }) => {
        await editPad({
            variables: {
                input: {
                    guid: entity.guid,
                    title,
                    inputLanguage,
                    isTranslationEnabled,
                    tagCategories: sanitizeTagCategories(
                        site.tagCategories,
                        categoryTags,
                    ),
                    tags: customTags.toJS(),
                },
            },
        }).then(() => {
            onClose()
        })
    }

    const onError = (errors) => {
        return new Promise((resolve, reject) => {
            reject(new Error(errors))
        })
    }

    const { contentTranslation, languageOptions } = site
    const showInputLanuage = contentTranslation && languageOptions?.length > 1
    const showLanguage = watch('isTranslationEnabled')

    return (
        <form onSubmit={handleSubmit(submit, onError)}>
            <Spacer>
                <FormItem
                    control={control}
                    type="text"
                    name="title"
                    title={t('form.title')}
                    required
                    errors={errors}
                />

                {showInputLanuage && (
                    <InputLanguage
                        control={control}
                        languageOptions={site.languageOptions}
                        showInputLanguage={showLanguage}
                    />
                )}

                <TagCategoriesField
                    name="filters"
                    tagCategories={site.tagCategories}
                    value={categoryTags}
                    setTags={setCategoryTags}
                />
                {site.customTagsAllowed && (
                    <CustomTagsField
                        name="customTags"
                        value={customTags}
                        onAddTag={handleAddTags}
                        onRemoveTag={handleRemoveTags}
                    />
                )}
            </Spacer>

            <Errors errors={mutationErrors} />

            <Flexer mt>
                <Button size="normal" variant="tertiary" onClick={onClose}>
                    {t('action.cancel')}
                </Button>
                <Button
                    type="submit"
                    size="normal"
                    variant="primary"
                    disabled={!isValid}
                    loading={isSubmitting}
                >
                    {t('action.save')}
                </Button>
            </Flexer>
        </form>
    )
}

export default Settings
