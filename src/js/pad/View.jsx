import React, { useState } from 'react'
import styled from 'styled-components'

import { Container } from 'js/components/Grid/Grid'
import ToggleTranslation from 'js/components/Item/components/ToggleTranslation'
import TiptapView from 'js/components/Tiptap/TiptapView'

const Wrapper = styled(Container)`
    padding-top: 24px;
    padding-bottom: 24px;
`

// Does the Tiptap JSON contain any content?
const hasContent = (text = '') => {
    if (!text.length) return false

    try {
        const parsedText = JSON.parse(text)
        return !!parsedText.content.length
    } catch (error) {
        return false
    }
}

const FileViewContent = ({ entity, ...rest }) => {
    const { localRichDescription, richDescription, isTranslationEnabled } =
        entity

    const hasTranslations = !!localRichDescription && isTranslationEnabled
    const [isTranslated, setIsTranslated] = useState(hasTranslations)

    const translatedRichDescription = isTranslated
        ? localRichDescription
        : richDescription

    return (
        <Wrapper size="normal" {...rest}>
            <TiptapView content={translatedRichDescription} textSize="large" />

            {hasTranslations && hasContent(localRichDescription) && (
                <ToggleTranslation
                    isTranslated={isTranslated}
                    setIsTranslated={setIsTranslated}
                    style={{ marginTop: '12px' }}
                />
            )}
        </Wrapper>
    )
}

export default FileViewContent
