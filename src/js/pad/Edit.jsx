import React, {
    useContext,
    useEffect,
    useLayoutEffect,
    useMemo,
    useState,
} from 'react'
import { useTranslation } from 'react-i18next'
import {
    Link,
    Navigate,
    useLocation,
    useNavigate,
    useParams,
} from 'react-router-dom'
import { gql, useMutation, useQuery } from '@apollo/client'
import { HocuspocusProvider } from '@hocuspocus/provider'
import styled, { css } from 'styled-components'
import * as Y from 'yjs'

import { AriaLiveContext, AriaLiveMessage } from 'js/components/AriaLive'
import DisplayDate from 'js/components/DisplayDate'
import DropdownButton from 'js/components/DropdownButton/DropdownButton'
import Flexer from 'js/components/Flexer/Flexer'
import { Container } from 'js/components/Grid/Grid'
import HeaderBar from 'js/components/HeaderBar/HeaderBar'
import HideVisually from 'js/components/HideVisually/HideVisually'
import IconButton from 'js/components/IconButton/IconButton'
import DeleteEntitiesModal from 'js/components/Item/ItemActions/components/DeleteEntitiesModal'
import Modal from 'js/components/Modal/Modal'
import TiptapEditor from 'js/components/Tiptap/TiptapEditor'
import Tooltip from 'js/components/Tooltip/Tooltip'
import NotFound from 'js/core/NotFound'

import BackIcon from 'icons/arrow-left.svg'
import CloudOffIcon from 'icons/cloud-off.svg'
import CloudIcon from 'icons/cloud-small.svg'
import VersionHistoryIcon from 'icons/history.svg'
import OptionsIcon from 'icons/options.svg'
import WarnIcon from 'icons/warn.svg'

import Settings from './components/Settings'

const Wrapper = styled.div`
    flex-grow: 1;
    display: flex;
    flex-direction: column;

    .PadHeaderPlaceholder {
        height: ${(p) => p.theme.headerBarHeight}px;
    }

    .PadHeader {
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        z-index: 11;
        height: ${(p) => p.theme.headerBarHeight}px;
    }

    .PadHeaderContainer {
        height: 100%;
        display: flex;
        align-items: center;
    }

    .PadHeaderMeta {
        flex-grow: 1;
        display: flex;
        flex-direction: column;
        margin-top: -1px;
    }

    .PadHeaderEdited {
        min-height: 16px;
        margin-top: -1px;
        margin-bottom: -1px;
        display: flex;
        flex-wrap: wrap;
        align-items: center;
        font-size: ${(p) => p.theme.font.size.tiny};
        line-height: ${(p) => p.theme.font.lineHeight.tiny};
        color: ${(p) => p.theme.color.text.grey};

        svg {
            height: 16px;
        }
    }

    .PadHeaderError {
        display: flex;
        align-items: center;
        color: ${(p) => p.theme.color.warn.main};
    }

    .PadEditorWrapper {
        position: relative;
        flex-grow: 1;
        display: flex;
    }

    .PadEditorErrorOverlay {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: rgba(0, 0, 0, 0.2);
    }

    .PadEditor {
        flex-grow: 1;
    }

    .ProseMirror {
        padding-top: 24px;
        padding-bottom: 24px;
    }

    .TiptapEditorLabel {
        top: 24px;
    }

    .TiptapEditorContentWrapper {
        margin: 0 auto;
        width: 100%;
        max-width: ${(p) => p.theme.containerWidth.normal};
    }
`

const TitleInput = styled.input`
    width: 100%;
    padding: 0;
    margin-bottom: 2px;
    margin-left: -1px;
    border: 1px solid transparent;
    font-size: ${(p) => p.theme.font.size.small};
    line-height: ${(p) => p.theme.font.lineHeight.small};
    font-weight: ${(p) => p.theme.font.weight.semibold};
    height: ${(p) => p.theme.font.lineHeight.small};
    border: 1px solid transparent;
    outline-offset: 0;

    &::placeholder {
        color: ${(p) => p.theme.color.text.grey};
    }

    &:focus {
        outline: ${(p) => p.theme.focusStyling};
    }

    ${(p) =>
        p.$isInvalid &&
        css`
            border-color: ${(p) => p.theme.color.warn.main};
        `};
`

const ErrorWrapper = ({ children }) => (
    <Flexer
        justifyContent="flex-start"
        gutter="tiny"
        className="PadHeaderError"
    >
        <WarnIcon />
        <span>{children}</span>
        <AriaLiveMessage message={children} />
    </Flexer>
)

const Edit = () => {
    const { t } = useTranslation()
    const location = useLocation()
    const navigate = useNavigate()

    const [timeUpdated, setTimeUpdated] = useState(null)
    const [showDeleteModal, setShowDeleteModal] = useState(false)
    const [showSettingsModal, setShowSettingsModal] = useState(false)

    const params = useParams()
    const { guid } = params

    // Keep track of latest state of the pad content
    // When the user navigates to the view page we need to pass this latest state
    // We can't update via the server because it might be lagging behind
    // because server updates via websocket are written only once every 5 seconds
    const [tiptapEditor, setTiptapEditor] = useState(null)
    const [editorState, setEditorState] = useState('')

    const { loading, data } = useQuery(EDIT_PAD, { variables: { guid } })

    const [status, setStatus] = useState('connecting')
    const [error, setError] = useState(null)

    const [invalidTitle, setInvalidTitle] = useState(false)
    const [titleField, setTitleField] = useState('')

    const { announcePolite } = useContext(AriaLiveContext)

    useEffect(() => {
        if (data?.entity?.title) {
            setTitleField(data.entity.title)
        }
    }, [data?.entity?.title])

    useLayoutEffect(() => {
        if (!timeUpdated && data?.entity?.timeUpdated) {
            setTimeUpdated(data?.entity?.timeUpdated)
        }
    }, [timeUpdated, data])

    const [mutateEditTitle] = useMutation(EDIT_FILE_FOLDER, {
        refetchQueries: ['FilesList'],
    })

    const provider = useMemo(() => {
        if (!data?.site.collabEditingEnabled || !data?.entity?.guid) return null

        const urlProtocol = window.location.protocol === 'https:' ? 'wss' : 'ws'
        const provider = new HocuspocusProvider({
            url: `${urlProtocol}://${window.location.host}/collab`,
            name: data.entity.guid,
            document: new Y.Doc(),
            token: data.viewer.user?.guid,
            onMessage({ message }) {
                const decoded = new TextDecoder('utf-8').decode(message.data)

                if (decoded.includes(`SAVED:${data.entity.guid}`)) {
                    setError()
                    setTimeUpdated(new Date().toISOString())
                    announcePolite(t('entity-pad.pad-is-saved'))
                } else if (
                    decoded.includes(`ERRORONSAVE:${data.entity.guid}`)
                ) {
                    setError('ERRORONSAVE')
                }
            },
            onStatus({ status }) {
                setStatus(status)
            },
        })

        return provider
    }, [
        data?.entity?.guid,
        data?.site.collabEditingEnabled,
        data?.viewer.user?.guid,
    ])

    const submitTitle = async (evt) => {
        const title = evt.target.value
        const trimmedTitle = title.trim()

        if (!trimmedTitle) {
            setInvalidTitle(true)
            return
        }

        setInvalidTitle(false)

        if (trimmedTitle === data?.entity?.title) {
            return
        }

        await mutateEditTitle({
            variables: {
                input: {
                    guid,
                    title: trimmedTitle,
                },
            },
        })
    }

    if (loading) return null
    if (!data || !data?.entity || !data?.viewer) return <NotFound />

    const { entity, viewer, site } = data

    if (!entity.canEdit) {
        // If the user is not allowed to edit the file, redirect to the view page
        return <Navigate to={entity.url} replace={true} />
    }

    const { url } = entity

    const getEditorState = () =>
        tiptapEditor
            ? tiptapEditor.isEmpty
                ? ''
                : JSON.stringify(tiptapEditor.getJSON())
            : ''

    const hasContent = entity.richDescription
        ? JSON.parse(entity.richDescription).content.length > 0
        : false

    return (
        <Wrapper>
            <div className="PadHeaderPlaceholder" />
            <HeaderBar
                className="PadHeader"
                $backgroundColor="white"
                data-sticky="true"
            >
                <Container className="PadHeaderContainer">
                    <IconButton
                        as={Link}
                        to={location?.state?.prevPathname || url}
                        size="large"
                        variant="secondary"
                        tooltip={t('action.go-back')}
                        offset={[0, 4]}
                        state={{
                            richDescription: editorState,
                        }}
                        onMouseDown={() => setEditorState(getEditorState())}
                    >
                        <BackIcon />
                    </IconButton>

                    <div className="PadHeaderMeta">
                        <TitleInput
                            $isInvalid={invalidTitle}
                            type="text"
                            value={titleField}
                            onChange={(evt) => setTitleField(evt.target.value)}
                            onBlur={submitTitle}
                            onFocus={(evt) => evt.target.select()}
                        />

                        <Flexer
                            justifyContent="flex-start"
                            gutter="tiny"
                            className="PadHeaderEdited"
                        >
                            {status === 'connected' ? (
                                <>
                                    <Tooltip
                                        content={t('entity-pad.connected')}
                                        theme="accept-white"
                                    >
                                        <div>
                                            <CloudIcon />
                                            <HideVisually>
                                                {t('entity-pad.connected')}
                                            </HideVisually>
                                        </div>
                                    </Tooltip>
                                    {timeUpdated && (
                                        <span>
                                            {t('global.last-saved')}:{'  '}
                                            <DisplayDate
                                                date={timeUpdated}
                                                type="timeSince"
                                                placement="right"
                                            />
                                        </span>
                                    )}
                                    {error === 'ERRORONSAVE' && (
                                        <ErrorWrapper>
                                            {t('entity-pad.error-on-save')}
                                        </ErrorWrapper>
                                    )}
                                </>
                            ) : status === 'connecting' ? (
                                <>
                                    <CloudOffIcon />
                                    <span>{t('entity-pad.connecting')}</span>
                                    <AriaLiveMessage
                                        message={t('entity-pad.connecting')}
                                    />
                                </>
                            ) : (
                                <>
                                    <CloudOffIcon />
                                    <span>
                                        {t('entity-pad.no-connection-error')}
                                    </span>
                                </>
                            )}
                        </Flexer>
                    </div>

                    <Flexer gutter="none">
                        <IconButton
                            as={Link}
                            to={`/${guid}/version-history`}
                            size="large"
                            variant="secondary"
                            tooltip={t('version-history.title')}
                            offset={[0, 4]}
                            state={{
                                refetchTimeout: 5_000,
                                prevPathname: location.pathname,
                            }}
                        >
                            <VersionHistoryIcon />
                        </IconButton>
                        <DropdownButton
                            options={[
                                {
                                    name: t('global.settings'),
                                    onClick: () => setShowSettingsModal(true),
                                },
                                {
                                    name: t('action.delete'),
                                    onClick: () => setShowDeleteModal(true),
                                },
                            ]}
                            placement="bottom-start"
                        >
                            <IconButton
                                variant="secondary"
                                size="large"
                                tooltip={t('global.options')}
                                aria-label={t('action.show-options')}
                            >
                                <OptionsIcon />
                            </IconButton>
                        </DropdownButton>

                        <Modal
                            isVisible={showSettingsModal}
                            onClose={() =>
                                setShowSettingsModal(!showSettingsModal)
                            }
                            title={t('global.settings')}
                            size="normal"
                        >
                            <Settings
                                onClose={() =>
                                    setShowSettingsModal(!showSettingsModal)
                                }
                                entity={entity}
                                site={site}
                                viewer={viewer}
                            />
                        </Modal>

                        <DeleteEntitiesModal
                            isVisible={showDeleteModal}
                            onClose={() => setShowDeleteModal(false)}
                            entities={[entity]}
                            onComplete={() => {
                                const container = entity?.container
                                const typeName = container?.__typename
                                const containerLink =
                                    typeName === 'Group'
                                        ? `${container.url}/files`
                                        : typeName === 'User'
                                          ? `/user/${container.guid}/files`
                                          : typeName === 'Folder'
                                            ? container.url
                                            : '/'

                                navigate(containerLink)
                            }}
                            refetchQueries={['FilesList']}
                        />
                    </Flexer>
                </Container>
            </HeaderBar>
            <TiptapEditor
                name={`${entity.guid}-editor}`}
                content={entity.richDescription}
                collab={{
                    guid: entity.guid,
                    user: viewer.user,
                    provider,
                }}
                textSize="large"
                options={{
                    textAnchor: true,
                    textCSSClass: true,
                    textFormat: true,
                    textStyle: true,
                    textLink: true,
                    textQuote: true,
                    textList: true,
                    insertMedia: true,
                    insertAccordion: true,
                    insertButton: true,
                    insertTable: true,
                    insertMention: true,
                    textLanguage: true,
                }}
                borderStyle="none"
                placeholder={hasContent ? '' : t('form.start-typing')}
                getEditorInstance={setTiptapEditor}
                group={entity.group}
                className="PadEditor"
            />
        </Wrapper>
    )
}

const EDIT_PAD = gql`
    query EditPad($guid: String!) {
        site {
            guid
            collabEditingEnabled
            customTagsAllowed
            contentTranslation
            languageOptions {
                value
                label
            }
            tagCategories {
                name
                values
            }
        }
        viewer {
            guid
            user {
                guid
                name
            }
        }
        entity(guid: $guid) {
            guid
            ... on Pad {
                title
                richDescription
                url
                canEdit
                timeUpdated
                subtype
                inputLanguage
                isTranslationEnabled
                tags
                tagCategories {
                    name
                    values
                }
                group {
                    guid
                    isClosed
                }
                container {
                    guid
                    ... on User {
                        url
                    }
                    ... on Group {
                        url
                    }
                    ... on FileFolder {
                        url
                    }
                }
            }
        }
    }
`

const EDIT_FILE_FOLDER = gql`
    mutation editFileFolder($input: editFileFolderInput!) {
        editFileFolder(input: $input) {
            entity {
                guid
                ... on Pad {
                    title
                }
            }
        }
    }
`

export default Edit
