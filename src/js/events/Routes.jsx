import React from 'react'
import { Route, Routes } from 'react-router-dom'

import NotFound from 'js/core/NotFound'

import ConfirmByEmail from './ConfirmByEmail/ConfirmByEmail'
import Item from './Item'
import List from './List'

const Component = () => (
    <Routes>
        <Route path="/" element={<List />} />
        <Route path="/view/:guid" element={<Item />} />
        <Route path="/view/:guid/:slug" element={<Item />} />
        <Route path="/confirm/:guid" element={<ConfirmByEmail />} />
        <Route path="*" element={<NotFound />} />
    </Routes>
)

// Test: http://pleio.test:8080/events/confirm/791?code=23rq2332432&email=jorn@pleio.nl

export default Component
