import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'
import { useDebounce } from 'helpers'

import EntityList from 'js/components/EntityList/EntityList'
import FetchMore from 'js/components/FetchMore'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import SearchBar from 'js/components/SearchBar/SearchBar'
import { useIsMount } from 'js/lib/helpers'

import InvitedItem from './InvitedItem'

const Invited = ({ guid }) => {
    const { t } = useTranslation()

    const [queryLimit, setQueryLimit] = useState(50)

    const onChangeSearchInput = (evt) => {
        setSearchInput(evt.target.value)
    }

    const [searchInput, setSearchInput] = useState('')
    const q = useDebounce(searchInput, 200)

    const { loading, data, fetchMore, refetch } = useQuery(GET_INVITED_LIST, {
        variables: {
            q,
            guid,
            limit: queryLimit,
        },
    })

    const isMount = useIsMount()
    useEffect(() => {
        if (isMount) {
            refetch()
        }
    }, [isMount, refetch])

    return (
        <>
            <SearchBar
                name="search-attendee"
                value={searchInput}
                onChange={onChangeSearchInput}
                label={t('entity-event.search-invited')}
                style={{ margin: '20px 0 8px' }}
            />
            {loading ? (
                <LoadingSpinner />
            ) : (
                <FetchMore
                    edges={data.entity.attendees.edges}
                    getMoreResults={(data) => data.entity.attendees.edges}
                    fetchMore={fetchMore}
                    fetchCount={50}
                    setLimit={setQueryLimit}
                    maxLimit={data.entity.attendees.total}
                    isScrollableBox
                >
                    <EntityList>
                        {data.entity.attendees.edges.map((invited) => (
                            <InvitedItem
                                key={`invited-${invited.guid || invited.email}`}
                                eventGuid={guid}
                                invited={invited}
                            />
                        ))}
                    </EntityList>
                </FetchMore>
            )}
        </>
    )
}

const GET_INVITED_LIST = gql`
    query InvitedList($guid: String!, $limit: Int!, $q: String) {
        entity(guid: $guid) {
            guid
            ... on Event {
                attendees(
                    limit: $limit
                    query: $q
                    orderBy: timeUpdated
                    orderDirection: desc
                    isInvited: true
                    state: "unconfirmed"
                ) {
                    total
                    edges {
                        guid
                        icon
                        url
                        name
                        email
                    }
                }
            }
        }
    }
`

export default Invited
