import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'
import { enableMapSet, produce } from 'immer'

import AddUserField from 'js/components/AddUserField'
import Button from 'js/components/Button/Button'
import EntityList from 'js/components/EntityList/EntityList'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import { H4 } from 'js/components/Heading'
import IconButton from 'js/components/IconButton/IconButton'
import Text from 'js/components/Text/Text'
import UserLink from 'js/components/UserLink'

import RemoveIcon from 'icons/cross.svg'

enableMapSet()

const Invite = ({ guid, onComplete }) => {
    const { t } = useTranslation()

    const [inviteToEvent, { loading, error }] = useMutation(INVITE_TO_EVENT)

    const [invites, setInvites] = useState([])

    const addInvite = (invite) => {
        setInvites(
            produce((newState) => {
                newState.push(invite)
            }),
        )
    }

    const removeInvite = (index) => {
        setInvites(
            produce((newState) => {
                newState.splice(index, 1)
            }),
        )
    }

    const submit = async () => {
        await inviteToEvent({
            variables: {
                input: {
                    guid,
                    users: invites.map(({ guid, email }) =>
                        guid ? { guid } : { email },
                    ),
                },
            },
        })
            .then(() => {
                onComplete()
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    return (
        <>
            <AddUserField
                label={t('entity-event.add-invite')}
                users={invites}
                onAddUser={addInvite}
                allowEmail
                style={{ paddingTop: '16px' }}
            />

            {invites.length > 0 && (
                <>
                    <H4 style={{ marginTop: '16px' }}>
                        {t('entity-event.count-invite', {
                            count: invites.length,
                        })}
                    </H4>
                    <EntityList
                        style={{
                            overflowY: 'auto',
                        }}
                    >
                        {invites.map((invite, i) => {
                            const isUser = !!invite.guid
                            return (
                                <Flexer
                                    key={i}
                                    style={{
                                        alignItems: 'center',
                                        justifyContent: 'space-between',
                                    }}
                                >
                                    <UserLink entity={invite} isEmail={!isUser}>
                                        {isUser && (
                                            <Text size="small" variant="grey">
                                                {invite.email}
                                            </Text>
                                        )}
                                    </UserLink>

                                    <Flexer>
                                        <IconButton
                                            size="large"
                                            variant="secondary"
                                            onClick={() => removeInvite(i)}
                                            tooltip={t('action.remove')}
                                            aria-label={t(
                                                'event.remove-invite',
                                            )}
                                        >
                                            <RemoveIcon />
                                        </IconButton>
                                    </Flexer>
                                </Flexer>
                            )
                        })}
                    </EntityList>
                </>
            )}

            <Errors errors={error} />

            <Flexer mt>
                <Button
                    size="normal"
                    variant="primary"
                    loading={loading}
                    disabled={invites.length === 0}
                    onClick={submit}
                >
                    {t('action.invite')}
                </Button>
            </Flexer>
        </>
    )
}

const INVITE_TO_EVENT = gql`
    mutation InviteToEvent($input: inviteToEventInput!) {
        inviteToEvent(input: $input) {
            event {
                ... on Event {
                    guid
                }
            }
            status {
                item
                status
            }
        }
    }
`

export default Invite
