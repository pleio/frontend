import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import IconButton from 'js/components/IconButton/IconButton'
import ConfirmationPopover from 'js/components/Popover/ConfirmationPopover'
import Text from 'js/components/Text/Text'
import UserLink from 'js/components/UserLink'

import RemoveIcon from 'icons/cross.svg'

const InvitedItem = ({ eventGuid, invited }) => {
    const { t } = useTranslation()

    const [inviteSent, setInviteSent] = useState(false)
    const [showResendInvite, setShowResendInvite] = useState(false)
    const [resendInvite, { loading: resendLoading, error: resendError }] =
        useMutation(RESEND_INVITE)
    const handleResendInvite = async () => {
        await resendInvite({
            variables: {
                input: {
                    guid: eventGuid,
                    users: [{ email }],
                    resend: true,
                },
            },
        })
            .then(() => {
                setShowResendInvite(false)
                setInviteSent(true)
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    const [showDeleteInvited, setShowDeleteInvited] = useState(false)
    const [deleteInvited, { loading: deleteLoading, error: deleteError }] =
        useMutation(DELETE_INVITED)
    const handleDeleteInvited = async () => {
        await deleteInvited({
            variables: {
                input: {
                    guid: eventGuid,
                    emailAddresses: [email],
                },
            },
            refetchQueries: ['InvitedList'],
        })
            .then(() => {
                setShowDeleteInvited(false)
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    const { guid, email } = invited
    const isUser = !!guid

    return (
        <Flexer
            key={`invited-${guid || email}`}
            style={{
                alignItems: 'center',
                justifyContent: 'space-between',
            }}
        >
            <UserLink
                entity={invited}
                isEmail={!isUser}
                style={{
                    width: '100%',
                    flex: 1,
                }}
            >
                <Text size="small" variant="grey">
                    {email}
                </Text>
            </UserLink>
            <Flexer gutter="tiny">
                {inviteSent ? (
                    <Text size="small">{t('global.invitation-sent')}</Text>
                ) : (
                    <div>
                        <ConfirmationPopover
                            visible={showResendInvite}
                            setVisible={setShowResendInvite}
                            title={t('entity-event.resend-invite')}
                            description={t('global.are-you-sure')}
                            onConfirm={handleResendInvite}
                            loading={resendLoading}
                            error={resendError}
                        >
                            <Button
                                size="normal"
                                variant="secondary"
                                aria-label={t('entity-event.resend-invite')}
                                onClick={() =>
                                    setShowResendInvite(!showResendInvite)
                                }
                            >
                                {t('action.resend')}
                            </Button>
                        </ConfirmationPopover>
                    </div>
                )}

                <div>
                    <ConfirmationPopover
                        visible={showDeleteInvited}
                        setVisible={setShowDeleteInvited}
                        title={t('entity-event.remove-invited')}
                        description={t('global.are-you-sure')}
                        onConfirm={handleDeleteInvited}
                        loading={deleteLoading}
                        error={deleteError}
                    >
                        <IconButton
                            size="large"
                            hoverSize="normal"
                            variant="secondary"
                            aria-label={t('entity-event.remove-invited')}
                            tooltip={t('action.remove')}
                            onClick={() =>
                                setShowDeleteInvited(!showDeleteInvited)
                            }
                        >
                            <RemoveIcon />
                        </IconButton>
                    </ConfirmationPopover>
                </div>
            </Flexer>
        </Flexer>
    )
}

const RESEND_INVITE = gql`
    mutation ResendInvite($input: inviteToEventInput!) {
        inviteToEvent(input: $input) {
            event {
                ... on Event {
                    guid
                }
            }
            status {
                item
                status
            }
        }
    }
`

const DELETE_INVITED = gql`
    mutation RemoveInvited($input: deleteEventAttendeesInput!) {
        deleteEventAttendees(input: $input) {
            entity {
                guid
            }
        }
    }
`

export default InvitedItem
