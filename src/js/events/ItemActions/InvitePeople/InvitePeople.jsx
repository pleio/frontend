import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'

import TabMenu from 'js/components/TabMenu'
import TabPage from 'js/components/TabPage'

import Invite from './components/Invite'
import Invited from './components/Invited'

const InvitePeople = ({ guid }) => {
    const { t } = useTranslation()

    const [tab, setTab] = useState('invite')
    const tabOptions = [
        {
            label: t('action.invite'),
            onClick: () => setTab('invite'),
            isActive: tab === 'invite',
            id: 'tab-invite',
            ariaControls: 'tabpanel-invite',
        },
        {
            label: t('global.invited'),
            onClick: () => setTab('invited'),
            isActive: tab === 'invited',
            id: 'tab-invited',
            ariaControls: 'tabpanel-invited',
        },
    ]

    return (
        <>
            <TabMenu
                label={t('action.invite')}
                options={tabOptions}
                showBorder
                edgePadding
                edgeMargin
            />
            <TabPage
                id={`tabpanel-${tab}`}
                aria-labelledby={`tab-${tab}`}
                visible
            >
                {tab === 'invite' && (
                    <Invite guid={guid} onComplete={() => setTab('invited')} />
                )}
                {tab === 'invited' && <Invited guid={guid} />}
            </TabPage>
        </>
    )
}

export default InvitePeople
