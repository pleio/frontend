import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { Trans, useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'

import Button from 'js/components/Button/Button'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import Spacer from 'js/components/Spacer/Spacer'

const AddAttendee = ({ guid, onClose }) => {
    const { t } = useTranslation()

    const [queryErrors, setQueryErrors] = useState()
    const [success, setSuccess] = useState(false)

    const [mutate] = useMutation(FORCE_ADD_ATTENDEE)

    const submit = async ({ name, email }) => {
        await mutate({
            variables: {
                input: {
                    guid,
                    name,
                    email,
                },
            },
        })
            .then(() => {
                setSuccess(true)
            })
            .catch((errors) => {
                setQueryErrors(errors)
                console.error(errors)
            })
    }

    const defaultValues = {
        name: '',
        email: '',
    }

    const {
        control,
        handleSubmit,
        reset,
        getValues,
        formState: { errors, isValid, isSubmitting },
    } = useForm({ mode: 'onChange', defaultValues })

    const resetForm = () => {
        reset()
        setSuccess(false)
    }

    if (success) {
        return (
            <>
                <Trans i18nKey="entity-event.added-to-guest-list">
                    You added <strong>{{ name: getValues('name') }}</strong>{' '}
                    <strong>({{ email: getValues('email') }})</strong> to the
                    guest list! Go back to add another.
                </Trans>
                <Flexer mt>
                    <Button
                        size="normal"
                        variant="secondary"
                        type="button"
                        onClick={resetForm}
                    >
                        {t('action.go-back')}
                    </Button>
                    <Button size="normal" variant="primary" onClick={onClose}>
                        {t('action.close')}
                    </Button>
                </Flexer>
            </>
        )
    }

    return (
        <Spacer
            as="form"
            spacing="small"
            noValidate
            onSubmit={handleSubmit(submit)}
        >
            <div>{t('entity-event.add-to-guest-list')}</div>
            <FormItem
                control={control}
                type="text"
                name="name"
                label={t('global.name')}
                required
                errors={errors}
            />
            <FormItem
                control={control}
                type="email"
                name="email"
                label={t('form.email-address')}
                errors={errors}
                required
            />
            <Errors errors={queryErrors} />
            <Flexer mt>
                <Button
                    size="normal"
                    variant="tertiary"
                    type="button"
                    onClick={onClose}
                >
                    {t('action.cancel')}
                </Button>
                <Button
                    size="normal"
                    variant="primary"
                    type="submit"
                    disabled={!isValid}
                    loading={isSubmitting}
                >
                    {t('action.confirm')}
                </Button>
            </Flexer>
        </Spacer>
    )
}

const FORCE_ADD_ATTENDEE = gql`
    mutation AttendEventWithoutAccount(
        $input: attendEventWithoutAccountInput!
    ) {
        attendEventWithoutAccount(input: $input) {
            entity {
                guid
                ... on Event {
                    attendees(
                        limit: 3
                        state: "accept"
                        orderBy: timeUpdated
                        orderDirection: desc
                    ) {
                        total
                        totalAccept
                        totalMaybe
                        totalOnline
                        totalReject
                        totalWaitinglist
                        edges {
                            guid
                            name
                            state
                            url
                            icon
                        }
                    }
                }
            }
        }
    }
`

export default AddAttendee
