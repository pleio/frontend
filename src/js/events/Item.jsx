import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useLocation, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { format, formatISO } from 'date-fns'
import { getGroupUrl, media } from 'helpers'
import showDate, { isSameDate } from 'helpers/date/showDate'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import CoverImage from 'js/components/CoverImage'
import Flexer from 'js/components/Flexer/Flexer'
import { Col, Container, Row } from 'js/components/Grid/Grid'
import { H1, H4 } from 'js/components/Heading'
import HideVisually from 'js/components/HideVisually/HideVisually'
import ToggleTranslation from 'js/components/Item/components/ToggleTranslation'
import ItemActions from 'js/components/Item/ItemActions/ItemActions'
import ItemStatusTag from 'js/components/Item/ItemStatusTag'
import ItemSuggestedItems from 'js/components/Item/ItemSuggestedItems'
import ItemTags from 'js/components/ItemTags'
import Modal from 'js/components/Modal/Modal'
import Section from 'js/components/Section/Section'
import SocialShare from 'js/components/SocialShare/SocialShare'
import TabMenu from 'js/components/TabMenu'
import TabPage from 'js/components/TabPage'
import Text from 'js/components/Text/Text'
import TiptapView from 'js/components/Tiptap/TiptapView'
import NotFound from 'js/core/NotFound'
import { eventViewFragment } from 'js/events/fragments/entity'
import { useSiteStore } from 'js/lib/stores/SiteStore'
import ThemeProvider from 'js/theme/ThemeProvider'

import CameraIcon from 'icons/camera.svg'

import CommentList from '../components/CommentList'
import Document from '../components/Document'

import Activity from './components/Activity'
import ActivityActions from './components/ActivityActions'
import AttendeesActionsCard from './components/AttendeesActionsCard'
import EventDate from './components/EventDate'
import EventLink from './components/EventLink'
import EventLocation from './components/EventLocation'
import SendMessage from './components/SendMessage'
import SlotsForm from './components/SlotsForm'
import AttendeesModal from './EventAttendees/components/AttendeesModal'
import getRefetchQueries from './helpers/getRefetchQueries'
import AddAttendee from './ItemActions/AddAttendee'
import InvitePeople from './ItemActions/InvitePeople/InvitePeople'

const Wrapper = styled(Section)`
    .EventCoverContainer {
        ${media.tabletDown`
            padding: 0;
        `}
    }
`

const Item = () => {
    const params = useParams()
    const { hash } = useLocation()
    const { guid, groupGuid } = params

    const { site } = useSiteStore()
    const {
        integratedVideocallEnabled,
        showCustomTagsInDetail,
        showTagsInDetail,
    } = site

    const { loading, data } = useQuery(GET_EVENT_ITEM, {
        variables: {
            guid,
            groupGuid,
        },
        fetchPolicy: 'cache-and-network',
    })

    const { t } = useTranslation()

    useEffect(() => {
        if (!loading && data?.entity) {
            setIsTranslated(!!data.entity.localTitle)
        }
    }, [loading, data])

    const [attendeesModal, setAttendeesModal] = useState(false)

    const [showInvitePeopleModal, setShowInvitePeopleModal] = useState(false)
    const toggleInvitePeopleModal = () =>
        setShowInvitePeopleModal(!showInvitePeopleModal)

    const [showAddAttendeeModal, setShowAddAttendeeModal] = useState(false)
    const toggleAddAttendeeModal = () =>
        setShowAddAttendeeModal(!showAddAttendeeModal)

    const [showSendMessageModal, setShowSendMessageModal] = useState(false)
    const toggleSendMessageModal = () =>
        setShowSendMessageModal(!showSendMessageModal)

    const onAfterDelete = () => `${getGroupUrl(params)}/events`

    const [showSlotsModal, setShowSlotsModal] = useState(false)
    const toggleSlotsModal = () => {
        setShowSlotsModal(!showSlotsModal)
    }

    const [isTranslated, setIsTranslated] = useState(null)

    if (loading) return null
    if (!loading && !data?.entity) return <NotFound />

    const { entity, viewer } = data
    const {
        localTitle,
        title,
        url,
        featured,
        parent,
        group,
        location,
        locationAddress,
        locationLink,
        hasChildren,
        children,
        statusPublished,
        canAttendWithEmail,
        startDate,
        endDate,
        source,
        localRichDescription,
        richDescription,
        tags,
        tagCategories,
        commentCount,
        suggestedItems,
        isTranslationEnabled,
        canEdit,
        videoCallEnabled,
        videoCallUrl,
    } = entity

    const hasTranslations = !!localTitle && isTranslationEnabled

    const translatedTitle = isTranslated ? localTitle : title
    const translatedRichDescription = isTranslated
        ? localRichDescription
        : richDescription

    const onEdit = `${getGroupUrl(params)}/events/edit/${guid}`
    const refetchQueries = getRefetchQueries(true, !!group)
    const containerTitle = group?.name
    const showLocation = !!location || !!locationAddress || !!locationLink

    const activities = []
    let currentActivities
    if (children?.length > 0) {
        children.forEach((activity) => {
            const key = format(new Date(activity.startDate), 'yyyy-MM-dd')
            const dateIndex = activities.findIndex((el) => {
                return Object.keys(el)[0] === key
            })

            if (dateIndex === -1) {
                activities.push({ [key]: [activity] })
            } else {
                Object.values(activities[dateIndex])[0].push(activity)
            }
        })

        const activitiesByHash =
            hash &&
            activities.find((el) => {
                return Object.keys(el)[0] === hash.substring(1)
            })
        currentActivities = activitiesByHash
            ? Object.values(activitiesByHash)[0]
            : Object.values(activities[0])[0]
    }

    const isPublished = statusPublished === 'published'

    // Show tabs if activity date is unclear
    const showActivityTabmenu =
        activities.length > 1 ||
        (activities.length === 1 &&
            (!isSameDate(children[0].startDate, startDate) ||
                !isSameDate(children[0].startDate, endDate)))

    const hasSuggestedItems = suggestedItems?.length > 0

    const canEditItem = canEdit && statusPublished !== 'archived'

    const canAddSubEvent = !parent && !entity?.rangeSettings?.type

    return (
        <ThemeProvider bodyColor="#FFFFFF">
            <Document title={translatedTitle} containerTitle={containerTitle} />

            <Wrapper backgroundColor="white" style={{ paddingTop: 0 }}>
                <Container className="EventCoverContainer">
                    <CoverImage featured={featured} />
                </Container>
                <Container>
                    <Row $spacing={20} style={{ marginTop: '32px' }}>
                        <Col mobileLandscape={1 / 2} tabletUp={2 / 3}>
                            <div
                                style={{
                                    display: 'flex',
                                    alignItems: 'flex-start',
                                    justifyContent: 'space-between',
                                }}
                            >
                                <div>
                                    <H1>
                                        {translatedTitle}
                                        <ItemStatusTag
                                            entity={entity}
                                            style={{
                                                marginTop: '8px',
                                            }}
                                        />
                                    </H1>

                                    {parent && (
                                        <Text size="small">
                                            <HideVisually>
                                                {t('entity-event.sub-event-of')}{' '}
                                            </HideVisually>
                                            <Link to={parent.url}>
                                                {parent.title}
                                            </Link>
                                        </Text>
                                    )}
                                </div>

                                <ItemActions
                                    showEditButton
                                    entity={entity}
                                    onEdit={onEdit}
                                    onAfterDelete={onAfterDelete}
                                    canDuplicate
                                    refetchQueries={refetchQueries}
                                    customOptions={[
                                        [
                                            ...(canEdit
                                                ? [
                                                      {
                                                          name: t(
                                                              'entity-event.attendees',
                                                          ),
                                                          onClick: () =>
                                                              setAttendeesModal(
                                                                  'going',
                                                              ),
                                                      },
                                                  ]
                                                : []),
                                            ...(canEdit &&
                                            window.__SETTINGS__.eventExport
                                                ? [
                                                      {
                                                          name: t(
                                                              'entity-event.export-attendees',
                                                          ),
                                                          href: `/exporting/event/${guid}`,
                                                      },
                                                  ]
                                                : []),
                                            ...(canEdit
                                                ? [
                                                      {
                                                          name: t(
                                                              'entity-event.guest-list',
                                                          ),
                                                          href: `${url}/guest-list`,
                                                          target: '_blank',
                                                      },
                                                  ]
                                                : []),
                                        ],
                                        [
                                            ...(canEdit
                                                ? [
                                                      {
                                                          name: t(
                                                              'global.send-message',
                                                          ),
                                                          onClick:
                                                              toggleSendMessageModal,
                                                      },
                                                  ]
                                                : []),
                                            ...(canEdit
                                                ? [
                                                      {
                                                          name: t(
                                                              'global.invite-people',
                                                          ),
                                                          onClick:
                                                              toggleInvitePeopleModal,
                                                      },
                                                  ]
                                                : []),
                                            ...(canEdit && canAttendWithEmail
                                                ? [
                                                      {
                                                          name: t(
                                                              'entity-event.add-attendee',
                                                          ),
                                                          onClick:
                                                              toggleAddAttendeeModal,
                                                      },
                                                  ]
                                                : []),
                                        ],
                                        [
                                            ...(canEditItem && hasChildren
                                                ? [
                                                      {
                                                          name: t(
                                                              'entity-event.activity-slots',
                                                          ),
                                                          onClick:
                                                              toggleSlotsModal,
                                                      },
                                                  ]
                                                : []),
                                            ...(canEditItem && canAddSubEvent
                                                ? [
                                                      {
                                                          name: t(
                                                              'entity-event.create-activity',
                                                          ),
                                                          to: `${getGroupUrl(
                                                              params,
                                                          )}/events/add/${guid}`,
                                                          state: {
                                                              prevPathname:
                                                                  location.pathname,
                                                          },
                                                      },
                                                  ]
                                                : []),
                                        ],
                                    ]}
                                    style={{ marginLeft: 'auto' }}
                                />
                            </div>
                            <Row $growContent>
                                <Col tabletUp={showLocation ? 1 / 2 : 1}>
                                    <EventDate
                                        startDate={startDate}
                                        endDate={endDate}
                                        size="large"
                                        style={{ marginTop: '20px' }}
                                    />
                                </Col>
                                {showLocation && (
                                    <Col
                                        tabletUp={1 / 2}
                                        style={{ marginTop: '20px' }}
                                    >
                                        <EventLocation
                                            entity={entity}
                                            size="normal"
                                        />
                                    </Col>
                                )}
                            </Row>

                            {source && <EventLink entity={entity} />}

                            {videoCallEnabled &&
                                videoCallUrl &&
                                integratedVideocallEnabled && (
                                    <Flexer
                                        justifyContent="flex-start"
                                        style={{ marginTop: 20 }}
                                    >
                                        <Button
                                            size="large"
                                            variant="secondary"
                                            as="a"
                                            href={videoCallUrl}
                                            target="_blank"
                                            rel="noopener noreferrer"
                                        >
                                            <CameraIcon
                                                style={{
                                                    margin: '0 8px 0 -2px',
                                                }}
                                            />
                                            {t('global.video-call')}
                                            <HideVisually>
                                                {t(
                                                    'global.opens-in-new-window',
                                                )}
                                            </HideVisually>
                                        </Button>
                                    </Flexer>
                                )}

                            <TiptapView
                                content={translatedRichDescription}
                                style={{ marginTop: '24px' }}
                            />

                            {hasTranslations && (
                                <ToggleTranslation
                                    isTranslated={isTranslated}
                                    setIsTranslated={setIsTranslated}
                                    style={{ marginTop: '12px' }}
                                />
                            )}

                            <ItemTags
                                showCustomTags={showCustomTagsInDetail}
                                showTags={showTagsInDetail}
                                customTags={tags}
                                tagCategories={tagCategories}
                                style={{ marginTop: '24px' }}
                            />

                            {activities?.length > 0 && (
                                <div style={{ marginTop: '24px' }}>
                                    {showActivityTabmenu && (
                                        <TabMenu
                                            label={t('entity-event.activities')}
                                            showBorder
                                            options={activities.map(
                                                (activity, index) => {
                                                    const key =
                                                        Object.keys(activity)[0]
                                                    const hashIsKey =
                                                        hash === `#${key}`
                                                    const { guid } =
                                                        Object.values(
                                                            activity,
                                                        )[0][0]

                                                    return {
                                                        id: `tab-${guid}`,
                                                        label: showDate(
                                                            formatISO(key),
                                                            false,
                                                            true,
                                                        ),
                                                        link: `#${key}`,
                                                        isActive:
                                                            index === 0
                                                                ? !hash ||
                                                                  hashIsKey
                                                                : hashIsKey,
                                                        ariaControls: `tabpanel-${guid}`,
                                                    }
                                                },
                                            )}
                                            style={{ margin: '8px 0 16px' }}
                                        />
                                    )}

                                    <div>
                                        {currentActivities.map((activity) => {
                                            return (
                                                <TabPage
                                                    key={activity.guid}
                                                    id={`tabpanel-${activity.guid}`}
                                                    ariaLabelledby={`tab-${activity.guid}`}
                                                    visible
                                                >
                                                    <Activity
                                                        entity={activity}
                                                        showSlots
                                                        showSpots
                                                    >
                                                        <div
                                                            style={{
                                                                display: 'flex',
                                                                paddingRight:
                                                                    '4px',
                                                            }}
                                                        >
                                                            <ActivityActions
                                                                entity={
                                                                    activity
                                                                }
                                                                viewer={viewer}
                                                                refetchQueries={
                                                                    refetchQueries
                                                                }
                                                            />
                                                        </div>
                                                    </Activity>
                                                </TabPage>
                                            )
                                        })}
                                    </div>
                                </div>
                            )}

                            {isPublished &&
                                window.__SETTINGS__.enableSharing && (
                                    <SocialShare />
                                )}
                        </Col>
                        <Col mobileLandscape={1 / 2} tabletUp={1 / 3}>
                            <AttendeesActionsCard
                                entity={entity}
                                viewer={viewer}
                                site={site}
                            />
                        </Col>
                    </Row>
                </Container>

                <Container size="large">
                    <Row style={{ marginTop: '32px' }}>
                        <Col tabletUp={2 / 3}>
                            <H4 variant="grey">
                                {t('comments.count-comments', {
                                    count: commentCount,
                                })}
                            </H4>
                            <CommentList
                                viewer={viewer}
                                entity={entity}
                                refetchQueries={refetchQueries}
                            />
                        </Col>
                        {hasSuggestedItems && (
                            <Col tabletUp={1 / 3}>
                                <ItemSuggestedItems entity={entity} />
                            </Col>
                        )}
                    </Row>
                </Container>
            </Wrapper>

            <AttendeesModal
                tab={attendeesModal}
                setTab={setAttendeesModal}
                entity={entity}
            />

            <Modal
                size="normal"
                isVisible={showInvitePeopleModal}
                onClose={toggleInvitePeopleModal}
                title={t('global.invite-people')}
                showCloseButton
                containHeight
            >
                <InvitePeople guid={guid} onClose={toggleInvitePeopleModal} />
            </Modal>

            <Modal
                size="small"
                isVisible={showAddAttendeeModal}
                onClose={toggleAddAttendeeModal}
                title={t('entity-event.add-attendee')}
                showCloseButton
            >
                <AddAttendee guid={guid} onClose={toggleAddAttendeeModal} />
            </Modal>

            <Modal
                size="normal"
                title={t('global.send-message')}
                showCloseButton
                isVisible={showSendMessageModal}
                onClose={toggleSendMessageModal}
                containHeight
            >
                <SendMessage entity={entity} onClose={toggleSendMessageModal} />
            </Modal>

            {hasChildren && (
                <Modal
                    isVisible={showSlotsModal}
                    showCloseButton
                    onClose={toggleSlotsModal}
                    title={t('entity-event.activity-slots')}
                    size="normal"
                >
                    <SlotsForm entity={entity} onClose={toggleSlotsModal} />
                </Modal>
            )}
        </ThemeProvider>
    )
}

const GET_EVENT_ITEM = gql`
    query EventItem($guid: String!, $groupGuid: String) {
        viewer {
            guid
            loggedIn
            user {
                guid
                name
                icon
                email
            }
            requiresCommentModeration(subtype: "event", groupGuid: $groupGuid)
        }
        entity(guid: $guid) {
            guid
            ${eventViewFragment}
        }
    }
`

export default Item
