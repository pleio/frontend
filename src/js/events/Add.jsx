import React from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import EntityAddEditForm from 'js/components/EntityActions/EntityAddEditForm'

import getRefetchQueries from './helpers/getRefetchQueries'

const Add = () => {
    const { t } = useTranslation()
    const { groupGuid, containerGuid } = useParams()

    const { data, loading } = useQuery(Query, {
        variables: {
            guid: containerGuid,
        },
        skip: !containerGuid,
    })

    if (loading && !data) return null

    const refetchQueries = getRefetchQueries(false, !!groupGuid)

    const addTitle = containerGuid
        ? 'entity-event.create-activity'
        : 'entity-event.create'

    return (
        <EntityAddEditForm
            title={t(addTitle)}
            subtype="event"
            isSubEvent={!!containerGuid}
            defaultStartDate={data?.entity?.startDate}
            refetchQueries={refetchQueries}
        />
    )
}

const Query = gql`
    query AddEvent($guid: String!) {
        entity(guid: $guid) {
            guid
            ... on Event {
                startDate
            }
        }
    }
`

export default Add
