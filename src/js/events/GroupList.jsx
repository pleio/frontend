import React from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { useWindowScrollPosition } from 'helpers'

import Document from 'js/components/Document'
import { Container } from 'js/components/Grid/Grid'
import FeedView from 'js/page/Widget/components/Feed/FeedView'

// First load data, otherwise useWindowScrollPosition and FeedView won't work properly
const ListWrapper = () => {
    const { groupGuid } = useParams()
    const { data, loading } = useQuery(GET_LIST_SETTINGS, {
        variables: {
            guid: groupGuid,
        },
    })

    if (loading) return null
    return <GroupList data={data} />
}

const GroupList = ({ data }) => {
    const { t } = useTranslation()
    const { groupGuid } = useParams()

    const guid = `${groupGuid}-events`
    useWindowScrollPosition(guid)

    const { entity, site } = data

    const settings = {
        showTagFilter: site?.pageTagFilters.showTagFilter,
        showTagCategories: site?.pageTagFilters.showTagCategories,
        typeFilter: ['event'],
        showEventFilter: true,
        itemView: window.__SETTINGS__.eventTiles ? 'tile' : 'list',
        itemCount: 10,
    }

    return (
        <>
            <Document
                title={t('entity-event.title-list')}
                containerTitle={entity?.name}
            />
            <Container size={window.__SETTINGS__.eventTiles ? 'large' : 'tiny'}>
                <FeedView
                    guid={guid}
                    containerGuid={groupGuid}
                    settings={settings}
                />
            </Container>
        </>
    )
}

const GET_LIST_SETTINGS = gql`
    query GroupItem($guid: String!) {
        entity(guid: $guid) {
            guid
            ... on Group {
                name
            }
        }
        site {
            guid
            pageTagFilters(contentType: "event") {
                showTagFilter
                showTagCategories
            }
        }
    }
`

export default ListWrapper
