import {
    entityEditFragment,
    entityViewFragment,
} from 'js/components/EntityActions/fragments/entity'

const eventFragment = `
    startDate
    endDate
    maxAttendees
    location
    locationAddress
    locationLink
    rsvp
    qrAccess
    ticketLink
    enableMaybeAttendEvent
    attendEventOnline
    attendEventWithoutAccount
    parent {
        guid
        title
        url
        statusPublished
    }
`

export const eventEditFragment = `
... on Event {
    ${entityEditFragment}
    ${eventFragment}
    videoCallEnabled
    videoCallModerators {
        guid
        name
        url
        email
        icon
        roles
    }
    attendeeWelcomeMailSubject
    attendeeWelcomeMailContent
    rangeSettings {
        repeatUntil
        instanceLimit
        type
        interval
        isIgnored
    }
    isFormEnabled
    form {
        title
        description
        fields {
            guid
            title
            description
            fieldType
            isMandatory
            fieldOptions
            __typename @include(if: false)
        }
        __typename @include(if: false)
    }
}
`

export const eventViewFragment = `
... on Event {
    ${entityViewFragment}
    ${eventFragment}
    hasChildren
    canAttendWithEmail
    isAttendingSubEvents
    alreadySignedUpInSlot
    isAttending
    publishRequest {
        status
    }
    videoCallEnabled
    videoCallUrl
    rangeSettings {
        type
    }
    isFormEnabled
    form {
        title
        description
        fields {
            guid
            title
            description
            fieldType
            isMandatory
            fieldOptions
        }
    }
    slotsAvailable {
        name
        subEventGuids
    }
    attendees(
        limit: 3
        state: "accept"
        orderBy: timeUpdated
        orderDirection: desc
    ) {
        total
        totalAccept
        totalMaybe
        totalOnline
        totalReject
        totalWaitinglist
        totalUnconfirmed
        edges {
            guid
            name
            state
            url
            icon
        }
    }
    children {
        guid
        canEdit
        owner {
            guid
        }
        title
        url
        startDate
        endDate
        rsvp
        attendEventWithoutAccount
        isAttending
        maxAttendees
        attendees(limit: 0) {
            totalAccept
        }
        group {
            guid
            ... on Group {
                url
            }
        }
        slots
        alreadySignedUpInSlot
    }
}
`
