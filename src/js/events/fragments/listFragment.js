import { featuredViewFragment } from 'js/lib/fragments/featured'
import { ownerFragment } from 'js/lib/fragments/owner'

const eventListFragment = `
    fragment EventListFragment on Event {
        guid
        localTitle
        localExcerpt
        title
        excerpt
        canEdit
        canArchiveAndDelete
        subtype
        url
        votes
        hasVoted
        isBookmarked
        isPinned
        inGroup
        canBookmark
        isTranslationEnabled
        tagCategories {
            name
            values
        }
        tags
        rsvp
        isFeatured
        ${featuredViewFragment}
        startDate
        endDate
        location
        locationLink
        timePublished
        statusPublished
        commentCount
        canComment
        ${ownerFragment}
        isAttending
        attendees(
            limit: 3
            state: "accept"
            orderBy: timeUpdated
            orderDirection: desc
        ) {
            totalAccept
            edges {
                guid
                icon
                url
                name
                state
            }
        }
        group {
            guid
            ... on Group {
                isClosed
                name
                url
                membership
                isMembershipOnRequest
                isJoinButtonVisible
            }
        }
        publishRequest {
            status
        }
    }
`
export default eventListFragment
