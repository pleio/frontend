import React from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { getGroupUrl } from 'helpers'

import EntityAddEditForm from 'js/components/EntityActions/EntityAddEditForm'
import NotFound from 'js/core/NotFound'
import { eventEditFragment } from 'js/events/fragments/entity'

import getRefetchQueries from './helpers/getRefetchQueries'

const Edit = () => {
    const { t } = useTranslation()
    const navigate = useNavigate()
    const params = useParams()
    const { guid } = params

    const { loading, data } = useQuery(Query, {
        variables: {
            guid,
        },
    })

    if (loading) return null
    if (!data || !data.entity) return <NotFound />

    const { entity } = data

    const editTitle = entity.parent
        ? 'entity-event.edit-activity'
        : 'entity-event.edit'

    const deleteTitle = entity.parent
        ? 'entity-event.delete-activity'
        : 'entity-event.delete'

    const afterDelete = () => {
        navigate(`${getGroupUrl(params)}/events`, {
            replace: true,
        })
    }

    const refetchQueries = getRefetchQueries(true, !!entity.group)

    return (
        <EntityAddEditForm
            title={t(editTitle)}
            deleteTitle={t(deleteTitle)}
            subtype="event"
            entity={entity}
            isSubEvent={!!entity.parent}
            afterDelete={afterDelete}
            refetchQueries={refetchQueries}
        />
    )
}

const Query = gql`
    query EditEvent($guid: String!) {
        entity(guid: $guid) {
            guid
            ${eventEditFragment}
        }
    }
`

export default Edit
