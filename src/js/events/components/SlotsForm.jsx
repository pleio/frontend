import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { format, formatISO } from 'date-fns'
import showDate from 'helpers/date/showDate'
import { produce } from 'immer'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import IconButton from 'js/components/IconButton/IconButton'
import TabMenu from 'js/components/TabMenu'
import TabPage from 'js/components/TabPage'
import Text from 'js/components/Text/Text'

import AddIcon from 'icons/add.svg'
import RemoveIcon from 'icons/cross.svg'

import Activity from './Activity'
import TabElement from './TabElement'

const SlotsForm = ({ mutate, entity, onClose }) => {
    const { t } = useTranslation()

    const [slotsAvailable, setSlotsAvailable] = useState(entity.slotsAvailable)

    const [activeSlotIndex, setActiveSlotIndex] = useState(0)

    const [activeSlotActivities, setActiveSlotActivities] = useState([])

    useEffect(() => {
        const activities = []
        entity.children.forEach((activity) => {
            const guids = slotsAvailable[activeSlotIndex]?.subEventGuids
            const name = slotsAvailable[activeSlotIndex]?.name

            if (guids && guids.indexOf(activity.guid) !== -1) {
                activities.push({ ...activity, name })
            }
        })
        setActiveSlotActivities(activities)
    }, [activeSlotIndex, slotsAvailable, entity.children])

    const addSlot = () => {
        const index = slotsAvailable.length
        setSlotsAvailable(
            produce((newState) => {
                newState.push({
                    name: `Slot ${index + 1}`,
                    subEventGuids: [],
                })
            }),
        )
        setActiveSlotIndex(index)
    }

    const submit = async () => {
        await mutate({
            variables: {
                input: {
                    guid: entity.guid,
                    slotsAvailable: slotsAvailable.map((el) => {
                        return {
                            name: el.name,
                            subEventGuids: el.subEventGuids,
                        }
                    }),
                },
            },
        })
            .then(() => {
                onClose()
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    const {
        handleSubmit,
        formState: { isSubmitting },
    } = useForm()

    const [activeDate, setActiveDate] = useState(null)

    const activities = []
    entity.children.forEach((activity) => {
        const key = format(new Date(activity.startDate), 'yyyy-MM-dd')
        const dateIndex = activities.findIndex((el) => {
            return Object.keys(el)[0] === key
        })

        if (dateIndex === -1) {
            activities.push({ [key]: [activity] })
        } else {
            Object.values(activities[dateIndex])[0].push(activity)
        }
    })

    const activitiesByDate =
        activeDate &&
        activities.find((el) => {
            return Object.keys(el)[0] === activeDate
        })
    const currentActivities = activitiesByDate
        ? Object.values(activitiesByDate)[0]
        : Object.values(activities[0])[0]

    return (
        <>
            <Text as="p" size="small">
                {t('entity-event.activity-slots-helper')}
            </Text>

            <div style={{ marginTop: '16px' }}>
                {activities.length > 1 && (
                    <TabMenu
                        label={t('entity-event.activities')}
                        showBorder
                        options={activities.map((activity, i) => {
                            const key = Object.keys(activity)[0]
                            const { guid } = activity[key]

                            return {
                                id: `tab-${guid}`,
                                label: showDate(formatISO(key), false, true),
                                onClick: () => setActiveDate(key),
                                isActive: activeDate
                                    ? activeDate === key
                                    : i === 0,
                                ariaControls: `tabpanel-${guid}`,
                            }
                        })}
                        style={{ margin: '8px 0 16px' }}
                    />
                )}

                <div>
                    {currentActivities.map((activity) => {
                        const { guid, title, rsvp, attendEventWithoutAccount } =
                            activity

                        const addActivity = () => {
                            if (slotsAvailable.length > 0) {
                                setSlotsAvailable(
                                    produce((newState) => {
                                        newState[
                                            activeSlotIndex
                                        ].subEventGuids.splice(
                                            slotsAvailable[activeSlotIndex]
                                                .subEventGuids.length,
                                            0,
                                            activity.guid,
                                        )
                                    }),
                                )
                            } else {
                                setSlotsAvailable([
                                    {
                                        name: 'Slot 1',
                                        subEventGuids: [activity.guid],
                                    },
                                ])
                            }
                        }

                        return (
                            <TabPage
                                key={guid}
                                id={`tabpanel-${guid}`}
                                ariaLabelledby={`tab-${guid}`}
                                visible
                            >
                                <Activity entity={activity}>
                                    {activeSlotActivities.findIndex(
                                        (el) => el.guid === guid,
                                    ) === -1 &&
                                        (rsvp || attendEventWithoutAccount) && (
                                            <IconButton
                                                size="large"
                                                hoverSize="normal"
                                                variant="secondary"
                                                onClick={addActivity}
                                                tooltip={
                                                    slotsAvailable.length > 0
                                                        ? t(
                                                              'entity-event.add-to-slot',
                                                          )
                                                        : t(
                                                              'entity-event.add-to-new-slot',
                                                          )
                                                }
                                                placement="right"
                                                aria-label={
                                                    slotsAvailable.length > 0
                                                        ? t(
                                                              'entity-event.add-to-slot-label',
                                                              {
                                                                  title,
                                                                  name: slotsAvailable[
                                                                      activeSlotIndex
                                                                  ].name,
                                                              },
                                                          )
                                                        : t(
                                                              'entity-event.add-to-new-slot-label',
                                                              { title },
                                                          )
                                                }
                                            >
                                                <AddIcon />
                                            </IconButton>
                                        )}
                                </Activity>
                            </TabPage>
                        )
                    })}
                </div>
            </div>

            {slotsAvailable.length > 0 ? (
                <TabMenu
                    label={t('entity-event.activity-slots')}
                    edgePadding
                    edgeMargin
                    showBorder
                    options={slotsAvailable.map(({ name }, i) => {
                        return {
                            name,
                            onClick: () => setActiveSlotIndex(i),
                            isActive: activeSlotIndex === i,
                            slotIndex: i,
                            id: `tab-${name}`,
                            'aria-controls': `tabpanel-${name}`,
                        }
                    })}
                    TabElement={(props) => {
                        return (
                            <TabElement
                                {...props}
                                setSlotsAvailable={setSlotsAvailable}
                                setActiveSlotIndex={setActiveSlotIndex}
                            />
                        )
                    }}
                    style={{ marginTop: '16px' }}
                >
                    <Flexer divider="normal" gutter="none">
                        <div
                            style={{
                                alignSelf: 'stretch',
                            }}
                        />
                        <IconButton
                            size="normal"
                            variant="secondary"
                            onClick={addSlot}
                            aria-label={t('entity-event.add-slot')}
                        >
                            <AddIcon />
                        </IconButton>
                    </Flexer>
                </TabMenu>
            ) : (
                <Text textAlign="center" style={{ padding: '20px 20px 0' }}>
                    {t('entity-event.no-slots')}{' '}
                    <Button
                        size="small"
                        variant="tertiary"
                        onClick={() => {
                            setSlotsAvailable([
                                {
                                    name: 'Slot 1',
                                    subEventGuids: [],
                                },
                            ])
                        }}
                        style={{ display: 'inline-flex' }}
                    >
                        {t('entity-event.add-slot')}
                    </Button>
                </Text>
            )}

            {activeSlotActivities?.length > 0 && (
                <TabPage
                    id={`tabpanel-${activeSlotActivities[0]?.name}`}
                    ariaLabelledby={`tab-${activeSlotActivities[0]?.name}`}
                    visible
                >
                    <div style={{ marginTop: '16px' }}>
                        {activeSlotActivities.map((activity) => {
                            const { guid, title } = activity
                            const removeActivity = () => {
                                setSlotsAvailable(
                                    produce((newState) => {
                                        newState[
                                            activeSlotIndex
                                        ].subEventGuids.splice(
                                            slotsAvailable[
                                                activeSlotIndex
                                            ].subEventGuids.indexOf(
                                                activity.guid,
                                            ),
                                            1,
                                        )
                                    }),
                                )
                            }

                            return (
                                <Activity key={guid} entity={activity}>
                                    <IconButton
                                        size="large"
                                        hoverSize="normal"
                                        variant="secondary"
                                        onClick={removeActivity}
                                        tooltip={t(
                                            'entity-event.remove-from-slot',
                                        )}
                                        placement="right"
                                        aria-label={t(
                                            'entity-event.remove-from-slot-label',
                                            { title },
                                        )}
                                    >
                                        <RemoveIcon />
                                    </IconButton>
                                </Activity>
                            )
                        })}
                    </div>
                </TabPage>
            )}

            <Flexer mt as="form" onSubmit={handleSubmit(submit)}>
                <Button
                    size="normal"
                    variant="tertiary"
                    type="button"
                    onClick={onClose}
                >
                    {t('action.cancel')}
                </Button>
                <Button
                    type="submit"
                    size="normal"
                    variant="primary"
                    loading={isSubmitting}
                >
                    {t('action.save')}
                </Button>
            </Flexer>
        </>
    )
}

const Mutation = gql`
    mutation editEntity($input: editEntityInput!) {
        editEntity(input: $input) {
            entity {
                guid
                ... on Event {
                    children {
                        guid
                        slots
                        alreadySignedUpInSlot
                    }
                    slotsAvailable {
                        name
                        subEventGuids
                    }
                }
            }
        }
    }
`

export default graphql(Mutation)(SlotsForm)
