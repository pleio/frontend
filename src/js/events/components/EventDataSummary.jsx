import React from 'react'
import { useTranslation } from 'react-i18next'
import {
    isSameDate,
    isSameTime,
    showTime,
    showWeekDay,
} from 'helpers/date/showDate'
import filterViewerFromAttendees from 'helpers/filterViewerFromAttendees'
import styled from 'styled-components'

import People from 'js/components/People'

import DateCalendar from './DateCalendar'
import HiddenEventDates from './HiddenEventDates'

const Wrapper = styled.div`
    display: flex;
    align-items: flex-start;

    .EventDataSummaryTime {
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
    }
`

const EventDataSummary = ({ viewer, entity, ...rest }) => {
    const { t } = useTranslation()

    if (!entity) return null

    const { startDate, endDate, attendees, isAttending } = entity
    const sameDate = isSameDate(startDate, endDate)
    const sameTime = isSameTime(startDate, endDate)

    return (
        <>
            <HiddenEventDates startDate={startDate} endDate={endDate} />
            <Wrapper aria-hidden {...rest}>
                <DateCalendar
                    startDate={startDate}
                    size="normal"
                    style={{ marginRight: '12px' }}
                />

                <div style={{ padding: '4px 0' }}>
                    <div className="EventDataSummaryTime">
                        {showWeekDay(startDate)} {showTime(startDate)}
                        {!!endDate && !sameTime ? (
                            <span>
                                {' '}
                                – {sameDate ? '' : `${showWeekDay(endDate)} `}
                                {showTime(endDate)}
                            </span>
                        ) : null}
                    </div>

                    <People
                        size="small"
                        showCheck={isAttending === 'accept'}
                        checkLabel={t('entity-event.you-are-going')}
                        users={
                            isAttending === 'accept'
                                ? filterViewerFromAttendees(
                                      attendees.edges,
                                      viewer.user.guid,
                                  )
                                : attendees.edges
                        }
                        total={attendees.totalAccept}
                        style={{
                            marginTop: '4px',
                            justifyContent: 'flex-start',
                        }}
                    />
                </div>
            </Wrapper>
        </>
    )
}

export default EventDataSummary
