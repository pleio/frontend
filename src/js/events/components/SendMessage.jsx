import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import Button from 'js/components/Button/Button'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import { Col, Row } from 'js/components/Grid/Grid'
import Spacer from 'js/components/Spacer/Spacer'
import Text from 'js/components/Text/Text'

const SendMessage = ({ mutate, entity, onClose }) => {
    const { t } = useTranslation()

    const [errorList, setErrorList] = useState()
    const [completed, setCompleted] = useState(false)
    const toggleCompleted = () => setCompleted(!completed)

    const {
        guid,
        enableMaybeAttendEvent,
        attendEventOnline,
        maxAttendees,
        attendees: {
            totalAccept,
            totalMaybe,
            totalOnline,
            totalReject,
            totalWaitinglist,
            totalUnconfirmed,
        },
    } = entity

    const submit = async ({
        subject,
        message,
        sendToStatus,
        isTest,
        sendCopyToSender,
    }) => {
        setErrorList(null)

        await mutate({
            variables: {
                input: {
                    guid,
                    message,
                    sendToStatus,
                    ...(isTest
                        ? {
                              isTest: true,
                              subject: `Test: ${subject}`,
                          }
                        : {
                              subject,
                              sendCopyToSender,
                          }),
                },
            },
        })
            .then(() => {
                if (!isTest) {
                    reset()
                }
                toggleCompleted()
            })
            .catch((errors) => {
                console.error(errors)
                setErrorList(errors)
            })
    }

    const defaultValues = {
        subject: '',
        message: '',
        sendToStatus: 'accept',
        sendCopyToSender: true,
        isTest: false,
    }
    const {
        control,
        handleSubmit,
        reset,
        watch,
        formState: { errors, isValid, isSubmitting },
    } = useForm({ mode: 'onChange', defaultValues })

    return (
        <>
            {completed ? (
                <>
                    <Text as="p">{t('global.message-sent')}</Text>
                    <Flexer mt>
                        <Button
                            size="normal"
                            variant="primary"
                            onClick={
                                watch('isTest') ? toggleCompleted : onClose
                            }
                        >
                            {t('action.ok')}
                        </Button>
                    </Flexer>
                </>
            ) : (
                <Spacer
                    as="form"
                    onSubmit={handleSubmit(submit)}
                    style={{
                        display: 'flex',
                        flexDirection: 'column',
                        overflow: 'hidden',
                    }}
                >
                    <Text as="p">{t('entity-event.message-guests')}</Text>

                    <FormItem
                        control={control}
                        type="text"
                        name="subject"
                        label={t('global.subject')}
                        required
                        errors={errors}
                    />

                    <FormItem
                        control={control}
                        type="rich"
                        name="message"
                        placeholder={t('form.write-message')}
                        options={{
                            textStyle: true,
                            textLink: true,
                        }}
                        required
                        errors={errors}
                        contentType="html"
                        style={{ overflowY: 'auto' }}
                    />

                    <Row>
                        <Col mobileLandscapeUp={1 / 2}>
                            <FormItem
                                control={control}
                                type="select"
                                name="sendToStatus"
                                options={[
                                    {
                                        value: 'accept',
                                        label: `${t('entity-event.going')} (${totalAccept})`,
                                    },
                                    ...(maxAttendees || totalWaitinglist
                                        ? [
                                              {
                                                  value: 'waitinglist',
                                                  label: `${t('entity-event.waiting-list')} (${totalWaitinglist})`,
                                              },
                                          ]
                                        : []),
                                    ...(attendEventOnline || totalOnline
                                        ? [
                                              {
                                                  value: 'online',
                                                  label: `${t('entity-event.online')} (${totalOnline})`,
                                              },
                                          ]
                                        : []),
                                    ...(enableMaybeAttendEvent || totalMaybe
                                        ? [
                                              {
                                                  value: 'maybe',
                                                  label: `${t('entity-event.maybe')} (${totalMaybe})`,
                                              },
                                          ]
                                        : []),
                                    {
                                        value: 'reject',
                                        label: `${t('entity-event.not-going')} (${totalReject})`,
                                    },
                                    ...(totalUnconfirmed
                                        ? [
                                              {
                                                  value: 'unconfirmed',
                                                  label: `${t('entity-event.unconfirmed')} (${totalUnconfirmed})`,
                                              },
                                          ]
                                        : []),
                                ]}
                                label={t('entity-event.recipients')}
                                errors={errors}
                            />
                        </Col>
                    </Row>

                    <Errors errors={errorList} />

                    <div>
                        <FormItem
                            type="switch"
                            control={control}
                            name="isTest"
                            label={t('global.send-message-as-test')}
                            size="small"
                        />
                        <FormItem
                            type="switch"
                            control={control}
                            name="sendCopyToSender"
                            label={t('global.send-message-copy')}
                            size="small"
                            disabled={watch('isTest')}
                        />
                    </div>

                    <Flexer>
                        <Button
                            variant="primary"
                            size="normal"
                            type="submit"
                            disabled={!isValid}
                            loading={isSubmitting}
                        >
                            {t('action.send')}
                        </Button>
                    </Flexer>
                </Spacer>
            )}
        </>
    )
}

const Mutation = gql`
    mutation SendMessageToEvent($input: sendMessageToEventInput!) {
        sendMessageToEvent(input: $input) {
            success
        }
    }
`

export default graphql(Mutation)(SendMessage)
