import React from 'react'
import { useTranslation } from 'react-i18next'
import { isSameDate, isSameTime, showDateTime } from 'helpers/date/showDate'

import HideVisually from 'js/components/HideVisually/HideVisually'

// Show full start and end date for screenreaders

const HiddenEventDates = ({ startDate, endDate }) => {
    const sameDate = isSameDate(startDate, endDate)
    const sameTime = isSameTime(startDate, endDate)

    const { t } = useTranslation()

    return (
        <>
            <HideVisually>
                {t('entity-event.start-date')}{' '}
                <time dateTime={startDate}>{showDateTime(startDate)}</time>
            </HideVisually>
            {(!sameDate || (sameDate && !sameTime)) && (
                <HideVisually>
                    {t('entity-event.end-date')}{' '}
                    <time dateTime={endDate}>{showDateTime(endDate)}</time>
                </HideVisually>
            )}
        </>
    )
}
export default HiddenEventDates
