import React from 'react'
import { useTranslation } from 'react-i18next'
import styled, { css } from 'styled-components'

import HideVisually from 'js/components/HideVisually/HideVisually'
import Text from 'js/components/Text/Text'

import PinIcon from 'icons/pin.svg'

const Wrapper = styled.div`
    display: flex;
    align-items: center;
    border: 1px solid ${(p) => p.theme.color.grey[30]};
    border-radius: ${(p) => p.theme.radius.normal};
    max-width: 100%;
    white-space: nowrap;
    overflow: hidden;

    ${(p) =>
        p.size === 'small' &&
        css`
            padding: 0 6px;

            svg {
                width: 12px;
                margin-right: 6px;
            }
        `};

    ${(p) =>
        p.size === 'normal' &&
        css`
            padding: 0 16px;

            svg {
                margin-right: 12px;
            }
        `};

    ${(p) =>
        p.$hasLink &&
        css`
            &:hover .EventLocationTitle {
                text-decoration: underline;
            }
        `};

    svg {
        color: ${(p) => p.theme.color.primary.main};
    }

    .EventLocationText {
        overflow: hidden;

        ${(p) =>
            p.size === 'small' &&
            css`
                padding: 5px 0;
                font-size: ${(p) => p.theme.font.size.small};
                line-height: ${(p) => p.theme.font.lineHeight.small};
            `};

        ${(p) =>
            p.size === 'normal' &&
            css`
                padding: 8px 0;
                font-size: ${(p) => p.theme.font.size.normal};
                line-height: ${(p) => p.theme.font.lineHeight.normal};
            `};

        > * {
            overflow: hidden;
            text-overflow: ellipsis;
        }
    }

    .EventLocationTitle {
        color: ${(p) => p.theme.color.primary.main};
    }
`

const EventLocation = ({ entity, size, ...rest }) => {
    const { location, locationAddress, locationLink } = entity
    const { t } = useTranslation()

    const locationTitle = location || t('entity-event.location')

    return (
        <>
            {locationLink && (
                <HideVisually id={`${entity.guid}-location-description`}>
                    {`${t('entity-event.view-location')}${t(
                        'global.opens-in-new-window',
                    )}`}
                </HideVisually>
            )}
            <Wrapper
                size={size}
                $hasLink={locationLink}
                as={locationLink ? 'a' : null}
                href={locationLink || null}
                target={locationLink ? '_blank' : null}
                rel={locationLink ? 'noopener noreferrer' : null}
                aria-describedby={
                    locationLink ? `${entity.guid}-location-description` : null
                }
                {...rest}
            >
                <PinIcon />
                <div className="EventLocationText">
                    <div className="EventLocationTitle" title={locationTitle}>
                        {locationTitle}
                    </div>
                    {locationAddress && size !== 'small' && (
                        <Text
                            size="small"
                            variant="grey"
                            title={locationAddress}
                        >
                            {locationAddress}
                        </Text>
                    )}
                </div>
            </Wrapper>
        </>
    )
}

export default EventLocation
