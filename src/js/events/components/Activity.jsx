import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { media } from 'helpers'
import { isSameTime } from 'helpers/date/showDate'
import { TabletUp } from 'helpers/mediaWrapper'
import styled, { css } from 'styled-components'

import DisplayDate from 'js/components/DisplayDate'
import Flexer from 'js/components/Flexer/Flexer'
import Tag from 'js/components/Tag/Tag'
import Text from 'js/components/Text/Text'

const Wrapper = styled.div`
    display: flex;
    align-items: flex-start;
    border: 1px solid ${(p) => p.theme.color.grey[30]};

    ${(p) =>
        !p.$canAttend &&
        css`
            background-color: ${(p) => p.theme.color.grey[10]};
        `}

    &:not(:last-child) {
        border-bottom: none;
    }
    &:first-child {
        border-top-left-radius: ${(p) => p.theme.radius.normal};
        border-top-right-radius: ${(p) => p.theme.radius.normal};
    }
    &:last-child {
        border-bottom-left-radius: ${(p) => p.theme.radius.normal};
        border-bottom-right-radius: ${(p) => p.theme.radius.normal};
    }

    .ActivityTime {
        flex-shrink: 0;
        min-height: 40px;
        padding-top: 1px;
        display: flex;
        align-items: center;
        justify-content: center;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        text-align: center;

        ${media.mobileLandscapeDown`
            flex-direction: column;
            width: 56px;
        `}

        ${media.tabletUp`
            width: 108px;
        `}
    }

    .ActivityLink {
        border-left: 1px solid ${(p) => p.theme.color.grey[30]};
        flex-grow: 1;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: flex-start;
        padding: 10px 8px 10px 0;
        min-height: 40px;

        ${media.mobileLandscapeDown`
            padding-left: 8px;
        `}

        ${media.tabletUp`
            padding-left: 16px;
        `}

        &:hover .ActivityTitle {
            text-decoration: underline;
        }
    }

    .ActivityTitle {
        color: ${(p) => p.theme.color.primary.main};
        font-size: ${(p) => p.theme.font.size.normal};
        line-height: ${(p) => p.theme.font.lineHeight.small};
    }
`

const Activity = ({ entity, showSlots, showSpots, children }) => {
    const { t } = useTranslation()

    const {
        title,
        url,
        startDate,
        endDate,
        slots,
        maxAttendees,
        attendees: { totalAccept },
        rsvp,
        attendEventWithoutAccount,
    } = entity

    const sameTime = isSameTime(startDate, endDate)

    const hasSlots = showSlots && slots.length > 0
    const hasSpots = showSpots && maxAttendees

    const spotsLeft = maxAttendees - totalAccept

    return (
        <Wrapper $canAttend={rsvp || attendEventWithoutAccount}>
            <div className="ActivityTime" aria-hidden>
                <DisplayDate date={startDate} type="time" />
                {!!endDate && !sameTime ? (
                    <>
                        <TabletUp>&nbsp;–&nbsp;</TabletUp>
                        <DisplayDate
                            date={endDate}
                            type="time"
                            placement="right"
                        />
                    </>
                ) : null}
            </div>
            <Link
                to={url}
                state={{ prevPathname: location.pathname }}
                className="ActivityLink"
            >
                <div className="ActivityTitle">{title}</div>
                {(hasSlots || hasSpots) && (
                    <Flexer
                        justifyContent="flex-start"
                        gutter="tiny"
                        wrap
                        style={hasSlots ? { marginTop: '2px' } : {}}
                    >
                        {hasSlots && (
                            <Flexer
                                justifyContent="flex-start"
                                gutter="tiny"
                                wrap
                            >
                                {slots.map((slot, i) => (
                                    <Tag
                                        key={`${slot}-${i}`}
                                        style={{
                                            marginTop: '2px',
                                            marginBottom: '2px',
                                        }}
                                    >
                                        {slot}
                                    </Tag>
                                ))}
                            </Flexer>
                        )}
                        {hasSpots && (
                            <Text size="small" variant="grey">
                                {spotsLeft > 0
                                    ? t('entity-event.spots-available', {
                                          count: spotsLeft,
                                      })
                                    : t('entity-event.guest-list-full')}
                            </Text>
                        )}
                    </Flexer>
                )}
            </Link>
            {children}
        </Wrapper>
    )
}

export default Activity
