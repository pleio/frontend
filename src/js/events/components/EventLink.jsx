import React from 'react'
import { useTranslation } from 'react-i18next'

import Text from 'js/components/Text/Text'

import ExternalIcon from 'icons/new-window-small.svg'

const EventLink = ({ entity }) => {
    const { t } = useTranslation()

    return (
        <Text>
            <a href={entity.source} target="_blank" rel="noopener noreferrer">
                {t('entity-event.external-link')}
                <ExternalIcon
                    style={{ display: 'inline-block', marginLeft: '6px' }}
                />
            </a>
        </Text>
    )
}

export default EventLink
