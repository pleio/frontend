import React, { useEffect, useRef, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { produce } from 'immer'
import styled from 'styled-components'

import Flexer from 'js/components/Flexer/Flexer'
import IconButton from 'js/components/IconButton/IconButton'

import CheckIcon from 'icons/check.svg'
import DeleteIcon from 'icons/delete-small.svg'
import EditIcon from 'icons/edit-small.svg'

const Wrapper = styled.div`
    position: relative;

    input {
        position: absolute;
        background: white;
        top: 2px;
        bottom: 2px;
        left: 0;
        padding: 0 0 0 10px;
        width: 100%;
        border: none;
        font-size: ${(p) => p.theme.font.size.small};
    }
`

const TabElement = ({
    name,
    onClick,
    isActive,
    slotIndex,
    setSlotsAvailable,
    setActiveSlotIndex,
    ...rest
}) => {
    const editButton = useRef()

    const { t } = useTranslation()

    const [editing, setEditing] = useState()
    const toggleEdit = () => setEditing(!editing)
    useEffect(() => {
        if (!editing) {
            editButton?.current?.focus()
        }
    }, [editing])

    useEffect(() => {
        if (!isActive) {
            setValue(name)
        }
    }, [isActive])

    const [value, setValue] = useState(name)
    const changeValue = (evt) => setValue(evt.target.value)

    const submitName = (evt) => {
        evt.stopPropagation()
        const name = value.trim()
        if (name) {
            setSlotsAvailable(
                produce((newState) => {
                    newState[slotIndex].name = name
                }),
            )
        } else {
            setValue(name)
        }
        toggleEdit()
    }

    const deleteSlot = () => {
        setSlotsAvailable(
            produce((newState) => {
                newState.splice(slotIndex, 1)
            }),
        )
        if (slotIndex) {
            setActiveSlotIndex(slotIndex - 1)
        }
    }

    return (
        <>
            <Wrapper>
                <button
                    type="button"
                    onClick={onClick}
                    aria-current={isActive ? 'page' : false}
                    {...rest}
                >
                    {value.replace(/ /g, '\u00A0')}
                </button>
                {editing && (
                    <form onSubmit={submitName}>
                        <input
                            type="text"
                            size={1}
                            value={value}
                            onChange={changeValue}
                            autoFocus
                        />
                    </form>
                )}
            </Wrapper>
            {isActive && (
                <Flexer divider="normal" gutter="none">
                    {editing ? (
                        <IconButton
                            size="normal"
                            variant="primary"
                            onClick={submitName}
                            aria-label={t('entity-event.save-slot-name')}
                            style={{
                                marginLeft: '-6px',
                            }}
                        >
                            <CheckIcon />
                        </IconButton>
                    ) : (
                        <IconButton
                            ref={editButton}
                            size="normal"
                            variant="secondary"
                            onClick={toggleEdit}
                            aria-label={t('entity-event.edit-slot-name', {
                                name,
                            })}
                            style={{
                                marginLeft: '-6px',
                            }}
                        >
                            <EditIcon />
                        </IconButton>
                    )}
                    <IconButton
                        size="normal"
                        variant="secondary"
                        onClick={deleteSlot}
                        aria-label={t('entity-event.delete-slot', {
                            name,
                        })}
                    >
                        <DeleteIcon />
                    </IconButton>
                </Flexer>
            )}
        </>
    )
}

export default TabElement
