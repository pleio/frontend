import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'

import DropdownButton from 'js/components/DropdownButton/DropdownButton'
import IconButton from 'js/components/IconButton/IconButton'
import CopyItem from 'js/components/Item/ItemActions/components/CopyItem'
import DeleteForm from 'js/components/Item/ItemActions/components/DeleteForm'
import Modal from 'js/components/Modal/Modal'
import Tooltip from 'js/components/Tooltip/Tooltip'
import JoinQueueModal from 'js/events/EventAttend/components/JoinQueueModal'
import LeaveGuestListModal from 'js/events/EventAttend/components/LeaveGuestListModal'
import LeaveQueueModal from 'js/events/EventAttend/components/LeaveQueueModal'
import useEventActions from 'js/events/hooks/useEventActions'

import CalendarCheckIcon from 'icons/calendar-check.svg'
import CalendarPlusIcon from 'icons/calendar-plus.svg'
import CalendarWaitingIcon from 'icons/calendar-waiting.svg'
import OptionsIcon from 'icons/options.svg'

const ActivityActions = ({ entity, viewer, refetchQueries }) => {
    const { t } = useTranslation()

    const { loading, handleSubmit } = useEventActions(entity.guid)

    const [showCopyEventModal, setShowCopyEventModal] = useState(false)

    const [showDeleteModal, setShowDeleteModal] = useState(false)
    const toggleDeleteModal = () => {
        setShowDeleteModal(!showDeleteModal)
    }

    const [showJoinQueueModal, setShowJoinQueueModal] = useState(false)
    const toggleJoinQueueModal = () => {
        setShowJoinQueueModal(!showJoinQueueModal)
    }

    const [showLeaveQueueModal, setShowLeaveQueueModal] = useState(false)
    const toggleLeaveQueueModal = () =>
        setShowLeaveQueueModal(!showLeaveQueueModal)

    const [showLeaveGuestListModal, setShowLeaveGuestListModal] =
        useState(false)
    const toggleShowLeaveGuestListModal = () =>
        setShowLeaveGuestListModal(!showLeaveGuestListModal)

    if (!viewer.loggedIn) return null

    const {
        guid,
        title,
        group,
        canEdit,
        isAttending,
        maxAttendees,
        attendees: { totalAccept },
        alreadySignedUpInSlot,
        rsvp,
    } = entity

    const options = canEdit
        ? [
              {
                  name: t('action.edit-item'),
                  label: t('action.edit-item-label', {
                      title,
                  }),
                  to: `${group?.url || ''}/events/edit/${guid}`,
                  state: { prevPathname: location.pathname },
              },
              {
                  name: t('action.duplicate-item'),
                  label: t('action.duplicate-item-label', {
                      title: entity.title,
                  }),
                  onClick: () => setShowCopyEventModal(true),
              },
              {
                  name: t('action.delete-item'),
                  label: t('action.delete-item-label', {
                      title,
                  }),
                  onClick: toggleDeleteModal,
              },
          ]
        : []

    const spotsLeft = maxAttendees - totalAccept
    const isFull = maxAttendees && spotsLeft < 1

    const signUp = () => {
        handleSubmit({ state: 'accept' })
    }
    const signOut = () => {
        toggleShowLeaveGuestListModal()
    }

    return (
        <>
            {isAttending === 'accept' && (
                <IconButton
                    variant="primary"
                    size="large"
                    tooltip={t('entity-event.you-are-going')}
                    placement="left"
                    aria-label={t('entity-event.exit-guest-list')}
                    onClick={signOut}
                    loading={loading}
                >
                    <CalendarCheckIcon />
                </IconButton>
            )}

            {isAttending === 'waitinglist' && (
                <IconButton
                    variant="primary"
                    size="large"
                    tooltip={t('entity-event.in-the-queue')}
                    placement="left"
                    aria-label={t('entity-event.exit-queue')}
                    onClick={toggleLeaveQueueModal}
                >
                    <CalendarWaitingIcon />
                </IconButton>
            )}

            {rsvp &&
                isAttending !== 'accept' &&
                isAttending !== 'waitinglist' && (
                    <>
                        {alreadySignedUpInSlot ? (
                            <Tooltip
                                content={t(
                                    'entity-event.already-signed-up-in-slot',
                                )}
                                placement="left"
                            >
                                <div>
                                    <IconButton
                                        variant="tertiary"
                                        size="large"
                                        disabled
                                    >
                                        <CalendarPlusIcon />
                                    </IconButton>
                                </div>
                            </Tooltip>
                        ) : isFull ? (
                            <IconButton
                                variant="secondary"
                                size="large"
                                tooltip={t('entity-event.join-queue')}
                                placement="left"
                                onClick={toggleJoinQueueModal}
                            >
                                <CalendarWaitingIcon />
                            </IconButton>
                        ) : (
                            <IconButton
                                variant="secondary"
                                size="large"
                                tooltip={t('entity-event.sign-up')}
                                placement="left"
                                onClick={signUp}
                                loading={loading}
                            >
                                <CalendarPlusIcon />
                            </IconButton>
                        )}
                    </>
                )}
            <DropdownButton options={options}>
                <IconButton
                    variant="secondary"
                    size="large"
                    tooltip={t('global.options')}
                    aria-label={t('action.show-options')}
                    style={{ marginLeft: '-4px' }}
                >
                    <OptionsIcon />
                </IconButton>
            </DropdownButton>

            <Modal
                isVisible={showDeleteModal}
                onClose={toggleDeleteModal}
                title={t('action.delete-item')}
                size="small"
            >
                <DeleteForm
                    entity={entity}
                    refetchQueries={refetchQueries}
                    onClose={toggleDeleteModal}
                />
            </Modal>

            <JoinQueueModal
                entity={entity}
                isVisible={showJoinQueueModal}
                handleSubmit={handleSubmit}
                onClose={toggleJoinQueueModal}
                loading={loading}
            />

            <LeaveQueueModal
                entity={entity}
                isVisible={showLeaveQueueModal}
                handleSubmit={handleSubmit}
                onClose={toggleLeaveQueueModal}
                loading={loading}
            />

            <LeaveGuestListModal
                entity={entity}
                isVisible={showLeaveGuestListModal}
                state="reject"
                onSubmit={handleSubmit}
                onClose={toggleShowLeaveGuestListModal}
                loading={loading}
            />

            <CopyItem
                isVisible={showCopyEventModal}
                entity={entity}
                onClose={() => setShowCopyEventModal(false)}
            />
        </>
    )
}

export default ActivityActions
