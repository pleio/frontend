import React from 'react'
import styled from 'styled-components'

import Card from 'js/components/Card/Card'
import EventActions from 'js/events/EventActions/EventActions'
import EventAttend from 'js/events/EventAttend/EventAttend'
import EventAttendees from 'js/events/EventAttendees/EventAttendees'

const Wrapper = styled(Card)`
    .AttendeesWrapper {
        padding: 20px 8px;
    }

    .AttendMessage {
        padding: 0 8px;

        &:not(:first-child) {
            padding-top: 8px;
        }

        + .AttendMessage {
            padding-top: 8px;
        }

        + .AttendLoggedOut {
            margin-top: 12px;
        }
    }
`

const AttendeesAndActionsCard = ({ entity, viewer, site }) => {
    const { rsvp, attendEventWithoutAccount } = entity

    return (
        <Wrapper>
            {(rsvp || attendEventWithoutAccount) && (
                <div className="AttendeesWrapper">
                    <EventAttendees entity={entity} viewer={viewer} />
                    <EventAttend entity={entity} viewer={viewer} />
                </div>
            )}
            <EventActions entity={entity} site={site} />
        </Wrapper>
    )
}

export default AttendeesAndActionsCard
