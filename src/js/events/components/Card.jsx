import React from 'react'
import { useTranslation } from 'react-i18next'
import filterViewerFromAttendees from 'helpers/filterViewerFromAttendees'

import FeedItem from 'js/components/FeedItem/FeedItem'
import FeedItemContent from 'js/components/FeedItem/FeedItemContent'
import FeedItemFooter from 'js/components/FeedItem/FeedItemFooter'
import FeedItemImage from 'js/components/FeedItem/FeedItemImage'
import Flexer from 'js/components/Flexer/Flexer'
import People from 'js/components/People'
import EventDate from 'js/events/components/EventDate'
import EventLocation from 'js/events/components/EventLocation'

import getRefetchQueries from '../helpers/getRefetchQueries'

const Card = ({
    'data-feed': dataFeed,
    viewer,
    entity,
    canPin,
    hideSubtype,
    hideGroup,
    hideComments,
    hideActions,
    excerptMaxLines,
}) => {
    const {
        guid,
        location,
        group,
        startDate,
        endDate,
        attendees,
        isAttending,
    } = entity

    const totalAccept = attendees?.totalAccept

    const onEdit = `${group?.url || ''}/events/edit/${guid}`

    const hasAttendees = totalAccept > 0

    const { t } = useTranslation()

    return (
        <FeedItem data-feed={dataFeed}>
            <FeedItemContent
                entity={entity}
                hideSubtype={hideSubtype}
                hideGroup={hideGroup}
                hideExcerpt
                excerptMaxLines={excerptMaxLines}
                canPin={canPin}
                canDuplicate
                onEdit={onEdit}
                hideActions={hideActions}
                refetchQueries={getRefetchQueries(false, !!group)}
            >
                <EventDate
                    startDate={startDate}
                    endDate={endDate}
                    size="normal"
                    style={{ marginTop: '10px' }}
                />
                <Flexer justifyContent="flex-start" gutter="small">
                    {location && (
                        <EventLocation
                            entity={entity}
                            size="small"
                            style={{ marginTop: '12px' }}
                        />
                    )}
                    {hasAttendees && (
                        <People
                            size="normal"
                            showCheck={isAttending === 'accept'}
                            checkLabel={t('entity-event.you-are-going')}
                            users={
                                isAttending === 'accept'
                                    ? filterViewerFromAttendees(
                                          attendees.edges,
                                          viewer.user.guid,
                                      )
                                    : attendees.edges
                            }
                            total={totalAccept}
                            style={{
                                marginTop: '12px',
                                justifyContent: 'flex-start',
                            }}
                        />
                    )}
                </Flexer>
            </FeedItemContent>
            <FeedItemFooter
                entity={entity}
                hideComments={hideComments}
                hideLikes
            />
            <FeedItemImage entity={entity} />
        </FeedItem>
    )
}

export default Card
