import React from 'react'
import { isSameDate, showDay, showMonth } from 'helpers/date/showDate'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

const Wrapper = styled.div`
    flex-shrink: 0;
    display: inline-flex;
    background-color: white;
    border: 1px solid ${(p) => p.theme.color.grey[30]};

    ${(p) =>
        p.size === 'small' &&
        css`
            border-radius: ${(p) => p.theme.radius.small};
        `};

    ${(p) =>
        p.size === 'normal' &&
        css`
            border-radius: ${(p) => p.theme.radius.normal};
        `};

    ${(p) =>
        p.size === 'large' &&
        css`
            border-radius: ${(p) => p.theme.radius.normal};
        `};

    .DateCalendarDate {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;

        &:not(:last-child) {
            border-right: 1px solid ${(p) => p.theme.color.grey[30]};
        }

        ${(p) =>
            p.size === 'small' &&
            css`
                width: 38px;
                height: 44px;
            `};

        ${(p) =>
            p.size === 'normal' &&
            css`
                width: 44px;
                height: 52px;
            `};

        ${(p) =>
            p.size === 'large' &&
            css`
                width: 50px;
                height: 60px;
            `};
    }

    .DateCalendarDay {
        margin-bottom: -4px;
        font-weight: ${(p) => p.theme.font.weight.bold};
        color: ${(p) => p.theme.color.text.black};

        ${(p) =>
            p.size === 'small' &&
            css`
                font-size: ${(p) => p.theme.font.size.large};
                line-height: ${(p) => p.theme.font.lineHeight.large};
            `};

        ${(p) =>
            p.size === 'normal' &&
            css`
                font-size: ${(p) => p.theme.font.size.huge};
                line-height: ${(p) => p.theme.font.lineHeight.huge};
            `};

        ${(p) =>
            p.size === 'large' &&
            css`
                font-size: ${(p) => p.theme.font.size.huge};
                line-height: ${(p) => p.theme.font.lineHeight.huge};
            `};
    }

    .DateCalendarMonth {
        margin-bottom: 2px;
        color: ${(p) => p.theme.color.text.grey};

        ${(p) =>
            p.size === 'small' &&
            css`
                font-size: ${(p) => p.theme.font.size.tiny};
                line-height: ${(p) => p.theme.font.lineHeight.tiny};
            `};

        ${(p) =>
            p.size === 'normal' &&
            css`
                font-size: ${(p) => p.theme.font.size.small};
                line-height: ${(p) => p.theme.font.lineHeight.small};
            `};

        ${(p) =>
            p.size === 'large' &&
            css`
                font-size: ${(p) => p.theme.font.size.small};
                line-height: ${(p) => p.theme.font.lineHeight.small};
            `};
    }
`

const DateCalendar = ({ startDate, endDate, size = 'normal', ...rest }) => (
    <Wrapper aria-hidden size={size} {...rest}>
        {startDate && (
            <div className="DateCalendarDate">
                <div className="DateCalendarDay">{showDay(startDate)}</div>
                <div className="DateCalendarMonth">{showMonth(startDate)}</div>
            </div>
        )}
        {endDate && !isSameDate(startDate, endDate) && (
            <div className="DateCalendarDate">
                <div className="DateCalendarDay">{showDay(endDate)}</div>
                <div className="DateCalendarMonth">{showMonth(endDate)}</div>
            </div>
        )}
    </Wrapper>
)

DateCalendar.propTypes = {
    size: PropTypes.oneOf(['small', 'normal', 'large']),
}

export default DateCalendar
