import React from 'react'
import { useMobilePortrait } from 'helpers/breakpoints'
import showDate, {
    isSameDate,
    isSameTime,
    showDateTime,
    showTime,
} from 'helpers/date/showDate'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import DateCalendar from './DateCalendar'
import HiddenEventDates from './HiddenEventDates'

const Wrapper = styled.div`
    display: flex;
    align-items: center;

    .EventDateFull {
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};

        > *:not(:last-child) {
            margin-bottom: 4px;
        }
    }

    .EventDateCalendar {
        margin-right: ${(p) => (p.$size === 'large' ? '16px' : '12px')};
    }
`

const EventDate = ({ startDate, endDate, size, ...rest }) => {
    const isMobilePortrait = useMobilePortrait()

    if (!startDate && !endDate) return null

    const sameDate = isSameDate(startDate, endDate)
    const sameTime = isSameTime(startDate, endDate)

    return (
        <>
            <HiddenEventDates startDate={startDate} endDate={endDate} />
            <Wrapper $size={size} aria-hidden {...rest}>
                <DateCalendar
                    startDate={startDate}
                    endDate={isMobilePortrait ? null : endDate}
                    size={size}
                    className="EventDateCalendar"
                />
                <div className="EventDateFull">
                    {sameDate ? (
                        <>
                            <div>{showDate(startDate, true, true)}</div>
                            <div>
                                {showTime(startDate)}
                                {!!endDate && !sameTime ? (
                                    <> – {showTime(endDate)}</>
                                ) : null}
                            </div>
                        </>
                    ) : (
                        <>
                            <div>{showDateTime(startDate, true, true)}</div>
                            <div>{showDateTime(endDate, true, true)}</div>
                        </>
                    )}
                </div>
            </Wrapper>
        </>
    )
}

EventDate.propTypes = {
    startDate: PropTypes.string,
    endDate: PropTypes.string,
    size: PropTypes.string,
}

export default EventDate
