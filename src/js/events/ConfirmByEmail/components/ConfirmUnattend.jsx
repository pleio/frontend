import React from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import { gql, useMutation } from '@apollo/client'

import Button from 'js/components/Button/Button'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import Text from 'js/components/Text/Text'

const ConfirmUnattend = ({ entity, email, code, onClose }) => {
    const { t } = useTranslation()
    const navigate = useNavigate()

    const { guid, title, url } = entity

    const [confirmUnattendEvent, { error, loading }] = useMutation(
        CONFIRM_UNATTEND_EVENT,
    )

    const onSubmit = async () => {
        await confirmUnattendEvent({
            variables: { input: { guid, code, email, delete: true } },
            refetchQueries: ['EventItem'],
        })
            .then(() => {
                onClose()
                navigate(url, { replace: true })
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    return (
        <>
            <Text size="small" as="p">
                {t('entity-event.exit-attendees-email-confirm', {
                    title,
                    email,
                })}
            </Text>
            <Errors errors={error} />
            <Flexer mt>
                <Button variant="tertiary" size="normal" onClick={onClose}>
                    {t('action.no-cancel')}
                </Button>
                <Button size="normal" onClick={onSubmit} loading={loading}>
                    {t('action.yes-confirm')}
                </Button>
            </Flexer>
        </>
    )
}

const CONFIRM_UNATTEND_EVENT = gql`
    mutation updateAttendEventState($input: updateAttendEventStateInput!) {
        updateAttendEventState(input: $input) {
            entity {
                guid
            }
        }
    }
`

export default ConfirmUnattend
