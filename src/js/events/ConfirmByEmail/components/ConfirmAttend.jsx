import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import { gql, useMutation } from '@apollo/client'

import Button from 'js/components/Button/Button'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import Spacer from 'js/components/Spacer/Spacer'
import Text from 'js/components/Text/Text'
import AttendFormModal from 'js/events/EventAttend/components/AttendFormModal'

const ConfirmAttend = ({ entity, code, email, onClose }) => {
    const { t } = useTranslation()
    const navigate = useNavigate()

    const {
        control,
        handleSubmit,
        watch,
        formState: { isValid, isSubmitting },
    } = useForm()

    const [updateAttendEventState, { error, loading }] =
        useMutation(CONFIRM_ATTEND_EVENT)

    const onSubmit = async ({ state, formFields }) => {
        const variables = {
            input: { guid, code, email, state },
        }
        if (formFields) {
            variables.input.formFields = formFields
        }

        const result = await updateAttendEventState({
            variables,
            refetchQueries: ['EventItem'],
        })
            .then(() => {
                onClose()
                navigate(url, { replace: true })
            })
            .catch((errors) => {
                return errors
            })
        return result
    }

    const {
        guid,
        title,
        url,
        enableMaybeAttendEvent,
        attendEventOnline,
        maxAttendees,
        attendees: { totalAccept },
        form,
        isFormEnabled,
    } = entity

    const spotsLeft = maxAttendees - totalAccept
    const isFull = maxAttendees && spotsLeft < 1

    const watchState = watch('state')

    const [showEventFormModal, setShowEventFormModal] = useState(false)

    const showEventForm =
        isFormEnabled && form?.fields.length > 0 && watchState === 'accept'

    return (
        <>
            <Spacer spacing="small">
                <Text size="small" as="p">
                    {t('entity-event.sign-up-event-confirm', {
                        title,
                        email,
                    })}
                </Text>
                <FormItem
                    type="radio"
                    control={control}
                    name="state"
                    size="small"
                    legend={t('form.apply-to')}
                    options={[
                        {
                            value: isFull ? 'waitinglist' : 'accept',
                            label: t('entity-event.going'),
                        },
                        ...(attendEventOnline
                            ? [
                                  {
                                      value: 'online',
                                      label: t('entity-event.online'),
                                  },
                              ]
                            : []),
                        ...(enableMaybeAttendEvent
                            ? [
                                  {
                                      value: 'maybe',
                                      label: t('entity-event.maybe'),
                                  },
                              ]
                            : []),
                        {
                            value: 'reject',
                            label: t('entity-event.not-going'),
                        },
                    ]}
                    required
                />

                {watchState === 'waitinglist' && (
                    <p>
                        {t('entity-event.guest-list-full', {
                            title,
                        })}
                        .{' '}
                        {t('entity-event.guest-list-full-confirm', {
                            title,
                        })}
                    </p>
                )}

                <Errors errors={error} />
            </Spacer>

            <Flexer mt>
                <Button variant="tertiary" size="normal" onClick={onClose}>
                    {t('action.cancel')}
                </Button>
                <Button
                    size="normal"
                    disabled={!isValid}
                    loading={isSubmitting}
                    onClick={() => {
                        showEventForm
                            ? setShowEventFormModal(true)
                            : handleSubmit(onSubmit)()
                    }}
                >
                    {t('action.confirm')}
                </Button>
            </Flexer>

            <AttendFormModal
                form={entity.form}
                isVisible={showEventFormModal}
                formState={isFull ? 'waitinglist' : 'accept'}
                onSubmit={onSubmit}
                onClose={() => setShowEventFormModal(false)}
                loading={loading}
            />
        </>
    )
}

const CONFIRM_ATTEND_EVENT = gql`
    mutation updateAttendEventState($input: updateAttendEventStateInput!) {
        updateAttendEventState(input: $input) {
            entity {
                guid
            }
        }
    }
`

export default ConfirmAttend
