import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { getQueryVariable } from 'helpers'

import Modal from 'js/components/Modal/Modal'
import NotFound from 'js/core/NotFound'

import Item from '../Item'

import ConfirmAttend from './components/ConfirmAttend'
import ConfirmUnattend from './components/ConfirmUnattend'

const ConfirmByEmailLink = () => {
    const { t } = useTranslation()
    const location = useLocation()
    const { guid } = useParams()

    const code = getQueryVariable('code', location.search)
    const email = getQueryVariable('email', location.search)
    const unattend = Boolean(getQueryVariable('delete', location.search))

    const [isModalVisible, setModalVisible] = useState(true)
    const closeModal = () => setModalVisible(false)

    const { loading, data } = useQuery(GET_EVENT, {
        variables: {
            guid,
        },
    })

    if (!guid || !code || !email) {
        console.error('Required url params not found')
        return <NotFound />
    }

    if (loading) return null
    if (!data || !data.entity) return <NotFound />

    const { entity } = data

    return (
        <>
            <Item guid={guid} />
            {unattend ? (
                <Modal
                    size="small"
                    isVisible={isModalVisible}
                    title={t('entity-event.exit-guest-list')}
                >
                    <ConfirmUnattend
                        entity={entity}
                        email={email}
                        code={code}
                        onClose={closeModal}
                    />
                </Modal>
            ) : (
                <Modal
                    size="small"
                    isVisible={isModalVisible}
                    title={t('entity-event.sign-up')}
                >
                    <ConfirmAttend
                        entity={entity}
                        email={email}
                        code={code}
                        onClose={closeModal}
                    />
                </Modal>
            )}
        </>
    )
}

const GET_EVENT = gql`
    query EditEvent($guid: String!) {
        entity(guid: $guid) {
            guid
            ... on Event {
                title
                url
                attendees {
                    total
                    totalAccept
                }
                maxAttendees
                enableMaybeAttendEvent
                attendEventOnline
                isFormEnabled
                form {
                    title
                    description
                    fields {
                        guid
                        title
                        description
                        fieldType
                        isMandatory
                        fieldOptions
                    }
                }
            }
        }
    }
`

export default ConfirmByEmailLink
