import { gql, useMutation } from '@apollo/client'

type HandleSubmitProps = {
    state?: 'accept' | 'reject' | 'waitinglist' | 'online' | 'maybe'
    updateSubEvents?: boolean
    formFields?: any
}

const useEventActions = (guid: string) => {
    const [attendEvent, { loading }] = useMutation(ATTEND_EVENT)

    const handleSubmit = ({
        state = null,
        updateSubEvents = false,
        formFields = null,
    }: HandleSubmitProps) => {
        attendEvent({
            variables: {
                input: {
                    guid,
                    state,
                    updateSubEvents,
                    formFields,
                },
            },
            refetchQueries: ['EventItem', 'AttendeesList'],
        }).catch((error) => {
            console.error(error)
        })
    }

    return { loading, handleSubmit }
}

const ATTEND_EVENT = gql`
    mutation AttendEvent($input: attendEventInput!) {
        attendEvent(input: $input) {
            entity {
                guid
                ... on Event {
                    isAttending
                }
            }
        }
    }
`

export default useEventActions
