import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'
import { useWindowScrollPosition } from 'helpers'

import { Container } from 'js/components/Grid/Grid'
import PageHeader from 'js/components/PageHeader/PageHeader'
import Section from 'js/components/Section/Section'
import FeedView from 'js/page/Widget/components/Feed/FeedView'

// First load data, otherwise useWindowScrollPosition and FeedView won't work properly
const ListWrapper = () => {
    const { data, loading } = useQuery(GET_LIST_SETTINGS)

    if (loading) return null
    return <List data={data} />
}

const List = ({ data }) => {
    const { viewer, site } = data

    const { t } = useTranslation()

    const settings = {
        showTagFilter: site?.pageTagFilters.showTagFilter,
        showTagCategories: site?.pageTagFilters.showTagCategories,
        typeFilter: ['event'],
        showEventFilter: true,
        itemView: window.__SETTINGS__.eventTiles ? 'tile' : 'list',
        itemCount: window.__SETTINGS__.eventTiles ? 3 : 10,
    }

    useWindowScrollPosition('events')

    return (
        <>
            <PageHeader
                title={t('entity-event.title-list')}
                canCreate={viewer?.canWriteToContainer}
                createLink="/events/add"
                createLabel={t('entity-event.create')}
            />
            <Section>
                <Container
                    size={window.__SETTINGS__.eventTiles ? 'large' : 'tiny'}
                >
                    <FeedView guid="events" settings={settings} />
                </Container>
            </Section>
        </>
    )
}

const GET_LIST_SETTINGS = gql`
    query ActivityList {
        viewer {
            guid
            canWriteToContainer(subtype: "event")
        }
        site {
            guid
            pageTagFilters(contentType: "event") {
                showTagFilter
                showTagCategories
            }
        }
    }
`

export default ListWrapper
