import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { media } from 'helpers'
import { showDateTime } from 'helpers/date/showDate'
import styled from 'styled-components'

import FetchMore from 'js/components/FetchMore'
import Flexer from 'js/components/Flexer/Flexer'
import IconButton from 'js/components/IconButton/IconButton'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import SearchBar from 'js/components/SearchBar/SearchBar'
import Table from 'js/components/Table'
import TableSortableHeader from 'js/components/TableSortableHeader/TableSortableHeader'
import Text from 'js/components/Text/Text'

import CheckIcon from 'icons/check.svg'

const Wrapper = styled.tr`
    ${media.tabletUp`
        font-size: ${(p) => p.theme.font.size.normal};
        line-height: ${(p) => p.theme.font.lineHeight.normal};
    `};

    .ToggleCheckInButton {
        border-radius: 50%;
        background-color: ${(p) => p.theme.color.grey[10]};
    }
`

const Item = ({ entity, isCheckedIn, onToggleCheckIn }) => {
    const { name, email, timeCheckedIn } = entity

    const toggleCheckIn = () => {
        return onToggleCheckIn(email)
    }

    return (
        <Wrapper $isCheckedIn={isCheckedIn}>
            <td>
                <Flexer justifyContent="flex-start">
                    <IconButton
                        size="large"
                        variant={isCheckedIn ? 'primary' : 'secondary'}
                        className="ToggleCheckInButton"
                        labeliconb
                        tooltip={isCheckedIn ? 'Remove check in' : 'Check in'}
                        onHandle={toggleCheckIn}
                    >
                        <CheckIcon />
                    </IconButton>
                    <span>{name}</span>
                </Flexer>
            </td>
            <td>{email}</td>
            {isCheckedIn && (
                <td style={{ textAlign: 'right' }}>
                    {showDateTime(timeCheckedIn, true, true)}
                </td>
            )}
        </Wrapper>
    )
}

const List = ({ mutate, guid }) => {
    const { t } = useTranslation()
    const { hash } = useLocation()
    const state =
        hash === '#going'
            ? 'accept'
            : hash === '#waiting'
              ? 'waitinglist'
              : null

    const isCheckedIn = !hash

    useEffect(() => {
        setQ('')
        setSearchInput('')
    }, [state])

    const [orderBy, setOrderBy] = useState(
        isCheckedIn ? 'timeCheckedIn' : 'name',
    )
    const [orderDirection, setOrderDirection] = useState('desc')

    useEffect(() => {
        if (!isCheckedIn) setOrderBy('name')
    }, [isCheckedIn])

    const [queryLimit, setQueryLimit] = useState(50)
    const [searchInput, setSearchInput] = useState('')

    const onChangeSearchInput = (evt) => {
        setSearchInput(evt.target.value)
    }

    const [q, setQ] = useState('')
    const onSubmitSearchInput = (evt) => {
        evt.preventDefault()
        setQ(searchInput)
    }

    const { loading, data, fetchMore } = useQuery(Query, {
        variables: {
            query: q,
            guid,
            state,
            isCheckedIn,
            offset: 0,
            limit: queryLimit,
            orderBy,
            orderDirection,
        },
    })

    const toggleCheckIn = (emailAddress) => {
        return new Promise((resolve, reject) => {
            mutate({
                variables: {
                    input: {
                        guid,
                        emailAddress,
                        timeCheckedIn: isCheckedIn
                            ? null
                            : new Date().toISOString(),
                    },
                },
                refetchQueries: ['EvenGuestList', 'EventGuestListWrapper'],
            })
                .then(() => {
                    resolve()
                })
                .catch((errors) => {
                    reject(new Error(errors))
                })
        })
    }

    const attendees = data?.entity?.attendees

    if (!loading && attendees.totalCheckedIn === 0 && state === null) {
        return (
            <Text
                textAlign="center"
                style={{ marginTop: '40px' }}
                role="status"
            >
                {t('entity-event.checked-in-no-results')}
            </Text>
        )
    } else if (
        !loading &&
        attendees.totalAcceptNotCheckedIn === 0 &&
        state === 'accept'
    ) {
        return (
            <Text
                textAlign="center"
                style={{ marginTop: '40px' }}
                role="status"
            >
                {t('entity-event.going-no-results')}
            </Text>
        )
    } else if (
        !loading &&
        attendees.totalWaitinglistNotCheckedIn === 0 &&
        state === 'waitinglist'
    ) {
        return (
            <Text
                textAlign="center"
                style={{ marginTop: '40px' }}
                role="status"
            >
                {t('entity-event.queue-no-results')}
            </Text>
        )
    }

    return (
        <>
            <form method="GET" onSubmit={onSubmitSearchInput}>
                <SearchBar
                    name="search-attendee"
                    value={searchInput}
                    onChange={onChangeSearchInput}
                    label={t('entity-event.search-attendee')}
                    size="large"
                    style={{ maxWidth: '480px', margin: '20px auto 8px' }}
                />
            </form>
            {loading ? (
                <LoadingSpinner style={{ marginTop: '40px' }} />
            ) : (
                <Table style={{ marginTop: '24px' }}>
                    <thead>
                        <tr>
                            <TableSortableHeader
                                name="name"
                                label={t('global.name')}
                                orderBy={orderBy}
                                orderDirection={orderDirection}
                                setOrderBy={setOrderBy}
                                setOrderDirection={setOrderDirection}
                                style={{ paddingLeft: '52px' }}
                            />

                            <TableSortableHeader
                                name="email"
                                label={t('form.email-address')}
                                orderBy={orderBy}
                                orderDirection={orderDirection}
                                setOrderBy={setOrderBy}
                                setOrderDirection={setOrderDirection}
                            />

                            {isCheckedIn && (
                                <TableSortableHeader
                                    name="timeCheckedIn"
                                    label={t('entity-event.checked-in')}
                                    orderBy={orderBy}
                                    orderDirection={orderDirection}
                                    setOrderBy={setOrderBy}
                                    setOrderDirection={setOrderDirection}
                                    align="right"
                                />
                            )}
                        </tr>
                    </thead>
                    <FetchMore
                        as="tbody"
                        edges={data.entity.attendees.edges}
                        getMoreResults={(data) => data.attendees.edges}
                        fetchMore={fetchMore}
                        fetchType="scroll"
                        fetchCount={50}
                        setLimit={setQueryLimit}
                        maxLimit={data.entity.attendees.total}
                    >
                        {data.entity.attendees.edges.map((attendee, i) => {
                            return (
                                <Item
                                    key={`${attendee.email}-${i}`}
                                    entity={attendee}
                                    isCheckedIn={isCheckedIn}
                                    onToggleCheckIn={toggleCheckIn}
                                />
                            )
                        })}
                    </FetchMore>
                </Table>
            )}
        </>
    )
}

const Query = gql`
    query EvenGuestList(
        $guid: String!
        $limit: Int!
        $state: String
        $isCheckedIn: Boolean
        $orderBy: AttendeeOrderBy
        $orderDirection: OrderDirection
        $query: String
    ) {
        entity(guid: $guid) {
            guid
            ... on Event {
                attendees(
                    limit: $limit
                    state: $state
                    isCheckedIn: $isCheckedIn
                    orderBy: $orderBy
                    orderDirection: $orderDirection
                    query: $query
                ) {
                    total
                    totalCheckedIn
                    totalAcceptNotCheckedIn
                    totalWaitinglistNotCheckedIn
                    edges {
                        name
                        email
                        state
                        url
                        icon
                        timeCheckedIn
                    }
                }
            }
        }
    }
`

const Mutation = gql`
    mutation editEventAttendee($input: editEventAttendeeInput!) {
        editEventAttendee(input: $input) {
            entity {
                guid
            }
        }
    }
`

export default graphql(Mutation)(List)
