import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useLocation, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import Document from 'js/components/Document'
import Flexer from 'js/components/Flexer/Flexer'
import { Container } from 'js/components/Grid/Grid'
import { H1 } from 'js/components/Heading'
import Section from 'js/components/Section/Section'
import TabMenu from 'js/components/TabMenu'
import TabPage from 'js/components/TabPage'
import Text from 'js/components/Text/Text'
import NotFound from 'js/core/NotFound'
import EventDataSummary from 'js/events/components/EventDataSummary'

import List from './List'

const GuestList = () => {
    const params = useParams()
    const hash = useLocation().hash?.slice(1) || ''

    const { guid } = params

    const { loading, data } = useQuery(GET_EVENT_GUEST_LIST, {
        variables: {
            guid,
        },
    })

    const { t } = useTranslation()

    if (loading) return null
    if (!data || !data.entity || !data.entity.canEdit) return <NotFound />

    const { entity, viewer } = data
    const { title, url, attendees } = entity
    const {
        totalCheckedIn,
        totalAcceptNotCheckedIn,
        totalWaitinglistNotCheckedIn,
    } = attendees

    return (
        <>
            <Document
                title={t('entity-event.guest-list')}
                containerTitle={title}
            />

            <Section backgroundColor="white" grow>
                <Container>
                    <Flexer justifyContent="flex-start" wrap>
                        <div style={{ flexGrow: 1 }}>
                            <H1>{t('entity-event.guest-list')}</H1>
                            <Text>
                                <Link to={url}>{title}</Link>
                            </Text>
                        </div>
                        <EventDataSummary
                            entity={entity}
                            viewer={viewer}
                            style={{ padding: '4px 0' }}
                        />
                    </Flexer>
                    <TabMenu
                        label={t('entity-event.guest-list')}
                        showBorder
                        options={[
                            {
                                label: `${t(
                                    'entity-event.checked-in',
                                )} (${totalCheckedIn})`,
                                link: '',
                                isActive: !hash,
                                id: 'tab-checked-in',
                                ariaControls: 'tabpanel-checked-in',
                            },
                            {
                                label: `${t(
                                    'entity-event.going',
                                )} (${totalAcceptNotCheckedIn})`,
                                link: 'going',
                                isActive: hash === 'going',
                                id: 'tab-going',
                                ariaControls: 'tabpanel-going',
                            },
                            {
                                label: `${t(
                                    'entity-event.queue',
                                )} (${totalWaitinglistNotCheckedIn})`,
                                link: 'waiting',
                                isActive: hash === 'waiting',
                                id: 'tab-waiting',
                                ariaControls: 'tabpanel-waiting',
                            },
                        ]}
                        sticky
                    />
                    <TabPage
                        id={`tabpanel-${hash || 'checked-in'}`}
                        aria-labelledby={`tab-${hash || 'checked-in'}`}
                        visible
                    >
                        <List guid={guid} />
                    </TabPage>
                </Container>
            </Section>
        </>
    )
}

const GET_EVENT_GUEST_LIST = gql`
    query EventGuestListWrapper($guid: String!) {
        viewer {
            guid
            user {
                guid
            }
        }
        entity(guid: $guid) {
            guid
            ... on Event {
                title
                startDate
                endDate
                isAttending
                attendees(
                    limit: 3
                    state: "accept"
                    orderBy: timeUpdated
                    orderDirection: desc
                ) {
                    total
                    totalAccept
                    totalCheckedIn
                    totalAcceptNotCheckedIn
                    totalWaitinglistNotCheckedIn
                    edges {
                        guid
                        name
                        state
                        url
                        icon
                    }
                }
                url
                canEdit
            }
        }
    }
`

export default GuestList
