import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useLocation } from 'react-router-dom'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import DropdownButton from 'js/components/DropdownButton/DropdownButton'
import Flexer from 'js/components/Flexer/Flexer'
import HideVisually from 'js/components/HideVisually/HideVisually'
import Modal from 'js/components/Modal/Modal'
import RadioField from 'js/components/RadioField/RadioField'
import Text from 'js/components/Text/Text'
import useEventActions from 'js/events/hooks/useEventActions'

import ExclamationIcon from 'icons/exclamation.svg'

import AttendFormModal from './components/AttendFormModal'
import AttendWithoutAccount from './components/AttendWithoutAccount'
import JoinQueueModal from './components/JoinQueueModal'
import LeaveGuestListModal from './components/LeaveGuestListModal'
import LeaveQueueModal from './components/LeaveQueueModal'
import SignedUpInSlotModal from './components/SignedUpInSlotModal'

const AttendingWrapper = styled.fieldset`
    display: flex;
    justify-content: center;

    .AttendRadioField {
        width: calc(100% / 3);
        padding: 8px 0;
        justify-content: center;
    }

    &:last-child {
        margin-bottom: -4px;
    }
`

const EventAttend = ({ viewer, entity }) => {
    const { t } = useTranslation()
    const location = useLocation()

    const { loading, handleSubmit } = useEventActions(entity.guid)

    const [showAttendWithoutAccountModal, setShowAttendWithoutAccountModal] =
        useState(false)
    const toggleAttendWithoutAccountModal = () =>
        setShowAttendWithoutAccountModal(!showAttendWithoutAccountModal)

    const [showJoinQueueModal, setShowJoinQueueModal] = useState(false)
    const toggleJoinQueueModal = () => {
        setShowJoinQueueModal(!showJoinQueueModal)
    }

    const [showLeaveQueueModal, setShowLeaveQueueModal] = useState(false)
    const toggleLeaveQueueModal = () => {
        setShowLeaveQueueModal(!showLeaveQueueModal)
    }

    const [showSignedUpInSlotModal, setShowSignedUpInSlotModal] =
        useState(false)
    const toggleSignedUpInSlotModal = () => {
        setShowSignedUpInSlotModal(!showSignedUpInSlotModal)
    }

    const [leaveGuestListState, setLeaveGuestListState] = useState(null)
    const closeLeaveGuestListModal = () => setLeaveGuestListState(null)

    const [showAttendForm, setShowAttendForm] = useState(false)
    const toggleAttendForm = () => setShowAttendForm(!showAttendForm)

    const {
        guid,
        rsvp,
        isAttending,
        maxAttendees,
        attendees: { totalAccept },
        alreadySignedUpInSlot,
        attendEventWithoutAccount,
        enableMaybeAttendEvent,
        attendEventOnline,
        statusPublished,
        isAttendingSubEvents,
        isFormEnabled,
    } = entity

    const { loggedIn } = viewer

    const spotsLeft = maxAttendees - totalAccept
    const isFull = maxAttendees && spotsLeft < 1

    const setAttending = (state) => {
        // Show warning if user is leaving the guest list (if it has max attendees or also attending sub events)
        if (
            isAttending === 'accept' &&
            state !== 'accept' &&
            (maxAttendees || isAttendingSubEvents)
        ) {
            setLeaveGuestListState(state)
            return
        }

        if (state === 'accept') {
            if (alreadySignedUpInSlot) {
                toggleSignedUpInSlotModal()
                return
            }
            if (isFull) {
                toggleJoinQueueModal()
                return
            }
            if (isFormEnabled) {
                toggleAttendForm()
                return
            }
        }

        handleSubmit({ state })
    }

    const joinQueue = (options) => {
        if (isFormEnabled) {
            toggleAttendForm()
            return
        }

        handleSubmit(options)
    }

    if (statusPublished !== 'published') return null

    const defaultFieldProps = {
        className: 'AttendRadioField',
        name: `${guid}-attend`,
        disabled: loading,
    }

    return (
        <>
            {rsvp && isAttending && (
                <AttendingWrapper>
                    <HideVisually as="legend">
                        {t('entity-event.your-attendance')}
                    </HideVisually>
                    <RadioField
                        {...defaultFieldProps}
                        id={`${guid}-accept`}
                        value={!isFull ? 'accept' : 'waitinglist'}
                        checked={
                            isAttending === 'accept' ||
                            isAttending === 'waitinglist'
                        }
                        onChange={(evt) => setAttending(evt.target.value)}
                        iconElement={
                            isAttending === 'waitinglist' && <ExclamationIcon />
                        }
                    />
                    {attendEventOnline && (
                        <RadioField
                            {...defaultFieldProps}
                            id={`${guid}-online`}
                            value="online"
                            checked={isAttending === 'online'}
                            onChange={(evt) => setAttending(evt.target.value)}
                        />
                    )}
                    {enableMaybeAttendEvent && (
                        <RadioField
                            {...defaultFieldProps}
                            id={`${guid}-maybe`}
                            value="maybe"
                            checked={isAttending === 'maybe'}
                            onChange={(evt) => setAttending(evt.target.value)}
                        />
                    )}
                    <RadioField
                        {...defaultFieldProps}
                        id={`${guid}-reject`}
                        value="reject"
                        checked={isAttending === 'reject'}
                        onChange={(evt) => setAttending(evt.target.value)}
                    />

                    <LeaveGuestListModal
                        entity={entity}
                        isVisible={!!leaveGuestListState}
                        state={leaveGuestListState}
                        onSubmit={handleSubmit}
                        onClose={closeLeaveGuestListModal}
                        loading={loading}
                    />
                </AttendingWrapper>
            )}

            {maxAttendees && (rsvp || attendEventWithoutAccount) && (
                <>
                    <Text
                        textAlign="center"
                        size="small"
                        className="AttendMessage"
                    >
                        {spotsLeft > 0
                            ? t('entity-event.spots-available', {
                                  count: spotsLeft,
                              })
                            : t('entity-event.guest-list-full')}
                        .
                    </Text>
                    {isAttending === 'waitinglist' && (
                        <AttendingWrapper>
                            <Button
                                style={{ margin: '8px auto 0' }}
                                size="normal"
                                variant="secondary"
                                onClick={toggleLeaveQueueModal}
                            >
                                {t('entity-event.exit-queue')}
                            </Button>
                        </AttendingWrapper>
                    )}
                </>
            )}

            {loggedIn && rsvp && !isAttending && (
                <div
                    style={{
                        display: 'flex',
                        justifyContent: 'center',
                        marginTop: '16px',
                    }}
                >
                    <DropdownButton
                        options={[
                            {
                                onClick: () => setAttending('accept'),
                                name: attendEventOnline
                                    ? t('entity-event.going')
                                    : !isFull
                                      ? t('entity-event.sign-up')
                                      : t('entity-event.join-queue'),
                                default: !attendEventOnline,
                            },
                            ...(attendEventOnline
                                ? [
                                      {
                                          onClick: () =>
                                              handleSubmit({ state: 'online' }),
                                          name: t('entity-event.online'),
                                      },
                                  ]
                                : []),
                            ...(enableMaybeAttendEvent
                                ? [
                                      {
                                          onClick: () =>
                                              handleSubmit({ state: 'maybe' }),
                                          name: t('entity-event.maybe'),
                                      },
                                  ]
                                : []),
                            {
                                onClick: () =>
                                    handleSubmit({ state: 'reject' }),
                                name: t('entity-event.not-going'),
                            },
                        ]}
                        showArrow
                    >
                        {attendEventOnline && (
                            <Button
                                size="normal"
                                variant="primary"
                                loading={loading}
                            >
                                <span
                                    style={{
                                        marginRight: '6px',
                                    }}
                                >
                                    {t('entity-event.sign-up')}
                                </span>
                            </Button>
                        )}
                    </DropdownButton>
                </div>
            )}

            {!loggedIn &&
                (window.__SETTINGS__.showLoginRegister ||
                    attendEventWithoutAccount) && (
                    <Flexer gutter="small" wrap className="AttendLoggedOut">
                        <Text size="small" variant="grey">
                            {t('entity-event.to-sign-up')}
                        </Text>

                        {window.__SETTINGS__.showLoginRegister && rsvp && (
                            <Button
                                size="small"
                                variant="primary"
                                as={Link}
                                to="/login"
                                state={{ next: location.pathname }}
                            >
                                {t('action.login')}
                            </Button>
                        )}

                        {window.__SETTINGS__.showLoginRegister &&
                            rsvp &&
                            attendEventWithoutAccount && (
                                <Text size="small" variant="grey">
                                    {t('global.or')}
                                </Text>
                            )}

                        {attendEventWithoutAccount && (
                            <>
                                <Button
                                    size="small"
                                    variant="secondary"
                                    type="button"
                                    onClick={toggleAttendWithoutAccountModal}
                                >
                                    {t('global.use-email')}
                                </Button>
                                <Modal
                                    size="small"
                                    isVisible={showAttendWithoutAccountModal}
                                    onClose={toggleAttendWithoutAccountModal}
                                    title={
                                        isFull
                                            ? t('entity-event.guest-list-full')
                                            : t('entity-event.sign-up')
                                    }
                                >
                                    <AttendWithoutAccount
                                        entity={entity}
                                        state={
                                            isFull ? 'waitinglist' : 'accept'
                                        }
                                        onClose={
                                            toggleAttendWithoutAccountModal
                                        }
                                    />
                                </Modal>
                            </>
                        )}
                    </Flexer>
                )}
            <AttendFormModal
                form={entity.form}
                isVisible={showAttendForm}
                formState={isFull ? 'waitinglist' : 'accept'}
                onSubmit={handleSubmit}
                onClose={() => {
                    toggleAttendForm()
                    setShowLeaveQueueModal(false)
                }}
                loading={loading}
            />
            <JoinQueueModal
                entity={entity}
                isVisible={showJoinQueueModal}
                handleSubmit={joinQueue}
                onClose={toggleJoinQueueModal}
                loading={loading}
            />
            <LeaveQueueModal
                entity={entity}
                isVisible={showLeaveQueueModal}
                handleSubmit={handleSubmit}
                onClose={toggleLeaveQueueModal}
                loading={loading}
            />
            <SignedUpInSlotModal
                isVisible={showSignedUpInSlotModal}
                onClose={toggleSignedUpInSlotModal}
            />
        </>
    )
}

export default EventAttend
