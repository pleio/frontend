import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import Button from 'js/components/Button/Button'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import Spacer from 'js/components/Spacer/Spacer'

const AttendWithoutAccount = ({ mutateAttend, entity, state, onClose }) => {
    const { t } = useTranslation()
    const [queryErrors, setQueryErrors] = useState()
    const [success, setSuccess] = useState(false)

    const submit = async ({ name, email }) => {
        await mutateAttend({
            variables: {
                input: {
                    guid: entity.guid,
                    name,
                    email,
                },
            },
        })
            .then(() => {
                setSuccess(true)
            })
            .catch((errors) => {
                setQueryErrors(errors)
                console.error(errors)
            })
    }

    const defaultValues = {
        name: '',
        email: '',
    }

    const {
        control,
        handleSubmit,
        formState: { errors, isValid, isSubmitting },
    } = useForm({ mode: 'onChange', defaultValues })

    if (success) {
        return (
            <>
                {t('entity-event.request-attend-email-sent')}
                <Flexer mt>
                    <Button size="normal" variant="primary" onClick={onClose}>
                        {t('action.ok')}
                    </Button>
                </Flexer>
            </>
        )
    } else {
        return (
            <Spacer
                as="form"
                spacing="small"
                noValidate
                onSubmit={handleSubmit(submit)}
            >
                {state === 'waitinglist' && (
                    <p style={{ marginBottom: 24 }}>
                        {t('entity-event.guest-list-full-confirm', {
                            title: entity.title,
                        })}
                    </p>
                )}
                <FormItem
                    control={control}
                    type="text"
                    name="name"
                    label={t('global.name')}
                    required
                    errors={errors}
                />
                <FormItem
                    control={control}
                    type="email"
                    name="email"
                    label={t('form.email-address')}
                    errors={errors}
                    required
                />
                <Errors errors={queryErrors} />
                <Flexer mt>
                    <Button
                        size="normal"
                        variant="tertiary"
                        type="button"
                        onClick={onClose}
                    >
                        {t('action.cancel')}
                    </Button>
                    <Button
                        size="normal"
                        variant="primary"
                        type="submit"
                        disabled={!isValid}
                        loading={isSubmitting}
                    >
                        {t('action.confirm')}
                    </Button>
                </Flexer>
            </Spacer>
        )
    }
}

const RequestAttendMutation = gql`
    mutation RequestAttendance($input: attendEventWithoutAccountInput!) {
        attendEventWithoutAccount(input: $input) {
            entity {
                guid
                ... on Event {
                    title
                }
            }
        }
    }
`

export default graphql(RequestAttendMutation, { name: 'mutateAttend' })(
    AttendWithoutAccount,
)
