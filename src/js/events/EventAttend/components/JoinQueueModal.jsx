import React from 'react'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import Modal from 'js/components/Modal/Modal'

const JoinQueueModal = ({
    entity,
    isVisible,
    handleSubmit,
    onClose,
    loading,
}) => {
    const { t } = useTranslation()
    const submitJoinQueue = () => handleSubmit({ state: 'waitinglist' })

    return (
        <Modal
            size="small"
            isVisible={isVisible}
            onClose={onClose}
            title={t('entity-event.guest-list-full')}
        >
            <p>
                {t('entity-event.guest-list-full-confirm', {
                    title: entity.title,
                })}
            </p>
            <Flexer mt>
                <Button variant="tertiary" size="normal" onClick={onClose}>
                    {t('action.no-cancel')}
                </Button>
                <Button
                    onClick={submitJoinQueue}
                    size="normal"
                    loading={loading}
                >
                    {t('action.yes-confirm')}
                </Button>
            </Flexer>
        </Modal>
    )
}

export default JoinQueueModal
