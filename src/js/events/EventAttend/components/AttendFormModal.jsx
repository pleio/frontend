import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import RequiredFieldsMessage from 'js/components/EntityActions/components/RequiredFieldsMessage'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import Modal from 'js/components/Modal/Modal'
import Spacer from 'js/components/Spacer/Spacer'

const AttendFormModal = ({
    form,
    isVisible,
    onClose,
    onSubmit,
    loading,
    formState,
    ...rest
}) => {
    const { t } = useTranslation()

    if (!form) return null

    return (
        <Modal
            size="normal"
            isVisible={isVisible}
            onClose={onClose}
            title={`${form.title}${onSubmit ? '' : ` (${t('action.preview')})`}`}
            showCloseButton
        >
            <AttendForm
                form={form}
                onClose={onClose}
                onSubmit={onSubmit}
                loading={loading}
                formState={formState}
                {...rest}
            />
        </Modal>
    )
}

// Render separately so that it only loads if modal is visible
const AttendForm = ({ form, onClose, onSubmit, loading, formState }) => {
    const { t } = useTranslation()

    const [serverErrors, setServerErrors] = useState(false)

    const { description, fields } = form

    const hasMandatoryFields = fields.some((field) => field.isMandatory)

    const {
        control,
        handleSubmit,
        formState: { errors, isValid },
    } = useForm({
        mode: 'onChange',
    })

    const submit = async (values) => {
        const valuesArray = Object.keys(values).map((key) => {
            const value = values[key]
            return {
                guid: key,
                value: Array.isArray(value) ? value.join(',') : value || '',
            }
        })
        const errors = await onSubmit({
            state: formState,
            formFields: valuesArray,
        })
        setServerErrors(errors)
    }

    const fieldTypes = {
        textField: 'text',
        htmlField: 'rich',
        radioField: 'radio',
        checkboxField: 'checkboxes',
        dateField: 'date',
        dateTimeField: 'datetime',
        selectField: 'select',
        multiSelectField: 'select',
    }

    return (
        <form onSubmit={handleSubmit(submit)}>
            <Spacer spacing="normal">
                {description && <p>{description}</p>}
                {fields.map(
                    (
                        {
                            guid,
                            title,
                            description,
                            fieldType,
                            isMandatory,
                            fieldOptions,
                        },
                        i,
                    ) => {
                        const customFieldProps = {
                            ...([
                                'radioField',
                                'checkboxField',
                                'selectField',
                                'multiSelectField',
                            ].includes(fieldType) && {
                                options: fieldOptions.map((option) => ({
                                    value: option,
                                    label: option,
                                })),
                            }),
                            ...([
                                'selectField',
                                'dateField',
                                'dateTimeField',
                            ].includes(fieldType) && {
                                isClearable: true,
                            }),
                            ...(['radioField', 'checkboxField'].includes(
                                fieldType,
                            ) && {
                                size: 'small',
                            }),
                            ...(fieldType === 'multiSelectField' && {
                                isMulti: true,
                            }),
                            ...(fieldType === 'htmlField' && {
                                textSize: 'small',
                            }),
                        }

                        const id = guid || `${i}-${title}` // Fallback to index for form preview

                        return (
                            <FormItem
                                key={id}
                                control={control}
                                name={id}
                                type={fieldTypes[fieldType]}
                                title={title}
                                description={description}
                                errors={errors}
                                required={isMandatory}
                                {...customFieldProps}
                            />
                        )
                    },
                )}
                {hasMandatoryFields && <RequiredFieldsMessage />}
                {serverErrors && <Errors errors={serverErrors} />}
            </Spacer>

            {onSubmit && (
                <Flexer mt>
                    <Button variant="tertiary" size="normal" onClick={onClose}>
                        {t('action.cancel')}
                    </Button>
                    <Button
                        size="normal"
                        type="submit"
                        disabled={!isValid}
                        loading={loading}
                    >
                        {t('action.confirm')}
                    </Button>
                </Flexer>
            )}
        </form>
    )
}

export default AttendFormModal
