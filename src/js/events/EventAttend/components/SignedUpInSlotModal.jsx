import React from 'react'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import Modal from 'js/components/Modal/Modal'

const SignedUpInSlotModal = ({ isVisible, onClose }) => {
    const { t } = useTranslation()

    return (
        <Modal
            size="small"
            isVisible={isVisible}
            onClose={onClose}
            title={t('entity-event.sign-up')}
        >
            <p>{t('entity-event.already-signed-up-in-slot')}</p>
            <Flexer mt>
                <Button onClick={onClose} size="normal">
                    {t('action.ok')}
                </Button>
            </Flexer>
        </Modal>
    )
}

export default SignedUpInSlotModal
