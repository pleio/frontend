import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import Checkbox from 'js/components/Checkbox/Checkbox'
import Flexer from 'js/components/Flexer/Flexer'
import Modal from 'js/components/Modal/Modal'

const LeaveGuestListModal = ({
    entity,
    isVisible,
    state,
    onSubmit,
    onClose,
    loading,
}) => {
    const { t } = useTranslation()

    const { title, isAttendingSubEvents } = entity

    const [updateSubEvents, setUpdateSubEvents] = useState(false)

    return (
        <Modal
            size="small"
            isVisible={isVisible}
            onClose={onClose}
            title={t('entity-event.exit-guest-list')}
        >
            {state === 'online'
                ? t('entity-event.exit-physical-guest-list-confirm', {
                      title,
                  })
                : t('entity-event.exit-guest-list-confirm', {
                      title,
                  })}

            {isAttendingSubEvents && (
                <Checkbox
                    name="updateSubEvents"
                    label={t('entity-event.exit-guest-list-all-activities')}
                    checked={updateSubEvents}
                    onChange={(evt) => setUpdateSubEvents(evt.target.checked)}
                    size="small"
                    style={{ marginTop: '8px' }}
                />
            )}

            <Flexer mt>
                <Button variant="tertiary" size="normal" onClick={onClose}>
                    {t('action.no-cancel')}
                </Button>
                <Button
                    size="normal"
                    variant="primary"
                    onClick={() => {
                        onSubmit({ state, updateSubEvents })
                    }}
                    loading={loading}
                >
                    {t('action.yes-confirm')}
                </Button>
            </Flexer>
        </Modal>
    )
}

export default LeaveGuestListModal
