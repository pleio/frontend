import React from 'react'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import Modal from 'js/components/Modal/Modal'

const LeaveQueueModal = ({
    entity,
    isVisible,
    handleSubmit,
    onClose,
    loading,
}) => {
    const { t } = useTranslation()
    const submitLeaveQueue = () => handleSubmit({ state: null })

    return (
        <Modal
            size="small"
            isVisible={isVisible}
            onClose={onClose}
            title={t('entity-event.exit-queue')}
        >
            <p>
                {t('entity-event.exit-queue-confirm', {
                    title: entity.title,
                })}
            </p>
            <Flexer mt>
                <Button variant="tertiary" size="normal" onClick={onClose}>
                    {t('action.no-cancel')}
                </Button>
                <Button
                    onClick={submitLeaveQueue}
                    size="normal"
                    loading={loading}
                >
                    {t('action.yes-confirm')}
                </Button>
            </Flexer>
        </Modal>
    )
}

export default LeaveQueueModal
