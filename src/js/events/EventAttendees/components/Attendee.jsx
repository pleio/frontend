import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import IconButton from 'js/components/IconButton/IconButton'
import Modal from 'js/components/Modal/Modal'
import UserLink from 'js/components/UserLink'

import RemoveIcon from 'icons/cross.svg'

const Wrapper = styled.div`
    display: flex;
    padding: 4px 0;

    &:not(:last-child) {
        border-bottom: 1px solid ${(p) => p.theme.color.grey[20]};
    }

    .RemoveUser {
        display: flex;
        flex: 1;
        justify-content: flex-end;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        align-items: center;
    }
`

const Attendee = ({ containerGuid, containerCanEdit, entity }) => {
    const { t } = useTranslation()

    const [showRemoveAttendeeModal, setShowRemoveAttendeeModal] =
        useState(false)
    const toggleShowRemoveAttendeeModal = () =>
        setShowRemoveAttendeeModal(!showRemoveAttendeeModal)

    const [mutate] = useMutation(DELETE_ATTENDEE)

    const submitRemoveAttendee = async ({ updateSubEvents }) => {
        await mutate({
            variables: {
                input: {
                    guid: containerGuid,
                    emailAddresses: [email],
                    updateSubEvents,
                },
            },
            refetchQueries: ['EventItem', 'AttendeesList'],
        })
            .then(() => {
                toggleShowRemoveAttendeeModal()
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    const defaultValues = {
        updateSubEvents: false,
    }

    const {
        control,
        handleSubmit,
        formState: { isValid, isSubmitting },
    } = useForm({ mode: 'onChange', defaultValues })

    const { guid, email, isAttendingSubEvents } = entity
    const isUser = !!guid

    return (
        <>
            <Wrapper>
                <UserLink
                    entity={entity}
                    isEmail={!isUser}
                    style={{ width: '100%', flex: 1 }}
                />
                {containerCanEdit && (
                    <div className="RemoveUser">
                        {email}
                        <IconButton
                            size="large"
                            hoverSize="normal"
                            variant="secondary"
                            aria-label={t('entity-event.remove-attendee')}
                            tooltip={t('action.remove')}
                            onClick={toggleShowRemoveAttendeeModal}
                        >
                            <RemoveIcon />
                        </IconButton>
                    </div>
                )}
            </Wrapper>

            <Modal
                isVisible={showRemoveAttendeeModal}
                onClose={toggleShowRemoveAttendeeModal}
                title={t('entity-event.remove-attendee')}
                size="small"
            >
                <form onSubmit={handleSubmit(submitRemoveAttendee)}>
                    {t('entity-event.remove-attendee-confirm', {
                        email,
                    })}
                    {isAttendingSubEvents && (
                        <FormItem
                            control={control}
                            type="checkbox"
                            name="updateSubEvents"
                            label={t(
                                'entity-event.remove-attendee-all-activities',
                            )}
                            size="small"
                            style={{ marginTop: '8px' }}
                        />
                    )}
                    <Flexer mt>
                        <Button
                            size="normal"
                            variant="tertiary"
                            type="button"
                            onClick={toggleShowRemoveAttendeeModal}
                        >
                            {t('action.no-back')}
                        </Button>
                        <Button
                            size="normal"
                            variant="primary"
                            type="submit"
                            disabled={!isValid}
                            loading={isSubmitting}
                        >
                            {t('action.yes-confirm')}
                        </Button>
                    </Flexer>
                </form>
            </Modal>
        </>
    )
}

const DELETE_ATTENDEE = gql`
    mutation DeleteEventAttendees($input: deleteEventAttendeesInput!) {
        deleteEventAttendees(input: $input) {
            entity {
                guid
            }
        }
    }
`

export default Attendee
