import React from 'react'
import { useTranslation } from 'react-i18next'

import Modal from 'js/components/Modal/Modal'
import TabMenu from 'js/components/TabMenu'
import TabPage from 'js/components/TabPage'

import AttendeesList from './AttendeesList'

const AttendeesModal = ({ tab, setTab, entity }) => {
    const {
        canEdit,
        maxAttendees,
        attendees: {
            totalAccept,
            totalUnconfirmed,
            totalMaybe,
            totalOnline,
            totalReject,
            totalWaitinglist,
        },
        enableMaybeAttendEvent,
        attendEventOnline,
    } = entity

    const { t } = useTranslation()

    const showAttendMaybeTab = enableMaybeAttendEvent || (canEdit && totalMaybe)
    const showAttendOnlineTab = attendEventOnline || (canEdit && totalOnline)

    const tabOptions = [
        {
            label: `${t('entity-event.going')} (${totalAccept})`,
            onClick: () => setTab('going'),
            isActive: tab === 'going',
            id: 'tab-going',
            'aria-controls': 'tabpanel-going',
        },
        ...(canEdit && (maxAttendees || totalWaitinglist)
            ? [
                  {
                      label: `${t('entity-event.queue')} (${totalWaitinglist})`,
                      onClick: () => setTab('waitinglist'),
                      isActive: tab === 'waitinglist',
                      id: 'tab-waitinglist',
                      'aria-controls': 'tabpanel-waitinglist',
                  },
              ]
            : []),
        ...(showAttendOnlineTab
            ? [
                  {
                      label: `${t('entity-event.online')} (${totalOnline})`,
                      onClick: () => setTab('online'),
                      isActive: tab === 'online',
                      id: 'tab-online',
                      'aria-controls': 'tabpanel-online',
                  },
              ]
            : []),
        ...(showAttendMaybeTab
            ? [
                  {
                      label: `${t('entity-event.maybe')} (${totalMaybe})`,
                      onClick: () => setTab('maybe'),
                      isActive: tab === 'maybe',
                      id: 'tab-maybe',
                      'aria-controls': 'tabpanel-maybe',
                  },
              ]
            : []),
        {
            label: `${t('entity-event.not-going')} (${totalReject})`,
            onClick: () => setTab('not-going'),
            isActive: tab === 'not-going',
            id: 'tab-not-going',
            'aria-controls': 'tabpanel-not-going',
        },
        ...(canEdit && totalUnconfirmed
            ? [
                  {
                      label: `${t(
                          'entity-event.unconfirmed',
                      )} (${totalUnconfirmed})`,
                      onClick: () => setTab('unconfirmed'),
                      isActive: tab === 'unconfirmed',
                      id: 'tab-unconfirmed',
                      'aria-controls': 'tabpanel-unconfirmed',
                  },
              ]
            : []),
    ]

    return (
        <Modal
            isVisible={!!tab}
            onClose={() => setTab(false)}
            title={t('entity-event.attendees')}
            size="normal"
            showCloseButton
            containHeight
        >
            <TabMenu
                label={t('entity-event.attendees')}
                options={tabOptions}
                edgePadding
                edgeMargin
                showBorder
            />

            <TabPage
                id={`tabpanel-${tab}`}
                aria-labelledby={`tab-${tab}`}
                visible
            >
                {tab === 'going' && (
                    <AttendeesList guid={entity.guid} state="accept" />
                )}
                {tab === 'maybe' && (
                    <AttendeesList guid={entity.guid} state="maybe" />
                )}
                {tab === 'online' && (
                    <AttendeesList guid={entity.guid} state="online" />
                )}
                {tab === 'not-going' && (
                    <AttendeesList guid={entity.guid} state="reject" />
                )}
                {tab === 'waitinglist' && (
                    <AttendeesList guid={entity.guid} state="waitinglist" />
                )}
                {tab === 'unconfirmed' && (
                    <AttendeesList guid={entity.guid} state="unconfirmed" />
                )}
            </TabPage>
        </Modal>
    )
}

export default AttendeesModal
