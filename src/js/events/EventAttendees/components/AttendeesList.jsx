import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'
import { useIsMount } from 'helpers'

import FetchMore from 'js/components/FetchMore'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import SearchBar from 'js/components/SearchBar/SearchBar'

import Attendee from './Attendee'

const AttendeesList = ({ guid, state }) => {
    const [queryLimit, setQueryLimit] = useState(50)

    const [searchInput, setSearchInput] = useState('')

    const onChangeSearchInput = (evt) => {
        setSearchInput(evt.target.value)
    }

    const [q, setQ] = useState('')
    const onSubmitSearchInput = (evt) => {
        evt.preventDefault()
        setQ(searchInput)
    }

    const { loading, data, fetchMore, refetch } = useQuery(GET_ATTENDEES_LIST, {
        variables: {
            query: q,
            guid,
            state,
            offset: 0,
            limit: queryLimit,
        },
    })
    const isMount = useIsMount()

    useEffect(() => {
        if (isMount) {
            refetch()
        }
    })

    const { t } = useTranslation()

    return (
        <>
            <form method="GET" onSubmit={onSubmitSearchInput}>
                <SearchBar
                    name="search-attendee"
                    value={searchInput}
                    onChange={onChangeSearchInput}
                    label={t('entity-event.search-attendee')}
                    style={{ margin: '20px 0 8px' }}
                />
            </form>
            {loading ? (
                <LoadingSpinner />
            ) : (
                <FetchMore
                    edges={data.entity.attendees.edges}
                    getMoreResults={(data) => data.entity.attendees.edges}
                    fetchMore={fetchMore}
                    fetchType="scroll"
                    fetchCount={50}
                    setLimit={setQueryLimit}
                    maxLimit={data.entity.attendees.total}
                    isScrollableBox
                >
                    <div style={{ padding: '8px 0' }}>
                        {data.entity.attendees.edges.map((attendee) => (
                            <Attendee
                                key={`attendee-${
                                    attendee.guid || attendee.email
                                }`}
                                entity={attendee}
                                containerGuid={data.entity.guid}
                                containerCanEdit={data.entity.canEdit}
                            />
                        ))}
                    </div>
                </FetchMore>
            )}
        </>
    )
}

const GET_ATTENDEES_LIST = gql`
    query AttendeesList(
        $guid: String!
        $limit: Int!
        $state: String
        $query: String
    ) {
        entity(guid: $guid) {
            guid
            ... on Event {
                canEdit
                attendees(
                    limit: $limit
                    state: $state
                    query: $query
                    orderBy: timeUpdated
                    orderDirection: desc
                ) {
                    total
                    edges {
                        guid
                        icon
                        url
                        name
                        email
                        state
                        isAttendingSubEvents
                    }
                }
            }
        }
    }
`

export default AttendeesList
