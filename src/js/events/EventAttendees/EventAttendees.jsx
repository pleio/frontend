import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import filterViewerFromAttendees from 'helpers/filterViewerFromAttendees'
import styled from 'styled-components'

import HideVisually from 'js/components/HideVisually/HideVisually'
import People from 'js/components/People'

import AttendeesModal from './components/AttendeesModal'

const Wrapper = styled.div`
    .AttendeesPeople {
        width: 100%;
        margin: -4px 0 8px;
    }

    .AttendeesColumns {
        display: flex;
        justify-content: center;

        &:first-child {
            margin-top: -8px;
        }

        &:last-child {
            margin-bottom: -8px;
        }
    }

    .AttendeesColumn {
        width: calc(100% / 3);
        padding: 6px 0 8px 0;
        text-align: center;
        border-radius: ${(p) => p.theme.radius.normal};

        &:hover {
            background-color: ${(p) => p.theme.color.hover};
        }

        &:active {
            background-color: ${(p) => p.theme.color.active};
        }

        &:hover,
        &:active {
            .AttendeesStatus {
                color: black;
            }
        }
    }

    .AttendeesNumber {
        font-size: ${(p) => p.theme.font.size.huge};
        line-height: ${(p) => p.theme.font.lineHeight.huge};
        font-weight: ${(p) => p.theme.font.weight.bold};
    }

    .AttendeesStatus {
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        color: ${(p) => p.theme.color.text.grey};
    }
`

const EventAttendeesButton = ({ total, label, onClick }) => {
    const { t } = useTranslation()

    return (
        <button className="AttendeesColumn" type="button" onClick={onClick}>
            <div className="AttendeesNumber" aria-hidden>
                {total}
            </div>
            <div className="AttendeesStatus" aria-hidden>
                {label}
            </div>
            <HideVisually>
                {t('entity-event.view-attendees')} ({total} {label})
            </HideVisually>
        </button>
    )
}

const EventAttendees = ({ entity, viewer }) => {
    const {
        attendees,
        isAttending,
        rsvp,
        attendEventWithoutAccount,
        enableMaybeAttendEvent,
        attendEventOnline,
    } = entity
    const { totalAccept, totalMaybe, totalOnline, totalReject } = attendees
    const { loggedIn } = viewer

    const { t } = useTranslation()

    const [attendeesModal, setAttendeesModal] = useState(false)

    if (!loggedIn || (loggedIn && !rsvp && !attendEventWithoutAccount)) {
        return null
    }

    return (
        <Wrapper>
            <People
                tabIndex="-1"
                as="button"
                type="button"
                onClick={() => setAttendeesModal('going')}
                showCheck={isAttending === 'accept'}
                checkLabel={t('entity-event.you-are-going')}
                users={
                    isAttending === 'accept'
                        ? filterViewerFromAttendees(
                              attendees.edges,
                              viewer.user.guid,
                          )
                        : attendees.edges
                }
                total={totalAccept}
                size="large"
                className="AttendeesPeople"
                aria-hidden
            />

            <div className="AttendeesColumns">
                <EventAttendeesButton
                    total={totalAccept}
                    label={t('entity-event.going')}
                    onClick={() => setAttendeesModal('going')}
                />

                {attendEventOnline && (
                    <EventAttendeesButton
                        total={totalOnline}
                        label={t('entity-event.online')}
                        onClick={() => setAttendeesModal('online')}
                    />
                )}

                {enableMaybeAttendEvent && (
                    <EventAttendeesButton
                        total={totalMaybe}
                        label={t('entity-event.maybe')}
                        onClick={() => setAttendeesModal('maybe')}
                    />
                )}

                <EventAttendeesButton
                    total={totalReject}
                    label={t('entity-event.not-going')}
                    onClick={() => setAttendeesModal('not-going')}
                />
            </div>

            <AttendeesModal
                tab={attendeesModal}
                setTab={setAttendeesModal}
                entity={entity}
            />
        </Wrapper>
    )
}

export default EventAttendees
