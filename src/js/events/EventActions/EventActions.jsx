import React from 'react'
import styled from 'styled-components'

import Flexer from 'js/components/Flexer/Flexer'

import AddToCalendar from './components/AddToCalendar'
import ViewQrCode from './components/ViewQrCode'
import ViewTicketLink from './components/ViewTicketLink'

const Wrapper = styled(Flexer)`
    display: flex;

    &:not(:first-child) {
        border-top: 1px solid ${(p) => p.theme.color.grey[30]};
    }

    .EventAction {
        flex: 1;
        padding: 4px;

        .EventActionButton {
            width: 100%;
            height: 64px;
            background-color: transparent;

            svg {
                height: 24px;
                margin: 4px 0;
            }
        }
    }
`

const EventActions = ({ site, entity }) => {
    const { ticketLink, qrAccess, isAttending } = entity

    return (
        <Wrapper gutter="none" divider="small">
            {ticketLink && <ViewTicketLink entity={entity} />}
            {qrAccess && isAttending === 'accept' && (
                <ViewQrCode entity={entity} />
            )}
            <AddToCalendar site={site} entity={entity} />
        </Wrapper>
    )
}

export default EventActions
