import React from 'react'
import { useTranslation } from 'react-i18next'

import IconButton from 'js/components/IconButton/IconButton'

import QrIcon from 'icons/qr.svg'

const ViewQrCode = ({ entity }) => {
    const { t } = useTranslation()

    return (
        <div className="EventAction">
            <IconButton
                variant="primary"
                radiusStyle="rounded"
                className="EventActionButton"
                as="a"
                href={`/qr/access/${entity.guid}`}
            >
                <QrIcon />
                {t('entity-event.qr-code')}
            </IconButton>
        </div>
    )
}

export default ViewQrCode
