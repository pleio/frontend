import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { isExternalUrl } from 'helpers'

import IconButton from 'js/components/IconButton/IconButton'

import TicketIcon from 'icons/ticket.svg'

const ViewTicketLink = ({ entity }) => {
    const { t } = useTranslation()

    const { ticketLink } = entity

    const isExternal = isExternalUrl(ticketLink)

    return (
        <div className="EventAction">
            <IconButton
                variant="primary"
                className="EventActionButton"
                radiusStyle="rounded"
                as={isExternal ? 'a' : Link}
                href={isExternal ? ticketLink : null}
                to={!isExternal ? ticketLink : null}
                target={isExternal ? '_blank' : null}
                rel={isExternal ? 'noopener noreferrer' : null}
                aria-label={`${t('entity-event.ticket')} ${
                    isExternal ? t('global.opens-in-new-window') : ''
                }`}
            >
                <TicketIcon />
                {t('entity-event.ticket')}
            </IconButton>
        </div>
    )
}

export default ViewTicketLink
