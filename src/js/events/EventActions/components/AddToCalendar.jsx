import React from 'react'
import { useTranslation } from 'react-i18next'

import DropdownButton from 'js/components/DropdownButton/DropdownButton'
import IconButton from 'js/components/IconButton/IconButton'
import { parseToCalendarDate } from 'js/lib/helpers/date/parseDate'

import CalendarIcon from 'icons/calendar-day.svg'

const AddToCalendar = ({ entity, site }) => {
    const { t } = useTranslation()

    const getRandomKey = () => {
        const n = Math.floor(Math.random() * 999999999999).toString()
        return new Date().getTime().toString() + '_' + n
    }

    const formatTime = (date) => {
        if (!date) return null
        const formattedDate = parseToCalendarDate(date)
        return formattedDate.replace('+00:00', 'Z')
    }

    const { integratedVideocallEnabled } = site
    const {
        title,
        excerpt,
        startDate,
        endDate,
        location,
        locationAddress,
        locationLink,
        videoCallUrl,
    } = entity

    const event = {
        startDate: formatTime(startDate),
        endDate: endDate ? formatTime(endDate) : '',
        title,
        excerpt: (excerpt || '').replace('<p>', '').replace('</p>', ''),
        location: locationAddress || locationLink || location || '',
        detailsLocation:
            location && locationLink
                ? `${location}: ${locationLink}`
                : location || locationLink || '',
        videoCallUrl: integratedVideocallEnabled && videoCallUrl,
    }

    const getIcal = () => {
        let calendarUrl = '/exporting/calendar/'
        calendarUrl += '?startDate=' + event.startDate
        calendarUrl += '&endDate=' + event.endDate
        calendarUrl += '&location=' + encodeURIComponent(event.location)
        calendarUrl += '&text=' + encodeURIComponent(event.title)
        calendarUrl +=
            '&details=' +
            encodeURIComponent(
                event.excerpt +
                    '\n\n' +
                    event.detailsLocation +
                    '\n\n' +
                    event.videoCallUrl,
            )
        calendarUrl += '&url=' + encodeURIComponent(window.location.href)
        return calendarUrl
    }

    const getGoogle = () => {
        let calendarUrl = 'https://calendar.google.com/calendar/render'
        calendarUrl += '?action=TEMPLATE'
        calendarUrl += '&dates=' + event.startDate
        calendarUrl += '/' + event.endDate
        calendarUrl += '&location=' + encodeURIComponent(event.location)
        calendarUrl += '&text=' + encodeURIComponent(event.title)
        calendarUrl +=
            '&details=' +
            encodeURIComponent(
                event.excerpt +
                    '<br/><br/>' +
                    window.location.href +
                    '<br/><br/>' +
                    event.detailsLocation +
                    '<br/><br/>' +
                    event.videoCallUrl,
            )

        return calendarUrl
    }

    const getOutlook = () => {
        let calendarUrl = 'https://outlook.live.com/owa/?rru=addevent'
        calendarUrl += '&startdt=' + event.startDate
        calendarUrl += '&enddt=' + event.endDate
        calendarUrl += '&subject=' + encodeURIComponent(event.title)
        calendarUrl += '&location=' + encodeURIComponent(event.location)
        calendarUrl +=
            '&body=' +
            encodeURIComponent(
                event.excerpt +
                    '<br/><br/>' +
                    window.location.href +
                    '<br/><br/>' +
                    event.detailsLocation +
                    '<br/><br/>' +
                    event.videoCallUrl,
            )
        calendarUrl += '&allday=false'
        calendarUrl += '&uid=' + getRandomKey()
        calendarUrl += '&path=/calendar/view/Month'

        return calendarUrl
    }

    const options = [
        {
            href: getIcal(),
            name: t('entity-event.agenda'),
            download: true,
        },
        { href: getGoogle(), name: 'Google.com', target: '_blank' },
        { href: getOutlook(), name: 'Outlook.com', target: '_blank' },
    ]

    return (
        <DropdownButton
            options={options}
            className="EventAction"
            placement="bottom"
        >
            <IconButton
                variant="primary"
                radiusStyle="rounded"
                className="EventActionButton"
            >
                <CalendarIcon />
                {t('entity-event.save-to-cal')}
            </IconButton>
        </DropdownButton>
    )
}

export default AddToCalendar
