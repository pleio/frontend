import React from 'react'
import { JitsiMeeting } from '@jitsi/react-sdk'

import { useSiteStore } from 'js/lib/stores'

const JitsiProvider = ({
    jwt,
    domain,
    roomName,
    user,
    showConfigScreen = false,
}) => {
    const { site } = useSiteStore()

    return (
        <div
            style={{
                overflow: 'hidden',
            }}
        >
            <JitsiMeeting
                jwt={jwt}
                domain={domain}
                roomName={roomName}
                configOverwrite={{
                    defaultLanguage: user?.language || site?.language || 'nl',
                    prejoinConfig: {
                        enabled: showConfigScreen,
                    },
                    enableClosePage: true, // Show the close button in the bottom right corner
                    startWithVideoMuted: !showConfigScreen,
                }}
                interfaceConfigOverwrite={{
                    DISABLE_JOIN_LEAVE_NOTIFICATIONS: true,
                }}
                userInfo={{
                    displayName: user?.name || '',
                    email: user?.email || '',
                }}
                getIFrameRef={(iframeRef) => {
                    iframeRef.style.height = '100vh'
                }}
                onReadyToClose={() => {
                    window.location = '/' // Redirect to the homepage when the meeting is closed
                }}
            />
        </div>
    )
}

export default JitsiProvider
