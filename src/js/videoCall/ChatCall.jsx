import React from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import chatFragment from 'js/chat/fragments/chat'
import Document from 'js/components/Document'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import NotFound from 'js/core/NotFound'

import JitsiProvider from './components/JitsiProvider'

const ChatCall = () => {
    const { t } = useTranslation()
    const { guid } = useParams()

    const { loading, data } = useQuery(GET_VIDEO_CALL, {
        variables: {
            chatGuid: guid,
        },
    })

    const { site, viewer, chat } = data || {}

    if (loading) return <LoadingSpinner />

    // If video calling is disabled or the video call does not exist
    if (!site?.integratedVideocallEnabled || !viewer.loggedIn || !chat) {
        return <NotFound />
    }

    const { videoCallToken } = chat

    return (
        <>
            <Document title={t('global.video-call')} />
            <JitsiProvider
                jwt={videoCallToken}
                domain={site?.integratedTokenOnlyVideocallUrl}
                roomName={guid}
                user={viewer?.user}
            />
        </>
    )
}

const GET_VIDEO_CALL = gql`
    query ChatCall($chatGuid: String) {
        site {
            guid
            integratedVideocallEnabled
            integratedTokenOnlyVideocallUrl
        }
        viewer {
            guid
            loggedIn
            user {
                guid
                name
                email
                language
            }
        }
        chat(chatGuid: $chatGuid) {
            ${chatFragment}
        }
    }
`

export default ChatCall
