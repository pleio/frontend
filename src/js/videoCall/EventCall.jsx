import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useLocation, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import Document from 'js/components/Document'
import Flexer from 'js/components/Flexer/Flexer'
import { H2 } from 'js/components/Heading'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Text from 'js/components/Text/Text'
import NotFound from 'js/core/NotFound'

import PleioIcon from 'icons/pleio-logo.svg'

import JitsiProvider from './components/JitsiProvider'

const Wrapper = styled.div`
    z-index: 1;
    flex-grow: 1;
    background-color: white;
    display: flex;
    flex-direction: column;
    padding: 40px 40px 120px;

    .VideoCallPleioLogo {
        color: ${(p) => p.theme.color.pleio};
    }

    .VideoCallPortal {
        margin-top: 24px;
        flex-grow: 1;
        display: flex;
        align-items: center;
        justify-content: center;
    }
`

const EventCall = () => {
    const location = useLocation()
    const { t } = useTranslation()
    const { guid } = useParams()

    const [continueWithoutAccount, setContinueWithoutAccount] = useState(false)

    const { loading, data } = useQuery(GET_VIDEO_CALL, {
        variables: {
            guid,
        },
    })

    // If video calling is disabled or the video call does not exist
    if (
        !loading &&
        (!data?.site?.integratedVideocallEnabled || !data?.videoCall)
    )
        return <NotFound />

    // Already logged in
    const loggedIn = !loading && data?.viewer?.loggedIn

    return (
        <>
            <Document title={t('global.video-call')} />
            {loggedIn || continueWithoutAccount ? (
                <JitsiProvider
                    jwt={data?.videoCall?.jwtToken}
                    domain={data?.site?.integratedVideocallUrl}
                    roomName={data?.videoCall?.guid}
                    user={data?.viewer?.user}
                    showConfigScreen
                />
            ) : (
                <Wrapper>
                    <PleioIcon className="VideoCallPleioLogo" />
                    <div className="VideoCallPortal">
                        {loading ? (
                            <LoadingSpinner />
                        ) : (
                            <div>
                                <H2 as="h1">{t('action.join-call')}</H2>
                                <Text variant="grey">
                                    {data?.videoCall?.title}
                                </Text>
                                <Flexer mt justifyContent="flex-start" wrap>
                                    <Button
                                        size="large"
                                        variant="primary"
                                        as={Link}
                                        to="/login"
                                        state={{ next: location.pathname }}
                                    >
                                        {t('action.login')}
                                    </Button>
                                    <Text variant="grey">{t('global.or')}</Text>
                                    <Button
                                        size="large"
                                        variant="tertiary"
                                        onClick={() =>
                                            setContinueWithoutAccount(true)
                                        }
                                    >
                                        {t('action.continue-without-account')}
                                    </Button>
                                </Flexer>
                            </div>
                        )}
                    </div>
                </Wrapper>
            )}
        </>
    )
}

const GET_VIDEO_CALL = gql`
    query EventCall($guid: String!) {
        site {
            guid
            integratedVideocallEnabled
            integratedVideocallUrl
        }
        viewer {
            guid
            loggedIn
            user {
                guid
                name
                email
            }
        }
        videoCall(guid: $guid) {
            guid
            title
            isModerator
            jwtToken
        }
    }
`

export default EventCall
