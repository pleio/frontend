import React, { useRef, useState } from 'react'

import Loader from 'js/components/Form/Loader'
import Tiptap from 'js/components/Tiptap/TiptapEditor'

const FormTiptap = ({ name, onSubmit, ...rest }) => {
    const [loading, setLoading] = useState(false)
    const [saved, setSaved] = useState(false)
    let timer = null

    const refTiptap = useRef()

    const showLoader = () => {
        setSaved(false)
        setLoading(true)
    }

    const hideLoader = () => setLoading(false)

    const showSaved = () => {
        setSaved(true)
        setLoading(false)

        clearTimeout(timer)
        timer = setTimeout(() => setSaved(false), 2000)
    }

    const handleBlur = () => {
        const value = refTiptap.current.getValue()
        showLoader()
        onSubmit(name, value, showSaved, hideLoader)
    }

    return (
        <div style={{ position: 'relative' }}>
            <Tiptap {...rest} ref={refTiptap} onBlur={handleBlur} />
            <Loader
                position="bottom-right"
                isLoading={loading}
                isSaved={saved}
            />
        </div>
    )
}

export default FormTiptap
