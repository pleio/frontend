import React, { useState } from 'react'

import FormikError from 'js/components/Form/FormikError'
import Loader from 'js/components/Form/Loader'
import Textarea from 'js/components/Textarea/Textarea'

const FormikTextarea = ({ field, form, onSubmit, ...props }) => {
    const [loading, setLoading] = useState(false)
    const [saved, setSaved] = useState(false)

    if (!field || !form) return null

    let timer = null

    const showLoader = () => {
        setSaved(false)
        setLoading(true)
    }

    const hideLoader = () => {
        setLoading(false)
    }

    const showSaved = () => {
        setSaved(true)
        setLoading(false)

        clearTimeout(timer)
        timer = setTimeout(() => setSaved(false), 2000)
    }

    const handleBlur = (evt) => {
        field.onBlur(evt)

        // Check if field produces errors
        if (
            !form.errors[field.name] &&
            form.initialValues[field.name] !== field.value &&
            onSubmit
        ) {
            showLoader()
            onSubmit(field.name, field.value, showSaved, hideLoader)
        }
    }

    return (
        <div style={{ position: 'relative' }}>
            <Textarea {...field} {...props} onBlur={handleBlur} />
            <FormikError form={form} field={field} />
            <Loader position="top-right" isLoading={loading} isSaved={saved} />
        </div>
    )
}

export default FormikTextarea
