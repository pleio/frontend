import React from 'react'
import { useField } from 'formik'

import DropdownButton from 'js/components/DropdownButton/DropdownButton'

const FormikDropdown = ({ name, options, children, ...props }) => {
    const [field, meta, helpers] = useField(name)

    const { value } = meta
    const { setValue } = helpers

    const handleChange = (value) => {
        setValue(value)
    }

    const newOptions = options.map((option) => {
        const handleClick = () => setValue(option.value)

        return {
            name: option.label,
            onClick: handleClick,
            active: option.value === value,
        }
    })

    return (
        <DropdownButton
            {...field}
            {...props}
            options={newOptions}
            error={meta.error && meta.touched}
            onChange={handleChange}
            showArrow
        >
            {children}
        </DropdownButton>
    )
}

export default FormikDropdown
