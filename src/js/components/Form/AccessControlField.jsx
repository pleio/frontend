import React from 'react'
import { useTranslation } from 'react-i18next'
import { partitionArray } from 'helpers'
import { produce } from 'immer'
import orderBy from 'lodash.orderby'
import styled from 'styled-components'

import Avatar from 'js/components/Avatar/Avatar'
import DropdownButton from 'js/components/DropdownButton/DropdownButton'
import UserField from 'js/components/EntityActions/Advanced/components/UserField.jsx'
import { H5 } from 'js/components/Heading'
import Spacer from 'js/components/Spacer/Spacer'

import PublicIcon from 'icons/earth.svg'
import PrivateIcon from 'icons/eye-off.svg'
import GroupIcon from 'icons/group.svg'
import UsersIcon from 'icons/users.svg'

const Wrapper = styled(Spacer)`
    .AccessFields {
        border: 1px solid ${(p) => p.theme.color.grey[40]};
        border-radius: ${(p) => p.theme.radius.normal};
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
    }

    .AccessField {
        box-sizing: content-box;
        width: 100%;
        min-height: 48px;
        display: flex;
        align-items: stretch;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};

        > :first-child {
            flex-grow: 1;
            display: flex;
        }

        &:not(:first-child) {
            border-top: 1px solid ${(p) => p.theme.color.grey[30]};
        }
    }

    .AccessFieldType {
        flex-grow: 1;
        display: flex;
        align-items: center;

        > .AccessFieldIcon {
            margin-left: 4px;
        }

        > svg {
            margin-left: 6px;
        }
    }

    .AccessFieldIcon {
        margin-left: -12px;
        flex-shrink: 0;
        height: 100%;
        width: 40px;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .AccessFieldTypeSubtitle {
        margin-bottom: 2px;
        font-size: ${(p) => p.theme.font.size.tiny};
        line-height: ${(p) => p.theme.font.lineHeight.tiny};
        color: ${(p) => p.theme.color.text.grey};
    }

    .AccessFieldRole {
        height: 100%;
        display: flex;
        align-items: center;
        justify-content: flex-end;
        padding: 0 16px;
        white-space: nowrap;

        > svg {
            margin-left: 6px;
        }
    }

    .AccessFieldGiveAccess {
        height: 48px;
    }
`

export const sanitizeAccessControl = (accessControl) => {
    return accessControl.map(({ type, grant, guid, user, group }) => ({
        type,
        grant,
        guid: guid || user?.guid || group?.guid,
    }))
}

const AccessControlField = ({
    name = 'access',
    group,
    accessControl,
    setAccessControl,
    ...rest
}) => {
    const { t } = useTranslation()

    const groupGuid = group?.guid

    const siteIsClosed = window.__SETTINGS__.site.isClosed

    function changeAccessGrant(i, grant) {
        setAccessControl(
            produce((newState) => {
                newState[i].grant = grant
            }),
        )
    }

    function removeAccess(i) {
        setAccessControl(
            produce((newState) => {
                newState.splice(i, 1)
            }),
        )
    }

    const [accessFields, accessUsers] = partitionArray(
        accessControl,
        (el) => el.type !== 'user',
    )

    const sortedAccessFields = orderBy(accessFields, 'type', 'asc')
    const sortedAccessUsers = orderBy(accessUsers, [(c) => c.user.name], 'asc')

    const noBaseAccess = accessFields.length === 0

    const accessIds = [
        {
            value: 'me',
            name: (
                <>
                    <div className="AccessFieldIcon">
                        <PrivateIcon />
                    </div>
                    {t('permissions.just-me')}
                </>
            ),
            onClick: () => {
                setAccessControl(
                    produce((newState) => {
                        newState.splice(0, 1)
                    }),
                )
            },
            active: noBaseAccess,
        },
        ...(groupGuid
            ? [
                  {
                      value: 'group',
                      name: (
                          <>
                              <div className="AccessFieldIcon">
                                  <GroupIcon />
                              </div>
                              {t('permissions.group-members')}
                          </>
                      ),
                      onClick: () => {
                          setAccessControl(
                              produce((newState) => {
                                  const accessProps = {
                                      type: 'group',
                                      grant: 'read',
                                      guid: groupGuid,
                                  }
                                  if (noBaseAccess) {
                                      newState.unshift(accessProps)
                                  } else {
                                      newState[0] = accessProps
                                  }
                              }),
                          )
                      },
                      active: accessControl[0]?.type === 'group',
                  },
              ]
            : []),
        ...(group?.isClosed
            ? []
            : [
                  {
                      value: 'loggedIn',
                      name: (
                          <>
                              <div className="AccessFieldIcon">
                                  <UsersIcon />
                              </div>
                              {t('permissions.logged-in-users')}
                          </>
                      ),
                      onClick: () => {
                          setAccessControl(
                              produce((newState) => {
                                  if (noBaseAccess) {
                                      newState.unshift({
                                          type: 'loggedIn',
                                          grant: 'read',
                                      })
                                  } else {
                                      newState[0].type = 'loggedIn'
                                      newState[0].grant = 'read'
                                      newState[0].guid = null
                                  }
                              }),
                          )
                      },
                      active: siteIsClosed
                          ? accessControl[0]?.type === 'public'
                          : accessControl[0]?.type === 'loggedIn', // Check if site is closed
                  },
              ]),
        ...(siteIsClosed || group?.isClosed
            ? []
            : [
                  {
                      value: 'public',
                      name: (
                          <>
                              <div className="AccessFieldIcon">
                                  <PublicIcon />
                              </div>
                              {t('permissions.public')}
                          </>
                      ),
                      onClick: () => {
                          setAccessControl(
                              produce((newState) => {
                                  if (noBaseAccess) {
                                      newState.unshift({
                                          type: 'public',
                                          grant: 'read',
                                      })
                                  } else {
                                      newState[0].type = 'public'
                                      newState[0].grant = 'read'
                                      newState[0].guid = null
                                  }
                              }),
                          )
                      },
                      active: accessControl[0]?.type === 'public',
                  },
              ]),
    ]

    return (
        <Wrapper $spacing="small" {...rest}>
            <div>
                <div className="AccessFields">
                    {sortedAccessFields.length > 0 ? (
                        sortedAccessFields.map((item, i) => {
                            const { type, grant, guid } = item
                            const typeName = accessIds.find(
                                (el) =>
                                    el.value ===
                                    (type === 'public'
                                        ? siteIsClosed
                                            ? 'loggedIn'
                                            : 'public'
                                        : type), // Check if site is closed
                            )?.name

                            if (!typeName) {
                                return console.error(
                                    t('error.access-type-name-not-found'),
                                )
                            }

                            const id = `${name}-${type}-${guid || ''}`

                            return (
                                <div key={id} className="AccessField">
                                    <DropdownButton
                                        name={`${id}-role`}
                                        options={accessIds}
                                        showArrow
                                        placement="bottom-start"
                                    >
                                        <button
                                            type="button"
                                            className="AccessFieldType"
                                        >
                                            {typeName}
                                        </button>
                                    </DropdownButton>

                                    {type !== 'me' && (
                                        <DropdownButton
                                            name={`${id}-role`}
                                            options={[
                                                [
                                                    {
                                                        onClick: () =>
                                                            changeAccessGrant(
                                                                i,
                                                                'read',
                                                            ),
                                                        name: t(
                                                            'permissions.grant-read',
                                                        ),
                                                        active:
                                                            grant === 'read',
                                                    },
                                                    ...(!(
                                                        i === 0 &&
                                                        type === 'public'
                                                    )
                                                        ? [
                                                              {
                                                                  onClick: () =>
                                                                      changeAccessGrant(
                                                                          i,
                                                                          'readWrite',
                                                                      ),
                                                                  name: t(
                                                                      'permissions.grant-read-write',
                                                                  ),
                                                                  active:
                                                                      grant ===
                                                                      'readWrite',
                                                              },
                                                          ]
                                                        : []),
                                                ],
                                                [
                                                    {
                                                        onClick: () =>
                                                            setAccessControl(
                                                                produce(
                                                                    (
                                                                        newState,
                                                                    ) => {
                                                                        newState.splice(
                                                                            i,
                                                                            1,
                                                                        )
                                                                    },
                                                                ),
                                                            ),
                                                        name: t(
                                                            'permissions.remove-access',
                                                        ),
                                                    },
                                                ],
                                            ]}
                                            showArrow
                                        >
                                            <button
                                                type="button"
                                                className="AccessFieldRole"
                                            >
                                                {grant === 'read'
                                                    ? t(
                                                          'permissions.grant-read',
                                                      )
                                                    : grant === 'readWrite'
                                                      ? t(
                                                            'permissions.grant-read-write',
                                                        )
                                                      : null}
                                            </button>
                                        </DropdownButton>
                                    )}
                                </div>
                            )
                        })
                    ) : (
                        <div key="me" className="AccessField">
                            <DropdownButton
                                name={`me-role`}
                                options={accessIds}
                                showArrow
                                placement="bottom-start"
                            >
                                <button
                                    type="button"
                                    className="AccessFieldType"
                                >
                                    {
                                        accessIds.find(
                                            (el) => el.value === 'me',
                                        )?.name
                                    }
                                </button>
                            </DropdownButton>
                        </div>
                    )}
                </div>
            </div>
            {sortedAccessUsers.length > 0 && (
                <div>
                    <H5
                        style={{
                            marginBottom: '8px',
                        }}
                    >
                        {t('permissions.shared-with')}
                    </H5>
                    <div className="AccessFields">
                        {sortedAccessUsers.map((item) => {
                            const { type, grant, user } = item
                            const { guid } = user
                            const id = `${name}-${type}-${guid || ''}`

                            const accessControlIndex = accessControl.findIndex(
                                (el) => el.user?.guid === guid,
                            )

                            return (
                                <div key={id} className="AccessField">
                                    <div className="AccessFieldType">
                                        {type === 'user' && (
                                            <>
                                                <div className="AccessFieldIcon">
                                                    <Avatar
                                                        name={name}
                                                        image={item.user.icon}
                                                        disabled
                                                        size="small"
                                                    />
                                                </div>
                                                <div>
                                                    {item.user.name}
                                                    <div className="AccessFieldTypeSubtitle">
                                                        {item.user.email}
                                                    </div>
                                                </div>
                                            </>
                                        )}
                                    </div>

                                    <DropdownButton
                                        name={`${id}-role`}
                                        options={[
                                            [
                                                {
                                                    onClick: () =>
                                                        changeAccessGrant(
                                                            accessControlIndex,
                                                            'read',
                                                        ),
                                                    name: t(
                                                        'permissions.grant-read',
                                                    ),
                                                    active: grant === 'read',
                                                },

                                                {
                                                    onClick: () =>
                                                        changeAccessGrant(
                                                            accessControlIndex,
                                                            'readWrite',
                                                        ),
                                                    name: t(
                                                        'permissions.grant-read-write',
                                                    ),
                                                    active:
                                                        grant === 'readWrite',
                                                },
                                            ],
                                            [
                                                {
                                                    onClick: () =>
                                                        removeAccess(
                                                            accessControlIndex,
                                                        ),
                                                    name: t(
                                                        'permissions.remove-access',
                                                    ),
                                                },
                                            ],
                                        ]}
                                        showArrow
                                    >
                                        <button
                                            type="button"
                                            className="AccessFieldRole"
                                        >
                                            {grant === 'read'
                                                ? t('permissions.grant-read')
                                                : grant === 'readWrite'
                                                  ? t(
                                                        'permissions.grant-read-write',
                                                    )
                                                  : null}
                                        </button>
                                    </DropdownButton>
                                </div>
                            )
                        })}
                    </div>
                </div>
            )}

            <UserField
                name="ownerGuid"
                placeholder={t('permissions.give-access-to')}
                excludeGuids={accessControl
                    .filter((el) => el.type === 'user')
                    .map((el) => el.guid)}
                onChange={(user) => {
                    setAccessControl(
                        produce((newState) => {
                            newState.push({
                                type: 'user',
                                guid: user.guid,
                                user,
                                grant: 'read',
                            })
                        }),
                    )
                }}
                className="AccessFieldGiveAccess"
            />
        </Wrapper>
    )
}

// const GET_GROUP = gql`
//     query AccessField($groupGuid: String) {
//         group: entity(guid: $groupGuid) {
//             guid
//             ... on Group {
//                 defaultReadAccessId
//                 readAccessIds {
//                     id
//                     description
//                 }
//                 defaultWriteAccessId
//                 writeAccessIds {
//                     id
//                     description
//                 }
//             }
//         }
//     }
// `

export default AccessControlField
