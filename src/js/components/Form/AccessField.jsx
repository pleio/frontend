import React from 'react'
import { Controller } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'
import { partitionArray } from 'helpers'
import styled from 'styled-components'

import FormItem from 'js/components/Form/FormItem'
import { H4 } from 'js/components/Heading'
import RadioField from 'js/components/RadioField/RadioField'

import EarthIcon from 'icons/earth.svg'
import PrivateIcon from 'icons/eye-off.svg'
import GroupIcon from 'icons/group.svg'
import SubgroupIcon from 'icons/subgroup.svg'
import UsersIcon from 'icons/users.svg'

const groupAccessIds = `
    defaultReadAccessId
    readAccessIds {
        id
        description
    }
    defaultWriteAccessId
    writeAccessIds {
        id
        description
    }
`

const GET_GROUP_ACCESS_IDS = gql`
    query AccessField($guid: String) {
        entity(guid: $guid) {
            guid
            ... on Group {
                ${groupAccessIds}
            }
            ... on Folder {
                accessId
                rootContainer {
                    guid
                    ... on Group {
                        ${groupAccessIds}
                    }
                }
            }
        }
    }
`

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;

    .AccessFieldOption {
        position: relative;
        display: flex;

        &:first-child .AccessFieldBox {
            border-top-left-radius: ${(p) => p.theme.radius.small};
            border-top-right-radius: ${(p) => p.theme.radius.small};
        }

        &:last-child .AccessFieldBox {
            border-bottom-left-radius: ${(p) => p.theme.radius.small};
            border-bottom-right-radius: ${(p) => p.theme.radius.small};
        }

        &:not(:last-child) .AccessFieldBox {
            margin-bottom: -1px;
        }

        &[aria-current='true'] {
            z-index: 1;

            .AccessFieldBox {
                border-color: ${(p) => p.theme.color.secondary.main};
            }

            .AccessFieldIcon,
            .AccessFieldName {
                color: ${(p) => p.theme.color.secondary.main};
            }
        }
    }

    .SelectContent {
        padding-top: 0;
        padding-bottom: 0;
        padding-left: 0;
    }

    .AccessFieldBox {
        flex-grow: 1;
        margin-left: 16px;
        display: flex;
        align-items: center;
        border: 1px solid ${(p) => p.theme.color.grey[40]};
    }

    .AccessFieldIcon {
        flex-shrink: 0;
        display: flex;
        align-items: center;
        justify-content: center;
        width: 48px;
        height: 48px;
    }

    .AccessFieldName {
        padding: 5px 0;
        flex-grow: 1;
        align-self: stretch;
        display: flex;
        align-items: center;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        color: ${(p) => p.theme.color.text.black};
    }

    .AccessFieldSubgroupsButtonContainer {
        position: relative;
        width: 100%;
        height: 100%;
        display: flex;
    }

    .AccessFieldSubgroupsButton {
        height: 100%;
        width: 100%;
        display: flex;
        align-items: center;
        padding-right: 16px;
        right: -1px;
        position: absolute;
        inset: 0px;
        width: 100%;
        height: 100%;

        pointer-events: auto;

        .AccessFieldSubgroupsButtonText {
            padding-right: 4px;
            margin-right: auto;
        }

        &[aria-current='false'] .AccessFieldSubgroupsButtonText {
            color: ${(p) => p.theme.color.text.grey};
        }
    }
`

const AccessField = ({ containerGuid, name, label, control, ...rest }) => {
    const { t } = useTranslation()
    const site = window.__SETTINGS__.site

    // Get group access options and default values
    // Get container folder access values
    const { data, loading } = useQuery(GET_GROUP_ACCESS_IDS, {
        fetchPolicy: 'no-cache',
        variables: {
            guid: containerGuid,
        },
        skip: !containerGuid,
    })

    if (!site || loading) return null

    const { entity } = data || {}
    const { __typename, rootContainer } = entity || {}

    const isRead = name === 'accessId'
    const accessIdsName = isRead ? 'readAccessIds' : 'writeAccessIds'

    const group =
        __typename === 'Group' // If container is the group (root)
            ? entity
            : rootContainer?.__typename === 'Group' // If container is a folder
              ? rootContainer
              : null // Not in group

    const accessIds = group ? group[accessIdsName] : site[accessIdsName]

    if (!accessIds) return null

    const defaultAccessIdName = isRead
        ? 'defaultReadAccessId'
        : 'defaultWriteAccessId'
    const accessIdName = isRead ? 'accessId' : 'writeAccessId'

    // Take over parent folder read access value
    const folder = __typename === 'Folder' && isRead ? entity : null

    const defaultValue = folder
        ? folder[accessIdName] // Folder read access value
        : group
          ? group[defaultAccessIdName] // Group default access value
          : site[defaultAccessIdName] // Site default access value

    const [optionsWithoutSubgroups, optionsWithSubgroups] = partitionArray(
        accessIds,
        (option) => option.id < 1000,
    )

    const options = [...optionsWithoutSubgroups]
    if (optionsWithSubgroups.length > 0) {
        options.splice(1, 0, {
            id: 'subgroups',
        })
    }

    const render = ({ field }) => {
        const { value, onChange } = field

        return (
            <Wrapper {...rest}>
                {options.map((option, index) => {
                    const handleChange = () => {
                        if (option.id === 'subgroups') {
                            onChange(optionsWithSubgroups[0].id)
                        } else {
                            onChange(option.id)
                        }
                    }

                    const isCurrent =
                        option.id === 'subgroups'
                            ? value > 1000
                            : value === option.id

                    const subgroupSelectOptions = optionsWithSubgroups.map(
                        (option) => {
                            return {
                                value: option.id,
                                label: option.description,
                            }
                        },
                    )

                    const showSubgroupDropdown =
                        option.id === 'subgroups' &&
                        subgroupSelectOptions.length > 1

                    return (
                        <div
                            key={option.id}
                            className="AccessFieldOption"
                            aria-current={isCurrent}
                        >
                            <RadioField
                                name={name}
                                id={`${name}-${index}`}
                                checked={isCurrent}
                                size="small"
                                style={{
                                    position: 'static',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}
                                onChange={
                                    showSubgroupDropdown
                                        ? () =>
                                              onChange(
                                                  optionsWithSubgroups[0].id,
                                              )
                                        : handleChange
                                }
                            />
                            <div className="AccessFieldBox">
                                <div className="AccessFieldIcon">
                                    {option.id === 0 && <PrivateIcon />}
                                    {option.id === 1 && <UsersIcon />}
                                    {option.id === 2 && <EarthIcon />}
                                    {option.id === 4 && <GroupIcon />}
                                    {option.id === 'subgroups' && (
                                        <SubgroupIcon />
                                    )}
                                </div>
                                <label
                                    className="AccessFieldName"
                                    htmlFor={`${name}-${index}`}
                                >
                                    {showSubgroupDropdown ? (
                                        <FormItem
                                            type="select"
                                            name={`${name}-subgroups`}
                                            options={subgroupSelectOptions}
                                            value={value}
                                            onChange={onChange}
                                            control={control}
                                            className="AccessFieldSubgroupsButtonContainer"
                                            placeholder={t(
                                                'entity-group.subgroup',
                                            )}
                                            searchPlaceholder={t(
                                                'entity-group.search-subgroup',
                                            )}
                                            borderStyle="none"
                                            wrapSelectedValue
                                        />
                                    ) : option.id === 'subgroups' ? (
                                        subgroupSelectOptions[0].label
                                    ) : (
                                        option.description
                                    )}
                                </label>
                            </div>
                        </div>
                    )
                })}
            </Wrapper>
        )
    }

    return (
        <fieldset>
            <H4
                as="legend"
                style={{
                    marginBottom: '4px',
                }}
            >
                {label}
            </H4>
            <Controller
                name={name}
                render={render}
                control={control}
                label={label}
                defaultValue={defaultValue}
            />
        </fieldset>
    )
}

export default AccessField
