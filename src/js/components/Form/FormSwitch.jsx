import React from 'react'

import Switch from 'js/components/Switch/Switch'

const FormSwitch = ({ name, checked, onChange, ...props }) => {
    const handleChange = (evt) => {
        const { checked } = evt.target

        return new Promise((resolve, reject) => {
            onChange(name, checked, resolve, reject)
        })
    }

    return (
        <Switch
            name={name}
            checked={checked}
            onHandle={handleChange}
            {...props}
        />
    )
}

export default React.memo(
    FormSwitch,
    (prev, next) =>
        prev.value === next.value &&
        prev.disabled === next.disabled &&
        prev.onChange === next.onChange,
)
