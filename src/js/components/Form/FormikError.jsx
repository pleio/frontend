import React from 'react'
import styled from 'styled-components'

import AnimatePresence from 'js/components/AnimatePresence/AnimatePresence'

const Wrapper = styled.div`
    padding-top: 8px;
    font-size: ${(p) => p.theme.font.size.small};
    line-height: ${(p) => p.theme.font.lineHeight.small};
    color: ${(p) => p.theme.color.warn.main};
`

const FormikError = ({ form, field }) => (
    <AnimatePresence
        visible={form.touched[field.name] && !!form.errors[field.name]}
    >
        <Wrapper>{form.errors[field.name] || <br />}</Wrapper>
    </AnimatePresence>
)

export default FormikError
