import React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import Spacer from 'js/components/Spacer/Spacer'

const InsertVideo = ({
    video,
    setVideo,
    videoTitle,
    setVideoTitle,
    onClose,
}) => {
    const { t } = useTranslation()

    const { kalturaVideoEnabled, kalturaVideoPartnerId, kalturaVideoPlayerId } =
        window.__SETTINGS__
    const kalturaEnabled =
        !!kalturaVideoEnabled &&
        !!kalturaVideoPartnerId &&
        !!kalturaVideoPlayerId

    const videoValidationPattern = kalturaEnabled
        ? /((?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/(?:[^/]*)\/videos\/|album\/(?:\d+)\/video\/|video\/|)(\d+)(?:[a-zA-Z0-9_-]+)?)|(^(https?:\/\/)?((www\.)?(youtube(-nocookie)?|youtube.googleapis)\.com.*(v\/|v=|vi=|vi\/|e\/|embed\/|user\/.*\/u\/\d+\/)|youtu\.be\/)([_0-9a-z-]+)|(https?:\/\/videoleren.nvwa.nl(.*?)(?="|$)))/i
        : /((?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/(?:[^/]*)\/videos\/|album\/(?:\d+)\/video\/|video\/|)(\d+)(?:[a-zA-Z0-9_-]+)?)|(^(https?:\/\/)?((www\.)?(youtube(-nocookie)?|youtube.googleapis)\.com.*(v\/|v=|vi=|vi\/|e\/|embed\/|user\/.*\/u\/\d+\/)|youtu\.be\/)([_0-9a-z-]+))/i

    const videoHelper = kalturaEnabled
        ? t('action.insert-video-helper-kaltura')
        : t('action.insert-video-helper')

    const submit = ({ video, videoTitle }) => {
        setVideo(video)
        setVideoTitle(videoTitle)
        onClose()
    }

    const defaultValues = {
        video,
        videoTitle,
    }

    const {
        control,
        handleSubmit,
        formState: { isValid },
    } = useForm({
        mode: 'onChange',
        defaultValues,
    })

    return (
        <form
            onSubmit={(evt) => {
                evt.preventDefault()
                handleSubmit(submit)()
                // Prevent parent form from submitting
                evt.stopPropagation()
            }}
        >
            <Spacer>
                <FormItem
                    control={control}
                    type="text"
                    name="video"
                    label={t('form.url')}
                    id="video-url"
                    helper={videoHelper}
                    required
                    rules={{
                        pattern: {
                            value: videoValidationPattern,
                            message: t('error.invalid-video-url'),
                        },
                    }}
                />

                <FormItem
                    control={control}
                    type="text"
                    name="videoTitle"
                    label={t('editor.video-title')}
                    helper={t('editor.video-title-helper')}
                />

                <Flexer>
                    <Button
                        size="normal"
                        variant="tertiary"
                        type="button"
                        onClick={onClose}
                    >
                        {t('action.cancel')}
                    </Button>
                    <Button
                        size="normal"
                        variant="primary"
                        type="submit"
                        disabled={!isValid}
                    >
                        {video ? t('action.save') : t('action.insert')}
                    </Button>
                </Flexer>
            </Spacer>
        </form>
    )
}

export default InsertVideo
