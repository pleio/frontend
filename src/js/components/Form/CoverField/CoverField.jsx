import React, {
    forwardRef,
    useEffect,
    useImperativeHandle,
    useState,
} from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useLazyQuery } from '@apollo/client'
import { media } from 'helpers'
import styled, { css } from 'styled-components'

import Button from 'js/components/Button/Button'
import CoverImage from 'js/components/CoverImage'
import InsertImageForm from 'js/components/EntityActions/components/InsertImageForm'
import Flexer from 'js/components/Flexer/Flexer'
import { H4 } from 'js/components/Heading'
import HideVisually from 'js/components/HideVisually/HideVisually'
import Modal from 'js/components/Modal/Modal'
import Text from 'js/components/Text/Text'

import InsertVideo from './InsertVideo'

const Wrapper = styled.div`
    ${media.tabletDown`
        margin-left: -20px;
        margin-right: -20px;

        .FeaturedFieldSettings {
            padding-left: 20px;
            padding-right: 20px;
        }
    `}

    ${(p) =>
        p.$showCover &&
        css`
            margin-top: -24px;

            .FeaturedFieldSettings {
                padding-top: 16px;
            }
        `};

    .FeaturedFieldImage {
        background-color: ${(p) => p.theme.color.grey[10]};
        min-height: 100px;
    }
`

const CoverField = forwardRef(({ featured, size, disableCaption }, ref) => {
    // Image
    const [image, setImage] = useState(featured?.image || null)
    const [alt, setAlt] = useState(featured?.alt || '')
    const [caption, setCaption] = useState(featured?.caption || '')
    const [originY, setOriginY] = useState()
    const [positionY, setPositionY] = useState(featured?.positionY || 50)
    const [newY, setNewY] = useState(50)
    const [showImageModal, setShowImageModal] = useState(false)
    const toggleShowImageModal = () => {
        setShowImageModal(!showImageModal)
    }

    // Video
    const [video, setVideo] = useState(featured?.video || null)
    const [videoThumbnailUrl, setVideoThumbnailUrl] = useState(
        featured?.videoThumbnailUrl || null,
    )
    const [videoTitle, setVideoTitle] = useState(featured?.videoTitle || '')
    const [showVideoModal, setShowVideoModal] = useState(false)
    const toggleShowVideoModal = () => {
        setShowVideoModal(!showVideoModal)
    }

    const handleSetVideo = (val) => {
        setVideo(val)
        getVideoThumbnail({
            variables: {
                input: {
                    url: val,
                },
            },
        })
    }

    const [getVideoThumbnail, { loading, data }] =
        useLazyQuery(GET_VIDEO_THUMBNAIL)
    useEffect(() => {
        if (!loading && data) {
            const thumbnail = data?.fetchVideoMetadata?.metadata?.thumbnailUrl
            setVideoThumbnailUrl(thumbnail)
        }
    }, [loading, data])

    const [isDragging, setIsDragging] = useState(false)

    useEffect(() => {
        window.addEventListener('mouseup', onMouseUp)
        window.addEventListener('mousemove', onMouseMove)

        return () => {
            window.removeEventListener('mouseup', onMouseUp)
            window.removeEventListener('mousemove', onMouseMove)
        }
    })

    const handleClickRemoveImage = () => {
        setImage(null)
        setAlt('')
        setCaption('')
    }

    const handleClickRemoveVideo = () => {
        setVideo(null)
        setVideoThumbnailUrl(null)
        setVideoTitle('')
    }

    const onMouseDown = (evt) => {
        !isDragging && setIsDragging(true)
        setOriginY(evt.clientY)
    }

    const onMouseMove = (evt) => {
        if (!isDragging) return

        setNewY(Math.min(Math.max(0, newY - (evt.clientY - originY) / 5), 100))
        setOriginY(evt.clientY)
        setPositionY(Math.round(newY))
    }

    const onMouseUp = () => {
        isDragging && setIsDragging(false)
    }

    const onDoubleClick = () => {
        setPositionY(50)
    }

    const handleKeyDown = (evt) => {
        switch (evt.key) {
            case 'ArrowUp':
            case 'w':
                evt.preventDefault()
                if (positionY > 50) {
                    setPositionY(50)
                } else {
                    setPositionY(0)
                }

                break

            case 'ArrowDown':
            case 's':
                evt.preventDefault()
                if (positionY < 50) {
                    setPositionY(50)
                } else {
                    setPositionY(100)
                }
                break
        }
    }

    const getValue = () => {
        return {
            imageGuid: image?.guid || null,
            video,
            positionY,
            alt,
            caption,
            videoTitle,
        }
    }

    useImperativeHandle(ref, () => ({
        getValue() {
            return getValue()
        },
    }))

    const { t } = useTranslation()

    const showCover = !!image || !!video

    const eventListeners =
        size === 'full'
            ? {}
            : {
                  onMouseDown,
                  onDoubleClick,
                  onKeyDown: handleKeyDown,
              }

    return (
        <>
            <Wrapper $showCover={showCover}>
                <HideVisually id="featured-image-move">
                    {t('form.move-cover')}
                </HideVisually>
                <CoverImage
                    className="FeaturedFieldImage"
                    featured={{
                        image,
                        positionY,
                        video,
                        videoTitle,
                        videoThumbnailUrl,
                        alt,
                    }}
                    size={size}
                    maxHeight={size === 'full' ? '210px' : null}
                    tabIndex={0}
                    aria-describedby="featured-image-move"
                    {...eventListeners}
                />
                {image && (
                    <HideVisually role="status" aria-live="assertive">
                        {t('form.cover-position')}
                        {positionY > 75
                            ? t('form.cover-position-bottom')
                            : positionY < 25
                              ? t('form.cover-position-top')
                              : t('form.cover-position-middle')}
                    </HideVisually>
                )}
                <div className="FeaturedFieldSettings">
                    <H4>{t('form.cover')}</H4>
                    {showCover && (
                        <Text
                            variant="grey"
                            size="small"
                            style={{ marginBottom: '8px' }}
                        >
                            {t('form.cover-drag-to-reposition')}
                        </Text>
                    )}
                    <Flexer
                        justifyContent="flex-start"
                        gutter="small"
                        style={{ marginTop: '4px' }}
                    >
                        <Button
                            size="small"
                            variant="secondary"
                            onClick={toggleShowImageModal}
                        >
                            {image
                                ? t('form.edit-image')
                                : t('action.insert-image')}
                            ..
                        </Button>
                        {image && (
                            <Button
                                size="small"
                                variant="secondary"
                                onClick={handleClickRemoveImage}
                            >
                                {t('form.remove-image')}
                            </Button>
                        )}
                        {!video ? (
                            <Button
                                size="small"
                                variant="secondary"
                                onClick={toggleShowVideoModal}
                            >
                                {t('action.insert-video')}..
                            </Button>
                        ) : (
                            <>
                                <Button
                                    size="small"
                                    variant="secondary"
                                    onClick={toggleShowVideoModal}
                                >
                                    {t('form.edit-video')}..
                                </Button>
                                <Button
                                    size="small"
                                    variant="secondary"
                                    onClick={handleClickRemoveVideo}
                                >
                                    {t('form.remove-video')}
                                </Button>
                            </>
                        )}
                    </Flexer>
                </div>
            </Wrapper>

            <Modal
                isVisible={showVideoModal}
                onClose={toggleShowVideoModal}
                size="small"
                title={video ? t('form.edit-video') : t('action.insert-video')}
            >
                <InsertVideo
                    video={video}
                    setVideo={handleSetVideo}
                    videoTitle={videoTitle}
                    setVideoTitle={setVideoTitle}
                    setVideoThumbnailUrl={setVideoThumbnailUrl}
                    onClose={toggleShowVideoModal}
                />
            </Modal>

            <Modal
                isVisible={showImageModal}
                onClose={toggleShowImageModal}
                size="small"
                title={image ? t('form.edit-image') : t('action.insert-image')}
            >
                <InsertImageForm
                    image={image}
                    setImage={setImage}
                    alt={alt}
                    setAlt={setAlt}
                    caption={caption}
                    setCaption={disableCaption ? null : setCaption}
                    uploadHelper={t('form.cover-dimensions', {
                        width: 1000,
                        height: size === 'small' ? 250 : 350,
                    })}
                    onClose={toggleShowImageModal}
                />
            </Modal>
        </>
    )
})

const GET_VIDEO_THUMBNAIL = gql`
    query FetchVideoMetadata($input: FetchVideoMetadataInput!) {
        fetchVideoMetadata(input: $input) {
            metadata {
                thumbnailUrl
            }
        }
    }
`

CoverField.displayName = 'CoverField'

export default CoverField
