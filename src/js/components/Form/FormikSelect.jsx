import React from 'react'
import { useField } from 'formik'

import Select from 'js/components/Select/Select'

const FormikSelect = ({ name, options, ...props }) => {
    const [field, meta, helpers] = useField(name)

    const { setValue } = helpers

    const handleChange = (value) => {
        setValue(value)
    }

    return (
        <Select
            {...field}
            {...props}
            options={options}
            error={meta.error && meta.touched}
            onChange={handleChange}
            onBlur={null}
        />
    )
}

export default FormikSelect
