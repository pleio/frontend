import React from 'react'
import { AnimatePresence, motion } from 'framer-motion'
import styled, { css } from 'styled-components'

import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import shouldForwardProp from 'js/lib/helpers/shouldForwardProp'

import CheckIcon from 'icons/check.svg'

export interface Props {
    backgroundColor?: string
    color?: string
    position: 'top-right' | 'center-right' | 'bottom-right' | 'center'
    isLoading?: boolean
    isSaved?: boolean
}

const Wrapper = styled(motion.div).withConfig({
    shouldForwardProp: shouldForwardProp([
        'color',
        'backgroundColor',
        'position',
        'isLoading',
        'isSaved',
    ]),
})<Props>`
    position: absolute;
    width: 20px;
    height: 20px;
    background-color: ${(p) => p.backgroundColor};
    border-radius: 50%;
    overflow: hidden;
    z-index: 1;

    ${(p) =>
        p.position === 'top-right' &&
        css`
            top: 10px;
            right: 12px;
        `}

    ${(p) =>
        p.position === 'center-right' &&
        css`
            top: 0;
            bottom: 0;
            right: 12px;
            margin: auto 0;
        `}

    ${(p) =>
        p.position === 'bottom-right' &&
        css`
            bottom: 10px;
            right: 12px;
        `}

    ${(p) =>
        p.position === 'center' &&
        css`
            top: 0;
            bottom: 0;
            right: 0;
            left: 0;
            margin: auto;
        `}

    .LoaderCircle {
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        transition: opacity ${(p) => p.theme.transition.fast};
        opacity: ${(p) => (p.isLoading ? 1 : 0)};
    }

    .LoaderCheck {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        color: ${(p) => p.color || p.theme.color.secondary.main};
        display: flex;
        align-items: center;
        justify-content: center;
        transform: ${(p) => (p.isSaved ? 'scale(1)' : 'scale(0)')};
        opacity: ${(p) => (p.isSaved ? 1 : 0)};
        transition:
            transform ${(p) => p.theme.transition.materialFast},
            opacity ${(p) => p.theme.transition.fast};

        svg {
            width: 12px;
        }
    }
`

const Loader = ({
    color,
    backgroundColor,
    position,
    isLoading,
    isSaved,
}: Props) => (
    <AnimatePresence>
        {(isLoading || isSaved) && (
            <Wrapper
                initial={{ opacity: 0 }}
                animate={{ opacity: 1 }}
                exit={{ opacity: 0 }}
                transition={{ duration: 0.15 }}
                isLoading={isLoading}
                isSaved={isSaved}
                backgroundColor={backgroundColor}
                color={color}
                position={position}
            >
                <div className="LoaderCheck">
                    <CheckIcon />
                </div>
                <LoadingSpinner className="LoaderCircle" color={color} />
            </Wrapper>
        )}
    </AnimatePresence>
)

export default Loader
