import React, { useState } from 'react'

import FormikError from 'js/components/Form/FormikError'
import Loader from 'js/components/Form/Loader'
import Textfield from 'js/components/Textfield/Textfield'

const FormikTextfield = ({ field, form, style, onSubmit, ...rest }) => {
    const [loading, setLoading] = useState(false)
    const [saved, setSaved] = useState(false)

    if (!field || !form) return null

    let timer = null

    const showLoader = () => {
        setSaved(false)
        setLoading(true)
    }

    const hideLoader = () => {
        setLoading(false)
    }

    const showSaved = () => {
        setSaved(true)
        setLoading(false)

        clearTimeout(timer)
        timer = setTimeout(() => setSaved(false), 2000)
    }

    const handleBlur = (evt) => {
        field.onBlur(evt)

        // Check if field produces errors
        if (
            !form.errors[field.name] &&
            form.initialValues[field.name] !== field.value &&
            onSubmit
        ) {
            showLoader()
            onSubmit(field.name, field.value, showSaved, hideLoader)
        }
    }

    const handleKeyPress = (evt) => {
        if (evt.key === 'Enter') {
            evt.target.blur()
        }
    }

    return (
        <div style={{ ...style, ...{ position: 'relative' } }}>
            <Textfield
                {...field}
                {...rest}
                onBlur={handleBlur}
                onKeyPress={handleKeyPress}
            />
            <FormikError form={form} field={field} />
            <Loader
                position="center-right"
                backgroundColor="white"
                isLoading={loading}
                isSaved={saved}
            />
        </div>
    )
}

export default FormikTextfield
