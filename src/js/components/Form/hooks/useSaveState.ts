import { useState } from 'react'

/**
 * Helper hook to manage loading and saved states in form fields
 */

const defaultState = {
    loading: false,
    saved: false,
}

const useSaveState = (fields: string[]) => {
    const states = Object.fromEntries(
        fields.map((fieldName) => [fieldName, defaultState]),
    )

    const [successState, setSuccessState] = useState(states)

    const setLoadingState = (fieldName: string) =>
        setSuccessState({
            ...successState,
            [fieldName]: {
                loading: true,
                saved: false,
            },
        })

    const setSavedState = (fieldName: string) =>
        setSuccessState({
            ...successState,
            [fieldName]: {
                loading: false,
                saved: true,
            },
        })

    const resetState = (fieldName: string) =>
        setSuccessState({
            ...successState,
            [fieldName]: defaultState,
        })

    return { successState, setLoadingState, setSavedState, resetState }
}

export default useSaveState
