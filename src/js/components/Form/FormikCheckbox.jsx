import React from 'react'

import Checkbox from 'js/components/Checkbox/Checkbox'

const FormikCheckbox = ({ field, ...props }) => {
    return <Checkbox {...field} {...props} />
}

export default FormikCheckbox
