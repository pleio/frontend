import React from 'react'
import { transparentize } from 'polished'
import styled from 'styled-components'

import LoadingSpinner from '../LoadingSpinner/LoadingSpinner'

const Wrapper = styled.div`
    position: relative;

    .FormContainerLoading {
        display: flex;
        justify-content: center;
        align-items: center;
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: ${transparentize(0.35, 'white')};
        margin: 0;
        padding: 0;
    }
`

const FormContainer = ({ isLoading, children, ...rest }) => (
    <Wrapper {...rest}>
        {children}
        {isLoading && (
            <div className="FormContainerLoading">
                <LoadingSpinner />
            </div>
        )}
    </Wrapper>
)

export default FormContainer
