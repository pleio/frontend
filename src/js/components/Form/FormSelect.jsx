import React, { useState } from 'react'

import Select from 'js/components/Select/Select'

const FormSelect = ({ name, options, value, onChange, ...props }) => {
    const [loading, setLoading] = useState(false)

    const hideLoader = () => {
        setLoading(false)
    }

    const handleChange = (value) => {
        setLoading(true)
        onChange(name, value, hideLoader, hideLoader)
    }

    return (
        <Select
            name={name}
            options={options}
            value={value}
            isLoading={loading}
            onChange={handleChange}
            {...props}
        />
    )
}

export default FormSelect
