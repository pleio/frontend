/*
    This component is meant to make creating forms easy.
    Value and errors are managed by react-hook-form.

    Example use of FormItem:

    const defaultValues = {
        title: 'Default title',
    }

    const {
        control,
        handleSubmit,
        errors,
        formState: { isValid, isSubmitting },
    } = useForm({
        mode: 'onChange',
        defaultValues,
    })

    const submit = async (values) => {
        // Do stuff
    }

    return (
        <form onSubmit={handleSubmit(submit)}>
            <FormItem
                control={control}
                type="text"
                name="title"
                errors={errors}
                required
            />
            <button
                type="submit"
                disabled={!isValid || isSubmitting}
            >
                Submit
            </button>
        </form>
    )
*/

import React, { Fragment, useEffect, useRef } from 'react'
import { Controller } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useIsMount } from 'helpers'
import PropTypes from 'prop-types'
import styled, { css, useTheme } from 'styled-components'

import AttachmentUploadField from 'js/components/AttachmentUploadField/AttachmentUploadField'
import Checkbox from 'js/components/Checkbox/Checkbox'
import ColorField from 'js/components/ColorField/ColorField'
import DateField from 'js/components/DateField/DateField'
import DateTimeField from 'js/components/DateTimeField'
import { H4 } from 'js/components/Heading'
import HideVisually from 'js/components/HideVisually/HideVisually'
import RadioFields from 'js/components/RadioField/RadioFields'
import Select from 'js/components/Select/Select'
import Spacer from 'js/components/Spacer/Spacer'
import Switch from 'js/components/Switch/Switch'
import Text from 'js/components/Text/Text'
import Textarea from 'js/components/Textarea/Textarea'
import Textfield from 'js/components/Textfield/Textfield'
import Tiptap from 'js/components/Tiptap/TiptapEditor'
import Tooltip from 'js/components/Tooltip/Tooltip'
import { REGEX } from 'js/lib/constants'

import WarnIcon from 'icons/warn.svg'

const Wrapper = styled.div`
    ${(p) =>
        p.$prependField &&
        css`
            display: grid;
            grid-template-columns: auto 1fr;
            grid-template-areas:
                'a b'
                'c d'
                'e f';
            grid-column-gap: 8px;

            .FormItemTitle {
                grid-area: b;
            }

            .FormItemDescription {
                grid-area: d;
            }

            .FormItemErrorAndHelper {
                grid-area: f;
            }
        `}

    ${(p) =>
        !p.$prependField &&
        css`
            display: flex;
            flex-direction: column;

            .FormItemTitle {
                order: -2;
                margin-bottom: 8px;
            }

            .FormItemTitle + .FormItemDescription {
                margin-top: -6px;
            }

            .FormItemDescription {
                order: -1;
                margin-bottom: 12px;
            }

            .FormItemErrorAndHelper {
                margin-top: 8px;
            }
        `}

    .FormItemTitle {
        display: flex;
        align-items: center;
    }
`

const FormItem = ({
    control,
    type,
    name,
    id,
    title,
    description,
    helper,
    size,
    required,
    rules,
    error,
    errors,
    style,
    className,
    options,
    min,
    max,
    label,
    defaultValue,
    gridTemplateColumns,
    prependField,
    titleAppend,
    ...rest
}) => {
    const fieldId = id || name

    let render
    switch (type) {
        case 'text':
        case 'email':
        case 'tel':
        case 'url':
            render = ({ field }) => (
                <Textfield
                    {...field}
                    name={fieldId}
                    hasError={!!errorMessage}
                    required={required}
                    size={size}
                    type={type}
                    label={label}
                    {...rest}
                />
            )
            break

        case 'number':
            render = ({ field }) => {
                const { value, onChange, onBlur } = field

                const handleBlur = (evt) => {
                    let newValue = parseInt(evt.target.value)

                    // Correct value if outside boundries
                    if (newValue < min) {
                        newValue = min
                        onChange(min)
                    } else if (newValue > max) {
                        newValue = max
                        onChange(max)
                    }

                    onBlur(newValue)
                }

                const handleChange = (evt) => {
                    onChange(parseInt(evt.target.value))
                }

                return (
                    <Textfield
                        value={value}
                        name={fieldId}
                        hasError={!!errorMessage}
                        required={required}
                        size={size}
                        type={type}
                        min={min}
                        label={label}
                        max={max}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        {...rest}
                    />
                )
            }
            break

        case 'textarea':
            render = ({ field }) => (
                <Textarea
                    {...field}
                    name={fieldId}
                    label={label}
                    required={required}
                    size={size}
                    {...rest}
                />
            )
            break

        case 'rich':
            render = ({ field }) => {
                const { value, onChange, onBlur } = field
                const refEditor = useRef()
                const isMount = useIsMount()

                useEffect(() => {
                    if (!isMount && !value) {
                        refEditor.current.clearContent()
                    }
                }, [value])

                const validateOnChange = control?._options?.mode === 'onChange'

                const handleBlur = (evt, val) => {
                    if (!validateOnChange) {
                        onChange(val)
                    }
                    onBlur(val)
                }

                const handleChange = (val) => {
                    onChange(val)
                }

                return (
                    <Tiptap
                        ref={refEditor}
                        name={fieldId}
                        onChange={validateOnChange && handleChange}
                        onBlur={handleBlur}
                        content={value}
                        hasError={!!errorMessage}
                        size={size}
                        required={required}
                        aria-label={label || title}
                        label={label}
                        options={options}
                        {...rest}
                    />
                )
            }
            break

        case 'select':
            render = ({ field }) => (
                <Select
                    {...field}
                    name={fieldId}
                    options={options}
                    size={size}
                    label={label}
                    required={required}
                    {...rest}
                />
            )
            break

        case 'date':
            render = ({ field }) => {
                const { value, onChange } = field

                return (
                    <DateField
                        name={fieldId}
                        required={required}
                        label={label}
                        value={value}
                        onChange={onChange}
                        {...rest}
                    />
                )
            }
            break

        case 'datetime':
            render = ({ field }) => {
                const { value, onChange } = field

                return (
                    <DateTimeField
                        name={fieldId}
                        required={required}
                        label={label}
                        value={value}
                        onChange={onChange}
                        {...rest}
                    />
                )
            }
            break

        case 'checkbox':
            render = ({ field }) => {
                const { value, onChange } = field
                return (
                    <Checkbox
                        name={fieldId}
                        onChange={(evt) => onChange(evt.target.checked)}
                        checked={value}
                        label={label}
                        size={size}
                        {...rest}
                    />
                )
            }
            break

        case 'checkboxes':
            render = ({ field }) => {
                const { value, onChange } = field

                if (!options) return null

                return (
                    <div
                        style={
                            gridTemplateColumns
                                ? { display: 'grid', gridTemplateColumns }
                                : {}
                        }
                    >
                        {options.map((option) => {
                            const handleChange = (evt) => {
                                const valueCopy = value ? [...value] : []
                                if (evt.target.checked) {
                                    valueCopy.push(option.value)
                                } else {
                                    valueCopy.splice(
                                        valueCopy.indexOf(option.value),
                                        1,
                                    )
                                }
                                onChange(valueCopy)
                            }

                            return (
                                <Checkbox
                                    key={`${fieldId}-${option.value}`}
                                    name={`${fieldId}-${option.value}`}
                                    onChange={handleChange}
                                    label={option.label}
                                    checked={
                                        option.checked ||
                                        value?.some(
                                            (existingValue) =>
                                                existingValue === option.value,
                                        )
                                    }
                                    disabled={option.disabled}
                                    size={size}
                                    {...rest}
                                />
                            )
                        })}
                    </div>
                )
            }
            break

        case 'radio':
            render = ({ field }) => {
                if (!options) return null

                return (
                    <RadioFields
                        {...field}
                        name={fieldId}
                        options={options}
                        size={size}
                        required={required}
                        label={label}
                        gridTemplateColumns={gridTemplateColumns}
                        {...rest}
                    />
                )
            }
            break

        case 'switch':
            render = ({ field }) => {
                const { value, onChange } = field
                return (
                    <Switch
                        name={fieldId}
                        onChange={(evt) => onChange(evt.target.checked)}
                        checked={value}
                        label={label}
                        size={size}
                        {...rest}
                    />
                )
            }
            break

        case 'file':
            render = ({ field }) => {
                const { value, onChange } = field

                return (
                    <AttachmentUploadField
                        fileType="file"
                        name={fieldId}
                        value={value}
                        onUploaded={(attachment) => onChange(attachment.id)}
                        {...rest}
                    />
                )
            }
            break

        case 'image':
            render = ({ field }) => {
                const { value, onChange } = field

                return (
                    <AttachmentUploadField
                        fileType="image"
                        name={fieldId}
                        size={size}
                        value={value}
                        onUploaded={(attachment) => onChange(attachment.id)}
                        {...rest}
                    />
                )
            }
            break

        case 'audio':
            render = ({ field }) => {
                const { value, onChange } = field

                return (
                    <AttachmentUploadField
                        fileType="audio"
                        name={fieldId}
                        value={value}
                        onUploaded={(attachment) => onChange(attachment.id)}
                        {...rest}
                    />
                )
            }
            break

        case 'color':
            render = ({ field }) => {
                const { value, onChange } = field

                return (
                    <ColorField
                        value={value}
                        onChange={onChange}
                        name={fieldId}
                        hasError={!!errorMessage}
                        required={required}
                        size={size}
                        type={type}
                        label={label}
                        {...rest}
                    />
                )
            }
            break
    }

    const errorMessage = error || errors?.[name]?.message

    const { t } = useTranslation()
    const theme = useTheme()
    const fieldName = title || label
    const titleAsLabel = !label

    return (
        <Wrapper
            className={className}
            style={style}
            $prependField={prependField}
        >
            <Controller
                control={control}
                type={type}
                render={render}
                name={name}
                aria-describedby={`${description && `${fieldId}-description`} ${helper && `${fieldId}-helper`}`}
                rules={{
                    ...(required
                        ? {
                              required: t('error.required'),
                          }
                        : {}),
                    ...(type === 'email'
                        ? {
                              pattern: {
                                  value: REGEX.email,
                                  message: t('error.invalid-email'),
                              },
                          }
                        : {}),
                    ...(type === 'tel'
                        ? {
                              minLength: {
                                  value: 10,
                                  message: t('error.invalid-tel'),
                              },
                              maxLength: {
                                  value: 14,
                                  message: t('error.invalid-tel'),
                              },
                          }
                        : {}),
                    ...rules,
                }}
                defaultValue={defaultValue}
            />

            {title && (
                <div className="FormItemTitle">
                    <H4
                        as={titleAsLabel ? 'label' : null}
                        htmlFor={titleAsLabel ? fieldId : null}
                    >
                        {title}
                        {required && (
                            <>
                                <HideVisually>
                                    ({t('global.required')})
                                </HideVisually>
                                <Tooltip
                                    theme="warn-white"
                                    placement="right"
                                    content={t('global.required')}
                                >
                                    <span
                                        aria-hidden
                                        style={{
                                            paddingLeft: '2px',
                                            color: theme.color.warn.main,
                                        }}
                                    >
                                        *
                                    </span>
                                </Tooltip>
                            </>
                        )}
                    </H4>
                    {titleAppend}
                </div>
            )}

            {description && (
                <Text
                    id={`${fieldId}-description`}
                    size="small"
                    className="FormItemDescription"
                >
                    {description}
                </Text>
            )}

            {(errorMessage || helper) && (
                <Spacer spacing="tiny" className="FormItemErrorAndHelper">
                    {!!errorMessage && (
                        <Text size="small" variant="warn" role="alert">
                            <WarnIcon
                                style={{
                                    display: 'inline-block',
                                    verticalAlign: 'text-top',
                                    marginRight: '5px',
                                }}
                            />
                            <HideVisually>
                                {t('error.error-for-field', { fieldName })}
                            </HideVisually>
                            {t(`error.${errorMessage}`, errorMessage)}
                        </Text>
                    )}
                    {helper && (
                        <Text
                            id={`${fieldId}-helper`}
                            size="small"
                            variant="grey"
                        >
                            {helper}
                        </Text>
                    )}
                </Spacer>
            )}
        </Wrapper>
    )
}

FormItem.propTypes = {
    control: PropTypes.object.isRequired,
    type: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    id: PropTypes.string, // More specific id if there can be multiple instances of the same FormItem on screen
    size: PropTypes.string,
    rules: PropTypes.object,
    errors: PropTypes.object,
    maxChars: PropTypes.number,
    gridTemplateColumns: PropTypes.string, // Grid template to place checkboxes next to each other
    prependField: PropTypes.bool,
}

export default FormItem
