import React from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

import CheckIcon from 'icons/check-small.svg'

const Wrapper = styled.div`
    position: relative;
    display: flex;
    padding: 4px 0;

    .RadioFieldLabel {
        margin-left: 8px;
        font-family: ${(p) => p.theme.font.family};
        font-size: ${(p) => p.theme.font.size[p.$size]};
        line-height: ${(p) => p.theme.font.lineHeight[p.$size]};
        font-weight: ${(p) => p.theme.font.weight.normal};
        color: ${(p) => p.theme.color.text.black};
        user-select: none;
    }

    input {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        margin: 0;
        opacity: 0;

        &[disabled],
        &:checked {
            pointer-events: none;
        }

        &:checked + .RadioFieldLabel + .RadioFieldBox {
            background-color: ${(p) => p.theme.color.secondary.main};
            border-color: transparent;
            color: white;
        }

        &:not(:checked) {
            cursor: pointer;

            &:hover + .RadioFieldLabel + .RadioFieldBox {
                color: ${(p) => p.theme.color.icon.grey};
            }

            &:active + .RadioFieldLabel + .RadioFieldBox {
                border-color: black;
                color: black;
            }
        }

        body:not(.mouse-user) &:focus + .RadioFieldLabel + .RadioFieldBox {
            outline: ${(p) => p.theme.focusStyling};
        }
    }

    .RadioFieldBox {
        flex-shrink: 0;
        order: -1;

        border-radius: 50%;
        color: transparent;

        display: flex;
        align-items: center;
        justify-content: center;
        border: 2px solid ${(p) => p.theme.color.grey[40]};

        ${(p) =>
            p.$size === 'small' &&
            css`
                width: 16px;
                height: 16px;
                margin-top: 2px;
            `}

        ${(p) =>
            p.$size === 'normal' &&
            css`
                width: 18px;
                height: 18px;
                margin-top: 3px;
            `}
    }
`

const RadioField = ({
    name,
    id,
    value,
    size = 'normal',
    checked,
    label,
    onChange,
    disabled,
    iconElement,
    ...rest
}) => {
    return (
        <Wrapper $size={size} {...rest}>
            <input
                type="radio"
                name={name}
                id={id}
                value={value}
                checked={checked}
                disabled={disabled}
                onChange={onChange}
            />
            <label
                className="RadioFieldLabel"
                htmlFor={label ? id : null}
                style={!label ? { display: 'none' } : {}}
            >
                {label}
            </label>
            <div className="RadioFieldBox">{iconElement || <CheckIcon />}</div>
        </Wrapper>
    )
}

RadioField.propTypes = {
    size: PropTypes.oneOf(['small', 'normal']),
    onChange: PropTypes.func,
}

export default RadioField
