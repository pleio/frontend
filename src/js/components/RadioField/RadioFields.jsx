import React, { forwardRef } from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

import HideVisually from 'js/components/HideVisually/HideVisually'

import RadioField from './RadioField'

const Wrapper = styled.fieldset`
    ${(p) =>
        p.$gridTemplateColumns &&
        css`
            display: grid;
            grid-template-columns: ${p.$gridTemplateColumns};
        `};
`

const RadioFields = forwardRef(
    (
        {
            legend,
            name,
            options,
            value,
            size,
            onChange,
            gridTemplateColumns,
            ...rest
        },
        ref,
    ) => {
        return (
            <Wrapper
                $gridTemplateColumns={gridTemplateColumns}
                ref={ref}
                {...rest}
            >
                {legend && <HideVisually as="legend">{legend}</HideVisually>}
                {options.map((option, index) => (
                    <RadioField
                        key={`${name}-${index}`}
                        name={name}
                        id={`${name}-${index}`}
                        size={size}
                        value={option.value}
                        checked={value === option.value}
                        label={option.label}
                        onChange={onChange}
                    />
                ))}
            </Wrapper>
        )
    },
)

RadioFields.displayName = 'RadioFields'

RadioFields.propTypes = {
    name: PropTypes.string.isRequired,
    options: PropTypes.oneOfType([PropTypes.string, PropTypes.array])
        .isRequired,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    onChange: PropTypes.func,
}

export default RadioFields
