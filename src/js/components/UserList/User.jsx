import React from 'react'
import { useRoleLabel } from 'helpers'

import UserLink from 'js/components/UserLink'
import { showShortDate } from 'js/lib/helpers/date/showDate'

const User = ({ entity }) => (
    <tr>
        <td>
            <UserLink
                entity={entity}
                role={useRoleLabel(entity.roles)}
                style={{ height: '100%' }}
            />
        </td>

        {entity.fieldsInOverview.map(({ key, value, fieldType }) => (
            <td key={key}>
                {value && fieldType === 'dateField'
                    ? showShortDate(value)
                    : value}
            </td>
        ))}
    </tr>
)

export default User
