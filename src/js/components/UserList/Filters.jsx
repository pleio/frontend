import React from 'react'
import { useTranslation } from 'react-i18next'
import PropTypes from 'prop-types'

import FilterWrapper from 'js/components/FilterWrapper'
import Select from 'js/components/Select/Select'

const Filters = ({
    availableFilters = [],
    selectedFilters,
    onUpdateFilter,
    availableProfileSets,
    profileSelection,
    setProfileSelection,
}) => {
    const listSelectedFilters = (filterName) => {
        for (const name in selectedFilters) {
            if (name === filterName) {
                return selectedFilters[name] || []
            }
        }
        return []
    }

    const { t } = useTranslation()

    return (
        <FilterWrapper style={{ flex: 1, padding: '8px 0' }}>
            {availableFilters.map(({ name, label, keys }) => {
                const options = keys.map((option) => ({
                    label: option,
                    value: option,
                }))

                const handleChange = (val) => {
                    onUpdateFilter(val, name)
                }

                return (
                    <Select
                        key={name}
                        label={label}
                        name={name}
                        placeholder={t('filters.filter-by')}
                        options={options}
                        value={listSelectedFilters(name)}
                        onChange={handleChange}
                        isMulti
                    />
                )
            })}

            {availableProfileSets?.length > 0 && (
                <Select
                    name="profile-set"
                    label={t('filters.profile-set')}
                    placeholder={t('filters.filter-by')}
                    options={availableProfileSets.map(({ id, field }) => ({
                        label: field.name,
                        value: id,
                    }))}
                    value={profileSelection}
                    onChange={setProfileSelection}
                    isClearable
                />
            )}
        </FilterWrapper>
    )
}

Filters.propTypes = {
    onUpdateFilter: PropTypes.func.isRequired,
    availableFilters: PropTypes.array,
    selectedFilters: PropTypes.object.isRequired,
}

export default Filters
