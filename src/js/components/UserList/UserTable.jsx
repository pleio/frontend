import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'

import ErrorStyles from 'js/components/ErrorStyles'
import FetchMore from 'js/components/FetchMore'
import { Container } from 'js/components/Grid/Grid'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import NoResultsMessage from 'js/components/NoResultsMessage/NoResultsMessage'
import Table from 'js/components/Table'

import User from './User'

const UserTable = ({ q, filters, profileSelection }) => {
    const { t } = useTranslation()

    const [queryLimit, setQueryLimit] = useState(50)

    const { loading, data, error, fetchMore } = useQuery(GET_USER_LIST, {
        variables: {
            q,
            offset: 0,
            limit: queryLimit,
            filters: Object.entries(filters).map(([key, value]) => ({
                name: key,
                values: value,
            })),
            profileSetGuid: profileSelection,
        },
    })

    if (!loading && data?.users.edges.length === 0) {
        return <NoResultsMessage title={t('user.no-results')} />
    }

    const fieldsInOverview = data?.users?.fieldsInOverview || []

    if (error)
        return (
            <Container>
                <ErrorStyles>
                    {error.message?.includes('missing_required_field')
                        ? t('error.missing-required-profile-field-error')
                        : t('error.unknown_error')}
                </ErrorStyles>
            </Container>
        )

    return (
        <Container $noPadding size="large">
            <Table edgePadding>
                {fieldsInOverview?.length > 0 && (
                    <thead>
                        <tr>
                            <th>{t('global.name')}</th>
                            {fieldsInOverview.map(({ key, name }) => (
                                <th key={key}>{name}</th>
                            ))}
                        </tr>
                    </thead>
                )}
                {loading ? (
                    <tbody>
                        <tr>
                            <td colSpan={100} style={{ padding: '24px' }}>
                                <LoadingSpinner />
                            </td>
                        </tr>
                    </tbody>
                ) : (
                    <FetchMore
                        as="tbody"
                        edges={data.users.edges}
                        getMoreResults={(data) => data.users.edges}
                        fetchMore={fetchMore}
                        fetchType="scroll"
                        fetchCount={50}
                        setLimit={setQueryLimit}
                        maxLimit={data.users.total}
                    >
                        {data.users.edges.map((entity) => (
                            <User key={entity.guid} entity={entity} />
                        ))}
                    </FetchMore>
                )}
            </Table>
        </Container>
    )
}

const GET_USER_LIST = gql`
    query UserList(
        $offset: Int!
        $limit: Int!
        $q: String!
        $filters: [FilterInput]
        $profileSetGuid: String
    ) {
        users(
            offset: $offset
            limit: $limit
            q: $q
            filters: $filters
            profileSetGuid: $profileSetGuid
        ) {
            edges {
                guid
                name
                url
                icon
                fieldsInOverview {
                    key
                    name
                    value
                    fieldType
                }
                roles
            }
            total
            fieldsInOverview {
                key
                name
            }
        }
    }
`

export default UserTable
