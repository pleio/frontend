import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import AnimatePresence from 'js/components/AnimatePresence/AnimatePresence'
import { Container } from 'js/components/Grid/Grid'
import SearchBar from 'js/components/SearchBar/SearchBar'

import Filters from './Filters'
import UserTable from './UserTable'

const UserList = ({ data }) => {
    const [isFilterPanelVisible, setFilterPanelVisible] = useState(true)
    const toggleFilterArea = () => setFilterPanelVisible(!isFilterPanelVisible)

    const [profileSelection, setProfileSelection] = useState(null)

    const [searchInput, setSearchInput] = useState('')
    const onChangeSearchInput = (evt) => {
        setSearchInput(evt.target.value)
    }

    const [q, setQ] = useState('')
    const onSubmitSearchInput = (evt) => {
        evt.preventDefault()
        setQ(searchInput)
    }

    const [filterValue, setFilterValue] = useState({})
    const handleUpdateFilter = (selected, name) => {
        const newfilters = { ...filterValue }
        if (Array.isArray(selected) ? selected.length > 0 : !!selected) {
            newfilters[name] = selected
        } else {
            delete newfilters[name]
        }
        setQ(searchInput)
        setFilterValue(newfilters)
    }

    const { t } = useTranslation()

    const availableFilters = data?.filters?.users || []

    const availableProfileSets =
        data?.viewer?.profileSetManager
            ?.map((profile) => {
                // Check if the profile is active by checking if it has a value
                // (empty values will cause an API error)
                const guid = profile.field.guid
                const hasProfileValue = data.viewer.user.profile.find(
                    (userProfile) => userProfile.guid === guid,
                )?.value
                return hasProfileValue ? profile : null
            })
            .filter(Boolean) || []

    const hasAvailableFilters =
        availableFilters.length > 0 || availableProfileSets.length > 0

    return (
        <>
            <Container>
                <form method="GET" onSubmit={onSubmitSearchInput}>
                    <SearchBar
                        name="user-list-search"
                        value={searchInput}
                        label={t('global.search-user')}
                        onChange={onChangeSearchInput}
                        showFilters={isFilterPanelVisible}
                        onToggleFilters={
                            hasAvailableFilters && toggleFilterArea
                        }
                        size="large"
                        style={{
                            width: '100%',
                            maxWidth: '300px',
                            margin: '0 auto 8px',
                        }}
                    />
                </form>

                {hasAvailableFilters && (
                    <AnimatePresence visible={isFilterPanelVisible}>
                        <Filters
                            onUpdateFilter={handleUpdateFilter}
                            availableFilters={availableFilters}
                            selectedFilters={filterValue}
                            availableProfileSets={availableProfileSets}
                            profileSelection={profileSelection}
                            setProfileSelection={setProfileSelection}
                        />
                    </AnimatePresence>
                )}
            </Container>
            <UserTable
                q={q}
                filters={filterValue}
                profileSelection={profileSelection}
            />
        </>
    )
}

const Query = gql`
    query UsersFilters {
        filters {
            users {
                name
                label
                fieldType
                keys
            }
        }
        viewer {
            guid
            user {
                guid
                profile {
                    guid
                    name
                    value
                }
            }
            profileSetManager {
                id
                name
                field {
                    guid
                    name
                }
            }
        }
    }
`

export default graphql(Query)(UserList)
