import React from 'react'
import { ErrorBoundary as ErrorBoundaryPackage } from 'react-error-boundary'
import { useTranslation } from 'react-i18next'

const ErrorBoundary = ({ fallback, children }) => {
    const { t } = useTranslation()

    return (
        <ErrorBoundaryPackage
            fallback={fallback || t('error.error-boundary-fallback')}
        >
            {children}
        </ErrorBoundaryPackage>
    )
}

export default ErrorBoundary
