import React, { HTMLAttributes } from 'react'
import { Link, useLocation } from 'react-router-dom'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import Document from 'js/components/Document'
import { Container } from 'js/components/Grid/Grid'
import { H1 } from 'js/components/Heading'
import Section from 'js/components/Section/Section'

import AddIcon from 'icons/add.svg'

/*
    Props createLink and createLabel are only allowed when canCreate is true
*/

interface BaseProps extends HTMLAttributes<HTMLDivElement> {
    title: string
    children?: React.ReactNode
}

interface CanCreateTrueProps extends BaseProps {
    canCreate: true
    createLink: string
    createLabel: string
}

interface CanCreateFalseProps extends BaseProps {
    canCreate?: false
    createLink?: never
    createLabel?: never
}

export type Props = CanCreateTrueProps | CanCreateFalseProps

const TopWrapper = styled.div`
    display: flex;
    align-items: center;
    flex-wrap: wrap;
`

const PageHeader = ({
    title,
    canCreate,
    createLink,
    createLabel,
    children,
    ...rest
}: Props) => {
    const location = useLocation()
    return (
        <>
            <Document title={title} />
            <Section backgroundColor="white" divider {...rest}>
                <Container>
                    <TopWrapper>
                        <H1>{title}</H1>
                        {canCreate && (
                            <Button
                                as={Link}
                                to={createLink}
                                state={{ prevPathname: location.pathname }}
                                size="large"
                                variant="primary"
                                style={{ marginLeft: 'auto' }}
                            >
                                <AddIcon style={{ marginRight: '6px' }} />
                                {createLabel}
                            </Button>
                        )}
                    </TopWrapper>
                    {children}
                </Container>
            </Section>
        </>
    )
}

export default PageHeader
