import styled, { css } from 'styled-components'

import shouldForwardProp from 'js/lib/helpers/shouldForwardProp'

import { Props as TextfieldProps } from './Textfield'

interface Props extends TextfieldProps {
    hasValue: boolean
    isDisabled: boolean
}
const Wrapper = styled.div.withConfig({
    shouldForwardProp: shouldForwardProp([
        'hasError',
        'size',
        'borderStyle',
        'hasValue',
        'isDisabled',
    ]),
})<Props>`
    flex-shrink: 0;
    position: relative;
    flex-grow: 1;
    display: flex;
    align-items: center;
    line-height: 1;
    background-color: white;
    font-weight: ${(p) => p.theme.font.weight.normal};
    border: 1px solid ${(p) => p.theme.color.grey[40]};

    ${(p) =>
        p.hasError &&
        css`
            border-color: ${(p) => p.theme.color.warn.main};
        `};

    ${(p) =>
        p.size === 'normal' &&
        css`
            height: 40px;
            font-size: ${(p) => p.theme.font.size.small};
            line-height: ${(p) => p.theme.font.lineHeight.small};
        `};

    ${(p) =>
        p.size === 'large' &&
        css`
            height: 44px;
            font-size: ${(p) => p.theme.font.size.normal};
            line-height: ${(p) => p.theme.font.lineHeight.normal};
        `};

    ${(p) =>
        p.borderStyle === 'none' &&
        css`
            background-color: transparent;
            border-width: 0;
        `};

    ${(p) =>
        p.borderStyle === 'rounded-left' &&
        css`
            border-top-left-radius: ${(p) => p.theme.radius.small};
            border-bottom-left-radius: ${(p) => p.theme.radius.small};
        `};

    ${(p) =>
        p.borderStyle === 'rounded-right' &&
        css`
            border-top-right-radius: ${(p) => p.theme.radius.small};
            border-bottom-right-radius: ${(p) => p.theme.radius.small};
        `};

    ${(p) =>
        p.borderStyle === 'rounded' &&
        css<Props>`
            ${(p) =>
                p.size === 'normal' &&
                css`
                    border-radius: ${(p) => p.theme.radius.small};
                `};

            ${(p) =>
                p.size === 'large' &&
                css`
                    border-radius: ${(p) => p.theme.radius.normal};
                `};
        `};

    > * {
        flex-shrink: 0;
    }

    .TextFieldPrefix {
        height: 100%;
        display: flex;
        align-items: center;
        color: ${(p) => p.theme.color.text.grey};
        padding: 0 0 1px 16px;
        user-select: none;

        + .TextFieldInput {
            padding-left: 4px;
        }
    }

    .TextFieldInput {
        width: 100%;
        height: 100%;
        background: none;
        color: ${(p) => p.theme.color.text.black};
        font: inherit;
        outline-offset: ${(p) => (p.borderStyle === 'none' ? '-2px' : '-1px')};
        border: none;
        padding: 0 0 1px 16px;

        &::placeholder {
            color: ${(p) => p.theme.color.text.grey};
        }

        &:-webkit-autofill {
            box-shadow: 0 0 0 100px white inset;
            -webkit-text-fill-color: ${(p) => p.theme.color.text.black};
        }
    }

    .TextFieldLabel {
        height: 100%;
        padding-left: 14px;

        ${(p) =>
            p.hasValue &&
            css`
                height: ${(p) => p.theme.font.lineHeight.tiny};
                transform: translateY(-8px);
            `};
    }

    ${(p) =>
        p.isDisabled &&
        css`
            background-color: ${(p) => p.theme.color.grey[20]};

            .TextFieldInput {
                color: ${(p) => p.theme.color.text.grey};
            }
        `};
`

export default Wrapper
