import React, { ForwardedRef, forwardRef, useState } from 'react'
import { t } from 'i18next'

import FieldLabel from 'js/components/FieldLabel'
import IconButton from 'js/components/IconButton/IconButton'
import Text from 'js/components/Text/Text'

import CrossIcon from 'icons/cross-small.svg'

import Wrapper from './Wrapper'

export interface Props
    // the size prop is a number on native on inputs, but Textfields accepts a string, so we omit this prop
    extends Omit<React.InputHTMLAttributes<HTMLInputElement>, 'size'> {
    label?: string
    size?: 'normal' | 'large'
    helper?: any
    ElementBefore?: React.ReactElement
    ElementAfter?: React.ReactElement
    borderStyle?:
        | 'none'
        | 'square'
        | 'rounded-left'
        | 'rounded-right'
        | 'rounded'
    hasError?: boolean
    valuePrefix?: string
    onClear?: () => void
}

const Textfield = forwardRef(
    (
        {
            type = 'text',
            name,
            label,
            placeholder,
            size = 'normal',
            required,
            value,
            hasError,
            disabled,
            onChange,
            onBlur,
            onClear,
            valuePrefix,
            ElementBefore,
            ElementAfter,
            helper,
            borderStyle = 'rounded',
            min,
            max,
            'aria-label': ariaLabel,
            maxLength,
            autoFocus,
            autoComplete,
            ...rest
        }: Props,
        ref: ForwardedRef<HTMLInputElement>,
    ) => {
        const [valueState, setValueState] = useState(value || '')
        const handleChange = (evt: React.ChangeEvent<HTMLInputElement>) =>
            setValueState(evt.target.value)

        const hasValue = !!(
            placeholder ||
            valuePrefix ||
            (onChange ? value : valueState)
        )

        return (
            <>
                <Wrapper
                    size={size}
                    hasError={hasError}
                    borderStyle={borderStyle}
                    isDisabled={disabled}
                    hasValue={hasValue}
                    {...rest}
                >
                    {!!ElementBefore && ElementBefore}
                    <div
                        style={{
                            position: 'relative',
                            flexShrink: 1,
                            flexGrow: 1,
                            height: '100%',
                            display: 'flex',
                            alignItems: 'center',
                        }}
                    >
                        {valuePrefix && (
                            <label
                                htmlFor={name}
                                className="TextFieldPrefix"
                                aria-hidden
                            >
                                {valuePrefix}
                            </label>
                        )}
                        <input
                            ref={ref} // Allow parent to manually focus the input
                            className="TextFieldInput"
                            type={type}
                            id={name}
                            disabled={disabled}
                            placeholder={placeholder}
                            aria-label={ariaLabel || null}
                            value={onChange ? value || '' : valueState}
                            onChange={onChange || handleChange}
                            aria-describedby={helper ? `${name}-helper` : null}
                            required={required}
                            aria-required={required}
                            aria-invalid={hasError}
                            min={min}
                            max={max}
                            onBlur={onBlur}
                            maxLength={maxLength}
                            // eslint-disable-next-line jsx-a11y/no-autofocus
                            autoFocus={autoFocus}
                            autoComplete={autoComplete}
                        />
                        <FieldLabel
                            as="label"
                            htmlFor={name}
                            label={label}
                            placeholder={placeholder}
                            required={required}
                            active={hasValue}
                            hasError={hasError}
                            disabled={disabled}
                            size={size === 'normal' ? 'small' : 'normal'}
                            className="TextFieldLabel"
                        />
                    </div>
                    {!!ElementAfter && ElementAfter}
                    {onClear && value && (
                        <IconButton
                            variant="secondary"
                            size="normal"
                            onClick={onClear}
                            tooltip={t('action.clear')}
                            style={{ marginRight: '4px' }}
                        >
                            <CrossIcon />
                        </IconButton>
                    )}
                </Wrapper>
                {helper && (
                    <Text
                        id={`${name}-helper`}
                        size="small"
                        variant="grey"
                        style={{ marginTop: '8px' }}
                    >
                        {helper}
                    </Text>
                )}
            </>
        )
    },
)

Textfield.displayName = 'Textfield'

export default Textfield
