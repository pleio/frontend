import React from 'react'
import { useTranslation } from 'react-i18next'
import PropTypes from 'prop-types'

import DropdownButton from 'js/components/DropdownButton/DropdownButton'
import HideVisually from 'js/components/HideVisually/HideVisually'
import Tooltip from 'js/components/Tooltip/Tooltip'

import TableSortableHeaderArrow from './components/TableSortableHeaderArrow'
import TableSortableHeaderButton from './components/TableSortableHeaderButton'

const TableSortableHeader = ({
    name,
    label,
    options,
    orderBy,
    orderDirection,
    setOrderBy,
    setOrderDirection,
    align = 'left',
    defaultOrderDirection = 'asc',
    ...rest
}) => {
    const { t } = useTranslation()

    const hasOptions = options?.length > 0

    // Check if there is at least one name to sort by
    if (hasOptions ? !options[0].name : !name) return <th /> // Empty column header

    const toggleOrderBy = (newOrderBy, defaultOrderDirection) => {
        if (orderBy === newOrderBy) {
            setOrderDirection(orderDirection === 'asc' ? 'desc' : 'asc')
        } else {
            setOrderBy(newOrderBy)
            setOrderDirection(defaultOrderDirection)
        }
    }

    // Only show dropdown if there are multiple options available
    if (options?.length > 1) {
        const activeOption = options?.find((o) => o.name === orderBy)
        const isActive = !!activeOption

        return (
            <th {...rest}>
                <DropdownButton
                    options={options.map(
                        ({ label, name, defaultOrderDirection = 'asc' }) => {
                            const isActiveOption = name === orderBy
                            return {
                                name: (
                                    <>
                                        <TableSortableHeaderArrow
                                            direction={
                                                isActiveOption
                                                    ? orderDirection === 'asc'
                                                        ? 'desc'
                                                        : 'asc'
                                                    : defaultOrderDirection
                                            }
                                            style={{ marginRight: '4px' }}
                                        />
                                        {label}
                                    </>
                                ),
                                onClick: () =>
                                    toggleOrderBy(name, defaultOrderDirection),
                            }
                        },
                    )}
                    showArrow
                >
                    <TableSortableHeaderButton
                        align={align}
                        isActive={isActive}
                        hasOptions
                        title={activeOption?.label || options[0].label}
                        arrowDirection={isActive ? orderDirection : null}
                    />
                </DropdownButton>
            </th>
        )
    } else {
        // If options is used but only contains one item, use that item in normal button
        const newName = hasOptions ? options[0].name : name
        const newLabel = hasOptions ? options[0].label : label
        const newDefaultOrderDirection = hasOptions
            ? options[0].defaultOrderDirection
            : defaultOrderDirection

        const isActive = orderBy === newName

        const sortByDirection = isActive
            ? orderDirection === 'asc'
                ? 'desc'
                : 'asc'
            : newDefaultOrderDirection

        const sortByLabel =
            sortByDirection === 'asc'
                ? t('global.sort-asc')
                : t('global.sort-desc')

        const tooltipPlacement = {
            left: 'top-start',
            center: 'top',
            right: 'top-end',
        }

        return (
            <th {...rest}>
                <Tooltip
                    content={sortByLabel}
                    placement={tooltipPlacement[align]}
                    offset={[0, 0]}
                    delay
                >
                    <TableSortableHeaderButton
                        align={align}
                        isActive={isActive}
                        title={
                            <>
                                {newLabel}{' '}
                                <HideVisually>({sortByLabel})</HideVisually>
                            </>
                        }
                        arrowDirection={
                            isActive ? orderDirection : newDefaultOrderDirection
                        }
                        onClick={() =>
                            toggleOrderBy(newName, newDefaultOrderDirection)
                        }
                    />
                </Tooltip>
            </th>
        )
    }
}

TableSortableHeader.propTypes = {
    name: PropTypes.string,
    label: PropTypes.string,
    align: PropTypes.oneOf(['left', 'center', 'right']),
    defaultOrderDirection: PropTypes.string,
    orderBy: PropTypes.string.isRequired,
    orderDirection: PropTypes.string.isRequired,
    setOrderBy: PropTypes.func.isRequired,
    setOrderDirection: PropTypes.func.isRequired,
    options: PropTypes.arrayOf(
        PropTypes.shape({
            name: PropTypes.string.isRequired,
            label: PropTypes.string.isRequired,
            defaultOrderDirection: PropTypes.string,
        }),
    ),
}

export default TableSortableHeader
