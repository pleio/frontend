import React, { forwardRef } from 'react'
import styled, { css } from 'styled-components'

import TableSortableHeaderArrow from './TableSortableHeaderArrow'

const Wrapper = styled.button`
    width: 100%;
    height: 100%;
    text-align: inherit;
    display: flex;
    align-items: center;
    justify-content: inherit;

    ${(p) =>
        p.$align === 'right' &&
        css`
            justify-content: flex-end;
        `};

    ${(p) =>
        p.$align === 'center' &&
        css`
            justify-content: center;
        `};

    &:hover,
    &:active {
        color: black;
    }

    ${(p) =>
        p.$isActive &&
        css`
            color: black;
        `};

    ${(p) =>
        p.$hasOptions &&
        css`
            .TableSortableHeaderTitle {
                margin-right: 4px;
            }
        `};

    .TableSortableHeaderArrow {
        color: ${(p) => (p.$isActive ? 'black' : p.theme.color.icon.grey)};
        margin-right: 4px;
    }
`

const TableSortableHeaderButton = forwardRef(
    (
        {
            align,
            isActive,
            hasOptions,
            title,
            arrowDirection,
            children,
            ...rest
        },
        ref,
    ) => (
        <Wrapper
            ref={ref}
            type="button"
            $align={align}
            $isActive={isActive}
            $hasOptions={hasOptions}
            aria-current={isActive}
            {...rest}
        >
            <TableSortableHeaderArrow
                direction={arrowDirection}
                className="TableSortableHeaderArrow"
            />
            <span className="TableSortableHeaderTitle">{title}</span>
            {children} {/* Needed for DropdownButton chevron */}
        </Wrapper>
    ),
)

TableSortableHeaderButton.displayName = 'TableSortableHeaderButton'

export default TableSortableHeaderButton
