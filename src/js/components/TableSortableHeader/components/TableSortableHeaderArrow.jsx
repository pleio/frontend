import React from 'react'

import SortDownIcon from 'icons/arrow-down-small.svg'
import SortUpIcon from 'icons/arrow-up-small.svg'

const TableSortableHeaderArrow = ({ direction, ...rest }) => {
    if (direction === 'asc') {
        return <SortUpIcon {...rest} />
    } else if (direction === 'desc') {
        return <SortDownIcon {...rest} />
    } else {
        return null
    }
}

export default TableSortableHeaderArrow
