import React from 'react'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import Textfield from 'js/components/Textfield/Textfield'

const MultipleInputField = ({
    value,
    setValue,
    name,
    placeholder,
    disabled,
}) => {
    const addField = (e) => {
        e.preventDefault()
        setValue([...value, ''])
    }

    const deleteField = () => {
        const newValue = [...value]
        newValue.splice(value.length - 1, 1)
        setValue(newValue)
    }

    const handleChange = (i, e) => {
        const newValue = [...value]
        newValue.splice(i, 1, e.target.value)
        setValue(newValue)
    }

    const { t } = useTranslation()

    return (
        <>
            {value.map((item, i) => (
                <Textfield
                    key={i}
                    name={`${name}-${i}`}
                    label={`${placeholder} ${i + 1}`}
                    onChange={(e) => handleChange(i, e)}
                    value={item}
                    disabled={disabled}
                    style={{ marginBottom: '12px' }}
                />
            ))}
            <Flexer mt>
                <Button size="normal" variant="secondary" onClick={addField}>
                    {t('form.add-option')}
                </Button>
                {value?.length > 2 && (
                    <Button
                        size="normal"
                        variant="tertiary"
                        onClick={deleteField}
                    >
                        {t('form.remove-option')}
                    </Button>
                )}
            </Flexer>
        </>
    )
}

export default MultipleInputField
