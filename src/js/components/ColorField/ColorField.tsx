import React, { useCallback, useEffect, useRef, useState } from 'react'
import { ChromePicker } from 'react-color'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import Popover from 'js/components/Popover/Popover'
import Textfield from 'js/components/Textfield/Textfield'
import { REGEX } from 'js/lib/constants'

import TypographyIcon from 'icons/typography.svg'

import Wrapper from './Wrapper'

interface Props
    // the size prop is a number on native on inputs, but Textfields accepts a string, so we omit this prop
    extends Omit<React.InputHTMLAttributes<HTMLInputElement>, 'size'> {
    value?: string
    label?: string
    size?: 'normal' | 'large'
    hasError?: boolean
    isClearable?: boolean
    onChange?: (value: string | React.ChangeEvent<HTMLInputElement>) => void
}

const ColorField = ({
    name,
    value,
    label,
    required,
    autoComplete,
    size = 'normal',
    isClearable,
    hasError,
    disabled,
    onChange,
    ...rest
}: Props) => {
    const { t } = useTranslation()

    const refInput = useRef<HTMLInputElement>()

    const [pickerColor, setPickerColor] = useState(value)
    const handleChangePicker = (color) => {
        setPickerColor(color.hex)
    }

    const handleCompletePicker = ({ hex }) => {
        onChange(hex)
        setInputColor(hex)
    }

    const [inputColor, setInputColor] = useState(value || '')
    const handleChangeInput = (evt) => {
        setInputColor(`#${evt.target.value}`)
    }

    const handleBlurInput = () => {
        setInputColor(value)
    }

    const handlePasteInput = () => {
        setTimeout(() => {
            checkAndSetColor(`#${refInput?.current?.value}`)
        }, 0)
    }

    const checkAndSetColor = useCallback(
        (color) => {
            if (color && REGEX.colorHex.test(color)) {
                onChange(color)
            }
        },
        [onChange],
    )

    useEffect(() => {
        checkAndSetColor(inputColor)
    }, [onChange, checkAndSetColor, inputColor])

    useEffect(() => {
        setInputColor(value)
        setPickerColor(value)
    }, [value])

    return (
        <Wrapper size={size} color={value} {...rest}>
            <Flexer justifyContent="flex-start">
                <Popover
                    disabled={disabled}
                    content={
                        <ChromePicker
                            disableAlpha
                            color={pickerColor}
                            onChange={handleChangePicker}
                            onChangeComplete={handleCompletePicker}
                        />
                    }
                >
                    <button
                        type="button"
                        className="ColorFieldTile"
                        aria-label={t('admin.color-show-picker')}
                        disabled={disabled}
                    >
                        <TypographyIcon />
                    </button>
                </Popover>
                <Textfield
                    ref={refInput}
                    name={name}
                    value={inputColor.replace(/^#+/, '').toLowerCase()}
                    label={label || t('global.color-hex')}
                    maxLength={7} // Allow pasting of hex color with #
                    valuePrefix="#"
                    hasError={hasError}
                    required={required}
                    disabled={disabled}
                    autoComplete={autoComplete}
                    size={size}
                    onChange={handleChangeInput}
                    onBlur={handleBlurInput}
                    onPaste={handlePasteInput}
                    className="ColorFieldInput"
                />
                {!disabled && isClearable && value && (
                    <Button
                        size="small"
                        variant="secondary"
                        onClick={() => onChange('')}
                    >
                        {t('action.remove-color')}
                    </Button>
                )}
            </Flexer>
        </Wrapper>
    )
}

export default ColorField
