import { getReadableColor } from 'helpers/getContrast'
import { transparentize } from 'polished'
import styled, { css } from 'styled-components'

interface Props {
    color: string
    size: string
}
const Wrapper = styled.div<Props>`
    .ColorFieldTile {
        display: flex;
        align-items: center;
        justify-content: center;
        width: 40px;
        height: 40px;
        border: 1px solid ${transparentize(0.8, 'black')};
        background-color: ${(p) => p.color};
        color: ${(p) => (p.color ? getReadableColor(p.color) : 'black')};

        ${(p) =>
            p.size === 'normal' &&
            css`
                border-radius: ${(p) => p.theme.radius.small};
            `};

        ${(p) =>
            p.size === 'large' &&
            css`
                border-radius: ${(p) => p.theme.radius.normal};
            `};
    }

    .ColorFieldInput {
        width: 100px;
        flex-grow: 0;
    }
`

export default Wrapper
