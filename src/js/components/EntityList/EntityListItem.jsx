import React from 'react'
import PropTypes from 'prop-types'

import FileListItem from './components/FileListItem'
import UserListItem from './components/UserListItem'

const EntityListItem = ({ type, ...rest }) => {
    if (type === 'file') {
        return <FileListItem {...rest} />
    } else if (type === 'user') {
        return <UserListItem {...rest} />
    }
    return null
}

EntityListItem.propTypes = {
    type: PropTypes.oneOf(['file', 'user']).isRequired,
}

export default EntityListItem
