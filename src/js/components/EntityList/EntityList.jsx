import styled from 'styled-components'

export default styled.div`
    border: ${(p) => p.$showBorder && `1px solid ${p.theme.color.grey[30]}`};
    border-radius: ${(p) => p.theme.radius.large};

    > *:not(:last-child) {
        border-bottom: 1px solid ${(p) => p.theme.color.grey[20]};
    }
`
