import React from 'react'
import { humanFileSize } from 'helpers'
import styled from 'styled-components'

import FileFolderIcon from 'js/files/components/FileFolderIcon'
import getIconName from 'js/files/helpers/getIconName'
import useSubtypes from 'js/lib/hooks/useSubtypes'

const Wrapper = styled.div`
    display: flex;
    padding: 4px;

    .EntityListItemIcon {
        flex-shrink: 0;
        width: 32px;
        height: 32px;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .EntityListItemLabel {
        margin-left: 4px;
        flex-grow: 1;
        display: flex;
        align-items: center;
        padding: 4px 0;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
    }

    .EntityListItemInfo {
        flex-shrink: 0;
        padding: 8px 4px 8px 4px;
        font-size: ${(p) => p.theme.font.size.tiny};
        line-height: ${(p) => p.theme.font.lineHeight.tiny};
        color: ${(p) => p.theme.color.text.grey};
    }
`

const FileListItem = ({ entity, children, ...rest }) => {
    const { subtype, name, title, size, hasChildren, mimeType } = entity

    const { subtypes } = useSubtypes([subtype])
    const subtypeObject = subtypes[subtype]

    const iconName = getIconName(subtype, mimeType, hasChildren)

    return (
        <Wrapper {...rest}>
            {iconName && (
                <div className="EntityListItemIcon">
                    <FileFolderIcon name={iconName} />
                </div>
            )}
            <div className="EntityListItemLabel">{name || title}</div>
            {size ? (
                <div className="EntityListItemInfo">{humanFileSize(size)}</div>
            ) : subtypeObject ? (
                <div className="EntityListItemInfo">
                    {subtypeObject.contentName}
                </div>
            ) : null}
            {children}
        </Wrapper>
    )
}

export default FileListItem
