import React from 'react'
import styled from 'styled-components'

import Text from 'js/components/Text/Text'
import UserLink from 'js/components/UserLink'
import { useRoleLabel } from 'js/lib/helpers'

const Wrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 4px 4px 4px 8px;

    .UserListItemLink {
        padding: 4px 0;
    }
`

const UserListItem = ({ entity, children, ...rest }) => {
    const avatarRole = useRoleLabel(entity.roles)

    return (
        <Wrapper {...rest}>
            <UserLink
                entity={entity}
                isEmail={!entity.guid}
                role={avatarRole}
                className="UserListItemLink"
            >
                {entity.guid && (
                    <Text size="small" variant="grey">
                        {entity.email}
                    </Text>
                )}
            </UserLink>
            {children}
        </Wrapper>
    )
}

export default UserListItem
