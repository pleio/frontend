import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useQuery } from '@apollo/client'

import FetchMore from 'js/components/FetchMore'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'

import NoResultsMessage from './NoResultsMessage/NoResultsMessage'

const FetchMoreWrapper = ({
    offset,
    limit,
    childComponent: Component,
    query,
    type,
    containerGuid,
    subtype,
    tags,
}) => {
    const { t } = useTranslation()
    const [queryLimit, setQueryLimit] = useState(limit)

    const { loading, data, fetchMore } = useQuery(query, {
        variables: {
            offset,
            limit: queryLimit,
            subtype,
            type,
            containerGuid,
            tags,
        },
        options: {
            fetchPolicy: 'cache-and-network',
        },
    })

    if (loading) {
        return <LoadingSpinner />
    }

    if (data?.entities?.total === 0)
        return (
            <NoResultsMessage
                title={t('widget-feed.no-results')}
                subtitle={t('widget-feed.no-results-helper')}
            />
        )

    if (!data?.entities) return null

    return (
        <FetchMore
            edges={data.entities.edges}
            getMoreResults={(data) => data.entities.edges}
            fetchMore={fetchMore}
            fetchCount={limit}
            setLimit={setQueryLimit}
            maxLimit={data.entities.total}
            resultsMessage={t('global.result', {
                count: data.entities.total,
            })}
            fetchMoreButtonHeight={48}
        >
            {data.entities.edges.map((entity) => (
                <Component key={entity.guid} entity={entity} />
            ))}
        </FetchMore>
    )
}

export default FetchMoreWrapper
