import React, { useLayoutEffect, useRef, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { transparentize } from 'polished'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

import Button from 'js/components/Button/Button'

const Wrapper = styled.div`
    position: relative;

    ${(p) =>
        p.$hideContent &&
        !p.$showContent &&
        css`
            max-height: ${(p) => `${p.$maxHeight}px`};
            overflow: hidden;
        `}

    .ShowMore {
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
        margin: 0 auto;

        height: 80px;
        background: linear-gradient(
            0deg,
            ${transparentize(0, 'white')} 0%,
            ${transparentize(1, 'white')} 100%
        );

        display: flex;
        justify-content: center;
        align-items: flex-end;

        pointer-events: none;
    }

    .ShowMoreButton {
        pointer-events: auto;
    }
`

const ShowMore = ({ maxHeight = 300, label, children }) => {
    const { t } = useTranslation()

    const [showContent, setShowContent] = useState(0)
    const [contentHeight, setContentHeight] = useState(0)

    const refContent = useRef()

    useLayoutEffect(() => {
        const content = refContent?.current

        async function getClientHeight(content) {
            const clientHeight = await new Promise((resolve) => {
                // Prevent race condition with rendering of TiptapView
                setTimeout(() => {
                    resolve(content.clientHeight)
                }, 0)
            })
            setContentHeight(clientHeight)
        }

        if (content) {
            getClientHeight(content)
        }
    }, [children])

    const showMore = () => {
        setShowContent(true)
    }

    const hideContent = contentHeight > maxHeight

    return (
        <Wrapper
            $hideContent={hideContent}
            $showContent={showContent}
            $maxHeight={maxHeight}
        >
            <div ref={refContent}>{children}</div>
            {hideContent && !showContent && (
                <div className="ShowMore">
                    <Button
                        className="ShowMoreButton"
                        size="small"
                        variant="tertiary"
                        onClick={showMore}
                    >
                        {label || t('global.read-more')}
                    </Button>
                </div>
            )}
        </Wrapper>
    )
}

ShowMore.propTypes = {
    maxHeight: PropTypes.number,
    label: PropTypes.string,
    children: PropTypes.node.isRequired,
}

export default ShowMore
