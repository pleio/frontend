import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import { Col, Row } from 'js/components/Grid/Grid'
import Spacer from 'js/components/Spacer/Spacer'
import Text from 'js/components/Text/Text'

const Wrapper = styled.div`
    position: relative;
    width: 100%;
`

const ReplyWithoutAccount = ({ mutate, containerGuid, onClose, ...rest }) => {
    const [messageSent, setMessageSent] = useState(false)

    const submit = async ({ name, email, richDescription }) => {
        await mutate({
            variables: {
                input: {
                    containerGuid,
                    name,
                    email,
                    richDescription,
                },
            },
        })
            .then(() => {
                setMessageSent(true)
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    const { t } = useTranslation()

    const defaultValues = {
        name: '',
        email: '',
        richDescription: '',
    }

    const {
        control,
        handleSubmit,
        formState: { errors, isValid, isSubmitting },
    } = useForm({ mode: 'onChange', defaultValues })

    return (
        <Wrapper {...rest}>
            {messageSent ? (
                <>
                    <Text textAlign="center">
                        {t('comments.reply-without-account-sent')}
                    </Text>
                </>
            ) : (
                <Spacer
                    spacing="small"
                    as="form"
                    noValidate
                    onSubmit={handleSubmit(submit)}
                >
                    <Row $gutter={16}>
                        <Col mobileLandscapeUp={1 / 2}>
                            <FormItem
                                control={control}
                                type="text"
                                name="name"
                                id={`${containerGuid}-name`}
                                label={t('global.name')}
                                required
                                errors={errors}
                            />
                        </Col>
                        <Col mobileLandscapeUp={1 / 2}>
                            <FormItem
                                control={control}
                                type="email"
                                name="email"
                                id={`${containerGuid}-email`}
                                label={t('form.email-address')}
                                errors={errors}
                                required
                            />
                        </Col>
                    </Row>
                    <FormItem
                        control={control}
                        type="rich"
                        name="richDescription"
                        id={`${containerGuid}-richDescription`}
                        placeholder={t('form.message')}
                        options={{
                            textFormat: true,
                            textStyle: true,
                            textLink: true,
                            textList: true,
                        }}
                        required
                        errors={errors}
                        height="small"
                    />
                    <Flexer>
                        <Button
                            size="normal"
                            variant="tertiary"
                            type="button"
                            onClick={onClose}
                        >
                            {t('action.cancel')}
                        </Button>
                        <Button
                            size="normal"
                            variant="primary"
                            type="submit"
                            disabled={!isValid}
                            loading={isSubmitting}
                        >
                            {t('comments.reply')}
                        </Button>
                    </Flexer>
                </Spacer>
            )}
        </Wrapper>
    )
}

const Mutation = gql`
    mutation AddCommentWithoutAccount($input: addCommentWithoutAccountInput!) {
        addCommentWithoutAccount(input: $input) {
            success
        }
    }
`

export default graphql(Mutation)(ReplyWithoutAccount)
