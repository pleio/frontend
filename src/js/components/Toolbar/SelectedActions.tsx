import React from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import IconButton from 'js/components/IconButton/IconButton'

import CrossIcon from 'icons/cross.svg'

const Wrapper = styled.div`
    display: flex;
    align-items: center;

    .SelectedActionsLabel {
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        font-weight: ${(p) => p.theme.font.weight.semibold};
        color: ${(p) => p.theme.color.secondary.main};
    }
`

export interface Props {
    count: number
    onClear: () => void
}

const SelectedActions = ({ count, onClear, ...rest }: Props) => {
    const { t } = useTranslation()

    if (!onClear && !count) return null

    return (
        <Wrapper {...rest}>
            <div
                style={{
                    display: 'flex',
                    alignItems: 'center',
                }}
            >
                {onClear && (
                    <IconButton
                        size="large"
                        variant="secondary"
                        tooltip={t('action.deselect-all')}
                        placement="top"
                        onClick={onClear}
                    >
                        <CrossIcon />
                    </IconButton>
                )}
                {count && (
                    <div className="SelectedActionsLabel" role="status">
                        {t('global.selected', {
                            count,
                        })}
                    </div>
                )}
            </div>
        </Wrapper>
    )
}

export default SelectedActions
