import React from 'react'
import styled from 'styled-components'

import Sticky from 'js/components/Sticky/Sticky'

import SelectedActions, {
    Props as SelectedActionsProps,
} from './SelectedActions'

export interface Props extends SelectedActionsProps {
    children: React.ReactNode
}

const Wrapper = styled(Sticky)`
    display: flex;
    align-items: center;
    height: ${(p) => p.theme.headerBarHeight}px;
    padding: 0 8px;
    border: 1px solid ${(p) => p.theme.color.secondary.main};
    overflow: hidden;

    &[data-sticky='true'] {
        background-color: white;
        box-shadow: ${(p) => p.theme.shadow.soft.bottom};
        z-index: 9;
    }
`

const Toolbar = ({ count, children, onClear, ...rest }: Props) => {
    return (
        <Wrapper {...rest}>
            <SelectedActions count={count} onClear={onClear} />
            {children}
        </Wrapper>
    )
}

export default Toolbar
