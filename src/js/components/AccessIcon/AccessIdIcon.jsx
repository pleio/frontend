import React from 'react'
import { useTranslation } from 'react-i18next'

import IconPublic from 'icons/earth.svg'
import IconPrivate from 'icons/eye-off.svg'
import GroupIcon from 'icons/group.svg'
import SubgroupIcon from 'icons/subgroup.svg'
import IconUsers from 'icons/users.svg'

export function useAccessIdTitle(accessId) {
    const { t } = useTranslation()

    const accessTitle =
        accessId === 0
            ? 'permissions.administrators'
            : accessId === 1
              ? 'permissions.logged-in-users'
              : accessId === 4
                ? 'permissions.group-members'
                : 'permissions.public'

    return t(accessTitle)
}

export const AccessIdIcon = ({ accessId, ...rest }) => {
    const icons = {
        0: <IconPrivate {...rest} />,
        1: <IconUsers {...rest} />,
        2: <IconPublic {...rest} />,
        4: <GroupIcon {...rest} />,
        5: <SubgroupIcon {...rest} />,
    }

    return icons[accessId] || null
}
