import React from 'react'
import { useTranslation } from 'react-i18next'

import IconPublic from 'icons/earth.svg'
import IconPrivate from 'icons/eye-off.svg'
import GroupIcon from 'icons/group.svg'
import IconLogOut from 'icons/log-out.svg'
import IconUsers from 'icons/users.svg'

export function useAccessTitle(access: string) {
    const { t } = useTranslation()

    const accessTitle =
        access === 'administrator'
            ? 'permissions.administrators'
            : access === 'loggedIn'
              ? 'permissions.logged-in-users'
              : access === 'publicOnly'
                ? 'permissions.logged-out'
                : access === 'members'
                  ? 'permissions.members'
                  : 'permissions.public'

    return t(accessTitle)
}

export interface Props {
    access: 'administrator' | 'loggedIn' | 'public' | 'publicOnly' | 'members'
}

export const AccessIcon = ({ access, ...rest }: Props) => {
    const icons = {
        administrator: <IconPrivate {...rest} />,
        loggedIn: <IconUsers {...rest} />,
        public: <IconPublic {...rest} />,
        publicOnly: <IconLogOut {...rest} />,
        members: <GroupIcon {...rest} />,
    }

    return icons[access] || null
}
