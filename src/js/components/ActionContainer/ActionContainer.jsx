import React from 'react'
import { useTranslation } from 'react-i18next'
import { showDateTime } from 'helpers/date/showDate'
import { MobileLandscapeUp } from 'helpers/mediaWrapper'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import Document from 'js/components/Document'
import Flexer from 'js/components/Flexer/Flexer'
import { Container } from 'js/components/Grid/Grid'
import { H3 } from 'js/components/Heading'
import IconButton from 'js/components/IconButton/IconButton'
import Section from 'js/components/Section/Section'
import Sticky from 'js/components/Sticky/Sticky'
import Tooltip from 'js/components/Tooltip/Tooltip'
import ThemeProvider from 'js/theme/ThemeProvider'

import BackIcon from 'icons/arrow-left.svg'
import DeleteIcon from 'icons/delete.svg'

const Wrapper = styled(Sticky)`
    position: relative;
    flex-shrink: 0;
    height: ${(p) => p.theme.headerBarHeight}px;
    background: white;
    border-bottom: 1px solid ${(p) => p.theme.color.grey[30]};

    &[data-sticky='true'] {
        box-shadow: ${(p) => p.theme.shadow.soft.bottom};
        z-index: 11;
    }

    ${Container} {
        height: 100%;
        display: flex;
        align-items: center;
    }

    ${H3} {
        color: ${(p) => p.theme.color.primary.main};
    }
`

const ActionContainer = ({
    title,
    canSaveAsDraft,
    saveAsDraft,
    publish,
    onDelete,
    onClose,
    timeScheduled,
    children,
    isPublished,
    isValid,
    isSavingAsDraft,
    isPublishing,
    isChangingRepeatingEvent,
}) => {
    const { t } = useTranslation()

    const isCreating = !onDelete
    const isScheduling = !!timeScheduled

    const isLoading = isSavingAsDraft || isPublishing

    return (
        <>
            <Document title={title} />
            <header>
                <Wrapper>
                    <Container>
                        <IconButton
                            size="large"
                            variant="secondary"
                            onClick={onClose}
                            tooltip={t('action.go-back')}
                            disabled={isLoading}
                        >
                            <BackIcon />
                        </IconButton>

                        <MobileLandscapeUp>
                            <H3>{title}</H3>
                        </MobileLandscapeUp>

                        <div style={{ marginRight: 'auto' }} />

                        <Flexer gutter="small">
                            <Flexer>
                                {canSaveAsDraft && !isPublished && (
                                    <Button
                                        size="normal"
                                        variant="secondary"
                                        disabled={!isValid || isPublishing}
                                        loading={isSavingAsDraft}
                                        onClick={saveAsDraft}
                                    >
                                        {isCreating
                                            ? t('action.save-as-draft')
                                            : t('action.save-draft')}
                                    </Button>
                                )}
                                <Tooltip
                                    disabled={!isScheduling}
                                    content={t('form.scheduled-for', {
                                        date: showDateTime(timeScheduled),
                                    })}
                                >
                                    <div>
                                        <Button
                                            size="normal"
                                            variant={
                                                isChangingRepeatingEvent
                                                    ? 'secondary'
                                                    : 'primary'
                                            }
                                            disabled={
                                                !isValid ||
                                                isSavingAsDraft ||
                                                isScheduling
                                            }
                                            loading={isPublishing}
                                            onClick={publish}
                                        >
                                            {isPublished
                                                ? t('action.save')
                                                : t('action.publish')}
                                        </Button>
                                    </div>
                                </Tooltip>
                            </Flexer>

                            {!!onDelete && (
                                <IconButton
                                    size="large"
                                    variant="secondary"
                                    onClick={onDelete}
                                    tooltip={t('action.delete')}
                                    disabled={isLoading}
                                >
                                    <DeleteIcon />
                                </IconButton>
                            )}
                        </Flexer>
                    </Container>
                </Wrapper>
            </header>
            {/* TODO: Section makes card shadow harder (better to fix with ThemeProvider in the future) */}
            <Section backgroundColor="white" style={{ padding: '24px 0 80px' }}>
                <ThemeProvider bodyColor="#FFFFFF" />
                {children}
            </Section>
        </>
    )
}

export default ActionContainer
