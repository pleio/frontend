import React, { useEffect, useRef, useState } from 'react'
import { media } from 'helpers'
import { useDesktopUp } from 'helpers/breakpoints'
import styled from 'styled-components'

import Bookmark from 'js/components/Bookmark/Bookmark'
import Follow from 'js/components/Follow/Follow'
import Likes from 'js/components/Likes/Likes'

const Wrapper = styled.ul`
    display: inline-flex;
    backface-visibility: hidden;

    ${media.tabletDown`
        padding-top: 0 !important;
        flex-direction: row;
        margin-top: 20px;
        margin-left: auto;

        > *:not(:last-child) {
            margin-right: 20px;
        }
    `};

    ${media.desktopUp`
        flex-direction: column;
        position: absolute;
        top: 0;
        padding-top: 60px;
        right: -20px;
        transform: translateX(100%);
    `};
`

const LikeAndBookmark = ({ entity }) => {
    const [paddingTop, setPaddingTop] = useState(0)

    const isDesktopUp = useDesktopUp()

    const refLikeAndBookmark = useRef()

    useEffect(() => {
        window.addEventListener('scroll', onScroll)
        window.addEventListener('touchmove', onScroll)

        return () => {
            window.removeEventListener('scroll', onScroll)
            window.removeEventListener('touchmove', onScroll)
        }
    })

    const onScroll = () => {
        if (!isDesktopUp) return

        const headerHeight = 60
        const scrolled =
            document.body.scrollTop || document.documentElement.scrollTop

        if (typeof refLikeAndBookmark.current === 'undefined') return

        const offsetTop = refLikeAndBookmark.current.getBoundingClientRect().top

        if (scrolled < offsetTop - headerHeight) return

        setPaddingTop(-offsetTop + headerHeight + 10)
    }

    return (
        <Wrapper ref={refLikeAndBookmark} style={{ paddingTop }}>
            <li>
                <Bookmark entity={entity} />
            </li>
            <li>
                <Likes entity={entity} />
            </li>
            <li>
                <Follow entity={entity} />
            </li>
        </Wrapper>
    )
}

export default LikeAndBookmark
