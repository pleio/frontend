import React, { useState } from 'react'
import handleInputPromise from 'helpers/handleInputPromise'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import styles from './selectButtonStyles'

const Wrapper = styled.div`
    ${styles};
`

const SelectButton = React.memo(
    ({ name, options, value, disabled, onChange, onHandle, ...rest }) => {
        const [isLoading, setisLoading] = useState(false)

        const handleChange = (evt) => {
            handleInputPromise(onHandle(evt), setisLoading)
        }

        return (
            <Wrapper isLoading={isLoading} {...rest}>
                {options.map((option, index) => (
                    <div
                        className="SelectButtonOption"
                        key={`${name}-${index}`}
                    >
                        <input
                            type="radio"
                            id={`${name}-${index}`}
                            name={name}
                            value={option.value}
                            checked={value === option.value}
                            disabled={disabled}
                            onChange={onChange || handleChange}
                        />
                        <label htmlFor={`${name}-${index}`}>
                            {option.label}
                        </label>
                    </div>
                ))}
            </Wrapper>
        )
    },
    (prev, next) =>
        prev.value === next.value && prev.disabled === next.disabled,
)

SelectButton.displayName = 'SelectButton'

SelectButton.propTypes = {
    name: PropTypes.string.isRequired,
    options: PropTypes.oneOfType([PropTypes.string, PropTypes.array])
        .isRequired,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    onChange: PropTypes.func,
}

export default SelectButton
