import { css } from 'styled-components'

export default css`
    display: flex;
    flex-wrap: wrap;
    background-color: white;
    padding-bottom: 1px;

    ${(p) =>
        p.isLoading &&
        css`
            pointer-events: none;
            cursor: default;
        `};

    .SelectButtonOption {
        margin-bottom: -1px;

        label {
            display: flex;
            align-items: center;
            height: 32px;
            padding: 0 12px;
            white-space: nowrap;
            font-family: ${(p) => p.theme.font.family};
            font-size: ${(p) => p.theme.font.size.small};
            font-weight: ${(p) => p.theme.font.weight.semibold};
            color: ${(p) => p.theme.color.text.grey};
            border: 1px solid ${(p) => p.theme.color.grey[30]};
            user-select: none;
            transition:
                color ${(p) => p.theme.transition.fast},
                border-color ${(p) => p.theme.transition.fast},
                background-color ${(p) => p.theme.transition.fast};
        }

        &:first-child label {
            border-radius: ${(p) => p.theme.radius.small} 0 0
                ${(p) => p.theme.radius.small};
        }

        &:last-child label {
            border-radius: 0 ${(p) => p.theme.radius.small}
                ${(p) => p.theme.radius.small} 0;
        }

        &:not(:last-child) label {
            margin-right: -1px;
        }

        input {
            opacity: 0;
            position: absolute;
            pointer-events: none;

            &[disabled] + label {
                pointer-events: none;
            }

            &:checked + label {
                /* Places checked label on top of grey border of next option */
                position: relative;
                background-color: ${(p) => p.theme.color.secondary.main};
                border-color: transparent;
                color: white;
            }
            &:checked[disabled] + label {
                background-color: ${(p) => p.theme.color.grey[20]};
                color: ${(p) => p.theme.color.grey[40]};
            }

            body:not(.mouse-user) &:focus + label {
                outline: ${(p) => p.theme.focusStyling};
            }

            &:not(:checked) + label {
                cursor: pointer;

                &:hover {
                    background-color: ${(p) => p.theme.color.hover};
                }

                &:active {
                    background-color: ${(p) => p.theme.color.active};
                }
            }
            &:not(:checked)[disabled] + label {
                border-color: ${(p) => p.theme.color.grey[30]};
                color: ${(p) => p.theme.color.grey[40]};
            }
        }
    }
`
