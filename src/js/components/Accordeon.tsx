import React, { useState } from 'react'
import { useTabletDown } from 'helpers/breakpoints'
import styled from 'styled-components'

import Card, { CardContent } from 'js/components/Card/Card'
import { H3 } from 'js/components/Heading'

import ArrowRightIcon from 'icons/chevron-right.svg'

const Wrapper = styled.div`
    button {
        display: flex;
        justify-content: space-between;

        svg {
            align-self: center;
            transform: rotate(90deg);
        }
    }
    &.___is-open {
        button svg {
            transform: rotate(-90deg);
        }
    }
`

export interface Props {
    title: string
    children: React.ReactNode
}

const Accordeon = ({ title, children }: Props) => {
    const [isOpen, setIsOpen] = useState(false)

    const toggleOpen = () => {
        setIsOpen(!isOpen)
    }

    return useTabletDown() ? (
        <Wrapper className={`card ${isOpen ? '___is-open' : ''}`}>
            <div className="card__content">
                <button style={{ width: '100%' }} onClick={toggleOpen}>
                    <H3 as="h2">{title}</H3>
                    <ArrowRightIcon />
                </button>
                {isOpen && (
                    <div data-accordion-content>
                        <div>{children}</div>
                    </div>
                )}
            </div>
        </Wrapper>
    ) : (
        <Card style={{ marginBottom: '20px' }}>
            <CardContent>
                <H3 as="h2" style={{ marginBottom: '8px' }}>
                    {title}
                </H3>
                {children}
            </CardContent>
        </Card>
    )
}

export default Accordeon
