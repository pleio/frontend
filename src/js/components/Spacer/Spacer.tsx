import styled, { css } from 'styled-components'

import shouldForwardProp from 'js/lib/helpers/shouldForwardProp'

interface Props {
    spacing?: 'tiny' | 'small' | 'normal' | 'large'
}

const Spacer = styled.div
    .attrs((props) => ({ spacing: 'normal', ...props }))
    .withConfig({
        shouldForwardProp: shouldForwardProp(['spacing']),
    })<Props>`
    ${(p) => p.spacing === 'tiny' && spacingHandler(p, 8)};
    ${(p) => p.spacing === 'small' && spacingHandler(p, 16)};
    ${(p) => p.spacing === 'normal' && spacingHandler(p, 24)};
    ${(p) => p.spacing === 'large' && spacingHandler(p, 32)};
`

const spacingHandler = (_: any, margin: number) => {
    return css`
        > *:not(:first-child) {
            margin-top: ${margin}px;
        }
    `
}

export default Spacer
