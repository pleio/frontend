import React from 'react'
import { useTranslation } from 'react-i18next'
import { textContrastIsAA } from 'helpers/getContrast'
import styled from 'styled-components'

import Avatar from 'js/components/Avatar/Avatar'
import DropdownButton from 'js/components/DropdownButton/DropdownButton'
import NavigationAction from 'js/layout/Navigation/components/NavigationAction'

const UserMenuButton = styled(NavigationAction)`
    .UserMenuAvatar {
        width: 34px;
        height: 34px;
        border-width: 2px;
        border-color: ${(p) =>
            textContrastIsAA(p.theme.color.header.main) ? 'white' : 'black'};
    }

    &:hover {
        &:before {
            /* Cancel NavigationAction styling */
            background-color: transparent;
        }

        .UserMenuAvatar:before {
            background-color: rgba(255, 255, 255, 0.2);
        }
    }

    &:active {
        &:before {
            /* Cancel NavigationAction styling */
            background-color: transparent;
        }

        .UserMenuAvatar:before {
            background-color: transparent;
        }
    }
`

const NavigationUserMenu = ({ site, viewer }) => {
    const { t } = useTranslation()

    const firstPublishRequestType = getFirstPublishRequestType(site)

    const { user, canAccessAdmin, updateAccountUrl } = viewer
    const { username, icon, name } = user

    const options = [
        [
            {
                name: t('saved.title'),
                to: '/saved',
            },
            {
                name: t('user.my-files'),
                to: `/user/${username}/files`,
            },
        ],
        [
            {
                name: t('profile.title'),
                to: `/user/${username}/profile`,
            },
            {
                name: t('user.activity'),
                to: `/user/${username}/activity`,
            },
            ...(firstPublishRequestType
                ? [
                      {
                          name: t('content-moderation.publish-requests'),
                          to: `/user/${username}/publish-requests/${firstPublishRequestType}`,
                      },
                  ]
                : []),
            {
                name: t('global.settings'),
                to: `/user/${username}/settings`,
            },
        ],
        [
            ...(canAccessAdmin
                ? [
                      {
                          name: t('global.admin-panel'),
                          to: `/admin`,
                      },
                  ]
                : []),
            {
                name: t('user.account'),
                href: updateAccountUrl,
                target: '_blank',
            },
            {
                name: t('action.logout'),
                to: '/logout',
            },
        ],
    ]

    return (
        <DropdownButton
            arrow={true}
            offset={[0, 0]}
            placement="bottom"
            animation="shift-away"
            options={options}
        >
            <UserMenuButton aria-label={t('user.usermenu')}>
                <Avatar
                    size="large"
                    outline
                    image={icon}
                    name={name}
                    disabled
                    className="UserMenuAvatar"
                />
            </UserMenuButton>
        </DropdownButton>
    )
}

function getFirstPublishRequestType(site) {
    const {
        contentModerationEnabled,
        requireContentModerationFor,
        commentModerationEnabled,
        requireCommentModerationFor,
    } = site

    if (contentModerationEnabled && requireContentModerationFor.length > 0) {
        const requireContentModerationForWithoutFile =
            requireContentModerationFor.filter((t) => t !== 'file')
        if (requireContentModerationForWithoutFile.length > 0) {
            return 'content'
        } else if (requireContentModerationFor.includes('file')) {
            return 'files'
        }
    } else if (
        commentModerationEnabled &&
        requireCommentModerationFor.length > 0
    ) {
        return 'comments'
    }
    return null
}

export default NavigationUserMenu
