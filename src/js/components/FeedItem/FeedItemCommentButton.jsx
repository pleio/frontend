import React, { forwardRef } from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import HideVisually from 'js/components/HideVisually/HideVisually'

import RepliesIcon from 'icons/replies-small.svg'
import RepliesFillIcon from 'icons/replies-small-fill.svg'

const Wrapper = styled.button`
    &:hover {
        background-color: ${(p) => p.theme.color.hover};
    }

    &:active {
        background-color: ${(p) => p.theme.color.active};
    }

    .FeedItemFooterActionIcon {
        color: ${(p) =>
            p.$commentsVisible
                ? p.theme.color.secondary.main
                : p.theme.color.icon.black};
        margin-right: 6px;
    }
`

const FeedItemCommentButton = forwardRef(
    ({ entity, commentsVisible, toggleComments }, ref) => {
        const { t } = useTranslation()

        if (!entity) return null

        const { title, guid, url, commentCount } = entity

        const idCommentCount = `${guid}-comment-count`

        return (
            <>
                <HideVisually id={idCommentCount}>
                    {commentCount === 0
                        ? commentsVisible
                            ? t('comments.close-add-comment-to-item', { title })
                            : t('comments.add-comment-to-item', { title })
                        : commentsVisible
                          ? t('comments.hide-comments', { title })
                          : t('comments.show-comments', { title })}
                </HideVisually>
                <Wrapper
                    ref={ref}
                    as={toggleComments ? 'button' : Link}
                    to={toggleComments ? null : url}
                    className="FeedItemFooterAction"
                    $commentsVisible={commentsVisible}
                    onClick={toggleComments || null}
                    aria-describedby={idCommentCount}
                    aria-controls={`${guid}-comments`}
                    aria-expanded={commentsVisible}
                >
                    {commentsVisible ? (
                        <RepliesFillIcon className="FeedItemFooterActionIcon" />
                    ) : (
                        <RepliesIcon className="FeedItemFooterActionIcon" />
                    )}
                    {commentCount === 0
                        ? t('comments.comment')
                        : t('comments.count-comments', {
                              count: commentCount,
                          })}
                </Wrapper>
            </>
        )
    },
)

FeedItemCommentButton.displayName = 'FeedItemCommentButton'

export default FeedItemCommentButton
