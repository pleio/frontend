import React from 'react'
import { media } from 'helpers'
import styled from 'styled-components'

import Card from 'js/components/Card/Card'

const Wrapper = styled(Card)`
    display: flex;
    flex-direction: column;
    margin-bottom: 20px;

    .FeedItemImage {
        border-top-left-radius: calc(${(p) => p.theme.radius.normal} - 1px);
        border-top-right-radius: calc(${(p) => p.theme.radius.normal} - 1px);
        order: -1;
    }

    .FeedItemActions {
        margin-right: -8px; // Create some extra space in the header
        margin-left: auto;
    }

    .FeedItemOption {
        outline-offset: 0;
        pointer-events: auto;

        ${media.tabletDown`
            background-color: transparent;
        `};

        ${media.desktopUp`
            opacity: 0;
            transition: opacity ${(p) => p.theme.transition.fast};

            body:not(.mouse-user) &,
            &[aria-expanded="true"] {
                opacity: 1;
            }
        `};
    }

    ${media.desktopUp`
        &:hover {
            .FeedItemOption {
                opacity: 1;
            }
        }
    `};

    .FeedItemContent {
        width: 100%;
        display: flex;
        flex-direction: column;
        max-width: 540px;
        margin: 0 auto;
        flex-grow: 1;
    }

    .FeedItemHeader {
        order: -1;
        display: flex;
    }

    .FeedItemAuthorMetaWrapper {
        display: flex;
        margin-bottom: 12px;
    }

    .FeedItemAvatar {
        margin-right: 10px;
    }

    .FeedItemAuthorMeta {
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        margin-top: -2px;
    }

    .FeedItemAuthor {
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        font-weight: ${(p) => p.theme.font.weight.semibold};
        color: ${(p) => p.theme.color.text.black};
    }

    .FeedItemMeta {
        font-size: ${(p) => p.theme.font.size.tiny};
        line-height: 16px;
        color: ${(p) => p.theme.color.text.grey};
        order: -1; // Set for when author is hidden
        margin-bottom: 2px; // Set for when author is hidden
        align-self: center; // Set for when author is hidden

        > * {
            display: inline-block;

            &:not(:last-child):after {
                content: '  •  ';
                white-space: pre;
            }
        }

        a:hover {
            color: ${(p) => p.theme.color.text.black};
        }
    }

    .FeedItemAuthor + .FeedItemMeta {
        margin-top: -2px;
        order: 0; // Reset for when author is shown
        margin-bottom: 0; // Reset for when author is shown
        align-self: flex-start; // Reset for when author is shown
    }

    .FeedItemTitle {
        + .FeedItemExcerpt {
            margin-top: 4px;
        }
    }

    .FeedItemDate {
        display: flex;
        justify-content: center;
        align-items: center;
        padding-top: 24px;
    }
`

const FeedItem = ({ ...props }) => <Wrapper as="article" {...props} />

export default FeedItem
