import React from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation, useNavigate } from 'react-router-dom'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import styled, { css } from 'styled-components'

import HideVisually from 'js/components/HideVisually/HideVisually'

import HeartIcon from 'icons/heart-small.svg'
import HeartFillIcon from 'icons/heart-small-fill.svg'
import ThumbsIcon from 'icons/thumbs-small.svg'
import ThumbsFillIcon from 'icons/thumbs-small-fill.svg'

const Wrapper = styled.div`
    ${(p) =>
        p.$canLike &&
        css`
            &:hover {
                background-color: ${(p) => p.theme.color.hover};
            }

            &:active {
                background-color: ${(p) => p.theme.color.active};
            }
        `};

    .FeedItemFooterActionIcon {
        margin-right: 5px;
        color: ${(p) =>
            p.$hasLiked
                ? p.theme.color.secondary.main
                : p.theme.color.icon.black};
    }
`

const FeedItemLikeButton = ({ viewer, entity, mutate }) => {
    const { t } = useTranslation()
    const navigate = useNavigate()
    const location = useLocation()

    if (!viewer || !entity) return null

    const { loggedIn } = viewer

    const toggleLike = (evt) => {
        evt.preventDefault()

        const { guid, __typename, hasVoted } = entity

        mutate({
            variables: {
                input: {
                    guid,
                    score: hasVoted ? -1 : 1,
                },
            },
            optimisticResponse: {
                vote: {
                    __typename: 'votePayload',
                    object: {
                        __typename,
                        guid,
                        hasVoted: !hasVoted,
                        votes: hasVoted ? entity.votes - 1 : entity.votes + 1,
                    },
                },
            },
        }).catch((error) => {
            if (error.graphQLErrors[0].message === 'not_logged_in') {
                navigate('/login', {
                    state: { next: location.pathname },
                })
            }
        })
    }

    let Icon
    if (window.__SETTINGS__.site.likeIcon === 'thumbs') {
        Icon = entity.hasVoted ? ThumbsFillIcon : ThumbsIcon
    } else {
        Icon = entity.hasVoted ? HeartFillIcon : HeartIcon
    }

    const id = `${entity.guid}-likes`

    const hasVotes = entity.votes > 0

    const likeDisabled = !loggedIn && !window.__SETTINGS__.showLoginRegister

    return (
        <>
            <HideVisually id={id}>
                {likeDisabled
                    ? t('global.likes', { count: entity.votes })
                    : entity.hasVoted
                      ? t('action.unlike-item', {
                            title: entity.title,
                        })
                      : t('action.like-item', {
                            title: entity.title,
                        })}
            </HideVisually>
            <Wrapper
                className="FeedItemFooterAction"
                {...(likeDisabled
                    ? {}
                    : {
                          as: 'button',
                          type: 'button',
                          onClick: toggleLike,
                          'aria-describedby': id,
                      })}
                $canLike={!likeDisabled}
                $hasLiked={entity.hasVoted}
            >
                <Icon
                    className="FeedItemFooterActionIcon"
                    style={
                        window.__SETTINGS__.site.likeIcon === 'thumbs'
                            ? { marginBottom: '2px' }
                            : {}
                    }
                />
                {hasVotes || likeDisabled
                    ? t('global.likes', { count: entity.votes })
                    : t('action.like')}
            </Wrapper>
        </>
    )
}

const Mutation = gql`
    mutation Likes($input: voteInput!) {
        vote(input: $input) {
            object {
                guid
                ... on Comment {
                    hasVoted
                    votes
                }
                ... on FileFolder {
                    hasVoted
                    votes
                }
                ... on Task {
                    hasVoted
                    votes
                }
                ... on Blog {
                    hasVoted
                    votes
                }
                ... on News {
                    hasVoted
                    votes
                }
                ... on Question {
                    hasVoted
                    votes
                }
                ... on Discussion {
                    hasVoted
                    votes
                }
                ... on StatusUpdate {
                    hasVoted
                    votes
                }
                ... on Podcast {
                    hasVoted
                    votes
                }
            }
        }
    }
`

export default graphql(Mutation)(FeedItemLikeButton)
