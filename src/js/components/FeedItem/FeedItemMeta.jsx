import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'

import DisplayDate from 'js/components/DisplayDate'
import useSubtypes from 'js/lib/hooks/useSubtypes'

const FeedItemMeta = ({ entity, subtype, translateSubtype = true, group }) => {
    const { t } = useTranslation()

    const { siteName, siteUrl } = entity

    const { subtypes } = useSubtypes()

    const showTimePublished =
        entity.timePublished &&
        entity.__typename !== 'Event' &&
        !!entity?.timePublished

    return (
        <div className="FeedItemMeta">
            {showTimePublished && (
                <span>
                    <DisplayDate
                        date={entity.timePublished}
                        type="timeSince"
                        hiddenPrefix={t('global.publication-date')}
                        isFocusable
                    />
                </span>
            )}

            {subtype && subtypes[subtype] ? (
                translateSubtype ? (
                    <span>{subtypes[subtype].contentName}</span>
                ) : (
                    <span>{subtype}</span>
                )
            ) : null}

            {siteName && <Link to={siteUrl}>{siteName}</Link>}

            {group && (
                <Link
                    to={group.url}
                    aria-label={t('widget-feed.published-in-group', {
                        group: group.name,
                    })}
                >
                    {group.name}
                </Link>
            )}
        </div>
    )
}

export default FeedItemMeta
