import React, { useState } from 'react'
import { Link, useLocation } from 'react-router-dom'

import { CardContent } from 'js/components/Card/Card'
import FeedItemActions from 'js/components/FeedItem/FeedItemActions'
import { H3 } from 'js/components/Heading'
import ToggleTranslation from 'js/components/Item/components/ToggleTranslation'
import ItemStatusTag from 'js/components/Item/ItemStatusTag'
import ItemTags from 'js/components/ItemTags'
import TiptapView from 'js/components/Tiptap/TiptapView'
import Truncate from 'js/components/Truncate/Truncate'
import UserAvatar from 'js/components/UserAvatar'
import { useSiteStore } from 'js/lib/stores'

import FeedItemMeta from './FeedItemMeta'

const RenderExcerpt = ({ excerptMaxLines, children }) => {
    return excerptMaxLines ? (
        <Truncate lines={excerptMaxLines}>{children}</Truncate>
    ) : (
        children
    )
}

const FeedItemContent = ({
    entity,
    hideExcerpt,
    hideSubtype,
    hideGroup,
    excerptMaxLines,
    canPin,
    onEdit,
    hideActions,
    canDuplicate,
    refetchQueries,
    children,
}) => {
    const location = useLocation()

    const { site } = useSiteStore()
    const { showTagsInFeed, showCustomTagsInFeed } = site

    const {
        localTitle,
        title,
        url,
        localExcerpt,
        excerpt,
        owner,
        tags,
        tagCategories,
        showOwner,
        isTranslationEnabled,
    } = entity

    const hasTranslations = !!localTitle && isTranslationEnabled

    const [isTranslated, setIsTranslated] = useState(hasTranslations)

    const translatedTitle = isTranslated ? localTitle : title
    const translatedExcerpt = isTranslated ? localExcerpt || excerpt : excerpt

    const showAuthor = showOwner && owner
    const showExcerpt = !hideExcerpt && excerpt
    const subtype = !hideSubtype && entity.subtype
    const group = !hideGroup && entity.group

    const meta = (
        <FeedItemMeta entity={entity} subtype={subtype} group={group} />
    )

    return (
        <CardContent className="FeedItemContent">
            {title && (
                <H3 className="FeedItemTitle">
                    <Link
                        to={url}
                        state={{ prevPathname: location.pathname }}
                        style={{ display: 'block' }}
                    >
                        <Truncate lines={showExcerpt ? 2 : 3}>
                            {translatedTitle}
                        </Truncate>
                    </Link>
                    <ItemStatusTag
                        entity={entity}
                        style={{
                            margin: '6px 0',
                        }}
                    />
                </H3>
            )}

            <div className="FeedItemHeader">
                {showAuthor ? (
                    <div className="FeedItemAuthorMetaWrapper">
                        <div className="FeedItemAvatar">
                            <UserAvatar user={owner} />
                        </div>
                        <div className="FeedItemAuthorMeta">
                            <Link to={owner.url} className="FeedItemAuthor">
                                {owner.name}
                            </Link>
                            {meta}
                        </div>
                    </div>
                ) : (
                    meta
                )}
                <FeedItemActions
                    entity={entity}
                    canPin={canPin}
                    canDuplicate={canDuplicate}
                    onEdit={onEdit}
                    hideActions={hideActions}
                    refetchQueries={refetchQueries}
                />
            </div>

            {showExcerpt && (
                <div className="FeedItemExcerpt">
                    <RenderExcerpt excerptMaxLines={excerptMaxLines}>
                        {/* TiptapView is needed because the introduction(`abstract`) can contain links. */}
                        <TiptapView
                            content={translatedExcerpt}
                            contentType={'html'}
                        />
                    </RenderExcerpt>
                </div>
            )}

            {hasTranslations && (
                <ToggleTranslation
                    isTranslated={isTranslated}
                    setIsTranslated={setIsTranslated}
                    style={{ marginTop: '4px' }}
                />
            )}

            {children}

            <ItemTags
                showCustomTags={showCustomTagsInFeed}
                showTags={showTagsInFeed}
                customTags={tags}
                tagCategories={tagCategories}
                style={{ marginTop: '12px' }}
            />
        </CardContent>
    )
}

export default FeedItemContent
