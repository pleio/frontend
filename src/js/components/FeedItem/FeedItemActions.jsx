import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation } from 'react-router-dom'
import { gql, useMutation } from '@apollo/client'

import DropdownButton from 'js/components/DropdownButton/DropdownButton'
import IconButton from 'js/components/IconButton/IconButton'
import CopyItem from 'js/components/Item/ItemActions/components/CopyItem'
import DeleteForm from 'js/components/Item/ItemActions/components/DeleteForm'
import PinItem from 'js/components/Item/ItemActions/components/PinItem'
import Modal from 'js/components/Modal/Modal'

import OptionsIcon from 'icons/options.svg'
import IconBookmark from 'icons/save.svg'
import IconBookmarkFill from 'icons/save-fill.svg'

export default function FeedItemActions({
    entity,
    canPin,
    onEdit,
    canDuplicate,
    refetchQueries,
    hideActions,
}) {
    const [mutateBookmark] = useMutation(BookmarkMutation)

    const { t } = useTranslation()
    const location = useLocation()

    const [showDeleteModal, setShowDeleteModal] = useState(false)

    const [showPinModal, setShowPinModal] = useState(false)

    const [showCopyEventModal, setShowCopyEventModal] = useState(false)

    if (!entity) return null

    const {
        __typename,
        guid,
        title,
        canEdit,
        isBookmarked,
        canBookmark,
        isPinned,
        statusPublished,
        canArchiveAndDelete,
        preventDeleteAndArchive,
    } = entity

    const toggleBookmark = () => {
        const isAdding = !isBookmarked

        mutateBookmark({
            variables: {
                input: {
                    guid,
                    isAdding,
                },
            },
            refetchQueries: ['BookmarkList'],
            optimisticResponse: {
                bookmark: {
                    __typename: 'bookmarkPayload',
                    object: {
                        __typename,
                        guid,
                        isBookmarked: isAdding,
                    },
                },
            },
        })
    }

    const isArchived = statusPublished === 'archived'
    const showEdit = canEdit && !isArchived && onEdit
    const showDuplicate = canEdit && canDuplicate
    const showDelete = canArchiveAndDelete && !preventDeleteAndArchive

    const hasSomeActions =
        !hideActions && (canPin || showEdit || showDuplicate || showDelete)

    const showBookmark = canBookmark && statusPublished === 'published'

    const options = hideActions
        ? []
        : [
              [
                  ...(showEdit
                      ? [
                            {
                                name: t('action.edit-item'),
                                label: t('action.edit-item-label', {
                                    title,
                                }),
                                ...(typeof onEdit === 'string'
                                    ? {
                                          to: onEdit,
                                          state: {
                                              prevPathname: location.pathname,
                                          },
                                      }
                                    : {
                                          onClick: onEdit,
                                      }),
                            },
                        ]
                      : []),
                  ...(showDuplicate
                      ? [
                            {
                                name: t('action.duplicate-item'),
                                label: t('action.duplicate-item-label', {
                                    title,
                                }),
                                onClick: () => setShowCopyEventModal(true),
                            },
                        ]
                      : []),
                  ...(showDelete
                      ? [
                            {
                                name: t('action.delete-item'),
                                label: t('action.delete-item-label', {
                                    title,
                                }),
                                onClick: () => setShowDeleteModal(true),
                            },
                        ]
                      : []),
              ],
              [
                  ...(showBookmark && hasSomeActions
                      ? [
                            {
                                name: isBookmarked
                                    ? t('action.unsave-item', {
                                          saved: t('saved.title'),
                                      })
                                    : t('action.save-item'),
                                onClick: toggleBookmark,
                            },
                        ]
                      : []),
                  ...(canPin
                      ? [
                            {
                                name: isPinned
                                    ? t('action.unpin-item')
                                    : t('action.pin-item'),
                                label: isPinned
                                    ? t('action.unpin-item-label', {
                                          title,
                                      })
                                    : t('action.pin-item-label', {
                                          title,
                                      }),
                                onClick: () => setShowPinModal(true),
                            },
                        ]
                      : []),
              ],
          ]

    return (
        <div className="FeedItemActions">
            {showBookmark && !hasSomeActions ? (
                <IconButton
                    variant={isBookmarked ? 'primary' : 'secondary'}
                    size="normal"
                    tooltip={
                        isBookmarked
                            ? t('action.unsave-item', {
                                  saved: t('saved.title'),
                              })
                            : t('action.save-item')
                    }
                    aria-label={
                        isBookmarked
                            ? t('action.unsave-item-label', {
                                  saved: t('saved.title'),
                                  title,
                              })
                            : t('action.save-item-label', { title })
                    }
                    onClick={toggleBookmark}
                    className="FeedItemOption"
                    style={
                        isBookmarked
                            ? {
                                  opacity: 1,
                              }
                            : {}
                    }
                >
                    {isBookmarked ? <IconBookmarkFill /> : <IconBookmark />}
                </IconButton>
            ) : (
                <>
                    <DropdownButton
                        options={options}
                        popperOptions={{
                            strategy: 'absolute', // Prevents weird positioning in featured slider
                        }}
                    >
                        <IconButton
                            variant="secondary"
                            size="normal"
                            className="FeedItemOption"
                            tooltip={t('global.options')}
                            aria-label={t('action.show-options')}
                        >
                            <OptionsIcon />
                        </IconButton>
                    </DropdownButton>

                    <Modal
                        isVisible={showDeleteModal}
                        onClose={() => setShowDeleteModal(false)}
                        title={t('action.delete-item')}
                        size="small"
                    >
                        <DeleteForm
                            entity={entity}
                            refetchQueries={refetchQueries}
                            onClose={() => setShowDeleteModal(false)}
                            afterDelete={() => setShowDeleteModal(false)}
                        />
                    </Modal>

                    {canPin && (
                        <PinItem
                            isVisible={showPinModal}
                            entity={entity}
                            onClose={() => setShowPinModal(false)}
                            refetchQueries={refetchQueries}
                        />
                    )}

                    {showDuplicate && (
                        <CopyItem
                            isVisible={showCopyEventModal}
                            entity={entity}
                            onClose={() => setShowCopyEventModal(false)}
                        />
                    )}
                </>
            )}
        </div>
    )
}

const BookmarkMutation = gql`
    mutation Bookmark($input: bookmarkInput!) {
        bookmark(input: $input) {
            object {
                guid
                ... on News {
                    isBookmarked
                }
                ... on Blog {
                    isBookmarked
                }
                ... on Discussion {
                    isBookmarked
                }
                ... on Event {
                    isBookmarked
                }
                ... on FileFolder {
                    isBookmarked
                }
                ... on Wiki {
                    isBookmarked
                }
                ... on Question {
                    isBookmarked
                }
                ... on StatusUpdate {
                    isBookmarked
                }
                ... on Podcast {
                    isBookmarked
                }
            }
        }
    }
`
