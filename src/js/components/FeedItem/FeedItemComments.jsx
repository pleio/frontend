import React from 'react'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import styled from 'styled-components'

import AddComment from 'js/components/AddComment/AddComment'
import Comment from 'js/components/Comment/Comment'
import CommentWrapper from 'js/components/Comment/components/Wrapper'
import { entityViewCommentsFragment } from 'js/components/EntityActions/fragments/comments'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'

const Wrapper = styled.div`
    border-top: 1px solid ${(p) => p.theme.color.grey[30]};

    ${CommentWrapper} + ${CommentWrapper} .CommentContent {
        padding-top: 0;
    }
`

const FeedItemComments = ({
    data,
    viewer,
    entity,
    canUpvote,
    refCommentsButton,
}) => {
    const { group, isClosed } = entity
    const viewerData = { ...data?.viewer, ...viewer }

    const refetchQueries = ['FeedComments']

    return (
        <Wrapper id={`${entity.guid}-comments`}>
            {entity.statusPublished === 'published' && (
                <AddComment
                    viewer={viewerData}
                    entity={entity}
                    group={group}
                    refCommentsButton={refCommentsButton}
                    refetchQueries={refetchQueries}
                    style={{ padding: '20px 20px 0' }}
                />
            )}
            {data?.loading && <LoadingSpinner style={{ margin: '16px 0' }} />}
            {!data?.loading && (
                <ol style={{ marginTop: '20px' }}>
                    {data?.entity.comments.map((item) => (
                        <Comment
                            key={item.guid}
                            viewer={viewerData}
                            entity={item}
                            group={group}
                            hideReply={
                                isClosed ||
                                (!!entity.group &&
                                    entity.group.membership !== 'joined')
                            }
                            isClosed={isClosed}
                            inActivityFeed
                            canUpvote={canUpvote}
                            canChooseBestAnswer={entity.canChooseBestAnswer}
                            refetchQueries={refetchQueries}
                        />
                    ))}
                </ol>
            )}
        </Wrapper>
    )
}

const Query = gql`
    query FeedComments($guid: String!, $subtype: String!, $groupGuid: String) {
        viewer {
            guid
            canInsertMedia(subtype: $subtype)
            requiresCommentModeration(subtype: $subtype, groupGuid: $groupGuid)
        }
        entity(guid: $guid) {
            guid
            ... on StatusUpdate {
                ${entityViewCommentsFragment}
            }
            ... on Blog {
                ${entityViewCommentsFragment}
            }
            ... on News {
                ${entityViewCommentsFragment}
            }
            ... on Discussion {
                ${entityViewCommentsFragment}
            }
            ... on Question {
                ${entityViewCommentsFragment}
            }
            ... on Event {
                ${entityViewCommentsFragment}
            }
        }
    }

`

export default graphql(Query)(FeedItemComments)
