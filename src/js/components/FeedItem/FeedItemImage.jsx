import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useLocation } from 'react-router-dom'
import { getVideoFromUrl } from 'helpers'
import styled, { css } from 'styled-components'

import IconButton from 'js/components/IconButton/IconButton'
import Modal from 'js/components/Modal/Modal'
import VideoModal from 'js/components/VideoModal'

import PlayIcon from 'icons/play.svg'

const Wrapper = styled.div`
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: white;
    height: 150px;

    ${(p) =>
        p.$imageSrc
            ? css`
                  @media (max-width: 360px) {
                      background-image: ${(p) => `url(${p.$imageSrc}size=360)`};
                  }
                  @media (min-width: 361px) and (max-width: 414px) {
                      background-image: ${(p) => `url(${p.$imageSrc}size=414)`};
                  }
                  @media (min-width: 415px) {
                      background-image: ${(p) => `url(${p.$imageSrc}size=660)`};
                  }
              `
            : css`
                  background-image: ${(p) => `url(${p.$videoSrc})`};
              `}

    background-size: cover;
    background-position: center;
    background-position-y: ${(p) => p.$backgroundPositionY}%;

    .FeedItemImageLink {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }
`

const FeedItemImage = ({ entity, ...rest }) => {
    const { t } = useTranslation()
    const location = useLocation()

    const [showVideoModal, setShowVideoModal] = useState(false)
    const toggleVideoModal = () => {
        setShowVideoModal(!showVideoModal)
    }

    const { url, featured } = entity
    const { image, alt, video, videoTitle, videoThumbnailUrl, positionY } =
        featured || {}

    const videoObject = !!video && getVideoFromUrl(video)

    let imageSrc = null
    if (image) {
        const hasParam = image?.download.indexOf('?') !== -1
        imageSrc = `${image?.download}${hasParam ? '&' : '?'}`
    }

    return image || video ? (
        <Wrapper
            className="FeedItemImage"
            role={alt ? 'img' : null}
            aria-label={alt || null}
            $imageSrc={imageSrc}
            $backgroundPositionY={positionY}
            $videoSrc={videoThumbnailUrl}
            {...rest}
        >
            {url && (
                <Link
                    to={url}
                    state={{ prevPathname: location.pathname }}
                    className="FeedItemImageLink"
                    tabIndex="-1"
                    aria-hidden
                />
            )}
            {!!videoObject && (
                <>
                    <IconButton
                        variant="secondary"
                        size="normal"
                        dropShadow
                        onClick={toggleVideoModal}
                        aria-label={t('action.play-video')}
                    >
                        <PlayIcon />
                    </IconButton>
                    <Modal
                        isVisible={showVideoModal}
                        onClose={toggleVideoModal}
                        maxWidth="860px"
                        size="normal"
                        hideBackground
                        noPadding
                    >
                        <VideoModal
                            id={videoObject.id}
                            title={videoTitle}
                            type={videoObject.type}
                            onClose={toggleVideoModal}
                        />
                    </Modal>
                </>
            )}
        </Wrapper>
    ) : null
}

export default FeedItemImage
