import React, { useRef, useState } from 'react'
import { gql, useQuery } from '@apollo/client'
import styled from 'styled-components'

import FeedItemCommentButton from 'js/components/FeedItem/FeedItemCommentButton'
import FeedItemComments from 'js/components/FeedItem/FeedItemComments'
import FeedItemLikeButton from 'js/components/FeedItem/FeedItemLikeButton'
import { useViewerStore } from 'js/lib/stores'

const Wrapper = styled.div`
    position: relative; // Makes for better styling of button outline
    display: flex;
    flex-shrink: 0;
    border-top: 1px solid ${(p) => p.theme.color.grey[30]};

    .FeedItemFooterAction {
        width: ${(p) => (p.$fillWidth ? '100%' : '50%')};
        height: 40px;
        display: flex;
        align-items: center;
        justify-content: center;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        color: ${(p) => p.theme.color.text.grey};
        user-select: none;
        outline-offset: -1px;

        &:not(:last-child) {
            border-right: 1px solid ${(p) => p.theme.color.grey[30]};
        }
    }
`

const FeedItemFooterWrapper = ({
    entity,
    hideLikes,
    hideComments,
    canUpvote,
}) => {
    if (
        !entity ||
        entity.statusPublished !== 'published' ||
        (hideLikes && hideComments)
    )
        return null

    return (
        <FeedItemFooter
            entity={entity}
            hideLikes={hideLikes}
            hideComments={hideComments}
            canUpvote={canUpvote}
        />
    )
}

const FeedItemFooter = ({ entity, hideLikes, hideComments, canUpvote }) => {
    const { viewer } = useViewerStore()

    const refCommentsButton = useRef()

    const [commentsVisible, setCommentsVisible] = useState(false)
    const toggleComments = () => setCommentsVisible(!commentsVisible)
    const showComments = !hideComments && commentsVisible

    return (
        <>
            <Wrapper
                className="FeedItemFooter" // Created for bdcommunicatie.pleio.nl
                $fillWidth={hideLikes || hideComments}
            >
                {!hideLikes && (
                    <FeedItemLikeButton viewer={viewer} entity={entity} />
                )}

                {!hideComments && (
                    <FeedItemCommentButton
                        ref={refCommentsButton}
                        entity={entity}
                        commentCount={entity?.commentCount}
                        commentsVisible={commentsVisible}
                        toggleComments={toggleComments}
                    />
                )}
            </Wrapper>

            {!!entity && showComments && (
                <FeedItemComments
                    guid={entity.guid}
                    groupGuid={entity.group?.guid}
                    subtype={entity.subtype}
                    viewer={viewer}
                    entity={entity}
                    canUpvote={canUpvote}
                    refCommentsButton={refCommentsButton}
                />
            )}
        </>
    )
}

export default FeedItemFooterWrapper
