import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import Tag from 'js/components/Tag/Tag'

const Wrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    margin-left: -2px;
    margin-right: -2px;
    margin-bottom: -4px;

    > * {
        margin-left: 2px;
        margin-right: 2px;
        margin-bottom: 4px;
    }
`

const ItemTags = ({
    tagCategories,
    customTags,
    showTags,
    showCustomTags,
    ...rest
}) => {
    if (
        (!tagCategories || tagCategories?.length === 0) &&
        (!customTags || customTags?.length === 0)
    )
        return null

    const searchQuery = '/search?'

    return (
        <Wrapper {...rest}>
            {tagCategories?.map((category) => {
                return category.values.map((tag, i) => {
                    const newCategory = [
                        {
                            name: category.name,
                            values: category.values.filter((t) => t === tag),
                        },
                    ]
                    const searchUrl = `${searchQuery}tags=${encodeURIComponent(
                        JSON.stringify(newCategory),
                    )}`
                    return (
                        <Tag
                            key={`${tag}-${i}`}
                            as={Link}
                            to={searchUrl}
                            data-track-content="" // Matomo tracking
                            data-content-name={tag} // Matomo tracking
                            style={{
                                display: showTags ? 'flex' : 'none',
                            }}
                        >
                            {tag}
                        </Tag>
                    )
                })
            })}

            {customTags?.map((tag, i) => {
                const searchUrl = `${searchQuery}q=${encodeURIComponent(tag)}`
                return (
                    <Tag
                        key={`${tag}-${i}`}
                        as={Link}
                        to={searchUrl}
                        data-track-content="" // Matomo tracking
                        data-content-name={tag} // Matomo tracking
                        style={{
                            display: showCustomTags ? 'flex' : 'none',
                        }}
                    >
                        {tag}
                    </Tag>
                )
            })}
        </Wrapper>
    )
}

export default ItemTags
