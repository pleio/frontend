import React from 'react'
import { useTranslation } from 'react-i18next'

import Error from './ErrorStyles'

interface Props {
    errors?: any
    style?: any
}
const Errors = ({ errors, style }: Props) => {
    const { t } = useTranslation()

    if (!errors || errors.length === 0) return null

    let errorMessages = []
    if (errors.graphQLErrors) {
        errorMessages = errors.graphQLErrors.map(({ message }) => {
            const fileNotClean = message.includes('file_not_clean')
            const contaminatedFileName = fileNotClean
                ? message.split('file_not_clean:')[1].trim()
                : ''
            return fileNotClean
                ? t('error.file-not-clean', { fileName: contaminatedFileName })
                : t(`error.${message}`)
        })
    } else {
        const error = errors.message
        const translation = t(`error.${error}`)

        errorMessages.push(
            translation === `error.${error}`
                ? `${t('error.unknown_error')} (${error})`
                : translation,
        )
    }

    const displayErrors = errorMessages
        .map((error, i) => (error ? <span key={i}>{error}</span> : null))
        .filter(Boolean)

    return <Error style={style}>{displayErrors}</Error>
}

export default Errors
