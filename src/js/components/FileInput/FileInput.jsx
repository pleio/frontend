import React, { forwardRef } from 'react'
import {
    alertFileTooLarge,
    convertFileListToArray,
    isFileSizeOk,
} from 'helpers'

const FileInput = forwardRef(
    ({ name, multiple, accept, maxSize, onSubmit, ...props }, ref) => {
        const handleChange = (evt) => {
            const files = evt.target.files
            if (!files) return

            let value
            if (multiple) {
                const filesArray = convertFileListToArray(files)

                if (maxSize) {
                    let i = filesArray.length
                    // Loop backwards to enable splice
                    while (i--) {
                        const file = filesArray[i]
                        if (!isFileSizeOk(file.size, maxSize)) {
                            alertFileTooLarge(file.size, maxSize, file.name)
                            filesArray.splice(i, 1)
                        }
                    }
                }

                value = filesArray
            } else {
                const file = files[0]
                if (!!maxSize && !isFileSizeOk(file.size, maxSize)) {
                    alertFileTooLarge(file.size, maxSize, file.name)
                    return
                }
                value = file
            }

            evt.target.value = ''

            onSubmit(value)
        }

        return (
            <input
                ref={ref}
                type="file"
                name={name}
                multiple={multiple}
                accept={accept}
                onChange={handleChange}
                {...props}
            />
        )
    },
)

FileInput.displayName = 'FileInput'

export default FileInput
