import React, { useEffect, useRef, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Set } from 'immutable'

import Flexer from 'js/components/Flexer/Flexer'
import { Col, Row } from 'js/components/Grid/Grid'
import Select from 'js/components/Select/Select'
import Tag from 'js/components/Tag/Tag'
import Textfield from 'js/components/Textfield/Textfield'

const TagFields = ({
    name,
    customTagsAllowed,
    tagCategories,
    tags,
    label,
    setTags,
    ...rest
}) => {
    const refCustomTagsField = useRef()

    const [customTags, setCustomTags] = useState()

    const [customTagsField, setCustomTagsField] = useState('')
    const changeCustomTagsField = (evt) => setCustomTagsField(evt.target.value)

    const keyPressCustomTagsField = (evt) => {
        const keyCode = evt.keyCode ? evt.keyCode : evt.which
        if (keyCode === 13) {
            evt.preventDefault()
            addTag(customTagsField)
        }
    }

    const addTag = (val) => {
        setTags(tags.add(val))
        setCustomTagsField('')
    }

    useEffect(() => {
        const getIgnoredTags = () => {
            let returnValue = Set()
            tagCategories.forEach((filter) => {
                returnValue = returnValue.merge(filter.values)
            })
            return returnValue
        }
        setCustomTags(tags.subtract(getIgnoredTags()))
    }, [tags])

    const { t } = useTranslation()

    return (
        <div {...rest}>
            {tagCategories.length > 0 && (
                <Row $gutter={8} $spacing={12}>
                    {tagCategories.map((category, i) => {
                        const { name, values } = category
                        const options = values.map((tag) => ({
                            value: tag,
                            label: tag,
                        }))

                        const categoryTags = {}
                        tagCategories.forEach((category) => {
                            categoryTags[category.name] = []
                            category.values.forEach((tag) => {
                                if (tags.includes(tag)) {
                                    categoryTags[category.name].push(tag)
                                }
                            })
                        })

                        const handleChange = (val) => {
                            const newCategoryTags = categoryTags
                            newCategoryTags[name] = val

                            const newTags = Object.values(
                                newCategoryTags,
                            ).reduce(
                                (tagss, moreTags) => tagss.concat(moreTags),
                                [],
                            )
                            setTags(new Set().merge(newTags).merge(customTags))
                        }

                        return (
                            <Col key={`${name}_${i}`} mobileUp={1 / 2}>
                                <Select
                                    name={name}
                                    label={name}
                                    options={options}
                                    value={categoryTags[name] || []}
                                    onChange={handleChange}
                                    isMulti
                                />
                            </Col>
                        )
                    })}
                </Row>
            )}
            {customTagsAllowed && customTags && (
                <div style={{ marginTop: '12px' }}>
                    <Textfield
                        ref={refCustomTagsField}
                        name={name || 'custom-tags-field'}
                        label={label || t('global.custom-tags')}
                        helper={t('form.custom-tags-helper')}
                        autoComplete="off"
                        onKeyPress={keyPressCustomTagsField}
                        onChange={changeCustomTagsField}
                        value={customTagsField}
                    />
                    {customTags.size > 0 && (
                        <Flexer
                            justifyContent="flex-start"
                            gutter="tiny"
                            style={{
                                marginTop: '8px',
                                marginBottom: '-4px',
                                flexWrap: 'wrap',
                            }}
                        >
                            {customTags.map((tag, i) => {
                                const removeTag = () => {
                                    setTags(tags.delete(tag))
                                }

                                return (
                                    <Tag
                                        key={i}
                                        style={{ marginBottom: '4px' }}
                                        onRemove={removeTag}
                                    >
                                        {tag}
                                    </Tag>
                                )
                            })}
                        </Flexer>
                    )}
                </div>
            )}
        </div>
    )
}

export default TagFields
