import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'

import Errors from 'js/components/Errors'
import UploadBox from 'js/components/UploadBox/UploadBox'
import { FILE_TYPE } from 'js/lib/constants'

import LoadingSpinner from '../LoadingSpinner/LoadingSpinner'

const AttachmentUploadField = ({
    name,
    value,
    fileType = 'file',
    maxFileSize,
    onUploaded,
}) => {
    const { t } = useTranslation()

    const [formFile, setFormFile] = useState(null)
    const [isLoading, setIsLoading] = useState(false)
    const [showError, setShowError] = useState('')
    const [mutate] = useMutation(ATTACHMENT_ADD)

    const onSubmitFile = async (value) => {
        setFormFile(value)
        setIsLoading(true)
        setShowError('')

        try {
            const { data, error } = await mutate({
                variables: {
                    input: {
                        file: value,
                    },
                },
            })

            if (data) {
                onUploaded?.(data.addAttachment.attachment)
            }

            if (error) {
                setShowError(error)
            }

            setIsLoading(false)
        } catch (e) {
            setIsLoading(false)
            setShowError(e)
        }
    }

    const { accept, maxSize } = FILE_TYPE[fileType]

    return (
        <>
            <UploadBox
                title={
                    fileType === 'image'
                        ? t('form.upload-image')
                        : t('form.upload-file')
                }
                name={name}
                accept={accept}
                maxSize={maxFileSize || maxSize}
                onSubmit={onSubmitFile}
            >
                {isLoading ? (
                    <LoadingSpinner />
                ) : (
                    formFile?.name || value?.title
                )}
            </UploadBox>

            <Errors errors={showError} />
        </>
    )
}

const ATTACHMENT_ADD = gql`
    mutation AddAttachment($input: addAttachmentInput!) {
        addAttachment(input: $input) {
            attachment {
                id
                name
                mimeType
                url
            }
        }
    }
`

export default AttachmentUploadField
