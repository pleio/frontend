import { useContext, useEffect } from 'react'

import AriaLiveContext from './AriaLiveContext'

const AriaLiveMessage = ({ message, 'aria-live': ariaLive = 'polite' }) => {
    const { announceAssertive, announcePolite } = useContext(AriaLiveContext)

    useEffect(() => {
        if (ariaLive === 'assertive') {
            announceAssertive(message || null)
        }
        if (ariaLive === 'polite') {
            announcePolite(message || null)
        }
    }, [message])

    return null
}

export default AriaLiveMessage
