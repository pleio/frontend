import React, { useState } from 'react'

import HideVisually from 'js/components/HideVisually/HideVisually'

import AriaLiveContext from './AriaLiveContext'

const AriaLiveProvider = ({ children }) => {
    const [assertiveMessage1, setAssertiveMessage1] = useState()
    const [assertiveMessage2, setAssertiveMessage2] = useState()
    const [politeMessage1, setPoliteMessage1] = useState()
    const [politeMessage2, setPoliteMessage2] = useState()

    const announceAssertive = (message) => {
        setAssertiveMessage1(assertiveMessage1 ? null : message)
        setAssertiveMessage2(assertiveMessage1 ? message : null)
    }

    const announcePolite = (message) => {
        setPoliteMessage1(politeMessage1 ? null : message)
        setPoliteMessage2(politeMessage1 ? message : null)
    }

    return (
        <AriaLiveContext.Provider
            value={{
                announcePolite,
                announceAssertive,
            }}
        >
            {children}
            <HideVisually role="log" aria-live="assertive" aria-atomic="true">
                {assertiveMessage1}
            </HideVisually>
            <HideVisually role="log" aria-live="assertive" aria-atomic="true">
                {assertiveMessage2}
            </HideVisually>
            <HideVisually role="log" aria-live="polite" aria-atomic="true">
                {politeMessage1}
            </HideVisually>
            <HideVisually role="log" aria-live="polite" aria-atomic="true">
                {politeMessage2}
            </HideVisually>
        </AriaLiveContext.Provider>
    )
}

export default AriaLiveProvider
