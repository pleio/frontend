import React from 'react'
import { useTranslation } from 'react-i18next'
import { media } from 'helpers'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

import { H3, H4 } from 'js/components/Heading'
import Text from 'js/components/Text/Text'

const Wrapper = styled.div`
    label.SettingTitle {
        display: block;
        cursor: pointer;
    }

    .SettingRow {
        display: flex;
        justify-content: space-between;
        margin-left: -4px;
        margin-right: -4px;
        margin-bottom: -12px;

        ${(p) =>
            (p.$inputWidth === 'normal' ||
                p.$inputWidth === 'large' ||
                p.$inputWidth === 'full') &&
            css`
                flex-wrap: wrap;
            `};
    }

    .SettingColLeft,
    .SettingColRight {
        padding: 0 4px;
    }

    .SettingColLeft {
        max-width: 330px;
        margin-bottom: 12px;
    }

    .SettingColRight {
        position: relative;
        flex-grow: 1;
        display: flex;
        flex-direction: column;
        align-items: flex-end;
        padding-bottom: 12px;

        ${(p) =>
            (p.$inputWidth === 'normal' || p.$inputWidth === 'large') &&
            css`
                ${media.mobilePortrait`
                    display: block;
                    width: 100%;
                `}
            `};

        ${(p) =>
            p.$inputWidth === 'full' &&
            css`
                display: block;
                width: 100%;
            `};
    }

    .SettingInputContainer {
        ${media.mobilePortrait`
            max-width: 100%;
        `}

        ${(p) =>
            p.$inputWidth === 'small' &&
            css`
                width: 100%;
                min-width: 72px;
                max-width: 72px;
            `};

        ${(p) =>
            p.$inputWidth === 'normal' &&
            css`
                width: 100%;
                max-width: 180px;
            `};

        ${(p) =>
            p.$inputWidth === 'large' &&
            css`
                width: 100%;
                max-width: 270px;
            `};
    }
`

const Setting = ({
    children,
    title,
    subtitle,
    htmlFor,
    helper,
    inputWidth,
    ...rest
}) => {
    const { t } = useTranslation()

    const Title = (
        <H3
            className="SettingTitle"
            as={!!htmlFor && !subtitle ? 'label' : 'h2'}
            htmlFor={!!htmlFor && !subtitle ? htmlFor : null}
            style={{
                marginBottom: subtitle ? '24px' : helper ? '4px' : '',
            }}
        >
            {typeof title === 'string' ? t(`${title}`) : title}
        </H3>
    )

    return (
        <Wrapper $inputWidth={inputWidth} {...rest}>
            {!!title &&
                (!!subtitle || (!subtitle && !helper && !children)) &&
                Title}
            {(!!subtitle || !!helper || !!children) && (
                <div className="SettingRow">
                    <div className="SettingColLeft">
                        {!!title &&
                            !subtitle &&
                            (!!helper || !!children) &&
                            Title}

                        {!!subtitle && (
                            <H4
                                className="SettingTitle"
                                as={htmlFor ? 'label' : 'h3'}
                                htmlFor={htmlFor}
                            >
                                {typeof subtitle === 'string'
                                    ? t(`${subtitle}`)
                                    : subtitle}
                            </H4>
                        )}
                        {helper && (
                            <Text variant="grey" size="small">
                                {typeof helper === 'string'
                                    ? t(`${helper}`)
                                    : helper}
                            </Text>
                        )}
                    </div>
                    <div className="SettingColRight">
                        {inputWidth ? (
                            <div className="SettingInputContainer">
                                {children}
                            </div>
                        ) : (
                            children
                        )}
                    </div>
                </div>
            )}
        </Wrapper>
    )
}

Setting.propTypes = {
    title: PropTypes.any,
    htmlFor: PropTypes.string,
    subtitle: PropTypes.any,
    helper: PropTypes.any,
    inputWidth: PropTypes.oneOf(['small', 'normal', 'large', 'full']),
}

export default Setting
