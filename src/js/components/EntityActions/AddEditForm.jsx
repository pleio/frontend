import React, { useRef, useState } from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useLocation, useNavigate, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { formatISO } from 'date-fns'
import { sanitizeTagCategories } from 'helpers'
import styled from 'styled-components'

import ActionContainer from 'js/components/ActionContainer/ActionContainer'
import DeleteModal from 'js/components/EntityActions/DeleteModal'
import Errors from 'js/components/Errors'
import FormItem from 'js/components/Form/FormItem'
import { Container } from 'js/components/Grid/Grid'
import HideVisually from 'js/components/HideVisually/HideVisually'
import ConfirmationModal from 'js/components/Modal/ConfirmationModal'
import Modal from 'js/components/Modal/Modal'
import Spacer from 'js/components/Spacer/Spacer'
import Sticky from 'js/components/Sticky/Sticky'
import TabMenu from 'js/components/TabMenu'
import TabPage from 'js/components/TabPage'
import Text from 'js/components/Text/Text'
import NotFound from 'js/core/NotFound'
import { MAX_LENGTH } from 'js/lib/constants'
import useCustomTags from 'js/page/Widget/hooks/useCustomTags'

import Advanced from './Advanced/Advanced'
import ChangeRepeatingEvent from './components/ChangeRepeatingEvent'
import EventDates from './components/EventDates'
import RequiredFieldsMessage from './components/RequiredFieldsMessage'
import Suggested from './components/Suggested'
import General from './General/General'
import RecursiveAccessRightsModal from './RecursiveAccessRightsModal'
import Schedule from './Schedule/Schedule'
import Settings from './Settings/Settings'

const TabMenuWrapper = styled(Sticky)`
    position: relative;

    &[data-sticky='true'] {
        background: white;
        box-shadow: ${(p) => p.theme.shadow.soft.bottom};
        z-index: 1;
    }
`

const AddEditFormWrapper = ({ entity, ...rest }) => {
    const params = useParams()
    const currentGroupGuid = entity?.group?.guid || params.groupGuid

    const { data, error, loading } = useQuery(GET_DATA, {
        variables: {
            getGroup: !!currentGroupGuid,
            groupGuid: currentGroupGuid,
            subtype: rest.subtype,
        },
    })

    if (error || (entity && !entity.canEdit)) return <NotFound />

    if (loading) return null

    return <AddEditForm entity={entity} data={data} {...rest} />
}

const AddEditForm = ({
    data,
    entity,
    title,
    subtype,
    deleteTitle,
    refetchQueries,
    afterDelete,
    isSubEvent,
    defaultStartDate,
    onClose,
    mutation,
    mutationInputFilter,
    renderCustomFields = null,
    customDefaultValues = {},
    onMutateSuccess,
}) => {
    const { viewer, site, group } = data || {}
    const { recurringEventsEnabled } = site

    const params = useParams()
    const navigate = useNavigate()
    const location = useLocation()
    const refFeatured = useRef()

    const [showDeleteModal, setShowDeleteModal] = useState(false)
    const toggleDeleteModal = () => setShowDeleteModal(!showDeleteModal)

    const [mutate] = mutation

    const [tab, setTab] = useState('general')

    const { t } = useTranslation()

    const [mutationErrors, setErrors] = useState()

    const [suggestedItems, setSuggestedItems] = useState(
        entity?.suggestedItems || [],
    )

    const [startDate, setStartDate] = useState(
        entity?.startDate || defaultStartDate || formatISO(new Date()),
    )

    const [endDate, setEndDate] = useState(
        entity?.endDate !== entity?.startDate ? entity?.endDate : null,
    )

    const [categoryTags, setCategoryTags] = useState(
        entity?.tagCategories || [],
    )

    const { customTags, handleAddTags, handleRemoveTags, resetCustomTags } =
        useCustomTags(entity?.tags)

    const isPublished = entity?.statusPublished === 'published'

    const [timeScheduled, setTimeScheduled] = useState(
        isPublished ? null : entity?.timePublished || null,
    )

    const [ownerGuid, setOwnerGuid] = useState('')
    const [timePublished, setTimePublished] = useState()

    const [scheduleArchiveEntity, setScheduleArchiveEntity] = useState(
        entity?.scheduleArchiveEntity || '',
    )
    const [scheduleDeleteEntity, setScheduleDeleteEntity] = useState(
        entity?.scheduleDeleteEntity || '',
    )

    const [groupGuid, setGroupGuid] = useState(
        entity?.group?.guid || params.groupGuid || null,
    )

    const handleClose = () => {
        onClose
            ? onClose()
            : navigate(location?.state?.prevPathname || entity?.url || '/', {
                  replace: true,
              })
    }

    const [showChangeRepeatingEventModal, setShowChangeRepeatingEventModal] =
        useState(false)
    const toggleChangeRepeatingEventModal = () => {
        setShowChangeRepeatingEventModal(!showChangeRepeatingEventModal)
    }

    const [showPublishRequestModal, setShowPublishRequestModal] =
        useState(false)

    const [showRecursiveAccessModal, setShowRecursiveAccessModal] =
        useState(false)

    const handleSaveRepeatingEvent = ({ range }) => {
        setValue('rangeSettings.updateRange', range === 'following')
        publish()
    }

    const isRepeating = !!entity?.rangeSettings?.type && !entity?.hasChildren

    const defaultValues = {
        isDraft: false,
        title: entity?.title || null,
        abstract: entity?.abstract || null,
        richDescription: entity?.richDescription || null,
        backgroundColor: entity?.backgroundColor || '',
        source: entity?.source || null,
        location: entity?.location || null,
        locationAddress: entity?.locationAddress || null,
        locationLink: entity?.locationLink || null,
        maxAttendees: entity?.maxAttendees || null,
        rsvp: entity ? entity.rsvp : true, // Default to true
        enableMaybeAttendEvent: entity?.enableMaybeAttendEvent || false,
        attendEventOnline: entity?.attendEventOnline || false,
        videoCallEnabled: entity?.videoCallEnabled || false,
        qrAccess: entity?.qrAccess || false,
        ticketLink: entity?.ticketLink || null,
        attendEventWithoutAccount: entity?.attendEventWithoutAccount || false,
        isFeatured: entity?.isFeatured || false,
        isRecommended: entity?.isRecommended || false,
        isRecommendedInSearch: entity?.isRecommendedInSearch || false,
        attendeeWelcomeMailSubject: entity?.attendeeWelcomeMailSubject || null,
        attendeeWelcomeMailContent: entity?.attendeeWelcomeMailContent || null,
        repeat: isRepeating,
        rangeSettings: {
            type: entity?.rangeSettings?.type || 'daily',
            interval: entity?.rangeSettings?.interval || 1,
            repeatUntil: entity?.rangeSettings?.instanceLimit
                ? 'after'
                : entity?.rangeSettings?.repeatUntil || 'never',
            instanceLimit: entity?.rangeSettings?.instanceLimit || 12,
            updateRange: null,
        },
        inputLanguage: entity?.inputLanguage || site?.language,
        isTranslationEnabled: entity ? entity.isTranslationEnabled : true, // Default to true
        isAccessRecursive: false,
        accessId: entity?.accessId,
        writeAccessId: entity?.writeAccessId,
        isFormEnabled: entity?.isFormEnabled || false,
        form: entity?.form,
        ...customDefaultValues,
    }

    const getEventValues = ({
        location,
        locationAddress,
        locationLink,
        maxAttendees,
        rsvp,
        enableMaybeAttendEvent,
        attendEventOnline,
        videoCallEnabled,
        videoCallModeratorUserGuids,
        qrAccess,
        ticketLink,
        attendEventWithoutAccount,
        attendeeWelcomeMailSubject,
        attendeeWelcomeMailContent,
        repeat,
        rangeSettings,
        isFormEnabled,
        form,
    }) => {
        const { type, interval, repeatUntil, instanceLimit, updateRange } =
            rangeSettings

        return {
            location,
            locationAddress,
            locationLink,
            maxAttendees: maxAttendees ? maxAttendees.toString() : '',
            rsvp,
            enableMaybeAttendEvent,
            attendEventOnline,
            videoCallEnabled,
            videoCallModeratorUserGuids,
            qrAccess,
            ticketLink,
            attendEventWithoutAccount,
            startDate,
            endDate: endDate || startDate,
            attendeeWelcomeMailSubject,
            attendeeWelcomeMailContent,
            ...(repeat
                ? {
                      rangeSettings: {
                          type,
                          interval,
                          ...(['never', 'after'].indexOf(repeatUntil) === -1
                              ? { repeatUntil }
                              : {}),
                          ...(repeatUntil === 'after' ? { instanceLimit } : {}),
                          updateRange: updateRange ?? true,
                      },
                  }
                : {}),
            isFormEnabled,
            form,
        }
    }

    const formMethods = useForm({
        mode: 'onChange',
        defaultValues,
    })
    const {
        control,
        handleSubmit,
        setValue,
        getValues,
        formState: { dirtyFields, errors, isValid, isSubmitting },
    } = formMethods

    const onSubmit = async (values) => {
        const {
            title,
            abstract,
            richDescription,
            inputLanguage,
            isTranslationEnabled,
            accessId,
            writeAccessId,
            backgroundColor,
            source,
            isRecommended,
            isRecommendedInSearch,
            isFeatured,
            isDraft,
            isAccessRecursive,
        } = values

        const filterInput = (input, values) => {
            if (mutationInputFilter) {
                return mutationInputFilter(input, values, dirtyFields)
            }
            return input
        }

        const input = {
            ...(entity ? { guid: entity.guid } : { subtype }),
            groupGuid,
            ...(params.containerGuid
                ? { containerGuid: params.containerGuid }
                : {}),
            title,
            abstract,
            featured: refFeatured.current && refFeatured.current.getValue(),
            richDescription,
            tagCategories: sanitizeTagCategories(
                site.tagCategories,
                categoryTags,
            ),
            tags: customTags.toJS(),
            accessId: parseInt(accessId, 10),
            scheduleArchiveEntity: scheduleArchiveEntity || null,
            scheduleDeleteEntity: scheduleDeleteEntity || null,
            // Schedule or save as draft
            ...(isDraft ? { timePublished: timeScheduled || null } : {}),
            // Publish now
            ...(!isDraft && entity?.statusPublished === 'draft'
                ? {
                      timePublished: formatISO(new Date()),
                  }
                : {}),
            // Edit publish date
            ...(!isDraft && timePublished
                ? {
                      timePublished,
                  }
                : {}),
            ...(canAccess.writeAccess
                ? {
                      writeAccessId: parseInt(writeAccessId, 10),
                  }
                : {}),
            ...(subtype === 'wiki' && !!entity
                ? {
                      isAccessRecursive,
                  }
                : {}),
            ...(canAccess.backgroundColor ? { backgroundColor } : {}),
            ...(canAccess.source ? { source } : {}),
            ...(canAccess.isRecommended ? { isRecommended } : {}),
            ...(canAccess.isRecommendedInSearch
                ? { isRecommendedInSearch }
                : {}),
            ...(canAccess.isFeatured ? { isFeatured } : {}),
            ...(canAccess.suggested
                ? {
                      suggestedItems: suggestedItems.map(
                          (entity) => entity.guid,
                      ),
                  }
                : {}),
            ...(canAccess.eventDates ? getEventValues(values) : {}),
            ...(canAccess.advanced && ownerGuid ? { ownerGuid } : {}),
            inputLanguage,
            isTranslationEnabled,
        }

        setErrors()

        await mutate({
            variables: {
                input: filterInput(input, values),
            },
            refetchQueries,
        })
            .then(({ data }) => {
                onMutateSuccess(data)
            })
            .catch((errors) => {
                console.error(errors)
                setErrors(errors)
            })
    }

    const onError = (errors) => {
        return new Promise((resolve, reject) => {
            reject(new Error(errors))
        })
    }

    const saveAsDraft = () => {
        if (requiresContentModeration && timeScheduled) {
            setShowPublishRequestModal(true)
            return
        }

        setValue('isDraft', true)
        submitForm()
    }

    const publish = () => {
        if (
            entity?.rangeSettings?.type &&
            getValues('rangeSettings.updateRange') === null
        ) {
            toggleChangeRepeatingEventModal()
            return
        }

        if (requiresContentModeration) {
            setShowPublishRequestModal(true)
            return
        }

        if (subtype === 'wiki' && entity?.hasChildren) {
            if (dirtyFields.accessId || dirtyFields.writeAccessId) {
                setShowRecursiveAccessModal(true)
                return
            }
        }

        setValue('isDraft', false)
        submitForm()
    }

    const submitForm = handleSubmit(onSubmit, onError)

    React.useEffect(() => {
        if (entity) return // We don't want to override the entity tag data
        if (data?.group) {
            const { group } = data
            if (group.defaultTags) {
                resetCustomTags(group.defaultTags)
            }
            if (group.defaultTagCategories) {
                setCategoryTags(group.defaultTagCategories)
            }
        }
    }, [data, entity])

    const canSaveAsDraft =
        ['news', 'blog', 'page', 'wiki', 'podcasts'].includes(subtype) ||
        (subtype === 'event' && !isSubEvent)

    const {
        isAdmin,
        isSubEditor,
        requiresContentModeration,
        canUpdateAccessLevel,
        canInsertMedia,
    } = viewer

    const canAccess = {
        abstract: !(
            subtype === 'event' ||
            (subtype === 'news' && !window.__SETTINGS__.showExcerptInNewsCard)
        ),
        featured:
            [
                'blog',
                'discussion',
                'event',
                'news',
                'question',
                'wiki',
            ].includes(subtype) && canInsertMedia,
        eventDates: subtype === 'event',
        backgroundColor:
            ['news', 'blog'].includes(subtype) && (isAdmin || isSubEditor),
        source: subtype === 'news',
        isRecommended: subtype === 'blog' && (isAdmin || isSubEditor),
        isRecommendedInSearch: isAdmin,
        isFeatured: (isAdmin || isSubEditor) && !isSubEvent,
        readAccess: canUpdateAccessLevel && !isSubEvent,
        writeAccess: subtype === 'wiki',
        advanced: entity?.canEditAdvanced,
        suggested:
            site.showSuggestedItems &&
            [
                'news',
                'blog',
                'discussion',
                'question',
                'event',
                'wiki',
            ].includes(subtype),
    }

    const enableRepeating =
        recurringEventsEnabled && !isSubEvent && !entity?.hasChildren

    return (
        <ActionContainer
            title={title}
            timeScheduled={timeScheduled}
            canSaveAsDraft={canSaveAsDraft}
            saveAsDraft={saveAsDraft}
            publish={publish}
            onDelete={entity ? toggleDeleteModal : null}
            onClose={handleClose}
            isValid={isValid}
            isSavingAsDraft={isSubmitting && getValues('isDraft')}
            isPublishing={isSubmitting && !getValues('isDraft')}
            isPublished={isPublished}
            isChangingRepeatingEvent={entity?.rangeSettings?.type}
        >
            <FormProvider {...formMethods}>
                <form>
                    <HideVisually aria-hidden>
                        <FormItem
                            control={control}
                            type="switch"
                            name="isDraft"
                            tabIndex="-1"
                        />
                    </HideVisually>
                    <Container size="small" style={{ paddingBottom: '16px' }}>
                        <Spacer spacing="small">
                            <FormItem
                                control={control}
                                type="text"
                                name="title"
                                label={t('form.title')}
                                size="large"
                                errors={errors}
                                required
                                maxLength={MAX_LENGTH.title}
                            />

                            {canAccess.eventDates && (
                                <EventDates
                                    startDate={startDate}
                                    endDate={endDate}
                                    setStartDate={setStartDate}
                                    setEndDate={setEndDate}
                                    enableRepeating={enableRepeating}
                                    isRepeating={isRepeating}
                                />
                            )}

                            <Errors errors={mutationErrors} />
                        </Spacer>
                    </Container>
                    <TabMenuWrapper>
                        <Container size="small">
                            <TabMenu
                                label={t('global.settings')}
                                options={[
                                    {
                                        onClick: () => setTab('general'),
                                        label: t('global.general'),
                                        isActive: tab === 'general',
                                        id: 'tab-general',
                                        ariaControls: 'panel-general',
                                    },
                                    {
                                        onClick: () => setTab('settings'),
                                        label: t('global.settings'),
                                        isActive: tab === 'settings',
                                        id: 'tab-settings',
                                        ariaControls: 'panel-settings',
                                    },
                                    {
                                        onClick: () => setTab('schedule'),
                                        label: t('form.schedule'),
                                        isActive: tab === 'schedule',
                                        id: 'tab-schedule',
                                        ariaControls: 'panel-schedule',
                                    },
                                    canAccess.advanced
                                        ? {
                                              onClick: () => setTab('advanced'),
                                              label: t('global.advanced'),
                                              isActive: tab === 'advanced',
                                              id: 'tab-advanced',
                                              ariaControls: 'panel-advanced',
                                          }
                                        : {},
                                    canAccess.suggested
                                        ? {
                                              onClick: () =>
                                                  setTab('suggested'),
                                              label: t('global.suggested'),
                                              isActive: tab === 'suggested',
                                              id: 'tab-suggested',
                                              ariaControls: 'panel-suggested',
                                          }
                                        : {},
                                ]}
                                edgePadding
                                edgeMargin
                                showBorder
                                style={{ position: 'static' }}
                            />
                        </Container>
                    </TabMenuWrapper>

                    <TabPage
                        id="panel-general"
                        visible={tab === 'general'}
                        ariaLabelledby="tab-general"
                    >
                        <General
                            ref={refFeatured}
                            canAccess={canAccess}
                            entity={entity}
                            site={site}
                            viewer={viewer}
                            group={group}
                            subtype={subtype}
                            tags={categoryTags}
                            setTags={setCategoryTags}
                            customTags={customTags}
                            handleAddTags={handleAddTags}
                            handleRemoveTags={handleRemoveTags}
                            renderCustomFields={renderCustomFields}
                        />
                    </TabPage>

                    <TabPage
                        id="panel-settings"
                        visible={tab === 'settings'}
                        ariaLabelledby="tab-settings"
                    >
                        <Settings
                            canAccess={canAccess}
                            entity={entity}
                            site={site}
                        />
                    </TabPage>

                    <TabPage
                        id="panel-schedule"
                        visible={tab === 'schedule'}
                        ariaLabelledby="tab-schedule"
                    >
                        <Schedule
                            canSaveAsDraft={canSaveAsDraft}
                            isPublished={isPublished}
                            timeScheduled={timeScheduled}
                            setTimeScheduled={setTimeScheduled}
                            scheduleArchiveEntity={scheduleArchiveEntity}
                            setScheduleArchiveEntity={setScheduleArchiveEntity}
                            scheduleDeleteEntity={scheduleDeleteEntity}
                            setScheduleDeleteEntity={setScheduleDeleteEntity}
                        />
                    </TabPage>

                    {canAccess.advanced && (
                        <TabPage
                            id="panel-advanced"
                            visible={tab === 'advanced'}
                            ariaLabelledby="tab-advanced"
                        >
                            <Advanced
                                entity={entity}
                                subtype={subtype}
                                isPublished={isPublished}
                                timePublished={timePublished}
                                setTimePublished={setTimePublished}
                                ownerGuid={ownerGuid}
                                setOwnerGuid={setOwnerGuid}
                                groupGuid={groupGuid}
                                setGroupGuid={setGroupGuid}
                            />
                        </TabPage>
                    )}

                    {canAccess.suggested && (
                        <TabPage
                            id="panel-suggested"
                            visible={tab === 'suggested'}
                            ariaLabelledby="tab-suggested"
                        >
                            <Suggested
                                suggestedItems={suggestedItems}
                                setSuggestedItems={setSuggestedItems}
                            />
                        </TabPage>
                    )}

                    <Container size="small">
                        <RequiredFieldsMessage />
                    </Container>
                </form>

                <DeleteModal
                    isVisible={showDeleteModal}
                    onClose={toggleDeleteModal}
                    title={deleteTitle}
                    entity={entity}
                    afterDelete={afterDelete}
                    refetchQueries={refetchQueries}
                />

                <Modal
                    isVisible={showChangeRepeatingEventModal}
                    onClose={toggleChangeRepeatingEventModal}
                    title={t('entity-event.change-repeating-event')}
                    size="tiny"
                >
                    <ChangeRepeatingEvent
                        onClose={toggleChangeRepeatingEventModal}
                        onSubmit={handleSaveRepeatingEvent}
                        isSubmitting={isSubmitting}
                        range={dirtyFields.rangeSettings ? 'following' : 'this'}
                    />
                </Modal>

                <ConfirmationModal
                    isVisible={showPublishRequestModal}
                    onCancel={() => setShowPublishRequestModal(false)}
                    onConfirm={() => {
                        setShowPublishRequestModal(false)
                        setValue('isDraft', timeScheduled)
                        submitForm()
                    }}
                    title={t('action.publish')}
                    cancelLabel={t('action.cancel')}
                    confirmLabel={t('action.send-request')}
                >
                    <Spacer>
                        <Text size="small">
                            {t('content-moderation.send-request-helper')}
                        </Text>
                        <Text size="small">
                            {t('content-moderation.send-request-confirm')}
                        </Text>
                    </Spacer>
                </ConfirmationModal>

                <RecursiveAccessRightsModal
                    showRecursiveAccessModal={showRecursiveAccessModal}
                    onClose={() => setShowRecursiveAccessModal(false)}
                    onSubmit={() => {
                        setShowRecursiveAccessModal(false)
                        submitForm()
                    }}
                />
            </FormProvider>
        </ActionContainer>
    )
}

const GET_DATA = gql`
    query GetEntity($getGroup: Boolean!, $groupGuid: String, $subtype: String) {
        viewer {
            guid
            isAdmin
            isSubEditor
            canUpdateAccessLevel
            canInsertMedia(subtype: $subtype)
            requiresContentModeration(subtype: $subtype, groupGuid: $groupGuid)
            requiresCommentModeration(subtype: $subtype, groupGuid: $groupGuid)
        }
        site {
            guid
            customTagsAllowed
            tagCategories {
                name
                values
            }
            maxCharactersInAbstract
            showSuggestedItems
            recurringEventsEnabled
            contentTranslation
            language
            languageOptions {
                value
                label
            }
            integratedVideocallEnabled
        }
        group: entity(guid: $groupGuid) @include(if: $getGroup) {
            guid
            ... on Group {
                isClosed
                canEdit
                defaultTags
                defaultTagCategories {
                    name
                    values
                }
            }
        }
    }
`

export default AddEditFormWrapper
