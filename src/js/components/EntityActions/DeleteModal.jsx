import React from 'react'

import DeleteForm from 'js/components/Item/ItemActions/components/DeleteForm'
import Modal from 'js/components/Modal/Modal'

const DeleteModal = ({
    isVisible,
    entity,
    title,
    refetchQueries,
    message,
    onClose,
    beforeDelete,
    afterDelete,
}) => (
    <Modal isVisible={isVisible} onClose={onClose} title={title} size="small">
        <DeleteForm
            entity={entity}
            refetchQueries={refetchQueries}
            beforeDelete={beforeDelete}
            afterDelete={afterDelete}
            message={message}
            onClose={onClose}
        />
    </Modal>
)

export default DeleteModal
