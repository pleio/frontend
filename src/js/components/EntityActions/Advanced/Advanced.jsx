import React from 'react'

import SettingContainer from 'js/admin/layout/SettingContainer'
import DateTimeField from 'js/components/DateTimeField'
import { Col, Container, Row } from 'js/components/Grid/Grid'
import Section from 'js/components/Section/Section'

import GroupGuidField from './components/GroupGuidField'
import UserField from './components/UserField'

const Advanced = ({
    entity,
    isPublished,
    timePublished,
    setTimePublished,
    ownerGuid,
    setOwnerGuid,
    groupGuid,
    setGroupGuid,
}) => {
    const canChangeGroup = entity?.canEditGroup

    return (
        <Container size="small">
            <Section style={{ paddingTop: '24px' }}>
                <Row $gutter={30}>
                    <Col tabletUp={1 / 2}>
                        <SettingContainer
                            subtitle="global.owner"
                            htmlFor="ownerGuid"
                            inputWidth="full"
                            $mb
                        >
                            <UserField
                                name="ownerGuid"
                                value={ownerGuid}
                                defaultValue={{
                                    value: entity.owner.guid,
                                    label: entity.owner.name,
                                }}
                                onChange={(user) =>
                                    setOwnerGuid(user?.guid || '')
                                }
                                groupGuid={entity?.group?.guid}
                            />
                        </SettingContainer>
                    </Col>
                    {isPublished && (
                        <Col tabletUp={1 / 2}>
                            <SettingContainer
                                subtitle="global.published"
                                inputWidth="full"
                                $mb
                            >
                                <DateTimeField
                                    name="timePublished"
                                    value={
                                        timePublished || entity.timePublished
                                    }
                                    onChange={setTimePublished}
                                    isClearable
                                />
                            </SettingContainer>
                        </Col>
                    )}
                    <Col tabletUp={1 / 2}>
                        {canChangeGroup ? (
                            <GroupGuidField
                                value={groupGuid}
                                onChange={setGroupGuid}
                            />
                        ) : null}
                    </Col>
                </Row>
            </Section>
        </Container>
    )
}

export default Advanced
