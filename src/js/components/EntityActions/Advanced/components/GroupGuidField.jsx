import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useLazyQuery, useQuery } from '@apollo/client'
import { useDebounce, useIsMount } from 'helpers'

import SettingContainer from 'js/admin/layout/SettingContainer'
import Select from 'js/components/Select/Select'

const GET_GROUP_NAME = gql`
    query GroupFieldEntity($guid: String!) {
        entity(guid: $guid) {
            guid
            ... on Group {
                name
            }
        }
    }
`

const GET_GROUPS = gql`
    query GroupsQuery($q: String!) {
        groups(q: $q) {
            edges {
                guid
                name
            }
        }
    }
`

const GroupGuidField = ({ value, onChange }) => {
    const handleChange = (val) => {
        onChange(val)
    }

    const { data: groupData, loading: groupLoading } = useQuery(
        GET_GROUP_NAME,
        {
            variables: {
                guid: value,
            },
            skip: !value,
        },
    )

    const [findGroup, { loading, data }] = useLazyQuery(GET_GROUPS)

    const [inputVal, setInputVal] = useState('')
    const handleInputChange = (val) => setInputVal(val)

    const debouncedVal = useDebounce(inputVal, 200)
    const isMount = useIsMount()

    useEffect(() => {
        if (!isMount && debouncedVal !== '') {
            findGroup({
                variables: {
                    q: debouncedVal,
                },
            })
        }
    }, [debouncedVal])

    const { t } = useTranslation()

    const defaultOptions =
        !groupLoading && groupData?.entity
            ? [
                  {
                      value: groupData.entity.guid,
                      label: groupData.entity.name,
                  },
              ]
            : []

    return (
        <SettingContainer
            subtitle="entity-group.content-name"
            htmlFor="groupGuid"
            inputWidth="full"
            $mb
        >
            <Select
                name="groupGuid"
                isSearchable
                isClearable
                isLoading={loading}
                value={value}
                options={
                    data && debouncedVal !== ''
                        ? data.groups.edges.map(({ guid, name }) => ({
                              value: guid,
                              label: name,
                          }))
                        : defaultOptions
                }
                onChangeSearch={handleInputChange}
                placeholder={t('entity-group.search')}
                noOptionsMessage={!inputVal && t('form.type-to-search')}
                onChange={handleChange}
            />
        </SettingContainer>
    )
}

export default GroupGuidField
