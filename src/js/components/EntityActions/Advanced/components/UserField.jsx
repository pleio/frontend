import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useLazyQuery } from '@apollo/client'
import { useDebounce, useIsMount } from 'helpers'

import Select from 'js/components/Select/Select'

const USERSQUERY = gql`
    query UsersQuery($q: String!, $groupGuid: String) {
        siteUsers(q: $q, groupGuid: $groupGuid) {
            edges {
                guid
                name
                email
                icon
            }
        }
    }
`

const UserField = ({
    size,
    placeholder,
    value,
    excludeGuids = [],
    onChange,
    groupGuid = null,
    ...rest
}) => {
    const [findUser, { loading, data }] = useLazyQuery(USERSQUERY)
    const [inputVal, setInputVal] = useState('')
    const debouncedVal = useDebounce(inputVal, 200)
    const isMount = useIsMount()

    useEffect(() => {
        if (!isMount) {
            findUser({
                variables: {
                    q: debouncedVal || null, // null resets to default
                    groupGuid,
                },
            })
        }
    }, [debouncedVal])

    const { t } = useTranslation()

    return (
        <Select
            size={size}
            isSearchable
            isClearable={!!value}
            isLoading={loading}
            value={value}
            options={
                data
                    ? data.siteUsers.edges
                          .filter((el) => !excludeGuids.includes(el.guid))
                          .map(({ guid, name }) => ({
                              value: guid,
                              label: name,
                          }))
                    : []
            }
            onChangeSearch={setInputVal}
            placeholder={placeholder || t('global.search-user')}
            searchPlaceholder={t('global.search-user')}
            noOptionsMessage={!inputVal && t('form.type-to-search')}
            onChange={(val) => {
                onChange(data.siteUsers.edges.find((user) => user.guid === val))
            }}
            {...rest}
        />
    )
}

export default UserField
