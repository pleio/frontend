import React from 'react'
import { useNavigate } from 'react-router-dom'
import { gql, useMutation } from '@apollo/client'

import { blogEditFragment } from 'js/blog/fragments/entity'
import { discussionEditFragment } from 'js/discussions/fragments/entity'
import { eventEditFragment } from 'js/events/fragments/entity'
import { newsEditFragment } from 'js/news/fragments/entity'
import { questionEditFragment } from 'js/questions/fragments/entity'
import taskFragment from 'js/tasks/fragments/entity'
import { wikiEditFragment } from 'js/wiki/fragments/entity'

import AddEditForm from './AddEditForm'

const EntityAddEditForm = ({
    entity,
    title,
    subtype,
    deleteTitle,
    refetchQueries,
    afterDelete,
    isSubEvent,
    defaultStartDate,
    onClose,
}) => {
    const mutation = useMutation(entity ? EDITMUTATION : ADDMUTATION)
    const navigate = useNavigate()

    const onMutateSuccess = (data) => {
        if (subtype === 'task') {
            navigate(-1)
        } else if (entity) {
            navigate(
                location?.state?.prevPathname || data.editEntity.entity.url,
                { replace: true },
            )
        } else {
            navigate(
                isSubEvent
                    ? data.addEntity.entity.parent.url
                    : data.addEntity.entity.url,
                { replace: true },
            )
        }
    }

    return (
        <AddEditForm
            entity={entity}
            title={title}
            subtype={subtype}
            deleteTitle={deleteTitle}
            refetchQueries={refetchQueries}
            afterDelete={afterDelete}
            isSubEvent={isSubEvent}
            defaultStartDate={defaultStartDate}
            onClose={onClose}
            mutation={mutation}
            onMutateSuccess={onMutateSuccess}
        />
    )
}

const addEntityFragment = `
    title
    url
`

export const ADDMUTATION = gql`
    mutation addEntity($input: addEntityInput!) {
        addEntity(input: $input) {
            entity {
                guid
                ... on News {
                    ${addEntityFragment}
                }
                ... on Blog {
                    ${addEntityFragment}
                }
                ... on Discussion {
                    ${addEntityFragment}
                }
                ... on Question {
                    ${addEntityFragment}
                }
                ... on Event {
                    ${addEntityFragment}
                    parent {
                        guid
                        url
                    }
                }
                ... on Task {
                    ${addEntityFragment}
                }
                ... on Wiki {
                    ${addEntityFragment}
                }
            }
        }
    }
`

const EDITMUTATION = gql`
    mutation editEntity($input: editEntityInput!) {
        editEntity(input: $input) {
            entity {
                guid
                ${newsEditFragment}
                ${blogEditFragment}
                ${discussionEditFragment}
                ${wikiEditFragment}
                ${questionEditFragment}
                ${eventEditFragment}
                ${taskFragment}
            }
        }
    }
`

export default EntityAddEditForm
