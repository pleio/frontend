import React from 'react'
import { useFormContext } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'

import AnimatePresence from 'js/components/AnimatePresence/AnimatePresence'
import AccessField from 'js/components/Form/AccessField'
import FormItem from 'js/components/Form/FormItem'
import { Col, Container, Row } from 'js/components/Grid/Grid'
import Section from 'js/components/Section/Section'
import Spacer from 'js/components/Spacer/Spacer'

import AttendanceForm from './components/AttendanceForm'
import VideoCallField from './components/VideoCallField'

const Settings = ({ canAccess, site, entity }) => {
    const { t } = useTranslation()
    const params = useParams()

    const { control, watch } = useFormContext()

    const { integratedVideocallEnabled } = site
    const { group, videoCallModerators } = entity || {}
    const groupGuid = params.groupGuid || group?.guid

    const attendEnabled =
        watch('rsvp') ||
        (!group?.isClosed && watch('attendEventWithoutAccount'))

    return (
        <Container size="small">
            <Section style={{ paddingTop: '24px' }}>
                <Row $spacing={20} $gutter={30}>
                    <Col mobileLandscapeUp={1 / 2}>
                        <Spacer spacing="small">
                            {canAccess.readAccess && (
                                <AccessField
                                    control={control}
                                    name="accessId"
                                    label={t('form.visible')}
                                    containerGuid={groupGuid}
                                />
                            )}

                            {canAccess.eventDates && (
                                <>
                                    <FormItem
                                        control={control}
                                        type="switch"
                                        prependField
                                        name="rsvp"
                                        size="small"
                                        title={t('entity-event.attend')}
                                        helper={t('entity-event.attend-helper')}
                                    />

                                    {!group?.isClosed && (
                                        <FormItem
                                            control={control}
                                            type="switch"
                                            prependField
                                            name="attendEventWithoutAccount"
                                            size="small"
                                            title={t(
                                                'entity-event.attend-with-email',
                                            )}
                                            helper={t(
                                                'entity-event.attend-with-email-helper',
                                            )}
                                        />
                                    )}

                                    <AnimatePresence visible={attendEnabled}>
                                        <Spacer spacing="small">
                                            <FormItem
                                                control={control}
                                                type="switch"
                                                prependField
                                                name="attendEventOnline"
                                                size="small"
                                                title={t(
                                                    'entity-event.attend-online',
                                                )}
                                                helper={t(
                                                    'entity-event.attend-online-helper',
                                                )}
                                            />

                                            <FormItem
                                                control={control}
                                                type="switch"
                                                prependField
                                                name="enableMaybeAttendEvent"
                                                size="small"
                                                title={t(
                                                    'entity-event.attend-maybe',
                                                )}
                                                helper={t(
                                                    'entity-event.attend-maybe-helper',
                                                )}
                                            />

                                            <AttendanceForm />
                                        </Spacer>
                                    </AnimatePresence>
                                </>
                            )}
                        </Spacer>
                    </Col>
                    <Col mobileLandscapeUp={1 / 2}>
                        <Spacer spacing="small">
                            {canAccess.writeAccess && (
                                <AccessField
                                    control={control}
                                    name="writeAccessId"
                                    label={t('form.editable')}
                                    containerGuid={groupGuid}
                                />
                            )}

                            {canAccess.eventDates && (
                                <>
                                    {integratedVideocallEnabled && (
                                        <VideoCallField
                                            videoCallModerators={
                                                videoCallModerators
                                            }
                                        />
                                    )}
                                    <FormItem
                                        control={control}
                                        type="switch"
                                        prependField
                                        name="qrAccess"
                                        size="small"
                                        title={t('entity-event.qr-code')}
                                        helper={t(
                                            'entity-event.qr-access-helper',
                                        )}
                                    />
                                </>
                            )}

                            {canAccess.isFeatured && (
                                <FormItem
                                    control={control}
                                    type="switch"
                                    prependField
                                    name="isFeatured"
                                    size="small"
                                    title={t('form.featured')}
                                    helper={t('form.featured-helper', {
                                        featuredWidget: t(
                                            'widget-featured.title',
                                        ),
                                        objectsWidget: t(
                                            'widget-objects.title',
                                        ),
                                        featuredSetting: t('form.featured'),
                                    })}
                                />
                            )}

                            {canAccess.isRecommended && (
                                <FormItem
                                    control={control}
                                    type="switch"
                                    prependField
                                    name="isRecommended"
                                    size="small"
                                    title={t('entity-blog.recommended')}
                                    helper={t('entity-blog.recommended-helper')}
                                />
                            )}

                            {canAccess.isRecommendedInSearch && (
                                <FormItem
                                    control={control}
                                    prependField
                                    type="switch"
                                    name="isRecommendedInSearch"
                                    size="small"
                                    title={t('form.recommended-search-result')}
                                    helper={t(
                                        'form.recommended-search-result-helper',
                                    )}
                                />
                            )}
                        </Spacer>
                    </Col>
                </Row>
            </Section>
        </Container>
    )
}

export default Settings
