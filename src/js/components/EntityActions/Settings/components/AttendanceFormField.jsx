import React from 'react'
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd'
import { useTranslation } from 'react-i18next'
import { media } from 'helpers'
import styled, { css } from 'styled-components'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import { Col, Row } from 'js/components/Grid/Grid'
import { H5 } from 'js/components/Heading'
import IconButton from 'js/components/IconButton/IconButton'
import Spacer from 'js/components/Spacer/Spacer'
import Text from 'js/components/Text/Text'
import sortByProperty from 'js/lib/helpers/sortByProperty'

import RemoveIcon from 'icons/delete-small.svg'
import EditIcon from 'icons/edit-small.svg'
import MoveIcon from 'icons/move-vertical.svg'

import AttendanceFormOption from './AttendanceFormOption'

const Wrapper = styled.li`
    font-size: ${(p) => p.theme.font.size.small};
    line-height: ${(p) => p.theme.font.lineHeight.small};
    color: ${(p) => p.theme.color.text.black};
    background-color: white;
    border-bottom: 1px solid ${(p) => p.theme.color.grey[20]};

    &:first-child {
        border-top: 1px solid ${(p) => p.theme.color.grey[20]};
    }

    ${(p) =>
        p.$isDragging &&
        css`
            box-shadow: ${p.theme.shadow.soft.center};
            border-bottom-color: transparent;
        `};

    .LinkListEditLinkLabel {
        flex-grow: 1;

        .TextFieldInput {
            padding-left: 0;
        }
    }

    .LinkListEditLinkProperties {
        padding-top: 8px;
        padding-bottom: 16px;

        ${media.mobileLandscapeDown`
            padding-left: ${(p) => p.theme.padding.horizontal.small};
            padding-right: ${(p) => p.theme.padding.horizontal.small};
        `};

        ${media.tabletUp`
            padding-left: ${(p) => p.theme.padding.horizontal.normal};
            padding-right: ${(p) => p.theme.padding.horizontal.normal};
        `};
    }

    .AttendanceFormFieldItemText {
        flex-grow: 1;
        overflow: hidden;
        white-space: nowrap;
    }

    .AttendanceFormFieldItemTextTitle {
        text-overflow: ellipsis;
        overflow: hidden;
    }
`

const AttendanceFormField = ({
    id,
    index,
    control,
    watchFields,
    setValue,
    onRemove,
}) => {
    const { t } = useTranslation()

    const { title, fieldType, fieldOptions, isEditing } =
        watchFields?.[index] ?? {}

    const toggleIsEditing = () => {
        setValue(`fields.${index}.isEditing`, !isEditing)
    }

    const handleAddOption = () => {
        setValue(`fields.${index}.fieldOptions`, [...fieldOptions, ''])
    }

    const handleRemoveOption = (optionIndex) => {
        const optionsCopy = [...fieldOptions]
        optionsCopy.splice(optionIndex, 1)
        setValue(`fields.${index}.fieldOptions`, optionsCopy)
    }

    const onDragEnd = (result) => {
        if (!result.destination) return

        const sourcePosition = result.source.index
        const destinationPosition = result.destination.index

        const newOptions = [...fieldOptions]
        const [movedOption] = newOptions.splice(sourcePosition, 1)
        newOptions.splice(destinationPosition, 0, movedOption)

        setValue(`fields.${index}.fieldOptions`, newOptions)
    }

    const buttonProps = {
        type: 'button',
        size: 'large',
        variant: 'secondary',
        hoverSize: 'normal',
        radiusStyle: 'rounded',
    }

    const types = [
        'textField',
        'selectField',
        'dateField',
        'htmlField',
        'multiSelectField',
        'dateTimeField',
        'radioField',
        'checkboxField',
    ]
    const typeOptions = types
        .map((type) => ({
            value: type,
            label: t(`entity-event.attendance-form-field.${type}`),
        }))
        .sort(sortByProperty('label'))

    const typesWithOptions = [
        'selectField',
        'multiSelectField',
        'radioField',
        'checkboxField',
    ]
    const hasOptions = typesWithOptions.includes(fieldType)

    const displayTitle = title || t('entity-event.new-field')
    const displayType = typeOptions.find(
        (option) => option.value === fieldType,
    )?.label
    const ariaLabel = `${index + 1}. ${displayType}: ${displayTitle}`

    return (
        <Draggable draggableId={id} index={index}>
            {(provided, snapshot) => (
                <Wrapper
                    ref={provided.innerRef}
                    $isDragging={snapshot.isDragging}
                    {...provided.draggableProps}
                >
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        <IconButton
                            {...buttonProps}
                            as="div"
                            tooltip={t('action.move')}
                            placement="left"
                            aria-label={t('aria.move-field', {
                                label: ariaLabel,
                            })}
                            {...provided.dragHandleProps}
                        >
                            <MoveIcon />
                        </IconButton>

                        <Flexer
                            gutter="small"
                            justifyContent="flex-start"
                            className="AttendanceFormFieldItemText"
                        >
                            <Text variant="grey" size="small">
                                {displayType}
                            </Text>
                            <Text
                                size="small"
                                className="AttendanceFormFieldItemTextTitle"
                            >
                                {displayTitle}
                            </Text>
                        </Flexer>

                        <Flexer gutter="tiny" divider="normal">
                            <IconButton
                                {...buttonProps}
                                onClick={toggleIsEditing}
                                aria-pressed={isEditing}
                                aria-label={t('aria.edit-field', {
                                    label: ariaLabel,
                                })}
                                tooltip={t('action.edit')}
                                placement="left"
                            >
                                <EditIcon />
                            </IconButton>
                            <IconButton
                                {...buttonProps}
                                onClick={onRemove}
                                aria-label={t('aria.remove-field', {
                                    label: ariaLabel,
                                })}
                                tooltip={t('action.delete')}
                                placement="right"
                            >
                                <RemoveIcon />
                            </IconButton>
                        </Flexer>
                    </div>

                    {isEditing && (
                        <Spacer
                            spacing="small"
                            className="LinkListEditLinkProperties"
                        >
                            <Row $growContent>
                                <Col mobileLandscapeUp={1 / 2}>
                                    <FormItem
                                        control={control}
                                        type="select"
                                        name={`fields.${index}.fieldType`}
                                        label={t('global.type')}
                                        options={typeOptions}
                                    />
                                </Col>
                                <Col mobileLandscapeUp={1 / 2}>
                                    <FormItem
                                        control={control}
                                        type="checkbox"
                                        name={`fields.${index}.isMandatory`}
                                        label={t('global.required')}
                                        size="small"
                                        style={{ justifyContent: 'center' }}
                                    />
                                </Col>
                            </Row>

                            <FormItem
                                control={control}
                                type="text"
                                name={`fields.${index}.title`}
                                label={t('form.title')}
                            />

                            <FormItem
                                control={control}
                                type="textarea"
                                name={`fields.${index}.description`}
                                label={t('form.description')}
                                size="small"
                            />

                            {hasOptions && (
                                <>
                                    <H5 style={{ margin: '20px 0 8px' }}>
                                        {t('entity-event.options', {
                                            count: fieldOptions.length,
                                        })}
                                    </H5>
                                    <DragDropContext onDragEnd={onDragEnd}>
                                        <Droppable
                                            droppableId={`attendance-form-field-${index}-options`}
                                        >
                                            {(provided) => (
                                                <ul
                                                    {...provided.droppableProps}
                                                    ref={provided.innerRef}
                                                >
                                                    {fieldOptions.map(
                                                        (
                                                            value,
                                                            optionIndex,
                                                        ) => {
                                                            const optionId = `${id}-${optionIndex}`
                                                            return (
                                                                <AttendanceFormOption
                                                                    key={
                                                                        optionId
                                                                    }
                                                                    id={
                                                                        optionId
                                                                    }
                                                                    fieldIndex={
                                                                        index
                                                                    }
                                                                    index={
                                                                        optionIndex
                                                                    }
                                                                    control={
                                                                        control
                                                                    }
                                                                    value={
                                                                        value
                                                                    }
                                                                    buttonProps={
                                                                        buttonProps
                                                                    }
                                                                    onRemove={
                                                                        handleRemoveOption
                                                                    }
                                                                />
                                                            )
                                                        },
                                                    )}
                                                    {provided.placeholder}
                                                </ul>
                                            )}
                                        </Droppable>
                                    </DragDropContext>
                                    <Flexer justifyContent="flex-start">
                                        <Button
                                            variant="tertiary"
                                            size="normal"
                                            onClick={handleAddOption}
                                            style={{ marginLeft: '40px' }}
                                        >
                                            {t('form.add-option')}
                                        </Button>
                                    </Flexer>
                                </>
                            )}
                        </Spacer>
                    )}
                </Wrapper>
            )}
        </Draggable>
    )
}

export default AttendanceFormField
