import React, { useState } from 'react'
import { DragDropContext, Droppable } from 'react-beautiful-dnd'
import {
    useFieldArray,
    useForm,
    useFormContext,
    useWatch,
} from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { media } from 'helpers'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import { H4 } from 'js/components/Heading'
import IconButton from 'js/components/IconButton/IconButton'
import Modal from 'js/components/Modal/Modal'
import Spacer from 'js/components/Spacer/Spacer'
import AttendFormModal from 'js/events/EventAttend/components/AttendFormModal'
import { MAX_LENGTH } from 'js/lib/constants'

import SettingsIcon from 'icons/settings-small.svg'

import AttendanceFormField from './AttendanceFormField'

const Wrapper = styled.div`
    .FormContent {
        ${media.mobileLandscapeDown`
            padding: ${(p) =>
                `${p.theme.padding.vertical.small} ${p.theme.padding.horizontal.small}`};
        `};

        ${media.tabletUp`
            padding: ${(p) =>
                `${p.theme.padding.vertical.normal} ${p.theme.padding.horizontal.normal}`};
        `};
    }
`

const AttendanceForm = () => {
    const { t } = useTranslation()

    const [showSettingsModal, setShowSettingsModal] = useState(false)

    const {
        control: rootControl,
        getValues: getRootValues,
        setValue: setRootValue,
    } = useFormContext()

    const defaultValues = {
        title: getRootValues('form.title'),
        description: getRootValues('form.description'),
        fields: getRootValues('form.fields'),
    }

    const {
        control,
        setValue,
        handleSubmit,
        reset,
        getValues,
        formState: { errors, isValid, isSubmitting },
    } = useForm({
        defaultValues,
    })

    const onSubmit = ({ title, description, fields }) => {
        const form = {
            title,
            description,
            fields: fields?.map(
                ({
                    guid,
                    title,
                    description,
                    fieldType,
                    isMandatory,
                    fieldOptions,
                }) => ({
                    guid,
                    title,
                    description,
                    fieldType,
                    isMandatory,
                    fieldOptions: fieldOptions.filter((option) => option), // Filter out empty values
                }),
            ),
        }
        setRootValue('form', form)
        setRootValue('isFormEnabled', fields?.length > 0)
        setShowSettingsModal(false)
    }

    const submitForm = handleSubmit(onSubmit)

    const { fields, append, remove, move } = useFieldArray({
        control,
        name: 'fields',
    })

    const handleCloseModal = () => {
        setShowSettingsModal(false)
        reset(defaultValues)
    }

    const [showPreviewForm, setShowPreviewForm] = useState(false)
    const values = getValues()

    const onDragEnd = (result) => {
        if (!result.destination) return
        move(result.source.index, result.destination.index)
    }

    const watchFields = useWatch({
        name: 'fields',
        control,
    })

    const handleAddField = () => {
        append({
            title: '',
            description: '',
            fieldType: 'textField',
            isMandatory: false,
            fieldOptions: [],
            isEditing: true, // Used as flag to show/hide field settings
        })
    }

    return (
        <>
            <FormItem
                control={rootControl}
                type="switch"
                prependField
                name="isFormEnabled"
                size="small"
                title={t('entity-event.attendance-form')}
                titleAppend={
                    <IconButton
                        size="small"
                        variant="secondary"
                        style={{
                            marginLeft: '2px',
                        }}
                        onClick={() => setShowSettingsModal(true)}
                    >
                        <SettingsIcon />
                    </IconButton>
                }
                helper={t('entity-event.attendance-form-helper')}
                onChange={({ target }) => {
                    if (target.checked) {
                        if (fields?.length > 0) {
                            setRootValue('isFormEnabled', true)
                        } else {
                            setShowSettingsModal(target.checked)
                        }
                    } else {
                        setRootValue('isFormEnabled', false)
                    }
                }}
            />

            <Modal
                size="normal"
                isVisible={showSettingsModal}
                title={t('entity-event.attendance-form')}
                onClose={handleCloseModal}
                noPadding
            >
                <Wrapper>
                    <div className="FormContent" style={{ paddingBottom: 0 }}>
                        <Spacer spacing="small">
                            <FormItem
                                control={control}
                                type="text"
                                name="title"
                                label={t('form.title')}
                                size="large"
                                errors={errors}
                                required
                                maxLength={MAX_LENGTH.title}
                            />

                            <FormItem
                                control={control}
                                type="textarea"
                                name="description"
                                label={t('form.description')}
                                size="small"
                                errors={errors}
                            />
                        </Spacer>

                        <H4 style={{ margin: '20px 0 8px' }}>
                            {t('entity-event.fields', {
                                count: fields.length,
                            })}
                        </H4>
                    </div>

                    <DragDropContext onDragEnd={onDragEnd}>
                        <Droppable droppableId="attendance-form-fields">
                            {(provided) => (
                                <ul
                                    {...provided.droppableProps}
                                    ref={provided.innerRef}
                                >
                                    {fields.map(({ id }, index) => (
                                        <AttendanceFormField
                                            key={id}
                                            id={id}
                                            index={index}
                                            control={control}
                                            watchFields={watchFields}
                                            setValue={setValue}
                                            onRemove={() => remove(index)}
                                        />
                                    ))}
                                    {provided.placeholder}
                                </ul>
                            )}
                        </Droppable>
                    </DragDropContext>

                    <div className="FormContent">
                        <Flexer justifyContent="flex-start">
                            <Button
                                variant="tertiary"
                                size="normal"
                                onClick={handleAddField}
                            >
                                {t('form.add-field')}
                            </Button>
                        </Flexer>

                        <Flexer mt>
                            <Button
                                variant="tertiary"
                                size="normal"
                                onClick={handleCloseModal}
                            >
                                {t('action.cancel')}
                            </Button>
                            <Button
                                size="normal"
                                variant="secondary"
                                onClick={() => setShowPreviewForm(true)}
                            >
                                {t('action.preview')}
                            </Button>
                            <AttendFormModal
                                form={values}
                                isVisible={showPreviewForm}
                                onClose={() => setShowPreviewForm(false)}
                            />

                            <Button
                                size="normal"
                                variant="primary"
                                disabled={!isValid}
                                loading={isSubmitting}
                                onClick={submitForm}
                            >
                                {t('action.save')}
                            </Button>
                        </Flexer>
                    </div>
                </Wrapper>
            </Modal>
        </>
    )
}

export default AttendanceForm
