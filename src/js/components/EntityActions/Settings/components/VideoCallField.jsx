import React, { useState } from 'react'
import { useFormContext } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { produce } from 'immer'
import PropTypes from 'prop-types'

import AddUserField from 'js/components/AddUserField'
import Button from 'js/components/Button/Button'
import EntityList from 'js/components/EntityList/EntityList'
import EntityListItem from 'js/components/EntityList/EntityListItem'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import IconButton from 'js/components/IconButton/IconButton'
import Modal from 'js/components/Modal/Modal'
import Setting from 'js/components/Setting'
import Spacer from 'js/components/Spacer/Spacer'

import RemoveIcon from 'icons/cross.svg'
import SettingsIcon from 'icons/settings-small.svg'

const VideoCallField = ({ videoCallModerators }) => {
    const { t } = useTranslation()

    const { control, setValue } = useFormContext()

    const [showSettingsModal, setShowSettingsModal] = useState(false)
    const [moderators, setModerators] = useState(videoCallModerators || [])

    return (
        <>
            <FormItem
                control={control}
                type="switch"
                prependField
                name="videoCallEnabled"
                size="small"
                title={t('entity-event.video-call')}
                titleAppend={
                    <IconButton
                        size="small"
                        variant="secondary"
                        style={{
                            marginLeft: '2px',
                        }}
                        onClick={() => setShowSettingsModal(true)}
                    >
                        <SettingsIcon />
                    </IconButton>
                }
                helper={t('entity-event.video-call-helper')}
            />

            <Modal
                size="small"
                isVisible={showSettingsModal}
                title={t('global.video-call')}
                onClose={() => setShowSettingsModal(false)}
                containHeight
            >
                <Spacer
                    spacing="small"
                    style={{
                        // Needed to show scrollbar if list is too long
                        display: 'flex',
                        flexDirection: 'column',
                        overflow: 'hidden',
                    }}
                >
                    <Setting
                        subtitle="entity-event.video-call-moderators"
                        helper="entity-event.video-call-moderators-helper"
                    />

                    <AddUserField
                        label={t('entity-event.add-moderator')}
                        users={moderators}
                        onAddUser={(user) =>
                            setModerators(
                                produce((newState) => {
                                    newState.push(user)
                                }),
                            )
                        }
                    />

                    {moderators.length > 0 && (
                        <EntityList
                            $showBorder
                            style={{
                                // Show scrollbar if list is too long
                                overflowY: 'auto',
                            }}
                        >
                            {moderators.map((user, index) => (
                                <EntityListItem
                                    key={user.guid}
                                    entity={user}
                                    type="user"
                                >
                                    <IconButton
                                        size="large"
                                        variant="secondary"
                                        onClick={() =>
                                            setModerators(
                                                produce((newState) => {
                                                    newState.splice(index, 1)
                                                }),
                                            )
                                        }
                                        tooltip={t('action.remove')}
                                        aria-label={t(
                                            'entity-event.remove-moderator',
                                        )}
                                    >
                                        <RemoveIcon />
                                    </IconButton>
                                </EntityListItem>
                            ))}
                        </EntityList>
                    )}
                </Spacer>

                <Flexer mt>
                    <Button
                        variant="tertiary"
                        size="normal"
                        onClick={() => setShowSettingsModal(false)}
                    >
                        {t('action.cancel')}
                    </Button>
                    <Button
                        size="normal"
                        variant="primary"
                        onClick={() => {
                            setValue('videoCallEnabled', true)
                            setValue(
                                'videoCallModeratorUserGuids',
                                moderators.map((user) => user.guid),
                            )
                            setShowSettingsModal(false)
                        }}
                    >
                        {t('action.save')}
                    </Button>
                </Flexer>
            </Modal>
        </>
    )
}

VideoCallField.propTypes = {
    videoCallModerators: PropTypes.array,
}

export default VideoCallField
