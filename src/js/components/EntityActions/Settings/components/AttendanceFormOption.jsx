import React from 'react'
import { Draggable } from 'react-beautiful-dnd'
import { useTranslation } from 'react-i18next'
import { media } from 'helpers'
import styled from 'styled-components'

import FormItem from 'js/components/Form/FormItem'
import IconButton from 'js/components/IconButton/IconButton'

import CrossIcon from 'icons/cross.svg'
import MoveIcon from 'icons/move-vertical.svg'

const Wrapper = styled.li`
    margin-bottom: 12px;

    .LinkListEditLinkLabel {
        flex-grow: 1;

        .TextFieldInput {
            padding-left: 0;
        }
    }

    .LinkListEditLinkProperties {
        padding-top: 8px;
        padding-bottom: 16px;

        ${media.mobileLandscapeDown`
            padding-left: ${(p) => p.theme.padding.horizontal.small};
            padding-right: ${(p) => p.theme.padding.horizontal.small};
        `};

        ${media.tabletUp`
            padding-left: ${(p) => p.theme.padding.horizontal.normal};
            padding-right: ${(p) => p.theme.padding.horizontal.normal};
        `};
    }

    .AttendanceFormFieldItemText {
        flex-grow: 1;
        overflow: hidden;
        white-space: nowrap;
    }

    .AttendanceFormFieldItemTextTitle {
        text-overflow: ellipsis;
        overflow: hidden;
    }
`

const AttendanceFormOption = ({
    id,
    fieldIndex,
    index,
    control,
    value,
    onRemove,
    buttonProps,
}) => {
    const { t } = useTranslation()

    const ariaLabel = `${index + 1}. ${value}`

    return (
        <Draggable draggableId={id} index={index}>
            {(provided, snapshot) => (
                <Wrapper
                    ref={provided.innerRef}
                    $isDragging={snapshot.isDragging}
                    {...provided.draggableProps}
                >
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        <IconButton
                            {...buttonProps}
                            as="div"
                            tooltip={t('action.move')}
                            placement="left"
                            aria-label={t('aria.move-option', {
                                label: ariaLabel,
                            })}
                            {...provided.dragHandleProps}
                        >
                            <MoveIcon />
                        </IconButton>

                        <FormItem
                            control={control}
                            type="text"
                            name={`fields.${fieldIndex}.fieldOptions.${index}`}
                            label={t('form.title')}
                            style={{ flexGrow: 1 }}
                        />

                        <IconButton
                            {...buttonProps}
                            onClick={() => onRemove(index)}
                            aria-label={t('aria.remove-option', {
                                label: ariaLabel,
                            })}
                            tooltip={t('action.remove')}
                            placement="right"
                        >
                            <CrossIcon />
                        </IconButton>
                    </div>
                </Wrapper>
            )}
        </Draggable>
    )
}

export default AttendanceFormOption
