import React from 'react'
import { useTranslation } from 'react-i18next'
import { differenceInMilliseconds, formatISO } from 'date-fns'

import SettingContainer from 'js/admin/layout/SettingContainer'
import DateTimeField from 'js/components/DateTimeField'
import { Col, Row } from 'js/components/Grid/Grid'
import HideVisually from 'js/components/HideVisually/HideVisually'

const ScheduleFields = ({
    canSchedulePublish,
    timeScheduled,
    setTimeScheduled,
    canScheduleArchive,
    scheduleArchiveEntity,
    setScheduleArchiveEntity,
    scheduleDeleteEntity,
    setScheduleDeleteEntity,
}) => {
    const { t } = useTranslation()

    return (
        <Row $gutter={30}>
            {canSchedulePublish && (
                <Col mobileLandscapeUp={1 / 2}>
                    <SettingContainer
                        subtitle="action.publish"
                        helper="form.schedule-helper"
                        inputWidth="full"
                        $mb
                    >
                        <DateTimeField
                            name="timeSchedule"
                            labelPrefix={
                                <HideVisually>
                                    {t('action.publish')}{' '}
                                </HideVisually>
                            }
                            value={timeScheduled}
                            fromDateTime={formatISO(new Date())}
                            toDateTime={
                                scheduleArchiveEntity ??
                                scheduleDeleteEntity ??
                                null
                            }
                            timeSpan="hour"
                            onChange={setTimeScheduled}
                            isClearable
                        />
                    </SettingContainer>
                </Col>
            )}
            {canScheduleArchive && (
                <Col mobileLandscapeUp={1 / 2}>
                    <SettingContainer
                        subtitle={t('action.archive')}
                        helper="form.schedule-archive-helper"
                        inputWidth="full"
                        $mb
                    >
                        <DateTimeField
                            name="scheduleArchiveEntity"
                            labelPrefix={
                                <HideVisually>
                                    {t('action.archive')}{' '}
                                </HideVisually>
                            }
                            value={scheduleArchiveEntity}
                            fromDateTime={
                                timeScheduled &&
                                differenceInMilliseconds(timeScheduled) < 0
                                    ? timeScheduled
                                    : formatISO(new Date())
                            }
                            toDateTime={scheduleDeleteEntity}
                            timeSpan="hour"
                            onChange={setScheduleArchiveEntity}
                            isClearable
                        />
                    </SettingContainer>
                </Col>
            )}
            <Col mobileLandscapeUp={1 / 2}>
                <SettingContainer
                    subtitle={t('action.delete')}
                    helper="form.schedule-delete-helper"
                    inputWidth="full"
                >
                    <DateTimeField
                        name="scheduleDeleteEntity"
                        labelPrefix={
                            <HideVisually>{t('action.delete')} </HideVisually>
                        }
                        value={scheduleDeleteEntity}
                        fromDateTime={
                            scheduleArchiveEntity &&
                            differenceInMilliseconds(scheduleArchiveEntity) < 0
                                ? scheduleArchiveEntity
                                : timeScheduled &&
                                    differenceInMilliseconds(timeScheduled) < 0
                                  ? timeScheduled
                                  : formatISO(new Date())
                        }
                        onChange={setScheduleDeleteEntity}
                        isClearable
                    />
                </SettingContainer>
            </Col>
        </Row>
    )
}

export default ScheduleFields
