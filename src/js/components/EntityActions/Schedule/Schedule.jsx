import React from 'react'

import { Container } from 'js/components/Grid/Grid'
import Section from 'js/components/Section/Section'

import ScheduleFields from './components/ScheduleFields'

const Schedule = ({ canSaveAsDraft, isPublished, ...rest }) => {
    return (
        <Container size="small">
            <Section style={{ paddingTop: '24px' }}>
                <ScheduleFields
                    canSchedulePublish={canSaveAsDraft && !isPublished}
                    canScheduleArchive
                    {...rest}
                />
            </Section>
        </Container>
    )
}

export default Schedule
