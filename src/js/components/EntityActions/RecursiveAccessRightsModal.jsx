import React from 'react'
import { useFormContext } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import Modal from 'js/components/Modal/Modal'
import Text from 'js/components/Text/Text'

const RecursiveAccessRightsModal = ({
    showRecursiveAccessModal,
    onClose,
    onSubmit,
}) => {
    const { t } = useTranslation()

    const { control } = useFormContext()

    return (
        <Modal
            isVisible={showRecursiveAccessModal}
            onClose={onClose}
            title={t('entity-wiki.modified-access')}
            size="small"
        >
            <Text>{t('entity-wiki.modified-access-confirm')}</Text>

            <FormItem
                control={control}
                type="checkbox"
                name="isAccessRecursive"
                label={t('entity-wiki.apply-access-to-underlying')}
                size="small"
                style={{ marginTop: '12px' }}
            />

            <Flexer mt>
                <Button
                    type="button"
                    size="normal"
                    variant="tertiary"
                    onClick={onClose}
                >
                    {t('action.cancel')}
                </Button>
                <Button
                    type="submit"
                    size="normal"
                    variant="primary"
                    onClick={onSubmit}
                >
                    {t('action.confirm')}
                </Button>
            </Flexer>
        </Modal>
    )
}

export default RecursiveAccessRightsModal
