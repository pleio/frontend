import React, { forwardRef } from 'react'
import { useFormContext } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import Card from 'js/components/Card/Card'
import CustomTagsField from 'js/components/CustomTagsField'
import CoverField from 'js/components/Form/CoverField/CoverField'
import FormItem from 'js/components/Form/FormItem'
import { Container } from 'js/components/Grid/Grid'
import Section from 'js/components/Section/Section'
import Setting from 'js/components/Setting'
import Spacer from 'js/components/Spacer/Spacer'
import TagCategoriesField from 'js/components/TagCategories/TagCategoriesField'

import DescriptionField from './components/DescriptionField'
import InputLanguage from './components/InputLanguage'

const EVENT_MAX_ATTENDEES = 10_000

const Wrapper = styled(Section)`
    padding-top: 0;
`

const General = forwardRef(
    (
        {
            viewer,
            entity,
            canAccess,
            group,
            subtype,
            site,
            tags,
            setTags,
            customTags,
            handleAddTags,
            handleRemoveTags,
            renderCustomFields,
            featuredFullHeight,
        },
        ref,
    ) => {
        const { t } = useTranslation()

        const {
            control,
            watch,
            formState: { errors },
        } = useFormContext()

        const { contentTranslation, languageOptions } = site
        const showInputLanuage =
            contentTranslation && languageOptions?.length > 1

        const watchIsTranslationEnabled = watch('isTranslationEnabled')

        return (
            <Wrapper>
                <Container size="small" style={{ paddingTop: '24px' }}>
                    <Spacer>
                        {canAccess.featured && (
                            <CoverField
                                ref={ref}
                                featured={entity?.featured}
                                size={featuredFullHeight && 'full'}
                            />
                        )}

                        {renderCustomFields?.({ entity, control })}

                        {canAccess.backgroundColor && (
                            <FormItem
                                control={control}
                                type="color"
                                name="backgroundColor"
                                title={t('global.background-color')}
                                isClearable
                            />
                        )}

                        {canAccess.source && (
                            <FormItem
                                control={control}
                                type="text"
                                name="source"
                                title={t('entity-news.source')}
                            />
                        )}

                        {canAccess.abstract && (
                            <FormItem
                                control={control}
                                type="rich"
                                name="abstract"
                                title={t('form.abstract')}
                                helper={t('form.abstract-helper')}
                                errors={errors}
                                maxChars={site.maxCharactersInAbstract}
                                options={{
                                    textLanguage: true,
                                }}
                                disableNewLine
                                disableNewParagraph
                            />
                        )}

                        <DescriptionField
                            control={control}
                            subtype={subtype}
                            viewer={viewer}
                            group={group}
                            errors={errors}
                            required={subtype !== 'event'}
                        />

                        {showInputLanuage && (
                            <InputLanguage
                                control={control}
                                languageOptions={site.languageOptions}
                                showInputLanguage={watchIsTranslationEnabled}
                            />
                        )}

                        {canAccess.eventDates && (
                            <>
                                <Card
                                    radiusSize="large"
                                    style={{ padding: '20px' }}
                                >
                                    <Spacer spacing="small">
                                        <Setting
                                            subtitle="entity-event.location"
                                            helper="entity-event.location-helper"
                                        />
                                        <FormItem
                                            control={control}
                                            type="text"
                                            name="location"
                                            label={t('global.name')}
                                        />
                                        <FormItem
                                            control={control}
                                            type="text"
                                            name="locationAddress"
                                            label={t('form.address')}
                                        />
                                        <FormItem
                                            control={control}
                                            type="text"
                                            name="locationLink"
                                            label={t('form.link')}
                                        />
                                    </Spacer>
                                </Card>

                                <FormItem
                                    type="number"
                                    min={1}
                                    max={EVENT_MAX_ATTENDEES}
                                    control={control}
                                    name="maxAttendees"
                                    title={t('entity-event.max-guests-label', {
                                        count: EVENT_MAX_ATTENDEES,
                                    })}
                                />

                                <FormItem
                                    control={control}
                                    type="text"
                                    name="ticketLink"
                                    title={t('entity-event.ticket-link')}
                                />

                                <Card
                                    radiusSize="large"
                                    style={{ padding: '20px' }}
                                >
                                    <Spacer spacing="small">
                                        <Setting
                                            subtitle="form.welcome-message"
                                            helper="entity-event.welcome-message-helper"
                                        />
                                        <FormItem
                                            control={control}
                                            type="text"
                                            name="attendeeWelcomeMailSubject"
                                            label={t('global.subject')}
                                        />
                                        <FormItem
                                            control={control}
                                            type="rich"
                                            name="attendeeWelcomeMailContent"
                                            textSize="small"
                                            options={{
                                                textFormat: true,
                                                textStyle: true,
                                                textLink: true,
                                                textList: true,
                                            }}
                                            placeholder={t('form.message')}
                                            helper={
                                                <>
                                                    {t(
                                                        'entity-event.welcome-message-personalise',
                                                    )}
                                                    <br />
                                                    <span
                                                        style={{
                                                            display:
                                                                'inline-block',
                                                            width: '100px',
                                                        }}
                                                    >
                                                        [name]
                                                    </span>
                                                    {t(
                                                        'entity-event.welcome-message-personalise-name',
                                                    )}
                                                </>
                                            }
                                        />
                                    </Spacer>
                                </Card>
                            </>
                        )}
                        {subtype !== 'task' && (
                            <>
                                <TagCategoriesField
                                    name="filters"
                                    tagCategories={site.tagCategories}
                                    value={tags}
                                    setTags={setTags}
                                />
                                {site.customTagsAllowed && (
                                    <CustomTagsField
                                        name="customTags"
                                        value={customTags}
                                        onAddTag={handleAddTags}
                                        onRemoveTag={handleRemoveTags}
                                    />
                                )}
                            </>
                        )}
                    </Spacer>
                </Container>
            </Wrapper>
        )
    },
)

General.displayName = 'General'

export default General
