import React from 'react'
import { useTranslation } from 'react-i18next'

import FormItem from 'js/components/Form/FormItem'

const DescriptionField = ({
    control,
    viewer,
    subtype,
    group,
    errors,
    ...rest
}) => {
    const { t } = useTranslation()

    return (
        <FormItem
            control={control}
            type="rich"
            name="richDescription"
            title={t('form.content')}
            height="large"
            options={{
                textFormat: true,
                textStyle: true,
                textLink: true,
                textQuote: true,
                textList: true,
                insertMedia: viewer.canInsertMedia,
                textAnchor: true,
                textCSSClass: true,
                textLanguage: true,
                insertTable: true,
                insertMention: true,
                insertAccordion: ['news', 'blog', 'wiki', 'event'].includes(
                    subtype,
                ),
                insertButton: ['news', 'blog', 'wiki', 'event'].includes(
                    subtype,
                ),
            }}
            group={group}
            errors={errors}
            {...rest}
        />
    )
}

export default DescriptionField
