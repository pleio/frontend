import React from 'react'
import { useTranslation } from 'react-i18next'

import AnimatePresence from 'js/components/AnimatePresence/AnimatePresence'
import FormItem from 'js/components/Form/FormItem'
import { Col, Row } from 'js/components/Grid/Grid'

const InputLanguage = ({ control, languageOptions, showInputLanguage }) => {
    const { t } = useTranslation()

    return (
        <Row $spacing={24}>
            <Col mobileLandscapeUp={1 / 2}>
                <FormItem
                    control={control}
                    type="switch"
                    prependField
                    name="isTranslationEnabled"
                    size="small"
                    title={t('form.translate')}
                    helper={t('form.translate-helper')}
                />
            </Col>
            <Col mobileLandscapeUp={1 / 2}>
                <AnimatePresence visible={showInputLanguage}>
                    <FormItem
                        control={control}
                        type="select"
                        name="inputLanguage"
                        label={t('form.input-language')}
                        options={languageOptions}
                    />
                </AnimatePresence>
            </Col>
        </Row>
    )
}

export default InputLanguage
