import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useMutation } from '@apollo/client'

import Button from 'js/components/Button/Button'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import Spacer from 'js/components/Spacer/Spacer'
import addAttachmentMutation from 'js/files/UploadOrSelectFiles/addAttachmentMutation'
import addAttachments from 'js/files/UploadOrSelectFiles/addAttachments'
import UploadOrSelectFiles from 'js/files/UploadOrSelectFiles/UploadOrSelectFiles'
import { FILE_TYPE } from 'js/lib/constants'

const InsertImageForm = ({
    image,
    setImage,
    alt,
    setAlt,
    caption,
    setCaption,
    uploadHelper,
    onClose,
}) => {
    const { t } = useTranslation()

    const [files, setFiles] = useState(image ? [image] : [])

    const [mutate] = useMutation(addAttachmentMutation)

    const [showError, setShowError] = useState('')

    const handleComplete = (file, alt, caption) => {
        if (file) {
            setImage?.(file)
        }
        setAlt?.(alt)
        setCaption?.(caption)
        onClose()
    }

    const submit = async ({ alt, caption }) => {
        await addAttachments(
            mutate,
            files,
            setFiles,
            (files) => handleComplete(files[0], alt, caption),
            setShowError,
        )
    }

    const defaultValues = {
        alt,
        caption,
    }

    const {
        control,
        handleSubmit,
        formState: { isSubmitting },
    } = useForm({
        defaultValues,
    })

    const { maxSize } = FILE_TYPE.image

    return (
        <Spacer
            as="form"
            onSubmit={(evt) => {
                evt.preventDefault()
                handleSubmit(submit)()
                // Prevent parent form from submitting
                evt.stopPropagation()
            }}
            style={{
                display: 'flex',
                flexDirection: 'column',
            }}
        >
            <div>
                <UploadOrSelectFiles
                    accept="image"
                    maxSize={maxSize}
                    files={files}
                    setFiles={setFiles}
                    isLoading={isSubmitting}
                    uploadHelper={uploadHelper}
                />

                <Errors errors={showError} />
            </div>

            {setAlt && (
                <FormItem
                    type="text"
                    control={control}
                    name="alt"
                    label={t('editor.image-alt')}
                    helper={t('editor.image-alt-helper')}
                />
            )}

            {setCaption && (
                <FormItem
                    control={control}
                    type="rich"
                    name="caption"
                    label={t('editor.image-caption')}
                    helper={t('editor.image-caption-helper')}
                    options={{
                        textLink: true,
                    }}
                    height="small"
                    textSize="small"
                    contentType="html"
                />
            )}

            <Flexer>
                <Button
                    size="normal"
                    variant="tertiary"
                    type="button"
                    onClick={onClose}
                >
                    {t('action.cancel')}
                </Button>
                <Button
                    size="normal"
                    variant="primary"
                    type="submit"
                    loading={isSubmitting}
                >
                    {image ? t('action.save') : t('action.insert')}
                </Button>
            </Flexer>
        </Spacer>
    )
}

export default InsertImageForm
