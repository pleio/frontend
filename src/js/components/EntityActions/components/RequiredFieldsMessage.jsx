import React from 'react'
import { Trans } from 'react-i18next'
import styled, { useTheme } from 'styled-components'

const Wrapper = styled.div`
    display: flex;
    justify-content: flex-start;

    .RequiredFieldsMessageBox {
        border-radius: ${(p) => p.theme.radius.normal};
        border: 1px solid ${(p) => p.theme.color.grey[30]};
        padding: 6px 12px;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
    }
`

const RequiredFieldsMessage = () => {
    const theme = useTheme()

    return (
        <Wrapper>
            <div className="RequiredFieldsMessageBox">
                <Trans i18nKey="global.fields-required">
                    Fields with an asterisk (
                    <span
                        style={{
                            color: theme.color.warn.main,
                        }}
                    >
                        *
                    </span>
                    ) are required.
                </Trans>
            </div>
        </Wrapper>
    )
}

export default RequiredFieldsMessage
