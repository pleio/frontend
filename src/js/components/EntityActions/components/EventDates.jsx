import React from 'react'
import { useFormContext } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { format, formatISO, isAfter, isBefore } from 'date-fns'

import DateTimeField from 'js/components/DateTimeField'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import Spacer from 'js/components/Spacer/Spacer'
import Text from 'js/components/Text/Text'
import {
    getWeekdayOccurrenceInMonth,
    showFullDay,
} from 'js/lib/helpers/date/showDate'

const EventDates = ({
    startDate,
    setStartDate,
    endDate,
    setEndDate,
    enableRepeating,
    isRepeating,
}) => {
    const { t } = useTranslation()

    const { control, watch, getValues, setValue } = useFormContext()

    const repeatCount = watch('rangeSettings.interval')

    const repeatUntil =
        ['never', 'after'].indexOf(watch('rangeSettings.repeatUntil')) !== -1
            ? null
            : getValues('rangeSettings.repeatUntil')

    const repeatUntilPlaceholder = formatISO(startDate)

    const repeatMonthly =
        ['dayOfTheMonth', 'weekdayOfTheMonth'].indexOf(
            watch('rangeSettings.type'),
        ) !== -1

    const weekdayOfTheMonthCount = getWeekdayOccurrenceInMonth(startDate)

    const handleChangeStartDate = (val) => {
        setStartDate(val)
        if (isAfter(val, endDate)) {
            setEndDate()
        }
        if (isBefore(repeatUntil, val)) {
            setValue('rangeSettings.repeatUntil', val)
        }
    }

    const handleChangeEndDate = (val) => {
        setEndDate(isAfter(val, startDate) ? val : startDate)
    }

    return (
        <Spacer spacing="small">
            <Flexer justifyContent="flex-start" wrap>
                <DateTimeField
                    name="startDate"
                    value={startDate}
                    fromDateTime={formatISO(new Date())}
                    onChange={handleChangeStartDate}
                    style={{ width: '210px' }}
                />
                <Text size="small" style={{ flexShrink: 0 }}>
                    {t('entity-event.to')}
                </Text>
                <DateTimeField
                    name="endDate"
                    value={endDate}
                    fromDateTime={startDate}
                    onChange={handleChangeEndDate}
                    isClearable
                    defaultMonth={startDate}
                    style={{ width: '210px' }}
                />
                {enableRepeating && (
                    <FormItem
                        type="checkbox"
                        name="repeat"
                        control={control}
                        label={t('entity-event.repeat')}
                        size="small"
                        disabled={isRepeating}
                        style={{
                            flexGrow: 1,
                            alignSelf: 'stretch',
                            display: 'flex',
                            justifyContent: 'center',
                        }}
                    />
                )}
            </Flexer>
            {enableRepeating && watch('repeat') && (
                <>
                    <Flexer justifyContent="flex-start" wrap>
                        <Text size="small" style={{ flexShrink: 0 }}>
                            {t('entity-event.repeat-every')}
                        </Text>
                        <FormItem
                            type="number"
                            min={1}
                            max={99}
                            control={control}
                            name="rangeSettings.interval"
                            aria-label={t('entity-event.frequency-to-repeat', {
                                frequency: t(
                                    `entity-event.${watch(
                                        'rangeSettings.type',
                                    )}`,
                                    {
                                        count: 2,
                                    },
                                ),
                            })}
                            style={{ width: '56px' }}
                        />
                        <FormItem
                            type="select"
                            control={control}
                            name="rangeSettings.type"
                            aria-label={t('entity-event.frequency')}
                            options={[
                                {
                                    value: 'daily',
                                    label: t('entity-event.day', {
                                        count: repeatCount,
                                    }),
                                },
                                {
                                    value: 'dayOfTheWeek',
                                    label: t('entity-event.week', {
                                        count: repeatCount,
                                    }),
                                },
                                {
                                    value:
                                        watch('rangeSettings.type') ===
                                        'weekdayOfTheMonth'
                                            ? 'weekdayOfTheMonth'
                                            : 'dayOfTheMonth',
                                    label: t('entity-event.month', {
                                        count: repeatCount,
                                    }),
                                },
                            ]}
                            style={{ width: '100px' }}
                        />

                        {repeatMonthly && (
                            <>
                                <Text size="small" style={{ flexShrink: 0 }}>
                                    {t('entity-event.on')}
                                </Text>
                                <FormItem
                                    type="select"
                                    control={control}
                                    name="rangeSettings.type"
                                    id="rangeSettings.type.month"
                                    aria-label={t('entity-event.frequency')}
                                    options={[
                                        {
                                            value: 'dayOfTheMonth',
                                            label: t(
                                                'entity-event.on-month-day',
                                                {
                                                    day: format(startDate, 'd'),
                                                },
                                            ),
                                        },
                                        {
                                            value: 'weekdayOfTheMonth',
                                            label: t(
                                                'entity-event.on-month-weekday',
                                                {
                                                    count: t('global.count', {
                                                        count: weekdayOfTheMonthCount,
                                                        ordinal: true,
                                                    }),
                                                    weekday:
                                                        showFullDay(startDate),
                                                },
                                            ),
                                        },
                                    ]}
                                    style={{ width: '163px' }}
                                />
                            </>
                        )}
                    </Flexer>
                    <Flexer justifyContent="flex-start">
                        <Text
                            size="small"
                            as="label"
                            htmlFor="repeatEnds"
                            style={{ flexShrink: 0 }}
                        >
                            {t('entity-event.ends')}
                        </Text>
                        <FormItem
                            type="select"
                            control={control}
                            name="rangeSettings.repeatUntil"
                            options={[
                                {
                                    value: 'never',
                                    label: t('entity-event.never'),
                                },
                                {
                                    value:
                                        repeatUntil || repeatUntilPlaceholder,
                                    label: t('entity-event.on-date'),
                                },
                                {
                                    value: 'after',
                                    label: t('entity-event.after'),
                                },
                            ]}
                            style={{ width: '110px' }}
                        />
                        {repeatUntil && (
                            <FormItem
                                type="date"
                                control={control}
                                name="rangeSettings.repeatUntil"
                                id="rangeSettings.repeatUntil.date"
                                fromDate={startDate}
                                aria-label={t('entity-event.frequency')}
                                style={{ width: '142px' }}
                            />
                        )}
                        {watch('rangeSettings.repeatUntil') === 'after' && (
                            <>
                                <FormItem
                                    type="number"
                                    min={1}
                                    max={99}
                                    control={control}
                                    name="rangeSettings.instanceLimit"
                                    aria-label={t('entity-event.frequency')}
                                    style={{ width: '56px' }}
                                />
                                <Text size="small" style={{ flexShrink: 0 }}>
                                    {t('entity-event.time', {
                                        count: watch(
                                            'rangeSettings.instanceLimit',
                                        ),
                                    })}
                                </Text>
                            </>
                        )}
                    </Flexer>
                </>
            )}
        </Spacer>
    )
}

export default EventDates
