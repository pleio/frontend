import React, { useState } from 'react'
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { produce } from 'immer'
import styled from 'styled-components'

import DisplayDate from 'js/components/DisplayDate'
import FetchMore from 'js/components/FetchMore'
import { Col, Container, Row } from 'js/components/Grid/Grid'
import { H4 } from 'js/components/Heading'
import IconButton from 'js/components/IconButton/IconButton'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import SearchBar from 'js/components/SearchBar/SearchBar'
import Section from 'js/components/Section/Section'
import Setting from 'js/components/Setting'
import Text from 'js/components/Text/Text'
import useSubtypes from 'js/lib/hooks/useSubtypes'

import AddIcon from 'icons/add.svg'
import CrossIcon from 'icons/cross.svg'
import MoveIcon from 'icons/move-vertical.svg'

const Wrapper = styled(Container)`
    .SuggestedSearchResult {
        display: flex;
        background: white;
        border-bottom: 1px solid transparent;

        &:not(:last-child) {
            border-bottom-color: ${(p) => p.theme.color.grey[20]};
        }

        &[aria-current='dragging'] {
            border-bottom-color: transparent;
            box-shadow: ${(p) => p.theme.shadow.soft.center};
            border-radius: ${(p) => p.theme.radius.small};
        }
    }

    .SuggestedSearchResultText {
        margin-right: auto;
        padding: 9px 0;
        line-height: ${(p) => p.theme.font.lineHeight.small};
    }

    a.SuggestedSearchResultTitle {
        color: ${(p) => p.theme.color.primary.main};

        &:hover,
        &:focus {
            text-decoration: underline;
        }
    }

    .SuggestedSearchResultTitle {
        margin-right: 4px;
        font-size: ${(p) => p.theme.font.size.small};
    }

    .SuggestedSearchResultSubtype {
        font-size: ${(p) => p.theme.font.size.tiny};
        color: ${(p) => p.theme.color.text.grey};
    }

    .SuggestedSearchResultMove {
        display: flex;
        align-items: center;
        height: 40px;
        margin-right: 8px;
    }
`

const NoResultMessage = () => {
    const { t } = useTranslation()
    return (
        <Text
            size="small"
            variant="grey"
            textAlign="center"
            style={{ padding: '20px' }}
        >
            {t('form.no-items-found')}
        </Text>
    )
}

const SuggestedListItem = ({ entity, addSuggested }) => {
    const { t } = useTranslation()
    const { subtypes } = useSubtypes()

    const { guid, title, url, subtype, timePublished } = entity

    return (
        <li key={guid} className="SuggestedSearchResult">
            <div className="SuggestedSearchResultText">
                <Link to={url} className="SuggestedSearchResultTitle">
                    {title}
                </Link>
                <span className="SuggestedSearchResultSubtype">
                    <DisplayDate
                        date={timePublished}
                        type="timeSince"
                        placement="bottom"
                        hiddenPrefix={t('global.publication-date')}
                    />
                    <>&nbsp;&nbsp;•&nbsp;&nbsp;</>
                    {subtypes[subtype].contentName}
                </span>
            </div>
            <IconButton
                variant="secondary"
                size="large"
                onClick={() => addSuggested(entity)}
                aria-label={t('form.add-to-suggested', {
                    title,
                })}
            >
                <AddIcon />
            </IconButton>
        </li>
    )
}

const DraggableListItem = ({ entity, i, removeSuggested }) => {
    const { t } = useTranslation()
    const { subtypes } = useSubtypes()

    const { guid, title, subtype, timePublished } = entity

    return (
        <Draggable key={`${guid}-draggable`} draggableId={guid} index={i}>
            {(provided, snapshot) => (
                <li
                    ref={provided.innerRef}
                    className="SuggestedSearchResult"
                    aria-current={snapshot.isDragging ? 'dragging' : null}
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                >
                    <div className="SuggestedSearchResultMove">
                        <MoveIcon />
                    </div>
                    <div className="SuggestedSearchResultText">
                        <span className="SuggestedSearchResultTitle">
                            {title}
                        </span>
                        <span className="SuggestedSearchResultSubtype">
                            <DisplayDate
                                date={timePublished}
                                type="timeSince"
                                placement="bottom"
                                hiddenPrefix={t('global.publication-date')}
                            />
                            <>&nbsp;&nbsp;•&nbsp;&nbsp;</>
                            {subtypes[subtype].contentName}
                        </span>
                    </div>
                    <IconButton
                        className="SuggestedSearchResultRemove"
                        variant="secondary"
                        size="large"
                        onClick={() => removeSuggested(i)}
                        aria-label={t('form.remove-from-suggested', {
                            title,
                        })}
                    >
                        <CrossIcon />
                    </IconButton>
                </li>
            )}
        </Draggable>
    )
}

const Suggested = ({ suggestedItems, setSuggestedItems }) => {
    const { t } = useTranslation()

    const [q, setQ] = useState('')
    const [searchInput, setSearchInput] = useState('')

    const changeSearchInput = (evt) => setSearchInput(evt.target.value)
    const submitSearch = () => setQ(searchInput)

    const [queryLimit, setQueryLimit] = useState(10)
    const { loading, data, fetchMore } = useQuery(GET_SUGGESTED_SEARCH, {
        variables: {
            q,
            subtypes: [
                'news',
                'blog',
                'discussion',
                'question',
                'event',
                'wiki',
                'podcast',
            ],
            offset: 0,
            limit: queryLimit,
        },
    })

    const onDragEnd = (result) => {
        if (!result.destination) return

        const sourcePosition = result.source.index
        const destinationPosition = result.destination.index

        setSuggestedItems(
            produce((newState) => {
                const [movedItem] = newState.splice(sourcePosition, 1)
                newState.splice(destinationPosition, 0, movedItem)
            }),
        )
    }

    const addSuggested = (entity) => {
        if (suggestedItems.findIndex((el) => el.guid === entity.guid) === -1) {
            setSuggestedItems([...suggestedItems, entity])
        }
    }

    const removeSuggested = (index) => {
        setSuggestedItems(
            produce((newState) => {
                newState.splice(index, 1)
            }),
        )
    }

    return (
        <Wrapper size="small">
            <Section divider style={{ paddingTop: '24px' }}>
                <Row $spacing={20} $gutter={30}>
                    <Col mobileLandscapeUp={1 / 2}>
                        <Setting
                            onSubmit={submitSearch}
                            subtitle="global.search"
                            helper="form.suggested-search-helper"
                            htmlFor="search-article"
                            inputWidth="full"
                        >
                            <SearchBar
                                name="search-article"
                                value={searchInput}
                                onChange={changeSearchInput}
                                onSearch={submitSearch}
                                label={t('global.search')}
                            />
                        </Setting>

                        {q && loading && (
                            <LoadingSpinner style={{ margin: '16px 0' }} />
                        )}
                        {q && !loading && (
                            <FetchMore
                                edges={data.search.edges}
                                getMoreResults={(data) => data.search.edges}
                                fetchMore={fetchMore}
                                fetchCount={10}
                                setLimit={setQueryLimit}
                                maxLimit={data.search.total}
                                noResults={<NoResultMessage />}
                            >
                                <ul style={{ marginTop: '8px' }}>
                                    {data.search.edges.map((entity) => {
                                        return (
                                            <SuggestedListItem
                                                key={entity.guid}
                                                entity={entity}
                                                addSuggested={addSuggested}
                                            />
                                        )
                                    })}
                                </ul>
                            </FetchMore>
                        )}
                    </Col>
                    <Col mobileLandscapeUp={1 / 2}>
                        {suggestedItems.length > 0 ? (
                            <>
                                <H4>
                                    {t('global.count-items', {
                                        count: suggestedItems.length,
                                    })}
                                </H4>
                                <DragDropContext onDragEnd={onDragEnd}>
                                    <Droppable droppableId="suggested-items">
                                        {(provided) => (
                                            <>
                                                <ul ref={provided.innerRef}>
                                                    {suggestedItems.map(
                                                        (entity, i) => {
                                                            return (
                                                                <DraggableListItem
                                                                    key={
                                                                        entity.guid
                                                                    }
                                                                    entity={
                                                                        entity
                                                                    }
                                                                    i={i}
                                                                    removeSuggested={
                                                                        removeSuggested
                                                                    }
                                                                />
                                                            )
                                                        },
                                                    )}
                                                </ul>
                                                {provided.placeholder}
                                            </>
                                        )}
                                    </Droppable>
                                </DragDropContext>
                            </>
                        ) : null}
                    </Col>
                </Row>
            </Section>
        </Wrapper>
    )
}

const suggestedFragment = `
__typename
guid
title
url
subtype
timePublished
`

const GET_SUGGESTED_SEARCH = gql`
    query SuggestedSearch(
        $q: String!
        $offset: Int
        $limit: Int
        $subtypes: [String]
        $orderBy: SearchOrderBy
        $orderDirection: OrderDirection
    ) {
        search(
            q: $q
            subtypes: $subtypes
            offset: $offset
            limit: $limit
            orderBy: $orderBy
            orderDirection: $orderDirection
        ) {
            total
            edges {
                ... on Blog {
                    ${suggestedFragment}
                }
                ... on News {
                    ${suggestedFragment}
                }
                ... on Question {
                    ${suggestedFragment}
                }
                ... on Discussion {
                    ${suggestedFragment}
                }
                ... on Event {
                    ${suggestedFragment}
                }
                ... on Wiki {
                    ${suggestedFragment}
                }
                ... on Podcast {
                    ${suggestedFragment}
                }
            }
        }
    }
`

export default Suggested
