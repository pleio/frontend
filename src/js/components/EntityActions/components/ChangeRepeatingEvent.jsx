import React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'

const ChangeRepeatingEvent = ({
    onClose,
    onSubmit,
    isSubmitting,
    range = 'this',
}) => {
    const { t } = useTranslation()

    const defaultValues = {
        range,
    }

    const { control, handleSubmit } = useForm({ defaultValues })

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <FormItem
                type="radio"
                control={control}
                name="range"
                legend={t('form.apply-to')}
                options={[
                    {
                        value: 'this',
                        label: t('entity-event.range-this'),
                    },
                    {
                        value: 'following',
                        label: t('entity-event.range-following'),
                    },
                ]}
            />
            <Flexer mt>
                <Button size="normal" variant="tertiary" onClick={onClose}>
                    {t('action.cancel')}
                </Button>
                <Button
                    size="normal"
                    variant="primary"
                    type="submit"
                    loading={isSubmitting}
                >
                    {t('action.save')}
                </Button>
            </Flexer>
        </form>
    )
}

export default ChangeRepeatingEvent
