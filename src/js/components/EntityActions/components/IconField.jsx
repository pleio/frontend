import React, { useState } from 'react'
import { useFormContext } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import { H4 } from 'js/components/Heading'
import Img from 'js/components/Img/Img'
import Modal from 'js/components/Modal/Modal'
import Spacer from 'js/components/Spacer/Spacer'

import InsertImageForm from './InsertImageForm'

const IconField = ({ iconProps, ...rest }) => {
    const { t } = useTranslation()
    const { watch, setValue } = useFormContext()

    const icon = watch('icon')
    const iconUrl = icon?.download

    const [showImageModal, setShowImageModal] = useState(false)
    const toggleShowImageModal = () => {
        setShowImageModal(!showImageModal)
    }

    const setImage = (val) => {
        setValue('icon', val)
    }

    const removeImage = () => {
        setValue('icon', null)
    }

    return (
        <div {...rest}>
            <H4 style={{ marginBottom: '4px' }}>{t('form.icon')}</H4>
            <Spacer spacing="small">
                {icon && <Img src={iconUrl} {...iconProps} />}

                <Flexer gutter="small" justifyContent="flex-start">
                    <Button
                        size="small"
                        variant="secondary"
                        onClick={toggleShowImageModal}
                    >
                        {icon ? t('form.edit-image') : t('action.insert-image')}
                        ..
                    </Button>
                    {icon && (
                        <Button
                            size="small"
                            variant="secondary"
                            onClick={removeImage}
                        >
                            {t('form.remove-image')}
                        </Button>
                    )}
                </Flexer>

                <Modal
                    isVisible={showImageModal}
                    onClose={toggleShowImageModal}
                    size="small"
                    title={
                        icon ? t('form.edit-image') : t('action.insert-image')
                    }
                >
                    <InsertImageForm
                        image={icon}
                        setImage={setImage}
                        onClose={toggleShowImageModal}
                    />
                </Modal>
            </Spacer>
        </div>
    )
}

export default IconField
