/*
For now only used for pubishing a page.
*/

import React from 'react'
import { useTranslation } from 'react-i18next'

import AccessControlField from 'js/components/Form/AccessControlField'
import ConfirmationModal from 'js/components/Modal/ConfirmationModal'
import Spacer from 'js/components/Spacer/Spacer'

const ConfirmAccessModal = ({
    showModal,
    onCancel,
    onConfirm,
    group,
    accessControl,
    setAccessControl,
}) => {
    const { t } = useTranslation()

    return (
        <ConfirmationModal
            title={t('action.publish')}
            cancelLabel={t('action.cancel')}
            confirmLabel={t('action.publish')}
            isVisible={showModal}
            onConfirm={onConfirm}
            onCancel={onCancel}
        >
            <Spacer spacing="small">
                <p>{t('permissions.publish-confirm')}</p>
                <AccessControlField
                    group={group}
                    accessControl={accessControl}
                    setAccessControl={setAccessControl}
                />
            </Spacer>
        </ConfirmationModal>
    )
}

export default ConfirmAccessModal
