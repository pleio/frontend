import { featuredViewFragment } from 'js/lib/fragments/featured'

const baseSuggestedItemFragment = `
    title
    url
    subtype
    timePublished
`

export const entityEditSuggestedItemsFragment = `
    suggestedItems {
        guid
        ... on Blog {
            ${baseSuggestedItemFragment}
        }
        ... on News {
            ${baseSuggestedItemFragment}
        }
        ... on Question {
            ${baseSuggestedItemFragment}
        }
        ... on Discussion {
            ${baseSuggestedItemFragment}
        }
        ... on Event {
            ${baseSuggestedItemFragment}
        }
        ... on Wiki {
            ${baseSuggestedItemFragment}
        }
        ... on Podcast {
            ${baseSuggestedItemFragment}
        }
    }
`

const entityViewSuggestedItemFragment = `
    ${baseSuggestedItemFragment}
    ${featuredViewFragment}
`

export const entityViewSuggestedItemsFragment = `
    suggestedItems {
        guid
        ... on Blog {
            ${entityViewSuggestedItemFragment}
        }
        ... on News {
            ${entityViewSuggestedItemFragment}
        }
        ... on Question {
            ${entityViewSuggestedItemFragment}
        }
        ... on Discussion {
            ${entityViewSuggestedItemFragment}
        }
        ... on Event {
            ${entityViewSuggestedItemFragment}
        }
        ... on Wiki {
            ${entityViewSuggestedItemFragment}
        }
        ... on Podcast {
            ${baseSuggestedItemFragment}
        }
    }
`
