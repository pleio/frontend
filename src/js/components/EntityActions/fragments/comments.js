import { commentOwnerFragment } from 'js/lib/fragments/owner'

export const entityViewCommentsFragment = `
    canComment
    commentCount
    comments {
        guid
        description
        excerpt
        richDescription
        timeCreated
        canEdit
        canArchiveAndDelete
        votes
        hasVoted
        isBestAnswer
        ${commentOwnerFragment}
        canComment
        pendingModeration
        commentCount
        comments {
            guid
            excerpt
            richDescription
            timeCreated
            canEdit
            canArchiveAndDelete
            votes
            hasVoted
            ${commentOwnerFragment}
            canComment
            pendingModeration
        }
    }
`
