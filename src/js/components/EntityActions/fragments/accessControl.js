export default `
accessControl {
    __typename @include(if: false)
    type
    grant
    ... on AccessItemGroup {
        group {
            guid
            name
        }
    }
    ... on AccessItemUser {
        user {
            guid
            name
            email
            icon
        }
    }
}
`
