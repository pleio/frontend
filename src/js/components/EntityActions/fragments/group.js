import { iconViewFragment } from 'js/lib/fragments/icon'

const baseGroupFragment = `
    isClosed
    url
    name
    membership
`

export const entityEditGroupFragment = `
    group {
        guid
        ... on Group {
            ${baseGroupFragment}
        }
    }
`

export const entityViewGroupFragment = `
    group {
        guid
        ... on Group {
            ${baseGroupFragment}
            ${iconViewFragment}
        }
    }
`
