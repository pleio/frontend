import { entityViewCommentsFragment } from 'js/components/EntityActions/fragments/comments'
import {
    entityEditGroupFragment,
    entityViewGroupFragment,
} from 'js/components/EntityActions/fragments/group'
import {
    entityEditSuggestedItemsFragment,
    entityViewSuggestedItemsFragment,
} from 'js/components/EntityActions/fragments/suggestedItems'
import {
    featuredEditFragment,
    featuredViewFragment,
} from 'js/lib/fragments/featured'
import { ownerFragment } from 'js/lib/fragments/owner'

export const baseEntityFragment = `
    title
    description
    richDescription
    url
    accessId
    timePublished
    statusPublished
    canEdit
    canArchiveAndDelete
    canEditAdvanced
    canEditGroup
    tags
    tagCategories {
        name
        values
    }
    subtype
    inputLanguage
    isTranslationEnabled
`

export const entityEditFragment = `
    ${baseEntityFragment}
    abstract
    writeAccessId
    scheduleArchiveEntity
    scheduleDeleteEntity
    isFeatured
    isRecommendedInSearch
    inputLanguage
    isTranslationEnabled
    ${featuredEditFragment}
    ${entityEditGroupFragment}
    ${entityEditSuggestedItemsFragment}
    ${ownerFragment}
`

// Excluded wiki and podcast
export const entityViewFragment = `
    ${baseEntityFragment}
    ${entityViewGroupFragment}
    ${entityViewSuggestedItemsFragment}
    ${featuredViewFragment}
    ${ownerFragment}
    ${entityViewCommentsFragment}
    localTitle
    localRichDescription
    localDescription
    views
    votes
    hasVoted
    isFollowing
    isBookmarked
    canBookmark
    isTranslationEnabled
`
