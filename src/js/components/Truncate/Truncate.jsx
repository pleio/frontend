import React from 'react'
import styled from 'styled-components'

const Truncated = styled.span`
    display: -webkit-box;
    -webkit-line-clamp: ${(p) => p.$lines};
    -webkit-box-orient: vertical;
    overflow: hidden;
`

const Truncate = ({ lines = 3, children }) => (
    <Truncated $lines={lines}>{children}</Truncated>
)

export default Truncate
