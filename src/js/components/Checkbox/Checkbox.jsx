import React, { useState } from 'react'
import handleInputPromise from 'helpers/handleInputPromise'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import CheckIcon from 'icons/check.svg'
import CheckSmallIcon from 'icons/check-small.svg'

import styles from './checkBoxStyles'

const StyledCheckbox = styled.div`
    ${styles};
`

const Checkbox = ({
    children,
    name,
    label,
    size = 'normal',
    checked,
    defaultChecked,
    disabled,
    onChange,
    onHandle,
    releaseInputArea,
    tabIndex,
    'aria-describedby': ariaDescribedby,
    'aria-label': ariaLabel,
    ...rest
}) => {
    const [isLoading, setisLoading] = useState(false)

    const handleChange = (evt) => {
        !!onHandle && handleInputPromise(onHandle(evt), setisLoading)
    }

    const CheckboxLabelElement = label ? 'label' : 'div'

    return (
        <StyledCheckbox
            $hasLabel={!!label || !!children}
            $releaseInputArea={releaseInputArea}
            $size={size}
            $loading={isLoading}
            {...rest}
        >
            <input
                className="CheckboxInput"
                type="checkbox"
                name={name}
                id={name}
                checked={checked}
                defaultChecked={defaultChecked}
                disabled={disabled}
                onChange={onChange || handleChange}
                aria-label={ariaLabel}
                tabIndex={tabIndex}
                aria-describedby={ariaDescribedby}
            />
            <div className="CheckboxContainer">
                {(!!label || !!children) && (
                    <CheckboxLabelElement
                        className="CheckboxLabel"
                        htmlFor={label ? name : null}
                        style={!label && !children ? { display: 'none' } : {}}
                    >
                        {label || children}
                    </CheckboxLabelElement>
                )}
                <div className="CheckboxIcon">
                    {size === 'small' ? <CheckSmallIcon /> : <CheckIcon />}
                </div>
            </div>
        </StyledCheckbox>
    )
}

Checkbox.propTypes = {
    name: PropTypes.string,
    label: PropTypes.string,
    size: PropTypes.oneOf(['small', 'normal']),
    required: PropTypes.bool,
    disabled: PropTypes.bool,
    releaseInputArea: PropTypes.bool,
}

export default React.memo(
    Checkbox,
    (prev, next) =>
        prev.checked === next.checked &&
        prev.disabled === next.disabled &&
        prev.onChange === next.onChange,
)
