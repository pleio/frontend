import { css } from 'styled-components'

export default css`
    position: ${(p) => !p.$releaseInputArea && 'relative'};

    ${(p) =>
        p.$loading &&
        css`
            pointer-events: none;
            cursor: default;
        `};

    .CheckboxContainer {
        position: relative;

        ${(p) =>
            !p.$hasLabel &&
            css`
                display: flex;
                align-items: center;
            `};
    }

    .CheckboxInput {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        margin: 0;
        opacity: 0;
        z-index: 1;
        cursor: pointer;

        &:hover + .CheckboxContainer .CheckboxIcon {
            background-color: ${(p) => p.theme.color.hover};

            svg {
                opacity: 1;
            }
        }

        &:active + .CheckboxContainer .CheckboxIcon {
            background-color: ${(p) => p.theme.color.active};
        }

        body:not(.mouse-user) &:focus + .CheckboxContainer .CheckboxIcon {
            outline: ${(p) => p.theme.focusStyling};
        }

        &:checked + .CheckboxContainer .CheckboxIcon {
            color: white;
            border-color: transparent;
            background-color: ${(p) => p.theme.color.secondary.main};

            svg {
                opacity: 1;
            }
        }

        &[disabled] {
            cursor: default;
            pointer-events: none;

            + .CheckboxContainer .CheckboxLabel {
                color: ${(p) => p.theme.color.text.grey};
                pointer-events: none;
            }

            + .CheckboxContainer .CheckboxIcon {
                background-color: ${(p) => p.theme.color.grey[30]};
                border-color: ${(p) => p.theme.color.grey[30]};
                color: ${(p) => p.theme.color.icon.grey};
            }
        }
    }

    .CheckboxLabel {
        display: block;
        font-family: ${(p) => p.theme.font.family};
        font-weight: ${(p) => p.theme.font.weight.normal};
        color: ${(p) => p.theme.color.text.black};

        ${(p) =>
            p.$size === 'small' &&
            css`
                padding: 2px 0 2px 20px;
                font-size: ${(p) => p.theme.font.size.small};
                line-height: ${(p) => p.theme.font.lineHeight.small};
            `};

        ${(p) =>
            p.$size === 'normal' &&
            css`
                padding: 3px 0 3px 26px;
                font-size: ${(p) => p.theme.font.size.normal};
                line-height: ${(p) => p.theme.font.lineHeight.normal};
            `};
    }

    .CheckboxIcon {
        order: -1;
        pointer-events: none;
        border: 2px solid ${(p) => p.theme.color.grey[40]};
        border-radius: ${(p) => p.theme.radius.tiny};
        color: ${(p) => p.theme.color.icon.grey};

        display: flex;
        align-items: center;
        justify-content: center;

        ${(p) =>
            p.$size === 'small' &&
            css`
                width: 14px;
                height: 14px;
            `};

        ${(p) =>
            p.$size === 'normal' &&
            css`
                width: 16px;
                height: 16px;
            `};

        ${(p) =>
            p.$hasLabel &&
            css`
                position: absolute;
                left: 0;

                ${(p) =>
                    p.$size === 'small' &&
                    css`
                        top: 5px;
                    `};

                ${(p) =>
                    p.$size === 'normal' &&
                    css`
                        top: 7px;
                    `};
            `}

        svg {
            opacity: 0;

            ${(p) =>
                p.$size === 'normal' &&
                css`
                    height: 8px;
                `};
        }
    }
`
