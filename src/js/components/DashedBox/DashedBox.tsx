import React, { ForwardedRef, forwardRef } from 'react'
import styled from 'styled-components'

export interface Props {
    children: React.ReactNode
}

const Wrapper = styled.div<Props>`
    position: relative;

    > .DashedBoxLine {
        position: absolute;
        pointer-events: none;
        stroke: ${(p) => p.theme.color.grey[40]};
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;

        path {
            fill: none;
            stroke-width: 2;
            stroke-dasharray: 8, 8;
        }
    }
`

const DashedBox = forwardRef<HTMLDivElement, Props>(
    ({ children, ...rest }: Props, ref: ForwardedRef<HTMLDivElement>) => (
        <Wrapper ref={ref} {...rest}>
            <svg
                viewBox="0 0 10 10"
                preserveAspectRatio="none"
                shapeRendering="crispEdges"
                className="DashedBoxLine"
            >
                <path
                    d="M0 0 10 0 10 10 0 10z"
                    vectorEffect="non-scaling-stroke"
                />
            </svg>
            {children}
        </Wrapper>
    ),
)

DashedBox.displayName = 'DashedBox'

export default DashedBox
