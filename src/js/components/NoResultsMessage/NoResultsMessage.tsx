import React from 'react'

import Text from 'js/components/Text/Text'

export interface Props {
    subtitle?: string
    title?: string
}

const NoResultsMessage = ({ subtitle, title }: Props) => {
    return (
        <div style={{ margin: '0 auto', textAlign: 'center' }}>
            {title && <Text as="h3">{title}</Text>}
            {subtitle && (
                <Text size="small" variant="grey">
                    {subtitle}
                </Text>
            )}
        </div>
    )
}

export default NoResultsMessage
