import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import HideVisually from 'js/components/HideVisually/HideVisually'
import VideoIframe from 'js/components/VideoIframe'

const Wrapper = styled.div`
    position: relative;
    width: 100%;
    padding-bottom: 56.3%;
    background-color: transparent;

    > iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
`

const VideoModal = ({ type, id, title, onClose }) => {
    const [disableKeyboard, setDisableKeyboard] = useState(false)
    const toggleKeyboard = () => setDisableKeyboard(!disableKeyboard)

    const { t } = useTranslation()

    return (
        <>
            <HideVisually as="button" type="button" onClick={toggleKeyboard}>
                {disableKeyboard
                    ? t('action.enable-video-kb')
                    : t('action.disable-video-kb')}
            </HideVisually>
            <Wrapper>
                <VideoIframe
                    id={id}
                    type={type}
                    title={title}
                    autoPlay
                    disableKeyboard={disableKeyboard}
                    onClose={onClose}
                />
            </Wrapper>
        </>
    )
}

export default VideoModal
