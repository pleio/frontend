import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import PropTypes from 'prop-types'

import ToggleButton from 'js/components/ToggleButton/ToggleButton'

import IconBell from 'icons/bell.svg'
import IconBellFill from 'icons/bell-fill.svg'

const Follow = (props) => {
    const [userIsFollowing, setUserIsFollowing] = useState(
        props.entity.isFollowing || false,
    )

    const toggleFollowing = () => {
        const { __typename, guid, isFollowing: wasFollowing } = props.entity
        const isFollowing = !wasFollowing

        props.mutate({
            variables: {
                input: {
                    guid,
                    isFollowing,
                },
            },

            optimisticResponse: {
                follow: {
                    __typename: 'followPayload',
                    object: {
                        __typename,
                        guid,
                        isFollowing,
                    },
                },
            },
        })

        setUserIsFollowing(isFollowing)
    }

    const { t } = useTranslation()

    return (
        <ToggleButton
            isEnabled={userIsFollowing}
            onClick={toggleFollowing}
            enabledContent={t('action.stop-following')}
            disabledContent={t('action.follow')}
            EnabledIcon={IconBellFill}
            DisabledIcon={IconBell}
        />
    )
}

Follow.propTypes = {
    entity: PropTypes.object.isRequired,
}

const Mutation = gql`
    mutation Follow($input: followInput!) {
        follow(input: $input) {
            object {
                guid
                ... on FileFolder {
                    isFollowing
                }
                ... on Task {
                    isFollowing
                }
                ... on Blog {
                    isFollowing
                }
                ... on News {
                    isFollowing
                }
                ... on Question {
                    isFollowing
                }
                ... on Discussion {
                    isFollowing
                }
                ... on StatusUpdate {
                    isFollowing
                }
                ... on Podcast {
                    isFollowing
                }
            }
        }
    }
`

export default graphql(Mutation)(Follow)
