import React from 'react'
import { Link, NavLink, useMatch, useResolvedPath } from 'react-router-dom'

import DropdownButton from 'js/components/DropdownButton/DropdownButton'
import LinkItem from 'js/components/LinkItem/LinkItem'

import ChevronDownIcon from 'icons/chevron-down-small.svg'

const focusNode = (node) => {
    // Give focus to a node if it's a button or link
    // If it's not a focusable node (DropdownButton is wrapped in a <span>), try to focus its first child
    const { tagName, firstChild } = node

    if (tagName === 'A' || tagName === 'BUTTON') {
        node.focus()
    } else if (firstChild.tagName === 'A' || firstChild.tagName === 'BUTTON') {
        firstChild.focus()
    }
}

// Restore focus on activated tab menu item
const restoreFocus = (event) => {
    setTimeout(() => {
        event.target.focus()
    }, 0)
}

const handleArrowKeys = (event) => {
    if (event.key === 'ArrowLeft') {
        const { previousSibling, parentNode } = event.target

        if (previousSibling) {
            focusNode(previousSibling)
        } else {
            const prevSib = parentNode.previousSibling
            prevSib && focusNode(prevSib)
        }
    }

    if (event.key === 'ArrowRight') {
        const { nextSibling, parentNode } = event.target

        if (nextSibling) {
            focusNode(nextSibling)
        } else {
            const nextSib = parentNode.nextSibling
            nextSib && focusNode(nextSib)
        }
    }

    // if key is Home or End, focus first or last item
    if (event.key === 'Home') {
        event.preventDefault()
        event.target.parentNode.firstChild.focus()
    }
    if (event.key === 'End') {
        event.preventDefault()
        event.target.parentNode.lastChild.focus()
    }
}

const TabMenuItemNavLink = ({ item }) => {
    // We need the resolved pathname to match relative paths like '../activity/published'
    const resolvedPath = useResolvedPath(item.to)
    const match = useMatch({ path: resolvedPath.pathname, end: !!item.end })

    return (
        <NavLink
            id={item.id}
            to={item.to}
            end={!!item.end}
            className="TabMenuItem"
            tabIndex={match ? 0 : -1} // Only enable active tab so focus starts there and use arrow keys to navigate
            onKeyDown={handleArrowKeys}
            role="tab"
            aria-controls={item.ariaControls}
            aria-selected={!!match}
            onClick={restoreFocus}
        >
            {item.label}
        </NavLink>
    )
}

const TabMenuItem = ({ item, counter, TabElement }) => {
    if (item.options?.length > 0) {
        const itemOptions = item.options
            // Sometimes items are undefined, filter them out
            .filter(Boolean)
            // Only include items with a label and a to or url
            .filter(({ label, to, url }) => label && (to || url))
            .map(({ label, to, url }) => {
                return {
                    name: label,
                    ...(to ? { to } : {}),
                    ...(url ? { url } : {}),
                }
            })

        return (
            <DropdownButton
                key={counter}
                options={itemOptions}
                trigger="click mouseenter"
                placement="bottom-start"
                arrow
                wrapContent
                showArrow
                arrowElement={<ChevronDownIcon style={{ marginLeft: '4px' }} />}
            >
                <button
                    key={counter}
                    type="button"
                    aria-current={item.isActive ? 'page' : false}
                    className="TabMenuItem"
                    tabIndex={item.isActive ? 0 : -1}
                    onKeyDown={handleArrowKeys}
                >
                    {item.label}
                </button>
            </DropdownButton>
        )
    } else if (TabElement) {
        return <TabElement key={counter} className="TabMenuItem" {...item} />
    } else if (item.onClick) {
        return (
            <button
                key={counter}
                id={item.id}
                type="button"
                onClick={(event) => {
                    item.onClick()
                    restoreFocus(event)
                }}
                aria-current={item.isActive ? 'page' : false}
                className="TabMenuItem"
                tabIndex={item.isActive ? 0 : -1}
                onKeyDown={handleArrowKeys}
                role="tab"
                aria-controls={item.ariaControls}
                aria-selected={item.isActive}
            >
                {item.label}
            </button>
        )
    }

    if (item.to !== undefined) {
        return (
            <TabMenuItemNavLink
                key={counter}
                item={item}
                onClick={restoreFocus}
            />
        )
    }

    if (item.link !== undefined) {
        return (
            <Link
                key={counter}
                id={item.id}
                to={item.link}
                aria-current={item.isActive ? 'page' : undefined}
                className="TabMenuItem"
                tabIndex={item.isActive ? 0 : -1}
                onKeyDown={handleArrowKeys}
                onClick={restoreFocus}
                role="tab"
                aria-controls={item.ariaControls}
                aria-selected={item.isActive}
            >
                {item.label}
            </Link>
        )
    }

    if (item.url !== undefined) {
        return (
            <LinkItem
                key={counter}
                id={item.id}
                url={item.url}
                className="TabMenuItem"
                tabIndex={item.isActive ? 0 : -1}
                onKeyDown={handleArrowKeys}
                role="tab"
                aria-controls={item.ariaControls}
                aria-selected={item.isActive}
            >
                {item.label}
            </LinkItem>
        )
    }
    return null
}

export default TabMenuItem
