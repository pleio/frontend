import { setLightness } from 'polished'
import { css } from 'styled-components'

const secondaryHover = (p) => setLightness(0.98, p.theme.color.secondary.main)
const secondaryActive = (p) => setLightness(0.95, p.theme.color.secondary.main)

export default css`
    position: ${(p) => !p.$releaseInputArea && 'relative'};
    display: flex;
    align-items: center;

    ${(p) =>
        p.$hasLabel &&
        css`
            align-items: flex-start;

            ${(p) =>
                p.$size === 'small' &&
                css`
                    padding: 3px 0;

                    .SwitchLabel {
                        margin-left: 6px;
                    }
                `};

            ${(p) =>
                p.$size === 'normal' &&
                css`
                    padding: 4px 0;

                    .SwitchLabel {
                        margin-left: 8px;
                    }
                `};
        `};

    ${(p) =>
        p.$loading &&
        css`
            pointer-events: none;
            cursor: default;
        `};

    ${(p) =>
        p.$size === 'small' &&
        css`
            .SwitchLabel {
                font-size: ${(p) => p.theme.font.size.small};
                line-height: ${(p) => p.theme.font.lineHeight.small};
            }

            .SwitchThumb {
                margin: 3px 0;
                width: 24px;
                height: 14px;
                border-radius: 7px;

                &:before {
                    width: 14px;
                    height: 14px;
                }
            }

            .SwitchInput:checked + .SwitchLabel + .SwitchThumb:before {
                transform: translateX(10px);
            }

            ${(p) =>
                p.$loading &&
                css`
                    .SwitchInput + .SwitchLabel + .SwitchThumb:before {
                        transform: translateX(5px);
                    }
                `};
        `};

    ${(p) =>
        p.$size === 'normal' &&
        css`
            .SwitchLabel {
                font-size: ${(p) => p.theme.font.size.normal};
                line-height: ${(p) => p.theme.font.lineHeight.normal};
            }

            .SwitchThumb {
                margin: 3px 0;
                width: 32px;
                height: 18px;
                border-radius: 9px;

                &:before {
                    width: 18px;
                    height: 18px;
                }
            }

            .SwitchInput:checked + .SwitchLabel + .SwitchThumb:before {
                transform: translateX(14px);
            }

            ${(p) =>
                p.$loading &&
                css`
                    .SwitchInput:not(:checked)
                        + .SwitchLabel
                        + .SwitchThumb:before {
                        border-color: ${(p) => p.theme.color.secondary.main};
                    }

                    .SwitchInput + .SwitchLabel + .SwitchThumb {
                        .SwitchThumbBar {
                            width: 50%;
                        }

                        &:before {
                            transform: translateX(7px);
                        }
                    }
                `};
        `};

    .SwitchInput {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        margin: 0;
        opacity: 0;
        cursor: pointer;

        &:not([disabled]) {
            &:hover + .SwitchLabel + .SwitchThumb:before {
                background-color: ${(p) => p.theme.color.grey[10]};
            }
            &:active + .SwitchLabel + .SwitchThumb:before {
                background-color: ${(p) => p.theme.color.grey[20]};
            }

            body:not(.mouse-user) &:focus + .SwitchLabel + .SwitchThumb {
                outline: ${(p) => p.theme.focusStyling};
            }

            &:checked:hover + .SwitchLabel + .SwitchThumb:before {
                background-color: ${secondaryHover};
            }
            &:checked:active + .SwitchLabel + .SwitchThumb:before {
                background-color: ${secondaryActive};
            }
            &:checked + .SwitchLabel + .SwitchThumb {
                .SwitchThumbBar {
                    width: 100%;
                }
                &:before {
                    border-color: ${(p) => p.theme.color.secondary.main};
                }
            }
        }

        &[disabled] {
            cursor: default;
            pointer-events: none;

            + .SwitchLabel {
                color: ${(p) => p.theme.color.text.grey};
                pointer-events: none;
            }

            + .SwitchLabel + .SwitchThumb {
                background-color: ${(p) => p.theme.color.grey[20]} !important;

                .SwitchThumbBar {
                    display: none;
                }

                &:before {
                    background-color: ${(p) => p.theme.color.grey[30]};
                    border-color: transparent !important;
                }
            }
        }
    }

    .SwitchThumb {
        flex-shrink: 0;
        position: relative;
        order: -1;
        pointer-events: none;
        color: white;
        outline-offset: 0;
        background-color: ${(p) => p.theme.color.grey[30]};
        overflow: hidden;

        &:before {
            content: '';
            position: absolute;
            left: 0;
            top: 0;
            background-color: white;
            border: 2px solid ${(p) => p.theme.color.grey[40]};
            border-radius: 50%;
            transition: transform ${(p) => p.theme.transition.materialFast};
        }
    }

    .SwitchThumbBar {
        width: 0;
        height: 100%;
        background-color: ${(p) => p.theme.color.secondary.main};
        transition:
            background-color ${(p) => p.theme.transition.fast},
            width ${(p) => p.theme.transition.materialFast};
    }
`
