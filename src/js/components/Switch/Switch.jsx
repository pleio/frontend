import React, { forwardRef, useState } from 'react'
import handleInputPromise from 'helpers/handleInputPromise'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import styles from './switchStyles'

const Wrapper = styled.div`
    ${styles};
`

const Switch = forwardRef(
    (
        {
            name,
            label,
            checked,
            disabled,
            size = 'normal',
            releaseInputArea,
            onChange,
            onHandle,
            loading,
            style,
            ...rest
        },
        ref,
    ) => {
        const [isLoading, setisLoading] = useState(false)

        const handleChange = (evt) => {
            !!onHandle && handleInputPromise(onHandle(evt), setisLoading)
        }

        return (
            <Wrapper
                $hasLabel={!!label}
                $size={size}
                $releaseInputArea={releaseInputArea}
                $loading={loading || isLoading}
                style={style}
            >
                <input
                    ref={ref}
                    className="SwitchInput"
                    type="checkbox"
                    name={name}
                    id={name}
                    checked={checked}
                    disabled={disabled}
                    onChange={onChange || handleChange}
                    {...rest}
                />
                {label ? (
                    <label className="SwitchLabel" htmlFor={name}>
                        {label}
                    </label>
                ) : (
                    // So that css doesn't break
                    <div className="SwitchLabel" />
                )}
                <div className="SwitchThumb">
                    <div className="SwitchThumbBar" />
                </div>
            </Wrapper>
        )
    },
)

Switch.displayName = 'Switch'

Switch.propTypes = {
    name: PropTypes.string.isRequired,
    checked: PropTypes.bool,
    disabled: PropTypes.bool,
    size: PropTypes.oneOf(['small', 'normal']),
    releaseInputArea: PropTypes.bool,
    onChange: PropTypes.func,
    onHandle: PropTypes.func,
}

export default Switch
