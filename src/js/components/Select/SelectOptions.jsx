import React, { forwardRef } from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import Checkbox from 'js/components/Checkbox/Checkbox'

const Wrapper = styled.div`
    .SelectOptions {
        max-height: 400px;
        overflow-y: auto;
        padding: 4px 0;

        > * {
            &.focused {
                outline: ${(p) => p.theme.focusStyling};
            }
        }
    }

    .SelectOption {
        width: 100%;
        padding: 4px 16px;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};

        &:hover {
            background-color: ${(p) => p.theme.color.hover};
        }

        &:active {
            background-color: ${(p) => p.theme.color.active};
        }

        &[disabled] {
            color: ${(p) => p.theme.color.text.grey};
        }

        &[aria-current='true'] {
            color: ${(p) => p.theme.color.primary.main};
            pointer-events: none;
            text-decoration: underline;
        }
    }

    .SelectNoOptions {
        padding: 7px 12px;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        color: ${(p) => p.theme.color.text.grey};
        text-align: center;
    }
`

const SelectOptions = forwardRef(
    (
        {
            name,
            options,
            value,
            isMulti,
            focusedOption,
            setFocusedOption,
            noOptionsMessage,
            onAdd,
            refFocusedOption,
        },
        ref,
    ) => {
        const { t } = useTranslation()

        return (
            <Wrapper ref={ref}>
                {options.length > 0 ? (
                    <ul
                        className="SelectOptions"
                        id={`${name}-listbox`}
                        aria-labelledby={`${name}-label`}
                        role="listbox"
                        aria-activedescendant={
                            focusedOption
                                ? `${name}-option-${focusedOption - 1}`
                                : null
                        }
                        tabIndex="-1"
                    >
                        {options.map((option, i) => {
                            // Prevent losing focus
                            const handleMouseDown = (evt) => {
                                evt.preventDefault()
                            }

                            const clickAdd = (evt) => {
                                evt.preventDefault()
                                if (options.length === focusedOption) {
                                    setFocusedOption(focusedOption - 1)
                                }
                                onAdd(option.value)
                            }

                            const id = `${name}-option-${i}`

                            return (
                                <li
                                    key={id}
                                    id={id}
                                    role="option"
                                    aria-selected={
                                        isMulti
                                            ? value?.some(
                                                  (el) => el === option.value,
                                              )
                                            : value === option.value ||
                                              focusedOption - 1 === i
                                    }
                                    className={
                                        focusedOption - 1 === i ? 'focused' : ''
                                    }
                                    ref={
                                        focusedOption - 1 === i
                                            ? refFocusedOption
                                            : null
                                    }
                                >
                                    {isMulti ? (
                                        <Checkbox
                                            name={id}
                                            size="small"
                                            checked={false}
                                            tabIndex="-1"
                                            onMouseDown={handleMouseDown}
                                            onChange={clickAdd}
                                            disabled={option.disabled}
                                            style={{
                                                padding: '0 8px',
                                            }}
                                        >
                                            {option.label}
                                        </Checkbox>
                                    ) : (
                                        <button
                                            className="SelectOption"
                                            type="button"
                                            onClick={clickAdd}
                                            tabIndex="-1"
                                            aria-current={
                                                option.value === value
                                            }
                                            disabled={option.disabled}
                                        >
                                            {option.label}
                                        </button>
                                    )}
                                </li>
                            )
                        })}
                    </ul>
                ) : (
                    <div className="SelectNoOptions">
                        {noOptionsMessage || t('form.no-options')}
                    </div>
                )}
            </Wrapper>
        )
    },
)

SelectOptions.displayName = 'SelectOptions'

export default SelectOptions
