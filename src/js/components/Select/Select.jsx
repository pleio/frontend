import React, {
    forwardRef,
    useContext,
    useEffect,
    useRef,
    useState,
} from 'react'
import { useTranslation } from 'react-i18next'
import TippyPlugin from '@tippyjs/react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

import { AriaLiveContext, AriaLiveMessage } from 'js/components/AriaLive'
import HideVisually from 'js/components/HideVisually/HideVisually'
import IconButton from 'js/components/IconButton/IconButton'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Tag from 'js/components/Tag/Tag'
import Tooltip from 'js/components/Tooltip/Tooltip'

import ChevronDownIcon from 'icons/chevron-down-small.svg'
import CrossIcon from 'icons/cross-small.svg'

import SelectOptions from './SelectOptions'

const Wrapper = styled.div`
    position: relative;
    width: 100%;

    .tippy-box[data-theme~='select'] {
        background-color: ${(p) => p.theme.menu.bg};
        border: 1px solid ${(p) => p.theme.color.grey[40]};
        border-top: none;
        border-radius: 0 0
            ${(p) => `${p.theme.radius.small} ${p.theme.radius.small}`};
        box-shadow: rgba(0, 0, 0, 0.15) 0 4px 16px -4px;

        color: ${(p) => p.theme.menu.color};
        font-size: ${(p) => p.theme.font.size.normal};
        line-height: ${(p) => p.theme.font.lineHeight.normal};
        font-weight: ${(p) => p.theme.font.weight.normal};
        text-align: left;
        overflow: hidden;

        > .tippy-content {
            padding: 0;
        }
    }

    ${(p) =>
        p.$borderStyle === 'none' &&
        css`
            .tippy-box[data-theme~='select'] {
                border: none;
                box-shadow:
                    rgba(0, 0, 0, 0.035) 0 0 0 1px,
                    rgba(0, 0, 0, 0.15) 0 4px 16px -4px;
            }
        `};

    .SelectLabel {
        display: flex;
        align-items: center;
        padding: 0 2px;
        position: absolute;
        top: -9px;
        left: ${(p) => (p.$borderStyle === 'none' ? '14px' : '15px')};
        max-width: calc(100% - 10px);
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        font-size: ${(p) => p.theme.font.size.tiny};
        line-height: 18px;
        color: ${(p) => p.theme.color.text.grey};
        z-index: 1;

        /* White background behind the label */
        &:before {
            content: '';
            position: absolute;
            top: 9px;
            left: 0;
            right: 0;
            height: ${(p) => (p.$disabled ? '1px' : '2px')};
            background-color: white;
            z-index: -1;
        }
    }

    .SelectLabelRequired {
        padding-left: 2px;
        color: ${(p) => p.theme.color.warn.main};
    }

    .SelectContainer {
        position: relative;
        width: 100%;
        height: 100%;
        display: flex;

        &:before {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            width: 100%;
            height: 100%;
            background-color: white;
            border: 1px solid ${(p) => p.theme.color.grey[40]};

            ${(p) =>
                p.$borderStyle === 'none' &&
                css`
                    background-color: transparent;
                    border-width: 0;
                `};

            ${(p) =>
                p.$borderStyle === 'rounded-left' &&
                css`
                    border-top-left-radius: ${(p) => p.theme.radius.small};
                    border-bottom-left-radius: ${(p) => p.theme.radius.small};
                `};

            ${(p) =>
                p.$borderStyle === 'rounded-right' &&
                css`
                    border-top-right-radius: ${(p) => p.theme.radius.small};
                    border-bottom-right-radius: ${(p) => p.theme.radius.small};
                `};

            ${(p) =>
                p.$borderStyle === 'rounded' &&
                css`
                    border-radius: ${(p) => p.theme.radius.small};
                `};

            ${(p) =>
                p.$visible &&
                css`
                    border-bottom-width: 1px;
                    border-bottom-left-radius: 0;
                    border-bottom-right-radius: 0;
                    border-bottom-color: ${(p) =>
                        p.$borderStyle === 'none'
                            ? p.theme.color.grey[30]
                            : p.theme.color.grey[30]};
                `};

            ${(p) =>
                p.$disabled &&
                css`
                    background-color: ${(p) => p.theme.color.grey[20]};
                `};
        }
    }

    .SelectContent {
        width: 100%;
        min-height: 40px;
        display: flex;
        align-items: center;
        padding: ${(p) =>
            p.$borderStyle === 'none'
                ? '5px 5px 5px 16px'
                : '6px 6px 6px 17px'};
        pointer-events: none;
    }

    .SelectToggleButton {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        width: 100%;
        height: 100%;

        ${(p) =>
            !p.$disabled &&
            !p.$loading &&
            css`
                pointer-events: auto;

                &:focus + .SelectContent,
                &:hover + .SelectContent {
                    .ToggleOptionsButton {
                        background-color: ${(p) => p.theme.color.hover};
                    }
                }

                &:active + .SelectContent .ToggleOptionsButton {
                    background-color: ${(p) => p.theme.color.active};
                }
            `};
    }

    .ToggleOptionsButton {
        margin: -2px 0 -2px -8px;
        position: relative;
        width: 32px;
        height: 32px;
        display: flex;
        align-items: center;
        justify-content: center;
        border-radius: 50%;
        color: ${(p) => p.theme.color.icon.black};
    }

    .SelectClearButton {
        margin: -2px 0;
        pointer-events: ${(p) => !p.$disabled && !p.$loading && 'auto'};

        &[aria-selected='true'] {
            outline: ${(p) => p.theme.focusStyling};
        }
    }

    .SelectActions {
        display: flex;
        align-items: center;
        margin-left: auto;
    }

    .SelectValues {
        flex-grow: 1;
        display: flex;
        align-items: center;
        flex-wrap: wrap;
        z-index: 1;

        &.HasValues {
            margin-left: -10px;

            > .SelectSearch {
                width: 80px;
                margin-left: 6px;
            }
        }

        /* Tag */
        > li {
            margin: 2px;
            user-select: none;
            pointer-events: ${(p) => !p.$disabled && !p.$loading && 'auto'};

            &.focused button {
                outline: ${(p) => p.theme.focusStyling};
            }
        }
    }

    .SelectValue {
        position: relative;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};

        ${(props) =>
            !props.$wrapSelectedValue &&
            css`
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
            `}
    }

    .SelectPlaceholder {
        position: relative;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        color: ${(p) => p.theme.color.text.grey};
    }

    .SelectSearch {
        flex-grow: 1;
        position: relative;
        background: none;
        margin-right: 2px; // outline won't appear below clear button
        border: none;
        padding: 0;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
    }
`

const Select = forwardRef(
    (
        {
            name,
            label,
            placeholder,
            options,
            value,
            defaultValue,
            disabled,
            isMulti,
            isClearable,
            isSearchable,
            isLoading,
            borderStyle = 'rounded',
            noOptionsMessage,
            onChange,
            onChangeSearch,
            required,
            tabIndex,
            searchPlaceholder,
            wrapSelectedValue = false,
            'aria-label': ariaLabel,
            ...rest
        },
        ref,
    ) => {
        const refOptions = useRef()
        const refSearchInput = useRef()
        const refFocusedOption = useRef()
        const [isVisible, setIsVisible] = useState(false)
        const [searchValue, setSearchValue] = useState('')
        const [focusedValue, setFocusedValue] = useState()
        const [focusedOption, setFocusedOption] = useState()

        const toggleOptions = () => {
            setIsVisible(!isVisible)
        }

        const hideOptions = () => {
            if (showSearch) {
                ref?.current && ref.current.focus()
            }
            setIsVisible(false)
        }

        const { announceAssertive } = useContext(AriaLiveContext)

        const handleRemove = (val) => {
            announceAssertive(
                t('select.removed-value', {
                    label: options.find((option) => option.value === val).label,
                }),
            )

            const newValue = value ? [...value] : []
            newValue.splice(value.indexOf(val), 1)
            onChange(newValue)
        }

        const handleAdd = (val) => {
            if (isMulti) {
                announceAssertive(
                    t('select.selected-option', {
                        label: options.find((option) => option.value === val)
                            .label,
                    }),
                )

                // If last available option, close options
                if (availableOptions.length === 1) {
                    hideOptions()
                }
                const newValue = value ? [...value] : []
                newValue.push(val)
                onChange(newValue)
            } else {
                onChange(val)
                hideOptions()
            }
        }

        const clearValue = () => {
            announceAssertive(
                t('select.cleared-value', {
                    label: selectedValue?.label,
                }),
            )
            setFocusedValue()
            handleAdd('')
        }

        const handleChangeSearch = (evt) => {
            setSearchValue(evt.target.value)

            if (onChangeSearch) {
                onChangeSearch(evt.target.value)
            }

            // Reset focused value
            if (focusedValue) {
                setFocusedValue()
            }

            // Reset focused option
            if (focusedOption) {
                setFocusedOption(1)
            }
        }

        const handleEscape = (evt) => {
            switch (evt.key) {
                case 'Escape':
                    evt.preventDefault()
                    evt.stopPropagation()
                    hideOptions()
                    // Manually focusing button, because focus in hideOptions() doesn't work ('showSearch' is not up to date via callback)
                    ref?.current && ref.current.focus()
                    break
            }
        }

        const handleKeyDown = (evt) => {
            switch (evt.key) {
                case 'ArrowDown':
                    if (filteredOptions.length > 0) {
                        evt.preventDefault()
                        // Show options
                        if (!isVisible) {
                            setIsVisible(true)
                        }
                        // Focus first or next option
                        if (
                            !focusedOption ||
                            focusedOption === filteredOptions.length
                        ) {
                            setFocusedOption(1)
                        } else {
                            setFocusedOption(focusedOption + 1)
                        }
                    }
                    break

                case 'ArrowUp':
                    if (filteredOptions.length > 0) {
                        evt.preventDefault()
                        // Show options
                        if (!isVisible) {
                            setIsVisible(true)
                        }
                        // Focus previous option
                        if (!focusedOption || focusedOption === 1) {
                            setFocusedOption(filteredOptions.length)
                        } else {
                            setFocusedOption(focusedOption - 1)
                        }
                    }
                    break

                case 'ArrowLeft':
                    if (showClearButton) {
                        // Toggle focus of clear single value button
                        focusedValue ? setFocusedValue() : setFocusedValue(1)
                    } else if (isMulti && value.length > 0) {
                        // Focus previous multi value
                        evt.preventDefault()
                        if (!focusedValue || focusedValue === 1) {
                            setFocusedValue(value.length)
                        } else {
                            setFocusedValue(focusedValue - 1)
                        }
                    }

                    break

                case 'ArrowRight':
                    if (showClearButton) {
                        // Toggle focus of clear single value button
                        focusedValue ? setFocusedValue() : setFocusedValue(1)
                    } else if (isMulti && value.length > 0) {
                        // Focus next multi value
                        evt.preventDefault()
                        if (!focusedValue || focusedValue === value.length) {
                            setFocusedValue(1)
                        } else {
                            setFocusedValue(focusedValue + 1)
                        }
                    }
                    break

                case ' ':
                    // Disable typing a space in search input
                    // ..enabling space for other actions
                    if (
                        evt.target.className === 'SelectSearch' &&
                        (!searchValue ||
                            (!!searchValue &&
                                (!!focusedOption || !!focusedValue)))
                    ) {
                        evt.preventDefault()
                    }
                    break

                case 'Enter':
                    evt.preventDefault()
                    break

                case 'Delete':
                case 'Backspace':
                    // Enable typing in search input
                    if (
                        evt.target.className === 'SelectSearch' &&
                        !!searchValue
                    ) {
                        return
                    }

                    if (showClearButton) {
                        // Focus or clear single value
                        evt.preventDefault()
                        if (focusedValue) {
                            clearValue()
                        } else {
                            setFocusedValue(1)
                        }
                    } else if (isMulti) {
                        // Focus or remove multi value
                        evt.preventDefault()
                        if (focusedValue) {
                            handleRemove(selectedValue[focusedValue - 1].value)
                            if (value.length === focusedValue) {
                                setFocusedValue(focusedValue - 1)
                            }
                        } else {
                            setFocusedValue(value.length)
                        }
                    }
                    break
            }
        }

        const handleKeyUp = (evt) => {
            switch (evt.key) {
                case ' ':
                case 'Enter':
                    if (showClearButton && focusedValue === 1) {
                        // Clear single value
                        evt.preventDefault()
                        clearValue()
                    } else if (isMulti && !!focusedValue) {
                        // Remove focused multi value
                        evt.preventDefault()
                        handleRemove(selectedValue[focusedValue - 1].value)
                        if (value.length === focusedValue) {
                            setFocusedValue(focusedValue - 1)
                        }
                    } else if (focusedOption) {
                        // Add focused option (single and multi)
                        evt.preventDefault()
                        handleAdd(filteredOptions[focusedOption - 1].value)
                        if (filteredOptions.length === focusedOption) {
                            setFocusedOption(focusedOption - 1)
                        }
                    }
                    break
            }
        }

        const handleShow = ({ popper, reference }) => {
            // document fires before window (keydown of modal)
            document.addEventListener('keydown', handleEscape)
            popper.style.width = reference.getBoundingClientRect().width + 'px'
        }

        const handleHide = () => {
            document.removeEventListener('keydown', handleEscape)
        }

        const handleClickOutside = () => {
            setIsVisible(false)
        }

        const handleBlur = (evt) => {
            // Check if blur happened outside component
            if (!refOptions.current.contains(evt.relatedTarget)) {
                if (focusedValue) setFocusedValue()
                setIsVisible(false)
            }
        }

        useEffect(() => {
            if (!isVisible) {
                if (searchValue) setSearchValue('')
                if (focusedValue) setFocusedValue()
                if (focusedOption) setFocusedOption()
            }

            // Focus search if options are shown
            if (isVisible && showSearch) {
                refSearchInput.current && refSearchInput.current.focus()
            }
        }, [isVisible])

        useEffect(() => {
            // Reset focused option if value gets focus
            if (!!focusedValue && !!focusedOption) {
                setFocusedOption()
            }

            if (!!focusedValue && !!selectedValue) {
                announceAssertive(
                    !isMulti
                        ? t('select.clear-value', {
                              label: selectedValue.label,
                          })
                        : t('select.remove-value', {
                              label: selectedValue[focusedValue - 1].label,
                              count: focusedValue,
                              total: selectedValue.length,
                          }),
                )
            }
        }, [focusedValue])

        useEffect(() => {
            // Reset focused value if option gets focus
            if (!!focusedOption && !!focusedValue) {
                setFocusedValue()
            }

            if (!!focusedOption && !!refFocusedOption.current) {
                refFocusedOption.current.scrollIntoView({
                    behavior: 'smooth',
                    block: 'nearest',
                    inline: 'start',
                })
            }

            if (!!focusedOption && filteredOptions.length > 0) {
                announceAssertive(
                    t('select.select-option', {
                        label: filteredOptions[focusedOption - 1].label,
                        count: focusedOption,
                        total: filteredOptions.length,
                    }),
                )
            }
        }, [focusedOption])

        const { t } = useTranslation()

        const SearchInput = (
            <>
                <HideVisually id={`${name}-search`}>
                    {t('select.search-options')}
                </HideVisually>
                <input
                    ref={refSearchInput}
                    className="SelectSearch"
                    type="text"
                    size="1"
                    placeholder={searchPlaceholder || `${t('global.search')}..`}
                    value={searchValue}
                    onChange={handleChangeSearch}
                    onKeyDown={handleKeyDown}
                    onKeyUp={handleKeyUp}
                    onBlur={handleBlur}
                    aria-describedby={`${name}-search`}
                    autoCapitalize="none"
                    autoComplete="off"
                    autoCorrect="off"
                    spellCheck="false"
                    aria-autocomplete="list"
                />
            </>
        )

        const Placeholder = (
            <div className="SelectPlaceholder">
                {placeholder || t('form.select-placeholder')}..
            </div>
        )

        // Filter out selected options if isMulti
        const availableOptions = isMulti
            ? options.filter((option) => !value?.includes(option.value))
            : options

        // Filter available options by search value
        const filteredOptions =
            searchValue && !onChangeSearch
                ? availableOptions.filter((option) =>
                      option.label
                          .toLowerCase()
                          .includes(searchValue.toLowerCase()),
                  )
                : availableOptions

        const hasValue = isMulti
            ? value?.length > 0
            : value !== undefined && value !== null && value !== '' // Accept value '0'

        const selectedValue = isMulti
            ? hasValue &&
              options.filter((option) => value.includes(option.value))
            : hasValue
              ? options.find((option) => option.value === value)
              : defaultValue

        const showSearch = (isSearchable || options.length > 15) && isVisible

        const showClearButton = isClearable && !isMulti && !disabled && hasValue

        return (
            <Wrapper
                $visible={isVisible}
                $disabled={disabled}
                $loading={isLoading}
                $borderStyle={borderStyle}
                $wrapSelectedValue={wrapSelectedValue}
                {...rest}
            >
                <HideVisually id={`${name}-describe`}>
                    {availableOptions.length > 0
                        ? t('select.focus-options')
                        : null}{' '}
                    {isMulti
                        ? selectedValue.length > 0 && t('select.focus-values')
                        : null}
                </HideVisually>

                {isVisible && (
                    <AriaLiveMessage
                        message={t('select.showing-options', {
                            count: filteredOptions.length,
                        })}
                    />
                )}

                <div className="SelectContainer">
                    {label && (
                        <span className="SelectLabel" aria-hidden>
                            {label}
                            {required && (
                                <Tooltip
                                    theme="warn-white"
                                    placement="right"
                                    content={t('global.required')}
                                >
                                    <span
                                        aria-hidden
                                        className="SelectLabelRequired"
                                    >
                                        *
                                    </span>
                                </Tooltip>
                            )}
                        </span>
                    )}
                    {(ariaLabel || label) && (
                        <HideVisually id={`${name}-label`}>
                            {ariaLabel ? `${ariaLabel}.` : `${label}.`}
                        </HideVisually>
                    )}

                    <TippyPlugin
                        visible={isVisible}
                        theme="select"
                        content={
                            <SelectOptions
                                name={name}
                                ref={refOptions}
                                options={filteredOptions}
                                value={value}
                                isMulti={isMulti}
                                focusedOption={focusedOption}
                                setFocusedOption={setFocusedOption}
                                noOptionsMessage={noOptionsMessage}
                                onAdd={handleAdd}
                                refFocusedOption={refFocusedOption}
                            />
                        }
                        offset={[0, 0]}
                        arrow={false}
                        animation="none"
                        duration="0"
                        maxWidth="none"
                        placement="bottom"
                        allowHTML={true}
                        interactive={true}
                        ignoreAttributes={true}
                        appendTo="parent"
                        popperOptions={{
                            modifiers: [
                                {
                                    name: 'preventOverflow',
                                    options: {
                                        // Can be positioned at the edge of the screen
                                        padding: 0,
                                    },
                                },
                                {
                                    name: 'flip',
                                    options: {
                                        // Always place options at the bottom
                                        fallbackPlacements: ['bottom'],
                                    },
                                },
                            ],
                        }}
                        onShow={handleShow}
                        onHide={handleHide}
                        onClickOutside={handleClickOutside}
                    >
                        <button
                            ref={ref}
                            className="SelectToggleButton"
                            id={name}
                            type="button"
                            aria-haspopup="listbox"
                            aria-owns={`${name}-listbox`}
                            tabIndex={showSearch || disabled ? '-1' : tabIndex}
                            onClick={toggleOptions}
                            onKeyDown={handleKeyDown}
                            onKeyUp={handleKeyUp}
                            onBlur={!showSearch ? handleBlur : null}
                            aria-labelledby={`${name}-label ${name}-selected`}
                            aria-describedby={`${name}-describe`}
                            disabled={disabled}
                            required={required}
                        />
                    </TippyPlugin>

                    <div className="SelectContent">
                        {!isMulti &&
                            !showSearch &&
                            !selectedValue &&
                            Placeholder}

                        {!isMulti && !showSearch && !!selectedValue && (
                            <div
                                className="SelectValue"
                                id={`${name}-selected`}
                            >
                                {selectedValue.label}
                            </div>
                        )}

                        {!isMulti && showSearch && SearchInput}

                        {isMulti &&
                            (!selectedValue || selectedValue.length === 0) &&
                            !showSearch &&
                            Placeholder}

                        {isMulti && (
                            <ul
                                className={`SelectValues${
                                    selectedValue && selectedValue.length > 0
                                        ? ' HasValues'
                                        : ''
                                }`}
                                id={`${name}-selected`}
                            >
                                {selectedValue &&
                                    selectedValue.length > 0 &&
                                    selectedValue.map((option, i) => {
                                        // Prevent losing focus
                                        const handleMouseDown = (evt) => {
                                            evt.preventDefault()
                                        }

                                        const clickRemove = () => {
                                            if (value.length === focusedValue) {
                                                setFocusedValue(
                                                    focusedValue - 1,
                                                )
                                            }
                                            handleRemove(option.value)
                                        }
                                        return (
                                            <Tag
                                                as="li"
                                                key={`${name}-value-${i}`}
                                                tabIndex="-1"
                                                className={
                                                    focusedValue - 1 === i
                                                        ? 'focused'
                                                        : ''
                                                }
                                                onMouseDown={handleMouseDown}
                                                onRemove={clickRemove}
                                                textRemoveLabel={t(
                                                    'form.remove-tag',
                                                    { tag: option.label },
                                                )}
                                            >
                                                {option.label}
                                                <HideVisually>,</HideVisually>
                                            </Tag>
                                        )
                                    })}
                                {showSearch && SearchInput}
                            </ul>
                        )}

                        <div className="SelectActions">
                            {showClearButton && !isLoading && (
                                <IconButton
                                    className="SelectClearButton"
                                    size="normal"
                                    variant="secondary"
                                    onClick={clearValue}
                                    tabIndex="-1"
                                    aria-selected={focusedValue === 1}
                                >
                                    <CrossIcon />
                                </IconButton>
                            )}

                            {!disabled && !isLoading && (
                                <div
                                    className="ToggleOptionsButton"
                                    aria-hidden
                                >
                                    <ChevronDownIcon />
                                </div>
                            )}
                            {isLoading && <LoadingSpinner />}
                        </div>
                    </div>
                </div>
            </Wrapper>
        )
    },
)

Select.propTypes = {
    name: PropTypes.string,
    label: PropTypes.string,
    'aria-label': PropTypes.string,
    placeholder: PropTypes.string,
    options: PropTypes.array.isRequired,
    value: PropTypes.any,
    defaultValue: PropTypes.any,
    disabled: PropTypes.bool,
    isMulti: PropTypes.bool,
    isClearable: PropTypes.bool,
    isSearchable: PropTypes.bool,
    isLoading: PropTypes.bool,
    borderStyle: PropTypes.oneOf([
        'none',
        'rounded-left',
        'rounded-right',
        'rounded',
    ]),
    noOptionsMessage: PropTypes.any,
    onChangeSearch: PropTypes.func,
    onChange: PropTypes.func,
    searchPlaceholder: PropTypes.string,
    wrapSelectedValue: PropTypes.bool,
}

Select.displayName = 'Select'

export default Select
