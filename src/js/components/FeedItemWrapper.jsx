import React from 'react'
import { useMeasure } from 'react-use'
import styled from 'styled-components'

const Wrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    margin-left: -10px;
    margin-right: -10px;

    > .FeedItemWrapperCol {
        display: flex;
        flex-direction: column;
        padding: 0 10px;

        > * {
            flex-grow: 1;
        }
    }
`

const FeedItemWrapper = ({ children, ...rest }) => {
    const [ref, { width }] = useMeasure()

    let colWidth = 1
    if (width >= 600 && width < 900) colWidth = 2
    else if (width >= 900) colWidth = 3

    return (
        <Wrapper ref={ref} {...rest}>
            {React.Children.map(children, (child) => {
                if (child)
                    return (
                        <div
                            className="FeedItemWrapperCol"
                            style={{
                                width: 100 / colWidth + '%',
                            }}
                        >
                            {React.cloneElement(child, {
                                ...child.props,
                                style: {
                                    ...child.props.style,
                                },
                            })}
                        </div>
                    )
            })}
        </Wrapper>
    )
}

export default FeedItemWrapper
