import React, { useEffect, useRef, useState } from 'react'
import styled from 'styled-components'

import Flexer from 'js/components/Flexer/Flexer'
import { getShortMonths } from 'js/lib/helpers/date/general'
import { getFullYearInt } from 'js/lib/helpers/date/parseDate'

import ChevronDownIcon from 'icons/chevron-down-small.svg'

const PickerGrid = styled.div`
    position: absolute;
    top: 32px;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: white;
    z-index: 1;
    overflow-y: auto;
    display: flex;
    align-items: center;
    flex-wrap: wrap;
    padding: 8px 0;

    > * {
        width: calc(100% / 3);
        height: 48px;
        display: flex;
        align-items: center;
        justify-content: center;
        font-size: ${(p) => p.theme.font.size.small};

        border-radius: ${(p) => p.theme.radius.small};

        &[aria-current='true'] {
            position: relative;
            color: ${(p) => p.theme.color.secondary.main};
            font-weight: ${(p) => p.theme.font.weight.semibold};
        }

        &:hover {
            background-color: ${(p) => p.theme.color.hover};
        }

        &:active {
            background-color: ${(p) => p.theme.color.active};
        }
    }
`

const Caption = ({ date, fromDate, toDate, onChange }) => {
    const selectedYear = date.getFullYear()
    const selectedMonth = date.getMonth()

    const currentYear = new Date().getFullYear()
    const fromYear = fromDate
        ? getFullYearInt(fromDate)
        : Math.min(selectedYear, currentYear) - 20
    const toYear = toDate
        ? getFullYearInt(toDate)
        : Math.max(selectedYear, currentYear) + 20

    const months = getShortMonths()

    const years = []
    for (let i = fromYear; i <= toYear; i += 1) years.push(i)

    const [showYears, setShowYears] = useState(false)
    const toggleShowYears = () => {
        setShowMonths(false)
        setShowYears(!showYears)
    }

    const refScrollYear = useRef()
    useEffect(() => {
        if (showYears && !!refScrollYear.current)
            refScrollYear.current.scrollIntoView({ block: 'center' })
    }, [showYears])

    const [showMonths, setShowMonths] = useState(false)

    const toggleShowMonths = ({ key, type }) => {
        if (type === 'click' || key === 'Enter' || key === ' ') {
            setShowYears(false)
            setShowMonths((show) => !show)
        }
    }

    const handleChangeMonth = (i) => {
        onChange(new Date(selectedYear, i))
    }

    const handleChangeYear = (year) => {
        onChange(new Date(year, selectedMonth))
    }

    return (
        <div className="DayPicker-Caption">
            <Flexer justifyContent="flex-start" gutter="none">
                <button
                    type="button"
                    className="DayPicker-CaptionButton"
                    onClick={toggleShowMonths}
                >
                    {months[selectedMonth]}
                    <ChevronDownIcon
                        style={{
                            transform: `scaleY(${showMonths ? -1 : 1})`,
                            marginLeft: '4px',
                        }}
                    />
                </button>
                {showMonths && (
                    <PickerGrid>
                        {months.map((month, i) => (
                            <button
                                key={month}
                                type="button"
                                aria-current={i === selectedMonth}
                                onClick={() => handleChangeMonth(i)}
                            >
                                {month}
                            </button>
                        ))}
                    </PickerGrid>
                )}

                <button
                    type="button"
                    className="DayPicker-CaptionButton"
                    onClick={toggleShowYears}
                >
                    {selectedYear}
                    <ChevronDownIcon
                        style={{
                            transform: `scaleY(${showYears ? -1 : 1})`,
                            marginLeft: '4px',
                        }}
                    />
                </button>
                {showYears && (
                    <PickerGrid>
                        {years.map((year) => (
                            <button
                                key={year}
                                type="button"
                                ref={
                                    (selectedYear && year === selectedYear) ||
                                    (!selectedYear && year === currentYear)
                                        ? refScrollYear
                                        : null
                                }
                                aria-current={year === selectedYear}
                                onClick={() => handleChangeYear(year)}
                            >
                                {year}
                            </button>
                        ))}
                    </PickerGrid>
                )}
            </Flexer>
        </div>
    )
}

export default Caption
