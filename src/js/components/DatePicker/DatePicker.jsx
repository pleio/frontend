import React, { useEffect, useRef, useState } from 'react'
import { DayPicker } from 'react-day-picker'
import { useTranslation } from 'react-i18next'
import { isSameMonth } from 'date-fns'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import { getActiveLocaleFile } from 'js/lib/helpers/date/general'

import Caption from './components/Caption'
import Wrapper from './DatePickerStyles'

const DateField = ({
    defaultMonth,
    value,
    fromDate,
    toDate,
    disabledDays,
    onDayClick,
    ...rest
}) => {
    const { t } = useTranslation()
    const refPicker = useRef()

    const [selected, setSelected] = useState(value ? new Date(value) : null)
    const [month, setMonth] = useState()

    const selectDay = (date) => {
        date && setSelected(date)
    }

    const handleMonthChange = (date) => {
        setMonth(date)
    }

    useEffect(() => {
        if (value) {
            setMonth(new Date(value))
        } else if (defaultMonth) {
            setMonth(new Date(defaultMonth))
        }
    }, [value, defaultMonth])

    const handleTodayButtonClick = () => {
        setMonth(new Date())
        setTimeout(() => {
            refPicker.current &&
                refPicker.current
                    .querySelector(`[data-today="true"] button`)
                    .focus()
        }, 100)
    }

    const isCurrentMonth = isSameMonth(month, new Date())

    return (
        <Wrapper ref={refPicker} {...rest}>
            <DayPicker
                disabled={disabledDays}
                fixedWeeks
                locale={getActiveLocaleFile()}
                mode="single"
                month={month}
                onDayClick={onDayClick}
                onMonthChange={handleMonthChange}
                selected={selected}
                onSelect={selectDay}
                showOutsideDays
                components={{
                    MonthCaption: (props) => {
                        if (!props?.calendarMonth?.date) return null

                        return (
                            <Caption
                                date={props.calendarMonth.date}
                                fromDate={fromDate}
                                toDate={toDate}
                                onChange={handleMonthChange}
                                {...props}
                            />
                        )
                    },
                }}
            />
            {!isCurrentMonth && (
                <Flexer>
                    <Button
                        size="small"
                        variant="secondary"
                        onClick={handleTodayButtonClick}
                        className="DatePickerGoToToday"
                    >
                        {t('entity-event.today')}
                    </Button>
                </Flexer>
            )}
        </Wrapper>
    )
}

export default DateField
