import React, { useEffect, useRef, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { formatISO, isAfter, isBefore, isValid } from 'date-fns'
import PropTypes from 'prop-types'

import { AriaLiveMessage } from 'js/components/AriaLive'
import DatePicker from 'js/components/DatePicker/DatePicker'
import IconButton from 'js/components/IconButton/IconButton'
import Popover from 'js/components/Popover/Popover'
import Textfield from 'js/components/Textfield/Textfield'
import Tooltip from 'js/components/Tooltip/Tooltip'
import {
    getDatePattern,
    updateDatePreserveTime,
} from 'js/lib/helpers/date/general'
import {
    parseDatePattern,
    parsePatternToDate,
} from 'js/lib/helpers/date/parseDate'
import { isSameDate } from 'js/lib/helpers/date/showDate'

import CalendarIcon from 'icons/calendar-day.svg'
import CrossIcon from 'icons/cross-small.svg'

import HideVisually from '../HideVisually/HideVisually'

import Wrapper from './DateFieldStyles'

const DateField = ({
    name,
    label,
    placeholder,
    value,
    disabled,
    fromDate,
    toDate,
    onChange,
    clearLabel,
    isClearable,
    ariaLabelIconButton,
    defaultMonth,
    autoComplete,
    ...rest
}) => {
    const { t } = useTranslation()

    const refTextField = useRef()

    const [instance, setInstance] = useState(null)

    const [inputValue, setInputValue] = useState('')
    const [isInvalidDate, setIsInvalidDate] = useState(false)

    useEffect(() => {
        setInputValue(value ? parseDatePattern(value) : '')
    }, [value])

    const handleChange = (date) => {
        const newDate = value ? updateDatePreserveTime(value, date) : date
        onChange(formatISO(newDate))
    }

    const handleChangeInput = ({ target }) => {
        setIsInvalidDate(false)
        setInputValue(target.value)
    }

    const handleKeyPress = (evt) => {
        if (evt.key === 'Enter') {
            evt.target.blur()
        }
    }

    const handleBlur = (evt) => {
        if (!evt.currentTarget.contains(evt.relatedTarget)) {
            setDateValue()
        }
    }

    const handleClear = () => {
        setIsInvalidDate(false)
        setInputValue('')
        onChange(null)
    }

    const setDateValue = () => {
        if (!inputValue) {
            handleClear()
            return
        }

        const newDate = parsePatternToDate(inputValue)

        if (isValid(newDate)) {
            // Check if date input is within bounds
            if (
                !isSameDate(newDate, value) &&
                ((fromDate && isBefore(newDate, fromDate)) ||
                    (toDate && isAfter(newDate, toDate)))
            ) {
                setIsInvalidDate(true)
            } else {
                setIsInvalidDate(false)
                handleChange(newDate)
            }
        } else {
            setIsInvalidDate(true)
        }
    }

    const handleDayClick = (day, { selected, disabled }) => {
        if (!selected && !disabled) {
            handleChange(day)
            instance.hide()
            setIsInvalidDate(false)
        }
    }

    const focusTextfield = () => {
        refTextField.current.focus()
    }

    const disabledDays = [
        {
            before: fromDate && new Date(fromDate),
            after: toDate && new Date(toDate),
        },
    ]

    const invalidDateMessage = (
        <>
            {label && <HideVisually>{label}: </HideVisually>}
            {t('error.invalid-date')}
        </>
    )

    return (
        <>
            <AriaLiveMessage
                message={
                    isInvalidDate
                        ? `${label}: ${t('error.invalid-date')}`
                        : null
                }
            />
            <Tooltip
                visible={isInvalidDate}
                content={invalidDateMessage}
                placement="top"
                theme="warn-white"
            >
                <Wrapper onBlur={handleBlur} {...rest}>
                    <Textfield
                        className="TextFieldWrapper"
                        ref={refTextField}
                        name={name}
                        label={label}
                        placeholder={
                            placeholder || getDatePattern().toLowerCase()
                        }
                        value={inputValue}
                        disabled={disabled}
                        autoComplete={autoComplete || 'off'}
                        hasError={isInvalidDate}
                        ElementBefore={
                            <div style={{ height: '100%' }}>
                                <Popover
                                    onCreate={setInstance}
                                    onHide={focusTextfield}
                                    content={
                                        <DatePicker
                                            defaultMonth={defaultMonth}
                                            value={value}
                                            onDayClick={handleDayClick}
                                            disabledDays={disabledDays}
                                            fromDate={fromDate}
                                            toDate={toDate}
                                            style={{ padding: '16px' }}
                                        />
                                    }
                                >
                                    <IconButton
                                        size="large"
                                        variant="secondary"
                                        radiusStyle="none"
                                        disabled={disabled}
                                        className="DatePickerButton"
                                        aria-label={
                                            ariaLabelIconButton ||
                                            t('entity-event.show-calendar')
                                        }
                                    >
                                        <CalendarIcon />
                                    </IconButton>
                                </Popover>
                            </div>
                        }
                        ElementAfter={
                            !!isClearable &&
                            !!inputValue && (
                                <IconButton
                                    variant="secondary"
                                    size="normal"
                                    onClick={handleClear}
                                    aria-label={
                                        clearLabel ||
                                        t('entity-event.clear-date')
                                    }
                                    style={{ marginRight: '4px' }}
                                >
                                    <CrossIcon />
                                </IconButton>
                            )
                        }
                        onChange={handleChangeInput}
                        onKeyPress={handleKeyPress}
                    />
                </Wrapper>
            </Tooltip>
        </>
    )
}

DateField.propTypes = {
    name: PropTypes.string,
    label: PropTypes.any,
    placeholder: PropTypes.string,
    value: PropTypes.string, // ISOString
    fromDate: PropTypes.string, // ISOString
    toDate: PropTypes.string, // ISOString
    onChange: PropTypes.func,
    clearLabel: PropTypes.string,
    isClearable: PropTypes.bool,
    ariaLabelIconButton: PropTypes.string,
}

export default DateField
