import styled from 'styled-components'

export default styled.div`
    position: relative;

    .DatePickerButton {
        height: 100%;
        background-color: transparent;
        outline-offset: -1px;
        border-right: 1px solid ${(p) => p.theme.color.grey[30]};
    }
`
