import React, { useContext, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'
import { useEventListener } from 'helpers'

import ChatSettings from 'js/chat/components/ChatSettings'
import chatFragment from 'js/chat/fragments/chat'
import Button from 'js/components/Button/Button'
import DropdownButton from 'js/components/DropdownButton/DropdownButton'
import FetchMore from 'js/components/FetchMore'
import Flexer from 'js/components/Flexer/Flexer'
import { H4 } from 'js/components/Heading'
import IconButton from 'js/components/IconButton/IconButton'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Modal from 'js/components/Modal/Modal'
import NavigationMenu from 'js/layout/Navigation/components/NavigationMenu'
import globalStateContext from 'js/lib/withGlobalState/globalStateContext'

import EditSquareSmallIcon from 'icons/edit-square-small.svg'
import ExternalIcon from 'icons/new-window-small.svg'
import OptionsIcon from 'icons/options.svg'

import NavigationChatItem from './NavigationChatItem'

const NavigationChatView = ({ user, visible, onHide }) => {
    const { setGlobalState } = useContext(globalStateContext)

    const { t } = useTranslation()

    const [maxHeight, setMaxHeight] = useState(window.innerHeight)
    const handleResize = () => {
        // Set max height to make menu fill the screen nicely on mobile
        // 100vh does not work because it includes the browser's address bar
        setMaxHeight(window.innerHeight)
    }

    const [showSettingsModal, setShowSettingsModal] = useState(false)

    useEventListener('resize', handleResize)

    const [queryLimit, setQueryLimit] = useState(10)
    const { loading, data, fetchMore, refetch, subscribeToMore } = useQuery(
        GET_CHATS,
        {
            skip: !visible,
            variables: {
                offset: 0,
                limit: queryLimit,
            },
        },
    )

    useEffect(() => {
        const unsubscribe = subscribeToMore({
            document: ON_CHATS_UPDATE,
            updateQuery: async (prev, { subscriptionData }) => {
                if (!subscriptionData.data) return prev

                const { action } = subscriptionData.data.onChatsUpdate

                if (action === 'refetch') {
                    refetch()
                }
            },
        })
        return () => unsubscribe()
    }, [subscribeToMore, refetch])

    useEffect(() => {
        if (visible === true) refetch()
    }, [visible, refetch])

    const newMessage = () => {
        setGlobalState((newState) => {
            newState.chatNewThread = true
        })
        onHide()
    }

    return (
        <>
            <NavigationMenu $maxHeight={maxHeight}>
                <div
                    className="NavigationMenuHeader"
                    style={{
                        padding: '16px 12px 16px 16px',
                    }}
                >
                    <H4
                        style={{
                            marginTop: '4px',
                            marginRight: '4px',
                        }}
                    >
                        {t('chat.latest-chats')}
                    </H4>
                    <Flexer>
                        <Button
                            size="normal"
                            variant="secondary"
                            onClick={newMessage}
                        >
                            <EditSquareSmallIcon
                                style={{ marginRight: '6px' }}
                            />
                            {t('chat.new-message')}
                        </Button>
                        <DropdownButton
                            options={[
                                {
                                    name: t('global.settings'),
                                    onClick: () => {
                                        setShowSettingsModal(true)
                                        onHide()
                                    },
                                },
                            ]}
                        >
                            <IconButton
                                size="normal"
                                variant="secondary"
                                tooltip={t('global.options')}
                                aria-label={t('action.show-options')}
                            >
                                <OptionsIcon />
                            </IconButton>
                        </DropdownButton>
                    </Flexer>
                </div>
                {!data || loading ? (
                    <LoadingSpinner
                        style={{ marginTop: '-8px', marginBottom: '16px' }}
                    />
                ) : (
                    <FetchMore
                        isScrollableBox
                        edges={data.chats.edges}
                        getMoreResults={(data) => data.chats.edges}
                        fetchMore={fetchMore}
                        fetchType="scroll"
                        fetchCount={10}
                        setLimit={setQueryLimit}
                        maxLimit={data.chats.total}
                        className="NavigationMenuList"
                    >
                        <ul>
                            {data.chats.edges.map((chat) => (
                                <NavigationChatItem
                                    key={`navigation-chats-${chat.guid}`}
                                    entity={chat}
                                    onHide={onHide}
                                />
                            ))}
                        </ul>
                    </FetchMore>
                )}

                <a
                    className="NavigationMenuFooter"
                    href="/chat"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    {t('chat.all-chats')}
                    <ExternalIcon style={{ marginLeft: '6px' }} />
                </a>
            </NavigationMenu>

            <Modal
                isVisible={showSettingsModal}
                onClose={() => setShowSettingsModal(false)}
                size="small"
                showCloseButton
                title={t('chat.chat-settings')}
            >
                <ChatSettings
                    user={user}
                    onClose={() => setShowSettingsModal(false)}
                />
            </Modal>
        </>
    )
}

const GET_CHATS = gql`
    query NavigationChats($offset: Int, $limit: Int) {
        chats(offset: $offset, limit: $limit) {
            total
            edges {
               ${chatFragment}
            }
        }
    }
`

const ON_CHATS_UPDATE = gql`
    subscription onChatsUpdate {
        onChatsUpdate {
            guid
            action
        }
    }
`

export default NavigationChatView
