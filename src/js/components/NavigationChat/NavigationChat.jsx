import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'
import { textContrastIsAA } from 'helpers/getContrast'
import { useTheme } from 'styled-components'

import Popover from 'js/components/Popover/Popover'
import NavigationAction from 'js/layout/Navigation/components/NavigationAction'
import NavigationBadge from 'js/layout/Navigation/components/NavigationBadge'

import ChatIcon from 'icons/chat.svg'
import ChatFillIcon from 'icons/chat-fill.svg'

import NavigationChatView from './NavigationChatView'

const NavigationChat = ({ user }) => {
    const { t } = useTranslation()
    const theme = useTheme()

    const { data, refetch, subscribeToMore } = useQuery(QUERY, {
        fetchPolicy: 'cache-and-network',
    })

    useEffect(() => {
        const unsubscribe = subscribeToMore({
            document: ON_CHATS_UPDATE,
            updateQuery: async (prev, { subscriptionData }) => {
                if (!subscriptionData.data) return prev

                const { action } = subscriptionData.data.onChatsUpdate

                if (action === 'refetch') {
                    refetch()
                }
            },
        })
        return () => unsubscribe()
    }, [subscribeToMore, refetch])

    const [visible, setVisible] = useState(false)
    const handleToggle = () => setVisible(!visible)
    const handleHide = () => setVisible(false)

    const totalUnread = data?.chats?.totalUnread
    const hasUnread = !!totalUnread && totalUnread !== 0

    return (
        <span>
            <Popover
                visible={visible}
                content={
                    <NavigationChatView
                        visible={visible}
                        onHide={handleHide}
                        user={user}
                    />
                }
                offset={[0, 0]}
                maxWidth={null}
                // Not onHide because content will disappear before transitioned
                onHidden={handleHide}
            >
                <NavigationAction
                    aria-label={
                        data
                            ? t('notifications.show-new-notifications', {
                                  count: totalUnread,
                              })
                            : null
                    }
                    onClick={handleToggle}
                >
                    {textContrastIsAA(theme.color.header.main) ? (
                        <ChatFillIcon />
                    ) : (
                        <ChatIcon />
                    )}
                    {hasUnread && (
                        <NavigationBadge>
                            {totalUnread < 100 ? totalUnread : '99+'}
                        </NavigationBadge>
                    )}
                </NavigationAction>
            </Popover>
        </span>
    )
}

const QUERY = gql`
    query UnreadChats {
        chats {
            totalUnread
            total
        }
    }
`

const ON_CHATS_UPDATE = gql`
    subscription onChatsUpdate {
        onChatsUpdate {
            guid
            action
        }
    }
`

export default NavigationChat
