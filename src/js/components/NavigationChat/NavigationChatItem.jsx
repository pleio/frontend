import React, { useContext } from 'react'
import { useTranslation } from 'react-i18next'
import styled, { css } from 'styled-components'

import ChatAvatar from 'js/chat/components/ChatAvatar'
import getChatContainer from 'js/chat/helpers/getChatContainer'
import globalStateContext from 'js/lib/withGlobalState/globalStateContext'

const Wrapper = styled.li`
    position: relative;

    .NavigationChatItemButton {
        display: flex;
        align-items: center;
        width: 100%;
        padding: 12px 16px;

        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        color: ${(p) => p.theme.color.text.black};
        text-align: left;

        user-select: none;

        &:hover {
            background-color: ${(p) => p.theme.color.hover};
        }

        &:active {
            background-color: ${(p) => p.theme.color.active};
        }
    }

    &:first-child {
        border-top: 1px solid ${(p) => p.theme.color.grey[30]};
    }

    &:not(:first-child) {
        border-top: 1px solid ${(p) => p.theme.color.grey[20]};
    }

    .NavigationChatItemAvatar {
        margin-right: 12px;
    }

    .NavigationChatItemName {
        flex-grow: 1;

        ${(p) =>
            p.$unread &&
            css`
                font-weight: ${(p) => p.theme.font.weight.bold};
            `};

        &.muted {
            color: ${(p) => p.theme.color.text.grey};
        }
    }
`

const NavigationChatItem = ({ entity, onHide }) => {
    const { t } = useTranslation()

    const { unread, isNotificationPushEnabled } = entity

    const { setGlobalState } = useContext(globalStateContext)

    const openChat = () => {
        setGlobalState((newState) => {
            newState.activeMainThread = entity
        })
        onHide()
    }

    const container = getChatContainer(entity.container)

    if (!container) {
        console.error(t('error.chat-container-not-found'), entity)
        return null
    }

    return (
        <Wrapper $unread={unread}>
            <button
                type="button"
                onClick={openChat}
                className="NavigationChatItemButton"
            >
                <ChatAvatar
                    container={container}
                    className="NavigationChatItemAvatar"
                />
                <div
                    className={`NavigationChatItemName ${isNotificationPushEnabled ? '' : 'muted'}`}
                >
                    {container.title}
                    {unread > 0 ? ` (${unread})` : null}
                </div>
            </button>
        </Wrapper>
    )
}

export default NavigationChatItem
