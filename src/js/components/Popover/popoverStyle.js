import { css } from 'styled-components'

export default css`
    .tippy-box[data-theme^='popover'] {
        background-color: ${(p) => p.theme.menu.bg};
        border-radius: ${(p) => p.theme.radius.normal};
        box-shadow: ${(p) => p.theme.menu.shadow};

        color: ${(p) => p.theme.menu.color};
        font-size: ${(p) => p.theme.font.size.normal};
        line-height: ${(p) => p.theme.font.lineHeight.normal};
        font-weight: ${(p) => p.theme.font.weight.normal};
        text-align: left;

        > .tippy-content {
            padding: 0;
            overflow: auto;

            svg #bg {
                fill: ${(p) => p.theme.menu.bg};
            }
        }

        > .tippy-arrow:before {
            transform: scale(0.625);
        }

        &[data-placement^='top'] > .tippy-arrow:before {
            border-top-color: ${(p) => p.theme.menu.bg};
        }
        &[data-placement^='bottom'] > .tippy-arrow:before {
            border-bottom-color: ${(p) => p.theme.menu.bg};
        }
        &[data-placement^='left'] > .tippy-arrow:before {
            border-left-color: ${(p) => p.theme.menu.bg};
        }
        &[data-placement^='right'] > .tippy-arrow:before {
            border-right-color: ${(p) => p.theme.menu.bg};
        }
        &[data-placement$='start'] > .tippy-arrow {
            transform: translate(16px, 0px) !important;
        }
    }

    .tippy-box[data-theme='popover-hidden'] {
        > .tippy-content {
            border-radius: ${(p) => p.theme.radius.normal};
            overflow: hidden; // hidden on tippy-box hides arrow
        }
    }

    .tippy-box[data-theme='popover-visible'] {
        > .tippy-content {
            border-radius: ${(p) => p.theme.radius.normal};
            overflow: visible;
        }
    }
`
