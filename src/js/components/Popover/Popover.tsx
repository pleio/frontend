import React, { useCallback } from 'react'
import TippyPlugin, { TippyProps } from '@tippyjs/react'

const hideOnPopperBlur = {
    name: 'hideOnPopperBlur',
    defaultValue: true,
    fn(instance: any) {
        return {
            onCreate() {
                instance.popper.addEventListener(
                    'focusout',
                    (event: FocusEvent) => {
                        if (
                            instance.props.hideOnPopperBlur &&
                            event.relatedTarget &&
                            !instance.popper.contains(event.relatedTarget)
                        ) {
                            instance.hide()
                        }
                    },
                )
            },
        }
    },
}

export interface Props extends TippyProps {
    allowHTML?: boolean
    ignoreAttributes?: boolean
    theme?: string
    maxWidth?: string
    trigger?: string
    animation?: string
    duration?: [number, number]
    arrow?: boolean
    offset?: [number, number]
    interactive?: boolean
    visible?: boolean
    popperOptions?: any
    plugins?: any[]
    overflowVisible?: boolean
    overflowHidden?: boolean
    onShow?: (instance: any) => void
    onHide?: (instance: any) => void
    onClickOutside?: (instance: any) => void
    content: React.ReactElement
    children: React.ReactElement
}

const Popover = ({
    visible,
    delay,
    overflowVisible,
    overflowHidden,
    onShow,
    onHide,
    onClickOutside,
    allowHTML = true,
    ignoreAttributes = true,
    maxWidth = 'none',
    trigger = 'click',
    placement = 'bottom',
    animation = 'shift-away',
    duration = [200, 150],
    arrow = true,
    offset = [0, 10],
    interactive = true,
    popperOptions = {
        strategy: 'fixed', // Prevents scrollbar in parent
    },
    plugins = [hideOnPopperBlur],
    ...rest
}: Props) => {
    const handleKeydown = useCallback((evt: any, instance: any) => {
        if (evt.key === 'Escape' && instance.state.isVisible) {
            instance.hide()
            // Prevent modal closing
            evt.stopPropagation()
        }
    }, [])

    const handleShow = (instance: any) => {
        document.addEventListener('keydown', (evt) =>
            handleKeydown(evt, instance),
        )
        onShow?.(instance)
    }

    const handleHide = (instance: any) => {
        document.removeEventListener('keydown', (evt) =>
            handleKeydown(evt, instance),
        )
        onHide?.(instance)
    }

    const handleClickOutside = (instance: any) => {
        instance.hide()
        onClickOutside?.(instance)
    }

    return (
        <TippyPlugin
            {...rest}
            theme={
                overflowHidden
                    ? 'popover-hidden'
                    : overflowVisible
                      ? 'popover-visible'
                      : 'popover'
            }
            trigger={visible === undefined ? trigger : undefined} // trigger is obsolete when visible is defined
            visible={visible}
            delay={delay ? [1000, 0] : null}
            onShow={handleShow}
            onHide={handleHide}
            onClickOutside={handleClickOutside}
            allowHTML={allowHTML}
            ignoreAttributes={ignoreAttributes}
            maxWidth={maxWidth}
            placement={placement}
            animation={animation}
            duration={duration}
            arrow={arrow}
            offset={offset}
            interactive={interactive}
            popperOptions={popperOptions}
            plugins={plugins}
        />
    )
}

export default Popover
