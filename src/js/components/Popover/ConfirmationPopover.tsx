import React from 'react'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import { H5 } from 'js/components/Heading'
import Popover from 'js/components/Popover/Popover'
import Spacer from 'js/components/Spacer/Spacer'
import Text from 'js/components/Text/Text'

const ConfirmationPopover = ({
    visible,
    setVisible,
    title,
    description,
    onCancel,
    onConfirm,
    loading,
    error,
    children,
}) => {
    const { t } = useTranslation()

    const handleCancel = () => {
        setVisible(false)
        onCancel?.()
    }

    return (
        <Popover
            visible={visible}
            onClickOutside={() => setVisible(false)}
            placement="top"
            offset={[0, 8]}
            content={
                <Spacer
                    spacing="tiny"
                    style={{
                        padding: '12px 16px',
                        textAlign: 'center',
                    }}
                >
                    <div>
                        <H5>{title}</H5>
                        <Text size="small">{description}</Text>
                    </div>

                    <Errors errors={error} />

                    <Flexer gutter="small">
                        <Button
                            size="small"
                            variant="tertiary"
                            type="button"
                            onClick={handleCancel}
                        >
                            {t('action.cancel')}
                        </Button>
                        <Button
                            size="small"
                            variant="primary"
                            loading={loading}
                            onClick={onConfirm}
                        >
                            {t('action.confirm')}
                        </Button>
                    </Flexer>
                </Spacer>
            }
        >
            {children}
        </Popover>
    )
}

export default ConfirmationPopover
