import { getReadableColor } from 'helpers/getContrast'
import styled, { css } from 'styled-components'

import Card from 'js/components/Card/Card'
import shouldForwardProp from 'js/lib/helpers/shouldForwardProp'

export interface Props {
    divider?: boolean
    grow?: boolean
    noTopPadding?: boolean
    noBottomPadding?: boolean
    backgroundColor?: string
}

const Section = styled.section
    .withConfig({
        shouldForwardProp: shouldForwardProp([
            'divider',
            'grow',
            'noTopPadding',
            'noBottomPadding',
            'backgroundColor',
        ]),
    })
    .attrs((props) => ({
        divider: false,
        grow: false,
        ...props,
    }))<Props>`
    flex-grow: ${(p) => (p.grow ? 1 : 0)};
    padding: 40px 0;
    transition: box-shadow ${(p) => p.theme.transition.fast};

    ${(p) =>
        p.noTopPadding &&
        css`
            padding-top: 0;
        `};

    ${(p) =>
        p.noBottomPadding &&
        css`
            padding-bottom: 0;
        `};

    ${(p) =>
        p.backgroundColor &&
        css<Props>`
            background-color: ${(p) => p.backgroundColor};
            color: ${(p) => getReadableColor(p.backgroundColor)};
        `};

    ${(p) =>
        (p.backgroundColor === '#FFFFFF' || p.backgroundColor === 'white') &&
        css`
            ${Card} {
                box-shadow: 0 0 0 1px ${(p) => p.theme.color.grey['30-alpha']};
            }
        `}

    ${(p) =>
        p.divider &&
        css`
            &:not(:last-child) {
                box-shadow: 0 1px 0 ${(p) => p.theme.color.grey[20]};
            }
        `}
`

Section.displayName = 'Section'
export default Section
