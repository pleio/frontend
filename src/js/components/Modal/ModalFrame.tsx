import React from 'react'
import { FocusOn } from 'react-focus-on'
import { AnimatePresence, AnimationProps, motion } from 'framer-motion'
import { media } from 'helpers'
import styled, { css } from 'styled-components'

import shouldForwardProp from 'js/lib/helpers/shouldForwardProp'

import ModalBox, { ModalBoxProps } from './ModalBox'

interface ModalWrapperProps extends AnimationProps {
    containHeight?: boolean
    canClose?: boolean
}

export interface ModalFrameProps extends ModalBoxProps {
    isVisible: boolean
}

const Wrapper = styled(motion.div).withConfig({
    shouldForwardProp: shouldForwardProp(['hideContent', 'canClose']),
})<ModalWrapperProps>`
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 1050;

    ${(p) =>
        !p.containHeight &&
        css`
            overflow-y: auto;
        `};

    .ModalContainer {
        position: relative;
        min-height: 100%; // Needed for overflow display (height does not work)
        display: flex;
        align-items: center;

        ${media.mobileLandscapeUp`
            padding: ${(p) => p.theme.headerBarHeight}px 0;
        `};
    }

    .ModalBackdrop {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: rgba(0, 0, 0, 0.2);
        cursor: ${(p) => (p.canClose ? 'pointer' : 'default')};
    }
`

const ModalFrame = ({
    isVisible,
    onClose,
    containHeight,
    showCloseButton,
    ...rest
}: ModalFrameProps) => {
    // Using this instead of onEscapeKey of 'react-focus-on' because that fires before other elements (like Select)
    const handleKeyDown = (evt: KeyboardEvent) => {
        switch (evt.key) {
            case 'Escape':
                evt.preventDefault()
                onClose && onClose()
                break
        }
    }

    return (
        <AnimatePresence>
            {isVisible && (
                <>
                    <FocusOn
                        /* eslint-disable-next-line */
                        autoFocus={true}
                        onActivation={() => {
                            window.addEventListener('keydown', handleKeyDown)
                        }}
                        onDeactivation={() => {
                            window.removeEventListener('keydown', handleKeyDown)
                        }}
                    >
                        <Wrapper
                            initial={{ opacity: 0 }}
                            animate={{ opacity: 1 }}
                            exit={{ opacity: 0 }}
                            transition={{ duration: 0.25 }}
                            canClose={!!onClose}
                            containHeight={containHeight}
                        >
                            <div className="ModalContainer">
                                {/* eslint-disable-next-line */}
                                <div
                                    className="ModalBackdrop"
                                    onClick={onClose}
                                />
                                <ModalBox
                                    onClose={onClose}
                                    showCloseButton={
                                        onClose ? showCloseButton : false
                                    }
                                    containHeight={containHeight}
                                    {...rest}
                                />
                            </div>
                        </Wrapper>
                    </FocusOn>
                </>
            )}
        </AnimatePresence>
    )
}

export default ModalFrame
