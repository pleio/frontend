import React from 'react'
import { createPortal } from 'react-dom'

import ModalFrame, { ModalFrameProps } from './ModalFrame'

/**
 * Modal will capture focus
 * The first element may be the close button. In order to manually set the focus element,
 * provide `data-autofocus` on the prop you want focus on.
 */
const Modal: React.FC<ModalFrameProps> = (props) =>
    createPortal(
        <ModalFrame {...props} />,
        document.querySelector('#modal-root'),
    )

export default Modal
