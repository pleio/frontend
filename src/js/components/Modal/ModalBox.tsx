import React from 'react'
import { useTranslation } from 'react-i18next'
import { media } from 'helpers'
import styled, { css } from 'styled-components'

import Card from 'js/components/Card/Card'
import IconButton from 'js/components/IconButton/IconButton'
import shouldForwardProp from 'js/lib/helpers/shouldForwardProp'

import CrossIcon from 'icons/cross-large.svg'

export interface ModalBoxProps {
    size: 'tiny' | 'small' | 'normal' | 'large'

    children?: React.ReactElement | React.ReactElement[]
    containHeight?: boolean // Maximizes content height to 100% of viewport
    growHeight?: boolean // Grow content height to 100% of viewport
    hideBackground?: boolean
    hideContent?: boolean
    maxWidth?: string // Set a custom max-width for the content (`size` will override this)
    noPadding?: boolean
    onClose?: () => void // Will close when pressing ESC or clicking outside of content
    showCloseButton?: boolean // Show close button next to title
    title?: string
}

export interface WrapperProps
    extends Omit<ModalBoxProps, 'hideContent' | 'onClose'> {}

const Wrapper = styled(Card).withConfig({
    shouldForwardProp: shouldForwardProp([
        'size',
        'containHeight',
        'growHeight',
        'hideBackground',
        'noPadding',
        'radiusSize',
        'maxWidth',
        'showCloseButton',
    ]),
})<WrapperProps>`
    width: 100%;
    margin: 0 auto;
    max-width: ${(p) => p.maxWidth};

    ${(p) =>
        p.size === 'tiny' &&
        css`
            max-width: 300px;

            .ModalContent {
                font-size: ${(p) => p.theme.font.size.small};
                line-height: ${(p) => p.theme.font.lineHeight.small};
            }
        `};

    ${(p) =>
        p.size === 'small' &&
        css`
            max-width: 400px;

            .ModalContent {
                font-size: ${(p) => p.theme.font.size.small};
                line-height: ${(p) => p.theme.font.lineHeight.small};
            }
        `};

    ${(p) =>
        (p.size === 'normal' || p.size === 'large') &&
        css`
            .ModalContent {
                font-size: ${(p) => p.theme.font.size.normal};
                line-height: ${(p) => p.theme.font.lineHeight.normal};
            }

            ${media.mobilePortrait`
                position: absolute;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                border-radius: 0;
                overflow-y: auto;
            `};

            ${media.mobileLandscapeUp`
                max-width: ${(p) =>
                    p.maxWidth
                        ? p.maxWidth
                        : p.size === 'normal'
                          ? '600px'
                          : '1000px'};
            `};
        `};

    ${(p) =>
        p.containHeight &&
        css`
            display: flex;
            flex-direction: column;

            ${media.mobileLandscapeUp`
                max-height: calc(100vh - (${(p) =>
                    p.theme.headerBarHeight}px * 2));
            `};
        `};

    ${(p) =>
        p.growHeight &&
        css`
            align-self: stretch;
            display: flex;
            flex-direction: column;

            .ModalContent {
                flex-grow: 1;
                display: flex;
                flex-direction: column;
            }
        `};

    ${(p) =>
        p.hideBackground &&
        css`
            background-color: transparent;
            box-shadow: none;
        `};

    ${(p) =>
        p.noPadding &&
        css`
            .ModalContent {
                padding: 0 !important;
            }
        `};

    .ModalHeader {
        ${media.mobileLandscapeDown`
            padding: ${(p) => p.theme.padding.horizontal.small} ${(p) =>
                p.theme.padding.horizontal.small} 0;

            ${(p) =>
                p.showCloseButton &&
                css`
                    padding-right: calc(
                        ${(p) => p.theme.padding.horizontal.small} + 40px
                    );
                `};
        `};

        ${media.tabletUp`
            padding: ${(p) => p.theme.padding.horizontal.normal} ${(p) =>
                p.theme.padding.horizontal.normal} 0;

            ${(p) =>
                p.$showCloseButton &&
                css`
                    padding-right: calc(
                        ${(p) => p.theme.padding.horizontal.normal} + 40px
                    );
                `};
        `};
    }

    .ModalTitle {
        font-family: ${(p) => p.theme.fontHeading.family};
        font-weight: ${(p) => p.theme.fontHeading.weight.bold};
        color: ${(p) => p.theme.color.primary.main};

        ${(p) =>
            (p.size === 'tiny' || p.size === 'small') &&
            css`
                font-size: ${(p) => p.theme.fontHeading.size.small};
                line-height: ${(p) => p.theme.fontHeading.lineHeight.small};
            `};

        ${(p) =>
            (p.size === 'normal' || p.size === 'large') &&
            css`
                font-size: ${(p) => p.theme.fontHeading.size.normal};
                line-height: ${(p) => p.theme.fontHeading.lineHeight.normal};
            `};
    }

    .ModalHeader + .ModalContent {
        margin-top: -8px;
    }

    .ModalContent {
        ${media.mobileLandscapeDown`
            padding: ${(p) =>
                `${p.theme.padding.vertical.small} ${p.theme.padding.horizontal.small}`};
        `};

        ${media.tabletUp`
            padding: ${(p) =>
                `${p.theme.padding.vertical.normal} ${p.theme.padding.horizontal.normal}`};
        `};

        ${(p) =>
            p.containHeight &&
            css`
                overflow: hidden;
                display: flex;
                flex-direction: column;
            `};
    }

    .ModalCloseButton {
        position: absolute;

        ${media.mobileLandscapeDown`
            top: calc(${(p) => p.theme.padding.horizontal.small} - 5px);
            right: ${(p) => p.theme.padding.horizontal.small};
        `};

        ${media.tabletUp`
            top: calc(${(p) => p.theme.padding.horizontal.normal} - 5px);
            right: ${(p) => p.theme.padding.horizontal.normal};
        `};
    }
`

const ModalBox = ({
    containHeight,
    growHeight,
    hideBackground,
    maxWidth,
    noPadding,
    onClose,
    showCloseButton,
    size,
    title,
    children,
}: ModalBoxProps) => {
    const { t } = useTranslation()

    return (
        <Wrapper
            containHeight={containHeight}
            growHeight={growHeight}
            hideBackground={hideBackground}
            maxWidth={maxWidth}
            noPadding={noPadding}
            radiusSize="large" // Card
            showCloseButton={showCloseButton}
            size={size}
            role="dialog"
            aria-labelledby="modal-title"
        >
            {showCloseButton && (
                <IconButton
                    className="ModalCloseButton"
                    variant="secondary"
                    size="large"
                    aria-label={t('action.close-modal')}
                    onClick={onClose}
                >
                    <CrossIcon />
                </IconButton>
            )}

            {title && (
                <div className="ModalHeader">
                    <h2 className="ModalTitle" id="modal-title">
                        {title}
                    </h2>
                </div>
            )}

            <div className="ModalContent">{children}</div>
        </Wrapper>
    )
}

export default ModalBox
