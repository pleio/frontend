import React from 'react'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import Modal from 'js/components/Modal/Modal'

const ConfirmationModal = ({
    title,
    cancelLabel,
    confirmLabel,
    isVisible,
    onCancel,
    onConfirm,
    loading,
    disabled,
    children,
}) => {
    const { t } = useTranslation()

    return (
        <Modal
            title={title}
            isVisible={isVisible}
            size="small"
            onClose={loading ? null : onCancel}
        >
            {children}
            <Flexer mt>
                <Button size="normal" variant="tertiary" onClick={onCancel}>
                    {cancelLabel || t('action.no-cancel')}
                </Button>
                <Button
                    size="normal"
                    variant="primary"
                    loading={loading}
                    disabled={disabled}
                    onClick={onConfirm}
                >
                    {confirmLabel || t('action.yes-confirm')}
                </Button>
            </Flexer>
        </Modal>
    )
}

export default ConfirmationModal
