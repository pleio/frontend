import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { getVideoFromUrl } from 'helpers'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

import IconButton from 'js/components/IconButton/IconButton'
import Img from 'js/components/Img/Img'
import Modal from 'js/components/Modal/Modal'
import Text from 'js/components/Text/Text'
import VideoModal from 'js/components/VideoModal'

import PlayIcon from 'icons/play.svg'

const Wrapper = styled.div`
    position: relative;
    width: 100%;
    user-select: none;

    ${(p) =>
        p.$size === 'landscape-small' &&
        css`
            min-height: 100px;
            padding-bottom: 25%;
        `};

    ${(p) =>
        p.$size === 'landscape' &&
        css`
            min-height: 100px;
            padding-bottom: 35%;
        `};

    ${(p) =>
        p.$size === 'landscape-large' &&
        css`
            min-height: 100px;
            padding-bottom: 60%;
        `};

    ${(p) =>
        !!p.onMouseDown &&
        css`
            cursor: grab;

            &:active {
                cursor: grabbing;
            }

            > * {
                pointer-events: none; // prevent dragging of children
            }
        `}

    ${(p) =>
        p.$borderRadius &&
        css`
            .cover-image--responsive img {
                border-radius: ${p.$borderRadius};
            }
        `};

    ${(p) =>
        p.$size !== 'full' &&
        css`
            .cover-image--responsive {
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;

                img {
                    width: 100%;
                    height: 100%;
                }
            }
        `};

    ${(p) =>
        p.$maxHeight &&
        css`
            img {
                max-height: ${p.$maxHeight} !important;
            }
        `};

    .cover-image--link {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }

    .cover-image--video-button {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        margin: auto;
    }
`

const CoverImage = ({
    featured = {},
    uploadedImage,
    url,
    openUrlInNewWindow,
    hideCaption,
    onMouseDown,
    size = 'landscape',
    maxHeight,
    videoButtonSize = 'large',
    borderRadius,
    onLoad,
    ...rest
}) => {
    const { t } = useTranslation()

    const {
        image,
        positionY,
        video,
        videoTitle,
        videoThumbnailUrl,
        alt,
        caption,
    } = featured

    const [showVideoModal, setShowVideoModal] = useState(false)
    const toggleVideoModal = () => {
        setShowVideoModal(!showVideoModal)
    }

    const videoObject = video && getVideoFromUrl(video)

    if (!uploadedImage && !image && !video) return null

    const LinkElement = openUrlInNewWindow ? 'a' : Link
    const linkProps = openUrlInNewWindow
        ? {
              href: url,
              target: '_blank',
              rel: 'noopener noreferrer',
          }
        : { to: url }

    const imgFitCover = size !== 'full'
    const imgObjectProps = imgFitCover
        ? {
              objectFit: 'cover',
              objectPosition: `center ${typeof positionY === 'number' ? `${positionY}%` : 'center'}`, // positionY = 0 is also a valid value
          }
        : {}

    return (
        <figure>
            <Wrapper
                $size={size}
                $imgFitCover={imgFitCover}
                $maxHeight={maxHeight}
                $positionY={positionY}
                $borderRadius={borderRadius}
                onMouseDown={onMouseDown}
                {...rest}
            >
                <Img
                    src={image?.download || videoThumbnailUrl}
                    alt={alt}
                    align="center"
                    onLoad={onLoad}
                    className="cover-image--responsive"
                    {...imgObjectProps}
                />
                {!!url && (
                    <LinkElement
                        {...linkProps}
                        className="cover-image--link"
                        tabIndex="-1"
                    />
                )}
                {!!videoObject && (
                    <>
                        <IconButton
                            className="cover-image--video-button"
                            variant="secondary"
                            size={videoButtonSize}
                            dropShadow
                            onClick={toggleVideoModal}
                            aria-label={t('action.play-video')}
                        >
                            <PlayIcon />
                        </IconButton>
                        <Modal
                            isVisible={showVideoModal}
                            onClose={toggleVideoModal}
                            maxWidth="860px"
                            hideBackground
                            size="normal"
                            noPadding
                        >
                            <VideoModal
                                id={videoObject.id}
                                type={videoObject.type}
                                title={videoTitle}
                                onClose={toggleVideoModal}
                            />
                        </Modal>
                    </>
                )}
            </Wrapper>
            {!hideCaption && caption && (
                <Text
                    as="figcaption"
                    size="small"
                    variant="grey"
                    style={{
                        marginTop: '12px',
                    }}
                    dangerouslySetInnerHTML={{ __html: caption }}
                />
            )}
        </figure>
    )
}

CoverImage.propTypes = {
    size: PropTypes.oneOf([
        'landscape-small',
        'landscape',
        'landscape-large',
        'cover',
        'full',
    ]), // image size
    videoButtonSize: PropTypes.oneOf(['small', 'normal', 'large']),
}

export default CoverImage
