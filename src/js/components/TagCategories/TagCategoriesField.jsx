import React from 'react'
import { useTranslation } from 'react-i18next'
import { produce } from 'immer'
import PropTypes from 'prop-types'

import FilterWrapper from 'js/components/FilterWrapper'
import { H4 } from 'js/components/Heading'
import Select from 'js/components/Select/Select'

const TagCategoriesField = ({
    tagCategories,
    value,
    label,
    setTags,
    ...rest
}) => {
    const onChange = (tags, categoryName) => {
        setTags(
            produce((draft) => {
                const category = draft.find((cat) => cat.name === categoryName)
                if (category) {
                    category.values = tags
                } else {
                    draft.push({ name: categoryName, values: tags })
                }
            }),
        )
    }

    const { t } = useTranslation()

    if (!tagCategories?.length) return null

    return (
        <div {...rest}>
            <H4 style={{ marginBottom: '8px' }}>{label || t('form.tags')}</H4>
            <FilterWrapper>
                {tagCategories.map((category, i) => {
                    const { name, values } = category
                    const options = values.map((tag) => ({
                        value: tag,
                        label: tag,
                    }))

                    const handleChange = (val) => {
                        onChange(val, name)
                    }

                    const selectedValues =
                        value.find((category) => category.name === name)
                            ?.values || []

                    return (
                        <Select
                            key={`${name}_${i}`}
                            name={name}
                            label={name}
                            options={options}
                            value={selectedValues}
                            onChange={handleChange}
                            isMulti
                            {...rest}
                        />
                    )
                })}
            </FilterWrapper>
        </div>
    )
}

TagCategoriesField.propTypes = {
    tagCategories: PropTypes.array.isRequired,
    value: PropTypes.any,
    label: PropTypes.string,
}

export default TagCategoriesField
