import React from 'react'
import PropTypes from 'prop-types'

import { Col, Row } from 'js/components/Grid/Grid'
import Select from 'js/components/Select/Select'

const TagCategoriesField = ({ tagCategories, value, onChange }) => {
    const tagsPerCategory = {}
    tagCategories.forEach((category) => {
        tagsPerCategory[category.name] = []
        category.values.forEach((tag) => {
            if (value.includes(tag)) {
                tagsPerCategory[category.name].push(tag)
            }
        })
    })

    const setTags = (tags, category) => {
        tagsPerCategory[category] = tags
        const values = [].concat.apply([], Object.values(tagsPerCategory))
        onChange(values)
    }

    return (
        <Row>
            {tagCategories.map((category, i) => {
                const { name, values } = category

                const options = values.map((tag) => ({
                    value: tag,
                    label: tag,
                }))

                const handleChange = (val) => {
                    setTags(val, name)
                }

                return (
                    <Col
                        mobile={1 / 2}
                        mobileLandscapeUp={1 / 3}
                        style={{ marginBottom: '8px' }}
                        key={`${name}_${i}`}
                    >
                        <Select
                            name={name}
                            label={name}
                            options={options}
                            value={tagsPerCategory[name]}
                            onChange={handleChange}
                            isMulti
                        />
                    </Col>
                )
            })}
        </Row>
    )
}

TagCategoriesField.propTypes = {
    tagCategories: PropTypes.array.isRequired,
    value: PropTypes.array.isRequired, // Flat array of tags
}

export default TagCategoriesField
