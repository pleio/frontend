import React from 'react'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import Modal from 'js/components/Modal/Modal'

import Errors from './Errors'

const ErrorModal = ({ showModal, setShowModal, error }) => {
    const { t } = useTranslation()

    return (
        <Modal
            size="small"
            title={t('error.error-occured')}
            isVisible={showModal}
            onClose={() => setShowModal(false)}
            showCloseButton={true}
        >
            <Errors errors={error} />

            <Flexer mt>
                <Button
                    size="normal"
                    variant="primary"
                    onClick={() => setShowModal(false)}
                >
                    {t('action.close-modal')}
                </Button>
            </Flexer>
        </Modal>
    )
}

export default ErrorModal
