import React, { HTMLAttributes } from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import { AriaLiveMessage } from 'js/components/AriaLive'
import shouldForwardProp from 'js/lib/helpers/shouldForwardProp'

export interface Props extends HTMLAttributes<HTMLDivElement> {
    color: string
}

const Wrapper = styled.div.withConfig({
    shouldForwardProp: shouldForwardProp(['color']),
})<Props>`
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    overflow: hidden;

    svg {
        width: 20px;
        height: 20px;
        animation: rotate 1.5s linear infinite;
        transform-origin: center center;

        circle {
            stroke-dasharray: 1, 200;
            stroke-dashoffset: 0;
            animation: dash 1.5s ease-in-out infinite;
            stroke-linecap: round;
            stroke: ${(p) => p.color || p.theme.color.icon.grey};
        }

        @keyframes rotate {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

        @keyframes dash {
            0% {
                stroke-dasharray: 1, 200;
                stroke-dashoffset: 0;
            }
            50% {
                stroke-dasharray: 89, 200;
                stroke-dashoffset: -35px;
            }
            100% {
                stroke-dasharray: 89, 200;
                stroke-dashoffset: -124px;
            }
        }
    }
`

const LoadingSpinner = ({ color, ...rest }: Props) => {
    const { t } = useTranslation()
    return (
        <Wrapper color={color} {...rest}>
            <AriaLiveMessage message={`${t('status.loading')}...`} />
            <svg viewBox="25 25 50 50" aria-hidden>
                <circle
                    cx="50"
                    cy="50"
                    r="20"
                    fill="none"
                    strokeWidth={color === 'white' ? '6' : '5'}
                    strokeMiterlimit="10"
                />
            </svg>
        </Wrapper>
    )
}

export default LoadingSpinner
