import React, { ForwardedRef, HTMLAttributes } from 'react'
import styled, { css } from 'styled-components'

import shouldForwardProp from 'js/lib/helpers/shouldForwardProp'

export interface Props extends HTMLAttributes<HTMLDivElement> {
    children: React.ReactElement | string
    ref?: ForwardedRef<HTMLElement>
    size?: 'tiny' | 'small' | 'normal' | 'large'
    textAlign?: 'left' | 'center' | 'right'
    variant?: 'black' | 'grey' | 'warn'
}

const Text = styled.div
    .withConfig({
        shouldForwardProp: shouldForwardProp(['variant', 'textAlign', 'size']),
    })
    .attrs((props) => ({ size: 'normal', variant: 'black', ...props }))<Props>`
    font-weight: ${(p) => p.theme.font.weight.normal};
    color: ${(p) => p.variant === 'black' && p.theme.color.text.black};
    color: ${(p) => p.variant === 'grey' && p.theme.color.text.grey};
    color: ${(p) => p.variant === 'warn' && p.theme.color.warn.main};

    ${(p) =>
        p.textAlign &&
        css<Props>`
            text-align: ${(p) => p.textAlign};
        `};

    ${(p) =>
        p.size === 'tiny' &&
        css`
            font-size: ${(p) => p.theme.font.size.tiny};
            line-height: ${(p) => p.theme.font.lineHeight.tiny};
        `};

    ${(p) =>
        p.size === 'small' &&
        css`
            font-size: ${(p) => p.theme.font.size.small};
            line-height: ${(p) => p.theme.font.lineHeight.small};
        `};

    ${(p) =>
        p.size === 'normal' &&
        css`
            font-size: ${(p) => p.theme.font.size.normal};
            line-height: ${(p) => p.theme.font.lineHeight.normal};
        `};

    ${(p) =>
        p.size === 'large' &&
        css`
            font-size: ${(p) => p.theme.font.size.large};
            line-height: ${(p) => p.theme.font.lineHeight.large};
        `};

    a,
    button {
        display: inline;
        color: ${(p) => p.theme.color.primary.main};
        text-decoration: underline;
    }
`

export default Text
