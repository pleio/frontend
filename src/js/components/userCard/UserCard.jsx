import React from 'react'
import { Link } from 'react-router-dom'

import Avatar from './components/Avatar'
import Wrapper from './components/Wrapper'

const UserCard = ({ entity, 'data-feed': dataFeed }) => {
    // iconUrl is an alias used in searchQuery
    const { url, name, icon, iconUrl } = entity
    return (
        <Wrapper as={Link} to={url} data-feed={dataFeed}>
            <Avatar image={icon || iconUrl} name={name} />
            {name}
        </Wrapper>
    )
}

export default UserCard
