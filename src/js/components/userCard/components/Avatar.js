import styled from 'styled-components'

import DefaultAvatar from 'js/components/Avatar/Avatar'

const Avatar = styled(DefaultAvatar)`
    margin-right: 16px;
`

export default Avatar
