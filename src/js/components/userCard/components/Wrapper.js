import styled from 'styled-components'

const Wrapper = styled.div`
    display: flex;
    align-items: center;
    padding: 11px 20px;

    &:not(:last-child) {
        border-bottom: 1px solid ${(p) => p.theme.color.grey[20]};
    }

    &:hover {
        background-color: ${(p) => p.theme.color.hover};
    }

    &:active {
        background-color: ${(p) => p.theme.color.active};
    }
`

export default Wrapper
