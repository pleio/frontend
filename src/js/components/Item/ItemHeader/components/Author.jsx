import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import UserAvatar from 'js/components/UserAvatar'

import Date from './Date'

const Wrapper = styled.div`
    display: flex;
    align-items: center;

    .AuthorName {
        font-weight: ${(p) => p.theme.font.weight.semibold};
    }

    .AuthorDate {
        margin-top: -4px;
    }
`

const Author = ({ entity, ...rest }) => {
    const { owner, timePublished } = entity

    return (
        <Wrapper {...rest}>
            <UserAvatar user={owner} size="large" />
            <div
                style={{
                    marginLeft: '12px',
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'flex-start',
                }}
            >
                <Link
                    className="AuthorName"
                    to={owner.url}
                    style={{ padding: 0 }}
                >
                    {owner.name}
                </Link>
                <Date timePublished={timePublished} className="AuthorDate" />
            </div>
        </Wrapper>
    )
}

export default Author
