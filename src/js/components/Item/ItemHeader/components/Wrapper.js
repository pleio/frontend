import styled from 'styled-components'

export default styled.div`
    position: relative;
    display: flex;
    align-items: center;
    margin-bottom: 32px;

    .Meta {
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        white-space: nowrap;
        overflow: hidden;
        z-index: 1;
    }

    .Author {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        justify-content: center;
    }
`
