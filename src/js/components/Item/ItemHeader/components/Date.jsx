import React from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import DisplayDate from 'js/components/DisplayDate'

const Wrapper = styled.div`
    font-size: ${(p) => p.theme.font.size.small};
    line-height: ${(p) => p.theme.font.lineHeight.small};
    color: ${(p) => p.theme.color.text.grey};
`

const Date = ({ timePublished, ...rest }) => {
    const { t } = useTranslation()

    if (!timePublished) return null

    return (
        <Wrapper {...rest}>
            <DisplayDate
                date={timePublished}
                type="dateTime"
                placement="right"
                hiddenPrefix={t('global.publication-date')}
            />
        </Wrapper>
    )
}

export default Date
