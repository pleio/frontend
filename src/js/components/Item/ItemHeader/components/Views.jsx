import React from 'react'
import { useTranslation } from 'react-i18next'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import HideVisually from 'js/components/HideVisually/HideVisually'
import Text from 'js/components/Text/Text'
import Tooltip from 'js/components/Tooltip/Tooltip'

import EyeIcon from 'icons/eye-small.svg'

const Wrapper = styled.div`
    display: flex;
    align-items: center;
    cursor: default;

    .ItemHeaderViewsIcon {
        margin-right: 4px;
        color: ${(p) => p.theme.color.icon.grey};
    }

    .ItemHeaderViewed {
        max-width: 200px;
        overflow: hidden;
        text-overflow: ellipsis;
    }
`

const Views = ({ children }) => {
    const { t } = useTranslation()

    return (
        <Tooltip
            content={t('global.times-viewed', { count: children })}
            placement="left"
        >
            <Wrapper>
                <EyeIcon className="ItemHeaderViewsIcon" />
                <Text size="small" variant="grey" className="ItemHeaderViewed">
                    <span aria-hidden="true">{children}</span>
                    <HideVisually as="span">
                        {t('global.times-viewed', { count: children })}
                    </HideVisually>
                </Text>
            </Wrapper>
        </Tooltip>
    )
}

Views.propTypes = {
    children: PropTypes.number.isRequired,
}

export default React.memo(Views)
