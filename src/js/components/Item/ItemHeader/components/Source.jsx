import React from 'react'
import { useTranslation } from 'react-i18next'
import { isExternalUrl } from 'helpers'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import Text from 'js/components/Text/Text'
import Tooltip from 'js/components/Tooltip/Tooltip'

import PublicIcon from 'icons/earth-small.svg'

const Wrapper = styled.div`
    display: flex;
    align-items: center;
    cursor: default;

    .ItemHeaderSourceIcon {
        margin-right: 4px;
        color: ${(p) => p.theme.color.icon.grey};
    }

    .ItemHeaderSource {
        max-width: 200px;
        overflow: hidden;
        text-overflow: ellipsis;
    }

    a.ItemHeaderSource {
        color: ${(p) => p.theme.color.primary.main};
        text-decoration: underline;
    }
`

const Source = ({ children }) => {
    const { t } = useTranslation()

    const isExternal = isExternalUrl(children)

    return (
        <Tooltip content={t('entity-news.source')} placement="left">
            <Wrapper
                aria-label={t('global.source-tooltip', { source: children })}
            >
                <PublicIcon className="ItemHeaderSourceIcon" />
                <Text
                    size="small"
                    as={isExternal ? 'a' : null}
                    href={isExternal ? children : null}
                    target={isExternal ? '_blank' : null}
                    rel={isExternal ? 'noopener noreferrer' : null}
                    className="ItemHeaderSource"
                >
                    {children}
                </Text>
            </Wrapper>
        </Tooltip>
    )
}

Source.propTypes = {
    children: PropTypes.string.isRequired,
}

export default React.memo(Source)
