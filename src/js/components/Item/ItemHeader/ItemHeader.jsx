import React from 'react'
import { Trans } from 'react-i18next'
import PropTypes from 'prop-types'

import { H1 } from 'js/components/Heading'
import ItemActions from 'js/components/Item/ItemActions/ItemActions'
import ItemStatusTag from 'js/components/Item/ItemStatusTag'
import Text from 'js/components/Text/Text'

import Author from './components/Author'
import Date from './components/Date'
import Source from './components/Source'
import Views from './components/Views'
import Wrapper from './components/Wrapper'

const ItemHeader = ({
    site,
    viewer,
    title,
    entity,
    onEdit,
    onAfterDelete,
    refetchQueries,
}) => {
    const { views, owner, source, timePublished, isLocked, showOwner } = entity

    const showAuthor = showOwner && owner

    return (
        <header
            // Visually fixing a11y order
            style={{
                display: 'flex',
                flexDirection: 'column',
            }}
        >
            <H1
                style={{
                    order: 1,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'flex-start',
                }}
            >
                {title}
                <ItemStatusTag
                    entity={entity}
                    style={{
                        marginTop: '8px',
                    }}
                />
            </H1>

            <Wrapper style={{ minHeight: '32px' }}>
                <div className="Meta">
                    {!showAuthor && <Date timePublished={timePublished} />}
                    {source && <Source>{source}</Source>}
                    {window.__SETTINGS__.showViewsCount &&
                        typeof views === 'number' && <Views>{views}</Views>}
                </div>
                {showAuthor && <Author entity={entity} className="Author" />}
                <ItemActions
                    showEditButton
                    entity={entity}
                    onEdit={onEdit}
                    onAfterDelete={onAfterDelete}
                    refetchQueries={refetchQueries}
                    style={{ marginLeft: 'auto' }}
                />
            </Wrapper>

            {isLocked && viewer.loggedIn && owner.guid === viewer.user.guid && (
                <Text
                    size="small"
                    variant="grey"
                    textAlign="center"
                    style={{ marginBottom: '16px' }}
                >
                    <Trans i18nKey="entity-question.is-locked">
                        This question is locked because it has answers already.
                        {site.questionLockAfterActivityLink ? (
                            <a
                                href={site.questionLockAfterActivityLink}
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                Contact an administrator
                            </a>
                        ) : (
                            <span>Contact an administrator</span>
                        )}
                        if you want to make changes.
                    </Trans>
                </Text>
            )}
        </header>
    )
}

ItemHeader.propTypes = {
    site: PropTypes.object,
    viewer: PropTypes.object,
    title: PropTypes.string,
    entity: PropTypes.object.isRequired,
    onEdit: PropTypes.any.isRequired,
    onAfterDelete: PropTypes.any.isRequired,
    refetchQueries: PropTypes.array,
}

export default ItemHeader
