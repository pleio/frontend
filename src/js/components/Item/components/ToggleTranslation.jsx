import React from 'react'
import { Trans, useTranslation } from 'react-i18next'

import Text from 'js/components/Text/Text'

const ToggleTranslation = ({ isTranslated, setIsTranslated, ...rest }) => {
    const { t } = useTranslation()

    return (
        <Text size="tiny" variant="grey" {...rest}>
            {isTranslated ? (
                <Trans i18nKey="action.translated-text">
                    <button
                        type="button"
                        onClick={() => setIsTranslated(false)}
                    >
                        Show original
                    </button>
                    (Translated by DeepL)
                </Trans>
            ) : (
                <button type="button" onClick={() => setIsTranslated(true)}>
                    {t('action.translate-text')}
                </button>
            )}
        </Text>
    )
}

export default ToggleTranslation
