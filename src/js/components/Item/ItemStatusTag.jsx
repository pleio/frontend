import React from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import Tag from 'js/components/Tag/Tag'

const Wrapper = styled.div`
    flex-shrink: 0;
    display: flex;
    gap: 8px;
    justify-content: flex-start;
`

const ItemStatusTag = ({ entity, ...rest }) => {
    const { t } = useTranslation()

    const { statusPublished, publishRequest } = entity

    if (statusPublished === 'draft' || statusPublished === 'archived') {
        return (
            <Wrapper {...rest}>
                <Tag>
                    {statusPublished === 'draft' && t('global.draft')}
                    {statusPublished === 'archived' && t('global.archived')}
                </Tag>
                {publishRequest && (
                    <Tag>{t('content-moderation.publish-request')}</Tag>
                )}
            </Wrapper>
        )
    }

    return null
}

export default ItemStatusTag
