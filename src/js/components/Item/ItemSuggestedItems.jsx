import React from 'react'
import { useTranslation } from 'react-i18next'

import { H4 } from 'js/components/Heading'
import RecommendedItem from 'js/components/RecommendedItem/RecommendedItem'

const ItemSuggestedItems = ({ entity, ...rest }) => {
    const { t } = useTranslation()

    return (
        <div {...rest}>
            <H4 style={{ marginBottom: '16px' }}>{t('global.suggested')}</H4>
            {entity.suggestedItems.map((entity) => (
                <RecommendedItem
                    key={`suggested-${entity.guid}`}
                    entity={entity}
                    showMeta
                />
            ))}
        </div>
    )
}

export default ItemSuggestedItems
