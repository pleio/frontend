import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useLocation, useNavigate } from 'react-router-dom'
import { gql, useMutation } from '@apollo/client'
import PropTypes from 'prop-types'

import DropdownButton from 'js/components/DropdownButton/DropdownButton'
import Flexer from 'js/components/Flexer/Flexer'
import IconButton from 'js/components/IconButton/IconButton'
import ChangeAccessModal from 'js/components/Item/ItemActions/components/ChangeAccessModal'
import CopyItem from 'js/components/Item/ItemActions/components/CopyItem'
import DeleteEntitiesModal from 'js/components/Item/ItemActions/components/DeleteEntitiesModal'
import ConfirmationModal from 'js/components/Modal/ConfirmationModal'
import { useSiteStore } from 'js/lib/stores'
import ThemeProvider from 'js/theme/ThemeProvider'
import WikiMoveModal from 'js/wiki/components/WikiMoveModal'

import EditIcon from 'icons/edit.svg'
import OptionsIcon from 'icons/options.svg'
import OptionsSmallIcon from 'icons/options-small.svg'

const ItemActions = ({
    showEditButton,
    entity,
    moveEntity,
    smallButton,
    onEdit,
    canChangeAccess,
    canDuplicate,
    onAfterDelete,
    onAfterArchive,
    onTogglePageSettings,
    refetchQueries,
    customOptions = [],
    disabled,
    hideArchive, // Needed for Magazine and MagazineIssue article
    ...rest
}) => {
    const {
        guid,
        title,
        subtype,
        isClosed,
        canEdit,
        canArchiveAndDelete,
        canClose,
        statusPublished,
        preventDeleteAndArchive,
        hasChildren,
    } = entity

    const [mutateToggleArchive, { loading: archiveLoading }] =
        useMutation(TOGGLE_ARCHIVE)
    const [mutateToggleClose, { loading: closeLoading }] =
        useMutation(TOGGLE_CLOSE)

    const { t } = useTranslation()
    const location = useLocation()
    const navigate = useNavigate()

    const { site } = useSiteStore()

    const [showCopyEntitytModal, setShowCopyEntityModal] = useState(false)

    const [showArchiveConfirmationModal, setShowArchiveConfirmationModal] =
        useState(false)

    const [showDeleteModal, setShowDeleteModal] = useState(false)
    const toggleDeleteModal = () => {
        setShowDeleteModal(!showDeleteModal)
    }

    const handleAfterDelete = () => {
        if (location?.state?.prevPathname) {
            navigate(location.state.prevPathname)
        } else if (onAfterDelete) {
            if (typeof onAfterDelete === 'string') {
                navigate(onAfterDelete, {
                    replace: true,
                })
            } else {
                onAfterDelete()
            }
        }
        setShowDeleteModal(false)
    }

    const handleToggleArchive = () => {
        mutateToggleArchive({
            variables: {
                guid,
            },
            refetchQueries,
        })
            .then(() => {
                setShowArchiveConfirmationModal(false)
                onAfterArchive?.()
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    const handleToggleClose = () => {
        mutateToggleClose({
            variables: {
                input: {
                    guid,
                },
            },
            refetchQueries,
        }).catch((errors) => {
            console.error(errors)
        })
    }

    const [showMoveModal, setShowMoveModal] = useState(false)
    const toggleMoveModal = () => setShowMoveModal(!showMoveModal)

    const [showAccessModal, setShowAccessModal] = useState(false)

    const isArchived = statusPublished === 'archived'
    const canEditContent = canEdit && !isArchived
    const canCloseItem = canClose && !isArchived
    const editIsLink = typeof onEdit === 'string'

    const options = [
        [
            ...(!showEditButton && canEditContent && onEdit
                ? [
                      {
                          name: t('action.edit-item'),
                          ...(editIsLink
                              ? {
                                    to: onEdit,
                                    state: {
                                        prevPathname: location.pathname,
                                    },
                                }
                              : {
                                    onClick: onEdit,
                                }),
                      },
                  ]
                : []),
            ...(canCloseItem
                ? [
                      {
                          name: isClosed
                              ? t(`action.open-item`)
                              : t(`action.close-item`),
                          onClick: handleToggleClose,
                      },
                  ]
                : []),
            ...(canEditContent && moveEntity
                ? [
                      {
                          name: t('action.move-item'),
                          label: t('action.move-item-label', {
                              title,
                          }),
                          onClick: toggleMoveModal,
                      },
                  ]
                : []),
            ...(canArchiveAndDelete && !preventDeleteAndArchive && !hideArchive
                ? [
                      {
                          name: isArchived
                              ? t(`action.unarchive-item`)
                              : t(`action.archive-item`),
                          onClick: isArchived
                              ? handleToggleArchive
                              : () => setShowArchiveConfirmationModal(true),
                      },
                  ]
                : []),
            ...(canEditContent && canDuplicate
                ? [
                      {
                          name: t('action.duplicate-item'),
                          label: t('action.duplicate-item-label', {
                              title,
                          }),
                          onClick: () => setShowCopyEntityModal(true),
                      },
                  ]
                : []),
            ...(canArchiveAndDelete && !preventDeleteAndArchive
                ? [
                      {
                          name: t('action.delete-item'),
                          label: t('action.delete-item-label', {
                              title,
                          }),
                          onClick: toggleDeleteModal,
                      },
                  ]
                : []),
        ],
        [
            ...(canEdit && onTogglePageSettings
                ? [
                      {
                          name: `${t('global.settings')}..`,
                          onClick: onTogglePageSettings,
                      },
                  ]
                : []),
            ...(canEdit && ['blog', 'news', 'wiki'].includes(subtype)
                ? [
                      {
                          name: t('version-history.title'),
                          label: t('action.view-item-history', {
                              title,
                          }),
                          to: `/${guid}/version-history`,
                          state: {
                              prevPathname: location.pathname,
                          },
                      },
                  ]
                : []),
            ...(canEdit && canChangeAccess
                ? [
                      {
                          name: `${t('global.access')}..`,
                          onClick: () => setShowAccessModal(true),
                      },
                  ]
                : []),
        ],
        ...customOptions,
    ]

    const isLoading = archiveLoading || closeLoading

    if ((canEditContent && showEditButton) || options.length > 0) {
        return (
            <div {...rest}>
                <Flexer gutter="small" divider="normal">
                    {canEditContent && showEditButton && (
                        <IconButton
                            size={smallButton ? 'normal' : 'large'}
                            variant="secondary"
                            as={editIsLink ? Link : null}
                            to={editIsLink ? onEdit : null}
                            state={
                                editIsLink
                                    ? { prevPathname: location.pathname }
                                    : null
                            }
                            onClick={!editIsLink ? onEdit : null}
                            tooltip={t('action.edit-item')}
                            loading={isLoading}
                        >
                            <EditIcon />
                        </IconButton>
                    )}
                    {options.length > 0 && (
                        <DropdownButton options={options}>
                            <IconButton
                                size={smallButton ? 'normal' : 'large'}
                                variant="secondary"
                                tooltip={t('global.options')}
                                aria-label={t('action.show-options')}
                                disabled={disabled}
                            >
                                {smallButton ? (
                                    <OptionsSmallIcon />
                                ) : (
                                    <OptionsIcon />
                                )}
                            </IconButton>
                        </DropdownButton>
                    )}
                </Flexer>

                {/* Reset custom theme */}
                <ThemeProvider theme="site" siteStyle={site?.style}>
                    <ChangeAccessModal
                        showModal={showAccessModal}
                        onClose={() => setShowAccessModal(false)}
                        entity={entity}
                    />

                    <ConfirmationModal
                        title={t('action.archive')}
                        isVisible={showArchiveConfirmationModal}
                        onConfirm={handleToggleArchive}
                        onCancel={() => setShowArchiveConfirmationModal(false)}
                        loading={archiveLoading}
                    >
                        {t('form.confirm-archive', {
                            count: 1,
                        })}{' '}
                        {hasChildren &&
                            t('form.confirm-archive-children-warning')}
                    </ConfirmationModal>

                    <DeleteEntitiesModal
                        isVisible={showDeleteModal}
                        onClose={toggleDeleteModal}
                        entities={[entity]}
                        refetchQueries={refetchQueries}
                        onComplete={handleAfterDelete}
                    />

                    <CopyItem
                        isVisible={showCopyEntitytModal}
                        entity={entity}
                        onClose={() => setShowCopyEntityModal(false)}
                    />

                    {moveEntity && (
                        <WikiMoveModal
                            isVisible={showMoveModal}
                            onClose={toggleMoveModal}
                            moveEntity={moveEntity}
                        />
                    )}
                </ThemeProvider>
            </div>
        )
    } else {
        return null
    }
}

ItemActions.propTypes = {
    entity: PropTypes.object.isRequired,
    showEditButton: PropTypes.bool,
    smallButton: PropTypes.bool,
    onEdit: PropTypes.any,
    canDuplicate: PropTypes.bool,
    onToggleClosed: PropTypes.func,
    onAfterDelete: PropTypes.any,
    onAfterArchive: PropTypes.any,
}

const TOGGLE_ARCHIVE = gql`
    mutation toggleEntityArchived($guid: String!) {
        toggleEntityArchived(guid: $guid) {
            success
        }
    }
`

const TOGGLE_CLOSE = gql`
    mutation toggleIsClosed($input: toggleIsClosedInput!) {
        toggleIsClosed(input: $input) {
            entity {
                guid
                ... on Question {
                    isClosed
                }
            }
        }
    }
`

export default ItemActions
