import React, { useEffect, useLayoutEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import EntityList from 'js/components/EntityList/EntityList'
import EntityListItem from 'js/components/EntityList/EntityListItem'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import Modal from 'js/components/Modal/Modal'
import Spacer from 'js/components/Spacer/Spacer'
import Text from 'js/components/Text/Text'
import DetailsReferences from 'js/files/Details/components/DetailsReferences'

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    overflow: hidden;

    .DeleteList {
        margin-top: 16px;
        overflow-y: auto;
    }
`

const DeleteEntitiesModal = ({
    entities,
    setSelected,
    title,
    isVisible,
    onClose,
    onComplete,
    refetchQueries,
}) => {
    const [errors, setErrors] = useState([])
    const [filesWithReferences, setFilesWithReferences] = useState([])

    const [deleteEntity] = useMutation(DELETE_ENTITY)

    const defaultValues = {
        softDelete: true,
    }

    const {
        control,
        handleSubmit,
        reset,
        formState: { isValid, isSubmitting },
    } = useForm({ mode: 'onChange', defaultValues })

    const submit = async ({ softDelete }) => {
        const deleteEntities = hasFilesWithReferences
            ? filesWithReferences
            : entities

        await Promise.all(
            deleteEntities.map((entity) => {
                return deleteEntity({
                    variables: {
                        input: {
                            guid: entity.guid,
                            recursive: hasFilesWithReferences
                                ? softDelete
                                    ? 'softDelete'
                                    : 'forceDelete'
                                : 'tryDelete',
                        },
                    },
                    refetchQueries,
                })
            }),
        )
            .then((payload) => {
                const errors = payload.filter(
                    (el) => el.data.deleteEntity.success === false,
                )
                if (errors?.length > 0) {
                    const failedEntities = []
                    errors.forEach((el) =>
                        el.data.deleteEntity.errors.forEach((error) =>
                            failedEntities.push(error.entity),
                        ),
                    )
                    setFilesWithReferences(failedEntities)
                    return
                }
                onComplete?.()
            })
            .catch((errors) => {
                setErrors(errors)
            })
    }

    const { t } = useTranslation()

    const hasFilesWithReferences = filesWithReferences?.length > 0

    useEffect(() => {
        if (setSelected && hasFilesWithReferences) {
            const filesWithReferencesGuids = filesWithReferences.map(
                (el) => el.guid,
            )
            const newSelected = [...entities].filter((el) =>
                filesWithReferencesGuids.includes(el.guid),
            )
            setSelected(newSelected)
        }
        // eslint-disable-next-line
    }, [filesWithReferences])

    useLayoutEffect(() => {
        if (isVisible) {
            reset()
            setFilesWithReferences([]) // Reset value on open
        }
    }, [isVisible, reset, setFilesWithReferences])

    return (
        <Modal
            isVisible={isVisible}
            title={title || t('action.delete')}
            size="small"
            onClose={onClose}
            containHeight
        >
            <Wrapper as="form" onSubmit={handleSubmit(submit)}>
                {filesWithReferences.length > 0 ? (
                    <>
                        {t('entity-file.is-referenced', {
                            count: filesWithReferences.length,
                        })}
                        <Spacer spacing="small" className="DeleteList">
                            {filesWithReferences.map((entity) => {
                                return (
                                    <EntityList key={entity.guid} $showBorder>
                                        <EntityListItem
                                            entity={entity}
                                            type="file"
                                        />
                                        <DetailsReferences
                                            references={entity.references}
                                        />
                                    </EntityList>
                                )
                            })}
                        </Spacer>
                        <FormItem
                            type="checkbox"
                            control={control}
                            name="softDelete"
                            label={t('entity-file.soft-delete', {
                                count: filesWithReferences.length,
                            })}
                            size="small"
                            style={{ marginTop: '12px' }}
                        />
                    </>
                ) : (
                    <>
                        {t('form.confirm-delete', {
                            count: entities.length,
                        })}
                        <div className="DeleteList">
                            <EntityList as="ul" $showBorder>
                                {entities.map((entity) => {
                                    return (
                                        <li key={entity.guid}>
                                            <EntityListItem
                                                entity={entity}
                                                type="file"
                                            />
                                        </li>
                                    )
                                })}
                            </EntityList>

                            {entities.some((entity) => entity.hasChildren) && (
                                <Text
                                    size="small"
                                    variant="warn"
                                    role="alert"
                                    style={{ marginTop: 16 }}
                                >
                                    {t('form.confirm-delete-children-warning')}
                                </Text>
                            )}
                        </div>
                    </>
                )}
                <Errors errors={errors} />
                <Flexer mt>
                    <Button size="normal" variant="tertiary" onClick={onClose}>
                        {t('action.cancel')}
                    </Button>
                    <Button
                        type="submit"
                        size="normal"
                        variant="primary"
                        disabled={!isValid}
                        loading={isSubmitting}
                    >
                        {t('action.delete')}
                    </Button>
                </Flexer>
            </Wrapper>
        </Modal>
    )
}

const DELETE_ENTITY = gql`
    mutation deleteEntity($input: deleteEntityInput!) {
        deleteEntity(input: $input) {
            success
            errors {
                next
                entity {
                    guid
                    ... on FileFolder {
                        title
                        subtype
                        referenceCount
                        references {
                            guid
                            label
                            url
                            referenceType
                            entityType
                        }
                    }
                    ... on Folder {
                        hasChildren
                    }
                    ... on File {
                        mimeType
                        size
                    }
                }
            }
        }
    }
`

export default DeleteEntitiesModal
