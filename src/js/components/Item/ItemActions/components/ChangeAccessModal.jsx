import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'

import Checkbox from 'js/components/Checkbox/Checkbox'
import AccessControlField, {
    sanitizeAccessControl,
} from 'js/components/Form/AccessControlField'
import ConfirmationModal from 'js/components/Modal/ConfirmationModal'

const ChangeAccessModal = ({ showModal, onClose, entity }) => {
    const { t } = useTranslation()
    const { pageType, group, hasChildren } = entity

    const [accessControl, setAccessControl] = useState(
        entity?.accessControl || [],
    )

    const [isAccessRecursive, setIsAccessRecursive] = useState(false)

    const [mutateEditEntity, { loading }] = useMutation(EDIT_ENTITY)

    const handleSubmitAccess = async () => {
        const input = {
            guid: entity.guid,
            accessControl: sanitizeAccessControl(accessControl),
            ...(pageType === 'text' && {
                isAccessRecursive,
            }),
        }

        await mutateEditEntity({
            variables: {
                input,
            },
            refetchQueries: ['PageList', 'AdminPages'],
        })
            .then(() => {
                onClose()
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    return (
        <ConfirmationModal
            title={t('global.access')}
            cancelLabel={t('action.cancel')}
            confirmLabel={t('action.save')}
            isVisible={showModal}
            onConfirm={handleSubmitAccess}
            onCancel={onClose}
            loading={loading}
        >
            <AccessControlField
                group={group}
                accessControl={accessControl}
                setAccessControl={setAccessControl}
            />

            {hasChildren && pageType === 'text' && (
                <Checkbox
                    name="isAccessRecursive"
                    size="small"
                    label={t('entity-page-text.apply-access-to-underlying')}
                    checked={isAccessRecursive}
                    onChange={(evt) => setIsAccessRecursive(evt.target.checked)}
                    style={{ marginTop: '12px' }}
                />
            )}
        </ConfirmationModal>
    )
}

// Change to editEntity when ready
const EDIT_ENTITY = gql`
    mutation EditEntity($input: editPageInput!) {
        editPage(input: $input) {
            entity {
                guid
                ... on Page {
                    url
                }
            }
        }
    }
`

export default ChangeAccessModal
