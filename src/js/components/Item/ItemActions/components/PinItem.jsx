import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import Modal from 'js/components/Modal/Modal'

export default function PinItem({
    entity,
    isVisible,
    onClose,
    refetchQueries,
}) {
    const { t } = useTranslation()

    const [togglePinEntity, { loading }] = useMutation(TOGGLE_PIN_ENTITY)

    const { guid, isPinned } = entity

    const togglePin = () => {
        togglePinEntity({
            variables: {
                input: {
                    guid,
                },
            },
            refetchQueries,
        })
            .then(() => {
                onClose()
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    return (
        <Modal
            isVisible={isVisible}
            onClose={onClose}
            title={isPinned ? t('action.unpin-item') : t('action.pin-item')}
            size="small"
        >
            {isPinned
                ? t('action.unpin-item-confirm')
                : t('action.pin-item-confirm')}
            <Flexer mt>
                <Button
                    size="normal"
                    variant="tertiary"
                    type="button"
                    onClick={onClose}
                >
                    {t('action.no-back')}
                </Button>
                <Button
                    size="normal"
                    variant="primary"
                    onClick={togglePin}
                    loading={loading}
                >
                    {t('action.yes-confirm')}
                </Button>
            </Flexer>
        </Modal>
    )
}

const TOGGLE_PIN_ENTITY = gql`
    mutation Pin($input: toggleEntityIsPinnedInput!) {
        toggleEntityIsPinned(input: $input) {
            success
        }
    }
`
