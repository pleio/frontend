import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import EntityList from 'js/components/EntityList/EntityList'
import EntityListItem from 'js/components/EntityList/EntityListItem'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import Modal from 'js/components/Modal/Modal'

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    overflow: hidden;

    .ArchiveList {
        margin-top: 16px;
        overflow-y: auto;
    }
`

const ArchiveEntitiesModal = ({
    entities,
    isVisible,
    onClose,
    onComplete,
    refetchQueries,
}) => {
    const [errors, setErrors] = useState([])

    const [archiveEntities] = useMutation(ARCHIVE_ENTITIES)

    const {
        handleSubmit,
        formState: { isSubmitting },
    } = useForm({ mode: 'onChange' })

    const submit = async () => {
        await archiveEntities({
            variables: {
                input: {
                    guids: entities.map((entity) => entity.guid),
                },
            },
            refetchQueries,
        })
            .then(() => {
                onComplete && onComplete()
            })
            .catch((errors) => {
                setErrors(errors)
            })
    }

    const { t } = useTranslation()

    const someHasChildren = entities.some((entity) => entity.hasChildren)

    return (
        <Modal
            isVisible={isVisible}
            title={t('action.archive')}
            size="small"
            onClose={onClose}
            containHeight
        >
            <Wrapper as="form" onSubmit={handleSubmit(submit)}>
                <Errors errors={errors} />
                {t('form.confirm-archive', {
                    count: entities.length,
                })}{' '}
                {someHasChildren && t('form.confirm-archive-children-warning')}
                <div className="ArchiveList">
                    <EntityList $showBorder>
                        {entities.map((entity) => {
                            return (
                                <EntityListItem
                                    key={entity.guid}
                                    entity={entity}
                                    type="file"
                                />
                            )
                        })}
                    </EntityList>
                </div>
                <Flexer mt>
                    <Button size="normal" variant="tertiary" onClick={onClose}>
                        {t('action.cancel')}
                    </Button>
                    <Button
                        type="submit"
                        size="normal"
                        variant="primary"
                        loading={isSubmitting}
                    >
                        {t('action.archive')}
                    </Button>
                </Flexer>
            </Wrapper>
        </Modal>
    )
}

const ARCHIVE_ENTITIES = gql`
    mutation archiveEntities($input: archiveEntitiesInput!) {
        archiveEntities(input: $input) {
            edges {
                guid
                status
            }
        }
    }
`

export default ArchiveEntitiesModal
