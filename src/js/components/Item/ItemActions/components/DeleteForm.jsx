import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import Button from 'js/components/Button/Button'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'

const DeleteForm = ({
    mutate,
    entity,
    message,
    refetchQueries,
    beforeDelete,
    afterDelete,
    onClose,
}) => {
    const { t } = useTranslation()

    const [errors, setErrors] = useState(null)

    const { guid, title, name } = entity

    const onSubmit = (e) => {
        e.preventDefault()

        setErrors(null)

        beforeDelete && beforeDelete()

        mutate({
            variables: {
                input: {
                    guid,
                },
            },
            refetchQueries,
        })
            .then(() => {
                afterDelete && afterDelete()
            })
            .catch((errors) => {
                setErrors(errors)
                console.error(errors)
            })
    }

    return (
        <form onSubmit={onSubmit}>
            {message ||
                t('form.entity-delete-warning', { title: title || name })}
            <Errors errors={errors} />
            <Flexer mt>
                <Button
                    type="button"
                    size="normal"
                    variant="tertiary"
                    onClick={onClose}
                >
                    {t('action.no-back')}
                </Button>
                <Button type="submit" size="normal" variant="primary">
                    {t('action.yes-delete')}
                </Button>
            </Flexer>
        </form>
    )
}

const Mutation = gql`
    mutation deleteEntity($input: deleteEntityInput!) {
        deleteEntity(input: $input) {
            success
        }
    }
`

export default graphql(Mutation)(DeleteForm)
