import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Navigate } from 'react-router-dom'
import { gql, useMutation } from '@apollo/client'

import Button from 'js/components/Button/Button'
import Errors from 'js/components/Errors'
import Flexer from 'js/components/Flexer/Flexer'
import Modal from 'js/components/Modal/Modal'

export default function CopyItem({ entity, isVisible, onClose }) {
    const { t } = useTranslation()

    const [errors, setErrors] = useState(null)

    const { guid, title } = entity

    const [copyItem, { data, loading, error }] = useMutation(COPY_PAGE, {
        variables: {
            input: {
                guid,
                ownerGuid: entity.owner?.guid || null,
            },
        },
    })

    useEffect(() => {
        setErrors(error)
    }, [error])

    const redirectUrl = data?.copyEntity?.entity?.url || null

    if (redirectUrl) {
        return <Navigate to={redirectUrl} replace={true} />
    }

    return (
        <Modal
            size="small"
            onClose={onClose}
            isVisible={isVisible}
            title={t('action.duplicate-item')}
        >
            {t('action.duplicate-item-confirm', { title })}

            <Errors errors={errors} />

            <Flexer mt>
                <Button size="normal" variant="tertiary" onClick={onClose}>
                    {t('action.no-cancel')}
                </Button>

                <Button
                    size="normal"
                    variant="primary"
                    onClick={copyItem}
                    loading={loading}
                    disabled={errors}
                >
                    {t('action.yes-open-copy')}
                </Button>
            </Flexer>
        </Modal>
    )
}

const COPY_PAGE = gql`
    mutation CopyEntity($input: copyEntityInput!) {
        copyEntity(input: $input) {
            entity {
                guid
                ... on Event {
                    title
                    url
                }
                ... on Page {
                    title
                    url
                }
                ... on Wiki {
                    title
                    url
                }
            }
        }
    }
`
