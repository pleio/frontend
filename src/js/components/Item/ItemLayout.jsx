import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { media } from 'helpers'
import styled from 'styled-components'

import CommentList from 'js/components/CommentList'
import CoverImage from 'js/components/CoverImage'
import Document from 'js/components/Document'
import { Col, Container, Row } from 'js/components/Grid/Grid'
import { H4 } from 'js/components/Heading'
import ItemHeader from 'js/components/Item/ItemHeader/ItemHeader'
import ItemSuggestedItems from 'js/components/Item/ItemSuggestedItems'
import ItemTags from 'js/components/ItemTags'
import LikeAndBookmark from 'js/components/LikeAndBookmark'
import Section from 'js/components/Section/Section'
import SocialShare from 'js/components/SocialShare/SocialShare'
import TiptapView from 'js/components/Tiptap/TiptapView'
import { useSiteStore } from 'js/lib/stores/SiteStore'
import ThemeProvider from 'js/theme/ThemeProvider'

import ToggleTranslation from './components/ToggleTranslation'

const Wrapper = styled(Section)`
    padding-top: 0;

    .ItemCoverContainer {
        ${media.tabletDown`
            padding: 0;
        `}
    }
`

const ItemLayout = ({
    viewer,
    entity,
    refetchQueries,
    canUpvote,
    hideComments,
    onEdit,
    onAfterDelete,
}) => {
    const { t } = useTranslation()
    const { site } = useSiteStore()

    const {
        localTitle,
        title,
        group,
        featured,
        localRichDescription,
        richDescription,
        tags,
        tagCategories,
        statusPublished,
        commentCount,
        suggestedItems,
        isTranslationEnabled,
        backgroundColor,
    } = entity

    const hasTranslations = !!localTitle && isTranslationEnabled
    const [isTranslated, setIsTranslated] = useState(hasTranslations)

    const translatedTitle = isTranslated ? localTitle : title
    const translatedRichDescription = isTranslated
        ? localRichDescription
        : richDescription

    const isPublished = statusPublished === 'published'
    const hasSuggestedItems = suggestedItems?.length > 0

    // White background won't activate custom theme
    const customTheme =
        backgroundColor && !['#ffffff', 'white'].includes(backgroundColor)

    return (
        <ThemeProvider
            theme={customTheme && backgroundColor}
            bodyColor={backgroundColor || '#FFFFFF'}
        >
            <Document title={translatedTitle} containerTitle={group?.name} />

            <Wrapper grow>
                <Container size="large" className="ItemCoverContainer">
                    <CoverImage featured={featured} />
                </Container>
                <Container size="normal">
                    <article
                        style={{
                            position: 'relative',
                            marginTop: '32px',
                        }}
                    >
                        <ItemHeader
                            site={site}
                            viewer={viewer}
                            title={translatedTitle}
                            entity={entity}
                            onEdit={onEdit}
                            onAfterDelete={onAfterDelete}
                            refetchQueries={refetchQueries}
                        />

                        <TiptapView
                            content={translatedRichDescription}
                            style={{ marginTop: '24px' }}
                        />

                        {hasTranslations && (
                            <ToggleTranslation
                                isTranslated={isTranslated}
                                setIsTranslated={setIsTranslated}
                                style={{ marginTop: '12px' }}
                            />
                        )}

                        <ItemTags
                            showCustomTags={site.showCustomTagsInDetail}
                            showTags={site.showTagsInDetail}
                            customTags={tags}
                            tagCategories={tagCategories}
                            style={{ marginTop: '24px' }}
                        />

                        {isPublished && viewer.loggedIn && (
                            <LikeAndBookmark entity={entity} />
                        )}

                        {isPublished && window.__SETTINGS__.enableSharing && (
                            <SocialShare />
                        )}
                    </article>
                </Container>

                <Container size={hasSuggestedItems ? 'large' : 'normal'}>
                    <Row style={{ marginTop: '32px' }}>
                        <Col tabletUp={hasSuggestedItems ? 2 / 3 : 1}>
                            {!hideComments && (
                                <H4 as="h2">
                                    {t('comments.count-comments', {
                                        count: commentCount,
                                    })}
                                </H4>
                            )}

                            {!hideComments && (
                                <CommentList
                                    viewer={viewer}
                                    entity={entity}
                                    refetchQueries={refetchQueries}
                                    canUpvote={canUpvote}
                                />
                            )}
                        </Col>
                        {hasSuggestedItems && (
                            <Col tabletUp={1 / 3}>
                                <ItemSuggestedItems entity={entity} />
                            </Col>
                        )}
                    </Row>
                </Container>
            </Wrapper>
        </ThemeProvider>
    )
}

export default ItemLayout
