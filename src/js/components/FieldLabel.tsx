import React, { HTMLAttributes } from 'react'
import { useTranslation } from 'react-i18next'
import styled, { css } from 'styled-components'

import HideVisually from 'js/components/HideVisually/HideVisually'
import Tooltip from 'js/components/Tooltip/Tooltip'
import shouldForwardProp from 'js/lib/helpers/shouldForwardProp'

interface Props extends HTMLAttributes<HTMLDivElement> {
    as?: React.ElementType
    htmlFor?: any
    size?: 'small' | 'normal' | 'large'
    label?: string
    placeholder?: string
    required?: boolean
    active?: boolean
    disabled?: boolean
    hasError?: boolean
}

const Wrapper = styled.div.withConfig({
    shouldForwardProp: shouldForwardProp([
        'size',
        'active',
        'disabled',
        'hasError',
    ]),
})<Props>`
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    display: flex;
    align-items: center;
    pointer-events: none;
    transition:
        transform ${(p) => p.theme.transition.materialFast},
        height ${(p) => p.theme.transition.materialFast};

    .TextFieldLabelBox {
        display: inline;
        position: relative;
        padding: 0 2px;
        font-size: ${(p) => p.theme.font.size[p.size]};
        line-height: ${(p) => p.theme.font.lineHeight[p.size]};
        color: ${(p) => p.theme.color.text.grey};
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
        transition:
            font-size ${(p) => p.theme.transition.materialFast},
            line-height ${(p) => p.theme.transition.materialFast};

        /* Background behind the label */
        &:before {
            content: '';
            position: absolute;
            top: 0;
            bottom: 0;
            margin: auto 0;
            left: 0;
            right: 0;
            height: 5px; // Minimum to prevent focus outline to strike through the text
            background-color: ${(p) => p.theme.button.bg};
            z-index: -1;
            opacity: 0;
            transition: opacity ${(p) => p.theme.transition.Fast};
        }

        ${(p) =>
            p.size === 'small' &&
            css`
                font-size: ${(p) => p.theme.font.size.small};
                line-height: ${(p) => p.theme.font.lineHeight.small};
            `};

        ${(p) =>
            p.size === 'normal' &&
            css`
                font-size: ${(p) => p.theme.font.size.normal};
                line-height: ${(p) => p.theme.font.lineHeight.normal};
            `};

        ${(p) =>
            p.active &&
            css`
                font-size: ${(p) => p.theme.font.size.tiny};
                line-height: ${(p) => p.theme.font.lineHeight.tiny};

                &:before {
                    opacity: 1;
                }
            `};

        ${(p) =>
            p.disabled &&
            css`
                &:before {
                    background-color: ${(p) => p.theme.color.grey[20]};
                }
            `};

        ${(p) =>
            p.hasError &&
            css`
                color: ${(p) => p.theme.color.warn.main};
            `};
    }

    .TextFieldLabelRequired {
        padding-left: 2px;
        color: ${(p) => p.theme.color.warn.main};
    }
`

const FieldLabel = ({
    size = 'small',
    label,
    placeholder,
    required,
    active,
    disabled,
    hasError,
    ...rest
}: Props) => {
    const { t } = useTranslation()

    if (!label) return null

    return (
        <Wrapper
            size={size}
            active={active}
            disabled={disabled}
            hasError={hasError}
            {...rest}
        >
            <div className="TextFieldLabelBox">
                {label}
                {placeholder && <HideVisually>({placeholder})</HideVisually>}
                {required && (
                    <Tooltip
                        theme="warn-white"
                        placement="right"
                        content={t('global.required')}
                    >
                        <span aria-hidden className="TextFieldLabelRequired">
                            *
                        </span>
                    </Tooltip>
                )}
            </div>
        </Wrapper>
    )
}

export default FieldLabel
