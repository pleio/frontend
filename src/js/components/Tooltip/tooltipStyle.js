import { css } from 'styled-components'

export default css`
    .tippy-box[data-theme^='tooltip'] {
        background-color: ${(p) => p.theme.tooltip.bg};
        border-radius: ${(p) => p.theme.radius.small};
        box-shadow: ${(p) => p.theme.tooltip.shadow};

        color: ${(p) => p.theme.tooltip.color};
        font-size: ${(p) => p.theme.font.size.tiny};
        line-height: ${(p) => p.theme.font.lineHeight.tiny};
        font-weight: ${(p) => p.theme.font.weight.bold};
        cursor: default;
    }

    .tippy-backdrop {
        background-color: ${(p) => p.theme.tooltip.bg};
    }

    .tippy-content {
        padding: 4px 6px 5px;
    }

    /* accept-white */
    .tippy-box[data-theme~='tooltip-accept-white'] {
        background-color: ${(p) => p.theme.color.accept};

        > .tippy-backdrop {
            background-color: ${(p) => p.theme.color.accept};
        }
    }

    /* pleio-white */
    .tippy-box[data-theme~='tooltip-pleio-white'] {
        background-color: ${(p) => p.theme.color.pleio};

        > .tippy-backdrop {
            background-color: ${(p) => p.theme.color.pleio};
        }
    }

    /* primary-white */
    .tippy-box[data-theme~='tooltip-primary-white'] {
        background-color: ${(p) => p.theme.color.secondary.main};

        > .tippy-backdrop {
            background-color: ${(p) => p.theme.color.secondary.main};
        }
    }

    /* warn-white */
    .tippy-box[data-theme~='tooltip-warn-white'] {
        background-color: ${(p) => p.theme.color.warn.main};

        > .tippy-backdrop {
            background-color: ${(p) => p.theme.color.warn.main};
        }
    }
`
