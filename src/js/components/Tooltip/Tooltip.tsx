import React, { forwardRef, useContext, useState } from 'react'
import TippyPlugin from '@tippyjs/react'
import {
    animateFill as AnimateFill,
    followCursor as FollowCursor,
    Placement,
} from 'tippy.js'

import { ThemeContext } from 'js/theme/ThemeProvider'

export interface Props {
    children: React.ReactElement<any>
    content: React.ReactNode
    delay?: number | boolean
    placement?: Placement
    duration?: [number, number]
    offset?: [number, number]
    theme?: string
    followCursor?: boolean | 'horizontal' | 'vertical' | 'initial'
    disabled?: boolean
}

const Tooltip = forwardRef<HTMLElement, Props>(
    (
        {
            children,
            content,
            delay,
            duration = [175, 150],
            offset = [0, 8],
            placement = 'bottom',
            followCursor,
            theme,
            ...rest
        },
        ref,
    ) => {
        const [instance, setInstance] = useState(null)

        const handleKeyDown = (evt) => {
            if (evt.key === 'Escape') {
                handleHide()
                instance.hide()
            }
        }

        const handleHide = () => {
            window.removeEventListener('keydown', handleKeyDown)
        }

        const handleShow = () => {
            window.addEventListener('keydown', handleKeyDown)
        }

        const context = useContext(ThemeContext)

        return (
            <TippyPlugin
                aria={{
                    expanded: false,
                    content: null,
                }}
                appendTo={() => context?.container || document.body} // Inherit theme colors if available
                theme={`tooltip${theme && `-${theme}`}`}
                arrow={false}
                ignoreAttributes={true} // Improves performance
                interactive={true} // Content must be selectable for accessibility
                plugins={[AnimateFill, FollowCursor]}
                animateFill={true}
                content={content}
                followCursor={followCursor}
                delay={
                    delay
                        ? typeof delay === 'boolean'
                            ? [500, 0]
                            : delay
                        : null
                }
                duration={duration}
                offset={offset}
                onCreate={setInstance}
                onHide={handleHide}
                onShow={handleShow}
                placement={placement}
                ref={ref}
                {...rest}
            >
                {children}
            </TippyPlugin>
        )
    },
)

Tooltip.displayName = 'Tooltip'

export default Tooltip
