import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useLocalStorage } from 'helpers'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import ModalBox from 'js/components/Modal/ModalBox'

const Iframe = styled.iframe`
    border: none;
`

const VideoIframe = ({
    type,
    id,
    title,
    autoPlay,
    disableKeyboard,
    editable,
    onClose,
}) => {
    const { t } = useTranslation()

    const [allowCookies, setAllowCookies] = useLocalStorage(
        'allow-third-party-cookies',
        false,
    )

    const [showCookieModal, setShowCookieModal] = useState(
        !allowCookies && !editable,
    )

    // Autoplay
    const ap = autoPlay ? 1 : 0
    // Disable keyboard
    const dkb = disableKeyboard ? 1 : 0
    // Allow keyboard
    const akb = disableKeyboard ? 0 : 1

    const defaultProps = {
        allow: 'accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture; fullscreen',
        title,
        tabIndex: editable ? '-1' : '0',
    }

    switch (type) {
        case 'youtube':
            return allowCookies || editable ? (
                <Iframe
                    src={`https://www.youtube-nocookie.com/embed/${id}?autoplay=${
                        allowCookies && ap
                    }&disablekb=${dkb}`}
                    {...defaultProps}
                />
            ) : (
                // Notice because YouTube places 'delayed cookies' when playing a video
                <ModalBox
                    isVisible={showCookieModal}
                    onClose={() => setShowCookieModal(false)}
                    size="small"
                    title={t('global.third-party-cookies')}
                    showCloseButton={!!onClose}
                >
                    <div style={{ padding: onClose ? 20 : 0 }}>
                        {t('global.third-party-cookies-confirm')}
                        <Flexer mt>
                            {onClose && (
                                <Button
                                    size="normal"
                                    variant="tertiary"
                                    onClick={() => {
                                        setShowCookieModal(false)
                                        onClose()
                                    }}
                                >
                                    {t('action.cancel')}
                                </Button>
                            )}
                            <Button
                                size="normal"
                                variant="primary"
                                onClick={() => {
                                    setAllowCookies(true)
                                    window.location.reload()
                                }}
                            >
                                {t('action.accept')}
                            </Button>
                        </Flexer>
                    </div>
                </ModalBox>
            )

        case 'vimeo':
            return (
                <>
                    <Iframe
                        src={`https://player.vimeo.com/video/${id}?dnt=1?autoplay=${ap}?keyboard=${akb}`}
                        {...defaultProps}
                    />
                    <script src="https://player.vimeo.com/api/player.js" />
                </>
            )

        case 'kaltura': {
            const { kalturaVideoPartnerId, kalturaVideoPlayerId } =
                window.__SETTINGS__

            return (
                <Iframe
                    id="kaltura_player"
                    src={`https://api.eu.kaltura.com/p/${kalturaVideoPartnerId}/sp/${kalturaVideoPartnerId}00/embedIframeJs/uiconf_id/${kalturaVideoPlayerId}/partner_id/${kalturaVideoPartnerId}?iframeembed=true&playerId=kaltura_player&entry_id=${id}&flashvars[streamerType]=auto&flashvars[localizationCode]=nl&flashvars[leadWithHTML5]=true&flashvars[sideBarContainer.plugin]=true&flashvars[sideBarContainer.position]=left&flashvars[sideBarContainer.clickToClose]=true&flashvars[chapters.plugin]=true&flashvars[chapters.layout]=vertical&flashvars[chapters.thumbnailRotator]=false&flashvars[streamSelector.plugin]=true&flashvars[EmbedPlayer.SpinnerTarget]=videoHolder&flashvars[dualScreen.plugin]=true&flashvars[hotspots.plugin]=1&flashvars[Kaltura.addCrossoriginToIframe]=true&`}
                    sandbox="allow-forms allow-same-origin allow-scripts allow-top-navigation allow-pointer-lock allow-popups allow-modals allow-orientation-lock allow-popups-to-escape-sandbox allow-presentation allow-top-navigation-by-user-activation"
                    {...defaultProps}
                />
            )
        }

        default:
            return null
    }
}

export default VideoIframe
