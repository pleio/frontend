import styled, { css } from 'styled-components'

type ChatStatusProps = {
    $status: 'online' | 'offline'
}

const StatusIndicator = styled.div<ChatStatusProps>`
    width: 7px;
    height: 7px;
    border-radius: 50%;
    box-shadow: 0 0 0 2px white;

    ${({ $status }) =>
        $status === 'online' &&
        css`
            background-color: ${({ theme }) => theme.color.accept};
        `}

    ${({ $status }) =>
        $status === 'offline' &&
        css`
            background-color: white;
            border: 2px solid ${({ theme }) => theme.color.icon.grey};
        `}
`

export default StatusIndicator
