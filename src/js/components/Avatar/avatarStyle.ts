import { transparentize } from 'polished'
import { css } from 'styled-components'

import { Props } from './Avatar'

export const avatarSizes = {
    tiny: 20,
    small: 24,
    normal: 32,
    large: 40,
    xlarge: 60,
    xxlarge: 80,
}

const avatarStyle = css<Props>`
    flex-shrink: 0;
    position: relative;

    background-color: ${(p) => p.theme.color.grey[20]};
    background-position: center;
    background-size: cover;

    user-select: none;
    transition: border-color ${(p) => p.theme.transition.fast};

    width: ${(p) => avatarSizes[p.size]}px;
    height: ${(p) => avatarSizes[p.size]}px;

    ${(p) =>
        p.radiusStyle === 'round' &&
        css`
            border-radius: 50%;

            &:before {
                border-radius: 50%;
            }
        `};

    ${(p) =>
        p.radiusStyle === 'rounded' &&
        css`
            border-radius: ${(p) => p.theme.radius.tiny};

            &:before {
                border-radius: ${(p) => p.theme.radius.tiny};
            }
        `};

    ${(p) =>
        p.radiusStyle === 'square' &&
        css`
            border-radius: 0;

            &:before {
                border-radius: 0;
            }
        `};

    ${(p) =>
        p.outline &&
        css`
            border: 3px solid white;
        `};

    ${(p) =>
        p.size === 'tiny' &&
        css`
            font-size: ${(p) => p.theme.font.size.tiny};
            border-width: 2px;
        `};

    ${(p) =>
        p.size === 'small' &&
        css`
            font-size: ${(p) => p.theme.font.size.small};
            border-width: 2px;
        `};

    ${(p) =>
        p.size === 'normal' &&
        css`
            font-size: ${(p) => p.theme.font.size.normal};
            border-width: 3px;
        `};

    ${(p) =>
        p.size === 'large' &&
        css`
            font-size: ${(p) => p.theme.font.size.large};
            border-width: 4px;
        `};

    ${(p) =>
        p.size === 'xlarge' &&
        css`
            font-size: ${(p) => p.theme.font.size.xlarge};
            border-width: 4px;
        `};

    ${(p) =>
        p.size === 'xxlarge' &&
        css`
            font-size: ${(p) => p.theme.font.size.xxlarge};
            border-width: 4px;
        `};

    ${(p) =>
        !p.image &&
        css`
            display: flex;
            align-items: center;
            justify-content: center;
            font-weight: ${(p) => p.theme.font.weight.semibold};
            color: ${(p) => p.theme.color.text.black};
            text-align: center;
            padding-top: 1px;
        `};

    ${(p) =>
        p.image &&
        css`
            display: block;
            background-image: url('${p.image}?size=${avatarSizes[p.size]}');
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
        `};

    &:before {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }

    &:not([disabled]) {
        &:hover:before {
            background-color: ${() => transparentize(0.8, 'white')};
        }

        &:active:before {
            background-color: ${transparentize(0.95, 'black')};
        }
    }

    ${(p) =>
        p.showHoverOverlay &&
        css`
            &:hover:before {
                background-color: ${() => transparentize(0.8, 'white')};
            }
        `};

    .AvatarRole {
        position: absolute;

        display: flex;
        align-items: center;
        justify-content: center;
        width: 12px;
        height: 12px;
        border-radius: 50%;
        background: ${(p) => p.theme.color.secondary.main};
        color: white;
        user-select: none;

        font-size: 10px;
        font-weight: ${(p) => p.theme.font.weight.bold};
        box-shadow: 0 0 0 2px white;

        ${(p) =>
            p.size === 'small' &&
            css`
                top: -4px;
                right: -4px;
            `};
        ${(p) =>
            p.size === 'normal' &&
            css`
                top: -2px;
                right: -2px;
            `};
        ${(p) =>
            p.size === 'large' &&
            css`
                top: 0;
                right: 0;
            `};
    }

    .AvatarStatus {
        position: absolute;
        bottom: 0;
        right: 0;
    }
`

export default avatarStyle
