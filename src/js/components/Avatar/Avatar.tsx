import React from 'react'
import styled from 'styled-components'

import Tooltip from 'js/components/Tooltip/Tooltip'
import shouldForwardProp from 'js/lib/helpers/shouldForwardProp'

import EmailIcon from 'icons/email.svg'

import avatarStyle from './avatarStyle'
import StatusIndicator from './StatusIndicator'

export interface Props {
    size?: 'tiny' | 'small' | 'normal' | 'large' | 'xlarge' | 'xxlarge'
    image?: string
    name?: string
    role?: string
    outline?: boolean
    radiusStyle?: 'round' | 'rounded' | 'square'
    status?: 'online' | 'offline'
    showHoverOverlay?: boolean
    isEmail?: boolean
}

const Wrapper = styled.div.withConfig({
    shouldForwardProp: shouldForwardProp([
        'size',
        'radiusStyle',
        'outline',
        'showHoverOverlay',
        'image',
    ]),
})<Props>`
    ${avatarStyle};
`

const Avatar = ({
    size = 'normal',
    image,
    name,
    role,
    outline,
    radiusStyle = 'round',
    showHoverOverlay,
    status,
    isEmail,
    ...rest
}: Props) => (
    <Wrapper
        size={size}
        image={image}
        outline={outline}
        showHoverOverlay={showHoverOverlay}
        radiusStyle={radiusStyle}
        {...rest}
    >
        {!image && (
            <>
                {isEmail ? (
                    <EmailIcon />
                ) : name ? (
                    <span aria-hidden>{name.substr(0, 1).toUpperCase()}</span>
                ) : null}
            </>
        )}

        {status && (
            <StatusIndicator $status={status} className="AvatarStatus" />
        )}

        {!!role && (
            <Tooltip
                content={role}
                theme="primary-white"
                offset={[0, 4]}
                placement="top"
            >
                <div className="AvatarRole">
                    {role.substr(0, 1).toUpperCase()}
                </div>
            </Tooltip>
        )}
    </Wrapper>
)

export default Avatar
