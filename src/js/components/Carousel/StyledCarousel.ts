import styled, { css } from 'styled-components'

interface WrapperProps {
    $slidesCount?: number
    $styleVars?: {
        'content-horizontal': string
        'content-bottom': string
        'overlay-gradient-height': string
        'title-size': string
        'button-top': string
    }
    $splitNavButtons?: boolean
}

export default styled.div<WrapperProps>`
    position: relative;

    swiper-container {
        height: 100%;
        z-index: 0; // Override z-index from swiper (messes with page editor)
    }

    swiper-slide {
        height: 100%;
    }

    /* Override swiper vars */
    --swiper-pagination-bottom: 24px;
    --swiper-pagination-color: transparent;
    --swiper-pagination-bullet-size: 22px;
    --swiper-pagination-bullet-horizontal-gap: 0;
    --swiper-pagination-bullet-inactive-color: transparent;
    --swiper-pagination-bullet-inactive-opacity: 1;

    /* Local vars */
    --carousel-content-horizontal-offset: ${(p) =>
        p.$styleVars['content-horizontal']};
    --carousel-content-bottom-offset: ${(p) => p.$styleVars['content-bottom']};
    --carousel-overlay-gradient-height: ${(p) =>
        p.$styleVars['overlay-gradient-height']};
    --carousel-title-font-size: ${(p) =>
        p.theme.fontHeading.size[p.$styleVars['title-size']]};
    --carousel-title-font-line-height: ${(p) =>
        p.theme.fontHeading.lineHeight[p.$styleVars['title-size']]};
    --carousel-button-top-offset: ${(p) => p.$styleVars['button-top']};

    .swiper-navigation-container {
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
        margin: 0 auto;
        pointer-events: none;
        z-index: 1;
    }

    .swiper-navigation {
        position: absolute;
        right: var(--carousel-content-horizontal-offset);
        bottom: 20px;
        display: flex;

        ${(p) =>
            p.$splitNavButtons
                ? css`
                      // Position nav buttons on either side on smaller screens
                      left: var(--carousel-content-horizontal-offset);
                      justify-content: space-between;
                  `
                : css`
                      // Position nav buttons on right side on larger screens
                      gap: 8px;
                  `};
    }

    .swiper-prev,
    .swiper-next {
        pointer-events: auto;
        display: flex !important;
        text-indent: 0;
        transform: none;
        transition:
            opacity ${(p) => p.theme.transition.fast},
            visibility ${(p) => p.theme.transition.fast};
        color: white;
        border-radius: 50%; // For focus outline
        outline-offset: 2px;
        outline-color: white !important;

        &[disabled] {
            visibility: hidden;
            opacity: 0;
            pointer-events: none;
        }
    }
`
