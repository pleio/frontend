import React from 'react'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import { Container } from 'js/components/Grid/Grid'
import Img from 'js/components/Img/Img'
import { LinkContent, LinkElement } from 'js/components/LinkItem/LinkItem'

const Wrapper = styled.div`
    background: white;
    width: 100%;
    height: 100%;
    display: flex;
    align-items: flex-end;

    .carousel-slide--image {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    .carousel-slide--content {
        position: relative;
        width: 100%;
        padding: var(--carousel-overlay-gradient-height)
            calc(var(--carousel-content-horizontal-offset) - 20px)
            var(--carousel-content-bottom-offset);
    }

    .carousel-slide--overlay {
        position: absolute;
        bottom: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: linear-gradient(
            180deg,
            rgba(0, 0, 0, 0) 0,
            rgba(0, 0, 0, 0.42) var(--carousel-overlay-gradient-height),
            rgba(0, 0, 0, 0.42) 100%
        );
    }

    .carousel-slide--container {
        position: relative; // Postion on top of overlay
        display: flex;
        flex-direction: column;
        align-items: flex-start;
    }

    .carousel-slide--title {
        display: flex; // Make link a clickable block
        max-width: 660px;
        color: white;
        font-size: var(--carousel-title-font-size);
        line-height: var(--carousel-title-font-line-height);
        font-weight: ${(p) => p.theme.fontHeading.weight.bold};
        transition: opacity ${(p) => p.theme.transition.fast};

        a {
            outline-color: white !important;
        }
    }

    .carousel-slide--button-wrapper {
        margin-top: var(--carousel-button-top-offset);
        outline-offset: 2px;
        outline-color: white !important;
    }

    .carousel-slide--button {
        background-color: white;
        color: black;
    }
`

export interface CarouselSlideProps {
    title: string
    url: string
    buttonText: string
    largeButton: boolean
    image: any
}

const CarouselSlide = ({
    title,
    url,
    buttonText,
    largeButton,
    image,
}: CarouselSlideProps) => (
    <Wrapper>
        {image?.url && (
            <Img
                src={image?.url}
                objectFit="cover"
                className="carousel-slide--image"
            />
        )}

        <div className="carousel-slide--content">
            <div className="carousel-slide--overlay" />

            <Container className="carousel-slide--container">
                {title && (
                    <h3 className="carousel-slide--title">
                        {url ? (
                            <LinkElement url={url}>{title}</LinkElement>
                        ) : (
                            title
                        )}
                    </h3>
                )}
                {buttonText && url && (
                    <LinkElement
                        url={url}
                        className="carousel-slide--button-wrapper"
                        aria-hidden
                    >
                        <Button
                            as="div"
                            variant="primary"
                            size={largeButton ? 'large' : 'normal'}
                            className="carousel-slide--button"
                        >
                            <LinkContent url={url}>{buttonText}</LinkContent>
                        </Button>
                    </LinkElement>
                )}
            </Container>
        </div>
    </Wrapper>
)

export default CarouselSlide
