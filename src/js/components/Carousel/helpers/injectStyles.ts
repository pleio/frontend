export default [
    `
        .swiper-pagination-bullet {
            display: inline-flex;
            align-items: center;
            justify-content: center;
            vertical-align: middle;
            background: none;
            transition: .2s left;
            outline-offset: -1px;
        }
        .swiper-pagination-bullet:focus,
        .swiper-pagination-bullet:focus-visible {
            outline: 2px solid white;
        }
        .swiper-pagination-bullet:before {
            content: '';
            display: block;
            width: 14px;
            height: 14px;
            border-radius: 50%;
            box-sizing: border-box;
            border: 3px solid white;
            transition: .2s transform;
        }
        .swiper-pagination-bullet-active:before {
            background: white;
        }
        .swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-prev,
        .swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-next {
            transform: scale(1);
        }
        .swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-prev:before,
        .swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-next:before {
            transform: scale(1);
        }
        .swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-prev-prev,
        .swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-next-next {
            transform: scale(1);
        }
        .swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-prev-prev:before,
        .swiper-pagination-bullets-dynamic .swiper-pagination-bullet-active-next-next:before {
            transform: scale(.8);
        }
        `,
]
