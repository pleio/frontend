import { Size } from '../Carousel'

export default (size: Size) => {
    switch (size) {
        case 'sm':
            return {
                'content-horizontal': '20px',
                'content-bottom': '72px',
                'overlay-gradient-height': '50px',
                'title-size': 'small',
                'button-top': '12px',
            }
        case 'md':
            return {
                'content-horizontal': '32px',
                'content-bottom': '76px',
                'overlay-gradient-height': '60px',
                'title-size': 'normal',
                'button-top': '16px',
            }
        case 'lg':
            return {
                'content-horizontal': '40px',
                'content-bottom': '60px',
                'overlay-gradient-height': '70px',
                'title-size': 'large',
                'button-top': '20px',
            }
        case 'xl':
            return {
                'content-horizontal': '40px',
                'content-bottom': '60px',
                'overlay-gradient-height': '80px',
                'title-size': 'huge',
                'button-top': '24px',
            }
    }
}
