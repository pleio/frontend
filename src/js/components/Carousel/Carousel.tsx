import React, { useCallback, useEffect, useRef, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useMeasure } from 'react-use'
import { register } from 'swiper/element/bundle'

import { Container } from 'js/components/Grid/Grid'
import IconButton from 'js/components/IconButton/IconButton'

import ArrowCircleLeftIcon from 'icons/arrow-circle-left-large.svg'
import ArrowCircleRightIcon from 'icons/arrow-circle-right-large.svg'

import CarouselSlide, { CarouselSlideProps } from './CarouselSlide'
import getStyleVars from './helpers/getStyleVars'
import injectStyles from './helpers/injectStyles'
import StyledCarousel from './StyledCarousel'

interface Props {
    slides: CarouselSlideProps[]
    className?: string
}

const CarouselWrapper = ({ ...props }: Props) => {
    const [ref, { width }] = useMeasure()

    const getSize = (width: number) => {
        if (width < 320) return 'sm'
        else if (width < 490) return 'md'
        else if (width < 1000) return 'lg'
        return 'xl'
    }

    const [size, setSize] = useState(null)

    useEffect(() => {
        if (!width) return
        setSize(getSize(width))
    }, [width])

    // Render the carousel only when size is calculated
    return <div ref={ref}>{size && <Carousel {...props} size={size} />}</div>
}

export type Size = 'sm' | 'md' | 'lg' | 'xl'

interface CarouselProps extends Props {
    size: Size
}

const Carousel = ({ slides, size, ...rest }: CarouselProps) => {
    const { t } = useTranslation()

    const refSwiper = useRef(null)

    const slidesCount = slides?.length || 0

    useEffect(() => {
        if (slidesCount < 2) return

        // Register Swiper web component
        register()

        const swiperRef = refSwiper?.current

        const params = {
            slidesPerView: 1,
            loop: true,
            grabCursor: true,
            effect: 'fade',
            speed: 250,
            spaceBetween: 0,
            pagination: {
                clickable: true,
                dynamicBullets: slidesCount > 3,
            },
            a11y: {
                prevSlideMessage: t('aria.prev-slide'),
                nextSlideMessage: t('aria.next-slide'),
                firstSlideMessage: t('aria.first-slide'),
                lastSlideMessage: t('aria.last-slide'),
                paginationBulletMessage: t('aria.go-to-slide'),
            },
            injectStyles,
        }

        Object.assign(swiperRef, params)

        swiperRef.initialize()
    }, [t, slidesCount])

    const handlePrev = useCallback(() => {
        refSwiper.current.swiper.slidePrev()
    }, [])

    const handleNext = useCallback(() => {
        refSwiper.current.swiper.slideNext()
    }, [])

    const styleVars = getStyleVars(size)

    const RenderSlide = ({ title, url, image, buttonText }) => (
        <CarouselSlide
            title={title}
            url={url}
            image={image}
            buttonText={buttonText}
            largeButton={size === 'xl'}
        />
    )

    if (slidesCount === 0) return null

    return (
        <StyledCarousel
            $styleVars={styleVars}
            $slidesCount={slidesCount}
            $splitNavButtons={['sm', 'md'].includes(size)}
            {...rest}
        >
            {slidesCount > 1 ? (
                <>
                    <Container className="swiper-navigation-container">
                        <div className="swiper-navigation">
                            <IconButton
                                size="normal"
                                variant="secondary"
                                aria-label={t('aria.prev-slide')}
                                onClick={handlePrev}
                                className="swiper-prev"
                            >
                                <ArrowCircleLeftIcon />
                            </IconButton>
                            <IconButton
                                size="normal"
                                variant="secondary"
                                aria-label={t('aria.next-slide')}
                                onClick={handleNext}
                                className="swiper-next"
                            >
                                <ArrowCircleRightIcon />
                            </IconButton>
                        </div>
                    </Container>

                    <swiper-container ref={refSwiper} init="false">
                        {slides.map((props, i) => {
                            return (
                                <swiper-slide key={i}>
                                    <RenderSlide {...props} />
                                </swiper-slide>
                            )
                        })}
                    </swiper-container>
                </>
            ) : (
                <RenderSlide {...slides[0]} />
            )}
        </StyledCarousel>
    )
}

export default CarouselWrapper
