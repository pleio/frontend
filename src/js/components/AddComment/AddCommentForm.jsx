import React, { useLayoutEffect, useRef, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useLocation } from 'react-router-dom'
import styled from 'styled-components'

import Avatar from 'js/components/Avatar/Avatar'
import Button from 'js/components/Button/Button'
import CommentEditor from 'js/components/Comment/CommentEditor'
import Flexer from 'js/components/Flexer/Flexer'
import ReplyWithoutAccount from 'js/components/ReplyWithoutAccount'
import Text from 'js/components/Text/Text'

const CommentsCreateBar = styled.button`
    width: 100%;
    display: flex;
    align-items: center;
    background-color: ${(p) => p.theme.color.grey[10]};
    color: ${(p) => p.theme.color.text.grey};
    border-radius: 16px;
    cursor: text;
    outline-offset: 0;
`

const AddCommentForm = ({
    viewer,
    entity,
    group,
    commentWithoutAccountEnabled,
    returnFocusElement,
    refetchQueries,
    isReply,
}) => {
    const { t } = useTranslation()
    const location = useLocation()
    const refReplyButton = useRef()

    const [showAddComment, setShowAddComment] = useState(false)
    const toggleShowAddComment = () => {
        setShowAddComment(!showAddComment)
    }

    useLayoutEffect(() => {
        // Immediately show editor if there are no comments
        if (
            !!returnFocusElement?.current &&
            entity.canComment &&
            entity.commentCount === 0
        ) {
            setShowAddComment(true)
        }
    }, [entity])

    const [showReplyWithoutAccount, setShowReplyWithoutAccount] =
        useState(false)
    const toggleReplyWithoutAccount = () => {
        setShowReplyWithoutAccount(!showReplyWithoutAccount)
    }

    const hideEditor = (hideComments) => {
        if (
            hideComments &&
            entity.commentCount === 0 &&
            !!returnFocusElement?.current
        ) {
            // Hide comment list
            returnFocusElement?.current.click()
            setTimeout(() => {
                returnFocusElement?.current.focus()
            }, 0)
        } else {
            toggleShowAddComment()
            setTimeout(() => {
                refReplyButton?.current?.focus()
            }, 0)
        }
    }

    if (!viewer) return null

    const { loggedIn, user } = viewer

    const showLoginRegister = window.__SETTINGS__.showLoginRegister

    const excerpt =
        entity.excerpt?.length > 30
            ? `${entity.excerpt.substring(0, 30)}...`
            : entity.excerpt

    return (
        <>
            {loggedIn && !showAddComment && (
                <CommentsCreateBar
                    ref={refReplyButton}
                    type="button"
                    onClick={toggleShowAddComment}
                    aria-label={t(
                        isReply
                            ? 'comments.reply-to-item'
                            : 'comments.add-comment-to-item',
                        {
                            title: isReply ? excerpt : entity.title,
                        },
                    )}
                >
                    <Avatar
                        name={user.name}
                        image={user.icon}
                        disabled
                        style={{ marginRight: '10px' }}
                    />
                    {isReply
                        ? t('comments.reply-placeholder')
                        : t('comments.comment-placeholder')}
                </CommentsCreateBar>
            )}
            {loggedIn && showAddComment && (
                <CommentEditor
                    viewer={viewer}
                    entity={entity}
                    group={group}
                    hideEditor={hideEditor}
                    refetchQueries={refetchQueries}
                    isReply={isReply}
                />
            )}
            {!loggedIn &&
                !showReplyWithoutAccount &&
                (showLoginRegister || commentWithoutAccountEnabled) && (
                    <Flexer gutter="small" wrap>
                        <Text size="small" variant="grey">
                            {t('comments.to-reply')}
                        </Text>
                        {showLoginRegister && (
                            <Button
                                size="small"
                                variant="primary"
                                onClick={toggleShowAddComment}
                                aria-label={t('comments.reply-to-item', {
                                    title: entity.title,
                                })}
                                as={Link}
                                to="/login"
                                state={{ next: location.pathname }}
                            >
                                {t('action.login')}
                            </Button>
                        )}
                        {commentWithoutAccountEnabled && (
                            <>
                                {showLoginRegister && (
                                    <Text size="small" variant="grey">
                                        {t('global.or')}
                                    </Text>
                                )}
                                <Button
                                    size="small"
                                    variant="secondary"
                                    onClick={toggleReplyWithoutAccount}
                                    aria-label={t('comments.reply-to-item', {
                                        title: entity.title,
                                    })}
                                >
                                    {t('global.use-email')}
                                </Button>
                            </>
                        )}
                    </Flexer>
                )}
            {showReplyWithoutAccount && (
                <ReplyWithoutAccount
                    containerGuid={entity.guid}
                    onClose={toggleReplyWithoutAccount}
                />
            )}
        </>
    )
}

export default AddCommentForm
