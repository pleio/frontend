import React from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import AddCommentForm from 'js/components/AddComment/AddCommentForm'
import Text from 'js/components/Text/Text'
import JoinGroupButton from 'js/group/components/JoinGroupButton'
import { useSiteStore } from 'js/lib/stores/SiteStore'

const Wrapper = styled.div`
    width: 100%;

    .CommentsMessage {
        display: flex;
        flex-direction: column;
        align-items: center;
    }
`

const AddComment = ({
    viewer,
    entity,
    refCommentsButton,
    refetchQueries,
    ...rest
}) => {
    const { group, isClosed } = entity

    const isMember = group?.membership === 'joined'
    const { t } = useTranslation()

    const { site } = useSiteStore()
    const { commentWithoutAccountEnabled } = site

    let content

    if (!!entity.group && !isMember) {
        content = (
            <div className="CommentsMessage">
                <Text
                    size="small"
                    variant="grey"
                    style={{ marginBottom: '4px' }}
                >
                    {t('comments.message-not-a-member')}
                </Text>
                <JoinGroupButton
                    entity={entity.group}
                    loggedIn={viewer?.loggedIn}
                    size="small"
                />
            </div>
        )
    } else if (isClosed) {
        content = (
            <div className="CommentsMessage">
                <Text size="small" variant="grey">
                    {t('comments.message-comments-closed')}
                </Text>
            </div>
        )
    } else {
        content = (
            <AddCommentForm
                entity={entity}
                group={group}
                commentWithoutAccountEnabled={commentWithoutAccountEnabled}
                viewer={viewer}
                returnFocusElement={refCommentsButton}
                refetchQueries={refetchQueries}
            />
        )
    }

    return <Wrapper {...rest}>{content}</Wrapper>
}

export default AddComment
