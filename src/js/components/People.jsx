import React from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

import Avatar from 'js/components/Avatar/Avatar'
import Tooltip from 'js/components/Tooltip/Tooltip'

import CheckIcon from 'icons/check.svg'
import CheckSmallIcon from 'icons/check-small.svg'

const Wrapper = styled.div`
    flex-shrink: 0;
    display: flex;
    justify-content: center;

    .PeopleWrapper {
        position: relative;
        display: flex;
        margin: -2px;
    }

    .PeopleAvatars {
        display: flex;

        > * {
            &:not(:first-child) {
                ${(p) =>
                    p.$size === 'small' &&
                    css`
                        margin-left: -8px;
                    `};

                ${(p) =>
                    p.$size === 'normal' &&
                    css`
                        margin-left: -10px;
                    `};

                ${(p) =>
                    p.$size === 'large' &&
                    css`
                        margin-left: -12px;
                    `};
            }
        }
    }

    .PeopleCheck {
        position: relative; // z-index
        display: flex;
        align-items: center;
        justify-content: center;
        background-color: ${(p) => p.theme.color.secondary.main};
        color: white;
        border: 3px solid white;
        border-radius: 50%;

        ${(p) =>
            p.$size === 'small' &&
            css`
                width: 24px;
                height: 24px;
                border-width: 2px;
            `};

        ${(p) =>
            p.$size === 'normal' &&
            css`
                width: 32px;
                height: 32px;
                border-width: 3px;
            `};

        ${(p) =>
            p.$size === 'large' &&
            css`
                width: 40px;
                height: 40px;
                border-width: 4px;
            `};
    }

    .PeopleMore {
        position: absolute;
        top: 0;
        bottom: 0;
        left: calc(100% + 2px);
        display: flex;
        align-items: center;
        font-size: ${(p) => p.theme.font.size.tiny};
        line-height: ${(p) => p.theme.font.lineHeight.tiny};
        font-weight: ${(p) => p.theme.font.weight.bold};
        color: ${(p) => p.theme.color.text.black};
        white-space: nowrap;
    }
`

const People = ({
    users,
    total,
    showCheck,
    checkLabel,
    size = 'normal',
    ...rest
}) => {
    if ((!users || users.length === 0) && !showCheck) return null

    const moreMembers = total - users.length - (showCheck ? 1 : 0)

    return (
        <Wrapper $size={size} {...rest}>
            <div className="PeopleWrapper">
                <div className="PeopleAvatars">
                    {users.map((user, i) => (
                        <Avatar
                            key={`people-${i}`}
                            image={user.icon}
                            name={user.name}
                            outline
                            disabled
                            size={size}
                        />
                    ))}
                    {showCheck && (
                        <Tooltip
                            theme="primary-white"
                            content={checkLabel}
                            offset={[0, 5]}
                        >
                            <div className="PeopleCheck">
                                {size === 'small' ? (
                                    <CheckSmallIcon />
                                ) : (
                                    <CheckIcon />
                                )}
                            </div>
                        </Tooltip>
                    )}
                </div>
                {moreMembers > 0 && (
                    <div className="PeopleMore">+{moreMembers}</div>
                )}
            </div>
        </Wrapper>
    )
}

People.propTypes = {
    size: PropTypes.oneOf(['small', 'normal', 'large']),
}

export default People
