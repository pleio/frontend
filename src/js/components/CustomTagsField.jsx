import React, { useRef, useState } from 'react'
import { useTranslation } from 'react-i18next'
import PropTypes from 'prop-types'

import Flexer from 'js/components/Flexer/Flexer'
import Tag from 'js/components/Tag/Tag'
import Textfield from 'js/components/Textfield/Textfield'

const CustomTagsField = ({
    value,
    label,
    name,
    onAddTag,
    onRemoveTag,
    ...rest
}) => {
    const [inputValue, setInputValue] = useState('')

    const refInput = useRef()

    const { t } = useTranslation()

    const handleChangeInput = (evt) => setInputValue(evt.target.value)

    const handleKeyPress = (evt) => {
        if (evt.key === 'Enter' && inputValue) {
            // Enter button
            evt.preventDefault()
            addTag(inputValue)
        }
    }

    const addTag = (name) => {
        onAddTag(name)
        setInputValue('')
        refInput.current.focus()
    }

    return (
        <div {...rest}>
            <Textfield
                ref={refInput}
                name={name || 'custom-tags-field'}
                label={label || t('global.custom-tags')}
                helper={t('form.custom-tags-helper')}
                autoComplete="off"
                onKeyPress={handleKeyPress}
                onChange={handleChangeInput}
                value={inputValue}
            />
            {value.size > 0 && (
                <Flexer
                    justifyContent="flex-start"
                    gutter="tiny"
                    style={{
                        marginTop: '8px',
                        flexWrap: 'wrap',
                    }}
                >
                    {value.map((tag, i) => (
                        <Tag
                            key={i}
                            style={{ marginBottom: '4px' }}
                            onRemove={() => onRemoveTag(tag)}
                        >
                            {tag}
                        </Tag>
                    ))}
                </Flexer>
            )}
        </div>
    )
}

CustomTagsField.propTypes = {
    value: PropTypes.any,
    label: PropTypes.string,
}

export default CustomTagsField
