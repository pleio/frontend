import React from 'react'
import { useTranslation } from 'react-i18next'
import { NavLink } from 'react-router-dom'
import styled from 'styled-components'

import HideVisually from 'js/components/HideVisually/HideVisually'

import MenuItem, { Wrapper as MenuItemElement } from './NavigationMenuItem'

const Wrapper = styled.nav`
    > ul {
        display: flex;
        flex-wrap: wrap;

        > li {
            flex-shrink: 0;
        }
    }
`

const NavigationMenu = ({ site }) => {
    const { t } = useTranslation()

    if (!site) return null

    return (
        <Wrapper>
            <ul>
                <li>
                    <MenuItemElement
                        as={NavLink}
                        to="/"
                        end
                        $showIcon={site.showIcon}
                    >
                        {site.showIcon ? (
                            <>
                                <HideVisually>
                                    {t('global.home-page')}
                                </HideVisually>
                                <img
                                    src={site.icon}
                                    alt={site.iconAlt}
                                    width="auto"
                                    height="auto"
                                    aria-hidden={!site.iconAlt}
                                />
                            </>
                        ) : (
                            <span>{t('global.home-title')}</span>
                        )}
                    </MenuItemElement>
                </li>

                {site.menu.map((item, i) => (
                    <MenuItem key={`nav-menu-item${i}`} item={item} />
                ))}
            </ul>
        </Wrapper>
    )
}

export default NavigationMenu
