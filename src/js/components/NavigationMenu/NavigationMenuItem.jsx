import React from 'react'
import { useTranslation } from 'react-i18next'
import { NavLink } from 'react-router-dom'
import { getCleanUrl, isExternalUrl } from 'helpers'
import { getReadableColor, textContrastIsAA } from 'helpers/getContrast'
import styled, { css } from 'styled-components'

import DropdownButton from 'js/components/DropdownButton/DropdownButton'

import ChevronDownIcon from 'icons/chevron-down.svg'
import ExternalIcon from 'icons/new-window-small.svg'

export const Wrapper = styled.button`
    position: relative;
    display: flex;
    align-items: center;

    height: ${(p) => p.theme.headerBarHeight}px;
    padding: 0 16px;

    color: ${(p) => getReadableColor(p.theme.color.header.main)};
    background-color: transparent;

    font-size: ${(p) => p.theme.font.size.normal};
    line-height: ${(p) => p.theme.font.lineHeight.normal};
    font-weight: ${(p) => p.theme.font.weight.semibold};

    text-shadow: none;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    user-select: none;
    outline-color: ${(p) =>
        getReadableColor(p.theme.color.header.main)} !important;

    &:before {
        content: '';
        position: absolute;
        top: 5px;
        bottom: 5px;
        left: 4px;
        right: 4px;
        border-radius: ${(p) => p.theme.radius.small};
        border: 2px solid transparent;
    }

    > * {
        position: relative;
    }

    ${(p) =>
        !p.$showIcon &&
        css`
            &:hover:before {
                background-color: ${(p) => p.theme.color.header.hover};
            }

            &:active:before {
                background-color: ${(p) => p.theme.color.header.active};
            }

            &[aria-current='page'] {
                ${textContrastIsAA(p.theme.color.header.main)
                    ? css`
                          color: ${(p) => p.theme.color.header.main};

                          &:before {
                              background-color: white;
                          }
                      `
                    : css`
                          &:before {
                              border-color: black;
                          }
                      `};
            }
        `};

    > img {
        display: block;
        max-height: 100%;
    }
`

const NavigationMenuItem = ({ item }) => {
    const { t } = useTranslation()

    const hasSubItems = item.children.length > 0
    const itemLink = getCleanUrl(item.link)
    const itemExternal = isExternalUrl(itemLink)
    const label = item.localLabel || item.label || itemLink

    if (hasSubItems) {
        const itemOptions = item.children.map((subItem) => {
            const subItemOptions = subItem.children.map((subSubitem) => {
                const subSubItemLink = getCleanUrl(subSubitem.link)
                const suSubItemExternal = isExternalUrl(subSubItemLink)
                const subSubItemLabel =
                    subSubitem.localLabel || subSubitem.label || subSubItemLink

                if (suSubItemExternal) {
                    return {
                        href: subSubItemLink,
                        name: subSubItemLabel,
                        target: '_blank',
                    }
                } else {
                    return {
                        to: subSubItemLink,
                        name: subSubItemLabel,
                    }
                }
            })

            const subItemLink = getCleanUrl(subItem.link)
            const subItemExternal = isExternalUrl(subItemLink)
            const subItemLabel =
                subItem.localLabel || subItem.label || subItemLink

            if (subItemExternal) {
                return {
                    href: subItemLink,
                    name: subItemLabel,
                    target: '_blank',
                    subOptions: subItemOptions,
                }
            } else {
                return {
                    to: subItemLink,
                    name: subItemLabel,
                    subOptions: subItemOptions,
                }
            }
        })

        return (
            <li>
                <DropdownButton
                    options={itemOptions}
                    trigger="click mouseenter"
                    placement="bottom-start"
                    arrow={true}
                    wrapContent={true}
                    showArrow
                    arrowElement={
                        <ChevronDownIcon style={{ marginLeft: '8px' }} />
                    }
                >
                    <Wrapper>
                        <span>{label}</span>
                    </Wrapper>
                </DropdownButton>
            </li>
        )
    } else {
        return (
            <li>
                {itemExternal ? (
                    <Wrapper
                        as="a"
                        href={itemLink}
                        target="_blank"
                        rel="noopener noreferrer"
                        aria-label={`${label}${t(
                            'global.opens-in-new-window',
                        )}`}
                    >
                        <span>{label}</span>
                        <ExternalIcon
                            style={{
                                marginLeft: '8px',
                                marginBottom: '2px',
                            }}
                        />
                    </Wrapper>
                ) : (
                    <Wrapper as={NavLink} to={itemLink} end>
                        <span>{label}</span>
                    </Wrapper>
                )}
            </li>
        )
    }
}

export default NavigationMenuItem
