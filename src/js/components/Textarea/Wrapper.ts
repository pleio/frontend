import styled, { css } from 'styled-components'

import shouldForwardProp from 'js/lib/helpers/shouldForwardProp'

import { Props } from './Textarea'

const Wrapper = styled.div.withConfig({
    shouldForwardProp: shouldForwardProp(['minHeight', 'size', 'hasValue']),
})<Props>`
    position: relative;
    width: 100%;

    .TextareaInput {
        display: block;
        width: 100%;
        min-height: ${(p) => p.minHeight || '100px'};
        padding: 9px 16px;

        color: ${(p) => p.theme.color.text.black};
        font: inherit;
        border: 1px solid ${(p) => p.theme.color.grey[40]};
        border-radius: ${(p) => p.theme.radius.small};

        resize: vertical;

        ${(p) =>
            p.size === 'small' &&
            css`
                font-size: ${(p) => p.theme.font.size.small};
                line-height: ${(p) => p.theme.font.lineHeight.small};
            `};

        ${(p) =>
            p.size === 'normal' &&
            css`
                font-size: ${(p) => p.theme.font.size.normal};
                line-height: ${(p) => p.theme.font.lineHeight.normal};
            `};

        &::placeholder {
            color: transparent;
        }

        &:-webkit-autofill {
            box-shadow: 0 0 0 100px white inset;
            -webkit-text-fill-color: ${(p) => p.theme.color.text.black};
        }

        &:not([disabled]) {
            background-color: white;
        }

        &[disabled] {
            color: ${(p) => p.theme.color.grey[40]};
            background-color: ${(p) => p.theme.color.grey[20]};
            border-color: transparent;
        }
    }

    .TextareaLabel {
        padding-left: 15px;
        top: 10px;
        height: ${(p) => p.theme.font.lineHeight[p.size]};

        ${(p) =>
            p.hasValue &&
            css`
                height: ${(p) => p.theme.font.lineHeight.tiny};
                transform: translateY(-17px);
            `};
    }
`

export default Wrapper
