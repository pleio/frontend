import React, { ForwardedRef, forwardRef, Ref } from 'react'

import FieldLabel from 'js/components/FieldLabel'
import Loader from 'js/components/Form/Loader'

import Wrapper from './Wrapper'

export interface Props {
    name?: string
    label?: string
    size?: 'small' | 'normal'
    minHeight?: string
    required?: boolean
    ref?: ForwardedRef<HTMLTextAreaElement>
    placeholder?: string
    value?: string
    disabled?: boolean
    hasValue?: boolean
    successState?: {
        loading: boolean
        saved: boolean
    }
}

const Textarea = forwardRef<HTMLTextAreaElement, Props>(
    (
        {
            name,
            label,
            placeholder,
            size = 'normal',
            minHeight,
            required,
            value,
            disabled,
            successState = {
                loading: false,
                saved: false,
            },
            ...rest
        }: Props,
        ref: Ref<HTMLTextAreaElement>,
    ) => {
        return (
            <Wrapper size={size} minHeight={minHeight} hasValue={!!value}>
                <FieldLabel
                    as="label"
                    size={size}
                    htmlFor={name}
                    label={label}
                    placeholder={placeholder}
                    required={required}
                    active={!!value}
                    disabled={disabled}
                    className="TextareaLabel"
                />
                <textarea
                    ref={ref}
                    rows={4}
                    className="TextareaInput"
                    id={name}
                    placeholder={label}
                    value={value || ''}
                    required={required}
                    aria-required={required}
                    {...rest}
                />
                <Loader
                    position="top-right"
                    isLoading={successState.loading}
                    isSaved={successState.saved}
                />
            </Wrapper>
        )
    },
)

Textarea.displayName = 'Textarea'

export default Textarea
