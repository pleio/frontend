import React from 'react'

import * as Styled from './Slider.style'

const Slider = (props) => (
    <Styled.SliderRoot {...props}>
        <Styled.SliderTrack>
            <Styled.SliderRange />
        </Styled.SliderTrack>

        <Styled.SliderThumb aria-label="Progress" />
    </Styled.SliderRoot>
)

export default Slider
