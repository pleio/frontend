import React from 'react'
import PropTypes from 'prop-types'

import Flexer from 'js/components/Flexer/Flexer'
import Text from 'js/components/Text/Text'

import Slider from './Slider'

function formatTime(seconds) {
    const roundedSeconds = Math.round(seconds)
    const minutes = Math.floor(roundedSeconds / 60)
    const remainingSeconds = roundedSeconds % 60

    return `${minutes.toString().padStart(2, '0')}:${remainingSeconds
        .toString()
        .padStart(2, '0')}`
}

const AudioProgressBar = ({ time, setTime, className }) => {
    const percentage = (time.current / time.duration) * 100 || 0

    return (
        <div className={className}>
            <Slider
                value={[percentage]}
                onValueChange={(value) => {
                    setTime((value / 100) * time.duration)
                }}
            />

            <Flexer justifyContent="space-between">
                <Text size="small" variant="grey">
                    {formatTime(time.current)}
                </Text>

                <Text size="small" variant="grey">
                    {time.left ? `-${formatTime(time.left)}` : '--:--'}
                </Text>
            </Flexer>
        </div>
    )
}

AudioProgressBar.displayName = 'AudioProgressBar'

AudioProgressBar.propTypes = {
    time: PropTypes.shape({
        current: PropTypes.number.isRequired,
        duration: PropTypes.number.isRequired,
        left: PropTypes.number.isRequired,
    }).isRequired,
    setTime: PropTypes.func.isRequired,
}

export default AudioProgressBar
