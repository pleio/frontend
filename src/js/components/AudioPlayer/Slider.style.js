import * as Slider from '@radix-ui/react-slider'
import styled from 'styled-components'

const SliderRoot = styled(Slider.Root)`
    position: relative;
    display: flex;
    align-items: center;
    user-select: none;
    touch-action: none;
    width: 100%;
    height: 20px;
`

const SliderTrack = styled(Slider.Track)`
    background-color: ${(p) => p.theme.color.grey[20]};
    position: relative;
    flex-grow: 1;
    border-radius: 4px;
    height: 8px;
    overflow: hidden;
`

const SliderRange = styled(Slider.Range)`
    position: absolute;
    background-color: ${(p) => p.theme.color.secondary.main};
    height: 100%;
`

const SliderThumb = styled(Slider.Thumb)`
    display: block;
    width: 18px;
    height: 18px;
    background-color: white;
    outline: none;
    border-radius: 50%;
    border: 3px solid ${(p) => p.theme.color.secondary.main};

    &:hover {
        cursor: pointer;
        border: 3px solid ${(p) => p.theme.color.secondary.hover};
    }
`

export { SliderRange, SliderRoot, SliderThumb, SliderTrack }
