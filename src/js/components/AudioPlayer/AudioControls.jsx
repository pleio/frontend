import React from 'react'
import { useTranslation } from 'react-i18next'
import PropTypes from 'prop-types'

import ForwardIcon from 'icons/forward-30.svg'
import PauseIcon from 'icons/pause-large.svg'
import PlayIcon from 'icons/play-large.svg'
import ReplayIcon from 'icons/replay-10.svg'
import SkipNext from 'icons/skip-next.svg'
import SkipPrevious from 'icons/skip-previous.svg'

import Flexer from '../Flexer/Flexer'
import IconButton from '../IconButton/IconButton'

const AudioControls = ({
    audio,
    isPlaying,
    togglePlay,
    rewind,
    fastForward,
    onNext,
    onPrevious,
    canGoNext = false,
    canGoPrevious = false,
    showPreviousNextControls = true,
    size = 'md',
}) => {
    const { t } = useTranslation()
    const prevNextIconSize = size === 'xs' ? 'normal' : 'large'
    const rewindForwardIconSize = size === 'xs' ? 'normal' : 'huge'

    return (
        <Flexer gutter="small">
            <Flexer gutter="tiny">
                {Boolean(showPreviousNextControls) && (
                    <IconButton
                        variant="secondary"
                        size={prevNextIconSize}
                        onClick={onPrevious}
                        disabled={!canGoPrevious}
                        hideContent={!canGoPrevious}
                        tooltip={t('action.previous')}
                        style={{ marginRight: '4px' }}
                    >
                        <SkipPrevious />
                    </IconButton>
                )}

                <IconButton
                    variant="secondary"
                    size={rewindForwardIconSize}
                    onClick={rewind}
                    tooltip={t('audio-player.rewind')}
                >
                    <ReplayIcon />
                </IconButton>
            </Flexer>

            <IconButton
                variant="primary-fill"
                size="huge"
                onClick={togglePlay}
                aria-controls={audio.id}
                data-pause-text={t('audio-player.pause')}
                data-play-text={t('audio-player.play')}
                tooltip={
                    isPlaying ? t('audio-player.pause') : t('audio-player.play')
                }
                theme="primary-white"
            >
                {isPlaying ? <PauseIcon /> : <PlayIcon />}
            </IconButton>

            <Flexer gutter="tiny">
                <IconButton
                    variant="secondary"
                    size={rewindForwardIconSize}
                    onClick={fastForward}
                    tooltip={t('audio-player.fast-forward')}
                >
                    <ForwardIcon />
                </IconButton>

                {Boolean(showPreviousNextControls) && (
                    <IconButton
                        variant="secondary"
                        size={prevNextIconSize}
                        onClick={onNext}
                        disabled={!canGoNext}
                        tooltip={t('action.next')}
                    >
                        <SkipNext />
                    </IconButton>
                )}
            </Flexer>
        </Flexer>
    )
}

AudioControls.displayName = 'AudioControls'

AudioControls.propTypes = {
    audio: PropTypes.instanceOf(Audio),
    togglePlay: PropTypes.func.isRequired,
    fastForward: PropTypes.func.isRequired,
    rewind: PropTypes.func.isRequired,
    isPlaying: PropTypes.bool.isRequired,
    onNext: PropTypes.func,
    onPrevious: PropTypes.func,
    canGoNext: PropTypes.bool,
    canGoPrevious: PropTypes.bool,
    showPreviousNextControls: PropTypes.bool,
}

export default AudioControls
