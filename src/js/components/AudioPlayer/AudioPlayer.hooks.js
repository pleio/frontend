import { useEffect, useRef, useState } from 'react'

/**
 * Hook to simplify playing audio files and creating an audio player.
 */
const useAudioPlayer = ({
    src,
    rewindTimeDelta = 10,
    forwardTimeDelta = 30,
    autoPlay = false,
}) => {
    const audioRef = useRef(null)
    if (audioRef.current === null) {
        audioRef.current = new Audio(src)
        audioRef.current.autoplay = autoPlay
    }

    const [duration, setDuration] = useState(0)
    const [currentTime, setCurrentTime] = useState(0)
    const [playing, setPlaying] = useState(autoPlay)

    /**
     * Play the audio
     */
    const play = async () => {
        await audioRef.current.play()
        setPlaying(true)
    }

    /**
     * Pause the audio
     */
    const pause = () => {
        setPlaying(false)
        audioRef.current.pause()
    }

    /**
     * Toggle between Play & Pause
     */
    const togglePlay = () => {
        if (!playing) {
            play()
        } else {
            pause()
        }
    }

    /**
     * Set the current audio time
     */
    const setAudioTime = (time) => {
        audioRef.current.currentTime = time
        setCurrentTime(time)
    }

    /**
     * Go backwards by the configured delta time in seconds
     */
    const rewind = (delta = rewindTimeDelta) => {
        const time = audioRef.current.currentTime - delta
        setAudioTime(time > 0 ? time : 0)
    }

    /**
     * Go forwards by the configured delta time in seconds
     */
    const fastForward = (delta = forwardTimeDelta) => {
        const time = audioRef.current.currentTime + delta
        setAudioTime(time < duration ? time : duration)
    }

    useEffect(() => {
        const handleLoadedMetaData = () => {
            setDuration(audioRef.current.duration)
            setCurrentTime(audioRef.current.currentTime)
        }

        const handleTimeUpdate = () =>
            setCurrentTime(audioRef.current.currentTime)
        const handleEnded = () => setPlaying(false)
        const handlePause = () => setPlaying(false)
        const handlePlay = () => setPlaying(true)

        audioRef.current.addEventListener(
            'loadedmetadata',
            handleLoadedMetaData,
        )
        audioRef.current.addEventListener('timeupdate', handleTimeUpdate)
        audioRef.current.addEventListener('ended', handleEnded)
        audioRef.current.addEventListener('pause', handlePause)
        audioRef.current.addEventListener('play', handlePlay)

        return () => {
            audioRef.current.pause()

            // Remove event listeners
            audioRef.current.removeEventListener(
                'loadedmetadata',
                handleLoadedMetaData,
            )

            audioRef.current.removeEventListener('timeupdate', handleTimeUpdate)
            audioRef.current.removeEventListener('ended', handleEnded)
            audioRef.current.removeEventListener('pause', handlePause)
            audioRef.current.removeEventListener('play', handlePlay)
        }
    }, [])

    const left =
        duration >= currentTime
            ? Math.round(duration) - Math.round(currentTime)
            : 0

    return {
        audio: audioRef.current,
        playing,
        actions: {
            play,
            pause,
            togglePlay,
            setTime: setAudioTime,
            rewind,
            fastForward,
        },
        time: {
            current: currentTime,
            duration,
            left,
        },
    }
}

export default useAudioPlayer
