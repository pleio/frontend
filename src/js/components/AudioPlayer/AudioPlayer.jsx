import React from 'react'
import PropTypes from 'prop-types'

import Spacer from 'js/components/Spacer/Spacer'
import { useKeyDown } from 'js/lib/hooks'

import AudioControls from './AudioControls'
import useAudioPlayer from './AudioPlayer.hooks'
import AudioProgressBar from './AudioProgressBar'

const DEFAULT_SEEK_DELTA = 5

const AudioPlayer = ({
    src,
    onNext,
    onPrevious,
    canGoNext,
    canGoPrevious,
    autoPlay = false,
    size,
    ...rest
}) => {
    const { audio, playing, time, actions } = useAudioPlayer({
        src,
        autoPlay,
    })

    // Keyboard listeners
    useKeyDown({
        ArrowLeft: () => actions.rewind(DEFAULT_SEEK_DELTA),
        ArrowRight: () => actions.fastForward(DEFAULT_SEEK_DELTA),
    })

    return (
        <Spacer {...rest}>
            <AudioControls
                size={size}
                audio={audio}
                togglePlay={() => actions.togglePlay()}
                rewind={() => actions.rewind()}
                fastForward={() => actions.fastForward()}
                isPlaying={playing}
                onPrevious={() => onPrevious()}
                onNext={() => onNext()}
                canGoNext={canGoNext}
                canGoPrevious={canGoPrevious}
            />

            <AudioProgressBar time={time} setTime={actions.setTime} />
        </Spacer>
    )
}

AudioPlayer.displayName = 'AudioPlayer'

AudioPlayer.propTypes = {
    src: PropTypes.string.isRequired,
    autoPlay: PropTypes.bool,
    onNext: PropTypes.func,
    onPrevious: PropTypes.func,
    canGoNext: PropTypes.bool,
    canGoPrevious: PropTypes.bool,
}

export default AudioPlayer
