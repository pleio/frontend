import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

import { Wrapper as Col } from 'js/components/Grid/Col'

const Row = styled.div.attrs((props) => ({ $gutter: 20, ...props }))`
    flex: 1 1 auto;
    display: flex;
    flex-wrap: wrap;
    margin-left: -${(p) => p.$gutter / 2}px;
    margin-right: -${(p) => p.$gutter / 2}px;

    ${(p) =>
        p.$spacing &&
        css`
            margin-bottom: -${(p) => p.$spacing}px;

            > ${Col} {
                margin-bottom: ${(p) => p.$spacing}px;
            }
        `};

    > ${Col} {
        padding-left: ${(p) => p.$gutter / 2}px;
        padding-right: ${(p) => p.$gutter / 2}px;
    }

    ${(p) =>
        p.$growContent &&
        css`
            > ${Col} {
                display: flex;
                flex-direction: column;

                > *:first-child:last-child {
                    flex-grow: 1;
                }
            }
        `}
`

Row.propTypes = {
    $growContent: PropTypes.bool,
    $gutter: PropTypes.number,
    $spacing: PropTypes.number,
}

export default Row
