import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

const Container = styled.div.attrs((props) => ({ size: 'large', ...props }))`
    width: 100%;
    margin: 0 auto;
    max-width: ${(p) => p.theme.containerWidth[p.size]};

    ${(p) =>
        !p.$noPadding &&
        css`
            padding: 0 20px;
        `};
`
Container.propTypes = {
    size: PropTypes.oneOf(['tiny', 'small', 'normal', 'large', 'huge']),
    $noPadding: PropTypes.bool,
}

export default Container
