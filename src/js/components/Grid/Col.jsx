import React from 'react'
import { media } from 'helpers'
import PropTypes from 'prop-types'
import styled from 'styled-components'

export const Wrapper = styled.div`
    ${media.mobilePortrait`
        width: ${(p) => `${p.$widths[0] * 100}%`};
    `}

    ${media.mobileLandscape`
        width: ${(p) => `${p.$widths[1] * 100}%`};
    `}

    ${media.tablet`
        width: ${(p) => `${p.$widths[2] * 100}%`};
    `}

    ${media.desktop`
        width: ${(p) => `${p.$widths[3] * 100}%`};
    `}

    ${media.desktopLarge`
        width: ${(p) => `${p.$widths[4] * 100}%`};
    `}
`

// Default to full width for all screen sizes
const widths = [
    1, // mobile
    1, // mobileLandscape
    1, // tablet
    1, // desktop
    1, // desktopLarge
]

// Define for each possible size prop, what position(s) in `widths` array should be changed
const inserts = new Map([
    ['mobile', [0, 1]],
    ['mobileUp', [0, 5]],
    ['mobileLandscape', [1, 1]],
    ['mobileLandscapeUp', [1, 4]],
    ['tablet', [2, 1]],
    ['tabletUp', [2, 3]],
    ['desktop', [3, 1]],
    ['desktopUp', [3, 2]],
    ['desktopLarge', [4, 1]],
])

/**
 * Usage:
 * Column will be half screen width on small screens and quarter width for medium screens and larger
 * <Col mobile={1 / 2} tabletUp={1 / 4}>content</Col>
 */

const Col = ({
    children,
    mobile = undefined,
    mobileUp = undefined,
    mobileLandscape = undefined,
    mobileLandscapeUp = undefined,
    tablet = undefined,
    tabletUp = undefined,
    desktop = undefined,
    desktopUp = undefined,
    desktopLarge = undefined,
    ...rest
}) => {
    const myWidths = widths.slice()

    const sizes = {
        mobile,
        mobileUp,
        mobileLandscape,
        mobileLandscapeUp,
        tablet,
        tabletUp,
        desktop,
        desktopUp,
        desktopLarge,
    }

    inserts.forEach((insert, prop) => {
        if (sizes[prop]) {
            const [at, times] = insert
            // Create new array `values` with passed size (eg. 1 / 2)
            const values = Array(times).fill(sizes[prop])
            // Replace the default width values with the passed value
            myWidths.splice(at, times, ...values)
        }
    })

    return (
        <Wrapper $widths={myWidths} {...rest}>
            {children}
        </Wrapper>
    )
}

Col.propTypes = {
    children: PropTypes.node,
    mobile: PropTypes.number,
    mobileUp: PropTypes.number,
    mobileLandscape: PropTypes.number,
    mobileLandscapeUp: PropTypes.number,
    tablet: PropTypes.number,
    tabletUp: PropTypes.number,
    desktop: PropTypes.number,
    desktopUp: PropTypes.number,
    desktopLarge: PropTypes.number,
}

export default Col
