import React from 'react'

import {
    AccessIdIcon,
    useAccessIdTitle,
} from 'js/components/AccessIcon/AccessIdIcon'
import HideVisually from 'js/components/HideVisually/HideVisually'
import Tooltip from 'js/components/Tooltip/Tooltip'

const DisplayAccessLevel = ({ accessId, ...rest }) => {
    const title = useAccessIdTitle(accessId)

    return (
        <Tooltip content={title} offset={0} placement="left">
            <div {...rest}>
                <AccessIdIcon aria-hidden accessId={accessId} />
                <HideVisually>{title}</HideVisually>
            </div>
        </Tooltip>
    )
}

export default DisplayAccessLevel
