import React from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

import TableSortableHeader from 'js/components/TableSortableHeader/TableSortableHeader'

const Wrapper = styled.div`
    text-align: left;

    ${(p) =>
        p.$layout !== 'fixed' &&
        css`
            overflow-x: auto;
        `};

    table {
        table-layout: ${(p) => p.$layout};
        width: 100%;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};

        ${(p) =>
            p.$wrap === 'word' &&
            css`
                word-break: break-word;
            `};
        ${(p) =>
            p.$wrap === 'normal' &&
            css`
                word-break: normal;
            `};
    }

    th {
        font-weight: ${(p) => p.theme.font.weight.normal};
        color: ${(p) => p.theme.color.text.grey};
        text-align: left;
        white-space: nowrap;

        ${(p) =>
            p.$rowHeight === 'small' &&
            css`
                height: 34px;
            `};
        ${(p) =>
            p.$rowHeight === 'normal' &&
            css`
                height: 40px;
            `};

        &:not(:last-child) {
            padding-right: 16px;
        }
    }

    tbody tr {
        body:not(.mouse-user) &.focused {
            outline: ${(p) => p.theme.focusStyling};
        }

        &:not(:first-child) {
            td {
                border-top: 1px solid ${(p) => p.theme.color.grey[20]};

                ${(p) =>
                    p.$rowHeight === 'small' &&
                    css`
                        height: 41px;
                    `};
                ${(p) =>
                    p.$rowHeight === 'normal' &&
                    css`
                        height: 49px;
                    `};

                &.no-border {
                    border-top: none;
                }
            }
        }
    }

    td {
        ${(p) =>
            p.$wrap === 'none' &&
            css`
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
            `};

        ${(p) =>
            p.$rowHeight === 'small' &&
            css`
                height: 42px;
            `};
        ${(p) =>
            p.$rowHeight === 'normal' &&
            css`
                height: 48px;
            `};

        &:not(:last-child) {
            padding-right: 16px;
        }

        .TableLink {
            color: ${(p) => p.theme.color.primary.main};

            &:hover {
                text-decoration: underline;
            }
        }

        .TableLinkBlock {
            /* Grow height to enlarge clickable area  */
            min-height: 100%; // height doesn't work properly
            display: flex;
            align-items: center;
            color: ${(p) => p.theme.color.primary.main};

            &:hover {
                text-decoration: underline;
            }
        }
    }

    ${(p) =>
        p.$edgePadding &&
        css`
            td,
            th {
                &:first-child {
                    padding-left: 20px;
                }
                &:last-child {
                    padding-right: 20px;
                }
            }
        `};
`

const Table = ({
    name,
    layout = 'auto',
    rowHeight = 'normal',
    edgePadding,
    wrap = 'normal',
    headers = [],
    orderBy,
    orderDirection,
    setOrderBy,
    setOrderDirection,
    tableWidth,
    children,
    ...rest
}) => {
    return (
        <Wrapper
            $layout={layout}
            $rowHeight={rowHeight}
            $edgePadding={edgePadding}
            $wrap={wrap}
            {...rest}
        >
            {/* Set explicit table width for iOS, that for some reason choses to ignores the colgroup widths */}
            <table
                style={
                    tableWidth ? { width: tableWidth, minWidth: '100%' } : null
                }
            >
                {headers?.length > 0 && (
                    <thead>
                        <tr>
                            {headers.map((props, i) => (
                                <TableSortableHeader
                                    key={`${name}-head-${i}`}
                                    {...props}
                                    orderBy={orderBy}
                                    orderDirection={orderDirection}
                                    setOrderBy={setOrderBy}
                                    setOrderDirection={setOrderDirection}
                                />
                            ))}
                        </tr>
                    </thead>
                )}

                {children}
            </table>
        </Wrapper>
    )
}

Table.propTypes = {
    layout: PropTypes.string,
    rowHeight: PropTypes.oneOf(['small', 'normal']),
    wrap: PropTypes.oneOf(['none', 'normal', 'word']),
    edgePadding: PropTypes.bool,
    headers: PropTypes.arrayOf(
        PropTypes.shape({
            name: PropTypes.string,
            label: PropTypes.string,
            defaultOrderDirection: PropTypes.string,
            align: PropTypes.string,
            options: PropTypes.arrayOf(
                PropTypes.shape({
                    name: PropTypes.string.isRequired,
                    label: PropTypes.string.isRequired,
                    defaultOrderDirection: PropTypes.string,
                }),
            ),
        }),
    ),
    orderBy: PropTypes.string,
    orderDirection: PropTypes.string,
    setOrderBy: PropTypes.func,
    setOrderDirection: PropTypes.func,
}

export default Table
