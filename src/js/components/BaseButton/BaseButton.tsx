import styled from 'styled-components'

const BaseButton = styled.button.attrs((props) => ({
    type: 'button',
    ...props,
}))`
    flex-shrink: 0;
    display: flex;
    position: relative;
    user-select: none;
    line-height: 1;

    /* Needed to lay transparent :hover/:active background on top of original background */
    &:before {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        margin: auto;
    }

    &[disabled] {
        pointer-events: none;
    }
`

export default BaseButton
