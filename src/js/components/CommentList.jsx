import React from 'react'

import AddComment from 'js/components/AddComment/AddComment'

import Comment from './Comment/Comment'

const CommentList = ({ viewer, entity, canUpvote, refetchQueries }) => {
    return (
        <section>
            {entity.statusPublished === 'published' && (
                <AddComment
                    viewer={viewer}
                    entity={entity}
                    refetchQueries={refetchQueries}
                    style={{ marginTop: '16px' }}
                />
            )}
            <ol style={{ marginTop: '20px' }}>
                {entity.comments.map((comment) => (
                    <Comment
                        key={comment.guid}
                        viewer={viewer}
                        entity={comment}
                        group={entity.group}
                        hideReply={
                            entity.isClosed ||
                            (!!entity.group &&
                                entity.group.membership !== 'joined')
                        }
                        isClosed={entity.isClosed}
                        canUpvote={canUpvote}
                        canChooseBestAnswer={entity.canChooseBestAnswer}
                        refetchQueries={refetchQueries}
                        highlightBestAnswer={comment.isBestAnswer}
                    />
                ))}
            </ol>
        </section>
    )
}

export default CommentList
