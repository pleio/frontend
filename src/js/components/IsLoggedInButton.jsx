import { cloneElement } from 'react'
import { Link, useLocation } from 'react-router-dom'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

const IsLoggedInButton = ({ data, children }) => {
    const location = useLocation()

    if (!data || !data.viewer) return null
    if (data.viewer.loggedIn) {
        return children
    } else {
        return cloneElement(children, {
            as: Link,
            to: '/login',
            state: {
                next: location.pathname,
            },
        })
    }
}

const Query = gql`
    query IsLoggedIn {
        viewer {
            guid
            loggedIn
        }
    }
`

export default graphql(Query)(IsLoggedInButton)
