import React, { forwardRef } from 'react'
import styled from 'styled-components'

import IconButton from 'js/components/IconButton/IconButton'

const Wrapper = styled(IconButton)`
    background-color: transparent;

    &[disabled] {
        color: ${(p) => p.theme.color.grey[30]};
    }

    .IconButtonContent {
        flex-direction: row; // Fixes DropdownButton arrow
    }
`

const ToolButton = forwardRef(({ children, ...rest }, ref) => (
    <Wrapper
        ref={ref}
        size="large"
        variant="tertiary"
        radiusStyle="none"
        tabIndex="-1"
        {...rest}
    >
        {children}
    </Wrapper>
))

ToolButton.displayName = 'ToolButton'

export default ToolButton
