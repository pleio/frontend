import React, { forwardRef } from 'react'
import { useTranslation } from 'react-i18next'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import Flexer from 'js/components/Flexer/Flexer'
import { H4 } from 'js/components/Heading'
import HideVisually from 'js/components/HideVisually/HideVisually'
import IconButton from 'js/components/IconButton/IconButton'
import Modal from 'js/components/Modal/Modal'

import KeyboardIcon from 'icons/keyboard.svg'

const ModalContentWrapper = styled.ul`
    .KeyboardShortcutsSection {
        margin-bottom: 16px;

        code {
            font-size: 0.8rem;
        }
    }
    .KeyboardShortcutsSection:last-child {
        margin-bottom: 0;
    }
    .KeyboardShortcutsSectionTitle {
        margin-bottom: 8px;
    }
    .KeyboardShortcutsRow {
        padding: 8px 0;
        border-bottom: 1px solid ${(p) => p.theme.color.grey[30]};
    }
    .KeyboardShortcutsRow:last-child {
        border-bottom: none;
    }
`

const KeyboardShortcutsRow = (props) => {
    const { shortcut } = props
    const { format, title, keys = [] } = shortcut

    return (
        <li className="KeyboardShortcutsRow">
            <Flexer justifyContent="space-between">
                <span>
                    {typeof format === 'function' ? format(title) : title}
                </span>
                <div>
                    {keys.map((key, i) => (
                        <React.Fragment key={key.key || key}>
                            <kbd aria-hidden={!!key.label}>
                                {key.key || key}
                            </kbd>
                            {key.label && (
                                <HideVisually> {key.label}</HideVisually>
                            )}
                            {i < keys.length - 1 && <span> + </span>}
                        </React.Fragment>
                    ))}
                </div>
            </Flexer>
        </li>
    )
}

const KeyboardShortcutsButton = forwardRef((props, ref) => {
    const {
        isVisible,
        onBlur,
        onFocus,
        modalIsVisible,
        setModalIsVisible,
        options,
        toolbarIsVisible,
    } = props

    const { t } = useTranslation()

    const showFormattingShortcuts =
        options.textFormat ||
        options.textStyle ||
        options.textList ||
        options.textQuote

    let platformIsMac = false

    if (navigator.userAgentData) {
        platformIsMac = navigator.userAgentData.platform === 'macOS'
    } else if (navigator.platform) {
        platformIsMac = navigator.platform.toUpperCase().indexOf('MAC') >= 0
    }

    const commandKey = platformIsMac
        ? {
              label: 'Command',
              key: '⌘',
          }
        : {
              label: 'Control',
              key: 'Ctrl',
          }
    const optionKey = platformIsMac
        ? {
              label: 'Option',
              key: 'Opt',
          }
        : {
              label: 'Alt',
              key: 'Alt',
          }

    const forwardSlashKey = {
        label: t('editor.forward-slash-key'),
        key: '/',
    }
    const atKey = {
        label: 'At',
        key: '@',
    }
    const getHashtagKey = (repetitions = 1) => ({
        label: Array(repetitions).fill(t('editor.hashtag-key')).join(' '),
        key: Array(repetitions).fill('#').join(''),
    })
    const asteriskKey = {
        label: t('editor.asterisk-key'),
        key: '*',
    }
    const greaterThanKey = {
        label: t('editor.greater-than-key'),
        key: '>',
    }

    const shortcuts = [
        {
            title: t('keyboard-shortcuts.editor'),
            shortcuts: [
                {
                    title: t('keyboard-shortcuts.keyboard-shortcuts'),
                    keys: [commandKey, forwardSlashKey],
                },
                ...(toolbarIsVisible
                    ? [
                          {
                              title: t('keyboard-shortcuts.focus-toolbar'),
                              keys: [optionKey, 'F10'],
                          },
                      ]
                    : []),
                {
                    title: t('keyboard-shortcuts.undo'),
                    keys: [commandKey, 'z'],
                },
                {
                    title: t('keyboard-shortcuts.redo'),
                    keys: [commandKey, 'Shift', 'z'],
                },
                ...(options.insertMention
                    ? [
                          {
                              title: t('keyboard-shortcuts.mention'),
                              keys: [atKey],
                          },
                      ]
                    : []),
            ],
        },
        ...(showFormattingShortcuts
            ? [
                  {
                      title: t('keyboard-shortcuts.formatting'),
                      shortcuts: [
                          ...(options.textStyle
                              ? [
                                    {
                                        title: t('editor.bold'),
                                        format: (title) => <b>{title}</b>,
                                        keys: [commandKey, 'b'],
                                    },
                                    {
                                        title: t('editor.italic'),
                                        format: (title) => <i>{title}</i>,
                                        keys: [commandKey, 'i'],
                                    },
                                    {
                                        title: t('editor.underline'),
                                        format: (title) => <u>{title}</u>,
                                        keys: [commandKey, 'u'],
                                    },
                                ]
                              : []),
                          ...(options.textFormat
                              ? [
                                    {
                                        title: t('editor.text-format-h2'),
                                        keys: [getHashtagKey(1), 'Space'],
                                    },
                                    {
                                        title: t('editor.text-format-h3'),
                                        keys: [getHashtagKey(2), 'Space'],
                                    },
                                    {
                                        title: t('editor.text-format-h4'),
                                        keys: [getHashtagKey(3), 'Space'],
                                    },
                                    {
                                        title: t('editor.text-format-h5'),
                                        keys: [getHashtagKey(4), 'Space'],
                                    },
                                ]
                              : []),
                          ...(options.textList
                              ? [
                                    {
                                        title: t('editor.ol'),
                                        keys: ['1.', 'Space'],
                                    },
                                    {
                                        title: t('editor.ul'),
                                        keys: [asteriskKey, 'Space'],
                                    },
                                ]
                              : []),
                          ...(options.textQuote
                              ? [
                                    {
                                        title: t('editor.blockquote'),
                                        keys: [greaterThanKey, 'Space'],
                                    },
                                ]
                              : []),
                          {
                              title: t('editor.text-format-inline-code'),
                              format: (title) => <code>{title}</code>,
                              keys: ['`'],
                          },
                          {
                              title: t('editor.text-format-code-block'),
                              format: (title) => <code>{title}</code>,
                              keys: ['```', 'Enter'],
                          },
                      ],
                  },
              ]
            : []),
    ]

    return (
        <>
            <IconButton
                ref={ref}
                size="normal"
                variant="tertiary"
                tooltip={t('keyboard-shortcuts.keyboard-shortcuts')}
                aria-label={t('keyboard-shortcuts.open')}
                onBlur={onBlur}
                onFocus={onFocus}
                onClick={() => setModalIsVisible(true)}
                style={{
                    position: 'absolute',
                    bottom: '6px',
                    right: '10px',
                    ...(isVisible
                        ? { opacity: 1, pointerEvents: 'auto' }
                        : { opacity: 0, pointerEvents: 'none' }),
                }}
            >
                <KeyboardIcon />
            </IconButton>
            <Modal
                size="small"
                isVisible={modalIsVisible}
                title={t('keyboard-shortcuts.keyboard-shortcuts')}
                onClose={() => setModalIsVisible(false)}
                showCloseButton={true}
            >
                <ModalContentWrapper>
                    {shortcuts.map((section) => (
                        <li
                            key={section.title}
                            className="KeyboardShortcutsSection"
                        >
                            <H4
                                as="h3"
                                className="KeyboardShortcutsSectionTitle"
                            >
                                {section.title}
                            </H4>
                            <ul>
                                {section.shortcuts.map((shortcut) => (
                                    <KeyboardShortcutsRow
                                        key={shortcut.title}
                                        shortcut={shortcut}
                                    />
                                ))}
                            </ul>
                        </li>
                    ))}
                </ModalContentWrapper>
            </Modal>
        </>
    )
})

KeyboardShortcutsButton.displayName = 'KeyboardShortcutsButton'

KeyboardShortcutsButton.propTypes = {
    isVisible: PropTypes.bool,
    modalIsVisible: PropTypes.bool,
    setModalIsVisible: PropTypes.func,
    onBlur: PropTypes.func,
    onFocus: PropTypes.func,
    toolbarIsVisible: PropTypes.bool,
    options: PropTypes.shape({
        textFormat: PropTypes.bool,
        textStyle: PropTypes.bool,
        textQuote: PropTypes.bool,
        textList: PropTypes.bool,
        insertMention: PropTypes.bool,
    }),
}

export default KeyboardShortcutsButton
