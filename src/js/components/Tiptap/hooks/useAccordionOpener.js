import { useContext, useEffect } from 'react'
import { useLocation } from 'react-router-dom'

import SearchContext from 'js/search/providers/SearchContext'

// If a certain key word is found, open the accordion
const useAccordionOpener = (rootNode, hasEditor) => {
    const { searchQuery } = useContext(SearchContext)

    const { hash } = useLocation()

    // A search query can contain one or more words
    // Match multiple words within quotes as 1 search term
    const searchQueries = searchQuery?.match(/(?:[^\s"]+|"[^"]*")+/g) || []

    // Remove single and double quotes
    const searchTerms = searchQueries?.map((term) => term.replace(/["']/g, ''))

    useEffect(() => {
        if (!searchQuery && !hash) return

        if (rootNode && hasEditor) {
            // Apparently when the Tiptap editor is created, we cannot immediately access the DOM. So we have to reset the event queue with setTimeout.
            setTimeout(() => {
                const detailsList = document.querySelectorAll('details')

                detailsList.forEach((detailsEl) => {
                    const iterator = document.createNodeIterator(
                        detailsEl,
                        NodeFilter.SHOW_TEXT,
                    )

                    let textnode
                    while ((textnode = iterator.nextNode())) {
                        const text = textnode.textContent.toLowerCase()
                        const found = searchTerms.some((term) =>
                            text.includes(term),
                        )

                        if (found && !detailsEl.hasAttribute('open')) {
                            detailsEl.setAttribute('open', '')
                            break
                        }
                    }

                    // If hash is found in closed accordion, open and scroll to it.
                    if (hash) {
                        const hashElement = detailsEl.querySelector(hash)
                        if (
                            hashElement !== null &&
                            !detailsEl.hasAttribute('open')
                        ) {
                            detailsEl.setAttribute('open', '')
                        }
                    }
                })
            }, 0)
        }
    }, [hasEditor, rootNode, searchQuery, searchTerms, hash])
}

export default useAccordionOpener
