import React, {
    createContext,
    forwardRef,
    useContext,
    useImperativeHandle,
    useRef,
    useState,
} from 'react'
import { useTranslation } from 'react-i18next'
import CollaborationCursor from '@tiptap/extension-collaboration-cursor'
import History from '@tiptap/extension-history'
import { EditorContent, useEditor } from '@tiptap/react'
import { hasJsonStructure } from 'helpers'
import { getReadableColor } from 'helpers/getContrast'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

import FieldLabel from 'js/components/FieldLabel'
import HideVisually from 'js/components/HideVisually/HideVisually'
import TextElement from 'js/components/Text/Text'
import globalStateContext from 'js/lib/withGlobalState/globalStateContext'

import WarnIcon from 'icons/warn.svg'

import BubbleMenu from './BubbleMenu/BubbleMenu'
import { CollaborationExtension as Collaboration } from './CollaborationExtension'
import KeyboardShortcutsButton from './components/KeyboardShortcutsButton'
import getExtensions from './getExtensions'
import Toolbar from './Toolbar/Toolbar'

export const TiptapEditorContext = createContext(null)

const getMinHeight = (height, lineHeight) => {
    const minHeight = {
        small: 'auto',
        normal: `calc(${lineHeight} * 1 + 16px + 16px)`, // 1 lines + vertical padding
        large: `calc(${lineHeight} * 5 + 16px + 16px)`, // 5 lines + vertical padding
    }
    return minHeight[height]
}

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    background-color: ${(p) => p.theme.editor.bg};
    border: ${(p) =>
        p.$borderStyle === 'rounded' && `1px solid ${p.theme.color.grey[40]}`};
    border-radius: ${(p) =>
        p.$borderStyle === 'rounded' && `${p.theme.radius.small}`};
    border-color: ${(p) => p.$hasError && p.theme.color.warn.main};
    position: relative; // Used to position the keyboard shortcuts button

    .TiptapEditorToolbar {
        background-color: ${(p) => p.theme.editor.bg};
        border-bottom: 1px solid ${(p) => p.theme.color.grey[30]};

        &[data-sticky='true'] {
            box-shadow: ${(p) => p.theme.shadow.soft.bottom};
            z-index: 9;
        }

        &:not([data-sticky='true']) {
            border-top-left-radius: ${(p) => p.theme.radius.small};
            border-top-right-radius: ${(p) => p.theme.radius.small};
        }
    }

    .TiptapEditorTools {
        display: flex;
        flex-wrap: wrap;
        max-width: ${(p) => p.theme.containerWidth.normal};
        margin-left: auto;
        margin-right: auto;
    }

    .TiptapEditorContent {
        flex-grow: 1;
    }

    .TiptapEditorToolGroup {
        flex-shrink: 0;
        position: relative;
        margin-bottom: -1px;
        display: flex;
        align-items: center;
        flex-wrap: wrap;
        border-bottom: 1px solid ${(p) => p.theme.color.grey[30]};

        &:not(:last-child) {
            padding-right: 1px;

            &:after {
                content: '';
                position: absolute;
                top: 8px;
                bottom: 8px;
                right: 0;
                width: 1px;
                background-color: ${(p) => p.theme.color.grey[30]};
                pointer-events: none;
            }
        }
    }

    .is-editor-empty:first-child:before {
        content: attr(data-placeholder);
        float: left;
        color: ${(p) => p.theme.color.text.grey};
        pointer-events: none;
        height: 0;
    }

    .ProseMirror-hideselection *::selection {
        background: transparent;
    }
    .ProseMirror-hideselection *::-moz-selection {
        background: transparent;
    }
    .ProseMirror-hideselection * {
        caret-color: transparent;
    }

    .ProseMirror-gapcursor {
        display: none;
        position: absolute;
        pointer-events: none;
        margin-top: 0 !important;
    }

    .ProseMirror-gapcursor:after {
        content: '';
        display: block;
        position: absolute;
        top: 6px;
        height: 1px;
        width: 20px;
        background: black;
        animation: ProseMirror-cursor-blink 1.1s steps(2, start) infinite;
    }

    @keyframes ProseMirror-cursor-blink {
        to {
            visibility: hidden;
        }
    }

    .ProseMirror-focused .ProseMirror-gapcursor {
        display: block;

        + .focus {
            box-shadow: none;
        }
    }

    .TiptapEditorContentWrapper {
        flex-grow: 1;
        display: flex;
        flex-direction: column;
        position: relative; // Used to position messages like "Share update" in the editor's textarea
        transition: padding-top ${(p) => p.theme.transition.materialFast};

        ${(p) =>
            p.$labelIsActive &&
            css`
                padding-top: ${(p) => p.theme.font.lineHeight[p.$textSize]};
            `};
    }

    [lang] {
        border-bottom: 1px dashed ${(p) => p.theme.color.text.grey};
    }

    .mark-cssclass {
        ${(p) =>
            p.$allowTextCssClasses &&
            css`
                border-bottom: 1px dashed ${(p) => p.theme.color.text.grey};
            `};
    }

    .TiptapEditorLabel {
        top: ${(p) => p.theme.padding.vertical.small};
        height: ${(p) => p.theme.font.lineHeight[p.$textSize]};
        padding-left: 18px;

        ${(p) =>
            p.$labelIsActive &&
            css`
                transform: translateY(-2px);
            `};
    }

    .ProseMirror {
        width: 100%;
        height: 100%;
        min-height: ${(p) =>
            getMinHeight(p.$height, p.theme.font.lineHeight[p.$textSize])};
        margin-left: auto;
        margin-right: auto;
        padding: ${(p) =>
            `${p.theme.padding.vertical.small} ${p.theme.padding.horizontal.small} 38px ${p.theme.padding.horizontal.small}`};

        ${(p) =>
            p.$showPaddingBottom &&
            css`
                padding-bottom: 42px;
            `};

        ${(p) =>
            p.$textSize === 'small' &&
            css`
                font-size: ${(p) => p.theme.font.size.small};
                line-height: ${(p) => p.theme.font.lineHeight.small};
            `};
        ${(p) =>
            p.$textSize === 'normal' &&
            css`
                font-size: ${(p) => p.theme.font.size.normal};
                line-height: ${(p) => p.theme.font.lineHeight.normal};
            `};
        ${(p) =>
            p.$textSize === 'large' &&
            css`
                font-size: ${(p) => p.theme.font.size.large};
                line-height: ${(p) => p.theme.font.lineHeight.large};
            `};

        a {
            cursor: text;
        }

        .tiptap-details-summary-input {
            width: 100%;
            height: 40px;
            padding: 0;
            border: none;
            background: none;
            font-weight: ${(p) => p.theme.font.weight.bold};
        }

        .selectedCell:after {
            content: '';
            left: -2px;
            right: -2px;
            top: -2px;
            bottom: -2px;
            border: 2px solid black;
            pointer-events: none;
            position: absolute;
            z-index: 2;
        }

        .column-resize-handle {
            position: absolute;
            top: 0;
            bottom: 0;
            right: -3px;
            width: 4px;
            background-color: black;
            pointer-events: none;
        }

        &.ProseMirror-hideselection
            .node-mention.focus
            [data-node-view-wrapper]:before {
            border-radius: 1px;
            border: 2px solid black;
        }

        // Only in edit mode we show the hash-icon before the anchor link (beheer#13967)
        .mark-anchor > a::before {
            content: '#';
            color: ${(p) => p.theme.color.text.grey};
            margin-right: 1px;
        }
    }

    .resize-cursor {
        cursor: ew-resize;
        cursor: col-resize;
    }

    ${(p) =>
        p.$isCollab &&
        css`
            .collaboration-cursor__caret {
                position: absolute;
                border-bottom-color: transparent;
                border-color: transparent !important;
                pointer-events: none;
                user-select: none;
                z-index: 10; // on top of toolbar

                ${(p) =>
                    p.$textSize === 'small' &&
                    css`
                        border-bottom: ${(p) => p.theme.font.lineHeight.small}
                            solid transparent;
                    `};
                ${(p) =>
                    p.$textSize === 'normal' &&
                    css`
                        border-bottom: ${(p) => p.theme.font.lineHeight.normal}
                            solid transparent;
                    `};
                ${(p) =>
                    p.$textSize === 'large' &&
                    css`
                        border-bottom: ${(p) => p.theme.font.lineHeight.large}
                            solid transparent;
                    `};
            }

            .collaboration-cursor__label {
                position: relative;
                margin-left: -1px;
                background-color: ${(p) =>
                    p.theme.color.secondary.main} !important;
                color: ${(p) => getReadableColor(p.theme.color.secondary.main)};
                font-size: ${(p) => p.theme.font.size.tiny};
                line-height: ${(p) => p.theme.font.lineHeight.tiny};
                font-weight: ${(p) => p.theme.font.weight.semibold};
                padding: 0 3px 2px;
                border-radius: 2px 2px 2px 0;
                transform: translateY(-100%);

                &:before {
                    content: '';
                    display: block;
                    width: 2px;
                    position: absolute;
                    left: 0;
                    background-color: inherit;

                    ${(p) =>
                        p.$textSize === 'small' &&
                        css`
                            height: ${(p) => p.theme.font.lineHeight.small};
                            bottom: -${(p) => p.theme.font.lineHeight.small};
                        `};
                    ${(p) =>
                        p.$textSize === 'normal' &&
                        css`
                            height: ${(p) => p.theme.font.lineHeight.normal};
                            bottom: -${(p) => p.theme.font.lineHeight.normal};
                        `};
                    ${(p) =>
                        p.$textSize === 'large' &&
                        css`
                            height: ${(p) => p.theme.font.lineHeight.large};
                            bottom: -${(p) => p.theme.font.lineHeight.large};
                        `};
                }
            }
        `};
`

const StyledTextElement = styled(TextElement)`
    &.abstract-helper {
        svg {
            display: none !important;
        }
        &.Chars__Remaining--Warning {
            color: ${(p) => p.theme.color.warn.main};

            svg {
                display: inline-block !important;
            }
        }
    }
`

const TiptapEditor = forwardRef(
    (
        {
            autofocus = false,
            borderStyle = 'rounded',
            className = '',
            collab,
            content,
            contentType = 'json',
            disableNewLine = false,
            disableNewParagraph = false,
            getEditorInstance,
            group,
            hasError = false,
            height = 'normal',
            'aria-label': ariaLabel = '',
            label = '',
            placeholder = '',
            maxChars,
            name,
            onBlur,
            onChange,
            options = {
                textAnchor: false,
                textCSSClass: false,
                textLanguage: false,
                textFormat: false,
                textStyle: false,
                textLink: false,
                textQuote: false,
                textList: false,
                insertMedia: false,
                disableAlignImage: false,
                insertAccordion: false,
                insertButton: false,
                insertTable: false,
                insertMention: false,
            },
            required,
            showPaddingBottom,
            hideKeyboardShortcuts = false,
            textSize = 'normal',
            delayMentionSuggestions,
            ...rest
        },
        ref,
    ) => {
        const {
            textFormat,
            textStyle,
            textLink,
            textAnchor,
            textCSSClass,
            textLanguage,
            textQuote,
            textList,
            insertAccordion,
            insertButton,
            insertMedia,
            disableAlignImage,
            insertTable,
            insertMention,
        } = options

        const refWrapper = useRef()
        const refToolbar = useRef()

        const { globalState } = useContext(globalStateContext)
        const { allowTextCssClasses } = globalState

        const editor = useEditor({
            editable: true,
            editorProps: {
                attributes: {
                    'aria-label': ariaLabel,
                    'aria-describedby': `${name}-describe`,
                    'aria-multiline': true,
                    'aria-required': !!required,
                    role: 'textbox',
                },
            },
            extensions: [
                ...getExtensions({
                    placeholder,
                    maxChars,
                    textStyle,
                    textLink,
                    textAnchor,
                    textCSSClass,
                    textLanguage,
                    textQuote,
                    textList,
                    insertButton,
                    insertMedia,
                    insertAccordion,
                    insertTable,
                    insertMention,
                    disableNewLine,
                    disableNewParagraph,
                    delayMentionSuggestions,
                    group,
                }),
                ...(collab?.provider && collab?.user
                    ? [
                          Collaboration.configure({
                              document: collab.provider.document,
                              field: 'default',
                              user: {
                                  guid: collab.user.guid,
                                  name: collab.user.name,
                              },
                          }),
                          CollaborationCursor.configure({
                              provider: collab.provider,
                              user: {
                                  guid: collab.user.guid,
                                  name: collab.user.name,
                              },
                          }),
                      ]
                    : [History]),
            ],
            autofocus: autofocus ? 'end' : false,
            injectCSS: false,
            content: collab?.provider
                ? undefined
                : contentType === 'json'
                  ? hasJsonStructure(content)
                      ? JSON.parse(content)
                      : content
                  : content,
            onUpdate({ editor }) {
                if (onChange) {
                    onChange(getValue(editor))
                }
            },
            onFocus({ editor }) {
                handleEditorKeyDownWrapper.current = (evt) =>
                    handleEditorKeyDown(evt, editor)

                window.addEventListener(
                    'keydown',
                    handleEditorKeyDownWrapper.current,
                )
            },
            onBlur({ editor, event }) {
                window.removeEventListener(
                    'keydown',
                    handleEditorKeyDownWrapper.current,
                )

                if (onBlur) {
                    onBlur(event, getValue(editor))
                }
            },
            onCreate({ editor }) {
                getEditorInstance && getEditorInstance(editor)
            },
        })

        const keyboardShortcutsButtonRef = useRef(null)
        const [
            keyboardShortcutsButtonIsFocused,
            setKeyboardShortcutsButtonIsFocused,
        ] = useState(false)
        const [
            keyboardShortcutsModalIsVisible,
            setKeyboardShortcutsModalIsVisible,
        ] = useState(false)

        const showKeyboardShortcutsButton =
            keyboardShortcutsModalIsVisible ||
            editor?.isFocused ||
            keyboardShortcutsButtonIsFocused

        // Store the event listener reference in a ref
        // This way we can remove the event listener when the editor is destroyed
        // Without the ref, the event listener would not be removed correctly
        const handleEditorKeyDownWrapper = useRef()

        const handleEditorKeyDown = (evt, editor) => {
            if (evt.altKey && evt.key === 'F10') {
                const toolbar = refToolbar?.current
                if (toolbar) {
                    evt.preventDefault()
                    toolbar.setAttribute('tabindex', 0)
                    toolbar.focus()
                }
            }

            if (evt.key === 'Escape') {
                const { doc, selection } = editor.view.state
                const { from, to } = selection
                const hasTextSelection = doc.textBetween(from, to).length > 0
                if (hasTextSelection) {
                    editor.commands.setTextSelection({ from: to, to })
                }
            }

            if ((evt.metaKey || evt.ctrlKey) && evt.key === '/') {
                evt.preventDefault()
                keyboardShortcutsButtonRef.current?.click()
            }
        }

        const getValue = (editor) => {
            if (!editor) {
                console.error('Please pass editor object')
                return null
            }
            if (contentType === 'html') {
                return editor.isEmpty ? '' : editor.getHTML()
            } else if (contentType === 'json') {
                return editor.isEmpty ? '' : JSON.stringify(editor.getJSON())
            } else {
                console.error('Content type does not exist')
            }
        }

        useImperativeHandle(ref, () => ({
            getValue() {
                return getValue(editor)
            },
            clearContent() {
                editor.commands.setContent(null)
            },
        }))

        const { t } = useTranslation()

        const getRemainingCharacterWarning = (maxChars, numberOfCharacters) => {
            const limit = 25
            if (
                maxChars - numberOfCharacters > 0 &&
                maxChars - numberOfCharacters <= limit
            ) {
                return {
                    message: t('editor.input-max-length-notify'),
                }
            } else if (maxChars === numberOfCharacters) {
                return {
                    message: t('editor.input-max-length-reached'),
                    className: 'Chars__Remaining--Warning',
                }
            }
            return {}
        }

        const toolbarPlugins = {
            textFormat,
            textQuote,
            textList,
            insertButton,
            insertMedia,
            insertAccordion,
            insertTable,
        }

        const toolbarIsVisible =
            textFormat ||
            textQuote ||
            textList ||
            insertButton ||
            insertMedia ||
            insertAccordion ||
            insertTable

        const bubbleMenuPlugins = {
            textStyle,
            textLink,
            textAnchor,
            textCSSClass: allowTextCssClasses && textCSSClass,
            textLanguage,
        }

        const labelIsActive = label && editor?.isEmpty === false

        return (
            <TiptapEditorContext.Provider
                value={{
                    groupGuid: group?.guid,
                    disableAlignImage,
                }}
            >
                <Wrapper
                    ref={refWrapper}
                    $borderStyle={borderStyle}
                    $height={height}
                    $showPaddingBottom={showPaddingBottom}
                    $textSize={textSize}
                    $hasError={hasError}
                    $isCollab={!!collab?.provider}
                    $labelIsActive={labelIsActive}
                    $allowTextCssClasses={allowTextCssClasses}
                    className={className}
                    {...rest}
                >
                    <Toolbar
                        ref={refToolbar}
                        editor={editor}
                        containerElement={refWrapper}
                        contentGuid={collab?.guid}
                        isVisible={toolbarIsVisible}
                        {...toolbarPlugins}
                    />
                    <BubbleMenu editor={editor} {...bubbleMenuPlugins} />
                    <div className="TiptapEditorContentWrapper">
                        <FieldLabel
                            size={textSize}
                            placeholder={placeholder}
                            label={label}
                            active={labelIsActive}
                            hasError={hasError}
                            className="TiptapEditorLabel"
                        />
                        <EditorContent
                            editor={editor}
                            className={`TiptapEditorContent`}
                        />
                    </div>
                    {/* Keep KeyboardShorcutsButton outside .TiptapEditorContentWrapper, otherwise it interferes with BubbleMenu's keyboard accessibility */}
                    {!hideKeyboardShortcuts && (
                        <KeyboardShortcutsButton
                            ref={keyboardShortcutsButtonRef}
                            isVisible={showKeyboardShortcutsButton}
                            onBlur={() =>
                                setKeyboardShortcutsButtonIsFocused(false)
                            }
                            onFocus={() =>
                                setKeyboardShortcutsButtonIsFocused(true)
                            }
                            modalIsVisible={keyboardShortcutsModalIsVisible}
                            setModalIsVisible={
                                setKeyboardShortcutsModalIsVisible
                            }
                            options={options}
                            toolbarIsVisible={toolbarIsVisible}
                        />
                    )}
                </Wrapper>
                <HideVisually id={`${name}-describe`} aria-hidden>
                    {t('editor.focus-toolbar-helper')}
                </HideVisually>
                {maxChars && (
                    <>
                        <HideVisually role="alert" aria-live="assertive">
                            {
                                getRemainingCharacterWarning(
                                    maxChars,
                                    editor?.view.state.doc.textContent?.length,
                                ).message
                            }
                        </HideVisually>
                        <StyledTextElement
                            id={`${name}-helper`}
                            size="small"
                            variant="grey"
                            className={`abstract-helper ${
                                getRemainingCharacterWarning(
                                    maxChars,
                                    editor?.view.state.doc.textContent?.length,
                                ).className
                            }`}
                            style={{ marginTop: '8px' }}
                        >
                            <WarnIcon
                                style={{
                                    display: 'inline-block',
                                    verticalAlign: 'text-top',
                                    marginRight: '5px',
                                }}
                            />
                            {`${
                                editor?.view.state.doc.textContent?.length || 0
                            } / ${maxChars}`}
                        </StyledTextElement>
                    </>
                )}
            </TiptapEditorContext.Provider>
        )
    },
)

TiptapEditor.displayName = 'TiptapEditor'

TiptapEditor.propTypes = {
    contentType: PropTypes.oneOf(['json', 'html']),
    borderStyle: PropTypes.oneOf(['none', 'rounded']),
    height: PropTypes.oneOf(['small', 'normal', 'large']),
    textSize: PropTypes.oneOf(['small', 'normal', 'large']),
    maxChars: PropTypes.number,
    options: PropTypes.shape({
        textFormat: PropTypes.bool,
        textStyle: PropTypes.bool,
        textLink: PropTypes.bool,
        textAnchor: PropTypes.bool,
        textCSSClass: PropTypes.bool,
        textLanguage: PropTypes.bool,
        textQuote: PropTypes.bool,
        textList: PropTypes.bool,
        insertAccordion: PropTypes.bool,
        insertMedia: PropTypes.bool,
        disableAlignImage: PropTypes.bool,
        insertTable: PropTypes.bool,
        insertButton: PropTypes.bool,
        insertMention: PropTypes.bool,
    }),
    group: PropTypes.shape({
        guid: PropTypes.string,
        isClosed: PropTypes.bool,
    }),
    autofocus: PropTypes.bool,
    hasError: PropTypes.bool,
    disableNewLine: PropTypes.bool,
    disableNewParagraph: PropTypes.bool,
    hideKeyboardShortcuts: PropTypes.bool,
    delayMentionSuggestions: PropTypes.number,
}

export default TiptapEditor
