import Bold from '@tiptap/extension-bold'
import BulletList from '@tiptap/extension-bullet-list'
import CharacterCount from '@tiptap/extension-character-count'
import Code from '@tiptap/extension-code'
import CodeBlock from '@tiptap/extension-code-block'
import Document from '@tiptap/extension-document'
import Dropcursor from '@tiptap/extension-dropcursor'
import Focus from '@tiptap/extension-focus'
import Gapcursor from '@tiptap/extension-gapcursor'
import HardBreak from '@tiptap/extension-hard-break'
import Italic from '@tiptap/extension-italic'
import OrderedList from '@tiptap/extension-ordered-list'
import Paragraph from '@tiptap/extension-paragraph'
import Placeholder from '@tiptap/extension-placeholder'
import Strike from '@tiptap/extension-strike'
import TableRow from '@tiptap/extension-table-row'
import Text from '@tiptap/extension-text'
import Underline from '@tiptap/extension-underline'

import Accordion from './extensions/Accordion'
import Anchor from './extensions/Anchor'
import Blockquote from './extensions/Blockquote'
import Button from './extensions/Button'
import CSSClass from './extensions/CSSClass'
import Figure from './extensions/Figure'
import File from './extensions/File'
import Heading from './extensions/Heading'
import Image from './extensions/Image'
import Lang from './extensions/Lang'
import Link from './extensions/Link'
import ListItem from './extensions/ListItem'
import Mention from './extensions/Mention'
import ParagraphBold from './extensions/ParagraphBold'
import Table from './extensions/Table'
import TableCell from './extensions/TableCell'
import TableHeader from './extensions/TableHeader'
import Video from './extensions/Video'

export default ({
    placeholder,
    maxChars,
    textStyle,
    textLink,
    textAnchor,
    textCSSClass,
    textLanguage,
    textQuote,
    textList,
    insertButton,
    insertMedia,
    insertAccordion,
    insertTable,
    insertMention,
    disableNewLine,
    disableNewParagraph,
    group,
    delayMentionSuggestions,
}) => [
    Document,
    Placeholder.configure({
        placeholder,
    }),
    Dropcursor.configure({
        color: '#000000',
        width: 2,
    }),
    Gapcursor,
    Focus.configure({
        className: 'focus',
        mode: 'all',
    }),
    Paragraph,
    ParagraphBold,
    Code,
    CodeBlock,
    HardBreak.extend({
        addKeyboardShortcuts() {
            return {
                'Mod-Enter': ({ editor }) => {
                    if (!disableNewLine) {
                        return editor.commands.setHardBreak()
                    }
                },
                'Shift-Enter': ({ editor }) => {
                    if (!disableNewLine) {
                        return editor.commands.setHardBreak()
                    }
                },
                Enter: () => {
                    if (disableNewParagraph) {
                        return true
                    }
                },
            }
        },
    }),
    Text,
    CharacterCount.configure({
        limit: maxChars || null,
    }),
    Heading,
    ...(textStyle ? [Bold, Italic, Underline, Strike] : []),
    ...(textLink ? [Link] : []),
    ...(textAnchor ? [Anchor] : []),
    ...(textCSSClass ? [CSSClass] : []),
    ...(textLanguage ? [Lang] : []),
    ...(textQuote ? [Blockquote] : []),
    ...(textList ? [BulletList, OrderedList, ListItem] : []),
    ...(insertMedia ? [Figure, Image, Video, File] : []),
    ...(insertAccordion ? [Accordion] : []),
    ...(insertButton ? [Button] : []),
    ...(insertTable ? [Table, TableHeader, TableRow, TableCell] : []),
    ...(insertMention ? [Mention(group, delayMentionSuggestions)] : []),
]
