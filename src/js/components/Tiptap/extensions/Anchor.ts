import { Mark } from '@tiptap/core'

export default Mark.create({
    name: 'anchor',

    addAttributes() {
        return {
            id: {
                default: null,
            },
        }
    },

    parseHTML() {
        return [{ tag: '[id]' }]
    },

    renderHTML({ HTMLAttributes }) {
        return [
            'span',
            {
                id: HTMLAttributes.id,
                class: 'mark-anchor',
            },
            [
                'a',
                {
                    href: `#${HTMLAttributes.id}`,
                },
                0,
            ],
        ]
    },
})
