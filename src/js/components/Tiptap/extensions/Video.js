import { Node } from '@tiptap/core'
import { ReactNodeViewRenderer } from '@tiptap/react'
import { getVideoFromUrl } from 'helpers'

import Video from './components/Video/Video'

export default Node.create({
    name: 'video',
    group: 'block',

    addAttributes() {
        return {
            guid: {
                default: null,
                parseHTML: (element) => {
                    if (element.getAttribute('guid')) {
                        return element.getAttribute('guid')
                    } else {
                        const { id } = getVideoFromUrl(
                            element.getAttribute('src'),
                        )
                        return id
                    }
                },
            },
            platform: {
                default: null,
                parseHTML: (element) => {
                    if (element.getAttribute('platform')) {
                        return element.getAttribute('platform')
                    } else {
                        const { type } = getVideoFromUrl(
                            element.getAttribute('src'),
                        )
                        return type
                    }
                },
            },
            title: {
                default: null,
            },
        }
    },

    parseHTML() {
        return [
            {
                tag: 'iframe',
            },
        ]
    },

    renderHTML() {
        return ['iframe']
    },

    addNodeView() {
        return ReactNodeViewRenderer(Video)
    },
})
