import { Mark } from '@tiptap/core'

export default Mark.create({
    name: 'cssclass',

    addAttributes() {
        return {
            class: {
                default: null,
            },
        }
    },

    parseHTML() {
        return [{ tag: '[class]' }]
    },

    renderHTML({ HTMLAttributes }) {
        const classname = HTMLAttributes.class || ''
        return [
            'span',
            {
                class: `mark-cssclass ${classname}`,
            },
        ]
    },
})
