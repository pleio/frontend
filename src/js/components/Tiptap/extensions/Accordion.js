import { Node } from '@tiptap/core'
import { ReactNodeViewRenderer } from '@tiptap/react'

import Accordion from './components/Accordion'

export default Node.create({
    name: 'accordion',
    content: 'block*',
    group: 'block',

    addAttributes() {
        return {
            summary: {
                default: '',
                parseHTML: (element) =>
                    element.querySelector('summary')?.innerText,
            },
        }
    },

    parseHTML() {
        return [
            {
                tag: 'details',
            },
        ]
    },

    renderHTML({ HTMLAttributes }) {
        return ['details', HTMLAttributes, 0]
    },

    addNodeView() {
        return ReactNodeViewRenderer(Accordion)
    },
})
