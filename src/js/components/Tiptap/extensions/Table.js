import Table from '@tiptap/extension-table'

export default Table.extend({
    renderHTML({ HTMLAttributes }) {
        return [
            'div',
            { class: 'tiptap-table' },
            ['table', HTMLAttributes, ['tbody', 0]],
        ]
    },
}).configure({
    resizable: true,
    handleWidth: 8,
    lastColumnResizable: true,
})
