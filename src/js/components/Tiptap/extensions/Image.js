import { Node } from '@tiptap/core'
import { ReactNodeViewRenderer } from '@tiptap/react'

import Image from './components/Image/Image'

export default Node.create({
    name: 'image',
    group: 'block',

    addAttributes() {
        return {
            src: {
                default: null,
            },
            size: {
                default: 'large',
            },
            align: {
                default: 'center',
            },
            alt: {
                default: null,
            },
            caption: {
                default: null,
                parseHTML: (element) => element.getAttribute('title'),
            },
        }
    },

    parseHTML() {
        return [
            {
                tag: 'img[src]',
            },
        ]
    },

    renderHTML() {
        return ['img']
    },

    addNodeView() {
        return ReactNodeViewRenderer(Image)
    },
})
