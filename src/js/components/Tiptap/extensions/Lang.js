import { Mark, mergeAttributes } from '@tiptap/core'

export default Mark.create({
    name: 'lang',

    addAttributes() {
        return {
            lang: {
                default: null,
            },
        }
    },

    parseHTML() {
        return [{ tag: '[lang]' }]
    },

    renderHTML({ HTMLAttributes }) {
        return [
            'span',
            mergeAttributes(this.options.HTMLAttributes, HTMLAttributes),
            0,
        ]
    },
})
