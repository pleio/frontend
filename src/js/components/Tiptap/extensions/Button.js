import { Node } from '@tiptap/core'
import { ReactNodeViewRenderer } from '@tiptap/react'

import Button from './components/Button'

export default Node.create({
    name: 'button',
    content: 'text*',
    group: 'inline',
    inline: true,

    addAttributes() {
        return {
            url: {
                default: '',
            },
        }
    },

    renderHTML({ HTMLAttributes }) {
        return ['span', HTMLAttributes, 0]
    },

    addNodeView() {
        return ReactNodeViewRenderer(Button)
    },
})
