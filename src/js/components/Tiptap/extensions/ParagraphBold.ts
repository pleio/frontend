import { RawCommands, SingleCommands } from '@tiptap/core'
import Paragraph from '@tiptap/extension-paragraph'

export default Paragraph.extend({
    name: 'paragraph_bold',

    addAttributes() {
        return {
            class: {
                default: 'paragraph_bold',
            },
        }
    },

    addCommands() {
        return {
            setParagraphBold:
                () =>
                ({ commands }: { commands: SingleCommands }) => {
                    return commands.setNode(this.name)
                },
        } as Partial<RawCommands>
    },
})
