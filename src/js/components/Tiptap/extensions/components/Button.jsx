import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { NodeViewContent, NodeViewWrapper } from '@tiptap/react'
import { isExternalUrl } from 'helpers'

import ButtonComponent from 'js/components/Button/Button'
import HideVisually from 'js/components/HideVisually/HideVisually'

import ExternalIcon from 'icons/new-window-small.svg'

const Button = ({ node, editor }) => {
    const { url } = node.attrs
    const isExternal = isExternalUrl(url)
    const LinkElement = isExternal ? 'a' : Link
    const { t } = useTranslation()

    const IsExternalContent = isExternal ? (
        <div contentEditable={false}>
            <HideVisually>{t('global.opens-in-new-window')}</HideVisually>
            <ExternalIcon style={{ flexShrink: 0, marginLeft: '8px' }} />
        </div>
    ) : null

    if (editor.view.editable) {
        return (
            <NodeViewWrapper as="span">
                <ButtonComponent
                    as="span"
                    size="large"
                    variant="primary"
                    style={{ pointerEvents: 'none' }}
                >
                    <NodeViewContent />
                    {IsExternalContent}
                </ButtonComponent>
            </NodeViewWrapper>
        )
    } else {
        return (
            <NodeViewWrapper as="span">
                <ButtonComponent
                    as={LinkElement}
                    to={!isExternal ? url : null}
                    href={isExternal ? url : null}
                    target={isExternal ? '_blank' : null}
                    rel={isExternal ? 'noopener noreferrer' : null}
                    size="large"
                    variant="primary"
                >
                    <NodeViewContent />
                    {IsExternalContent}
                </ButtonComponent>
            </NodeViewWrapper>
        )
    }
}

export default Button
