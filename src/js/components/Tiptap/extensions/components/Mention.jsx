import React from 'react'
import { Link } from 'react-router-dom'
import { NodeViewWrapper } from '@tiptap/react'

const Mention = ({ node, editor }) => {
    const { id, label } = node.attrs

    const { editable } = editor.view

    if (editable) {
        node.type.spec.draggable = true
        return (
            <NodeViewWrapper as="span" data-drag-handle>
                @{label}
            </NodeViewWrapper>
        )
    } else {
        return (
            <NodeViewWrapper as={Link} to={`/user/${id}/profile`}>
                @{label}
            </NodeViewWrapper>
        )
    }
}

export default Mention
