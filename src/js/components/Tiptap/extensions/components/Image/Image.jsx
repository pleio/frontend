import React from 'react'
import { NodeViewWrapper } from '@tiptap/react'
import styled, { css } from 'styled-components'

import Img from 'js/components/Img/Img'

import ImageEdit from './ImageEdit'

const Wrapper = styled(NodeViewWrapper)`
    // Prevent images float next to each other (messy layout)
    clear: both;

    ${(p) =>
        ['left', 'right'].includes(p.$align) &&
        ['small', 'medium'].includes(p.$size) &&
        css`
            /* Wrap text around image if aligned left or right and small enough */
            ${(p) =>
                p.$align === 'left' &&
                css`
                    float: left;
                    padding-right: 8px;
                    margin-right: 8px;
                `};

            ${(p) =>
                p.$align === 'right' &&
                css`
                    float: right;
                    padding-left: 8px;
                    margin-left: 8px;
                `};

            ${(p) =>
                p.$size === 'small' &&
                css`
                    width: 25%;
                `};

            ${(p) =>
                p.$size === 'medium' &&
                css`
                    width: 50%;
                `};
        `};

    .image-wrapper {
        position: relative;
        margin: 0 auto;
        text-align: center;
        cursor: default;

        ${(p) =>
            (!p.$align || p.$align === 'center') &&
            css`
                ${(p) =>
                    p.$size === 'small' &&
                    css`
                        width: 25%;
                    `};

                ${(p) =>
                    p.$size === 'medium' &&
                    css`
                        width: 50%;
                    `};
            `};

        ${(p) =>
            p.$size === 'large' &&
            css`
                width: auto;
            `};
    }

    ${(p) =>
        p.$editable &&
        css`
            .react-renderer.focus > & .image-wrapper {
                border-radius: 1px;
                box-shadow:
                    0 0 0 2px white,
                    0 0 0 4px black;
            }

            .ProseMirror-focused
                .ProseMirror-gapcursor
                + .react-renderer.focus
                > &
                .image-wrapper {
                box-shadow: none;
            }

            img,
            figcaption {
                user-select: none;
            }
        `};

    figcaption {
        color: ${(p) => p.theme.color.text.grey};
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
    }
`

const Image = ({ node, updateAttributes, editor, selected }) => {
    const { src, size, align, alt, caption } = node.attrs

    const { editable } = editor.view

    let captionElement

    if (caption && typeof caption === 'string') {
        captionElement = (
            <figcaption
                style={{
                    marginTop: '8px',
                }}
                dangerouslySetInnerHTML={{ __html: caption }}
            />
        )
    }

    const imageElement = (
        <Img
            key={src}
            src={src}
            alt={alt ?? ''}
            align="center"
            showLoadingPlaceholder
        />
    )

    return (
        <Wrapper
            $editable={editable}
            $size={size}
            $align={align}
            data-drag-handle={editable}
        >
            {editable ? (
                <ImageEdit
                    imageElement={imageElement}
                    captionElement={captionElement}
                    node={node}
                    updateAttributes={updateAttributes}
                    selected={selected}
                />
            ) : (
                <figure>
                    <div className="image-wrapper">{imageElement}</div>
                    {captionElement}
                </figure>
            )}
        </Wrapper>
    )
}

export default Image
