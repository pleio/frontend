import React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import Spacer from 'js/components/Spacer/Spacer'

const ImageProperties = ({ alt, caption, updateAttributes, onClose }) => {
    const { t } = useTranslation()

    const submit = ({ alt, caption }) => {
        updateAttributes({
            alt,
            caption,
        })
        onClose()
    }

    const defaultValues = {
        alt,
        caption,
    }

    const { control, handleSubmit } = useForm({
        defaultValues,
    })

    return (
        <Spacer
            as="form"
            onSubmit={(evt) => {
                evt.preventDefault()
                handleSubmit(submit)()
                // Prevent parent form from submitting
                evt.stopPropagation()
            }}
            style={{
                display: 'flex',
                flexDirection: 'column',
                overflow: 'hidden',
                paddingTop: '6px',
            }}
        >
            <FormItem
                type="text"
                control={control}
                name="alt"
                label={t('editor.image-alt')}
                helper={t('editor.image-alt-helper')}
            />

            <FormItem
                control={control}
                type="rich"
                name="caption"
                label={t('editor.image-caption')}
                helper={t('editor.image-caption-helper')}
                options={{
                    textLink: true,
                }}
                height="small"
                textSize="small"
                contentType="html"
                style={{ overflowY: 'auto' }}
            />

            <Flexer>
                <Button
                    size="normal"
                    variant="tertiary"
                    type="button"
                    onClick={onClose}
                >
                    {t('action.cancel')}
                </Button>
                <Button size="normal" variant="primary" type="submit">
                    {t('action.save')}
                </Button>
            </Flexer>
        </Spacer>
    )
}

export default ImageProperties
