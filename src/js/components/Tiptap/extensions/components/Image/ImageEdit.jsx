import React, { useContext, useState } from 'react'
import { useTranslation } from 'react-i18next'

import Modal from 'js/components/Modal/Modal'
import Popover from 'js/components/Popover/Popover'
import ActionMenu from 'js/components/Tiptap/extensions/components/ActionMenu'
import { TiptapEditorContext } from 'js/components/Tiptap/TiptapEditor'

import AlignCenterIcon from 'icons/image-align-center.svg'
import AlignLeftIcon from 'icons/image-align-left.svg'
import AlignRightIcon from 'icons/image-align-right.svg'
import SizeLargeIcon from 'icons/image-size-large.svg'
import SizeNormalIcon from 'icons/image-size-normal.svg'
import SizeSmallIcon from 'icons/image-size-small.svg'
import InfoIcon from 'icons/info.svg'

import ImageProperties from './ImageProperties'

const ImageEdit = ({
    imageElement,
    captionElement,
    node,
    updateAttributes,
    selected,
}) => {
    node.type.spec.draggable = true

    const { disableAlignImage } = useContext(TiptapEditorContext)

    const { t } = useTranslation()

    const { size, align, alt, caption } = node.attrs

    const [showSettingsModal, setShowSettingsModal] = useState(false)
    const toggleSettingsModal = () => {
        setShowSettingsModal(!showSettingsModal)
    }

    const onUpdateSize = (size) => {
        updateAttributes({
            size,
        })
    }

    const onUpdateAlign = (align) => {
        updateAttributes({
            align,
        })
    }

    const canAlign = !disableAlignImage && ['small', 'medium'].includes(size)

    const actionOptions = [
        [
            {
                isActive: size === 'small',
                onClick: () => onUpdateSize('small'),
                tooltip: t('editor.image-size', {
                    size: '25%',
                }),
                icon: <SizeSmallIcon />,
            },
            {
                isActive: size === 'medium',
                onClick: () => onUpdateSize('medium'),
                tooltip: t('editor.image-size', {
                    size: '50%',
                }),
                icon: <SizeNormalIcon />,
            },
            {
                isActive: size === 'large',
                onClick: () => onUpdateSize('large'),
                tooltip: t('editor.image-size', {
                    size: '100%',
                }),
                icon: <SizeLargeIcon />,
            },
        ],
        ...(canAlign
            ? [
                  [
                      {
                          isActive: align === 'left',
                          onClick: () => onUpdateAlign('left'),
                          tooltip: t('editor.image-align-left'),
                          icon: <AlignLeftIcon />,
                      },
                      {
                          isActive: align === 'center',
                          onClick: () => onUpdateAlign('center'),
                          tooltip: t('editor.image-align-center'),
                          icon: <AlignCenterIcon />,
                      },
                      {
                          isActive: align === 'right',
                          onClick: () => onUpdateAlign('right'),
                          tooltip: t('editor.image-align-right'),
                          icon: <AlignRightIcon />,
                      },
                  ],
              ]
            : []),
        {
            variant: 'secondary',
            onClick: toggleSettingsModal,
            tooltip: t('editor.image-properties'),
            icon: <InfoIcon />,
        },
    ]

    return (
        <Popover
            trigger="manual"
            visible={selected && !showSettingsModal}
            placement="bottom"
            arrow={false}
            content={
                <>
                    <ActionMenu options={actionOptions} />
                    <Modal
                        isVisible={showSettingsModal}
                        onClose={toggleSettingsModal}
                        size="small"
                        title={t('editor.image-properties')}
                        containHeight
                    >
                        <ImageProperties
                            alt={alt}
                            caption={caption}
                            updateAttributes={updateAttributes}
                            onClose={toggleSettingsModal}
                        />
                    </Modal>
                </>
            }
        >
            <figure>
                <div className="image-wrapper">{imageElement}</div>
                {captionElement}
            </figure>
        </Popover>
    )
}

export default ImageEdit
