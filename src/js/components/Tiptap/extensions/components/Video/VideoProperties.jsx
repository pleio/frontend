import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import Textfield from 'js/components/Textfield/Textfield'

const VideoProperties = ({ title, updateAttributes, onClose }) => {
    const [titleInput, setTitleInput] = useState(title)
    const changeTitleInput = (evt) => setTitleInput(evt.target.value)

    const onSubmit = () => {
        updateAttributes({
            title: titleInput,
        })
        onClose()
    }

    const { t } = useTranslation()

    return (
        <>
            <div style={{ marginBottom: '16px' }}>
                <Textfield
                    name="video-description"
                    label={t('editor.video-title')}
                    helper={t('editor.video-title-helper')}
                    value={titleInput}
                    onChange={changeTitleInput}
                />
            </div>

            <Flexer mt>
                <Button
                    size="normal"
                    variant="tertiary"
                    type="button"
                    onClick={onClose}
                >
                    {t('action.cancel')}
                </Button>
                <Button size="normal" variant="primary" onClick={onSubmit}>
                    {t('action.save')}
                </Button>
            </Flexer>
        </>
    )
}

export default VideoProperties
