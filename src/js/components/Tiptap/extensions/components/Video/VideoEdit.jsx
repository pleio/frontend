import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'

import Modal from 'js/components/Modal/Modal'
import Popover from 'js/components/Popover/Popover'
import ActionMenu from 'js/components/Tiptap/extensions/components/ActionMenu'

import InfoIcon from 'icons/info.svg'

import VideoProperties from './VideoProperties'

const VideoEdit = ({ node, updateAttributes, selected, videoElement }) => {
    const { title } = node.attrs

    node.type.spec.draggable = true

    const { t } = useTranslation()

    const [showSettingsModal, setShowSettingsModal] = useState(false)
    const toggleSettingsModal = () => {
        setShowSettingsModal(!showSettingsModal)
    }

    const actionOptions = [
        {
            variant: 'secondary',
            onClick: toggleSettingsModal,
            tooltip: t('editor.video-properties'),
            icon: <InfoIcon />,
        },
    ]

    return (
        <>
            <Popover
                trigger="manual"
                visible={selected && !showSettingsModal}
                placement="bottom"
                arrow={false}
                content={
                    <>
                        <ActionMenu options={actionOptions} />
                        <Modal
                            isVisible={showSettingsModal}
                            onClose={toggleSettingsModal}
                            size="small"
                            title={t('editor.video-properties')}
                        >
                            <VideoProperties
                                title={title}
                                updateAttributes={updateAttributes}
                                onClose={toggleSettingsModal}
                            />
                        </Modal>
                    </>
                }
            >
                <div className="OptionsReference" />
            </Popover>
            {videoElement}
        </>
    )
}

export default VideoEdit
