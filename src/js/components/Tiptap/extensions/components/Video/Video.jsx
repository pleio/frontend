import React from 'react'
import { NodeViewWrapper } from '@tiptap/react'
import styled, { css } from 'styled-components'

import VideoIframe from 'js/components/VideoIframe'

import VideoEdit from './VideoEdit'

const Wrapper = styled(NodeViewWrapper)`
    position: relative;
    width: 100%;
    height: 100px;
    padding-bottom: 56.3%;
    background-color: transparent;

    iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    ${(p) =>
        p.$editable &&
        css`
            iframe {
                pointer-events: none;
            }

            .react-renderer.focus > & {
                border-radius: 1px;
                box-shadow:
                    0 0 0 2px white,
                    0 0 0 4px black;
            }

            .ProseMirror-focused
                .ProseMirror-gapcursor
                + .react-renderer.focus
                > & {
                box-shadow: none;
            }

            .OptionsReference {
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
            }
        `};
`

const Video = ({ node, updateAttributes, editor, selected }) => {
    const { guid, platform, title } = node.attrs

    const { editable } = editor.view

    const videoElement = (
        <VideoIframe
            id={guid}
            type={platform}
            title={title}
            disableKeyboard
            editable={editable}
        />
    )

    return (
        <Wrapper $editable={editable} data-drag-handle={editable}>
            {editable ? (
                <VideoEdit
                    node={node}
                    updateAttributes={updateAttributes}
                    videoElement={videoElement}
                    selected={selected}
                />
            ) : (
                videoElement
            )}
        </Wrapper>
    )
}

export default Video
