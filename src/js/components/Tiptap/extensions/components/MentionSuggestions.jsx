import React, {
    forwardRef,
    useContext,
    useEffect,
    useImperativeHandle,
    useState,
} from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useLazyQuery } from '@apollo/client'
import { useDebounce } from 'helpers'
import styled from 'styled-components'

import { AriaLiveContext, AriaLiveMessage } from 'js/components/AriaLive'
import Avatar from 'js/components/Avatar/Avatar'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'

const USERSQUERY = gql`
    query MentionUsersQuery($q: String!, $groupGuid: String) {
        users(limit: 50, q: $q, groupGuid: $groupGuid) {
            edges {
                guid
                name
                icon
            }
        }
    }
`

const Wrapper = styled.div`
    padding: 4px 0;
    max-height: 340px;
    overflow: auto;

    .mention-suggestion {
        display: flex;
        width: 100%;
        padding: 4px 12px 4px 8px;

        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};

        span {
            padding-top: 2px;
        }

        &:hover {
            background-color: ${(p) => p.theme.color.hover};
        }

        &:active {
            background-color: ${(p) => p.theme.color.active};
        }

        &.is-focused {
            position: relative;
            background-color: ${(p) => p.theme.color.hover};

            &:before {
                content: '';
                position: absolute;
                top: 0;
                left: 0;
                width: 2px;
                height: 100%;
                background: black;
            }
        }
    }
`

const MentionSuggestions = forwardRef(({ query, command, group }, ref) => {
    if (!query || query.length < 2) return null

    const [findUser, { loading, data }] = useLazyQuery(USERSQUERY)

    const debouncedVal = useDebounce(query, 200)

    useEffect(() => {
        findUser({
            variables: {
                q: debouncedVal,
                groupGuid: group?.isClosed ? group?.guid : null,
            },
        })
    }, [debouncedVal])

    const [focusedIndex, setFocusedIndex] = useState(null)
    const { announceAssertive } = useContext(AriaLiveContext)
    const { t } = useTranslation()

    useEffect(() => {
        if (focusedIndex !== null) {
            announceAssertive(
                t('editor.mention-option', {
                    label: users[focusedIndex]?.name,
                    count: focusedIndex + 1,
                    total: users.length,
                }),
            )
        }
    }, [focusedIndex])

    const selectItem = (index) => {
        const user = users[index]

        if (user) {
            command({ id: user.guid, label: user.name })
        }
    }

    const upHandler = () => {
        focusedIndex === null
            ? setFocusedIndex(users.length - 1)
            : setFocusedIndex((focusedIndex + users.length - 1) % users.length)
    }

    const downHandler = () => {
        focusedIndex === null
            ? setFocusedIndex(0)
            : setFocusedIndex((focusedIndex + 1) % users.length)
    }

    const enterHandler = () => {
        selectItem(focusedIndex)
        setFocusedIndex(null)
    }

    useImperativeHandle(ref, () => ({
        onKeyDown: ({ event }) => {
            if (event.key === 'ArrowUp') {
                upHandler()
                event.preventDefault()
                return true
            }

            if (event.key === 'ArrowDown') {
                downHandler()
                event.preventDefault()
                return true
            }

            if (event.key === 'Enter') {
                event.stopPropagation() // Prevents submitting by Enter
                enterHandler()
                return true
            }

            return false
        },
    }))

    const users = data?.users?.edges

    return (
        <Wrapper>
            {loading || !data ? (
                <LoadingSpinner />
            ) : (
                <ul>
                    <AriaLiveMessage
                        message={t('editor.focus-mention-suggestions')}
                    />
                    {users.map((entity, index) => (
                        <li key={`${index}-${entity.name}`}>
                            <button
                                type="button"
                                onClick={() => selectItem(index)}
                                className={`mention-suggestion ${
                                    index === focusedIndex ? 'is-focused' : ''
                                }`}
                            >
                                <Avatar
                                    disabled
                                    image={entity.icon}
                                    name={entity.name}
                                    size="small"
                                    style={{ marginRight: '8px' }}
                                />
                                <span>{entity.name}</span>
                            </button>
                        </li>
                    ))}
                </ul>
            )}
        </Wrapper>
    )
})

MentionSuggestions.displayName = 'MentionSuggestions'

export default MentionSuggestions
