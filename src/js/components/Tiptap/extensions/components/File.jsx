import React from 'react'
import { useTranslation } from 'react-i18next'
import { NodeViewWrapper } from '@tiptap/react'
import { humanFileSize } from 'helpers'
import styled from 'styled-components'

import FileFolderIcon from 'js/files/components/FileFolderIcon'
import getIconNameByMimetype from 'js/files/helpers/getIconNameByMimetype'

const Wrapper = styled(NodeViewWrapper)`
    .react-renderer.focus > & {
        border-radius: 1px;
        box-shadow:
            0 0 0 2px white,
            0 0 0 4px black;
    }

    .ProseMirror-focused .ProseMirror-gapcursor + .react-renderer.focus > & {
        box-shadow: none;
    }

    .FileWrapper {
        display: flex;
        align-items: center;
        padding: 6px 8px;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        border-radius: ${(p) => p.theme.radius.normal};
        border: 1px solid ${(p) => p.theme.color.grey[30]};

        text-decoration: none;
    }

    a.FileWrapper {
        .AttachmentName {
            color: ${(p) => p.theme.color.primary.main};
        }

        &:hover .AttachmentName {
            text-decoration: underline;
        }
    }

    .AttachmentIcon {
        flex-shrink: 0;
        width: 20px;
        height: 24px;
        display: flex;
        justify-content: center;
        align-items: center;
        margin-right: 8px;
    }

    .AttachmentSize {
        flex-shrink: 0;
        margin-left: auto;
        font-size: ${(p) => p.theme.font.size.tiny};
        line-height: ${(p) => p.theme.font.lineHeight.tiny};
        color: ${(p) => p.theme.color.text.grey};
    }
`

const File = ({ node, editor }) => {
    const { name, subtype, mimeType, url, size, notAvailable } = node.attrs

    const { t } = useTranslation()

    const { editable } = editor.view

    // This check is required for folders that were inserted before subtype was being saved
    const isFolder = subtype ? subtype === 'folder' : mimeType === null
    const iconName = isFolder
        ? 'folder'
        : subtype === 'pad'
          ? 'pad'
          : getIconNameByMimetype(mimeType)

    const notFound = notAvailable || !url
    const title = (
        <>
            {name}
            {notFound ? ` (${t('entity-file.deleted')})` : ''}
        </>
    )

    if (editable) {
        node.type.spec.draggable = true
        return (
            <Wrapper data-drag-handle>
                <div className="FileWrapper">
                    <div className="AttachmentIcon">
                        <FileFolderIcon name={iconName} />
                    </div>
                    <div className="AttachmentName">{title}</div>
                    {!!size && !notFound && (
                        <div className="AttachmentSize">
                            {humanFileSize(size)}
                        </div>
                    )}
                </div>
            </Wrapper>
        )
    } else {
        const LinkElement = notFound ? 'div' : 'a'
        return (
            <Wrapper>
                <LinkElement
                    className="FileWrapper"
                    {...(notFound
                        ? {}
                        : {
                              href: url,
                              target: '_blank',
                              rel: 'noopener noreferrer',
                          })}
                >
                    <div className="AttachmentIcon">
                        <FileFolderIcon name={iconName} />
                    </div>
                    <div className="AttachmentName">{title}</div>
                    {!!size && !notFound && (
                        <div className="AttachmentSize">
                            {humanFileSize(size)}
                        </div>
                    )}
                </LinkElement>
            </Wrapper>
        )
    }
}

export default File
