import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { NodeViewContent, NodeViewWrapper } from '@tiptap/react'
import styled from 'styled-components'

import IconButton from 'js/components/IconButton/IconButton'

import ChevronDownIcon from 'icons/chevron-down.svg'
import MoveIcon from 'icons/move-vertical.svg'

const Wrapper = styled(NodeViewWrapper)`
    .AccordionDragHandle {
        cursor: grab;

        &:active {
            cursor: grabbing;
        }
    }
`

const Accordion = ({ node, updateAttributes, editor }) => {
    const { t } = useTranslation()

    if (editor.view.editable) {
        node.type.spec.draggable = true

        const handleKeyDown = (evt) => {
            // Prevent submitting a form (if only one input is present inside a form it will submit on enter)
            if (evt.key === 'Enter') {
                evt.preventDefault()
            }
        }

        const onUpdateSummary = (evt) => {
            updateAttributes({
                summary: evt.target.value,
            })
        }

        const [open, setOpen] = useState(true)
        const toggleOpen = () => {
            setOpen(!open)
        }

        return (
            <Wrapper className="tiptap-details" open={open || null}>
                <div className="tiptap-details-summary" contentEditable={false}>
                    <IconButton
                        size="large"
                        hoverSize="normal"
                        variant="secondary"
                        data-drag-handle
                        className="AccordionDragHandle"
                    >
                        <MoveIcon />
                    </IconButton>
                    <input
                        className="tiptap-details-summary-input"
                        type="text"
                        placeholder={t('form.title')}
                        aria-label={t('editor.accordion-title')}
                        value={node.attrs.summary}
                        onChange={onUpdateSummary}
                        onKeyDown={handleKeyDown}
                    />
                    <IconButton
                        size="large"
                        hoverSize="normal"
                        variant="secondary"
                        onClick={toggleOpen}
                        aria-expanded={open}
                        aria-label={t('editor.show-accordion-content')}
                    >
                        <ChevronDownIcon
                            style={{
                                transform: `scaleY(${open ? -1 : 1})`,
                            }}
                        />
                    </IconButton>
                </div>
                {open && <NodeViewContent className="tiptap-details-content" />}
            </Wrapper>
        )
    } else {
        return (
            <NodeViewWrapper>
                <details>
                    <summary>
                        <div className="tipta-details-title">
                            {node.attrs.summary || t('form.title')}
                        </div>
                        <div className="tiptap-details-expand">
                            <ChevronDownIcon />
                        </div>
                    </summary>
                    <NodeViewContent className="tiptap-details-content" />
                </details>
            </NodeViewWrapper>
        )
    }
}

export default Accordion
