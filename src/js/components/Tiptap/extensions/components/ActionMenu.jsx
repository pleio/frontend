import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import Flexer from 'js/components/Flexer/Flexer'
import IconButton from 'js/components/IconButton/IconButton'

const Wrapper = styled(Flexer)`
    display: flex;
    border-radius: ${(p) => p.theme.radius.large};
    overflow: hidden;
    padding: 4px;
`

const ActionMenu = ({ options, ...rest }) => (
    <Wrapper divider="normal" gutter="normal" {...rest}>
        {options.map((option, i) => {
            if (Array.isArray(option)) {
                return (
                    <Flexer gutter="none" key={i}>
                        {option.map(({ icon, ...rest }, j) => (
                            <ActionButton key={`${i}-${j}`} {...rest}>
                                {icon}
                            </ActionButton>
                        ))}
                    </Flexer>
                )
            } else {
                const { icon, ...rest } = option
                return (
                    <ActionButton key={i} {...rest}>
                        {icon}
                    </ActionButton>
                )
            }
        })}
    </Wrapper>
)

const ActionButton = ({ isActive, children, ...rest }) => {
    return (
        <IconButton
            size="normal"
            variant="tertiary"
            aria-pressed={isActive}
            disabled={isActive}
            radiusStyle="rounded"
            {...rest}
        >
            {children}
        </IconButton>
    )
}

const actionShape = PropTypes.shape({
    variant: PropTypes.string,
    onClick: PropTypes.func,
    tooltip: PropTypes.string,
    isActive: PropTypes.bool,
    icon: PropTypes.element,
})

ActionMenu.propTypes = {
    options: PropTypes.arrayOf(
        PropTypes.oneOfType([actionShape, PropTypes.arrayOf(actionShape)]),
    ).isRequired,
}

export default ActionMenu
