import { Node } from '@tiptap/core'
import { ReactNodeViewRenderer } from '@tiptap/react'

import File from './components/File'

export default Node.create({
    name: 'file',
    group: 'block',
    draggable: false,

    addAttributes() {
        return {
            guid: {
                default: null,
            },
            name: {
                default: null,
            },
            subtype: {
                default: null,
            },
            mimeType: {
                default: null,
            },
            url: {
                default: null,
            },
            size: {
                default: null,
            },
            notAvailable: {
                default: false,
            },
        }
    },

    renderHTML() {
        return ['a']
    },

    addNodeView() {
        return ReactNodeViewRenderer(File)
    },
})
