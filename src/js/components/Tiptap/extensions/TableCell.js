import TableCell from '@tiptap/extension-table-cell'

export default TableCell.extend({
    content: '(paragraph|list)+',
    // Add `width` attribute to correctly show column width.
    addAttributes() {
        return {
            ...this.parent?.(),
            width: {
                renderHTML: (attributes) => {
                    return {
                        style: `min-width: ${attributes.colwidth}px`,
                    }
                },
            },
        }
    },
})
