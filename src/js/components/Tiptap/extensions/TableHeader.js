import TableHeader from '@tiptap/extension-table-header'

export default TableHeader.extend({
    content: 'paragraph',
    // Add `width` attribute to correctly show column width.
    addAttributes() {
        return {
            ...this.parent?.(),
            width: {
                renderHTML: (attributes) => {
                    return {
                        style: `min-width: ${attributes.colwidth}px`,
                    }
                },
            },
        }
    },
})
