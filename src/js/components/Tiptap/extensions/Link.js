import Link from '@tiptap/extension-link'

export default Link.configure({
    openOnClick: false,
    HTMLAttributes: {
        rel: null,
        target: null,
        class: 'mark-link',
    },
})
