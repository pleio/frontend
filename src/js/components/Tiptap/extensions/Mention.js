import MentionExtension from '@tiptap/extension-mention'
import { ReactNodeViewRenderer, ReactRenderer } from '@tiptap/react'
import tippy from 'tippy.js'

import Mention from './components/Mention'
import MentionSuggestions from './components/MentionSuggestions'

export default (group, delayMentionSuggestions) =>
    MentionExtension.extend({
        selectable: true,
        addNodeView() {
            return ReactNodeViewRenderer(Mention)
        },
    }).configure({
        HTMLAttributes: {
            class: 'mention',
        },
        suggestion: {
            render: () => {
                let component
                let popup

                return {
                    onStart: (props) => {
                        component = new ReactRenderer(MentionSuggestions, {
                            props: {
                                ...props,
                                group,
                                delayMentionSuggestions,
                            },
                            editor: props.editor,
                        })

                        popup = tippy('body', {
                            getReferenceClientRect: props.clientRect,
                            appendTo: () => document.body,
                            content: component.element,
                            interactive: true,
                            theme: 'popover',
                            trigger: 'manual',
                            placement: 'bottom-start',
                            duration: [200, 150],
                        })
                    },

                    onUpdate(props) {
                        const popupIsVisible = popup[0].state.isVisible

                        if (props.query.length > 0) {
                            component.updateProps(props)
                            if (props.query.length > 0) {
                                component.updateProps(props)
                                if (!popupIsVisible) {
                                    setTimeout(() => {
                                        popup[0].show()
                                    }, delayMentionSuggestions) // Use delayMentionSuggestions here to fix tippy calculations (see ticket #15588)
                                }
                            }
                        }

                        if (popupIsVisible && props.query.length === 0) {
                            popup[0].hide()
                        }
                    },
                    onKeyDown(props) {
                        const popupIsVisible = popup[0].state.isVisible

                        if (popupIsVisible && props.event.key === 'Escape') {
                            popup[0].hide()
                            return true
                        }

                        if (
                            !popupIsVisible &&
                            (props.event.key === 'ArrowUp' ||
                                props.event.key === 'ArrowDown')
                        ) {
                            popup[0].show()
                            return true
                        }

                        if (popupIsVisible) {
                            return component.ref?.onKeyDown(props)
                        }
                    },

                    onExit() {
                        popup[0].destroy()
                        component.destroy()
                    },
                }
            },
        },
    })
