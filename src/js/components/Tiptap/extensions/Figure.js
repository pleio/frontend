import { Node } from '@tiptap/core'
import { ReactNodeViewRenderer } from '@tiptap/react'

import Image from './components/Image/Image'

export default Node.create({
    name: 'figure',
    group: 'block',
    draggable: false,

    addAttributes() {
        return {
            src: {
                default: null,
                parseHTML: (element) =>
                    element.querySelector('img')?.getAttribute('src'),
            },
            size: {
                default: 'large',
            },
            alt: {
                default: null,
                parseHTML: (element) =>
                    element.querySelector('img')?.getAttribute('alt'),
            },
            caption: {
                default: null,
                parseHTML: (element) =>
                    element.querySelector('figcaption')?.innerHTML,
            },
        }
    },

    parseHTML() {
        return [
            {
                tag: 'figure',
            },
        ]
    },

    renderHTML() {
        return ['figure']
    },

    addNodeView() {
        return ReactNodeViewRenderer(Image)
    },
})
