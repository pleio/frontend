import Heading from '@tiptap/extension-heading'

export default Heading.extend({
    addOptions: {
        ...Heading.options,
        levels: [2, 3, 4, 5],
    },
})
