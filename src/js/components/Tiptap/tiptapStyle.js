/*
    Editor base styling
*/

import { css } from 'styled-components'

export default css`
    .ProseMirror {
        text-wrap: pretty;
        word-wrap: break-word;
        white-space: pre-wrap;
        white-space: break-spaces;
        -webkit-font-variant-ligatures: none;
        font-variant-ligatures: none;
        font-feature-settings: 'liga' 0; /* the above doesn't seem to work in Edge */
        outline: none !important;
        font-size: ${(p) => p.theme.font.size.normal};
        line-height: ${(p) => p.theme.font.lineHeight.normal};
        font-weight: ${(p) => p.theme.font.weight.normal};

        [contenteditable='false'] {
            white-space: normal;

            [contenteditable='true'] {
                white-space: pre-wrap;
            }
        }

        /* Fix issue if last element is floating */
        &:after {
            content: '';
            display: block;
            clear: both;
        }

        pre {
            white-space: pre-wrap;
        }

        ul,
        ol {
            overflow: hidden; // Fixes padding when floating next to image
            padding-left: 24px;
        }

        ul {
            list-style: disc;
        }

        ol {
            list-style: numbered;
            list-style-type: decimal;
        }

        ul li::before {
            content: none;
        }

        h2 {
            font-family: ${(p) => p.theme.fontHeading.family};
            font-weight: ${(p) => p.theme.fontHeading.weight.bold};
            font-size: ${(p) => p.theme.fontHeading.size.normal};
            line-height: ${(p) => p.theme.fontHeading.lineHeight.normal};
        }

        h3 {
            font-family: ${(p) => p.theme.fontHeading.family};
            font-weight: ${(p) => p.theme.fontHeading.weight.bold};
            font-size: ${(p) => p.theme.fontHeading.size.small};
            line-height: ${(p) => p.theme.fontHeading.lineHeight.small};
        }

        h4 {
            font-family: ${(p) => p.theme.fontHeading.family};
            font-weight: ${(p) => p.theme.fontHeading.weight.semibold};
            font-size: ${(p) => p.theme.font.size.large};
            line-height: ${(p) => p.theme.font.lineHeight.large};
        }

        h5 {
            font-family: ${(p) => p.theme.fontHeading.family};
            font-weight: ${(p) => p.theme.fontHeading.weight.semibold};
            font-size: ${(p) => p.theme.font.size.normal};
            line-height: ${(p) => p.theme.font.lineHeight.normal};
        }

        code {
            font-size: 0.9em;
        }

        .mark-link {
            color: ${(p) => p.theme.color.primary.main};
            text-decoration: underline;
        }

        .mark-anchor {
            /* Create offset for fixed navigation */
            padding-top: ${(p) => p.theme.headerBarHeight * 2}px;
            margin-top: -${(p) => p.theme.headerBarHeight * 2}px;
            pointer-events: none;

            > a {
                pointer-events: auto;
            }
        }

        blockquote {
            border-left: 2px solid ${(p) => p.theme.color.grey[30]};
            margin: 0;
            padding-left: 24px;
            font-family: ${(p) => p.theme.fontHeading.family};
            font-weight: ${(p) => p.theme.fontHeading.weight.normal};
            font-size: ${(p) => p.theme.fontHeading.size.normal};
            line-height: ${(p) => p.theme.fontHeading.lineHeight.normal};
        }

        code {
            padding: 0 3px 1px;
            background-color: ${(p) => p.theme.color.grey[10]};
            border-radius: ${(p) => p.theme.radius.small};
            border: 1px solid ${(p) => p.theme.color.grey[20]};
        }

        pre {
            padding: 0 3px 1px;
            background-color: ${(p) => p.theme.color.grey[10]};
            border-radius: ${(p) => p.theme.radius.small};
            border: 1px solid ${(p) => p.theme.color.grey[20]};

            code {
                background-color: transparent;
                border: none;
                border-radius: 0;
            }
        }

        details,
        .tiptap-details {
            overflow: hidden;
            border: 1px solid ${(p) => p.theme.color.grey[30]};
            border-radius: ${(p) => p.theme.radius.small};

            &[open] {
                summary,
                .tiptap-details-summary {
                    border-bottom: 1px solid ${(p) => p.theme.color.grey[30]};

                    svg {
                        transform: scaleY(-1);
                    }
                }
            }
        }

        .tiptap-details-summary {
            display: flex;
        }

        summary {
            display: flex;
            justify-content: space-between;
            padding: 2px 4px 2px 16px;
            font-weight: ${(p) => p.theme.font.weight.bold};
            outline-offset: -1px;
            cursor: pointer;

            &::marker,
            &::-webkit-details-marker {
                display: none;
            }

            &:hover .tiptap-details-expand {
                background-color: ${(p) => p.theme.color.hover};
            }

            &:active .tiptap-details-expand {
                background-color: ${(p) => p.theme.color.active};
            }

            body:not(.mouse-user) &:focus {
                outline: none;

                .tiptap-details-expand {
                    outline: ${(p) => p.theme.focusStyling};
                }
            }
        }

        .tipta-details-title {
            margin: 6px 4px 5px 0;
        }

        .tiptap-details-expand {
            margin-top: 1px;
            flex-shrink: 0;
            width: 32px;
            height: 32px;
            border-radius: 50%;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .tiptap-details-content {
            padding: 12px 16px;

            > * > * + * {
                margin-top: 24px;
            }
        }

        /* View mode wrapper */
        .tiptap-table {
            overflow-x: auto;
        }

        /* Edit mode wrapper */
        .tableWrapper {
            overflow-x: auto;
        }

        table {
            min-width: 100% !important; // Override inline styles
            border-collapse: collapse;
            table-layout: fixed;

            td,
            th {
                border: 1px solid ${(p) => p.theme.color.grey[30]};
                box-sizing: border-box;
                min-width: 1em;
                height: 32px;
                padding: 3px 6px;
                position: relative;
                vertical-align: top;
                background-clip: padding-box; // fixes border visibility in Firefox

                > * {
                    margin: 0;
                }
            }

            th {
                background-color: ${(p) => p.theme.color.grey[10]};
                font-weight: ${(p) => p.theme.font.weight.bold};
                text-align: left;
            }
        }

        .node-file + .node-file,
        .node-accordion + .node-accordion {
            margin-top: -1px !important;
            position: relative;

            .FileWrapper,
            details,
            .tiptap-details {
                border-top-left-radius: 0;
                border-top-right-radius: 0;
            }

            &:after {
                content: '';
                position: absolute;
                display: block;
                top: -2px;
                left: 0;
                right: 0;
                height: 2px;
                background: white;
                border-left: 1px solid ${(p) => p.theme.color.grey[30]} !important;
                border-right: 1px solid ${(p) => p.theme.color.grey[30]} !important;
                pointer-events: none;
            }
        }

        .node-mention [data-node-view-wrapper] {
            position: relative;
            display: inline-block;
            text-decoration: none;
            background-color: white;
            color: ${(p) => p.theme.color.primary.main};
            padding: 0 3px;
            margin: 0 1px; // Make caret visible when focused before or after the mention

            /* Prevent  */
            &:before {
                content: '';
                position: absolute;
                top: 1px;
                bottom: 1px;
                left: 0;
                right: 0;
                border-radius: ${(p) => p.theme.radius.small};
                border: 1px solid ${(p) => p.theme.color.primary.main};
                pointer-events: none;
            }

            body:not(.mouse-user) &:focus {
                outline: none;

                &:before {
                    outline: ${(p) => p.theme.focusStyling};
                }
            }
        }

        .node-button {
            display: inline-block;
            vertical-align: middle;
        }

        > * + * {
            margin-top: 24px !important;
        }

        h2 {
            + * {
                margin-top: 16px !important;
            }

            + p,
            + ul,
            + ol {
                margin-top: 4px !important;
            }
        }

        h3,
        h4,
        h5 {
            + * {
                margin-top: 16px !important;
            }
        }

        h3 {
            + p,
            + ul,
            + ol {
                margin-top: 2px !important;
            }
        }

        h4,
        h5 {
            + p,
            + ul,
            + ol {
                margin-top: 0 !important;
            }
        }

        .paragraph_bold {
            font-weight: ${(p) => p.theme.font.weight.bold};
        }
    }
`
