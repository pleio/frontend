import React, { useRef } from 'react'
import { isTextSelection } from '@tiptap/core'
import { BubbleMenu as BubbleMenuExtension } from '@tiptap/react'

import Flexer from 'js/components/Flexer/Flexer'

import TextAnchor from './components/TextAnchor'
import TextCSSClass from './components/TextCSSClass'
import TextLanguage from './components/TextLanguage'
import TextLink from './components/TextLink'
import TextStyle from './components/TextStyle'

const BubbleMenu = ({
    editor,
    textStyle,
    textLink,
    textAnchor,
    textCSSClass,
    textLanguage,
}) => {
    const refWrapper = useRef()

    const handleKeyDown = (evt) => {
        if (evt.key === 'Tab') {
            const buttons = refWrapper.current.querySelectorAll(
                'button:not([disabled])',
            )
            const firstButton = buttons[0]
            const lastButton = buttons[buttons.length - 1]

            // Shift + Tab
            if (evt.shiftKey) {
                if (evt.target === firstButton) {
                    evt.preventDefault()
                    lastButton.focus()
                }
            }
            // Tab
            else if (evt.target === lastButton) {
                evt.preventDefault()
                firstButton.focus()
            }
        } else if (evt.key === 'Escape') {
            editor.view.focus()
        }
    }

    if (
        !editor ||
        (!textStyle &&
            !textLink &&
            !textAnchor &&
            !textCSSClass &&
            !textLanguage)
    ) {
        return null
    }

    return (
        <BubbleMenuExtension
            editor={editor}
            tippyOptions={{
                theme: 'popover-hidden',
                animation: 'fade',
                duration: [200, 150],
                arrow: false,
                placement: 'bottom',
            }}
            updateDelay={0}
            shouldShow={({ editor, view, state, from, to }) => {
                const { doc, selection } = state
                const { empty } = selection

                // Sometime check for `empty` is not enough.
                // Doubleclick an empty paragraph returns a node size of 2.
                // So we check also for an empty text size.
                const isEmptyTextBlock =
                    doc.textBetween(from, to).length === 0 ||
                    (!doc.textBetween(from, to).length &&
                        isTextSelection(selection))

                const hasEditorFocus = view.hasFocus()

                if (
                    editor.isActive('blockquote') ||
                    editor.isActive('button') ||
                    editor.isActive('code') ||
                    editor.isActive('codeBlock') ||
                    !hasEditorFocus ||
                    empty ||
                    isEmptyTextBlock ||
                    !editor.isEditable
                ) {
                    return false
                }

                return true
            }}
        >
            <Flexer
                ref={refWrapper}
                gutter="none"
                divider="normal"
                onKeyDown={handleKeyDown}
                style={{
                    overflow: 'hidden',
                }}
            >
                {textStyle && <TextStyle editor={editor} />}
                {(textLink || textAnchor) && (
                    <div style={{ display: 'flex' }}>
                        {textLink && <TextLink editor={editor} />}
                        {textAnchor && <TextAnchor editor={editor} />}
                    </div>
                )}
                {textLanguage && <TextLanguage editor={editor} />}
                {textCSSClass && <TextCSSClass editor={editor} />}
            </Flexer>
        </BubbleMenuExtension>
    )
}

export default BubbleMenu
