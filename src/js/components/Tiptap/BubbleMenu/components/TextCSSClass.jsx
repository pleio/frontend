import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import IconButton from 'js/components/IconButton/IconButton'
import Popover from 'js/components/Popover/Popover'
import ToolButton from 'js/components/Tiptap/components/ToolButton'

import CheckIcon from 'icons/check.svg'
import CSSIcon from 'icons/css.svg'
import DeleteIcon from 'icons/delete.svg'

import Wrapper from './SharedWrapper'

const TextCSSClass = ({ editor }) => {
    const { t } = useTranslation()

    const [instance, setInstance] = useState(null)

    const hide = () => instance.hide()

    const cssClass = editor.getAttributes('cssclass').class
    const hasCssClass = !!cssClass

    const defaultValues = {
        class: cssClass || '',
    }

    const { control, handleSubmit, setValue, setFocus } = useForm({
        defaultValues,
    })

    const submit = (values) => {
        if (values.class === '') {
            removeAnchor()
            return
        }

        editor
            .chain()
            .extendMarkRange('cssclass')
            .setMark('cssclass', { class: values.class })
            .run()

        hide()
    }

    const removeAnchor = () => {
        editor.chain().extendMarkRange('cssclass').unsetMark('cssclass').run()
        hide()
    }

    const onShow = () => {
        setValue('class', cssClass || '')
        setTimeout(() => {
            setFocus('class')
        }, 0)
    }

    return (
        <Popover
            onCreate={setInstance}
            onShow={onShow}
            onHide={() => editor.commands.focus()}
            content={
                <Wrapper
                    gutter="none"
                    divider="large"
                    as="form"
                    onSubmit={(evt) => {
                        evt.preventDefault()
                        handleSubmit(submit)()
                        // Prevent parent form from submitting
                        evt.stopPropagation()
                    }}
                >
                    <FormItem
                        valuePrefix="."
                        type="text"
                        name="class"
                        control={control}
                        placeholder={t('editor.cssclass-keyword')}
                        aria-label={t('editor.cssclass-keyword-helper')}
                        borderStyle="none"
                    />
                    <Flexer gutter="none" divider="normal">
                        <IconButton
                            size="large"
                            variant="primary"
                            radiusStyle="none"
                            type="submit"
                            tooltip={
                                hasCssClass
                                    ? t('action.save')
                                    : t('action.insert')
                            }
                        >
                            <CheckIcon />
                        </IconButton>
                        {hasCssClass && (
                            <IconButton
                                size="large"
                                variant="secondary"
                                radiusStyle="none"
                                onClick={removeAnchor}
                                tooltip={t('action.remove')}
                            >
                                <DeleteIcon />
                            </IconButton>
                        )}
                    </Flexer>
                </Wrapper>
            }
            appendTo={() => document.body} // So it can grow wider than its parent tippy
            overflowHidden
            arrow
            placement="bottom"
            offset={[0, 0]}
        >
            <ToolButton
                aria-pressed={hasCssClass}
                tooltip={
                    hasCssClass
                        ? t('editor.cssclass-edit')
                        : t('editor.cssclass-add')
                }
                tabIndex="0"
            >
                <CSSIcon />
            </ToolButton>
        </Popover>
    )
}
export default TextCSSClass
