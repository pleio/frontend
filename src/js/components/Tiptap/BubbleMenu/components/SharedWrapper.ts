import styled from 'styled-components'

import Flexer, { Props } from 'js/components/Flexer/Flexer'
import shouldForwardProp from 'js/lib/helpers/shouldForwardProp'

export default styled(Flexer).withConfig({
    shouldForwardProp: shouldForwardProp([
        'alignItems',
        'justifyContent',
        'mt',
        'wrap',
        'divider',
        'gutter',
    ]),
})<Props>`
    > * {
        flex-shrink: 0;
    }
`
