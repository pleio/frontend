import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import IconButton from 'js/components/IconButton/IconButton'
import Popover from 'js/components/Popover/Popover'
import ToolButton from 'js/components/Tiptap/components/ToolButton'

import AnchorIcon from 'icons/anchor.svg'
import CheckIcon from 'icons/check.svg'
import DeleteIcon from 'icons/delete.svg'

import Wrapper from './SharedWrapper'

const TextLink = ({ editor }) => {
    const { t } = useTranslation()

    const [instance, setInstance] = useState(null)

    const hide = () => instance.hide()

    const anchorId = editor.getAttributes('anchor').id
    const hasAnchorId = !!anchorId

    const defaultValues = {
        id: '',
    }

    const { control, handleSubmit, setValue, setFocus } = useForm({
        defaultValues,
    })

    const submit = ({ id }) => {
        if (id === '') {
            removeAnchor()
            return
        }

        editor.chain().extendMarkRange('anchor').setMark('anchor', { id }).run()

        hide()
    }

    const removeAnchor = () => {
        editor.chain().extendMarkRange('anchor').unsetMark('anchor').run()
        hide()
    }

    const onShow = () => {
        setValue('id', anchorId || '')
        setTimeout(() => {
            setFocus('id')
        }, 0)
    }

    return (
        <Popover
            onCreate={setInstance}
            onShow={onShow}
            onHide={() => editor.commands.focus()}
            content={
                <Wrapper
                    gutter="none"
                    divider="large"
                    as="form"
                    onSubmit={(evt) => {
                        evt.preventDefault()
                        handleSubmit(submit)()
                        // Prevent parent form from submitting
                        evt.stopPropagation()
                    }}
                >
                    <FormItem
                        valuePrefix="#"
                        type="text"
                        name="id"
                        control={control}
                        placeholder={t('editor.anchor-keyword')}
                        aria-label={t('editor.anchor-keyword-helper')}
                        borderStyle="none"
                    />
                    <Flexer gutter="none" divider="normal">
                        <IconButton
                            size="large"
                            variant="primary"
                            radiusStyle="none"
                            type="submit"
                            tooltip={
                                hasAnchorId
                                    ? t('action.save')
                                    : t('action.insert')
                            }
                        >
                            <CheckIcon />
                        </IconButton>
                        {hasAnchorId && (
                            <IconButton
                                size="large"
                                variant="secondary"
                                radiusStyle="none"
                                onClick={removeAnchor}
                                tooltip={t('action.remove')}
                            >
                                <DeleteIcon />
                            </IconButton>
                        )}
                    </Flexer>
                </Wrapper>
            }
            appendTo={() => document.body} // So it can grow wider than its parent tippy
            overflowHidden
            arrow
            placement="bottom"
            offset={[0, 0]}
        >
            <ToolButton
                disabled={editor.getAttributes('link').href}
                aria-pressed={hasAnchorId}
                tooltip={
                    hasAnchorId
                        ? t('editor.anchor-edit')
                        : t('editor.anchor-add')
                }
                tabIndex="0"
            >
                <AnchorIcon />
            </ToolButton>
        </Popover>
    )
}
export default TextLink
