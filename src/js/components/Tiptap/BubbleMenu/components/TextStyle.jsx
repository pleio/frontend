import React from 'react'
import { useTranslation } from 'react-i18next'

import ToolButton from 'js/components/Tiptap/components/ToolButton'

import BoldIcon from 'icons/bold.svg'
import ItalicIcon from 'icons/italic.svg'
import StrikeIcon from 'icons/strikethrough.svg'
import UnderlineIcon from 'icons/underline.svg'

const TextStyle = ({ editor }) => {
    const { t } = useTranslation()

    return (
        <div style={{ display: 'flex' }}>
            <ToolButton
                onClick={() => editor.chain().toggleBold().focus().run()}
                aria-pressed={editor && editor.isActive('bold')}
                tooltip={t('editor.bold')}
                tabIndex="0"
            >
                <BoldIcon />
            </ToolButton>
            <ToolButton
                onClick={() => editor.chain().toggleItalic().focus().run()}
                aria-pressed={editor && editor.isActive('italic')}
                tooltip={t('editor.italic')}
                tabIndex="0"
            >
                <ItalicIcon />
            </ToolButton>
            <ToolButton
                onClick={() => editor.chain().toggleUnderline().focus().run()}
                aria-pressed={editor && editor.isActive('underline')}
                tooltip={t('editor.underline')}
                tabIndex="0"
            >
                <UnderlineIcon />
            </ToolButton>
            <ToolButton
                onClick={() => editor.chain().toggleStrike().focus().run()}
                aria-pressed={editor && editor.isActive('strike')}
                tooltip={t('editor.strike')}
                tabIndex="0"
            >
                <StrikeIcon />
            </ToolButton>
        </div>
    )
}
export default TextStyle
