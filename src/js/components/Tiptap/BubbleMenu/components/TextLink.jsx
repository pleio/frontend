import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import IconButton from 'js/components/IconButton/IconButton'
import Popover from 'js/components/Popover/Popover'
import ToolButton from 'js/components/Tiptap/components/ToolButton'

import CheckIcon from 'icons/check.svg'
import DeleteIcon from 'icons/delete.svg'
import LinkIcon from 'icons/link.svg'

import Wrapper from './SharedWrapper'

const TextLink = ({ editor }) => {
    const { t } = useTranslation()

    const [instance, setInstance] = useState(null)

    const hide = () => instance.hide()

    const defaultValues = {
        linkUrl: '',
        linkNewWindow: false,
    }

    const { control, handleSubmit, setValue, setFocus } = useForm({
        mode: 'onChange',
        defaultValues,
    })

    const href = editor.getAttributes('link').href
    const hasHref = !!href

    const onShow = () => {
        setValue('linkUrl', href || '')
        setValue(
            'linkNewWindow',
            editor.getAttributes('link')?.target === '_blank',
        )
        setTimeout(() => {
            setFocus('linkUrl')
        }, 0)
    }

    const submit = ({ linkUrl, linkNewWindow }) => {
        if (linkUrl === '') {
            removeLink()
            return
        }

        editor
            .chain()
            .extendMarkRange('link')
            .setLink({
                href: linkUrl,
                target: linkNewWindow ? '_blank' : null,
            })
            .run()

        hide()
    }

    const removeLink = () => {
        hide()
        editor.chain().extendMarkRange('link').unsetLink().run()
    }

    return (
        <Popover
            onCreate={setInstance}
            onShow={onShow}
            onHide={() => editor.commands.focus()}
            content={
                <Wrapper
                    alignItems="stretch"
                    gutter="none"
                    divider="large"
                    as="form"
                    onSubmit={(evt) => {
                        evt.preventDefault()
                        handleSubmit(submit)()
                        // Prevent parent form from submitting
                        evt.stopPropagation()
                    }}
                >
                    <FormItem
                        name="linkUrl"
                        type="text"
                        control={control}
                        placeholder={t('form.link')}
                        borderStyle="none"
                    />
                    <FormItem
                        control={control}
                        type="checkbox"
                        name="linkNewWindow"
                        label={t('action.open-in-new-tab')}
                        size="small"
                        releaseInputArea
                        style={{
                            position: 'relative',
                            display: 'flex',
                            justifyContent: 'center',
                            padding: '0 12px',
                        }}
                    />
                    <Flexer gutter="none" divider="normal">
                        <IconButton
                            size="large"
                            variant="primary"
                            radiusStyle="none"
                            type="submit"
                            tooltip={
                                href ? t('action.save') : t('action.insert')
                            }
                        >
                            <CheckIcon />
                        </IconButton>
                        {hasHref && (
                            <IconButton
                                size="large"
                                variant="secondary"
                                radiusStyle="none"
                                onClick={removeLink}
                                tooltip={t('action.remove')}
                            >
                                <DeleteIcon />
                            </IconButton>
                        )}
                    </Flexer>
                </Wrapper>
            }
            appendTo={() => document.body} // So it can grow wider than its parent tippy
            overflowHidden
            placement="bottom"
            offset={[0, 0]}
        >
            <ToolButton
                disabled={editor.getAttributes('anchor').id}
                aria-pressed={hasHref}
                tooltip={hasHref ? t('editor.link-edit') : t('editor.link-add')}
                tabIndex="0"
            >
                <LinkIcon />
            </ToolButton>
        </Popover>
    )
}
export default TextLink
