import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import IconButton from 'js/components/IconButton/IconButton'
import Popover from 'js/components/Popover/Popover'
import ToolButton from 'js/components/Tiptap/components/ToolButton'

import CheckIcon from 'icons/check.svg'
import DeleteIcon from 'icons/delete.svg'
import LanguageIcon from 'icons/language.svg'

import Wrapper from './SharedWrapper'

const TextLanguage = ({ editor }) => {
    const { t } = useTranslation()

    const [instance, setInstance] = useState(null)

    const hide = () => instance.hide()

    const language = editor.getAttributes('lang').lang
    const hasLanguage = !!language

    const defaultValues = {
        lang: language || '',
    }

    const { control, handleSubmit, setValue, setFocus } = useForm({
        defaultValues,
    })

    const submit = ({ lang }) => {
        if (lang === '') {
            removeAnchor()
            return
        }

        editor.chain().extendMarkRange('lang').setMark('lang', { lang }).run()

        hide()
    }

    const removeAnchor = () => {
        editor.chain().extendMarkRange('lang').unsetMark('lang').run()
        hide()
    }
    const onShow = () => {
        setValue('lang', language || '')
        setTimeout(() => {
            setFocus('lang')
        }, 0)
    }

    const languageOptions = [
        {
            value: 'en',
            label: t('editor.text-language-english'),
        },
        {
            value: 'nl',
            label: t('editor.text-language-dutch'),
        },
        {
            value: 'de',
            label: t('editor.text-language-german'),
        },
        {
            value: 'fr',
            label: t('editor.text-language-french'),
        },
    ]

    return (
        <Popover
            onCreate={setInstance}
            onShow={onShow}
            onHide={() => editor.commands.focus()}
            content={
                <Wrapper
                    gutter="none"
                    divider="large"
                    as="form"
                    onSubmit={(evt) => {
                        evt.preventDefault()
                        handleSubmit(submit)()
                        // Prevent parent form from submitting
                        evt.stopPropagation()
                    }}
                >
                    <FormItem
                        type="select"
                        name="lang"
                        options={languageOptions}
                        control={control}
                        placeholder={t('editor.text-language-keyword')}
                        aria-label={t('editor.text-language-keyword-helper')}
                        borderStyle="none"
                    />
                    <Flexer gutter="none" divider="normal">
                        <IconButton
                            size="large"
                            variant="primary"
                            radiusStyle="none"
                            type="submit"
                            tooltip={
                                hasLanguage
                                    ? t('action.save')
                                    : t('action.insert')
                            }
                        >
                            <CheckIcon />
                        </IconButton>
                        {hasLanguage && (
                            <IconButton
                                size="large"
                                variant="secondary"
                                radiusStyle="none"
                                onClick={removeAnchor}
                                tooltip={t('action.remove')}
                            >
                                <DeleteIcon />
                            </IconButton>
                        )}
                    </Flexer>
                </Wrapper>
            }
            appendTo={() => document.body} // So it can grow wider than its parent tippy
            overflowVisible
            arrow
            placement="bottom"
            offset={[0, 0]}
        >
            <ToolButton
                aria-pressed={hasLanguage}
                tooltip={
                    hasLanguage
                        ? t('editor.text-language-edit')
                        : t('editor.text-language-add')
                }
                tabIndex="0"
            >
                <LanguageIcon />
            </ToolButton>
        </Popover>
    )
}
export default TextLanguage
