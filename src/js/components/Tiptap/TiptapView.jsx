import React, { useEffect, useState } from 'react'
import { Editor, EditorContent } from '@tiptap/react'
import { hasJsonStructure } from 'helpers'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

import getExtensions from './getExtensions'
import useAccordionOpener from './hooks/useAccordionOpener'

const Wrapper = styled.div`
    /* Add specificity to be sure style is overwritten */
    .ProseMirror.ProseMirror {
        ${(p) =>
            p.$textSize === 'small' &&
            css`
                font-size: ${(p) => p.theme.font.size.small};
                line-height: ${(p) => p.theme.font.lineHeight.small};
            `};
        ${(p) =>
            p.$textSize === 'normal' &&
            css`
                font-size: ${(p) => p.theme.font.size.normal};
                line-height: ${(p) => p.theme.font.lineHeight.normal};
            `};
        ${(p) =>
            p.$textSize === 'large' &&
            css`
                font-size: ${(p) => p.theme.font.size.large};
                line-height: ${(p) => p.theme.font.lineHeight.large};
            `};

        ${(p) =>
            p.$dropCaps &&
            css`
                > p:first-of-type:first-letter {
                    initial-letter: 2;
                    margin-top: 1px;
                    padding-right: 6px;
                }
            `};
    }
`

const Tiptap = ({
    content,
    contentType = 'json',
    textSize = 'normal',
    dropCaps,
    ...rest
}) => {
    const wrapperRef = React.useRef(null)
    const editorRef = React.useRef({})

    useAccordionOpener(wrapperRef?.current, editorRef?.current)

    const [editor, setEditor] = useState()

    useEffect(() => {
        const instance = new Editor({
            editable: false,
            editorProps: {
                attributes: {
                    tabindex: -1,
                    Translate: 'yes', // We use PascalCased "Translate" to trick the TiptapView to override the default "translate"
                    role: 'none', // Overwrite the default role of "textbox" to prevent screen readers from reading the content
                },
            },
            extensions: [
                ...getExtensions({
                    placeholder: '',
                    textStyle: true,
                    textLink: true,
                    textAnchor: true,
                    textCSSClass: true,
                    textQuote: true,
                    textList: true,
                    insertMedia: true,
                    insertAccordion: true,
                    insertButton: true,
                    insertTable: true,
                    insertMention: true,
                    textLanguage: true,
                }),
            ],
            content:
                contentType === 'json'
                    ? hasJsonStructure(content)
                        ? JSON.parse(content)
                        : content
                    : content,
            onCreate() {
                editorRef.current = true
                const { hash } = window.location
                if (hash) {
                    setTimeout(() => {
                        // querySelector(hash) causes error when hash is a number
                        const hashElement = document.getElementById(
                            hash.substring(1),
                        )
                        const hashElementTop =
                            hashElement?.getBoundingClientRect()?.top
                        if (hashElementTop > 0) {
                            window.scrollTo({
                                top: hashElementTop,
                            })
                        }
                    }, 0)
                }
            },
        })

        setEditor(instance)

        return () => {
            instance.destroy()
        }
    }, [content, contentType])

    return content ? (
        <Wrapper
            $textSize={textSize}
            $dropCaps={dropCaps}
            {...rest}
            ref={wrapperRef}
        >
            <EditorContent editor={editor} />
        </Wrapper>
    ) : null
}

Tiptap.contextTypes = {
    contentType: PropTypes.oneOf(['json', 'html']),
    textSize: PropTypes.oneOf(['small', 'normal', 'large']),
}

export default Tiptap
