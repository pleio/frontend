import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'

import ToolButton from 'js/components/Tiptap/components/ToolButton'
import UploadOrSelectFilesModal from 'js/files/UploadOrSelectFiles/UploadOrSelectFilesModal'
import { MAX_FILE_SIZE } from 'js/lib/constants'

import FileIcon from 'icons/file.svg'

const InsertFile = ({ editor }) => {
    const [showModal, setShowModal] = useState(false)

    const { t } = useTranslation()

    const handleComplete = (files) => {
        const newFiles = files.map(
            ({ guid, title, subtype, mimeType, url, size }) => {
                return {
                    type: 'file',
                    attrs: {
                        guid,
                        name: title,
                        subtype,
                        mimeType,
                        url,
                        size,
                    },
                }
            },
        )
        editor.chain().selectTextblockEnd().insertContent(newFiles).run()
    }

    const disabled = editor?.isActive('table')

    return (
        <>
            <ToolButton
                disabled={disabled}
                tooltip={t('editor.insert-file-s')}
                onClick={() => setShowModal(true)}
            >
                <FileIcon />
            </ToolButton>
            <UploadOrSelectFilesModal
                title={t('editor.insert-file-s')}
                maxSize={MAX_FILE_SIZE.attachment}
                showModal={showModal}
                setShowModal={setShowModal}
                onComplete={handleComplete}
                multiple
            />
        </>
    )
}

export default InsertFile
