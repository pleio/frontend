import React from 'react'
import { useTranslation } from 'react-i18next'

import ToolButton from 'js/components/Tiptap/components/ToolButton'

import AccordionIcon from 'icons/accordion.svg'

const InsertAccordion = ({ editor }) => {
    const { t } = useTranslation()

    const insertDetails = () => {
        editor.chain().toggleWrap('accordion').focus().run()
    }

    const disabled = editor?.isActive('table')

    return (
        <ToolButton
            disabled={disabled}
            onClick={insertDetails}
            aria-pressed={editor && editor.isActive('accordion')}
            tooltip={t('editor.insert-accordion')}
        >
            <AccordionIcon />
        </ToolButton>
    )
}
export default InsertAccordion
