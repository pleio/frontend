import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'

import ToolButton from 'js/components/Tiptap/components/ToolButton'
import UploadOrSelectFilesModal from 'js/files/UploadOrSelectFiles/UploadOrSelectFilesModal'

import ImageIcon from 'icons/image.svg'

const InsertImage = ({ editor }) => {
    const [showModal, setShowModal] = useState(false)

    const { t } = useTranslation()

    const handleComplete = (files) => {
        const images = files.map(({ download }) => {
            return {
                type: 'image',
                attrs: {
                    src: download,
                },
            }
        })

        editor.chain().selectTextblockEnd().insertContent(images).run()
    }

    const disabled = editor?.isActive('table')

    return (
        <>
            <ToolButton
                disabled={disabled}
                tooltip={t('editor.insert-image-s')}
                onClick={() => setShowModal(true)}
            >
                <ImageIcon />
            </ToolButton>
            <UploadOrSelectFilesModal
                title={t('editor.insert-image-s')}
                accept="image"
                showModal={showModal}
                setShowModal={setShowModal}
                onComplete={handleComplete}
                multiple
            />
        </>
    )
}

export default InsertImage
