import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { getVideoFromUrl } from 'helpers'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import Popover from 'js/components/Popover/Popover'
import Spacer from 'js/components/Spacer/Spacer'
import ToolButton from 'js/components/Tiptap/components/ToolButton'

import VideoIcon from 'icons/video.svg'

const InsertVideo = ({ editor }) => {
    const { t } = useTranslation()

    const [instance, setInstance] = useState(null)

    const { kalturaVideoEnabled, kalturaVideoPartnerId, kalturaVideoPlayerId } =
        window.__SETTINGS__
    const kalturaEnabled =
        !!kalturaVideoEnabled &&
        !!kalturaVideoPartnerId &&
        !!kalturaVideoPlayerId

    const videoValidationPattern = kalturaEnabled
        ? /((?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/(?:[^/]*)\/videos\/|album\/(?:\d+)\/video\/|video\/|)(\d+)(?:[a-zA-Z0-9_-]+)?)|(^(https?:\/\/)?((www\.)?(youtube(-nocookie)?|youtube.googleapis)\.com.*(v\/|v=|vi=|vi\/|e\/|embed\/|user\/.*\/u\/\d+\/)|youtu\.be\/)([_0-9a-z-]+)|(https?:\/\/videoleren.nvwa.nl(.*?)(?="|$)))/i
        : /((?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/(?:[^/]*)\/videos\/|album\/(?:\d+)\/video\/|video\/|)(\d+)(?:[a-zA-Z0-9_-]+)?)|(^(https?:\/\/)?((www\.)?(youtube(-nocookie)?|youtube.googleapis)\.com.*(v\/|v=|vi=|vi\/|e\/|embed\/|user\/.*\/u\/\d+\/)|youtu\.be\/)([_0-9a-z-]+))/i

    const videoHelper = kalturaEnabled
        ? t('action.insert-video-helper-kaltura')
        : t('action.insert-video-helper')

    const hide = () => {
        instance.hide()
    }

    const submit = ({ url }) => {
        const video = getVideoFromUrl(url)

        if (!video) return

        hide()
        editor
            .chain()
            .insertContent({
                type: 'video',
                attrs: {
                    platform: video.type,
                    guid: video.id,
                },
            })
            .focus()
            .run()
        reset()
    }

    const defaultValues = {
        url: '',
    }

    const {
        control,
        handleSubmit,
        reset,
        formState: { isValid },
    } = useForm({
        mode: 'onChange',
        defaultValues,
    })

    const disabled = editor?.isActive('table')

    return (
        <div>
            <Popover
                onCreate={setInstance}
                content={
                    <form
                        style={{ padding: '12px 12px 8px' }}
                        onSubmit={(evt) => {
                            evt.preventDefault()
                            handleSubmit(submit)()
                            // Prevent parent form from submitting
                            evt.stopPropagation()
                        }}
                        noValidate
                    >
                        <Spacer spacing="tiny">
                            <FormItem
                                type="text"
                                control={control}
                                name="url"
                                label={t('form.url')}
                                helper={videoHelper}
                                required
                                rules={{
                                    pattern: {
                                        value: videoValidationPattern,
                                        message: t('error.invalid-video-url'),
                                    },
                                }}
                            />

                            <Flexer>
                                <Button
                                    size="normal"
                                    variant="primary"
                                    type="submit"
                                    disabled={!isValid}
                                >
                                    {t('action.insert')}
                                </Button>
                            </Flexer>
                        </Spacer>
                    </form>
                }
                offset={[0, 1]}
            >
                <ToolButton
                    disabled={disabled}
                    tooltip={t('action.insert-video')}
                >
                    <VideoIcon />
                </ToolButton>
            </Popover>
        </div>
    )
}

export default InsertVideo
