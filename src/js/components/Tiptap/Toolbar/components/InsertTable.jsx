import React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import DropdownButton from 'js/components/DropdownButton/DropdownButton'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import Popover from 'js/components/Popover/Popover'
import Spacer from 'js/components/Spacer/Spacer'
import ToolButton from 'js/components/Tiptap/components/ToolButton'

import TableIcon from 'icons/table.svg'

const InsertTable = ({ editor }) => {
    const { t } = useTranslation()

    const defaultValues = {
        withHeaderRow: true,
        rows: 3,
        cols: 3,
    }

    const {
        control,
        handleSubmit,
        formState: { isValid },
    } = useForm({
        mode: 'onChange',
        defaultValues,
    })

    const insertTable = ({ rows, cols, withHeaderRow }) => {
        editor
            .chain()
            .focus()
            .insertTable({
                rows,
                cols,
                withHeaderRow,
            })
            .run()
    }

    if (editor?.isActive('table')) {
        const options = [
            [
                {
                    onClick: () => editor.commands.addRowBefore(),
                    name: t('editor.add-row-before'),
                },
                {
                    onClick: () => editor.commands.addRowAfter(),
                    name: t('editor.add-row-after'),
                },
                {
                    onClick: () => editor.commands.addColumnBefore(),
                    name: t('editor.add-column-before'),
                },
                {
                    onClick: () => editor.commands.addColumnAfter(),
                    name: t('editor.add-column-after'),
                },
                ...(editor.can().mergeCells()
                    ? [
                          {
                              onClick: () => editor.commands.mergeCells(),
                              name: t('editor.merge-cells'),
                          },
                      ]
                    : []),
                ...(editor.can().splitCell()
                    ? [
                          {
                              onClick: () => editor.commands.splitCell(),
                              name: t('editor.split-cell'),
                          },
                      ]
                    : []),
            ],
            [
                {
                    onClick: () => editor.commands.toggleHeaderRow(),
                    name: t('editor.toggle-header-row'),
                },
                {
                    onClick: () => editor.commands.toggleHeaderColumn(),
                    name: t('editor.toggle-header-column'),
                },
            ],
            [
                {
                    onClick: () => editor.commands.deleteRow(),
                    name: t('editor.delete-row'),
                },
                {
                    onClick: () => editor.commands.deleteColumn(),
                    name: t('editor.delete-column'),
                },
                {
                    onClick: () => editor.commands.deleteTable(),
                    name: t('editor.delete-table'),
                },
            ],
        ]

        return (
            <DropdownButton options={options} showArrow closeAfterClick={false}>
                <ToolButton aria-pressed aria-label={t('editor.delete-row')}>
                    <TableIcon style={{ marginRight: '4px' }} />
                </ToolButton>
            </DropdownButton>
        )
    }

    return (
        <div>
            <Popover
                content={
                    <form
                        style={{ width: '100%', padding: '12px 12px 8px' }}
                        onSubmit={(evt) => {
                            evt.preventDefault()
                            handleSubmit(insertTable)()
                            // Prevent parent form from submitting
                            evt.stopPropagation()
                        }}
                    >
                        <Spacer spacing="tiny">
                            <Flexer gutter="small">
                                <FormItem
                                    name="rows"
                                    type="number"
                                    min={1}
                                    control={control}
                                    label={t('editor.rows')}
                                    required
                                    style={{
                                        width: '90px',
                                    }}
                                />
                                <FormItem
                                    name="cols"
                                    type="number"
                                    min={1}
                                    control={control}
                                    label={t('editor.columns')}
                                    required
                                    style={{
                                        width: '90px',
                                    }}
                                />
                            </Flexer>
                            <div>
                                <FormItem
                                    control={control}
                                    type="checkbox"
                                    name="withHeaderRow"
                                    label={t('editor.show-header')}
                                    size="small"
                                />
                            </div>
                            <Flexer>
                                <Button
                                    size="normal"
                                    variant="primary"
                                    type="submit"
                                    aria-label={t('action.insert')}
                                    disabled={!isValid}
                                >
                                    {t('action.insert')}
                                </Button>
                            </Flexer>
                        </Spacer>
                    </form>
                }
                offset={[0, 1]}
            >
                <ToolButton
                    disabled={editor?.isActive('accordion')}
                    tooltip={t('editor.insert-table')}
                >
                    <TableIcon />
                </ToolButton>
            </Popover>
        </div>
    )
}
export default InsertTable
