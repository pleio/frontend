import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import Popover from 'js/components/Popover/Popover'
import Spacer from 'js/components/Spacer/Spacer'
import ToolButton from 'js/components/Tiptap/components/ToolButton'

import ButtonIcon from 'icons/button.svg'

const InsertButton = ({ editor }) => {
    const { t } = useTranslation()

    const [instance, setInstance] = useState(null)

    const hide = () => instance.hide()

    const isButtonSelected = editor?.isActive('button')

    const url = editor?.getAttributes('button')?.url || ''

    const defaultValues = { url }

    const {
        control,
        handleSubmit,
        setValue,
        reset,
        formState: { isValid },
    } = useForm({
        mode: 'onChange',
        defaultValues,
    })

    useEffect(() => {
        setValue('url', url)
    }, [url, setValue])

    const submit = ({ url }) => {
        hide()
        const { view, state } = editor
        const { from, to } = view.state.selection
        const text = state.doc.textBetween(from, to, '')
        editor
            .chain()
            .deleteNode('button')
            .insertContent({
                type: 'button',
                content: [
                    {
                        type: 'text',
                        text: text || t('form.button-text'),
                    },
                ],
                attrs: {
                    url,
                },
            })
            .focus()
            .run()

        reset()
    }

    const handleDelete = () => {
        hide()
        editor.chain().deleteNode('button').focus().run()
    }

    return (
        <Popover
            onCreate={setInstance}
            content={
                <form
                    style={{ padding: '12px 12px 8px' }}
                    onSubmit={(evt) => {
                        evt.preventDefault()
                        handleSubmit(submit)()
                        evt.stopPropagation()
                    }}
                    noValidate
                >
                    <Spacer spacing="tiny">
                        <FormItem
                            control={control}
                            type="text"
                            name="url"
                            label={t('form.url')}
                            required
                            autoComplete="off"
                        />

                        <Flexer>
                            {isButtonSelected && (
                                <Button
                                    size="normal"
                                    variant="secondary"
                                    onClick={handleDelete}
                                >
                                    {t('action.delete')}
                                </Button>
                            )}
                            <Button
                                size="normal"
                                variant="primary"
                                type="submit"
                                disabled={!isValid}
                            >
                                {isButtonSelected
                                    ? t('action.save')
                                    : t('action.insert')}
                            </Button>
                        </Flexer>
                    </Spacer>
                </form>
            }
            offset={[0, 1]}
        >
            <div>
                <ToolButton
                    aria-pressed={isButtonSelected}
                    tooltip={
                        isButtonSelected
                            ? t('action.edit-button')
                            : t('action.insert-button')
                    }
                >
                    <ButtonIcon />
                </ToolButton>
            </div>
        </Popover>
    )
}

export default InsertButton
