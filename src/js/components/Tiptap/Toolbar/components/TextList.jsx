import React from 'react'
import { useTranslation } from 'react-i18next'

import ToolButton from 'js/components/Tiptap/components/ToolButton'

import OlIcon from 'icons/ol.svg'
import UlIcon from 'icons/ul.svg'

const TextList = ({ editor }) => {
    const { t } = useTranslation()

    return (
        <ul className="TiptapEditorToolGroup">
            <li>
                <ToolButton
                    onClick={() =>
                        editor.chain().focus().toggleBulletList().run()
                    }
                    aria-pressed={editor && editor.isActive('bulletList')}
                    tooltip={t('editor.ul')}
                >
                    <UlIcon />
                </ToolButton>
            </li>
            <li>
                <ToolButton
                    onClick={() =>
                        editor.chain().focus().toggleOrderedList().run()
                    }
                    aria-pressed={editor && editor.isActive('orderedList')}
                    tooltip={t('editor.ol')}
                >
                    <OlIcon />
                </ToolButton>
            </li>
        </ul>
    )
}
export default TextList
