import React from 'react'
import { useTranslation } from 'react-i18next'

import Select from 'js/components/Select/Select'

const TextFormat = ({ editor }) => {
    const { t } = useTranslation()

    const onChange = (val) => {
        if (val) {
            if (val === 1) {
                editor.chain().focus().setParagraphBold().run()
            } else {
                editor.chain().focus().setHeading({ level: val }).run()
            }
        } else {
            editor.chain().focus().clearNodes().run()
        }
    }

    return (
        <div
            className="TiptapEditorToolGroup TextFormat"
            style={{ width: '140px' }}
        >
            <Select
                tabIndex="-1"
                name="editor-format"
                aria-label={t('editor.text-format')}
                options={[
                    {
                        value: 0,
                        label: t('editor.text-format-paragraph'),
                    },
                    {
                        value: 1,
                        label: t('editor.text-format-paragraph-bold'),
                    },
                    ...(editor && editor.isActive('blockquote')
                        ? []
                        : [
                              {
                                  value: 2,
                                  label: t('editor.text-format-h2'),
                              },
                              {
                                  value: 3,
                                  label: t('editor.text-format-h3'),
                              },
                              {
                                  value: 4,
                                  label: t('editor.text-format-h4'),
                              },
                              {
                                  value: 5,
                                  label: t('editor.text-format-h5'),
                              },
                          ]),
                ]}
                onChange={onChange}
                value={
                    editor && editor.isActive('paragraph_bold')
                        ? 1
                        : editor && editor.isActive('heading', { level: 2 })
                          ? 2
                          : editor && editor.isActive('heading', { level: 3 })
                            ? 3
                            : editor && editor.isActive('heading', { level: 4 })
                              ? 4
                              : editor &&
                                  editor.isActive('heading', { level: 5 })
                                ? 5
                                : 0
                }
                borderStyle="none"
            />
        </div>
    )
}
export default TextFormat
