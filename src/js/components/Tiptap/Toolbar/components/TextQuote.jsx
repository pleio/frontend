import React from 'react'
import { useTranslation } from 'react-i18next'

import ToolButton from 'js/components/Tiptap/components/ToolButton'

import QuoteIcon from 'icons/quote.svg'

const TextQuote = ({ editor }) => {
    const { t } = useTranslation()

    const disabled = editor?.isActive('table')

    return (
        <ToolButton
            disabled={disabled}
            onClick={() => editor.chain().focus().toggleBlockquote().run()}
            aria-pressed={editor && editor.isActive('blockquote')}
            tooltip={t('editor.blockquote')}
        >
            <QuoteIcon />
        </ToolButton>
    )
}
export default TextQuote
