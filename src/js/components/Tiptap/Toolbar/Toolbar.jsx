import React, { forwardRef } from 'react'

import Sticky from 'js/components/Sticky/Sticky'

import InsertAccordion from './components/InsertAccordion'
import InsertButton from './components/InsertButton'
import InsertFile from './components/InsertFile'
import InsertImage from './components/InsertImage'
import InsertTable from './components/InsertTable'
import InsertVideo from './components/InsertVideo'
import TextFormat from './components/TextFormat'
import TextList from './components/TextList'
import TextQuote from './components/TextQuote'

const Toolbar = forwardRef(
    (
        {
            editor,
            containerElement,
            contentGuid,
            textFormat,
            textQuote,
            textList,
            insertButton,
            insertMedia,
            insertTable,
            insertAccordion,
            isVisible,
        },
        ref,
    ) => {
        const toolbar = ref.current

        const resetTabindexes = () => {
            toolbar.querySelectorAll('[tabindex="0"]').forEach((el) => {
                el.setAttribute('tabindex', '-1')
            })
        }

        const returnFocusToEditor = () => {
            editor.commands.focus()
        }

        const handleKeyDown = (evt) => {
            if (evt.key === 'Tab') {
                const buttons = toolbar.querySelectorAll(
                    'button:not([disabled])',
                )
                const firstButton = buttons[0]
                const lastButton = buttons[buttons.length - 1]

                // Shift + Tab
                if (evt.shiftKey) {
                    if (evt.target === firstButton) {
                        evt.preventDefault()
                        lastButton.focus()
                    }
                }
                // Tab
                else if (evt.target === lastButton) {
                    evt.preventDefault()
                    firstButton.focus()
                }
            }
            if (evt.key === 'Escape') {
                resetTabindexes()
                returnFocusToEditor()
            }
        }

        const handleFocus = (evt) => {
            // Don't fire when child element gets focused
            if (evt.target === ref.current) {
                evt.preventDefault()

                // Prevent focus on wrapper any further
                toolbar.setAttribute('tabindex', '-1')

                // Make buttons focusable
                const buttons = toolbar.querySelectorAll('[tabindex="-1"]')
                buttons.forEach((el) => {
                    el.setAttribute('tabindex', '0')
                })

                // Focus first button
                const firstFocusable = toolbar.querySelector(
                    '[tabindex="0"]:not([disabled])',
                )
                if (firstFocusable) {
                    firstFocusable.focus()
                } else {
                    // There are no focusable buttons
                    returnFocusToEditor()
                }
            }
        }

        if (!isVisible) return null

        return (
            <Sticky
                className="TiptapEditorToolbar"
                unstickElement={containerElement}
            >
                {/* eslint-disable-next-line */}
                <div
                    className="TiptapEditorTools"
                    ref={ref}
                    onFocus={handleFocus}
                    onKeyDown={handleKeyDown}
                >
                    {textFormat && <TextFormat editor={editor} />}

                    {textQuote && (
                        <ul className="TiptapEditorToolGroup">
                            {textQuote && (
                                <li>
                                    <TextQuote editor={editor} />
                                </li>
                            )}
                        </ul>
                    )}

                    {textList && <TextList editor={editor} />}

                    {insertMedia && (
                        <ul className="TiptapEditorToolGroup">
                            <li>
                                <InsertImage
                                    editor={editor}
                                    contentGuid={contentGuid}
                                />
                            </li>
                            <li>
                                <InsertVideo editor={editor} />
                            </li>
                            <li>
                                <InsertFile
                                    editor={editor}
                                    contentGuid={contentGuid}
                                />
                            </li>
                        </ul>
                    )}

                    {(insertTable || insertAccordion || insertButton) && (
                        <ul className="TiptapEditorToolGroup">
                            {insertTable && (
                                <li>
                                    <InsertTable editor={editor} />
                                </li>
                            )}
                            {insertAccordion && (
                                <li>
                                    <InsertAccordion editor={editor} />
                                </li>
                            )}
                            {insertButton && (
                                <li>
                                    <InsertButton editor={editor} />
                                </li>
                            )}
                        </ul>
                    )}
                </div>
            </Sticky>
        )
    },
)

Toolbar.displayName = 'Toolbar'

export default Toolbar
