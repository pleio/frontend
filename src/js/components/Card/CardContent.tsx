import { media } from 'helpers'
import styled from 'styled-components'

const CardContent = styled.div`
    position: relative;

    ${media.mobileLandscapeDown`
        padding: ${(p: any) =>
            `${p.theme.padding.vertical.small} ${p.theme.padding.horizontal.small}`};
    `}

    ${media.tabletUp`
        padding: ${(p: any) =>
            `${p.theme.padding.vertical.normal} ${p.theme.padding.horizontal.normal}`};
    `}
`

export default CardContent
