import React from 'react'
import styled from 'styled-components'

import shouldForwardProp from 'js/lib/helpers/shouldForwardProp'

export interface Props {
    radiusSize?: 'small' | 'normal' | 'large'
    children: React.ReactElement<any> | string
}

export default styled.div.withConfig({
    shouldForwardProp: shouldForwardProp(['radiusSize']),
})<Props>`
    display: block;
    position: relative;
    width: 100%;
    color: ${(p) => p.theme.color.text.black};
    background-color: ${(p) => p.theme.card.bg};
    border-radius: ${(p) => p.theme.radius[p.radiusSize || 'normal']};
    box-shadow: ${(p) => p.theme.card.shadow};
`

export { default as CardContent } from './CardContent'
