import React from 'react'
import { useTranslation } from 'react-i18next'
import { NavLink } from 'react-router-dom'
import styled, { css } from 'styled-components'

import DropdownButton from 'js/components/DropdownButton/DropdownButton'
import LinkItem from 'js/components/LinkItem/LinkItem'
import ListItem from 'js/components/ListItem/ListItem'

import ChevronRightIcon from 'icons/chevron-right-small.svg'
import DownloadIcon from 'icons/download-small.svg'
import ExternalIcon from 'icons/new-window-small.svg'

const Wrapper = styled.li`
    ${(p) =>
        p.$showDivider &&
        css`
            border-top: 1px solid ${(p) => p.theme.menu.divider};
        `};

    ${(p) =>
        p.$isFirstItem &&
        css`
            padding-top: 6px;
        `};
    ${(p) =>
        p.$isLastItem &&
        css`
            padding-bottom: 6px;
        `}
`

const IconWrapper = styled.span`
    margin-left: auto;
    padding-left: 8px;

    // Visually center different sized icons
    width: 12px;
    display: flex;
    justify-content: center;
`

const DropdownButtonListItem = ({
    entity,
    onHide,
    wrapContent,
    closeAfterClick,
    showDivider,
    isFirstItem,
    isLastItem,
}) => {
    const handleClickButton = (evt, option) => {
        option.onClick(evt)
        closeAfterClick && onHide()
    }

    const handleClickLink = () => {
        closeAfterClick && onHide()
    }

    const { t } = useTranslation()

    const {
        subOptions,
        name,
        label,
        url,
        onClick,
        to,
        state,
        href,
        target,
        active,
        download,
        warn,
    } = entity

    const newWindow = target === '_blank'
    const props = {
        role: 'menuitem',
        $showDivider: showDivider,
        $isFirstItem: isFirstItem,
        $isLastItem: isLastItem,
    }

    if (subOptions?.length > 0) {
        return (
            <Wrapper {...props}>
                <DropdownButton
                    options={subOptions}
                    trigger="click mouseenter"
                    placement="right-start"
                    wrapContent={wrapContent}
                    aria-expanded={true}
                    offset={[-6, 0]}
                >
                    <ListItem
                        entity={entity}
                        as="button"
                        type="button"
                        aria-label={name}
                        style={{ justifyContent: 'space-between' }}
                    >
                        {name}
                        <IconWrapper>
                            <ChevronRightIcon />
                        </IconWrapper>
                    </ListItem>
                </DropdownButton>
            </Wrapper>
        )
    } else if (onClick) {
        return (
            <Wrapper {...props}>
                <ListItem
                    as="button"
                    type="button"
                    aria-selected={!!active}
                    aria-label={label}
                    disabled={!!active}
                    $nowrap={!wrapContent || null}
                    $warn={warn}
                    onClick={(evt) => handleClickButton(evt, entity)}
                >
                    {name}
                </ListItem>
            </Wrapper>
        )
    } else if (to) {
        return (
            <Wrapper {...props}>
                <ListItem
                    as={NavLink}
                    to={to}
                    state={state}
                    aria-label={label}
                    end
                    $nowrap={!wrapContent || null}
                    $warn={warn}
                    onClick={handleClickLink}
                >
                    {name}
                </ListItem>
            </Wrapper>
        )
    } else if (href) {
        return (
            <Wrapper {...props}>
                <ListItem
                    as="a"
                    href={href}
                    target={target}
                    $nowrap={!wrapContent || null}
                    onClick={handleClickLink}
                    aria-label={`${label || name}${
                        newWindow ? t('global.opens-in-new-window') : ''
                    }`}
                    $warn={warn}
                    download={download}
                >
                    {name}
                    {newWindow ? (
                        <IconWrapper>
                            <ExternalIcon />
                        </IconWrapper>
                    ) : download ? (
                        <IconWrapper>
                            <DownloadIcon />
                        </IconWrapper>
                    ) : null}
                </ListItem>
            </Wrapper>
        )
    } else if (url) {
        return (
            <Wrapper {...props}>
                <ListItem
                    as={LinkItem}
                    url={url}
                    $warn={warn}
                    $nowrap={!wrapContent || null}
                >
                    {name}
                </ListItem>
            </Wrapper>
        )
    } else {
        return null
    }
}

export default DropdownButtonListItem
