import React, { useMemo } from 'react'

import { ResizeMeasure } from 'js/components/ResizeMeasure'

import DropdownButtonListItem from './DropdownButtonListItem'

export default function DropdownButtonList({
    options,
    onHide,
    wrapContent,
    closeAfterClick,
}) {
    const ListItem = (props) => {
        return useMemo(
            () => (
                <DropdownButtonListItem
                    {...props}
                    onHide={onHide}
                    wrapContent={wrapContent}
                />
            ),
            [props],
        )
    }

    return (
        <ResizeMeasure as="ul" role="menu" options={options}>
            {Array.isArray(options[0])
                ? options.map((array, arrayIndex) => {
                      return array.map((option, index) => {
                          const key = `${arrayIndex}-${index}`
                          return (
                              <ListItem
                                  key={key}
                                  entity={option}
                                  isFirstItem={index === 0}
                                  isLastItem={index + 1 === array.length}
                                  showDivider={arrayIndex > 0 && index === 0}
                                  closeAfterClick={
                                      option.disableCloseAfterClick === true
                                          ? false
                                          : closeAfterClick
                                  }
                              />
                          )
                      })
                  })
                : options.map((option, index) => {
                      return (
                          <ListItem
                              key={index}
                              entity={option}
                              isFirstItem={index === 0}
                              isLastItem={index + 1 === options.length}
                              closeAfterClick={
                                  option.disableCloseAfterClick === true
                                      ? false
                                      : closeAfterClick
                              }
                          />
                      )
                  })}
        </ResizeMeasure>
    )
}
