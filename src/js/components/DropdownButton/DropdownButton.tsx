import React, { Fragment, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { NavLink } from 'react-router-dom'
import { Placement } from '@popperjs/core'
import styled from 'styled-components'

import HideVisually from 'js/components/HideVisually/HideVisually'
import Popover from 'js/components/Popover/Popover'

import ChevronDownIcon from 'icons/chevron-down-small.svg'

import DropdownButtonList from './DropdownButtonList'

const DefaultWrapper = styled.div<{ $hasExtraOptions: boolean }>`
    display: inline-flex;

    .DropdownDefaultButton,
    .DropdownDefaultButtonOptions {
        display: flex;
        align-items: center;
        height: 32px;
        color: white;
        outline-offset: 1px;
        background-color: ${(p) => p.theme.color.secondary.main};

        &:focus {
            z-index: 1;
        }

        &:hover {
            background-color: ${(p) => p.theme.color.secondary.hover};
        }

        &:active {
            background-color: ${(p) => p.theme.color.secondary.active};
        }
    }

    .DropdownDefaultButton {
        padding: 0 12px;
        font-family: ${(p) => p.theme.font.family};
        font-size: ${(p) => p.theme.font.size.small};
        font-weight: ${(p) => p.theme.font.weight.semibold};
        border-radius: ${(p) =>
            p.$hasExtraOptions
                ? `${p.theme.radius.normal} 0 0
            ${p.theme.radius.normal}`
                : p.theme.radius.normal};

        &:hover {
            background-color: ${(p) => p.theme.color.secondary.hover};
        }

        &:active {
            background-color: ${(p) => p.theme.color.secondary.active};
        }
    }

    .DropdownDefaultButtonOptions {
        position: relative;
        justify-content: center;
        width: 32px;
        border-radius: 0 ${(p) => p.theme.radius.normal}
            ${(p) => p.theme.radius.normal} 0;

        &:before {
            content: '';
            position: absolute;
            left: 0;
            top: 2px;
            bottom: 2px;
            background: white;
            width: 1px;
        }
    }
`

interface BaseOptionType {
    name: any
    default?: boolean
    warn?: boolean
}

interface ClickableOptionType extends BaseOptionType {
    onClick: () => void
    disableCloseAfterClick?: boolean
    active?: boolean
}

interface NavLinkOptionType extends BaseOptionType {
    to: string
    subOptions?: any[] // TODO: refine type or make custom component for NavigationMenuItem
    state?: any
}

interface UrlOptionType extends BaseOptionType {
    url: string
    subOptions?: any[] // TODO: refine type or make custom component for NavigationMenuItem
}

interface LinkOptionType extends BaseOptionType {
    href: string
    target?: string
}

type OptionType =
    | ClickableOptionType
    | NavLinkOptionType
    | UrlOptionType
    | LinkOptionType

interface DropdownButtonProps {
    options: (OptionType | OptionType[])[]
    trigger?: string
    placement?: Placement
    offset?: [number, number]
    animation?: string
    arrow?: boolean // Popover arrow
    closeAfterClick?: boolean
    wrapContent?: boolean // If true and scrollbar is visible, list items will wrap
    showArrow?: boolean
    children?: React.ReactNode
    popperOptions?: any
    appendToDocument?: boolean
    arrowElement?: React.ReactNode
}

const DropdownButton = ({
    options,
    trigger,
    placement = 'bottom-end',
    offset = [0, 0],
    animation = 'fade',
    arrow = false,
    wrapContent = false,
    showArrow,
    arrowElement,
    closeAfterClick = true,
    popperOptions,
    appendToDocument,
    children,
    ...rest
}: DropdownButtonProps) => {
    const { t } = useTranslation()

    const [instance, setInstance] = useState(null)

    if (
        Array.isArray(options[0])
            ? options?.every((array: OptionType[]) => array.length === 0)
            : options?.length === 0
    ) {
        return null
    }

    const handleHide = () => {
        instance.hide()
    }

    const popoverProps = {
        onCreate: setInstance,
        trigger,
        offset,
        placement,
        arrow,
        animation,
        popperOptions,
        ...(appendToDocument && {
            appendTo: () => document.body,
        }),
    }

    const flatOptions: OptionType[] = options.flat()
    const defaultOption = flatOptions.find((option) => option.default)

    // Type guard to check if defaultOption is ClickableOptionType
    const isClickableOption = (
        option: OptionType,
    ): option is ClickableOptionType => {
        return (option as ClickableOptionType).onClick !== undefined
    }

    const hasExtraOptions =
        flatOptions.filter((option) => !option.default).length > 0

    if (defaultOption) {
        const isClickable = isClickableOption(defaultOption)
        return (
            <DefaultWrapper $hasExtraOptions={hasExtraOptions} {...rest}>
                {isClickable ? (
                    <button
                        onClick={defaultOption.onClick}
                        className="DropdownDefaultButton"
                    >
                        {children || defaultOption.name}
                    </button>
                ) : (
                    <NavLink
                        to={(defaultOption as NavLinkOptionType).to}
                        state={(defaultOption as NavLinkOptionType).state}
                        className="DropdownDefaultButton"
                    >
                        {children || defaultOption.name}
                    </NavLink>
                )}
                {hasExtraOptions && (
                    <Popover
                        {...popoverProps}
                        content={
                            <DropdownButtonList
                                onHide={handleHide}
                                wrapContent={wrapContent}
                                options={flatOptions.filter(
                                    (option) => !option.default,
                                )}
                                closeAfterClick={closeAfterClick}
                            />
                        }
                    >
                        <button
                            className="DropdownDefaultButtonOptions"
                            type="button"
                        >
                            <ChevronDownIcon aria-hidden />
                            <HideVisually>
                                {t('global.other-options')}
                            </HideVisually>
                        </button>
                    </Popover>
                )}
            </DefaultWrapper>
        )
    } else {
        const WrapperElement = appendToDocument ? Fragment : 'span'

        return React.isValidElement(children) ? (
            // <span /> prevents component from turning into two (button and Popover when visible),
            // breaking styling that depends on number of children
            <WrapperElement {...rest}>
                <Popover
                    {...popoverProps}
                    content={
                        <DropdownButtonList
                            onHide={handleHide}
                            wrapContent={wrapContent}
                            options={options}
                            closeAfterClick={closeAfterClick}
                        />
                    }
                >
                    {React.cloneElement(children, {
                        ...children.props,
                        'aria-haspopup': 'menu',
                        children: (
                            <>
                                {children.props.children}
                                {showArrow && (
                                    <>{arrowElement || <ChevronDownIcon />}</>
                                )}
                            </>
                        ),
                    })}
                </Popover>
            </WrapperElement>
        ) : null
    }
}

export default DropdownButton
