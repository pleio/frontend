import React from 'react'

import LinkContent from './components/LinkContent'
import LinkElement from './components/LinkElement'

export interface Props extends React.HTMLProps<HTMLAnchorElement> {
    url: string
    hideIcon?: boolean
    children: React.ReactNode
}

const LinkItem = ({ url, hideIcon, children, ...rest }: Props) => {
    return (
        <LinkElement url={url} {...rest}>
            <LinkContent url={url} hideIcon={hideIcon}>
                {children}
            </LinkContent>
        </LinkElement>
    )
}

export { default as LinkContent } from './components/LinkContent'
export { default as LinkElement } from './components/LinkElement'

export default LinkItem
