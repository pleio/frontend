import React from 'react'
import { useTranslation } from 'react-i18next'
import { getCleanUrl, isExternalUrl } from 'helpers'
import styled from 'styled-components'

import HideVisually from 'js/components/HideVisually/HideVisually'

import ExternalIcon from 'icons/new-window-small.svg'

interface Props {
    url: string
    hideIcon?: boolean
    children: React.ReactNode
}

const ContentWrapper = styled.div`
    display: inline-flex;
    align-items: center;
    gap: 6px;
`

const LinkContent = ({ url, hideIcon, children, ...rest }: Props) => {
    const { t } = useTranslation()
    const cleanUrl = getCleanUrl(url)
    const isExternal = isExternalUrl(cleanUrl)

    return (
        <ContentWrapper {...rest}>
            {children}
            {isExternal ? (
                <>
                    <HideVisually>
                        {t('global.opens-in-new-window')}
                    </HideVisually>
                    {!hideIcon && (
                        <ExternalIcon
                            style={{
                                flexShrink: 0,
                            }}
                        />
                    )}
                </>
            ) : null}
        </ContentWrapper>
    )
}

export default LinkContent
