import React, { HTMLAttributes } from 'react'
import { Link } from 'react-router-dom'
import { getCleanUrl, isExternalUrl } from 'helpers'

interface Props extends HTMLAttributes<HTMLAnchorElement> {
    url: string
    children: React.ReactNode
}

const LinkElement = ({ url, children, ...rest }: Props) => {
    const cleanUrl = getCleanUrl(url)
    const isExternal = isExternalUrl(cleanUrl)

    return isExternal ? (
        <a
            href={cleanUrl}
            target="_blank"
            rel="noopener noreferrer"
            style={{
                display: 'inline-flex',
                alignItems: 'center',
            }}
            {...rest}
        >
            {children}
        </a>
    ) : (
        <Link
            to={cleanUrl}
            state={{
                prevPathname: location.pathname,
            }}
            {...rest}
        >
            {children}
        </Link>
    )
}
export default LinkElement
