import styled from 'styled-components'

export default styled.div`
    flex-shrink: 0;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 14px;
    height: 14px;
    border-radius: 50%;
    background-color: ${(p) => p.theme.color.secondary.main};
    color: white;
    font-size: ${(p) => p.theme.font.size.tiny};
    line-height: ${(p) => p.theme.font.lineHeight.tiny};
    font-weight: ${(p) => p.theme.font.weight.bold};
    white-space: nowrap;
    outline: 2px solid white;
    outline-offset: 0;
`
