import styled from 'styled-components'

export default styled.div`
    position: relative;
    width: 24px;
    height: 24px;
    margin-right: 4px;
`
