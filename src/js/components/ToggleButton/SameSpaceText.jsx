import styled from 'styled-components'

export default styled.div`
    position: relative;
    height: 1.5rem;
    text-align: left;

    & > * {
        display: block;
        line-height: 1.5rem;
        height: 1.5rem;
    }

    & > *:nth-child(2) {
        position: absolute;
        top: 0;
        left: 0;
    }
`
