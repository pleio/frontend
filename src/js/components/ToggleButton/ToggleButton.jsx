import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import IconContainer from './IconContainer'
import SameSpaceText from './SameSpaceText'

const Wrapper = styled.button`
    position: relative;
    display: flex;
    align-items: center;
    height: 40px;
    color: ${(p) => p.theme.color.text.grey};

    svg {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        margin: auto;
        color: ${(p) =>
            p.$isEnabled
                ? p.theme.color.secondary.main
                : p.theme.color.icon.grey};
        transition: color ${(p) => p.theme.transition.fast};
    }

    &:hover svg {
        color: ${(p) => p.theme.color.secondary.main};
    }
`

const ToggleButton = ({
    isEnabled,
    onClick,
    enabledContent,
    disabledContent,
    EnabledIcon,
    DisabledIcon,
    'aria-label': ariaLabel,
    className, // For styled-components
}) => (
    <Wrapper
        type="button"
        className={className}
        onClick={onClick}
        aria-pressed={isEnabled}
        aria-label={ariaLabel}
        $isEnabled={isEnabled}
    >
        <IconContainer>
            {isEnabled ? (
                <EnabledIcon aria-hidden="true" />
            ) : (
                <DisabledIcon aria-hidden="true" />
            )}
        </IconContainer>

        <SameSpaceText>
            {isEnabled ? enabledContent : disabledContent}
        </SameSpaceText>
    </Wrapper>
)

ToggleButton.propTypes = {
    isEnabled: PropTypes.bool.isRequired,
    onClick: PropTypes.func.isRequired,
    enabledContent: PropTypes.string.isRequired,
    disabledContent: PropTypes.string.isRequired,
    EnabledIcon: PropTypes.elementType,
    DisabledIcon: PropTypes.elementType,
}

export default ToggleButton
