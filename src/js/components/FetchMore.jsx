import React, {
    useContext,
    useEffect,
    useLayoutEffect,
    useRef,
    useState,
} from 'react'
import { useTranslation } from 'react-i18next'
import { getOffsetTop, useIsMount } from 'helpers'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

import { AriaLiveContext } from 'js/components/AriaLive'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'

import FetchIcon from 'icons/fetch.svg'

const Wrapper = styled.div`
    position: relative;

    ${(p) =>
        p.$isScrollableBox &&
        css`
            flex-grow: 1; // Fill available space
            display: flex;
            flex-direction: column;
            overflow: hidden;

            .InfiniteScrollBox {
                overflow-y: auto;
            }

            .InfiniteScrollTopShadow,
            .InfiniteScrollBottomShadow {
                position: absolute;
                z-index: 1;
                left: 0;
                right: 0;
                height: 100px;
                pointer-events: none;
                transition: box-shadow ${(p) => p.theme.transition.normal};
            }

            .InfiniteScrollTopShadow {
                top: -100px;
            }

            .InfiniteScrollBottomShadow {
                bottom: -100px;
            }
        `};

    .InfiniteScrollMoreButton {
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
        height: ${(p) => p.$fetchMoreButtonHeight}px;

        button {
            width: 100%;
            height: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            font-size: ${(p) => p.theme.font.size.small};
            line-height: ${(p) => p.theme.font.lineHeight.small};
            font-weight: ${(p) => p.theme.font.weight.semibold};
            border-radius: ${(p) => p.theme.radius.small};

            &:hover {
                background-color: ${(p) => p.theme.color.hover};
            }

            &:active {
                background-color: ${(p) => p.theme.color.active};
            }

            svg {
                margin-right: 7px;
            }
        }
    }
`

const scrollFetchDistance = 300

const FetchMore = ({
    as,
    children,
    scrollElement,
    edges,
    fetchMore,
    fetchType = 'click',
    fetchCount = 20,
    setLimit,
    maxLimit,
    isScrollableBox,
    containHeight,
    resultsMessage,
    noResults,
    fetchMoreButtonHeight = 40,
    getMoreResults,
    ...rest
}) => {
    const isMount = useIsMount()

    const refContent = useRef()
    const refTopShadow = useRef()
    const refBottomShadow = useRef()
    const refScrollBox = useRef()

    const [fetching, setFetching] = useState(false)
    const [noItemsLeft, setNoItemsLeft] = useState(true)
    const [lastItemNumber, setLastItemNumber] = useState()

    const numberOfItems = edges?.length || 0

    const handleFetchMore = () => {
        if (fetching || noItemsLeft) return

        setFetching(true)

        let limit = fetchCount

        if (maxLimit && maxLimit - numberOfItems < fetchCount) {
            limit = maxLimit - numberOfItems
        }

        fetchMore({
            variables: {
                offset: numberOfItems,
                limit,
            },
        }).then((fetchMoreResult) => {
            // Update variables.limit for the original query to include the newly added items.
            const newLimit =
                numberOfItems + getMoreResults(fetchMoreResult.data).length
            setLimit(newLimit)

            // Hide load more button if the total of items is met.
            if (!maxLimit) setNoItemsLeft(numberOfItems >= newLimit)
            setFetching(false)
        })
    }

    const listenToScroll = fetchType === 'scroll' && !isScrollableBox

    const handleScroll = () => {
        // Extra check is needed because handleScroll is also called on load.
        if (listenToScroll) {
            const scrollEl = scrollElement?.current

            // Use external scroll element if provided..
            if (scrollEl) {
                const scrollDistanceLeft =
                    scrollEl.scrollHeight -
                    (scrollEl.scrollTop + scrollEl.offsetHeight)

                if (scrollDistanceLeft < scrollFetchDistance) {
                    handleFetchMore()
                }

                // ..use window scroll if no external scroll element is provided.
            } else {
                if (!refContent?.current) return

                const windowBottom =
                    (window.scrollY || window.pageYOffset) +
                    (window.innerHeight ||
                        document.documentElement.clientHeight)
                const contentBottom =
                    getOffsetTop(refContent.current) +
                    refContent.current.clientHeight

                if (windowBottom > contentBottom - scrollFetchDistance) {
                    handleFetchMore()
                }
            }
        }
    }

    useEffect(() => {
        if (listenToScroll) {
            const scrollEl = scrollElement?.current || window
            scrollEl.addEventListener('scroll', handleScroll)

            return () => {
                scrollEl.removeEventListener('scroll', handleScroll)
            }
        }
    })

    const handleScrollBox = (evt) => {
        const scrollBox = evt.target
        const scrollDistanceLeft =
            scrollBox.scrollHeight -
            (scrollBox.scrollTop + scrollBox.offsetHeight)

        setTopShadow(scrollBox.scrollTop)
        setBottomShadow(scrollDistanceLeft)

        if (
            fetchType === 'scroll' &&
            scrollDistanceLeft < scrollFetchDistance
        ) {
            handleFetchMore()
        }
    }

    const setTopShadow = (scrollTop) => {
        if (scrollTop > 0) {
            refTopShadow.current.style.boxShadow =
                '0 1px 6px 0 rgba(0, 0, 0, 0.075), 0 20px 20px -15px rgba(0, 0, 0, 0.075)'
        } else if (scrollTop === 0) {
            refTopShadow.current.style.boxShadow = ''
        }
    }

    const setBottomShadow = (scrollDistanceLeft) => {
        if (scrollDistanceLeft >= 5) {
            refBottomShadow.current.style.boxShadow =
                '0 -1px 6px 0 rgba(0, 0, 0, 0.075), 0 -20px 20px -15px rgba(0, 0, 0, 0.075)'
        } else if (scrollDistanceLeft < 5) {
            refBottomShadow.current.style.boxShadow = ''
        }
    }

    const hasResults = numberOfItems > 0

    useEffect(() => {
        // Hide load more button if limit is met.
        if (!!maxLimit && numberOfItems >= maxLimit) {
            setNoItemsLeft(true)
        } else {
            setNoItemsLeft(false)
        }

        if (isScrollableBox && hasResults) {
            const scrollBox = refScrollBox.current
            const scrollDistanceLeft =
                scrollBox.scrollHeight -
                (scrollBox.scrollTop + scrollBox.offsetHeight)
            setBottomShadow(scrollDistanceLeft)
        }
    })

    useLayoutEffect(() => {
        // Set initial height (make it scrollable using isScrollableBox)
        const scrollBox = refScrollBox.current
        if (
            containHeight &&
            !!scrollBox &&
            (!maxLimit || (maxLimit && numberOfItems < maxLimit))
        ) {
            scrollBox.style.height =
                scrollBox.clientHeight + fetchMoreButtonHeight + 16 + 'px'
            scrollBox.style.flexGrow = 1
        }
    }, [])

    useEffect(() => {
        if (!fetching) {
            // If fetching by scroll window is enabled, keep fetching until scrollbar is visible.
            handleScroll()

            if (!isMount) {
                announcePolite(resultsMessage)
            }
        }

        // Focus last item after fetching more items (Doesn't work with fetchPolicy: 'cache-and-network')
        if (fetchType === 'click') {
            // Before fetching more items, set what the last item is.
            if (fetching) {
                setLastItemNumber(numberOfItems)
            }

            // When fetching is finished focus the previously last item.
            if (!fetching && !!refContent && !!lastItemNumber) {
                const lastItem = refContent.current.querySelector(
                    `[data-feed="${lastItemNumber}"`,
                )

                if (lastItem) {
                    const isFocusable =
                        'button, [href], [tabindex]:not([tabindex="-1"])'
                    if (lastItem.matches(isFocusable)) {
                        lastItem.focus({
                            preventScroll: true,
                        })
                    } else {
                        const firstFocusableChild = lastItem.querySelector(
                            'button, [href], [tabindex]:not([tabindex="-1"])',
                        )
                        firstFocusableChild?.focus({
                            preventScroll: true,
                        })
                    }
                }
            }
        }

        return () => {
            setLastItemNumber()
        }
    }, [fetching])

    const { t } = useTranslation()

    const { announcePolite } = useContext(AriaLiveContext)

    const FetchMore = (
        <div className="InfiniteScrollMoreButton">
            {fetching && <LoadingSpinner />}
            {fetchType === 'click' && !fetching && (
                <button type="button" onClick={handleFetchMore}>
                    <FetchIcon />
                    {t('action.show-items')}
                </button>
            )}
        </div>
    )

    const content = (
        <>
            {children}
            {!noItemsLeft && (
                <>
                    {as === 'tbody' ? (
                        <tr>
                            <td colSpan={100} style={{ borderTop: 'none' }}>
                                {FetchMore}
                            </td>
                        </tr>
                    ) : as === 'ul' ? (
                        <li>{FetchMore}</li>
                    ) : (
                        FetchMore
                    )}
                </>
            )}
        </>
    )

    return (
        <Wrapper
            ref={refContent}
            as={as}
            $isScrollableBox={isScrollableBox && hasResults}
            $fetchMoreButtonHeight={fetchMoreButtonHeight}
            {...rest}
        >
            {hasResults ? (
                <>
                    {isScrollableBox ? (
                        <>
                            <div
                                ref={refTopShadow}
                                className="InfiniteScrollTopShadow"
                            />
                            <div
                                ref={refBottomShadow}
                                className="InfiniteScrollBottomShadow"
                            />
                            <div
                                ref={refScrollBox}
                                onScroll={handleScrollBox}
                                className="InfiniteScrollBox"
                            >
                                {content}
                            </div>
                        </>
                    ) : (
                        content
                    )}
                </>
            ) : (
                <>
                    {as === 'tbody' ? (
                        <tr>
                            <td
                                colSpan={100}
                                style={{
                                    borderTop: 'none',
                                    textAlign: 'center',
                                }}
                            >
                                <div role="status">{noResults}</div>
                            </td>
                        </tr>
                    ) : (
                        <div role="status">{noResults}</div>
                    )}
                </>
            )}
        </Wrapper>
    )
}

FetchMore.propTypes = {
    edges: PropTypes.array,
    fetchMore: PropTypes.func,
    fetchType: PropTypes.oneOf(['click', 'scroll']), // Fetch on click (shows button) or on scroll (window by default, local box if isScrollableBox or custom scrollElement)
    fetchCount: PropTypes.number, // How many items to fetch
    getMoreResults: PropTypes.func.isRequired,
    setLimit: PropTypes.func,
    maxLimit: PropTypes.number, // Stop fetching items if limit is met
    isScrollableBox: PropTypes.bool, // Create a scrollable box (with shadows to indicate scroll position)
    containHeight: PropTypes.bool, // Fix the initial height even after fetching more results (use with isScrollableBox)
    scrollElement: PropTypes.any, // Custom scroll element (if not provided, window scroll is used)
    resultsMessage: PropTypes.string,
    noResults: PropTypes.any,
    fetchMoreButtonHeight: PropTypes.number,
}

export default FetchMore
