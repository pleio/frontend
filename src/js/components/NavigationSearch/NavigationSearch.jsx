import React, { useRef, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import { getSearchLink } from 'helpers'
import { getReadableColor } from 'helpers/getContrast'
import { getContrast } from 'polished'
import styled, { css } from 'styled-components'

import HideVisually from 'js/components/HideVisually/HideVisually'
import IconButton from 'js/components/IconButton/IconButton'

import SearchIcon from 'icons/search.svg'

const Wrapper = styled.div`
    position: relative;
    width: 40px;
    height: ${(p) => p.theme.headerBarHeight}px;

    .SearchFieldWrapper {
        position: absolute;
        top: 0;
        right: 0;
        width: 40px;
        height: 100%;
        transition: width ${(p) => p.theme.transition.materialFast};

        &:before {
            content: '';
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            margin: auto 0;
            width: 100%;
            height: 40px;
            background-color: white;
            border: 1px solid
                ${(p) =>
                    getContrast(p.theme.color.header.main, 'white') >= 3
                        ? 'transparent'
                        : p.theme.color.grey[40]};
            border-radius: ${(p) => p.theme.radius.small};
            opacity: 0;
            transition: opacity ${(p) => p.theme.transition.fast};
        }
    }

    .SearchInput {
        position: absolute;
        left: 0;
        top: 0;
        bottom: 0;
        margin: auto 0;
        height: 40px;
        border: none;
        padding: 0 0 0 16px;
        width: calc(100% - 40px);

        background: none;
        font-size: ${(p) => p.theme.font.size.small};
        opacity: 0;
        transition: opacity ${(p) => p.theme.transition.fast};
    }

    .SearchButton {
        position: absolute;
        right: 0;
        top: 0;
        bottom: 0;
        margin: auto 0;

        background-color: transparent;
    }

    ${(p) =>
        !p.$isVisible &&
        css`
            .SearchInput {
                pointer-events: none;
            }

            .SearchButton {
                color: ${(p) => getReadableColor(p.theme.color.header.main)};
                outline-color: ${(p) =>
                    getReadableColor(p.theme.color.header.main)} !important;

                &:hover .IconButtonBackground {
                    background-color: ${(p) => p.theme.color.header.hover};
                }

                &:active .IconButtonBackground {
                    background-color: ${(p) => p.theme.color.header.active};
                }
            }
        `};

    ${(p) =>
        p.$isVisible &&
        css`
            .SearchFieldWrapper {
                width: 220px;

                &:before {
                    opacity: 1;
                }
            }

            .SearchInput {
                opacity: 1;
            }
        `};
`

const NavigationSearch = ({ hideSidebar }) => {
    const { t } = useTranslation()
    const navigate = useNavigate()

    const [isFocused, setIsFocused] = useState(false)
    const handleFocus = () => setIsFocused(true)
    const handleBlur = () => setIsFocused(false)

    const [searchValue, setSearchValue] = useState('')
    const onChange = (evt) => setSearchValue(evt.target.value)

    const onSubmit = () => {
        navigate(
            getSearchLink({
                q: searchValue.trim(),
            }),
        )
        setSearchValue('')
        if (hideSidebar) hideSidebar()
    }

    const onKeyDown = (evt) => {
        if (evt.key === 'Enter') onSubmit()
        if (evt.key === 'Escape') evt.target.blur()
    }

    const refInput = useRef()
    const focusInput = () => refInput.current.focus()

    const disabled = isFocused && !searchValue

    return (
        <Wrapper $isVisible={isFocused || !!searchValue}>
            <div className="SearchFieldWrapper">
                <HideVisually as="label" htmlFor="nav-search">
                    {t('global.search')}
                </HideVisually>
                <input
                    ref={refInput}
                    className="SearchInput"
                    name="nav-search"
                    id="nav-search"
                    onFocus={handleFocus}
                    onBlur={handleBlur}
                    onKeyDown={onKeyDown}
                    onChange={onChange}
                    value={searchValue}
                    placeholder={t('global.search')}
                />
                <IconButton
                    className="SearchButton"
                    size="large"
                    variant={disabled ? 'tertiary' : 'secondary'}
                    radiusStyle={isFocused || searchValue ? 'none' : 'round'}
                    onClick={searchValue ? onSubmit : focusInput}
                    disabled={disabled}
                    aria-label={t('global.search')}
                    tabIndex="-1"
                >
                    <SearchIcon />
                </IconButton>
            </div>
        </Wrapper>
    )
}

export default NavigationSearch
