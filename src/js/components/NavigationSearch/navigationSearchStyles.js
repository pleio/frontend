import { media } from 'helpers'
import { getContrast } from 'polished'
import { css } from 'styled-components'

export default css`
    display: flex;
    height: 38px;
    max-width: 130px;
    background-color: white;
    border-radius: ${(p) => p.theme.radius.small};
    overflow: hidden;
    border: 1px solid
        ${(p) =>
            getContrast(p.theme.color.header.main, 'white') >= 3
                ? 'transparent'
                : p.theme.color.grey[40]};

    ${media.mobilePortrait`
        display: none;
    `}

    .NavigationSearchInput {
        flex-grow: 1;
        background-color: transparent;
        border: none;
        height: 100%;
        padding: 0 0 0 16px;

        font-size: ${(p) => p.theme.font.size.small};
    }

    .NavigationSearchButton {
        width: 36px;
        height: 36px;
    }
`
