import { useSiteStore } from 'js/lib/stores'

export interface Props {
    title?: string
    containerTitle?: string
}

const Document = ({ title, containerTitle }: Props) => {
    const { site } = useSiteStore()

    let documentTitle = ''
    if (title) documentTitle = `${title} · `
    if (containerTitle) documentTitle += `${containerTitle} · `

    documentTitle += site?.name || 'Pleio'

    if (document.title !== documentTitle) document.title = documentTitle

    return null
}

export default Document
