import styled from 'styled-components'

const heading = styled.h5`
    font-size: ${(p) => p.theme.font.size.small};
    line-height: ${(p) => p.theme.font.lineHeight.small};
    font-weight: ${(p) => p.theme.font.weight.semibold};
    color: ${(p) => p.theme.color.text.black};

    a {
        color: ${(p) => p.theme.color.primary.main};
    }
`

heading.displayName = 'H5'

export default heading
