import styled from 'styled-components'

const heading = styled.h1`
    font-family: ${(p) => p.theme.fontHeading.family};
    font-size: ${(p) => p.theme.fontHeading.size.large};
    line-height: ${(p) => p.theme.fontHeading.lineHeight.large};
    font-weight: ${(p) => p.theme.fontHeading.weight.bold};
    color: ${(p) => p.theme.color.primary.main};
`
heading.displayName = 'H1'

export default heading
