import styled from 'styled-components'

const heading = styled.h4`
    font-size: ${(p) => p.theme.font.size.normal};
    line-height: ${(p) => p.theme.font.lineHeight.normal};
    font-weight: ${(p) => p.theme.font.weight.semibold};
    color: ${(p) => p.theme.color.text.black};

    a {
        color: ${(p) => p.theme.color.primary.main};
    }
`

heading.displayName = 'H4'

export default heading
