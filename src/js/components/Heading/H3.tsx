import styled from 'styled-components'

const heading = styled.h3`
    font-family: ${(p) => p.theme.fontHeading.family};
    font-size: ${(p) => p.theme.fontHeading.size.small};
    line-height: ${(p) => p.theme.fontHeading.lineHeight.small};
    font-weight: ${(p) => p.theme.fontHeading.weight.bold};
    color: ${(p) => p.theme.color.text.black};

    a {
        color: ${(p) => p.theme.color.primary.main};
    }
`

heading.displayName = 'H3'

export default heading
