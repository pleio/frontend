import styled from 'styled-components'

const heading = styled.h2`
    font-family: ${(p) => p.theme.fontHeading.family};
    font-size: ${(p) => p.theme.fontHeading.size.normal};
    line-height: ${(p) => p.theme.fontHeading.lineHeight.normal};
    font-weight: ${(p) => p.theme.fontHeading.weight.bold};
    color: ${(p) => p.theme.color.text.black};

    a {
        color: ${(p) => p.theme.color.primary.main};
    }
`

heading.displayName = 'H2'

export default heading
