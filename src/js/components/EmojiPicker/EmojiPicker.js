import React from 'react'
import { useTranslation } from 'react-i18next'
import Picker from 'emoji-picker-react'
import styled from 'styled-components'

const Wrapper = styled.div`
    .EmojiPickerReact {
        --epr-emoji-size: 24px;
        --epr-bg-color: transparent;
        --epr-hover-bg-color: ${(p) => p.theme.color.hover};
        --epr-focus-bg-color: ${(p) => p.theme.color.active};
        --epr-text-color: ${(p) => p.theme.color.text.black};
        --epr-search-input-text-color: ${(p) => p.theme.color.text.black};
        --epr-highlight-color: ${(p) => p.theme.color.secondary.main};
        --epr-category-icon-active-color: transparent;
        border: none;

        .epr-btn {
            border-radius: ${(p) => p.theme.radius.large};

            &:hover {
                background-color: ${(p) => p.theme.color.hover};
            }

            &:active {
                background-color: ${(p) => p.theme.color.active};
            }
        }

        .epr-cat-btn:hover,
        .epr-cat-btn.epr-active {
            background-position-y: 0 !important;
        }

        .epr-cat-btn.epr-active {
            // Make active icon black instead of plugin theme color.
            filter: brightness(0);
        }

        .epr-search-container input::placeholder {
            color: ${(p) => p.theme.color.text.grey};
        }

        .epr-category-nav {
            padding-top: 0;
            padding-bottom: 0;
        }

        .epr-emoji-category-label {
            font-size: ${(p) => p.theme.font.size.small};
            line-height: ${(p) => p.theme.font.lineHeight.small};
        }

        * {
            font-family: ${(p) => p.theme.font.family}, Sans-Serif;
        }
    }
`

function EmojiPicker(props) {
    const { t } = useTranslation()

    const categories = [
        { category: 'suggested', name: t('emoji-picker.suggested') },
        { category: 'smileys_people', name: t('emoji-picker.smileys_people') },
        { category: 'animals_nature', name: t('emoji-picker.animals_nature') },
        { category: 'food_drink', name: t('emoji-picker.food_drink') },
        { category: 'travel_places', name: t('emoji-picker.travel_places') },
        { category: 'activities', name: t('emoji-picker.activities') },
        { category: 'objects', name: t('emoji-picker.objects') },
        { category: 'symbols', name: t('emoji-picker.symbols') },
    ]

    return (
        <Wrapper>
            <Picker
                emojiStyle="native"
                categories={categories}
                searchPlaceholder={t('global.search')}
                previewConfig={{
                    showPreview: false,
                }}
                {...props}
            />
        </Wrapper>
    )
}

export default React.memo(EmojiPicker)
