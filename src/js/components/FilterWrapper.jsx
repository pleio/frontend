import React, { useEffect, useState } from 'react'
import { useMeasure } from 'react-use'
import styled from 'styled-components'

const Wrapper = styled.div`
    pointer-events: none;

    > .FilterWrapperContainer {
        display: flex;
        flex-wrap: wrap;
        margin: -5px;

        > * {
            pointer-events: auto;
        }
    }
`

const getColWidth = (width) => {
    let colWidth = 1
    if (width >= 300 && width < 490) colWidth = 2
    else if (width >= 490 && width < 660) colWidth = 3
    else if (width >= 660) colWidth = 4
    return colWidth
}

const FilterWrapper = ({ children, ...rest }) => {
    const [ref, { width }] = useMeasure()

    const [colWidth, setColWidth] = useState(null)

    useEffect(() => {
        if (!width) return
        setColWidth(getColWidth(width))
    }, [width])

    return (
        <Wrapper ref={ref} {...rest}>
            {colWidth && (
                <div className="FilterWrapperContainer">
                    {React.Children.map(children, (child, i) => {
                        if (child)
                            return React.cloneElement(child, {
                                key: i,
                                ...child.props,
                                style: {
                                    ...child.props.style,
                                    padding: '0 5px',
                                    marginTop: '5px',
                                    marginBottom: '5px',
                                    width: 100 / colWidth + '%',
                                },
                            })
                    })}
                </div>
            )}
        </Wrapper>
    )
}

export default FilterWrapper
