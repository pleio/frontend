import React, {
    Fragment,
    useEffect,
    useLayoutEffect,
    useRef,
    useState,
} from 'react'
import { useIsMount } from 'helpers'
import PropTypes from 'prop-types'

const AnimatePresence = ({
    visible,
    transition = {
        opacity: 0.35,
        height: 0.35,
    },
    children,
    ...rest
}) => {
    const [shouldRender, setShouldRender] = useState(false)
    const isMount = useIsMount()
    const refWrapper = useRef()
    const refContent = useRef()

    const transitionStyle = `opacity ${transition.opacity}s ease, height ${transition.height}s cubic-bezier(0.4, 0, 0.2, 1)`

    const showContent = () => {
        setShouldRender(true)

        setTimeout(() => {
            if (transition.opacity) {
                refWrapper.current.style.opacity = 1
            }
            if (transition.height) {
                const contentHeight = refContent.current.clientHeight
                refWrapper.current.style.overflow = 'hidden'
                refWrapper.current.style.transition = transitionStyle
                refWrapper.current.style.height = `${contentHeight}px`
            }
        }, 20) // Wait 1 animation frame, because setShouldRender renders the `refWrapper`
    }

    const hideContent = () => {
        if (transition.opacity) {
            refWrapper.current.style.opacity = 1
        }
        if (transition.height) {
            const contentHeight = refContent.current.clientHeight
            refWrapper.current.style.overflow = 'hidden'
            refWrapper.current.style.height = `${contentHeight}px`
            refWrapper.current.style.transition = transitionStyle
        }

        setTimeout(() => {
            if (transition.opacity) {
                refWrapper.current.style.opacity = 0
            }
            if (transition.height) {
                refWrapper.current.style.height = 0
            }
        }, 50) // If its faster the animation fails
    }

    useLayoutEffect(() => {
        if (isMount && visible) {
            setShouldRender(true)
            setTimeout(() => {
                if (!refWrapper.current) return
                refWrapper.current.style.transition = null
                refWrapper.current.style.height = null
                refWrapper.current.style.opacity = null
                refWrapper.current.style.overflow = null
            }, 0)
        }
    }, [visible])

    useEffect(() => {
        if (isMount) return

        if (visible) {
            showContent()
        }

        if (!visible && shouldRender) {
            hideContent()
        }
    }, [visible])

    const handleTransitionEnd = () => {
        if (visible) {
            refWrapper.current.style.height = null
            refWrapper.current.style.opacity = null
            refWrapper.current.style.overflow = null
        } else {
            setShouldRender(false)
        }
    }

    const ContentWrapper = transition.height ? 'div' : Fragment

    return (
        <>
            {shouldRender && (
                <div
                    ref={refWrapper}
                    style={{
                        opacity: transition.opacity ? 0 : '',
                        height: transition.height ? 0 : '',
                        overflow: transition.height ? 'hidden' : '',
                    }}
                    onTransitionEnd={handleTransitionEnd}
                    {...rest}
                >
                    <ContentWrapper
                        ref={refContent}
                        style={{
                            // clientHeight will take children padding/margin into account (overflow: 'auto' showed scrollbar in some browsers)
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'stretch',
                        }}
                    >
                        {children}
                    </ContentWrapper>
                </div>
            )}
        </>
    )
}

AnimatePresence.propTypes = {
    visible: PropTypes.bool,
    transition: PropTypes.shape({
        opacity: PropTypes.number,
        height: PropTypes.number,
    }),
}

export default AnimatePresence
