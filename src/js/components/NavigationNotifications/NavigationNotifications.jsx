import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'
import { textContrastIsAA } from 'helpers/getContrast'
import { useTheme } from 'styled-components'

import Popover from 'js/components/Popover/Popover'
import NavigationAction from 'js/layout/Navigation/components/NavigationAction'
import NavigationBadge from 'js/layout/Navigation/components/NavigationBadge'
import withGlobalState from 'js/lib/withGlobalState'

import BellIcon from 'icons/bell.svg'
import BellFillIcon from 'icons/bell-fill.svg'

import NavigationNotificationsView from './NavigationNotificationsView'

const NavigationNotifications = ({ globalState, viewer }) => {
    const [visible, setVisible] = useState(false)

    const theme = useTheme()

    const handleToggle = () => setVisible(!visible)

    const handleHide = () => setVisible(false)

    const { data, startPolling, stopPolling } = useQuery(QUERY, {
        fetchPolicy: 'cache-and-network',
    })

    useEffect(() => {
        if (globalState.inactiveWindow || globalState.inactiveUser) {
            stopPolling()
        } else {
            startPolling(120000) // every 120 seconds
        }
    }, [globalState])

    const hasUnread =
        data && data.notifications && data.notifications.totalUnread !== 0

    const { t } = useTranslation()

    if (viewer && !viewer.user) return null

    return (
        <span>
            <Popover
                visible={visible}
                content={
                    <NavigationNotificationsView
                        visible={visible}
                        viewer={viewer}
                        hasUnread={hasUnread}
                        hideNotifications={handleHide}
                    />
                }
                offset={[0, 0]}
                maxWidth={null}
                // Not onHide because content will disappear before transitioned
                onHidden={handleHide}
            >
                <NavigationAction
                    aria-label={
                        data
                            ? t('notifications.show-new-notifications', {
                                  count: data.notifications.totalUnread,
                              })
                            : null
                    }
                    onClick={handleToggle}
                >
                    {textContrastIsAA(theme.color.header.main) ? (
                        <BellFillIcon />
                    ) : (
                        <BellIcon />
                    )}
                    {hasUnread && (
                        <NavigationBadge>
                            {data && data.notifications.totalUnread < 100
                                ? data.notifications.totalUnread
                                : '99+'}
                        </NavigationBadge>
                    )}
                </NavigationAction>
            </Popover>
        </span>
    )
}

const QUERY = gql`
    query NotificationsUnread {
        notifications {
            totalUnread
        }
    }
`

export default withGlobalState(NavigationNotifications)
