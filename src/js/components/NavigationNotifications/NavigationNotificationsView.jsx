import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { NavLink } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { useEventListener } from 'helpers'

import Button from 'js/components/Button/Button'
import FetchMore from 'js/components/FetchMore'
import { H4 } from 'js/components/Heading'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Text from 'js/components/Text/Text'
import NavigationMenu from 'js/layout/Navigation/components/NavigationMenu'
import Notification from 'js/user/Notifications/components/Notification'
import { NOTIFICATIONS_QUERY } from 'js/user/Notifications/Notifications'

const NavigationNotificationsView = ({
    mutate,
    viewer,
    visible,
    hasUnread,
    hideNotifications,
}) => {
    const { t } = useTranslation()

    const [maxHeight, setMaxHeight] = useState(window.innerHeight)
    const handleResize = () => {
        // Set max height to make menu fill the screen nicely on mobile
        // 100vh does not work because it includes the browser's address bar
        setMaxHeight(window.innerHeight)
    }

    useEventListener('resize', handleResize)

    const markAllAsRead = () => {
        mutate({
            variables: {
                input: {},
            },
            refetchQueries: ['NotificationsList', 'NotificationsUnread'],
        })
    }

    const [queryLimit, setQueryLimit] = useState(10)
    const { loading, data, fetchMore, refetch } = useQuery(
        NOTIFICATIONS_QUERY,
        {
            skip: !visible,
            variables: {
                offset: 0,
                limit: queryLimit,
                unread: true,
            },
        },
    )

    useEffect(() => {
        if (visible === true) refetch()
    }, [visible])

    return (
        <NavigationMenu $maxHeight={maxHeight}>
            <div className="NavigationMenuHeader">
                <H4
                    style={{
                        marginTop: '4px',
                        marginRight: '4px',
                    }}
                >
                    {t('notifications.unread-notifications')}
                </H4>
                {hasUnread && (
                    <Button
                        size="small"
                        variant="tertiary"
                        onClick={markAllAsRead}
                    >
                        {t('notifications.read-all')}
                    </Button>
                )}
            </div>

            {!data || loading ? (
                <LoadingSpinner
                    style={{ marginTop: '-8px', marginBottom: '16px' }}
                />
            ) : (
                <FetchMore
                    isScrollableBox
                    edges={data.notifications.edges}
                    getMoreResults={(data) => data.notifications.edges}
                    fetchMore={fetchMore}
                    fetchType="scroll"
                    fetchCount={10}
                    setLimit={setQueryLimit}
                    maxLimit={data.notifications.total}
                    noResults={
                        <Text
                            size="small"
                            variant="grey"
                            className="NavigationMenuEmpty"
                            role="alert"
                        >
                            {t('notifications.no-notifications')}
                        </Text>
                    }
                    className="NavigationMenuList"
                >
                    <ul>
                        {data.notifications.edges.map((notification, i) => (
                            <Notification
                                key={i}
                                notification={notification}
                                hideNotifications={hideNotifications}
                            />
                        ))}
                    </ul>
                </FetchMore>
            )}

            <NavLink
                to={`/user/${viewer.user.username}/notifications`}
                onClick={hideNotifications}
                className="NavigationMenuFooter"
            >
                {t('notifications.all-notifications')}
            </NavLink>
        </NavigationMenu>
    )
}

const Mutation = gql`
    mutation NotificationsTop($input: markAllAsReadInput!) {
        markAllAsRead(input: $input) {
            success
        }
    }
`

export default graphql(Mutation)(NavigationNotificationsView)
