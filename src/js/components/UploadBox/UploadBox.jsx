import React, { useRef } from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import DashedBox from 'js/components/DashedBox/DashedBox'
import FileInput from 'js/components/FileInput/FileInput'
import HideVisually from 'js/components/HideVisually/HideVisually'
import Text from 'js/components/Text/Text'

const Wrapper = styled.div`
    position: relative;
    font-size: ${(p) => p.theme.font.size.small};
    line-height: ${(p) => p.theme.font.lineHeight.small};
    user-select: none;

    body:not(.mouse-user) && .UploadBoxInput:focus + .UploadBoxContent {
        outline: ${(p) => p.theme.focusStyling};
    }

    .UploadBoxContent {
        display: flex;
        flex-direction: column;
        align-items: center;
        padding: 24px;
        background-color: white;

        cursor: pointer;

        &:hover {
            background-color: ${(p) => p.theme.color.hover};
        }

        &:active {
            background-color: ${(p) => p.theme.color.active};
        }
    }

    .UploadBoxTitle {
        color: ${(p) => p.theme.color.secondary.main};
        font-weight: ${(p) => p.theme.font.weight.semibold};
    }
`

const UploadBox = ({
    children,
    name,
    title,
    multiple,
    accept,
    maxSize,
    onSubmit,
    helper,
    'aria-describedby': describedBy,
    ...props
}) => {
    const refInput = useRef()

    const handleClick = () => {
        refInput.current.click()
    }

    const { t } = useTranslation()

    return (
        <Wrapper {...props}>
            <HideVisually
                as={FileInput}
                ref={refInput}
                className="UploadBoxInput"
                name={name}
                id={name}
                multiple={multiple}
                maxSize={maxSize}
                accept={accept}
                onSubmit={onSubmit}
                aria-describedby={describedBy}
            />

            <DashedBox className="UploadBoxContent" onClick={handleClick}>
                <div className="UploadBoxTitle">
                    {title ||
                        (multiple
                            ? t('form.upload-files')
                            : t('form.upload-file'))}
                </div>

                {!!maxSize && (
                    <Text size="small" variant="grey">
                        {t('form.max-file-size', {
                            size: maxSize,
                        })}
                    </Text>
                )}

                {helper && (
                    <Text size="small" variant="grey">
                        {helper}
                    </Text>
                )}

                {children && <div style={{ marginTop: '8px' }}>{children}</div>}
            </DashedBox>
        </Wrapper>
    )
}

export default UploadBox
