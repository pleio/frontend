import React from 'react'
import { media } from 'helpers'
import styled, { css } from 'styled-components'

import Sticky from 'js/components/Sticky/Sticky'

import TabMenuItem from './TabMenuItem'

const Wrapper = styled.div`
    &[data-sticky='true'] {
        background: white;
        box-shadow: ${(p) => p.theme.shadow.soft.bottom};
        z-index: 2;
    }

    ${(p) =>
        p.$edgeMargin &&
        css`
            ${media.mobileLandscapeDown`
                margin-left: -${p.theme.padding.horizontal.small};
                margin-right: -${p.theme.padding.horizontal.small};
            `};

            ${media.tabletUp`
                margin-left: -${p.theme.padding.horizontal.normal};
                margin-right: -${p.theme.padding.horizontal.normal};
            `};
        `};

    ${(p) =>
        p.$edgePadding &&
        css`
            ${media.mobileLandscapeDown`
                padding-left: ${p.theme.padding.horizontal.small};
                padding-right: ${p.theme.padding.horizontal.small};
            `};

            ${media.tabletUp`
                padding-left: ${p.theme.padding.horizontal.normal};
                padding-right: ${p.theme.padding.horizontal.normal};
            `};
        `};

    ${(p) =>
        p.$showBorder &&
        css`
            position: relative;

            &:before {
                content: '';
                position: absolute;
                bottom: 0;
                left: 0;
                right: 0;
                height: 1px;
                background: ${(p) => p.theme.color.grey[30]};
            }
        `};

    .TabMenuContainer {
        display: flex;
        align-items: center;
        flex-wrap: wrap;
        margin: 0 -10px;
    }

    .TabMenuItem {
        display: flex;
        align-items: center;
        height: 36px;
        padding: 0 10px;
        font-size: ${(p) => p.theme.font.size.small};
        color: ${(p) => p.theme.color.text.grey};
        white-space: nowrap;

        &[aria-current='page'] {
            position: relative;
            color: ${(p) => p.theme.color.primary.main};

            &:before {
                content: '';
                position: absolute;
                left: 10px;
                right: 10px;
                bottom: 0;
                height: 3px;
                background-color: ${(p) => p.theme.color.primary.main};
            }

            body:not(.mouse-user) &:focus:before {
                bottom: 4px;
            }
        }

        &:not([aria-current='page']) {
            &:hover {
                color: ${(p) => p.theme.color.text.black};
            }
        }
    }
`

const TabMenu = ({
    edgePadding,
    edgeMargin,
    showBorder,
    sticky,
    options,
    TabElement,
    children,
    label,
    ...rest
}) => {
    if (!options || !options.length) {
        return null
    }

    return (
        <Wrapper
            $edgePadding={edgePadding}
            $edgeMargin={edgeMargin}
            $showBorder={showBorder}
            $sticky={sticky}
            as={sticky ? Sticky : null}
            {...rest}
        >
            <ul className="TabMenuContainer" role="tablist" aria-label={label}>
                {options.map((item, i) => (
                    <li key={i}>
                        <TabMenuItem
                            key={i}
                            item={item}
                            counter={i}
                            TabElement={TabElement}
                        />
                    </li>
                ))}
                {children}
            </ul>
        </Wrapper>
    )
}

export default TabMenu
