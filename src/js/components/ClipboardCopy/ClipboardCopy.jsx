import React, { useRef, useState } from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import HideVisually from 'js/components/HideVisually/HideVisually'
import IconButton from 'js/components/IconButton/IconButton'
import Tooltip from 'js/components/Tooltip/Tooltip'

import CopyIcon from 'icons/copy.svg'

const CopyArea = styled.div`
    display: flex;
    background-color: ${(p) => p.theme.color.grey[10]};
    border-radius: ${(p) => p.theme.radius.small};

    .ClipboardCopyText {
        font-size: ${(p) => p.theme.font.size.normal};
        line-height: ${(p) => p.theme.font.lineHeight.normal};
        flex-grow: 1;
        padding: 10px 0 10px 16px;
    }
`

function ClipboardCopy({ copyText, ...rest }) {
    const { t } = useTranslation()

    const inputRef = useRef()

    const [isCopied, setIsCopied] = useState(false)

    const handleCopyClick = () => {
        inputRef.current.select()
        document.execCommand('copy')
        setIsCopied(true)
        setTimeout(() => setIsCopied(false), 3_000)
    }

    return (
        <CopyArea {...rest}>
            <HideVisually>
                <input
                    tabIndex={-1}
                    type="text"
                    value={copyText}
                    readOnly
                    ref={inputRef}
                />
            </HideVisually>
            {isCopied && (
                <HideVisually role="alert">
                    {t('entity-file.link-copied')}
                </HideVisually>
            )}

            <div className="ClipboardCopyText">{copyText}</div>

            <Tooltip visible={isCopied} content={t('entity-file.link-copied')}>
                <IconButton
                    size="large"
                    variant="secondary"
                    onClick={handleCopyClick}
                    tooltip={t('entity-file.copy-link')}
                    style={{ margin: '2px 4px 2px 0' }}
                >
                    <CopyIcon />
                </IconButton>
            </Tooltip>
        </CopyArea>
    )
}

export default ClipboardCopy
