import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import compose from 'lodash.flowright'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import FormItem from 'js/components/Form/FormItem'
import ConfirmationModal from 'js/components/Modal/ConfirmationModal'
import Spacer from 'js/components/Spacer/Spacer'
import Text from 'js/components/Text/Text'

const Wrapper = styled.div`
    position: relative;

    .ProseMirror {
        padding-bottom: 56px;
    }
`

const CommentEditor = ({
    mutateAdd,
    mutateEdit,
    isEditing,
    viewer,
    entity,
    group,
    refetchQueries,
    hideEditor,
    isReply,
}) => {
    const [showPublishRequestModal, setShowPublishRequestModal] =
        useState(false)

    const submit = async ({ richDescription }) => {
        if (isEditing) {
            await mutateEdit({
                variables: {
                    input: {
                        guid: entity.guid,
                        richDescription,
                    },
                },
            })
                .then(() => {
                    hideEditor()
                })
                .catch((errors) => {
                    console.error(errors)
                })
        } else {
            await mutateAdd({
                variables: {
                    input: {
                        type: 'object',
                        subtype: 'comment',
                        richDescription,
                        containerGuid: entity.guid,
                    },
                },
                refetchQueries,
            })
                .then(() => {
                    hideEditor()
                })
                .catch((errors) => {
                    console.error(errors)
                })
        }
    }

    const defaultValues = {
        richDescription: isEditing ? entity.richDescription : '',
    }

    const {
        control,
        handleSubmit,
        formState: { isValid, isSubmitting },
    } = useForm({ mode: 'onChange', defaultValues })

    const { t } = useTranslation()

    const submitForm = handleSubmit(submit)

    const preSubmit = () => {
        if (viewer.requiresCommentModeration) {
            setShowPublishRequestModal(true)
            return
        }

        submitForm()
    }

    const submitLabel = isEditing
        ? t('action.save')
        : isReply
          ? t('comments.reply')
          : t('comments.comment')

    return (
        <Wrapper>
            <FormItem
                control={control}
                type="rich"
                name="richDescription"
                guid={entity.guid}
                label={
                    isReply
                        ? t('comments.reply-placeholder')
                        : t('comments.comment-placeholder')
                }
                options={{
                    textFormat: true,
                    textStyle: true,
                    textLink: true,
                    textList: true,
                    insertMedia: viewer.canInsertMedia,
                    insertMention: true,
                    disableAlignImage: true,
                }}
                hideKeyboardShortcuts
                required
                autofocus
                height="small"
                group={group}
            />
            <Flexer
                style={{
                    position: 'absolute',
                    bottom: '12px',
                    right: '16px',
                }}
            >
                <Button
                    size="normal"
                    variant="tertiary"
                    onClick={() => hideEditor(true)}
                >
                    {t('action.cancel')}
                </Button>
                <Button
                    size="normal"
                    variant="primary"
                    disabled={!isValid}
                    loading={isSubmitting}
                    onClick={preSubmit}
                >
                    {submitLabel}
                </Button>
            </Flexer>

            <ConfirmationModal
                isVisible={showPublishRequestModal}
                onCancel={() => setShowPublishRequestModal(false)}
                onConfirm={() => {
                    setShowPublishRequestModal(false)
                    submitForm()
                }}
                title={submitLabel}
                cancelLabel={t('action.cancel')}
                confirmLabel={t('action.send-request')}
            >
                <Spacer>
                    <Text size="small">
                        {t('content-moderation.send-request-helper')}
                    </Text>
                    <Text size="small">
                        {t('content-moderation.send-request-confirm')}
                    </Text>
                </Spacer>
            </ConfirmationModal>
        </Wrapper>
    )
}

const ADDMUTATION = gql`
    mutation AddComment($input: addEntityInput!) {
        addEntity(input: $input) {
            entity {
                guid
                ... on Blog {
                    isFollowing
                }
                ... on News {
                    isFollowing
                }
                ... on Discussion {
                    isFollowing
                }
                ... on Event {
                    isFollowing
                }
                ... on FileFolder {
                    isFollowing
                }
                ... on Question {
                    isFollowing
                }
            }
        }
    }
`

const EDITMUTATION = gql`
    mutation EditComment($input: editEntityInput!) {
        editEntity(input: $input) {
            entity {
                guid
                ... on Comment {
                    richDescription
                }
            }
        }
    }
`

export default compose(
    graphql(ADDMUTATION, { name: 'mutateAdd' }),
    graphql(EDITMUTATION, { name: 'mutateEdit' }),
)(CommentEditor)
