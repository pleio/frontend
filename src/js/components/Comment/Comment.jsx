import React, { useLayoutEffect, useRef, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useMutation } from '@apollo/client'
import PropTypes from 'prop-types'

import AddCommentForm from 'js/components/AddComment/AddCommentForm'
import Button from 'js/components/Button/Button'
import Card from 'js/components/Card/Card'
import DropdownButton from 'js/components/DropdownButton/DropdownButton'
import IconButton from 'js/components/IconButton/IconButton'
import Modal from 'js/components/Modal/Modal'
import Tooltip from 'js/components/Tooltip/Tooltip'
import { useSiteStore } from 'js/lib/stores/SiteStore'

import EditIcon from 'icons/edit-small.svg'
import OptionsIcon from 'icons/options-small.svg'
import PrizeIcon from 'icons/prize.svg'
import RepliesIcon from 'icons/replies-small.svg'

import CommentLayout from './components/CommentLayout'
import DeleteForm from './components/DeleteForm'
import LikeButton from './components/LikeButton'
import Vote from './components/Vote'
import Wrapper from './components/Wrapper'

const Comment = ({
    viewer,
    entity,
    group,
    hideReply,
    isClosed,
    canUpvote,
    inActivityFeed,
    canChooseBestAnswer,
    highlightBestAnswer,
    refetchQueries,
    disabled,
}) => {
    const refEditButton = useRef()
    const refReplyButton = useRef()

    const { site } = useSiteStore()
    const { commentWithoutAccountEnabled } = site

    const [mutateToggleBestAnswer] = useMutation(TOGGLE_BEST_ANSWER)

    const [isEditing, setIsEditing] = useState(false)

    const [showDeleteModal, setShowDeleteModal] = useState(false)
    const toggleDeleteModal = () => setShowDeleteModal(!showDeleteModal)

    const [showReplies, setShowReplies] = useState(false)
    const toggleShowReplies = () => setShowReplies(!showReplies)

    const [showAllReplies, setShowAllReplies] = useState(false)
    const toggleShowAllReplies = () => setShowAllReplies(!showAllReplies)

    useLayoutEffect(() => {
        if (hasReplies) {
            setShowReplies(true)
        }
    }, [])

    const toggleBestAnswer = () => {
        mutateToggleBestAnswer({
            variables: {
                input: {
                    guid: entity.guid,
                },
            },
            refetchQueries,
        })
    }

    const toggleEdit = () => setIsEditing(!isEditing)

    const { canEdit, isBestAnswer, canArchiveAndDelete } = entity

    const showUpDownVote = canUpvote && window.__SETTINGS__.showUpDownVoting
    const showLike = !canUpvote || !window.__SETTINGS__.showUpDownVoting

    const { t } = useTranslation()

    const isParent = !!entity.comments

    // Total number of replies, excluding unapproved replies (publish requests).
    const { commentCount } = entity
    // Check if replies exist, including unapproved replies (publish requests).
    const hasReplies = entity.comments?.length > 0

    const excerpt =
        entity.excerpt?.length > 30
            ? `${entity.excerpt.substring(0, 30)}...`
            : entity.excerpt

    return (
        <Wrapper
            as={isParent && !inActivityFeed ? Card : 'li'}
            forwardedAs={isParent && !inActivityFeed ? 'li' : undefined}
            $radiusSize="normal"
            $inActivityFeed={inActivityFeed}
            $isParent={isParent}
            $highlightBestAnswer={highlightBestAnswer}
        >
            <CommentLayout
                viewer={viewer}
                entity={entity}
                group={group}
                inActivityFeed={inActivityFeed}
                isEditing={isEditing}
                setIsEditing={setIsEditing}
                refEditButton={refEditButton}
            />

            {!isEditing && !disabled && (
                <div
                    className={`CommentFooter ${
                        !canChooseBestAnswer && !isBestAnswer
                            ? 'IsIndented'
                            : ''
                    }`}
                >
                    {canChooseBestAnswer && (
                        <Tooltip
                            theme="primary-white"
                            content={
                                isBestAnswer
                                    ? t('comments.best-answer-deselect')
                                    : t('comments.best-answer-select')
                            }
                        >
                            <IconButton
                                size="small"
                                variant={isBestAnswer ? 'primary' : 'tertiary'}
                                onClick={toggleBestAnswer}
                                disabled={isClosed}
                                className="BestAnswerButton"
                                aria-label={
                                    isBestAnswer
                                        ? t('comments.best-answer-deselect')
                                        : t('comments.best-answer-select')
                                }
                            >
                                <PrizeIcon />
                            </IconButton>
                        </Tooltip>
                    )}
                    {!canChooseBestAnswer && isBestAnswer && (
                        <Tooltip
                            content={t('comments.best-answer')}
                            theme="primary-white"
                        >
                            <div className="BestAnswerIndicator">
                                <PrizeIcon />
                            </div>
                        </Tooltip>
                    )}

                    {isParent && hasReplies && (
                        <button
                            ref={refReplyButton}
                            type="button"
                            className="CommentReplyButton"
                            onClick={toggleShowReplies}
                            aria-label={
                                showReplies
                                    ? t('comments.hide-replies', {
                                          title: excerpt,
                                      })
                                    : t('comments.show-replies', {
                                          title: excerpt,
                                      })
                            }
                        >
                            <RepliesIcon />
                            {t('comments.count-replies', {
                                count: commentCount,
                            })}
                        </button>
                    )}

                    {isParent && !hideReply && !hasReplies && (
                        <button
                            ref={refReplyButton}
                            type="button"
                            className="CommentReplyButton"
                            onClick={toggleShowReplies}
                            aria-label={
                                showReplies
                                    ? t('comments.close-reply-to-item', {
                                          title: excerpt,
                                      })
                                    : t('comments.reply-to-item', {
                                          title: excerpt,
                                      })
                            }
                        >
                            <RepliesIcon />
                            {t('comments.reply')}
                        </button>
                    )}

                    <div className="CommentActions">
                        {showUpDownVote && (
                            <Vote
                                entity={entity}
                                viewer={viewer}
                                isClosed={isClosed}
                            />
                        )}

                        {showLike && (
                            <LikeButton
                                entity={entity}
                                viewer={viewer}
                                isClosed={isClosed}
                            />
                        )}

                        {canEdit && !isClosed && (
                            <IconButton
                                ref={refEditButton}
                                size="small"
                                variant="secondary"
                                onClick={toggleEdit}
                                aria-label={t('comments.edit')}
                            >
                                <EditIcon />
                            </IconButton>
                        )}

                        {canArchiveAndDelete && (
                            <DropdownButton
                                options={[
                                    {
                                        name: t('comments.delete'),
                                        onClick: toggleDeleteModal,
                                    },
                                ]}
                            >
                                <IconButton
                                    size="small"
                                    variant="secondary"
                                    tooltip={t('global.options')}
                                    aria-label={t('action.show-options')}
                                >
                                    <OptionsIcon />
                                </IconButton>
                            </DropdownButton>
                        )}
                    </div>
                </div>
            )}

            <Modal
                isVisible={showDeleteModal}
                onClose={toggleDeleteModal}
                title={t('comments.delete')}
                size="small"
            >
                <DeleteForm entity={entity} onClose={toggleDeleteModal} />
            </Modal>

            {showReplies && (
                <div className="CommentReplies">
                    {isParent && !hideReply && (
                        <div style={{ marginTop: '8px' }}>
                            <AddCommentForm
                                viewer={viewer}
                                entity={entity}
                                group={group}
                                commentWithoutAccountEnabled={
                                    commentWithoutAccountEnabled
                                }
                                refetchQueries={refetchQueries}
                                returnFocusElement={refReplyButton}
                                isReply
                            />
                        </div>
                    )}
                    {hasReplies && (
                        <ol style={{ marginTop: '20px' }}>
                            {entity.comments.map((comment, i) => {
                                if (
                                    !!showAllReplies ||
                                    (!showAllReplies && i < 3)
                                ) {
                                    return (
                                        <Comment
                                            key={comment.guid}
                                            viewer={viewer}
                                            entity={comment}
                                            group={group}
                                            isClosed={isClosed}
                                            inActivityFeed={inActivityFeed}
                                            canUpvote={canUpvote}
                                            canChooseBestAnswer={
                                                entity.canChooseBestAnswer
                                            }
                                            commentWithoutAccountEnabled={
                                                commentWithoutAccountEnabled
                                            }
                                            refetchQueries={refetchQueries}
                                        />
                                    )
                                } else {
                                    return null
                                }
                            })}
                            {!showAllReplies && entity.comments.length > 3 && (
                                <li>
                                    <Button
                                        size="small"
                                        variant="tertiary"
                                        onClick={toggleShowAllReplies}
                                    >
                                        {t('comments.show-all-replies')}
                                    </Button>
                                </li>
                            )}
                        </ol>
                    )}
                </div>
            )}
        </Wrapper>
    )
}

const TOGGLE_BEST_ANSWER = gql`
    mutation CommentBestAnswer($input: toggleBestAnswerInput!) {
        toggleBestAnswer(input: $input) {
            entity {
                guid
            }
        }
    }
`

Comment.propTypes = {
    entity: PropTypes.object.isRequired,
    viewer: PropTypes.object,
    hideReply: PropTypes.bool,
    canUpvote: PropTypes.bool,
    inActivityFeed: PropTypes.bool,
    canChooseBestAnswer: PropTypes.bool,
    commentWithoutAccountEnabled: PropTypes.bool,
    highlightBestAnswer: PropTypes.bool,
    refetchQueries: PropTypes.array,
    disabled: PropTypes.bool, // Show a comment without editing capabilities.
}

export default Comment
