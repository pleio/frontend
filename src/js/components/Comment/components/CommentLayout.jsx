import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { MobileLandscapeUp, MobilePortrait } from 'helpers/mediaWrapper'
import styled from 'styled-components'

import CommentEditor from 'js/components/Comment/CommentEditor'
import DisplayDate from 'js/components/DisplayDate'
import Flexer from 'js/components/Flexer/Flexer'
import ShowMore from 'js/components/ShowMore'
import Tag from 'js/components/Tag/Tag'
import TiptapView from 'js/components/Tiptap/TiptapView'
import UserAvatar from 'js/components/UserAvatar'

const Wrapper = styled.div`
    display: flex;
`

const CommentLayout = ({
    viewer,
    entity,
    group,
    inActivityFeed,
    isEditing,
    setIsEditing,
    refEditButton,
    ...rest
}) => {
    const { t } = useTranslation()

    const hideEditor = () => {
        setIsEditing(false)
        setTimeout(() => {
            refEditButton?.current?.focus()
        }, 0)
    }

    const {
        owner,
        ownerName,
        pendingModeration,
        timeCreated,
        richDescription,
    } = entity

    const hasOwner = !!owner

    const name = owner ? owner.name : ownerName

    const avatar = (
        <div className="CommentAvatarWrapper">
            <UserAvatar
                user={hasOwner ? owner : { name }}
                className="CommentAvatar"
            />
        </div>
    )

    const NameElement = hasOwner ? Link : 'span'

    return (
        <Wrapper {...rest}>
            <MobileLandscapeUp>{avatar}</MobileLandscapeUp>
            <div className="CommentMain">
                {isEditing ? (
                    <CommentEditor
                        viewer={viewer}
                        entity={entity}
                        group={group}
                        isEditing
                        hideEditor={hideEditor}
                    />
                ) : (
                    <>
                        <div className="CommentMeta">
                            <MobilePortrait>{avatar}</MobilePortrait>
                            <div
                                style={{
                                    display: 'flex',
                                    flexWrap: 'wrap',
                                    alignItems: 'baseline',
                                }}
                            >
                                <NameElement
                                    to={hasOwner ? owner.url : null}
                                    className="CommentAuthor"
                                >
                                    {name}
                                </NameElement>
                                <DisplayDate
                                    date={timeCreated}
                                    type="timeSince"
                                    placement="right"
                                    className="CommentDate"
                                />
                            </div>
                        </div>
                        {pendingModeration && (
                            <Flexer justifyContent="flex-start">
                                <Tag>
                                    {t('content-moderation.publish-request')}
                                </Tag>
                            </Flexer>
                        )}
                        <ShowMore maxHeight={inActivityFeed ? 200 : 400}>
                            <TiptapView content={richDescription} />
                        </ShowMore>
                    </>
                )}
            </div>
        </Wrapper>
    )
}

export default CommentLayout
