import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'

const DeleteForm = ({ mutate, entity, onClose }) => {
    const [loading, setLoading] = useState(false)

    const onSubmit = (e) => {
        e.preventDefault()

        setLoading(true)

        const refetchQueries = ['FeedComments']

        mutate({
            variables: {
                input: {
                    guid: entity.guid,
                },
            },
            refetchQueries,
        }).then(() => {
            setLoading(false)
            onClose()
        })
    }

    const { t } = useTranslation()

    return (
        <form onSubmit={onSubmit}>
            <p>{t('comments.form-delete-caption')}</p>
            <Flexer mt>
                <Button
                    size="normal"
                    variant="tertiary"
                    type="button"
                    onClick={onClose}
                >
                    {t('action.no-back')}
                </Button>
                <Button
                    size="normal"
                    variant="primary"
                    type="submit"
                    loading={loading}
                >
                    {t('action.yes-delete')}
                </Button>
            </Flexer>
        </form>
    )
}

const Mutation = gql`
    mutation deleteEntity($input: deleteEntityInput!) {
        deleteEntity(input: $input) {
            success
        }
    }
`

export default graphql(Mutation)(DeleteForm)
