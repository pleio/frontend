import { media } from 'helpers'
import styled, { css } from 'styled-components'

export default styled.div`
    ${(p) =>
        p.$isParent &&
        css`
            padding: 20px;
        `};

    ${(p) =>
        p.$isParent &&
        p.$inActivityFeed &&
        css`
            border-top: 1px solid ${(p) => p.theme.color.grey[20]};
        `};

    ${(p) =>
        p.$isParent &&
        !p.$inActivityFeed &&
        css`
            margin-bottom: 20px;
        `};

    ${(p) =>
        p.$highlightBestAnswer &&
        css`
            box-shadow: 0 0 0 1px ${(p) => p.theme.color.secondary.main} !important;
        `};

    .CommentAvatarWrapper {
        align-self: flex-start;
        margin-right: 10px;

        ${media.mobilePortrait`
            margin-right: 8px;

            .CommentAvatar {
                width: 24px;
                height: 24px;
            }
        `};
    }

    .CommentMain {
        flex-grow: 1;
    }

    .CommentMeta {
        display: flex;
        align-items: center;
        margin-bottom: 2px;
    }

    .CommentAuthor {
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        margin-right: 6px;
        font-weight: ${(p) => p.theme.font.weight.semibold};
        color: ${(p) => p.theme.color.text.black};
    }

    .CommentDate {
        font-size: ${(p) => p.theme.font.size.tiny};
        line-height: ${(p) => p.theme.font.lineHeight.tiny};
        color: ${(p) => p.theme.color.text.grey};
    }

    .CommentFooter {
        display: flex;
        align-items: center;

        &.IsIndented {
            ${media.mobileLandscapeUp`
                padding-left: 42px;
            `};
        }
    }

    .BestAnswerButton {
        ${media.mobilePortrait`
            width: 24px;
            margin-right: 8px;
        `};

        ${media.mobileLandscapeUp`
            margin-left: 4px;
            margin-right: 14px;
        `};
    }

    .BestAnswerIndicator {
        display: flex;
        align-items: center;
        justify-content: center;
        margin-right: 10px;
        font-size: ${(p) => p.theme.font.size.tiny};
        line-height: ${(p) => p.theme.font.lineHeight.tiny};
        font-weight: ${(p) => p.theme.font.weight.semibold};
        color: ${(p) => p.theme.color.secondary.main};
        user-select: none;

        ${media.mobilePortrait`
            width: 24px;
        `};

        ${media.mobileLandscapeUp`
            width: 32px;
        `};
    }

    .CommentReplyButton {
        display: flex;
        align-items: center;
        height: 32px;
        font-size: ${(p) => p.theme.font.size.tiny};
        line-height: ${(p) => p.theme.font.lineHeight.tiny};
        font-weight: ${(p) => p.theme.font.weight.semibold};
        color: ${(p) => p.theme.color.text.black};
        border-radius: ${(p) => p.theme.radius.small};
        padding: 0 4px 0 0;

        &:hover {
            background-color: ${(p) => p.theme.color.hover};
        }

        &:active {
            background-color: ${(p) => p.theme.color.active};
        }

        svg {
            margin-right: 5px;
        }
    }

    .CommentActions {
        margin-left: auto;
        display: flex;
    }

    .CommentReplies {
        padding: 0 0 0 42px;
    }
`
