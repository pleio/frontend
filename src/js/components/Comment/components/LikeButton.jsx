import React from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation, useNavigate } from 'react-router-dom'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

import HideVisually from 'js/components/HideVisually/HideVisually'

import IconHeart from 'icons/heart-small.svg'
import IconHeartFill from 'icons/heart-small-fill.svg'
import IconThumbup from 'icons/thumbs-small.svg'
import IconThumbupFill from 'icons/thumbs-small-fill.svg'

const Wrapper = styled.div`
    display: flex;
    align-items: center;
    height: 24px;
    padding: 0 6px;
    font-family: ${(p) => p.theme.font.family};
    font-size: ${(p) => p.theme.font.size.tiny};
    font-weight: ${(p) => p.theme.font.weight.semibold};
    border-radius: 12px;

    ${(p) =>
        p.$hasLiked &&
        css`
            color: ${(p) => p.theme.color.secondary.main};
        `};

    ${(p) =>
        p.$canLike &&
        css`
            &:hover {
                background-color: ${(p) => p.theme.color.hover};
            }

            &:active {
                background-color: ${(p) => p.theme.color.active};
            }
        `};

    .LikeButtonLikes {
        margin-left: 4px;
    }
`

const LikeButton = ({ mutate, entity, viewer, isClosed }) => {
    const { loggedIn } = viewer

    const navigate = useNavigate()
    const location = useLocation()
    const { t } = useTranslation()

    const toggleLike = (evt) => {
        evt.preventDefault()

        const { guid, __typename, hasVoted } = entity

        mutate({
            variables: {
                input: {
                    guid,
                    score: hasVoted ? -1 : 1,
                },
            },
            ...(loggedIn
                ? {
                      optimisticResponse: {
                          vote: {
                              __typename: 'votePayload',
                              object: {
                                  __typename,
                                  guid,
                                  hasVoted: !hasVoted,
                                  votes: hasVoted
                                      ? entity.votes - 1
                                      : entity.votes + 1,
                              },
                          },
                      },
                  }
                : {}),
        }).catch((error) => {
            if (error.graphQLErrors[0].message === 'not_logged_in') {
                navigate('/login', {
                    state: { next: location.pathname },
                })
            }
        })
    }

    const thumbs = window.__SETTINGS__.site.likeIcon === 'thumbs'

    let Icon
    if (thumbs) {
        Icon = entity.hasVoted ? IconThumbupFill : IconThumbup
    } else {
        Icon = entity.hasVoted ? IconHeartFill : IconHeart
    }

    const hasVotes = entity.votes > 0

    const likeDisabled =
        isClosed || (!loggedIn && !window.__SETTINGS__.showLoginRegister)

    if (likeDisabled && !hasVotes) {
        return null
    } else {
        return (
            <>
                <Wrapper
                    {...(likeDisabled
                        ? {}
                        : {
                              as: 'button',
                              type: 'button',
                              onClick: toggleLike,
                          })}
                    $canLike={!likeDisabled}
                    $hasLiked={entity.hasVoted}
                >
                    <Icon style={thumbs ? {} : { marginTop: '1px' }} />
                    {hasVotes && (
                        <span className="LikeButtonLikes">
                            {`${entity.votes}`}
                            <HideVisually>
                                {`${t('comments.times-liked')}`}
                                <br />
                                {entity.hasVoted && `${t('comments.unlike')} `}
                            </HideVisually>
                        </span>
                    )}
                    {!entity.hasVoted && (
                        <HideVisually>{t('comments.add-like')}</HideVisually>
                    )}
                </Wrapper>
            </>
        )
    }
}

const Query = gql`
    mutation Likes($input: voteInput!) {
        vote(input: $input) {
            object {
                guid
                ... on Comment {
                    hasVoted
                    votes
                }
                ... on FileFolder {
                    hasVoted
                    votes
                }
                ... on Task {
                    hasVoted
                    votes
                }
                ... on Blog {
                    hasVoted
                    votes
                }
                ... on News {
                    hasVoted
                    votes
                }
                ... on Question {
                    hasVoted
                    votes
                }
                ... on Discussion {
                    hasVoted
                    votes
                }
                ... on StatusUpdate {
                    hasVoted
                    votes
                }
            }
        }
    }
`

LikeButton.propTypes = {
    entity: PropTypes.object.isRequired,
    viewer: PropTypes.object.isRequired,
    isClosed: PropTypes.bool,
}

export default graphql(Query)(LikeButton)
