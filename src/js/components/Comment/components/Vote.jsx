import React from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation, useNavigate } from 'react-router-dom'
import { gql, useMutation } from '@apollo/client'
import styled from 'styled-components'

import IconButton from 'js/components/IconButton/IconButton'

import ChevronDownIcon from 'icons/chevron-down.svg'
import ChevronUpIcon from 'icons/chevron-up.svg'

const Wrapper = styled.div`
    display: flex;
    align-items: center;

    .VoteCount {
        font-size: ${(p) => p.theme.font.size.small};
        user-select: none;
    }
`

const VoteUpDown = ({ entity, viewer, isClosed, mutate }) => {
    const { loggedIn } = viewer

    const navigate = useNavigate()
    const location = useLocation()

    const [mutateSubmitVote, { loading }] = useMutation(SUBMIT_VOTE)

    const submitVote = (score) => {
        mutateSubmitVote({
            variables: {
                input: {
                    guid: entity.guid,
                    score,
                },
            },
        }).catch((error) => {
            if (error.graphQLErrors[0].message === 'not_logged_in') {
                navigate('/login', {
                    state: { next: location.pathname },
                })
            }
        })
    }

    const voteDown = () => {
        submitVote(hasVotedDown ? 1 : -1)
    }

    const voteUp = () => {
        submitVote(hasVotedUp ? -1 : 1)
    }

    const { t } = useTranslation()

    const voteDisabled =
        loading ||
        isClosed ||
        (!loggedIn && !window.__SETTINGS__.showLoginRegister)

    const excerpt =
        entity.excerpt?.length > 30
            ? `${entity.excerpt.substring(0, 30)}...`
            : entity.excerpt

    const hasVotedDown = entity.hasVoted === -1
    const hasVotedUp = entity.hasVoted === 1

    return (
        <Wrapper $hasVoted={entity.hasVoted} aria-live="polite">
            <IconButton
                size="small"
                variant={hasVotedDown ? 'primary' : 'secondary'}
                aria-label={t(
                    hasVotedDown
                        ? 'comments.remove-downvote-from-item'
                        : 'comments.vote-item-down',
                    {
                        title: excerpt,
                    },
                )}
                onClick={voteDown}
                disabled={voteDisabled}
                tooltip={t(
                    hasVotedDown
                        ? 'comments.remove-downvote'
                        : 'comments.vote-down',
                )}
            >
                <ChevronDownIcon />
            </IconButton>
            <div className="VoteCount">{entity.votes}</div>
            <IconButton
                size="small"
                variant={hasVotedUp ? 'primary' : 'secondary'}
                aria-label={t(
                    hasVotedUp
                        ? 'comments.remove-upvote-from-item'
                        : 'comments.vote-item-up',
                    {
                        title: excerpt,
                    },
                )}
                onClick={voteUp}
                disabled={voteDisabled}
                tooltip={t(
                    hasVotedUp ? 'comments.remove-upvote' : 'comments.vote-up',
                )}
            >
                <ChevronUpIcon />
            </IconButton>
        </Wrapper>
    )
}

const SUBMIT_VOTE = gql`
    mutation submitVote($input: voteInput!) {
        vote(input: $input) {
            object {
                guid
                ... on Comment {
                    votes
                    hasVoted
                }
            }
        }
    }
`

export default VoteUpDown
