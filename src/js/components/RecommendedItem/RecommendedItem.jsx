import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import CoverImage from 'js/components/CoverImage'
import DisplayDate from 'js/components/DisplayDate'
import { H4 } from 'js/components/Heading'
import ToggleTranslation from 'js/components/Item/components/ToggleTranslation'
import TiptapView from 'js/components/Tiptap/TiptapView'
import Truncate from 'js/components/Truncate/Truncate'
import useSubtypes from 'js/lib/hooks/useSubtypes'

const Wrapper = styled.div`
    display: flex;

    &:not(:last-child) {
        margin-bottom: 8px;
    }

    .SuggestedItemTitle {
        width: 100%;
        display: block;
        margin-right: 4px;
        color: ${(p) => p.theme.color.primary.main};

        &:hover {
            text-decoration: underline;
        }
    }

    .SuggestedItemMeta {
        margin-bottom: 8px;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        color: ${(p) => p.theme.color.text.grey};
    }

    .SuggestedItemImage {
        flex-shrink: 0;
        width: 60px;
        height: 60px;
        margin-left: 10px;
        border-radius: ${(p) => p.theme.radius.tiny};
        overflow: hidden;
    }

    .SuggestedItemText {
        flex-grow: 1;
    }
`

const RecommendedItem = ({
    entity,
    showMeta,
    showExcerpt,
    truncateTitle,
    ...rest
}) => {
    const { t } = useTranslation()

    const { subtypes } = useSubtypes()

    const {
        localTitle,
        title,
        url,
        timePublished,
        subtype,
        featured,
        localExcerpt,
        excerpt,
        isTranslationEnabled,
    } = entity

    const hasTranslations = !!localTitle && isTranslationEnabled

    const [isTranslated, setIsTranslated] = useState(hasTranslations)

    const translatedTitle = isTranslated ? localTitle : title
    const translatedExcerpt = isTranslated ? localExcerpt : excerpt

    return (
        <Wrapper {...rest}>
            <div className="SuggestedItemText">
                <H4 className="SuggestedItemTitle" as={Link} to={url}>
                    {truncateTitle ? (
                        <Truncate lines={1}>{translatedTitle}</Truncate>
                    ) : (
                        translatedTitle
                    )}
                </H4>
                {showMeta && (
                    <div className="SuggestedItemMeta">
                        <DisplayDate
                            date={timePublished}
                            type="timeSince"
                            placement="bottom"
                            hiddenPrefix={t('global.publication-date')}
                            isFocusable
                        />
                        <>&nbsp;&nbsp;•&nbsp;&nbsp;</>
                        <span>{subtypes[subtype].contentName}</span>
                    </div>
                )}
                {showExcerpt && (
                    <Truncate lines={2}>
                        <TiptapView
                            content={translatedExcerpt}
                            contentType={'html'}
                            textSize="small"
                        />
                    </Truncate>
                )}
                {hasTranslations && (
                    <ToggleTranslation
                        isTranslated={isTranslated}
                        setIsTranslated={setIsTranslated}
                        style={{ marginTop: '4px' }}
                    />
                )}
            </div>
            <CoverImage
                url={url}
                featured={featured}
                size="cover"
                videoButtonSize="small"
                hideCaption
                className="SuggestedItemImage"
            />
        </Wrapper>
    )
}

export default RecommendedItem
