import React from 'react'
import styled, { css } from 'styled-components'

import Avatar from 'js/components/Avatar/Avatar'

const Wrapper = styled.div`
    display: flex;
    align-items: center;

    ${(p) =>
        p.$isLink &&
        css`
            .UserLinkName {
                color: ${(p) => p.theme.color.primary.main};
            }

            &:hover .UserLinkName {
                text-decoration: underline;
            }
        `};

    .UserLinkAvatar {
        ${(p) =>
            p.$avatarSize === 'small'
                ? css`
                      margin-right: 8px;
                  `
                : css`
                      margin-right: 12px;
                  `};
    }

    .UserLinkName {
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
    }
`

const UserLink = ({ entity, role, isEmail, avatarSize, children, ...rest }) => {
    const { url, icon, name } = entity
    const isLink = !!url

    return (
        <Wrapper
            $isLink={isLink}
            $avatarSize={avatarSize}
            as={isLink ? 'a' : 'div'}
            href={isLink ? url : null}
            target={isLink ? '_blank' : null}
            rel={isLink ? 'noopener noreferrer' : null}
            {...rest}
        >
            <Avatar
                image={icon}
                name={name}
                isEmail={isEmail}
                role={role}
                disabled
                className="UserLinkAvatar"
                size={avatarSize}
            />
            <div>
                <div className="UserLinkName">{name}</div>
                {children}
            </div>
        </Wrapper>
    )
}

export default UserLink
