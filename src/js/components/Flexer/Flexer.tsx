import React, { HTMLAttributes } from 'react'
import styled, { css } from 'styled-components'

import shouldForwardProp from 'js/lib/helpers/shouldForwardProp'

const gutterSize = {
    none: 0,
    tiny: 4,
    small: 8,
    normal: 12,
}

export interface Props extends HTMLAttributes<HTMLDivElement> {
    alignItems?: string
    justifyContent?: string
    mt?: boolean
    wrap?: boolean
    divider?: 'none' | 'small' | 'normal' | 'large'
    gutter?: 'none' | 'tiny' | 'small' | 'normal'
    children: React.ReactNode
    as?: string
}

const Flexer = styled.div
    .withConfig({
        shouldForwardProp: shouldForwardProp([
            'alignItems',
            'justifyContent',
            'mt',
            'wrap',
            'divider',
            'gutter',
        ]),
    })
    .attrs((props) => ({
        alignItems: 'center',
        justifyContent: 'center',
        gutter: 'normal',
        divider: 'none',
        ...props,
    }))<Props>`
    display: flex;
    align-items: ${(p) => p.alignItems};
    justify-content: ${(p) => p.justifyContent};

    ${(p) =>
        p.mt &&
        css`
            margin-top: 24px;
        `};

    ${(p) =>
        p.wrap &&
        css<Props>`
            flex-wrap: wrap;

            ${(p) =>
                p.divider === 'none' &&
                p.gutter !== 'none' &&
                css<Props>`
                    margin-bottom: -${(p) => gutterSize[p.gutter]}px;

                    > * {
                        margin-bottom: ${(p) => gutterSize[p.gutter]}px;
                    }
                `};
        `};

    > *:not(:last-child) {
        ${(p) =>
            p.divider === 'none' &&
            p.gutter !== 'none' &&
            css<Props>`
                margin-right: ${(p) => gutterSize[p.gutter]}px;
            `};

        ${(p) =>
            p.divider !== 'none' &&
            css<Props>`
                position: relative;
                margin-right: ${(p) =>
                    gutterSize[p.gutter] +
                    1}px; // Add one pixel to gutter space for the divider

                &:after {
                    content: '';
                    position: absolute;
                    right: -${(p) => (gutterSize[p.gutter] + 2) / 2}px; // Place divider in the middle of the gutter
                    width: 1px;
                    background-color: ${(p) => p.theme.color.grey[30]};
                    pointer-events: none;

                    ${(p) =>
                        p.divider === 'small' &&
                        css`
                            top: 16px;
                            bottom: 16px;
                        `};

                    ${(p) =>
                        p.divider === 'normal' &&
                        css`
                            top: 8px;
                            bottom: 8px;
                        `};

                    ${(p) =>
                        p.divider === 'large' &&
                        css`
                            top: 0;
                            bottom: 0;
                        `};
                }
            `};
    }
`

export default Flexer
