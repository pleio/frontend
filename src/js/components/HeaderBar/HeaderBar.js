import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

const Header = styled.div.attrs((props) => ({
    $backgroundColor: 'default',
    ...props,
}))`
    ${(p) =>
        p.$backgroundColor === 'default' &&
        css`
            background-color: ${(p) => p.theme.color.header.main};
        `}

    ${(p) =>
        p.$backgroundColor === 'white' &&
        css`
            background-color: white;
            box-shadow: ${(p) => p.theme.shadow.hard.bottom};
        `}
`

Header.propTypes = {
    $backgroundColor: PropTypes.oneOf(['default', 'white']),
}

export default Header
