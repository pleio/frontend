import React from 'react'
import { useTranslation } from 'react-i18next'
import { formatISO, isSameDay, parseISO, set } from 'date-fns'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import DateField from 'js/components/DateField/DateField'
import TimeField from 'js/components/TimeField/TimeField'
import { showTime } from 'js/lib/helpers/date/showDate'

const Wrapper = styled.div`
    display: flex;

    > :nth-child(1) {
        width: 60%;
        min-width: 144px;

        .TextFieldWrapper {
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
        }
    }

    > :nth-child(2) {
        width: 40%;
        min-width: 66px;

        .TextFieldWrapper {
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
            border-left: none;
        }
    }
`

const DateTimeField = ({
    name,
    labelPrefix,
    value,
    disabled,
    fromDateTime,
    toDateTime,
    onChange,
    isClearable,
    timeSpan,
    defaultMonth,
    ...rest
}) => {
    const handleChangeTime = (val) => {
        const [hour, minute] = val.split(':')

        onChange(
            formatISO(
                set(parseISO(value), {
                    hours: parseInt(hour, 10),
                    minutes: parseInt(minute, 10),
                }),
            ),
        )
    }

    const { t } = useTranslation()

    return (
        <Wrapper {...rest}>
            <DateField
                name={name}
                label={
                    <>
                        {labelPrefix}
                        {t('global.date')}
                    </>
                }
                value={value}
                disabled={disabled}
                fromDate={fromDateTime}
                toDate={toDateTime}
                onChange={onChange}
                isClearable={isClearable}
                defaultMonth={defaultMonth}
            />
            <TimeField
                name={`${name}-time`}
                label={
                    <>
                        {labelPrefix}
                        {t('entity-event.time')}
                    </>
                }
                value={value ? showTime(value) : ''}
                disabled={disabled || !value}
                fromTime={
                    !!value && !!fromDateTime && isSameDay(value, fromDateTime)
                        ? showTime(fromDateTime)
                        : ''
                }
                toTime={
                    !!value && !!toDateTime && isSameDay(value, toDateTime)
                        ? showTime(toDateTime)
                        : ''
                }
                onChange={handleChangeTime}
                timeSpan={timeSpan}
            />
        </Wrapper>
    )
}

DateTimeField.propTypes = {
    name: PropTypes.string,
    value: PropTypes.string, // ISOString
    fromDateTime: PropTypes.string, // ISOString
    toDateTime: PropTypes.string, // ISOString
    onChange: PropTypes.func,
    onClear: PropTypes.func,
}

export default DateTimeField
