import React from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import PropTypes from 'prop-types'

import ToggleButton from 'js/components/ToggleButton/ToggleButton'

import IconHeart from 'icons/heart.svg'
import IconHeartFill from 'icons/heart-fill.svg'
import IconThumbup from 'icons/thumbs.svg'
import IconThumbupFill from 'icons/thumbs-fill.svg'

const Likes = ({ mutate, entity }) => {
    const navigate = useNavigate()
    const { t } = useTranslation()

    if (!entity) return null

    const { votes, hasVoted, title } = entity

    const toggleLike = () => {
        const { __typename, guid, hasVoted: previousVote, votes } = entity
        const hasVoted = !previousVote
        const score = hasVoted ? 1 : -1

        mutate({
            variables: {
                input: {
                    guid,
                    score,
                },
            },

            optimisticResponse: {
                vote: {
                    __typename: 'votePayload',
                    object: {
                        __typename,
                        guid,
                        hasVoted,
                        votes: votes + score,
                    },
                },
            },
        }).catch((error) => {
            if (error.graphQLErrors[0].message === 'not_logged_in') {
                navigate('/login', {
                    state: { next: location.pathname },
                })
            }
        })
    }

    const hasThumbAsLike = window.__SETTINGS__.site.likeIcon === 'thumbs'
    const likeText = t('global.likes', { count: votes })
    const ariaLabel = hasVoted
        ? `${likeText} - ${t('action.unlike-item', { title })}`
        : `${likeText} - ${t('action.like-item', { title })}`

    return (
        <ToggleButton
            isEnabled={!!hasVoted}
            onClick={toggleLike}
            enabledContent={likeText}
            disabledContent={likeText}
            EnabledIcon={hasThumbAsLike ? IconThumbupFill : IconHeartFill}
            DisabledIcon={hasThumbAsLike ? IconThumbup : IconHeart}
            aria-label={ariaLabel}
        />
    )
}

Likes.propTypes = {
    entity: PropTypes.object.isRequired,
}

const Mutation = gql`
    mutation Likes($input: voteInput!) {
        vote(input: $input) {
            object {
                guid
                ... on Comment {
                    hasVoted
                    votes
                }
                ... on FileFolder {
                    hasVoted
                    votes
                }
                ... on Task {
                    hasVoted
                    votes
                }
                ... on Blog {
                    hasVoted
                    votes
                }
                ... on News {
                    hasVoted
                    votes
                }
                ... on Question {
                    hasVoted
                    votes
                }
                ... on Discussion {
                    hasVoted
                    votes
                }
                ... on StatusUpdate {
                    hasVoted
                    votes
                }
                ... on Podcast {
                    hasVoted
                    votes
                }
            }
        }
    }
`

export default graphql(Mutation)(Likes)
