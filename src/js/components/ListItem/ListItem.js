import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

const ListItem = styled.div.attrs((props) => ({ $size: 'small', ...props }))`
    width: 100%;
    display: flex;
    align-items: center;

    font-family: ${(p) => p.theme.font.family};
    font-size: ${(p) => p.theme.font.size[p.$size]};
    line-height: ${(p) => p.theme.font.lineHeight[p.$size]};
    color: ${(p) => p.theme.menu.color};

    ${(p) =>
        p.$size === 'tiny' &&
        css`
            padding: 2px 12px;
        `};

    ${(p) =>
        p.$size === 'small' &&
        css`
            padding: 4px 16px;
        `};

    text-align: left;
    text-decoration: none;
    text-shadow: none;
    user-select: none;

    white-space: ${(p) => !!p.$nowrap && 'nowrap'};

    &:not([disabled]) {
        &:hover {
            background-color: ${(p) => p.theme.menu.hover};
        }

        &:active {
            background-color: ${(p) => p.theme.menu.active};
        }
    }

    &[aria-current],
    &[aria-selected='true'] {
        color: ${(p) => p.theme.menu.current};
        text-decoration: underline;
    }

    ${(p) =>
        p.$showBorder &&
        css`
            border-bottom: 1px solid ${(p) => p.theme.menu.divider};
        `}

    ${(p) =>
        p.$warn &&
        css`
            color: ${(p) => p.theme.color.warn.main};
        `}
`

ListItem.propTypes = {
    $size: PropTypes.oneOf(['tiny', 'small']),
}

export default ListItem
