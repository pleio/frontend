import React, { useState } from 'react'

import Element from './components/Element'
import Placeholder from './components/Placeholder'
import Responsive from './components/Responsive'

export interface Props {
    src: string
    alt?: string
    align?: 'right' | 'center'
    objectFit?: 'cover' | 'contain'
    objectPosition?: string
    fullSize?: boolean
    showLoadingPlaceholder?: boolean
    onLoad?: (evt: Event) => void
    style?: object
    className?: string
}

const Image = ({
    src,
    alt = '',
    fullSize,
    align,
    objectFit,
    objectPosition,
    showLoadingPlaceholder = false,
    onLoad,
    ...rest
}: Props) => {
    const [notFound, setNotFound] = useState(false)
    const [isLoaded, setIsLoaded] = useState(false)

    const handleOnLoad = (evt) => {
        onLoad?.(evt)
        setIsLoaded(true)
    }

    if (!src) return null

    if (notFound) {
        return <Placeholder status="not-found" />
    }

    const imageProps = {
        src,
        alt,
        objectFit,
        objectPosition,
        showLoadingPlaceholder,
        setNotFound,
        isLoaded,
        onLoad: handleOnLoad,
    }

    if (fullSize) {
        return (
            <div {...rest}>
                <Element {...imageProps} />
            </div>
        )
    } else {
        return <Responsive align={align} {...imageProps} {...rest} />
    }
}

export default React.memo(
    Image,
    (prev, next) =>
        prev.src === next.src &&
        prev.alt === next.alt &&
        prev.fullSize === next.fullSize &&
        prev.objectPosition === next.objectPosition,
)
