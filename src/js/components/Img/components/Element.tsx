import React, { Dispatch, ImgHTMLAttributes, SetStateAction } from 'react'
import styled, { css } from 'styled-components'

import { Props as ImgProps } from 'js/components/Img/Img'
import shouldForwardProp from 'js/lib/helpers/shouldForwardProp'

import ImagePlaceholder from './Placeholder'

export interface Props extends ImgProps {
    setNotFound: Dispatch<SetStateAction<boolean>>
    onLoad: (Event) => void
    isLoaded: boolean
}

interface WrapperProps extends ImgHTMLAttributes<HTMLImageElement> {
    objectFit: 'cover' | 'contain'
    objectPosition: string
}

const Wrapper = styled.img.withConfig({
    shouldForwardProp: shouldForwardProp(['objectFit', 'objectPosition']),
})<WrapperProps>`
    border: none;
    height: auto;
    max-width: 100%;
    max-height: 100%;
    object-position: ${(p) => p.objectPosition};

    ${(p) =>
        p.objectFit &&
        css`
            object-fit: ${p.objectFit};
            width: 100%;
            height: 100%;
        `};
`

const Element = ({
    src,
    alt,
    objectFit,
    objectPosition,
    showLoadingPlaceholder,
    setNotFound,
    onLoad,
    isLoaded,
}: Props) => (
    <>
        {!isLoaded && showLoadingPlaceholder && (
            <ImagePlaceholder status="loading" />
        )}
        <Wrapper
            src={src}
            alt={alt}
            onError={() => setNotFound(true)}
            onLoad={onLoad}
            objectFit={objectFit}
            objectPosition={objectPosition}
            style={
                isLoaded
                    ? {}
                    : {
                          display: 'none',
                      }
            }
        />
    </>
)

export default Element
