import React from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import HideVisually from 'js/components/HideVisually/HideVisually'

import ImageIcon from 'icons/image.svg'
import ImageNotFoundIcon from 'icons/image-not-found.svg'

interface Props {
    status: 'loading' | 'not-found'
}

const Wrapper = styled.div`
    position: relative;
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    .ImagePlaceholderText {
        margin: 8px 0 -4px;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }
`

const Placeholder = ({ status }: Props) => {
    const { t } = useTranslation()

    const Icon =
        status === 'not-found' ? (
            <ImageNotFoundIcon />
        ) : status === 'loading' ? (
            <ImageIcon />
        ) : null

    const text =
        status === 'not-found'
            ? t('error.image-not-found')
            : status === 'loading'
              ? `${t('status.loading')}...`
              : null

    return (
        <Wrapper>
            {Icon}
            <HideVisually>{text}</HideVisually>
        </Wrapper>
    )
}

export default Placeholder
