import React, { HTMLAttributes, useEffect, useState } from 'react'
import { useMeasure } from 'react-use'
import styled, { css } from 'styled-components'

import shouldForwardProp from 'js/lib/helpers/shouldForwardProp'

import ImageElement, { Props } from './Element'

const getSizeParameter = (width: number) => {
    const widths = [360, 414, 660, 1040, 1400]
    for (const w of widths) {
        if (width <= w) return w
    }
    return 'full'
}

interface WrapperProps extends HTMLAttributes<HTMLDivElement> {
    align: 'center' | 'right'
}

const Wrapper = styled.div.withConfig({
    shouldForwardProp: shouldForwardProp(['align']),
})<WrapperProps>`
    position: relative;
    display: flex;
    align-items: center;

    ${(p) =>
        p.align === 'right' &&
        css`
            justify-content: flex-end;
        `};

    ${(p) =>
        p.align === 'center' &&
        css`
            justify-content: center;
        `};
`

/*
    Don't flex this component, width could then return 0.
*/
const Responsive = ({
    src,
    setNotFound,
    onLoad,
    alt,
    align,
    objectFit,
    objectPosition,
    showLoadingPlaceholder,
    isLoaded,
    ...rest
}: Props) => {
    const [ref, { width }] = useMeasure()

    const [sizeParam, setSizeParam] = useState(null)

    useEffect(() => {
        if (!width) return
        setSizeParam(getSizeParameter(width))
    }, [width])

    const hasParam = src.indexOf('?') !== -1
    const url = `${src}${hasParam ? '&' : '?'}`

    return (
        <Wrapper ref={ref} align={align} {...rest}>
            {/* Wait until size parameter is known before loading the image */}
            {sizeParam && (
                <ImageElement
                    src={`${url}size=${sizeParam}`}
                    setNotFound={setNotFound}
                    onLoad={onLoad}
                    alt={alt}
                    objectFit={objectFit}
                    objectPosition={objectPosition}
                    isLoaded={isLoaded}
                    showLoadingPlaceholder={showLoadingPlaceholder}
                />
            )}
        </Wrapper>
    )
}

export default Responsive
