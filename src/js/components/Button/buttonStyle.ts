import { getReadableColor } from 'helpers/getContrast'
import { css } from 'styled-components'

import { StyledButtonProps } from './Button'

const buttonStyle = css<StyledButtonProps>`
    max-width: 100%;
    background-color: ${(p) => p.theme.button.bg};
    font-family: ${(p) => p.theme.font.family};
    font-weight: ${(p) => p.theme.font.weight.semibold};

    text-shadow: none;
    white-space: nowrap;
    overflow: hidden;

    &[disabled] {
        ${(p) =>
            !p.hideContent &&
            css`
                background-color: ${(p) => p.theme.button.disabled.bg};
                color: ${(p) => p.theme.button.disabled.color};
                border-color: transparent;
            `};
    }

    > .ButtonContent {
        width: 100%;
        height: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
        position: relative; // Content stays on top of transparent layer
        transition: opacity ${(p) => p.theme.transition.normal};
    }

    ${(p) =>
        p.hideContent &&
        css`
            pointer-events: none;
            cursor: default;

            > .ButtonContent {
                opacity: 0;
            }
        `};

    ${(p) =>
        p.size === 'small' &&
        css`
            height: 24px;
            padding: 0 6px;
            border-radius: ${(p) => p.theme.radius.small};
            font-size: ${(p) => p.theme.font.size.tiny};
        `};

    ${(p) =>
        p.size === 'normal' &&
        css`
            height: 32px;
            padding: 0 10px;
            border-radius: ${(p) => p.theme.radius.normal};
            font-size: ${(p) => p.theme.font.size.small};
        `};

    ${(p) =>
        p.size === 'large' &&
        css`
            height: 40px;
            padding: 0 14px;
            border-radius: ${(p) => p.theme.radius.large};
            font-size: ${(p) => p.theme.font.size.normal};
        `};

    ${(p) =>
        p.variant === 'primary' &&
        css`
            background-color: ${(p) => p.theme.color.secondary.main};
            color: ${(p) => p.theme.button.primary.color};
            border: 1px solid transparent;
            outline-offset: 2px;

            &:hover {
                background-color: ${(p) => p.theme.color.secondary.hover};
            }

            &:active {
                background-color: ${(p) => p.theme.color.secondary.active};
            }
        `};

    ${(p) =>
        p.variant === 'secondary' &&
        css`
            color: ${(p) => p.theme.color.secondary.main};
            border: 1px solid ${(p) => p.theme.color.secondary.main};

            &:hover {
                &:before {
                    background-color: ${(p) => p.theme.color.hover};
                }
            }

            &:active {
                &:before {
                    background-color: ${(p) => p.theme.color.active};
                }
            }
        `};

    ${(p) =>
        p.variant === 'tertiary' &&
        css`
            color: ${(p) => p.theme.color.text.black};
            border: 1px solid ${(p) => p.theme.color.grey[40]};

            &:hover {
                &:before {
                    background-color: ${(p) => p.theme.color.hover};
                }
            }

            &:active {
                &:before {
                    background-color: ${(p) => p.theme.color.active};
                }
            }
        `}

    ${(p) =>
        p.variant === 'warn' &&
        css`
            background-color: ${(p) => p.theme.color.warn.main};
            color: ${(p) => getReadableColor(p.theme.color.warn.main)};
            border: 1px solid transparent;
            outline-offset: 2px;

            &:hover {
                background-color: ${(p) => p.theme.color.warn.hover};
            }

            &:active {
                background-color: ${(p) => p.theme.color.warn.active};
            }
        `};

    ${(p) =>
        p.variant === 'warn-secondary' &&
        css`
            color: ${(p) => p.theme.color.warn.main};
            border: 1px solid ${(p) => p.theme.color.warn.main};

            &:hover {
                &:before {
                    background-color: ${(p) => p.theme.color.hover};
                }
            }

            &:active {
                &:before {
                    background-color: ${(p) => p.theme.color.active};
                }
            }
        `};

    ${(p) =>
        p.outline === false && // TODO: Is this variant=quarternary? But difficult to type..
        css`
            border: none;
        `};
`

export default buttonStyle
