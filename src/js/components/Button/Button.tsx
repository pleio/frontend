import React, {
    ForwardedRef,
    forwardRef,
    HTMLAttributes,
    useState,
} from 'react'
import handleInputPromise from 'helpers/handleInputPromise'
import styled, { useTheme } from 'styled-components'

import Loader from 'js/components/Form/Loader'
import { getReadableColor } from 'js/lib/helpers/getContrast'
import shouldForwardProp from 'js/lib/helpers/shouldForwardProp'

import BaseButton from '../BaseButton/BaseButton'

import buttonStyle from './buttonStyle'

export interface Props extends HTMLAttributes<HTMLButtonElement> {
    size?: 'small' | 'normal' | 'large'
    variant?: 'primary' | 'secondary' | 'tertiary' | 'warn' | 'warn-secondary'
    outline?: boolean
    children: React.ReactNode
    onClick?: () => void
    onHandle?: () => void
    disabled?: boolean
    loading?: boolean
    isSubmitted?: boolean
    type?: string
    as?: React.ElementType
    to?: string
    state?: any // react-router-dom also refers to state as any
}

// The styled(BaseButton) has one extra calculated prop hideContent
export interface StyledButtonProps extends Props {
    hideContent: boolean
}

const StyledButton = styled(BaseButton)
    .withConfig({
        shouldForwardProp: shouldForwardProp([
            'outline',
            'hideContent',
            'size',
            'variant',
            'isSubmitted',
        ]),
    })
    .attrs((props) => ({ hideContent: false, ...props }))<StyledButtonProps>`
    ${buttonStyle};
`

// forwardRef is needed for button as tippy trigger
const Button = forwardRef<HTMLButtonElement, Props>(
    (
        {
            children,
            disabled = false,
            isSubmitted = false,
            loading = false,
            outline = true,
            onClick,
            onHandle,
            size = 'normal',
            variant = 'primary',
            ...rest
        }: Props,
        ref: ForwardedRef<HTMLButtonElement>,
    ) => {
        const theme = useTheme()

        const [isLoading, setisLoading] = useState(false)
        const [isSaved, setIsSaved] = useState(false)

        const handleClick = () => {
            !!onHandle &&
                handleInputPromise(onHandle(), setisLoading, setIsSaved)
        }

        const isOccupied = isLoading || isSaved || loading || isSubmitted

        return (
            <StyledButton
                ref={ref}
                size={size}
                variant={variant}
                outline={outline}
                hideContent={isOccupied}
                disabled={isOccupied || disabled}
                onClick={onClick || handleClick}
                {...rest}
            >
                <Loader
                    color={
                        variant === 'primary'
                            ? theme.button.bg
                            : variant === 'warn'
                              ? getReadableColor(theme.color.warn.main)
                              : theme.color.secondary.main
                    }
                    position="center"
                    isLoading={isLoading || loading}
                    isSaved={isSaved || isSubmitted}
                />
                <div className="ButtonContent">{children}</div>
            </StyledButton>
        )
    },
)

Button.displayName = 'Button'

export default Button
