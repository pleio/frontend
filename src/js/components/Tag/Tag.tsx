import React from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import HideVisually from 'js/components/HideVisually/HideVisually'
import IconButton from 'js/components/IconButton/IconButton'

import CrossIcon from 'icons/cross-tiny.svg'

const Wrapper = styled.div`
    display: flex;
    background-color: transparent;
    border: 1px solid ${(p) => p.theme.color.grey[40]};
    border-radius: ${(p) => p.theme.radius.tiny};
    overflow: hidden;

    .TagLabel {
        padding: 1px 6px;
        font-family: ${(p) => p.theme.font.family};
        font-size: ${(p) => p.theme.font.size.tiny};
        font-weight: ${(p) => p.theme.font.weight.normal};
        line-height: 16px;
        color: ${(p) => p.theme.color.text.black};
    }

    .TagRemoveButton {
        margin-left: -8px;
        flex-shrink: 0;
        width: 18px;
        background-color: transparent;
    }
`

export interface Props {
    children: React.ReactNode
    tabIndex?: number
    textRemoveLabel?: string
    tooltip?: string
    disabled?: boolean
    onRemove?: () => void
}

const Tag = ({
    children,
    tabIndex = null,
    textRemoveLabel,
    tooltip,
    disabled,
    onRemove,
    ...rest
}: Props) => {
    const { t } = useTranslation()

    return (
        <Wrapper {...rest}>
            <span className="TagLabel">{children}</span>
            {onRemove && (
                <IconButton
                    className="TagRemoveButton"
                    tabIndex={tabIndex}
                    onClick={onRemove}
                    disabled={disabled}
                    variant="secondary"
                    tooltip={tooltip}
                    radiusStyle="none"
                    placement="right"
                >
                    <CrossIcon />
                    <HideVisually>
                        {textRemoveLabel ??
                            t('form.remove-tag', { tag: children })}
                    </HideVisually>
                </IconButton>
            )}
        </Wrapper>
    )
}

export default Tag
