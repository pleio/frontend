import React, { useEffect, useState } from 'react'
import { isAfter, subDays } from 'date-fns'
import showDate, {
    getDistanceToNow,
    showDateTime,
    showShortDate,
    showTime,
} from 'helpers/date/showDate'
import { Placement } from 'tippy.js'

import Tooltip from 'js/components/Tooltip/Tooltip'

import HideVisually from './HideVisually/HideVisually'

export interface Props {
    date: string
    placement?: Placement
    type: 'shortDate' | 'date' | 'timeSince' | 'dateTime' | 'time'
    hiddenPrefix?: string // hidden date prefix for screen readers
    isFocusable?: boolean // make date focusable for keyboard users
    disableTooltip?: boolean
}

const DisplayDate = ({
    date,
    placement = 'left',
    type = 'shortDate',
    hiddenPrefix,
    isFocusable,
    disableTooltip,
    ...rest
}: Props) => {
    const [dateState, setDateState] = useState<string>()

    useEffect(() => {
        if (!date) {
            setDateState(null) // Reset
            return
        }

        if (type === 'shortDate') {
            setDateState(showShortDate(date))
        }
        if (type === 'date') {
            setDateState(showDate(date, true))
        }
        if (type === 'dateTime') {
            setDateState(showDateTime(date))
        }
        if (type === 'time') {
            setDateState(showTime(date))
        }
        if (type === 'timeSince') {
            const eightDaysAgo = subDays(new Date(), 8)
            if (isAfter(date, eightDaysAgo)) {
                // show passed time since date
                setDateState(getDistanceToNow(date))

                const interval = setInterval(() => {
                    setDateState(getDistanceToNow(date))
                }, 1000)

                return () => {
                    clearInterval(interval)
                }
            } else {
                setDateState(showDate(date))
            }
        }
    }, [date, type])

    if (!dateState) return null

    const prefix = hiddenPrefix ? `${hiddenPrefix}: ` : ''

    return (
        <Tooltip
            disabled={disableTooltip || type === 'dateTime'} // Full date is already shown in the content
            content={showDateTime(date)}
            placement={placement}
        >
            {/* eslint-disable-next-line jsx-a11y/no-noninteractive-tabindex */}
            <span tabIndex={isFocusable ? 0 : null} {...rest}>
                <time dateTime={date}>
                    <span aria-hidden>{dateState}</span>
                    <HideVisually>
                        {prefix}
                        {showDateTime(date)}
                    </HideVisually>
                </time>
            </span>
        </Tooltip>
    )
}

export default DisplayDate
