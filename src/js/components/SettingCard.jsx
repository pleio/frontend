import { media } from 'helpers'
import styled from 'styled-components'

import Card from 'js/components/Card/Card'

export default styled(Card)`
    width: auto !important;

    ${media.mobilePortrait`
        padding: 0 ${(p) => p.theme.padding.horizontal.small} 32px;
        margin-left: -20px;
        margin-right: -20px;
        border-radius: 0 !important;
        box-shadow: none !important;

        &:not(:last-child) {
            border-bottom: 1px solid ${(p) =>
                p.theme.color.grey[30]} !important;
        }
    `}

    ${media.mobileLandscapeUp`
        box-shadow: 0 0 0 1px ${(p) => p.theme.color.grey[30]};
        padding: ${(p) => p.theme.padding.vertical.normal} ${(p) =>
            p.theme.padding.horizontal.normal};
    `}
`
