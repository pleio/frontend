import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import PropTypes from 'prop-types'

import ToggleButton from 'js/components/ToggleButton/ToggleButton'

import IconBookmark from 'icons/save.svg'
import IconBookmarkFill from 'icons/save-fill.svg'

const Bookmark = ({ mutate, entity }) => {
    const { t } = useTranslation()

    const { __typename, guid, canBookmark, isBookmarked } = entity

    if (!canBookmark) return null

    const toggleBookmark = (evt) => {
        evt.preventDefault()

        const isAdding = !isBookmarked

        mutate({
            variables: {
                input: {
                    guid,
                    isAdding,
                },
            },

            refetchQueries: ['BookmarkList'],

            optimisticResponse: {
                bookmark: {
                    __typename: 'bookmarkPayload',
                    object: {
                        __typename,
                        guid,
                        isBookmarked: isAdding,
                    },
                },
            },
        })
    }

    return (
        <ToggleButton
            isEnabled={entity.isBookmarked}
            onClick={toggleBookmark}
            enabledContent={t('saved.title')}
            disabledContent={t('saved.save')}
            EnabledIcon={IconBookmarkFill}
            DisabledIcon={IconBookmark}
        />
    )
}

Bookmark.propTypes = {
    entity: PropTypes.object.isRequired,
}

const Mutation = gql`
    mutation Bookmark($input: bookmarkInput!) {
        bookmark(input: $input) {
            object {
                guid
                ... on News {
                    isBookmarked
                }
                ... on Blog {
                    isBookmarked
                }
                ... on Discussion {
                    isBookmarked
                }
                ... on Event {
                    isBookmarked
                }
                ... on FileFolder {
                    isBookmarked
                }
                ... on Wiki {
                    isBookmarked
                }
                ... on Question {
                    isBookmarked
                }
                ... on StatusUpdate {
                    isBookmarked
                }
                ... on Podcast {
                    isBookmarked
                }
            }
        }
    }
`

export default graphql(Mutation)(Bookmark)
