import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

import AnimatePresence from 'js/components/AnimatePresence/AnimatePresence'
import { H3 } from 'js/components/Heading'

import ChevronDownIcon from 'icons/chevron-down.svg'
import ChevronUpIcon from 'icons/chevron-up.svg'

const Wrapper = styled.button`
    display: flex;
    align-items: center;
    width: 100%;

    ${(p) =>
        p.$align === 'left' &&
        css`
            justify-content: space-between;
        `};

    ${(p) =>
        p.$align === 'center' &&
        css`
            justify-content: center;
        `};

    .AdvancedSectionButton {
        display: flex;
        align-items: center;
        justify-content: center;

        width: 32px;
        height: 32px;
        border-radius: 50%;
    }

    &:hover .AdvancedSectionButton {
        background-color: ${(p) => p.theme.color.hover};
    }

    &:active .AdvancedSectionButton {
        background-color: ${(p) => p.theme.color.active};
    }
`

const ToggleSection = ({
    title,
    align = 'left',
    visibleOnLoad = false,
    children,
    ...rest
}) => {
    const [showContent, setShowContent] = useState(visibleOnLoad)

    return (
        <>
            <Wrapper
                $align={align}
                type="button"
                onClick={() => setShowContent(!showContent)}
                aria-expanded={showContent}
                {...rest}
            >
                <H3 as="span">{title}</H3>
                <div className="AdvancedSectionButton">
                    {showContent ? <ChevronUpIcon /> : <ChevronDownIcon />}
                </div>
            </Wrapper>
            <AnimatePresence visible={showContent}>{children}</AnimatePresence>
        </>
    )
}

ToggleSection.propTypes = {
    title: PropTypes.string,
    align: PropTypes.oneOf(['left', 'center']),
    visibleOnLoad: PropTypes.bool,
}

export default ToggleSection
