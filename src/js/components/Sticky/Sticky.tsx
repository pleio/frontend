import React, {
    RefObject,
    useContext,
    useEffect,
    useRef,
    useState,
} from 'react'
import { getStickyOffset } from 'helpers'

import { StickyContext } from './StickyProvider'

export interface Props {
    unstickElement?: RefObject<HTMLElement>
    children: React.ReactNode
}

const Sticky = ({ unstickElement, children, ...rest }: Props) => {
    const refWrapper = useRef<HTMLDivElement>(null)
    const refPlaceholder = useRef()

    const context = useContext(StickyContext)
    // Look for closest <StickyProvider /> to listen for scroll events. All <Sticky contained />'s inside this container will push each other out of view..
    const contained = refWrapper.current?.closest('[data-sticky-container]')
    // .. If there is no <StickyProvider /> in the tree, the <Sticky /> will be sticky to the window
    const scrollContainer = contained ? context?.container : window

    const [isSticky, setIsSticky] = useState(false)
    const [stickyOffset, setStickyOffset] = useState(0)

    const stickyStyleString = `
        position: fixed;
    `

    const handleScroll = () => {
        const placeholder: HTMLElement = refPlaceholder.current

        if (!placeholder) return

        const wrapper = refWrapper.current

        // Hide if element is not the last sticky of its group
        if (contained && isSticky) {
            const containedStickies = context?.container.querySelectorAll(
                '[data-sticky-contained="true"][data-sticky="true"]',
            )
            containedStickies.forEach((el: HTMLElement, i) => {
                if (
                    i ===
                    (context.reverseOrder ? 0 : containedStickies.length - 1)
                ) {
                    el.style.opacity = '1'
                } else {
                    el.style.opacity = '0'
                }
            })
        }

        const placeholderTop = placeholder.getBoundingClientRect().top

        const wrapperHeight = wrapper.clientHeight

        if (placeholder.clientHeight !== wrapperHeight) {
            placeholder.style.height = wrapperHeight + 'px'
        }

        const unstickBottom =
            unstickElement?.current.getBoundingClientRect().bottom

        const isWithinContainer =
            !unstickBottom ||
            (unstickBottom && unstickBottom > stickyOffset + wrapperHeight)

        if (placeholderTop < stickyOffset && isWithinContainer) {
            placeholder.style.maxHeight = 'none'

            if (!isSticky) {
                setIsSticky(true)
                wrapper.style.cssText = stickyStyleString
            }

            let nextOffset = 0

            if (contained) {
                const groupNotStickyElements =
                    context?.container.querySelectorAll(
                        '[data-sticky-contained="true"]',
                    )

                if (groupNotStickyElements?.length > 0) {
                    const currentStickyIndex = Array.from(
                        groupNotStickyElements,
                    )?.findIndex((el) => el === wrapper)

                    const nextGroupElement =
                        groupNotStickyElements[
                            context.reverseOrder
                                ? currentStickyIndex - 1
                                : currentStickyIndex + 1
                        ]

                    if (nextGroupElement) {
                        nextOffset =
                            nextGroupElement.getBoundingClientRect().top
                    }
                }
            }

            if (nextOffset && nextOffset < wrapperHeight + stickyOffset) {
                wrapper.style.top = `${nextOffset - wrapperHeight}px`
            } else if (wrapper.style.top !== `${stickyOffset}px`) {
                wrapper.style.top = `${stickyOffset}px`
            }

            if (wrapper.clientWidth !== placeholder.clientWidth) {
                wrapper.style.width = `${placeholder.clientWidth}px`
            }
        } else if (
            (isSticky && placeholderTop >= stickyOffset) ||
            (isSticky && !isWithinContainer)
        ) {
            setIsSticky(false)
            wrapper.style.cssText = ''
            placeholder.style.maxHeight = '0'
        }
    }

    const handleResize = () => {
        const scrollContainerOffset = contained
            ? context?.container.getBoundingClientRect().top
            : 0

        setStickyOffset(
            contained
                ? scrollContainerOffset
                : getStickyOffset(refWrapper.current) + scrollContainerOffset,
        )

        const placeholder: HTMLElement = refPlaceholder.current
        const wrapper = refWrapper.current
        const placeholderTop = placeholder.getBoundingClientRect().top

        if (placeholderTop < stickyOffset) {
            if (wrapper.clientWidth !== placeholder.clientWidth) {
                wrapper.style.width = `${placeholder.clientWidth}px`
            }
        }
    }

    useEffect(() => {
        scrollContainer.addEventListener('scroll', handleScroll)
        window.addEventListener('resize', handleResize)
        handleResize()
        handleScroll() // call to set sticky position

        return () => {
            scrollContainer.removeEventListener('scroll', handleScroll)
            window.removeEventListener('resize', handleResize)
        }
    })

    return (
        <>
            {/* 'flexShrink' prevents placeholder to be shrunk to height 0 */}
            <div ref={refPlaceholder} style={{ flexShrink: 0, maxHeight: 0 }} />
            <div
                ref={refWrapper}
                data-sticky={isSticky}
                data-sticky-contained={!!contained || null}
                data-sticky-ignore={!!unstickElement || null}
                {...rest}
            >
                {children}
            </div>
        </>
    )
}

export default Sticky
