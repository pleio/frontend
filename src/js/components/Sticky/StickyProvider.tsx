import React, { createContext, useState } from 'react'

export interface ContextProps {
    reverseOrder: boolean
    container: HTMLElement
}

export const StickyContext = createContext<ContextProps>(null)

export interface Props {
    children?: React.ReactNode
    reverseOrder: boolean
    id: string
}

const StickyProvider = ({
    children,
    reverseOrder,
    id = null,
    ...rest
}: Props) => {
    const [ref, setRef] = useState<HTMLDivElement>()

    return (
        <StickyContext.Provider value={{ container: ref, reverseOrder }}>
            <div
                data-sticky-container
                ref={(node) => setRef(node)}
                {...rest}
                id={id}
            >
                {ref && children}
            </div>
        </StickyContext.Provider>
    )
}

export default StickyProvider
