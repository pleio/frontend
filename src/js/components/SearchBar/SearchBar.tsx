import React, { ForwardedRef, forwardRef, HTMLAttributes } from 'react'
import { useTranslation } from 'react-i18next'
import styled, { css } from 'styled-components'

import Flexer from 'js/components/Flexer/Flexer'
import IconButton from 'js/components/IconButton/IconButton'
import Textfield from 'js/components/Textfield/Textfield'
import shouldForwardProp from 'js/lib/helpers/shouldForwardProp'

import SearchIcon from 'icons/search.svg'
import TuneIcon from 'icons/tune.svg'

export interface Props extends HTMLAttributes<HTMLDivElement> {
    borderStyle?: 'none' | 'rounded'
    disabled?: boolean
    label?: string
    name?: string
    onChange?: (changeEvent: React.ChangeEvent<HTMLInputElement>) => void
    onSearch?: () => void
    onToggleFilters?: () => void
    showFilters?: boolean
    size?: 'normal' | 'large'
    value?: string
}

const Wrapper = styled.div.withConfig({
    shouldForwardProp: shouldForwardProp(['borderStyle', 'size']),
})<Props>`
    display: flex;

    ${(p) =>
        p.borderStyle === 'rounded' &&
        css<Props>`
            background-color: white;
            border: 1px solid ${(p) => p.theme.color.grey[40]};

            ${(p) =>
                p.size === 'normal' &&
                css`
                    border-radius: ${(p) => p.theme.radius.normal};
                `};

            ${(p) =>
                p.size === 'large' &&
                css`
                    border-radius: ${(p) => p.theme.radius.large};
                `};
        `};

    .SearchBarInput {
        ${(p) =>
            p.size === 'normal' &&
            css`
                height: 38px;
            `};

        ${(p) =>
            p.size === 'large' &&
            css`
                height: 42px;
            `};
    }

    .SearchBarActions {
        padding: 2px 2px 2px 0;

        button {
            ${(p) =>
                p.size === 'normal' &&
                css`
                    width: 36px;
                    height: 36px;
                `};

            ${(p) =>
                p.size === 'large' &&
                css`
                    width: 40px;
                    height: 40px;
                `};
        }
    }
`

const SearchBar = forwardRef(
    (
        {
            borderStyle = 'rounded',
            disabled = false,
            label,
            name,
            onChange,
            onSearch,
            onToggleFilters,
            showFilters = false,
            size = 'normal',
            value = '',
            ...rest
        }: Props,
        ref: ForwardedRef<HTMLDivElement>,
    ) => {
        const { t } = useTranslation()

        const handleKeyDown = (evt: React.KeyboardEvent) => {
            if (evt.key === 'Enter') {
                evt.preventDefault()
                onSearch()
            }
        }

        return (
            <Wrapper ref={ref} size={size} borderStyle={borderStyle} {...rest}>
                <Textfield
                    label={label}
                    name={name}
                    autoComplete="off"
                    value={value}
                    onChange={onChange}
                    size={size}
                    disabled={disabled}
                    onKeyDown={onSearch ? handleKeyDown : null}
                    borderStyle="none"
                    style={{ flexShrink: 1 }} // Fix shrinking if narrow width
                    className="SearchBarInput"
                />
                <Flexer
                    gutter="small"
                    divider="normal"
                    className="SearchBarActions"
                >
                    <IconButton
                        size={size}
                        variant="secondary"
                        radiusStyle="rounded"
                        type={onSearch ? 'button' : 'submit'}
                        onClick={onSearch}
                        aria-label={label}
                        disabled={disabled}
                    >
                        <SearchIcon />
                    </IconButton>
                    {onToggleFilters && (
                        <IconButton
                            size={size}
                            variant="secondary"
                            radiusStyle="rounded"
                            onClick={onToggleFilters}
                            aria-pressed={showFilters}
                            tooltip={
                                showFilters
                                    ? t('filters.hide')
                                    : t('filters.show')
                            }
                        >
                            <TuneIcon />
                        </IconButton>
                    )}
                </Flexer>
            </Wrapper>
        )
    },
)

SearchBar.displayName = 'SearchBar'

export default SearchBar
