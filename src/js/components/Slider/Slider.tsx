import React, { useCallback, useEffect, useRef, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useMeasure } from 'react-use'
import styled, { css } from 'styled-components'
import { register } from 'swiper/element/bundle'
import { Swiper } from 'swiper/types'

import IconButton from 'js/components/IconButton/IconButton'

import PrevIcon from 'icons/chevron-left-large.svg'
import NextIcon from 'icons/chevron-right-large.svg'

register()

interface WrapperProps {
    $dragDisabled?: boolean
}

const Wrapper = styled.div<WrapperProps>`
    position: relative;

    .swiper-list {
        overflow: hidden;
        margin: -12px -10px -32px;
        pointer-events: none;

        ${(p) =>
            !p.$dragDisabled &&
            css`
                cursor: grab;

                &:active {
                    cursor: grabbing;
                }
            `};
    }

    swiper-container {
        display: flex;
        pointer-events: auto;
        z-index: 0; // Override z-index from swiper (messes with page editor)
    }

    swiper-slide {
        flex-grow: 1;
        height: auto;
        display: flex;
        padding: 12px 10px;

        > .FeedItemContent {
            height: 100%;
            display: flex;
        }
    }

    .swiper-prev,
    .swiper-next {
        display: flex !important;
        position: absolute;
        top: 0;
        bottom: 20px;
        margin: auto 0;
        text-indent: 0;
        transform: none;
        transition:
            opacity ${(p) => p.theme.transition.fast},
            visibility ${(p) => p.theme.transition.fast};
        z-index: 2;

        &:hover,
        &:focus {
            background-color: white;
            color: ${(p) => p.theme.color.icon.black};
        }

        &:before {
            margin-left: 0;
        }

        &.swiper-prev {
            left: -16px;
        }

        &.swiper-next {
            right: -16px;
        }

        &[disabled] {
            visibility: hidden;
            opacity: 0;
            pointer-events: none;
        }
    }
`

const getSlidesPerView = (width: number) => {
    return width <= 480
        ? 1
        : width <= 800
          ? 2
          : width <= 1280
            ? 3
            : width <= 1680
              ? 4
              : 5
}

export interface Props {
    slidesPerView?: number
    children?: React.ReactNode[]
}

type SwiperEvent = {
    detail: [Swiper]
}

const Slider = ({ children, ...rest }: Props) => {
    const { t } = useTranslation()

    const refSwiper = useRef(null)

    const [ref, { width }] = useMeasure()

    const [slidesPerView, setSlidesPerView] = useState(null)

    useEffect(() => {
        if (!width) return
        setSlidesPerView(getSlidesPerView(width))
    }, [width])

    const updateButtons = (swiper: Swiper) => {
        setIsBeginning(swiper?.isBeginning)
        setIsEnd(swiper?.isEnd)
    }

    useEffect(() => {
        const swiperEl = refSwiper?.current

        swiperEl.addEventListener('swiperinit', (e: SwiperEvent) => {
            const [swiper] = e.detail
            updateButtons(swiper)
        })

        swiperEl.addEventListener('swiperslidechange', (e: SwiperEvent) => {
            const [swiper] = e.detail
            updateButtons(swiper)
        })

        swiperEl.addEventListener('swiperupdate', (e: SwiperEvent) => {
            const [swiper] = e.detail
            updateButtons(swiper)
        })

        swiperEl.initialize()
    }, [])

    const [isBeginning, setIsBeginning] = useState(true)
    const [isEnd, setIsEnd] = useState(true)

    const handlePrev = useCallback(() => {
        refSwiper.current.swiper.slidePrev()
    }, [])

    const handleNext = useCallback(() => {
        refSwiper.current.swiper.slideNext()
    }, [])

    return (
        <Wrapper
            ref={ref}
            $dragDisabled={slidesPerView >= children?.length}
            {...rest}
        >
            <IconButton
                size="normal"
                variant="secondary"
                dropShadow
                aria-label={t('aria.prev-slide')}
                onClick={handlePrev}
                className="swiper-prev"
                disabled={isBeginning}
            >
                <PrevIcon />
            </IconButton>
            <IconButton
                size="normal"
                variant="secondary"
                dropShadow
                aria-label={t('aria.next-slide')}
                onClick={handleNext}
                className="swiper-next"
                disabled={isEnd}
            >
                <NextIcon />
            </IconButton>

            <div className="swiper-list">
                <swiper-container
                    ref={refSwiper}
                    init="false"
                    slides-per-view={slidesPerView}
                    speed="350"
                    loop="false"
                >
                    {children.map((child, index) => (
                        <swiper-slide key={index}>{child}</swiper-slide>
                    ))}
                </swiper-container>
            </div>
        </Wrapper>
    )
}

export default Slider
