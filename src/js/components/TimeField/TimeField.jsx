import React, { useEffect, useRef, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { format, setHours, setMinutes } from 'date-fns'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import Popover from 'js/components/Popover/Popover'
import Textfield from 'js/components/Textfield/Textfield'
import { getTimePattern } from 'js/lib/helpers/date/general'

const Wrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    width: 100px;

    > * {
        width: 50px;
        height: 24px;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        color: ${(p) => p.theme.color.text.black};
        text-align: center;

        &:hover {
            background-color: ${(p) => p.theme.color.hover};
        }

        &:active {
            background-color: ${(p) => p.theme.color.active};
        }
    }
`

const TimeField = ({
    name,
    label,
    value,
    disabled,
    fromTime,
    toTime,
    placeholder,
    timeSpan = 'half-hour',
    onChange,
}) => {
    const { t } = useTranslation()

    const refTextfield = useRef()
    const [instance, setInstance] = useState(null)
    const [inputValue, setInputValue] = useState('')

    const hideSuggestions = () => {
        instance.hide()
    }

    useEffect(() => {
        setInputValue(value)
    }, [value])

    const handleBlur = () => {
        // if value is the same do nothing (also fixes clicking sugggestion which fires blur)
        if (inputValue === value) return

        const strippedValue = inputValue.replace(/\D/g, '')
        const first4Numbers = strippedValue.match(/\d{4}/)
        const first2Numbers = strippedValue.match(/\d{2}/)

        if (first4Numbers || first2Numbers) {
            const newTime =
                timeSpan === 'hour'
                    ? `${first2Numbers[0]}:00`
                    : first4Numbers
                      ? `${first4Numbers[0].substring(
                            0,
                            2,
                        )}:${first4Numbers[0].substring(2, 4)}`
                      : `${first2Numbers[0]}:00`

            if (
                (!!fromTime && newTime < fromTime) ||
                (!!toTime && newTime > toTime)
            ) {
                setInputValue(value || '')
            } else {
                onChange(newTime)
            }
        } else {
            setInputValue(value || '')
        }
    }

    const handleChange = (evt) => setInputValue(evt.target.value)

    const handleKeyPress = (evt) => {
        if (evt.key === 'Enter') {
            evt.target.blur()
        }
    }

    const options = []
    new Array(24).fill().forEach((el, index) => {
        const timeWhole = format(
            setMinutes(setHours(new Date(), index), 0),
            'HH:mm',
        )
        if (
            !(fromTime && timeWhole < fromTime) &&
            !(toTime && timeWhole > toTime)
        ) {
            options.push({
                name: timeWhole,
                onClick: () => onChange(timeWhole),
            })
        }

        if (timeSpan === 'hour') return

        const timeHalf = format(
            setMinutes(setHours(new Date(), index), 30),
            'HH:mm',
        )
        if (
            !(fromTime && timeHalf < fromTime) &&
            !(toTime && timeHalf > toTime)
        ) {
            options.push({
                name: timeHalf,
                onClick: () => onChange(timeHalf),
            })
        }
    })

    return (
        <Popover
            onCreate={setInstance}
            triggerTarget={refTextfield.current}
            trigger="focus"
            content={
                <Wrapper>
                    {options.map((option) => {
                        const handleClickOption = () => {
                            option.onClick()
                            hideSuggestions()
                        }

                        return (
                            <button
                                key={option.name}
                                type="button"
                                onClick={handleClickOption}
                            >
                                {option.name}
                            </button>
                        )
                    })}
                </Wrapper>
            }
        >
            <div>
                <Textfield
                    ref={refTextfield}
                    className="TextFieldWrapper"
                    name={name}
                    label={label || t('entity-event.time')}
                    value={inputValue}
                    placeholder={placeholder || getTimePattern()}
                    disabled={disabled}
                    onChange={handleChange}
                    onKeyPress={handleKeyPress}
                    onBlur={handleBlur}
                    autoComplete="off"
                />
            </div>
        </Popover>
    )
}

TimeField.propTypes = {
    value: PropTypes.string.isRequired,
    fromTime: PropTypes.string,
    toTime: PropTypes.string,
    onChange: PropTypes.func,
    timeSpan: PropTypes.oneOf(['hour', 'half-hour']),
}

export default TimeField
