import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'

const UsersOnline = (props) => {
    const { t } = useTranslation()
    const { site } = props.data

    if (!site) return null

    return (
        <div className="users-online ___grey">
            <span>
                {t('widget-users-online.nr-online', {
                    count: site.usersOnline,
                })}
            </span>
        </div>
    )
}

const Query = gql`
    query UsersOnline {
        site {
            guid
            usersOnline
        }
    }
`

export default graphql(Query)(UsersOnline)
