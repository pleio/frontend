import React, { useEffect, useState } from 'react'
import { useMeasure } from 'react-use'

export const ResizeMeasure = ({
    children,
    options,
    maxHeight: defaultHeight = '48vh',
    as: Wrapper = 'div',
    ...rest
}) => {
    const [ref, { height, top }] = useMeasure()

    const [maxHeight, setMaxHeight] = useState(defaultHeight)

    useEffect(() => {
        const updateContainerPosition = () => {
            const spaceToBottom = window.innerHeight - top

            if (spaceToBottom < height) {
                setMaxHeight(`${spaceToBottom}px`)
            }
        }

        if (options?.length > 0) {
            updateContainerPosition()
        }

        window.addEventListener('resize', updateContainerPosition)

        return () => {
            window.removeEventListener('resize', updateContainerPosition)
        }
    }, [options, height, top])

    return (
        <Wrapper {...rest} ref={ref} style={{ maxHeight }}>
            {children}
        </Wrapper>
    )
}
