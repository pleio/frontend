import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { media } from 'helpers'
import styled from 'styled-components'

import Document from 'js/components/Document'
import NotFound from 'js/core/NotFound'
import { featuredViewFragment } from 'js/lib/fragments/featured'

import VersionList from './components/VersionList'
import VersionPreview from './components/VersionPreview'

const Wrapper = styled.div`
    flex-grow: 1;
    display: flex;
    overflow: hidden; // Enable scrollbars in children

    .VersionHistoryList {
        flex-shrink: 0;
        z-index: 1; // Position on top of VersionPreview
        overflow-y: auto;
    }

    .VersionHistoryPreview {
        flex-grow: 1;
        overflow-y: auto;
    }

    ${media.mobilePortrait`
        flex-direction: column;

        .VersionHistoryList {
            order: 1; // Position at bottom of screen
            max-height: 50%;
            border-top: 1px solid ${(p) => p.theme.color.grey[30]};
            box-shadow: ${(p) => p.theme.shadow.soft.top};
        }
    `};

    ${media.mobileLandscapeUp`
        .VersionHistoryList {
            height: 100%;
            box-shadow: ${(p) => p.theme.shadow.hard.right};
        }
    `};

    ${media.mobileLandscape`
        .VersionHistoryList {
            width: 280px;
        }
    `};

    ${media.tabletUp`
        .VersionHistoryList {
            width: 320px;
        }
    `};
`

const VersionHistory = () => {
    const { t } = useTranslation()

    const location = useLocation()

    const [refetchVersionHistory, setRefetchVersionHistory] = useState(
        location.state?.refetchTimeout || 0,
    )

    const params = useParams()
    const { guid, revisionGuid } = params

    const [queryLimit, setQueryLimit] = useState(50)

    const { loading, data, refetch, fetchMore } = useQuery(
        GET_VERSION_HISTORY,
        {
            variables: { guid, offset: 0, limit: queryLimit },
            fetchPolicy: 'cache-and-network',
        },
    )

    useEffect(() => {
        if (refetchVersionHistory > 0) {
            setTimeout(() => {
                refetch()
                setRefetchVersionHistory(0)
            }, refetchVersionHistory)
        }
    }, [refetch, refetchVersionHistory])

    if (loading) return null

    if (
        !data ||
        data?.revisions?.edges?.length === 0 ||
        (revisionGuid &&
            data?.revisions?.edges.findIndex(
                (el) => el.guid === revisionGuid,
            ) === -1)
    ) {
        return <NotFound />
    }

    const { site, entity, revisions } = data

    return (
        <Wrapper>
            <Document
                title={t('version-history.title')}
                containerTitle={entity?.title}
            />
            <VersionList
                entity={entity}
                revisions={revisions}
                className="VersionHistoryList"
                updating={refetchVersionHistory > 0}
                fetchMore={fetchMore}
                setQueryLimit={setQueryLimit}
            />
            <VersionPreview
                site={site}
                entity={
                    revisionGuid
                        ? revisions.edges.find((el) => el.guid === revisionGuid)
                              .content
                        : revisions.edges[0].content
                }
                className="VersionHistoryPreview"
            />
        </Wrapper>
    )
}

const GET_VERSION_HISTORY = gql`
    query VersionHistory($guid: String!, $limit: Int = 10, $offset: Int = 0) {
        site {
            guid
            showTagsInDetail
            showCustomTagsInDetail
        }
        entity(guid: $guid) {
            guid
            ... on Blog {
                title
                url
            }
            ... on News {
                title
                url
            }
            ... on Wiki {
                title
                url
            }
            ... on Pad {
                title
                url
            }
            ... on Page {
                title
                url
            }
        }
        revisions(containerGuid: $guid, offset: $offset, limit: $limit) {
            total
            edges {
                guid
                authors {
                    guid
                    name
                    url
                }
                type
                container {
                    guid
                }
                description
                content {
                    title
                    richDescription
                    ${featuredViewFragment}
                    tagCategories {
                        name
                        values
                    }
                    tags
                }
                changedFields
                statusPublishedChanged
                timeCreated
            }
        }
    }
`

export default VersionHistory
