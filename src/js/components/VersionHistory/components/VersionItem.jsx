import React, { useLayoutEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { NavLink, useMatch, useParams } from 'react-router-dom'
import { gql, useMutation } from '@apollo/client'
import showDate, { showDateTime, showTime } from 'helpers/date/showDate'
import styled, { css } from 'styled-components'

import HideVisually from 'js/components/HideVisually/HideVisually'

import FileIcon from 'icons/file.svg'
import FileArchiveIcon from 'icons/file-archive.svg'
import FileEditIcon from 'icons/file-edit.svg'
import FilePublishIcon from 'icons/file-publish.svg'

const Wrapper = styled.div`
    width: 100%;
    display: flex;

    ${(p) =>
        !p.$isActive &&
        css`
            &:hover {
                background-color: ${(p) => p.theme.color.hover};
            }
        `};

    ${(p) =>
        p.$isActive &&
        css`
            background-color: ${(p) => p.theme.color.hover};
            position: relative;

            &:before {
                content: '';
                position: absolute;
                top: 0;
                left: 0;
                bottom: 0;
                width: 5px;
                height: 100%;
                background-color: ${(p) => p.theme.color.primary.main};
            }

            .VersionItemIcon,
            .VersionItemTitle {
                color: ${(p) => p.theme.color.primary.main};
            }
        }
    `};

    .VersionItemIcon {
        flex-shrink: 0;
        width: 44px;
        height: 36px;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .VersionItemText {
        flex-grow: 1;
        padding: 6px 8px 8px 0;
    }

    .VersionItemTitle {
        display: block;
        width: 100%;
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
        font-weight: ${(p) => p.theme.font.weight.semibold};
        border: none;
        padding: 0;
        background: none;
    }

    input.VersionItemTitle {
        outline-offset: 0;

        &:hover {
            outline: 1px solid ${(p) => p.theme.color.grey[40]};
        }
        &:focus {
            color: black;
            outline: 1px solid ${(p) => p.theme.color.primary.main};
        }
    }

    .VersionItemDate {
        font-size: ${(p) => p.theme.font.size.small};
        line-height: ${(p) => p.theme.font.lineHeight.small};
    }

    .VersionItemAction {
        font-size: ${(p) => p.theme.font.size.tiny};
        line-height: ${(p) => p.theme.font.lineHeight.tiny};
        color: ${(p) => p.theme.color.text.grey};
    }
`

const VersionItem = ({ entity, revision, createdLast }) => {
    const { t } = useTranslation()
    const params = useParams()

    const {
        guid,
        type,
        description,
        timeCreated,
        authors,
        changedFields,
        statusPublishedChanged,
    } = revision

    const date = `${showDate(timeCreated, false, false)}, ${showTime(
        timeCreated,
    )}`

    const changedFieldsTranslated = changedFields.map(
        (field, i) =>
            `${t(`version-history.property.${field}`)}${
                i + 1 < changedFields.length ? ', ' : ''
            }`,
    )

    const url = `/${entity.guid}/version-history/${guid}`
    const matchUrl = useMatch({
        path: url,
        end: true,
    })
    const isActive = params.revisionGuid ? !!matchUrl : createdLast

    const [descriptionValue, setDescriptionValue] = useState('')
    const [lastDescription, setLastDescription] = useState('')

    useLayoutEffect(() => {
        if (isActive) {
            setDescriptionValue(description || date)
        }
    }, [isActive, revision])

    const submitDescription = () => {
        // Reset description input if no changes were made
        if ((!descriptionValue && !description) || descriptionValue === date) {
            setDescriptionValue(date)
            return
        }

        mutate({
            variables: {
                input: {
                    guid,
                    description: descriptionValue,
                },
            },
        })
    }

    const handleChangeDescription = (evt) => {
        setDescriptionValue(evt.target.value)
    }
    const handleFocusDescription = (evt) => {
        evt.target.select()
        if (descriptionValue) {
            setLastDescription(descriptionValue)
        }
    }
    const handleBlurDescription = () => {
        submitDescription()
    }
    const handleKeyDownDescription = (evt) => {
        if (evt.key === 'Enter') {
            evt.target.blur()
        }
        if (evt.key === 'Escape') {
            setDescriptionValue(lastDescription)
        }
    }

    const [mutate] = useMutation(
        gql`
            mutation EditRevisionDescription($input: updateRevisionInput!) {
                updateRevision(input: $input) {
                    guid
                    description
                }
            }
        `,
        {
            refetchQueries: ['VersionHistory'],
        },
    )

    const authorsString = authors.map((author) => author?.name || '').join(', ')

    const userCreatedAndPublished = `${t('version-history.user-created')} ${t(
        'global.and',
    )} ${t('version-history.user-published')}`

    return (
        <Wrapper
            $isActive={isActive}
            as={isActive ? null : NavLink}
            to={isActive ? null : url}
        >
            <div className="VersionItemIcon">
                {statusPublishedChanged === 'published' ? (
                    <FilePublishIcon />
                ) : statusPublishedChanged === 'archived' ? (
                    <FileArchiveIcon />
                ) : type === 'create' ? (
                    <FileIcon />
                ) : (
                    <FileEditIcon />
                )}
            </div>
            <div className="VersionItemText">
                {isActive ? (
                    <input
                        type="text"
                        aria-label={t('form.description')}
                        value={descriptionValue}
                        onChange={handleChangeDescription}
                        onFocus={handleFocusDescription}
                        onBlur={handleBlurDescription}
                        onKeyDown={handleKeyDownDescription}
                        className="VersionItemTitle"
                    />
                ) : (
                    <div className="VersionItemTitle">
                        {description || date}
                    </div>
                )}
                <HideVisually as="time" dateTime={timeCreated}>
                    {t('global.publication-date')}: {showDateTime(timeCreated)}
                </HideVisually>
                {!!description && (
                    <div aria-hidden className="VersionItemDate">
                        {date}
                    </div>
                )}

                <div className="VersionItemAction">
                    {entity.__typename === 'Pad' ? (
                        <>
                            {type === 'create' ? (
                                <>
                                    {`${authorsString} ${userCreatedAndPublished}`}
                                </>
                            ) : (
                                <>
                                    {`${t(
                                        'version-history.edited-by',
                                    )} ${authorsString}`}
                                </>
                            )}
                        </>
                    ) : (
                        <>
                            {authorsString}{' '}
                            {type === 'create' ? (
                                <>
                                    {statusPublishedChanged === 'draft'
                                        ? t(
                                              'version-history.user-created-draft',
                                          )
                                        : userCreatedAndPublished}
                                </>
                            ) : (
                                <>
                                    {statusPublishedChanged === 'archived'
                                        ? t('version-history.user-archived')
                                        : changedFieldsTranslated.length >
                                              0 && (
                                              <>
                                                  {t(
                                                      'version-history.user-edited',
                                                  )}{' '}
                                                  {changedFieldsTranslated}{' '}
                                                  {statusPublishedChanged ===
                                                      'published' &&
                                                      `${t('global.and')} `}
                                              </>
                                          )}
                                    {statusPublishedChanged === 'published' &&
                                        t('version-history.user-published')}
                                </>
                            )}
                        </>
                    )}
                </div>
            </div>
        </Wrapper>
    )
}

export default VersionItem
