import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useLocation } from 'react-router-dom'
import styled from 'styled-components'

import FetchMore from 'js/components/FetchMore'
import { H3 } from 'js/components/Heading'
import IconButton from 'js/components/IconButton/IconButton'
import Modal from 'js/components/Modal/Modal'

import BackIcon from 'icons/arrow-left.svg'

import VersionItem from './VersionItem'

const Wrapper = styled.div`
    background-color: white;

    .HistoryHeader {
        display: flex;
        align-items: center;
        padding: 16px 8px 8px 0;

        ${H3} {
            color: ${(p) => p.theme.color.primary.main};
        }
    }

    .HistoryVersionList {
        margin-bottom: 40px;

        & > li {
            &:not(:last-child) {
                border-bottom: 1px solid ${(p) => p.theme.color.grey[20]};
            }
        }
    }
`

const VersionList = ({
    entity,
    revisions,
    updating = false,
    fetchMore,
    setQueryLimit,
    ...rest
}) => {
    const { t } = useTranslation()
    const location = useLocation()

    return (
        <Wrapper {...rest}>
            <div className="HistoryHeader">
                <IconButton
                    as={Link}
                    to={location?.state?.prevPathname || entity.url}
                    size="large"
                    variant="secondary"
                    tooltip={t('action.back')}
                    style={{ margin: '0 2px' }}
                >
                    <BackIcon />
                </IconButton>
                <H3 as="h1">{t('version-history.title')}</H3>
            </div>

            <Modal
                size="small"
                isVisible={updating}
                title={t('version-history.versions-loading-title')}
            >
                {t('version-history.versions-loading-message')}
            </Modal>

            <FetchMore
                as="ul"
                className="HistoryVersionList"
                edges={revisions.edges}
                getMoreResults={(data) => data.revisions.edges}
                fetchMore={fetchMore}
                fetchCount={50}
                isScrollableBox
                setLimit={setQueryLimit}
                maxLimit={revisions.total}
                resultsMessage={t('global.result', {
                    count: revisions.total,
                })}
                fetchMoreButtonHeight={48}
            >
                {revisions.edges.map((revision, i) => (
                    <li key={revision.guid}>
                        <VersionItem
                            entity={entity}
                            revision={revision}
                            createdLast={i === 0}
                        />
                    </li>
                ))}
            </FetchMore>
        </Wrapper>
    )
}

export default VersionList
