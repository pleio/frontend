import React from 'react'
import { media } from 'helpers'
import styled from 'styled-components'

import CoverImage from 'js/components/CoverImage'
import { Container } from 'js/components/Grid/Grid'
import { H1 } from 'js/components/Heading'
import ItemTags from 'js/components/ItemTags'
import TiptapView from 'js/components/Tiptap/TiptapView'

const Wrapper = styled.div`
    background-color: white;

    .VersionPreviewCoverContainer {
        ${media.tabletDown`
            padding: 0;
        `}
    }
`

const VersionPreview = ({ site, entity, ...rest }) => {
    const { featured, title, richDescription, tags, tagCategories } = entity

    return (
        <Wrapper {...rest}>
            {featured && (
                <Container
                    size="large"
                    className="VersionPreviewCoverContainer"
                >
                    <CoverImage featured={featured} />
                </Container>
            )}
            <Container
                size="normal"
                style={{ paddingTop: '32px', paddingBottom: '32px' }}
            >
                <H1
                    style={{
                        order: 1,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'flex-start',
                    }}
                >
                    {title}
                </H1>

                <TiptapView
                    content={richDescription}
                    style={{ marginTop: '24px' }}
                />

                <ItemTags
                    showCustomTags={site.showCustomTagsInDetail}
                    showTags={site.showTagsInDetail}
                    customTags={tags}
                    tagCategories={tagCategories}
                    style={{ marginTop: '24px' }}
                />
            </Container>
        </Wrapper>
    )
}

export default VersionPreview
