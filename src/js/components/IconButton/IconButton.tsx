import React, {
    ButtonHTMLAttributes,
    ForwardedRef,
    forwardRef,
    useState,
} from 'react'
import handleInputPromise from 'helpers/handleInputPromise'
import styled from 'styled-components'
import { Placement } from 'tippy.js'

import BaseButton from 'js/components/BaseButton/BaseButton'
import Loader from 'js/components/Form/Loader'
import Tooltip from 'js/components/Tooltip/Tooltip'
import shouldForwardProp from 'js/lib/helpers/shouldForwardProp'

import iconButtonStyle from './iconButtonStyle'

export interface Props extends ButtonHTMLAttributes<HTMLButtonElement> {
    appendTo?: any
    as?: string
    children: React.ReactElement | React.ReactElement[]
    disabled?: boolean
    dropShadow?: boolean
    hideContent?: boolean
    hoverSize?: 'small' | 'normal' | 'large' | 'huge'
    isSubmitted?: boolean
    loading?: boolean
    offset?: [number, number]
    onClick?: () => void
    onHandle?: () => void
    placement?: Placement
    radiusStyle?: 'none' | 'round' | 'rounded'
    showOutline?: boolean
    size?: 'small' | 'normal' | 'large' | 'huge'
    target?: '_blank' | '_self' | '_parent' | '_top'
    theme?: string
    tooltip?: string
    variant?: 'pleio' | 'primary' | 'secondary' | 'tertiary' | 'primary-fill'
}

const Wrapper = styled(BaseButton).withConfig({
    shouldForwardProp: shouldForwardProp([
        'dropShadow',
        'hideContent',
        'hoverSize',
        'radiusStyle',
        'showOutline',
        'size',
        'variant',
    ]),
})<Props>`
    ${iconButtonStyle};
`

const IconButton = forwardRef<HTMLButtonElement, Props>(
    (
        {
            children,
            disabled = false,
            dropShadow = false,
            hideContent = false,
            hoverSize,
            isSubmitted = false,
            loading = false,
            offset,
            onClick,
            onHandle,
            placement,
            radiusStyle = 'round',
            showOutline = false,
            size,
            theme,
            tooltip,
            variant = 'pleio',
            ...rest
        },
        ref: ForwardedRef<HTMLButtonElement>,
    ) => {
        const [isLoading, setisLoading] = useState(false)
        const [isSaved, setIsSaved] = useState(false)

        const handleClick = () => {
            !!onHandle &&
                handleInputPromise(onHandle(), setisLoading, setIsSaved)
        }

        const isOccupied = isLoading || isSaved || loading || isSubmitted

        const Element = (
            <Wrapper
                aria-label={tooltip} // aria-label can overwrite this
                disabled={isOccupied || disabled}
                dropShadow={dropShadow}
                hideContent={hideContent || isOccupied}
                hoverSize={hoverSize}
                onClick={onClick || handleClick}
                radiusStyle={radiusStyle}
                ref={ref}
                showOutline={showOutline}
                size={size}
                variant={variant}
                {...rest}
            >
                <Loader
                    isLoading={isLoading || loading}
                    isSaved={isSaved || isSubmitted}
                    position="center"
                />
                <div className="IconButtonBackground" />
                <div className="IconButtonContent">{children}</div>
            </Wrapper>
        )

        if (tooltip)
            return (
                <Tooltip
                    content={tooltip}
                    delay
                    offset={offset}
                    placement={placement}
                    theme={theme || (variant ? `${variant}-white` : '')}
                >
                    {Element}
                </Tooltip>
            )
        else return Element
    },
)

IconButton.displayName = 'IconButton'

export default IconButton
