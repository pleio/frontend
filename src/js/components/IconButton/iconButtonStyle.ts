import { css } from 'styled-components'

import { Props } from './IconButton'

const iconButtonStyle = css<Props>`
    font-size: ${(p) => p.theme.font.size.small};
    line-height: ${(p) => p.theme.font.lineHeight.tiny};
    font-weight: ${(p) => p.theme.font.weight.semibold};
    text-align: center;
    box-shadow: ${(p) => (p.dropShadow ? p.theme.shadow.soft.center : 'none')};

    .IconButtonContent {
        /* Content stays on top of transparent layer */
        position: relative;
        width: 100%;
        height: 100%;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        transition: opacity ${(p) => p.theme.transition.normal};
    }

    .IconButtonBackground {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        margin: auto;
        overflow: hidden;
        pointer-events: none;

        /* Active indicator */
        &:before {
            content: '';
            position: absolute;
            bottom: 0;
            height: 3px;
            border-radius: 3px;
            left: 0;
            width: 100%;
        }
    }

    ${(p) =>
        p.hideContent &&
        css`
            pointer-events: none;
            cursor: default;

            .IconButtonContent {
                opacity: 0;
            }
        `};

    ${(p) =>
        (p.dropShadow || p.radiusStyle === 'none') &&
        css`
            background-color: ${(p) => p.theme.button.bg};
        `}

    ${(p) =>
        p.radiusStyle === 'round' &&
        css<Props>`
            ${(p) =>
                p.dropShadow &&
                css`
                    border-radius: 50%;
                `};

            .IconButtonBackground {
                border-radius: 50%;
            }
        `};

    ${(p) =>
        p.radiusStyle === 'rounded' &&
        css<Props>`
            ${(p) =>
                p.dropShadow &&
                p.size &&
                css<Props>`
                    border-radius: ${(p) =>
                        p.size === 'small'
                            ? p.theme.radius.small
                            : p.size === 'normal'
                              ? p.theme.radius.normal
                              : p.theme.radius.large};
                `};

            .IconButtonBackground {
                border-radius: ${(p) =>
                    p.size === 'small'
                        ? p.theme.radius.small
                        : p.size === 'normal'
                          ? p.theme.radius.normal
                          : p.theme.radius.large};
            }
        `};

    .IconButtonBackground {
        ${(p) =>
            !p.hoverSize &&
            css`
                width: 100%;
                height: 100%;
            `};

        ${(p) =>
            p.hoverSize === 'small' &&
            css`
                width: 24px;
                height: 24px;
            `};

        ${(p) =>
            p.hoverSize === 'normal' &&
            css`
                width: 32px;
                height: 32px;
            `};

        ${(p) =>
            p.hoverSize === 'large' &&
            css`
                width: 40px;
                height: 40px;
            `};

        ${(p) =>
            p.hoverSize === 'huge' &&
            css`
                width: 56px;
                height: 56px;
            `};
    }

    ${(p) =>
        p.size === 'small' &&
        css`
            width: 24px;
            height: 24px;
        `};

    ${(p) =>
        p.size === 'normal' &&
        css`
            width: 32px;
            height: 32px;
        `};

    ${(p) =>
        p.size === 'large' &&
        css`
            width: 40px;
            height: 40px;
        `};

    ${(p) =>
        p.size === 'huge' &&
        css`
            width: 56px;
            height: 56px;
        `};

    ${(p) =>
        p.variant === 'pleio' &&
        css<Props>`
            color: ${(p) => p.theme.color.pleio};

            &:hover .IconButtonBackground {
                background-color: ${(p) => p.theme.color.hover};
            }

            &:active .IconButtonBackground {
                background-color: ${(p) => p.theme.color.active};
            }

            &[aria-pressed='true'] {
                .IconButtonBackground {
                    background-color: ${(p) => p.theme.color.active};

                    &:before {
                        background-color: ${(p) => p.theme.color.pleio};
                    }
                }
            }

            ${(p) =>
                p.showOutline === true &&
                css`
                    .IconButtonBackground {
                        border: 1px solid ${(p) => p.theme.color.pleio};
                    }
                `};
        `};

    ${(p) =>
        p.variant === 'primary' &&
        css<Props>`
            color: ${(p) => p.theme.color.secondary.main};

            &:hover .IconButtonBackground {
                background-color: ${(p) => p.theme.color.hover};
            }

            &:active .IconButtonBackground {
                background-color: ${(p) => p.theme.color.active};
            }

            &[aria-pressed='true'] {
                .IconButtonBackground {
                    background-color: ${(p) => p.theme.color.active};

                    &:before {
                        background-color: ${(p) =>
                            p.theme.color.secondary.main};
                    }
                }
            }

            ${(p) =>
                p.showOutline === true &&
                css`
                    .IconButtonBackground {
                        border: 1px solid ${(p) => p.theme.color.secondary.main};
                    }
                `};
        `};

    ${(p) =>
        p.variant === 'secondary' &&
        css<Props>`
            color: ${(p) => p.theme.color.icon.black};

            &:hover .IconButtonBackground {
                background-color: ${(p) => p.theme.color.hover};
            }

            &:active .IconButtonBackground {
                background-color: ${(p) => p.theme.color.active};
            }

            &[aria-pressed='true'] {
                .IconButtonBackground {
                    background-color: ${(p) => p.theme.color.active};

                    &:before {
                        background-color: ${(p) => p.theme.color.icon.black};
                    }
                }
            }

            ${(p) =>
                p.showOutline === true &&
                css`
                    .IconButtonBackground {
                        border: 1px solid ${(p) => p.theme.color.grey[40]};
                    }
                `};
        `};

    ${(p) =>
        p.variant === 'tertiary' &&
        css<Props>`
            color: ${(p) => p.theme.color.icon.grey};

            &:hover {
                color: ${(p) => p.theme.color.icon.black};

                .IconButtonBackground {
                    background-color: ${(p) => p.theme.color.hover};
                }
            }

            &:active .IconButtonBackground {
                background-color: ${(p) => p.theme.color.active};
            }

            &[aria-pressed='true'] {
                color: ${(p) => p.theme.color.icon.black};

                .IconButtonBackground {
                    background-color: ${(p) => p.theme.color.active};

                    &:before {
                        background-color: ${(p) => p.theme.color.icon.black};
                    }
                }
            }

            ${(p) =>
                p.showOutline === true &&
                css`
                    border: 1px solid ${(p) => p.theme.color.grey[40]};
                `};
        `};

    ${(p) =>
        p.variant === 'primary-fill' &&
        css<Props>`
            color: white;
            outline-offset: 2px;

            .IconButtonBackground {
                background-color: ${(p) => p.theme.color.secondary.main};
            }

            &:hover .IconButtonBackground {
                background-color: ${(p) => p.theme.color.secondary.hover};
            }

            &:active .IconButtonBackground {
                background-color: ${(p) => p.theme.color.secondary.active};
            }

            &[aria-pressed='true'] {
                color: ${(p) => p.theme.color.icon.black};

                .IconButtonBackground {
                    background-color: ${(p) => p.theme.color.active};

                    &:before {
                        background-color: ${(p) => p.theme.color.icon.black};
                    }
                }
            }

            svg #bg {
                fill: none; // Remove possible background color from icon
            }

            ${(p) =>
                p.showOutline === true &&
                css`
                    border: 1px solid ${(p) => p.theme.color.grey[40]};
                `};
        `};
`

export default iconButtonStyle
