import styled, { css } from 'styled-components'

export default styled.li`
    transition: ${(p) =>
        `box-shadow ${p.theme.transition.fast}, padding ${p.theme.transition.fast}`};

    ${(p) =>
        p.$showSubItems &&
        css`
            padding: ${p.$isSubSubmenu ? '4px 0' : '4px 0 12px 0'};
            box-shadow:
                inset 0 1px 0 ${(p) => p.theme.color.grey[30]},
                inset 0 -1px 0 ${(p) => p.theme.color.grey[30]};
        `}
`
