import styled, { css } from 'styled-components'

import ListItem from 'js/components/ListItem/ListItem'

export default styled(ListItem)`
    justify-content: space-between;

    .ArrowDownIcon {
        transform: ${(p) => p.$isInverted && 'scaleY(-1)'};
    }

    ${(p) =>
        p.$size === 'small' &&
        css`
            padding: 6px 20px;
        `};

    ${(p) =>
        p.$size === 'normal' &&
        css`
            font-weight: ${(p) => p.theme.font.weight.semibold};
            padding: 10px 20px;
        `};
`
