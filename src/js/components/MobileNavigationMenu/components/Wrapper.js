import styled from 'styled-components'

export default styled.ul`
    display: flex;
    flex-direction: column;
    padding: 16px 0;
`
