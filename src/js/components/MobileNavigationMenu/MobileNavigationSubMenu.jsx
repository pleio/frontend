import React, { useState } from 'react'
import { AnimatePresence, motion } from 'framer-motion'

import SubmenuWrapper from './components/SubmenuWrapper'
import Item from './MobileNavigationMenuItem'
import MobileNavigationSubMenu from './MobileNavigationSubMenu'

const SubMenu = ({ id, item, toggleMobileNavigation, isSubSubmenu }) => {
    const [isOpen, setIsOpen] = useState(false)
    const toggleOpen = () => {
        setIsOpen(!isOpen)
    }

    return (
        <SubmenuWrapper
            key={id}
            $isSubSubmenu={isSubSubmenu}
            $showSubItems={isOpen}
        >
            <Item
                title={item.localLabel || item.label}
                size={isSubSubmenu ? 'small' : 'normal'}
                isOpen={isOpen}
                toggleOpenSubmenu={toggleOpen}
                toggleMobileNavigation={toggleMobileNavigation}
            />
            <AnimatePresence>
                {isOpen && (
                    <motion.ul
                        initial="hide"
                        animate="show"
                        exit="hide"
                        variants={{
                            hide: { opacity: 0, height: 0 },
                            show: {
                                opacity: 1,
                                height: 'auto',
                            },
                        }}
                        transition={{ duration: 0.15 }}
                        style={{
                            overflow: 'hidden',
                        }}
                    >
                        {item.children.map((subitem, i) => {
                            const subId = `${id}-${i}`

                            if (subitem.children?.length > 0) {
                                return (
                                    <MobileNavigationSubMenu
                                        key={subId}
                                        id={subId}
                                        item={subitem}
                                        toggleMobileNavigation={
                                            toggleMobileNavigation
                                        }
                                        isSubSubmenu
                                    />
                                )
                            } else
                                return (
                                    <li key={subId}>
                                        <Item
                                            title={
                                                subitem.localLabel ||
                                                subitem.label
                                            }
                                            link={subitem.link}
                                            size="small"
                                            toggleMobileNavigation={
                                                toggleMobileNavigation
                                            }
                                        />
                                    </li>
                                )
                        })}
                    </motion.ul>
                )}
            </AnimatePresence>
        </SubmenuWrapper>
    )
}

export default SubMenu
