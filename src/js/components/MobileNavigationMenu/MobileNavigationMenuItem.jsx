import React from 'react'
import { useTranslation } from 'react-i18next'
import { NavLink } from 'react-router-dom'
import { isExternalUrl } from 'helpers'

import ArrowDownIcon from 'icons/chevron-down-small.svg'
import ExternalIcon from 'icons/new-window-small.svg'

import Link from './components/Link'

const MobileNavigationMenuItem = ({
    title,
    link,
    size,
    isOpen,
    toggleOpenSubmenu,
    toggleMobileNavigation,
}) => {
    const { t } = useTranslation()

    // Has no child-items
    if (link) {
        if (isExternalUrl(link)) {
            return (
                <Link
                    as="a"
                    href={link}
                    target="_blank"
                    rel="noopener noreferrer"
                    $size={size}
                    aria-label={`${title}${t('global.opens-in-new-window')}`}
                >
                    <span>{title}</span>
                    <ExternalIcon style={{ marginLeft: '8px' }} />
                </Link>
            )
        } else {
            return (
                <Link
                    as={NavLink}
                    to={link}
                    $size={size}
                    end
                    onClick={toggleMobileNavigation}
                >
                    <span>{title}</span>
                </Link>
            )
        }
    } else {
        return (
            <Link
                as="button"
                $size={size}
                $isInverted={isOpen}
                onClick={toggleOpenSubmenu}
                aria-expanded={isOpen}
            >
                <span>{title}</span>
                <ArrowDownIcon className="ArrowDownIcon" />
            </Link>
        )
    }
}

export default React.memo(MobileNavigationMenuItem)
