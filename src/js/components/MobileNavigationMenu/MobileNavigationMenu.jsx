import React from 'react'
import { useTranslation } from 'react-i18next'
import { NavLink } from 'react-router-dom'

import Link from './components/Link'
import Wrapper from './components/Wrapper'
import Item from './MobileNavigationMenuItem'
import MobileNavigationSubMenu from './MobileNavigationSubMenu'

const Menu = ({ site, toggleMobileNavigation }) => {
    const { t } = useTranslation()

    if (!site) return null

    return (
        <Wrapper>
            <li>
                <Link
                    as={NavLink}
                    end
                    to="/"
                    $size="normal"
                    onClick={toggleMobileNavigation}
                    aria-label={t('global.home-page')}
                >
                    {t('global.home-title')}
                </Link>
            </li>

            {site.menu.map((item, i) => {
                const id = `nav-menu-item${i}`
                if (!item.children?.length > 0) {
                    return (
                        <li key={id}>
                            <Item
                                title={item.localLabel || item.label}
                                link={item.link}
                                size="normal"
                                toggleMobileNavigation={toggleMobileNavigation}
                            />
                        </li>
                    )
                } else {
                    return (
                        <MobileNavigationSubMenu
                            key={id}
                            id={id}
                            item={item}
                            toggleMobileNavigation={toggleMobileNavigation}
                        />
                    )
                }
            })}
        </Wrapper>
    )
}

export default React.memo(Menu)
