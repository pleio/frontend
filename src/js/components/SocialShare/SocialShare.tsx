import React, { useRef, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useLocalStorage } from 'helpers'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'
import Flexer, { Props as FlexerProps } from 'js/components/Flexer/Flexer'
import IconButton, {
    Props as IconButtonProps,
} from 'js/components/IconButton/IconButton'
import Modal from 'js/components/Modal/Modal'
import Spacer from 'js/components/Spacer/Spacer'
import Text from 'js/components/Text/Text'
import Textfield from 'js/components/Textfield/Textfield'

import FacebookIcon from 'icons/facebook.svg'
import LinkedinIcon from 'icons/linkedin.svg'
import EmailIcon from 'icons/mail.svg'
import MastodonIcon from 'icons/mastodon.svg'
import XIcon from 'icons/x.svg'

const Wrapper = styled(Flexer)`
    border: 1px solid ${(p) => p.theme.color.grey[30]};
    border-radius: ${(p) => p.theme.radius.normal};
    overflow: hidden;

    .SocialShareFacebook {
        color: ${(p) => p.theme.color.social.facebook};
    }
    .SocialShareLinkedin {
        color: ${(p) => p.theme.color.social.linkedin};
    }
    .SocialShareMastodon {
        color: ${(p) => p.theme.color.social.mastodon};
    }
`

interface SocialButtonProps extends IconButtonProps {
    ariaKey?: string
    children: React.ReactElement
    href: string
}

const SocialButton = ({ children, ariaKey, ...rest }: SocialButtonProps) => {
    const { t } = useTranslation()

    return (
        <li>
            <IconButton
                aria-label={`${t(ariaKey)}${t('global.opens-in-new-window')}`}
                tooltip={t(ariaKey)}
                as="a"
                radiusStyle="none"
                rel="noopener noreferrer"
                size="large"
                target="_blank"
                variant="secondary"
                {...rest}
            >
                {children}
            </IconButton>
        </li>
    )
}

type Platform = 'email' | 'facebook' | 'linkedin' | 'mastodon' | 'twitter'

const SocialShare = (props: Partial<FlexerProps>) => {
    const { t } = useTranslation()

    const instanceRef = useRef(null)

    const [showMastodonModal, setShowMastodonModal] = useState(false)

    const [mastodonInstance, setMastodonInstance] = useLocalStorage(
        `mastodon-instance`,
        'https://mastodon.social',
    )

    const getURL = (platform: Platform) => {
        const url = encodeURIComponent(window.location.href)

        switch (platform) {
            case 'mastodon':
                return `${mastodonInstance}/share?text=${url}`

            // Twitter is X nowadays. Since X is fairly vague, Twitter is still used as a reference
            case 'twitter':
                return `https://twitter.com/intent/tweet?url=${url}`

            case 'facebook':
                return `https://www.facebook.com/share.php?u=${url}`

            case 'linkedin':
                return `https://www.linkedin.com/shareArticle?mini=true&url=${url}`

            case 'email':
                return `mailto:?body=${t(
                    'action.social-share-email-body',
                )} ${url}`
        }
    }

    return (
        <>
            <Flexer justifyContent="center" style={{ marginTop: 32 }}>
                <Wrapper gutter="none" divider="normal" as="ul" {...props}>
                    <SocialButton
                        ariaKey="action.social-share-facebook"
                        className="SocialShare SocialShareFacebook"
                        href={getURL('facebook')}
                    >
                        <FacebookIcon />
                    </SocialButton>

                    <SocialButton
                        ariaKey="action.social-share-x"
                        className="SocialShare"
                        href={getURL('twitter')}
                    >
                        <XIcon />
                    </SocialButton>

                    <IconButton
                        className="SocialShare SocialShareMastodon"
                        aria-label={`${t('action.social-share-mastodon')}${t('global.opens-in-new-window')}`}
                        tooltip={t('action.social-share-mastodon')}
                        radiusStyle="none"
                        size="large"
                        variant="secondary"
                        onClick={() => setShowMastodonModal(true)}
                    >
                        <MastodonIcon />
                    </IconButton>

                    <SocialButton
                        ariaKey="action.social-share-linkedin"
                        className="SocialShare SocialShareLinkedin"
                        href={getURL('linkedin')}
                    >
                        <LinkedinIcon />
                    </SocialButton>

                    <SocialButton
                        ariaKey="action.social-share-email"
                        className="SocialShare"
                        href={getURL('email')}
                    >
                        <EmailIcon />
                    </SocialButton>
                </Wrapper>
            </Flexer>

            <Modal
                title={t('action.social-share-mastodon-title')}
                size="small"
                isVisible={showMastodonModal}
            >
                <Spacer>
                    <Text>{t('action.social-share-mastodon-text')}</Text>

                    <Textfield
                        ref={instanceRef}
                        label={t('form.link')}
                        value={mastodonInstance}
                        onChange={(e) => setMastodonInstance(e.target.value)}
                        type="url"
                    />

                    <Flexer>
                        <Button
                            size="normal"
                            variant="secondary"
                            onClick={() => setShowMastodonModal(false)}
                        >
                            {t('action.cancel')}
                        </Button>
                        <Button
                            size="normal"
                            variant="primary"
                            onClick={() =>
                                window.open(getURL('mastodon'), '_blank')
                            }
                            disabled={!mastodonInstance}
                        >
                            {t('action.ok')}
                        </Button>
                    </Flexer>
                </Spacer>
            </Modal>
        </>
    )
}

export default SocialShare
