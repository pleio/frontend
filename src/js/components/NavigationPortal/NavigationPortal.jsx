import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useLocation } from 'react-router-dom'
import styled from 'styled-components'

import Button from 'js/components/Button/Button'

import styles from './navigationPortalStyles'

const Wrapper = styled.div`
    ${styles}
`

const NavigationPortal = () => {
    const { t } = useTranslation()
    const { pathname } = useLocation()

    if (!window.__SETTINGS__.showLoginRegister) return null

    return (
        <Wrapper>
            <Button
                size="small"
                as={Link}
                to="/login"
                state={{ next: pathname }}
            >
                {t('action.login')}
            </Button>
            <Button
                size="small"
                as={Link}
                to="/register"
                state={{ next: pathname }}
            >
                {t('action.register')}
            </Button>
        </Wrapper>
    )
}

export default NavigationPortal
