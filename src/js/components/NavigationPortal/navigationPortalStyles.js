import { media } from 'helpers'
import { getReadableColor } from 'helpers/getContrast'
import { css } from 'styled-components'

export default css`
    display: flex;
    padding-left: 12px;

    ${media.desktopUp`
        flex-direction: column;
        align-items: flex-start;
    `};

    > * {
        padding: 0 4px;
        background-color: transparent;
        outline-color: ${(p) =>
            getReadableColor(p.theme.color.header.main)} !important;
        color: ${(p) => getReadableColor(p.theme.color.header.main)};

        ${media.tabletDown`
            height: 32px;
        `};

        ${media.desktopUp`
            height: 20px;
        `};

        &:hover {
            background-color: ${(p) => p.theme.color.header.hover};
        }

        &:active {
            background-color: ${(p) => p.theme.color.header.active};
        }
    }
`
