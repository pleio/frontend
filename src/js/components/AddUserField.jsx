import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useLazyQuery } from '@apollo/client'
import { useDebounce } from 'helpers'

import EntityList from 'js/components/EntityList/EntityList'
import EntityListItem from 'js/components/EntityList/EntityListItem'
import IconButton from 'js/components/IconButton/IconButton'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Popover from 'js/components/Popover/Popover'
import Text from 'js/components/Text/Text'
import Textfield from 'js/components/Textfield/Textfield'
import { REGEX } from 'js/lib/constants'

import AddIcon from 'icons/add.svg'

const AddUserField = ({ users, onAddUser, label, allowEmail, ...rest }) => {
    const { t } = useTranslation()

    const [getUsers, { loading, data }] = useLazyQuery(GET_USERS)

    const [inputValue, setInputValue] = useState('')
    const q = useDebounce(inputValue, 200)

    useEffect(() => {
        getUsers({
            variables: {
                q,
            },
        })
    }, [q, getUsers])

    const onChange = (evt) => {
        setInputValue(evt.target.value)
    }

    const onClear = () => {
        setInputValue('')
    }

    const onFocus = () => {
        getUsers({
            variables: {
                q,
            },
        })
    }

    const onKeyDown = (evt) => {
        if (evt.key === 'Escape') {
            evt.stopPropagation()
            setInputValue('')
        }
    }

    const handleShowSuggestions = ({ popper, reference }) => {
        popper.style.width = reference.getBoundingClientRect().width + 'px'
    }

    let email
    if (
        q &&
        allowEmail &&
        REGEX.email.test(q) &&
        !users.some((u) => u.email === q)
    ) {
        email = (
            <AddUserFieldSuggestion
                entity={{ name: q, email: q }}
                onAddInvite={onAddUser}
            />
        )
    }

    const filteredUsers = data?.users?.edges?.filter(
        (user) => !users.find((invite) => invite.guid === user.guid),
    )

    return (
        <div {...rest}>
            <Popover
                content={
                    <div
                        style={{
                            maxHeight: '500px',
                            overflowY: 'auto',
                        }}
                    >
                        <EntityList>
                            {email}
                            {loading ? (
                                <LoadingSpinner style={{ margin: '16px' }} />
                            ) : filteredUsers?.length === 0 ? (
                                <Text
                                    size="small"
                                    variant="grey"
                                    textAlign="center"
                                    style={{ padding: '16px' }}
                                >
                                    {t('global.no-results')}
                                </Text>
                            ) : (
                                filteredUsers?.map((user) => (
                                    <AddUserFieldSuggestion
                                        key={`${user.guid}-invite`}
                                        entity={user}
                                        onAddInvite={onAddUser}
                                    />
                                ))
                            )}
                        </EntityList>
                    </div>
                }
                trigger="manual"
                visible={!!q}
                onShow={handleShowSuggestions}
                maxWidth="none"
                placement="bottom"
                offset={0}
                arrow={false}
            >
                {/* div ensures handleShowSuggestions gets the correct width */}
                <div>
                    <Textfield
                        name="invite-user-search"
                        label={label}
                        value={inputValue}
                        onChange={onChange}
                        onKeyDown={onKeyDown}
                        placeholder={`${allowEmail ? t('global.search-user-or-email') : t('global.search-user')}..`}
                        autoComplete="off"
                        onFocus={onFocus}
                        onClear={onClear}
                    />
                </div>
            </Popover>
        </div>
    )
}

const AddUserFieldSuggestion = ({ entity, onAddInvite }) => {
    const { t } = useTranslation()

    return (
        <EntityListItem entity={entity} type="user">
            <IconButton
                size="large"
                variant="secondary"
                tooltip={t('action.add')}
                onClick={() => onAddInvite(entity)}
            >
                <AddIcon />
            </IconButton>
        </EntityListItem>
    )
}

const GET_USERS = gql`
    query UserList($q: String!) {
        users(offset: 0, limit: 9999, q: $q) {
            total
            edges {
                guid
                name
                icon
                email
                url
                roles
            }
        }
    }
`

export default AddUserField
