/**
 * Use this when part of your form should be hidden dynamically (f.e. if it contains tabs).
 * Not rendering the hidden part can cause problems, so it is hidden with css.
 */

import React from 'react'
import styled, { css } from 'styled-components'

const Wrapper = styled.div`
    ${(p) =>
        !p.$visible &&
        css`
            display: none;
        `};
`

const TabPage = ({ id, visible, ariaLabelledby, ...rest }) => {
    return (
        <Wrapper
            id={id}
            role="tabpanel"
            aria-hidden={!visible}
            aria-labelledby={ariaLabelledby}
            $visible={visible}
            {...rest}
        />
    )
}

export default TabPage
