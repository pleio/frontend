import React from 'react'
import { useRoleLabel } from 'helpers'
import styled from 'styled-components'

import Avatar from 'js/components/Avatar/Avatar'
import { H4 } from 'js/components/Heading'
import Popover from 'js/components/Popover/Popover'

const Wrapper = styled.div`
    max-width: 300px;
    padding: 12px 16px;
    font-size: ${(p) => p.theme.font.size.tiny};
    line-height: ${(p) => p.theme.font.lineHeight.tiny};

    ${H4} {
        text-align: center;
    }

    table {
        word-break: normal;

        th,
        td {
            padding-top: 2px;
            vertical-align: baseline;
        }

        th {
            font-weight: ${(p) => p.theme.font.weight.normal};
            color: ${(p) => p.theme.color.text.grey};
            padding-right: 6px;
        }
    }
`

const UserAvatar = ({ user, isOwner, ...rest }) => {
    const { name, icon, vcard, roles } = user

    const ownerHasVcard = vcard?.length > 0

    const role = useRoleLabel(roles, isOwner)

    const AvatarComponent = (
        <Avatar
            tabIndex={ownerHasVcard ? 0 : null}
            disabled
            role={role}
            showHoverOverlay={ownerHasVcard}
            name={name}
            image={icon}
            {...rest}
        />
    )

    if (ownerHasVcard) {
        const multipleFields = vcard.length > 1

        return (
            <Popover
                trigger="mouseenter focusin"
                touch={['hold', 350]}
                delay
                content={
                    <Wrapper>
                        <H4>{name}</H4>
                        {!multipleFields ? (
                            <div style={{ textAlign: 'center' }}>
                                {vcard[0].value}
                            </div>
                        ) : (
                            <table>
                                <tbody>
                                    {vcard.map((field) => (
                                        <tr key={field.guid}>
                                            <th scope="row">{field.name}</th>
                                            <td>{field.value}</td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        )}
                    </Wrapper>
                }
            >
                <div>{AvatarComponent}</div>
            </Popover>
        )
    } else {
        return AvatarComponent
    }
}

export default UserAvatar
