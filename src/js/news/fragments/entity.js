import {
    entityEditFragment,
    entityViewFragment,
} from 'js/components/EntityActions/fragments/entity'

export const newsEditFragment = `
... on News {
    ${entityEditFragment}
    backgroundColor
    source
}
`

export const newsViewFragment = `
... on News {
    ${entityViewFragment}
    backgroundColor
    source
    publishRequest {
        status
    }
}
`
