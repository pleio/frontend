import { featuredViewFragment } from 'js/lib/fragments/featured'
import { ownerFragment } from 'js/lib/fragments/owner'

const newsListFragment = `
    fragment NewsListFragment on News {
        guid
        localTitle
        localExcerpt
        title
        url
        excerpt
        votes
        hasVoted
        isBookmarked
        isPinned
        canBookmark
        tagCategories {
            name
            values
        }
        tags
        subtype
        canEdit
        canArchiveAndDelete,
        isTranslationEnabled
        ${featuredViewFragment}
        ${ownerFragment}
        timePublished
        statusPublished
        views
        commentCount
        canComment
        group {
            guid
            ... on Group {
                isClosed
                name
                url
                membership
                isMembershipOnRequest
                isJoinButtonVisible
            }
        }
        publishRequest {
            status
        }
    }
`
export default newsListFragment
