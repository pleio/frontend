import React from 'react'

import FeedItem from 'js/components/FeedItem/FeedItem'
import FeedItemContent from 'js/components/FeedItem/FeedItemContent'
import FeedItemFooter from 'js/components/FeedItem/FeedItemFooter'
import FeedItemImage from 'js/components/FeedItem/FeedItemImage'

import getRefetchQueries from '../helpers/getRefetchQueries'

const NewsCard = ({
    'data-feed': dataFeed,
    entity,
    canPin,
    hideSubtype,
    hideComments,
    hideLikes,
    hideExcerpt,
    hideActions,
    excerptMaxLines,
}) => {
    const onEdit = `/news/edit/${entity.guid}`

    const card = (
        <FeedItem data-feed={dataFeed}>
            <FeedItemContent
                entity={entity}
                hideSubtype={hideSubtype}
                hideExcerpt={
                    !window.__SETTINGS__.showExcerptInNewsCard || hideExcerpt
                }
                excerptMaxLines={excerptMaxLines}
                onEdit={onEdit}
                canPin={canPin}
                hideActions={hideActions}
                refetchQueries={getRefetchQueries(false)}
            />
            <FeedItemFooter
                entity={entity}
                hideComments={
                    hideComments || !window.__SETTINGS__.commentsOnNews
                }
                hideLikes={hideLikes}
            />
            <FeedItemImage entity={entity} />
        </FeedItem>
    )

    return card
}

export default NewsCard
