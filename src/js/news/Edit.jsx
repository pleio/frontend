import React from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import EntityAddEditForm from 'js/components/EntityActions/EntityAddEditForm'
import NotFound from 'js/core/NotFound'
import { newsEditFragment } from 'js/news/fragments/entity'

import getRefetchQueries from './helpers/getRefetchQueries'

const Edit = () => {
    const { t } = useTranslation()
    const navigate = useNavigate()
    const { guid } = useParams()

    const { loading, data } = useQuery(Query, {
        variables: {
            guid,
        },
    })

    if (loading) return null
    if (!data || !data.entity) return <NotFound />

    const { entity } = data

    const afterDelete = () => {
        navigate('/news', {
            replace: true,
        })
    }

    const refetchQueries = getRefetchQueries(true)

    return (
        <EntityAddEditForm
            title={t('entity-news.edit')}
            deleteTitle={t('entity-news.delete')}
            subtype="news"
            entity={entity}
            afterDelete={afterDelete}
            refetchQueries={refetchQueries}
        />
    )
}

const Query = gql`
    query EditNews($guid: String!) {
        entity(guid: $guid) {
            guid
            ${newsEditFragment}
        }
    }
`

export default Edit
