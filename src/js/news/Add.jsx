import React from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'

import EntityAddEditForm from 'js/components/EntityActions/EntityAddEditForm'

import getRefetchQueries from './helpers/getRefetchQueries'

const Add = () => {
    const { groupGuid } = useParams()
    const refetchQueries = getRefetchQueries(false, !!groupGuid)
    const { t } = useTranslation()

    return (
        <EntityAddEditForm
            subtype="news"
            title={t('entity-news.create')}
            refetchQueries={refetchQueries}
        />
    )
}

export default Add
