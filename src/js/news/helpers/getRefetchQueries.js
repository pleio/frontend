/**
 * Load the correct query names, depending on if we need
 * to update the detail view
 *
 * @param {boolean} isDetail
 */
export default (isDetail, isInGoup) => {
    const refetchQueries = ['ActivityList', 'NewsList', 'FeaturedList']
    if (isDetail) {
        refetchQueries.push('NewsItem')
    }
    if (isInGoup) {
        refetchQueries.push('GroupNews')
    }
    return refetchQueries
}
