import React from 'react'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import ItemLayout from 'js/components/Item/ItemLayout'
import NotFound from 'js/core/NotFound'
import { newsViewFragment } from 'js/news/fragments/entity'

import getRefetchQueries from './helpers/getRefetchQueries'

const Item = () => {
    const { guid, groupGuid } = useParams()

    const { loading, data } = useQuery(GET_NEWS_ITEM, {
        variables: {
            guid,
            groupGuid,
        },
    })

    if (loading) return null
    if (!data || !data.entity) return <NotFound />

    const { entity, viewer } = data

    const onEdit = `/news/edit/${entity.guid}`
    const onAfterDelete = '/news'
    const refetchQueries = getRefetchQueries(true)

    return (
        <ItemLayout
            viewer={viewer}
            entity={entity}
            refetchQueries={refetchQueries}
            hideComments={!window.__SETTINGS__.commentsOnNews}
            onEdit={onEdit}
            onAfterDelete={onAfterDelete}
        />
    )
}

const GET_NEWS_ITEM = gql`
    query NewsItem($guid: String!, $groupGuid: String) {
        viewer {
            guid
            loggedIn
            user {
                guid
                name
                icon
            }
            requiresCommentModeration(subtype: "news", groupGuid: $groupGuid)
        }
        entity(guid: $guid, incrementViewCount: true) {
            guid
            ${newsViewFragment}
        }
    }
`

export default Item
