import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'
import { useWindowScrollPosition } from 'helpers'

import { Container } from 'js/components/Grid/Grid'
import PageHeader from 'js/components/PageHeader/PageHeader'
import Section from 'js/components/Section/Section'
import FeedView from 'js/page/Widget/components/Feed/FeedView'

// First load data, otherwise useWindowScrollPosition and FeedView won't work properly
const ListWrapper = () => {
    const { data, loading } = useQuery(GET_LIST_SETTINGS)

    if (loading) return null
    return <List data={data} />
}

const List = ({ data }) => {
    const { viewer, site } = data

    const { t } = useTranslation()

    const settings = {
        showTagFilter: site?.pageTagFilters.showTagFilter,
        showTagCategories: site?.pageTagFilters.showTagCategories,
        typeFilter: ['news'],
        sortBy: 'timePublished',
        sortDirection: 'desc',
        itemView: 'tile',
        itemCount: 3,
    }

    useWindowScrollPosition('news')

    return (
        <>
            <PageHeader
                title={t('entity-news.title-list')}
                canCreate={viewer?.canWriteToContainer}
                createLink="/news/add"
                createLabel={t('entity-news.create')}
            />
            <Section>
                <Container>
                    <FeedView guid="news" settings={settings} />
                </Container>
            </Section>
        </>
    )
}

const GET_LIST_SETTINGS = gql`
    query Discussions {
        viewer {
            guid
            canWriteToContainer(subtype: "news")
        }
        site {
            guid
            pageTagFilters(contentType: "news") {
                showTagFilter
                showTagCategories
            }
        }
    }
`

export default ListWrapper
