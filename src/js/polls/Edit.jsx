import React from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'

import NotFound from 'js/core/NotFound'

import AddEditForm from './AddEditForm'
import pollDetailFragment from './fragments/detailFragment'

const Edit = () => {
    const { t } = useTranslation()
    const { guid } = useParams()

    const { loading, data } = useQuery(Query, {
        variables: {
            guid,
        },
    })

    if (loading) return null

    const { entity } = data || {}

    if (!entity || !entity?.canEdit) return <NotFound />

    const refetchQueries = ['PollsList', 'PollsItem', 'Poll']

    return (
        <AddEditForm
            title={t('entity-poll.edit')}
            entity={entity}
            refetchQueries={refetchQueries}
        />
    )
}

const Query = gql`
    query EditPoll($guid: String!) {
        entity(guid: $guid) {
            guid
            ...PollDetailFragment
        }
    }
    ${pollDetailFragment}
`

export default Edit
