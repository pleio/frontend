import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import showDate from 'helpers/date/showDate'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import { Container } from 'js/components/Grid/Grid'
import { H1 } from 'js/components/Heading'
import Section from 'js/components/Section/Section'
import Text from 'js/components/Text/Text'
import TiptapView from 'js/components/Tiptap/TiptapView'
import NotFound from 'js/core/NotFound'
import PollViewForm from 'js/page/Widget/components/Poll/PollViewForm'

// import EditIcon from 'icons/edit.svg'
import EditIcon from 'icons/edit-small.svg'

import Document from '../components/Document'

import pollDetailFragment from './fragments/detailFragment'

const Item = () => {
    const { t } = useTranslation()
    const params = useParams()
    const { guid } = params

    const { loading, data } = useQuery(Query, {
        variables: {
            guid,
        },
    })

    if (loading) return null
    if (!data || !data.entity) return <NotFound />

    const { entity, viewer } = data
    const { title, timePublished, richDescription, canEdit } = entity

    return (
        <>
            <Document title={title} />
            <Section backgroundColor="white" grow>
                <Container size="normal">
                    <article
                        style={{
                            position: 'relative',
                            marginTop: '32px',
                        }}
                    >
                        <Text size="small" variant="grey">
                            {showDate(timePublished)}
                        </Text>

                        <H1>{title}</H1>

                        {richDescription && (
                            <div style={{ marginBottom: '16px' }}>
                                <TiptapView content={richDescription} />
                            </div>
                        )}

                        <PollViewForm
                            viewer={viewer}
                            entity={entity}
                            legend={title}
                        />

                        {canEdit && (
                            <Flexer justifyContent="flex-start" mt>
                                <Button
                                    variant="tertiary"
                                    size="normal"
                                    as={Link}
                                    to={`/polls/edit/${entity.guid}`}
                                >
                                    <EditIcon style={{ marginRight: '4px' }} />
                                    {t('action.edit')}
                                </Button>
                            </Flexer>
                        )}
                    </article>
                </Container>
            </Section>
        </>
    )
}

const Query = gql`
    query PollsItem($guid: String!) {
        viewer {
            guid
            loggedIn
        }
        entity(guid: $guid) {
            guid
            ...PollDetailFragment
        }
    }
    ${pollDetailFragment}
`

export default Item
