import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useLocation, useNavigate } from 'react-router-dom'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import compose from 'lodash.flowright'

import ActionContainer from 'js/components/ActionContainer/ActionContainer'
import DeleteModal from 'js/components/EntityActions/DeleteModal'
import AccessField from 'js/components/Form/AccessField'
import FormItem from 'js/components/Form/FormItem'
import { Container } from 'js/components/Grid/Grid'
import LoadingSpinner from 'js/components/LoadingSpinner/LoadingSpinner'
import Setting from 'js/components/Setting'
import Spacer from 'js/components/Spacer/Spacer'
import { MAX_LENGTH } from 'js/lib/constants'

import MultipleInputField from '../components/MultipleInputField'

const AddEditForm = ({
    data,
    mutateAdd,
    mutateEdit,
    entity,
    title,
    refetchQueries,
}) => {
    const { t } = useTranslation()
    const navigate = useNavigate()
    const location = useLocation()

    const [showDeleteModal, setShowDeleteModal] = useState(false)
    const toggleDeleteModal = () => setShowDeleteModal(!showDeleteModal)

    const [choices, setChoices] = useState(
        entity?.choices.map((choice) => choice.text) || ['', ''],
    )

    const afterDelete = () => {
        navigate('/polls', {
            replace: true,
        })
    }

    const handleClose = () => {
        navigate(
            location?.state?.prevPathname || (entity && entity.url) || '/polls',
        )
    }

    const submit = async ({ title, richDescription, accessId }) => {
        if (choices?.length < 1) {
            return
        }

        const mutate = entity ? mutateEdit : mutateAdd

        await mutate({
            variables: {
                input: {
                    ...(entity ? { guid: entity.guid } : {}),
                    title,
                    richDescription,
                    choices: choices.filter((n) => n),
                    accessId: parseInt(accessId, 10),
                },
            },
            refetchQueries,
        })
            .then(({ data }) => {
                if (entity) {
                    navigate(data.editPoll.entity.url, {
                        replace: true,
                    })
                } else {
                    navigate(data.addPoll.entity.url, {
                        replace: true,
                    })
                }
            })
            .catch((errors) => {
                console.error(errors)
            })
    }

    const defaultValues = {
        title: (entity && entity.title) || null,
        richDescription: (entity && entity.richDescription) || '',
        accessId: entity?.accessId,
        writeAccessId: entity?.writeAccessId,
    }

    const {
        control,
        handleSubmit,
        formState: { errors, isValid, isSubmitting },
    } = useForm({
        mode: 'onChange',
        defaultValues,
    })

    const submitForm = handleSubmit(submit)

    if (data?.loading) return <LoadingSpinner />

    return (
        <ActionContainer
            title={title}
            publish={submitForm}
            onDelete={entity ? toggleDeleteModal : null}
            onClose={handleClose}
            isValid={isValid}
            isPublished={!!entity}
            isPublishing={isSubmitting}
        >
            <Container size="small">
                <Spacer
                    as="form"
                    spacing="small"
                    onSubmit={handleSubmit(submit)}
                >
                    <FormItem
                        control={control}
                        type="text"
                        name="title"
                        title={t('entity-poll.form-question')}
                        required
                        errors={errors}
                        maxLength={MAX_LENGTH.title}
                    />

                    <FormItem
                        control={control}
                        title={t('form.description')}
                        name="richDescription"
                        type="rich"
                        options={{
                            textStyle: true,
                            textLink: true,
                            textFormat: true,
                            textAnchor: true,
                            textCSSClass: true,
                            textQuote: true,
                            textList: true,
                            insertAccordion: true,
                            insertButton: true,
                            insertMedia: true,
                            insertTable: true,
                            textLanguage: true,
                        }}
                    />

                    <Setting
                        subtitle="entity-poll.form-answers"
                        inputWidth="full"
                    >
                        <MultipleInputField
                            name="choices"
                            type="text"
                            placeholder={t('entity-poll.form-answer')}
                            rules="required"
                            value={choices}
                            setValue={setChoices}
                        />
                    </Setting>

                    {data?.viewer?.canUpdateAccessLevel && (
                        <AccessField
                            control={control}
                            name="accessId"
                            label={t('permissions.read-access')}
                        />
                    )}
                </Spacer>
            </Container>
            <DeleteModal
                isVisible={showDeleteModal}
                onClose={toggleDeleteModal}
                title={t('entity-poll.delete')}
                entity={entity}
                afterDelete={afterDelete}
                refetchQueries={['PollsList']}
            />
        </ActionContainer>
    )
}

const ADDMUTATION = gql`
    mutation AddPoll($input: addPollInput!) {
        addPoll(input: $input) {
            entity {
                guid
                ... on Poll {
                    url
                }
            }
        }
    }
`

const EDITMUTATION = gql`
    mutation EditPoll($input: editPollInput!) {
        editPoll(input: $input) {
            entity {
                guid
                ... on Poll {
                    title
                    richDescription
                    url
                    accessId
                    choices {
                        guid
                        text
                        votes
                    }
                }
            }
        }
    }
`

const PollViewer = gql`
    query PollViewer {
        viewer {
            guid
            canUpdateAccessLevel
        }
    }
`

export default compose(
    graphql(PollViewer, { name: 'data' }),
    graphql(ADDMUTATION, { name: 'mutateAdd' }),
    graphql(EDITMUTATION, { name: 'mutateEdit' }),
)(AddEditForm)
