import React, { Fragment } from 'react'
import { useTranslation } from 'react-i18next'
import { gql } from '@apollo/client'
import { graphql } from '@apollo/client/react/hoc'
import { useWindowScrollPosition } from 'helpers'

import FetchMoreWrapper from 'js/components/FetchMoreWrapper'
import { Container } from 'js/components/Grid/Grid'
import PageHeader from 'js/components/PageHeader/PageHeader'
import Section from 'js/components/Section/Section'

import Card from './components/Card'

const List = ({ data }) => {
    const { viewer } = data
    const { t } = useTranslation()

    useWindowScrollPosition('polls')

    return (
        <Fragment>
            <PageHeader
                title={t('entity-poll.title')}
                canCreate={viewer && viewer.canWriteToContainer}
                createLink="/polls/add"
                createLabel={t('entity-poll.create')}
            />
            <Section>
                <Container size="tiny">
                    <FetchMoreWrapper
                        childComponent={Card}
                        subtype="poll"
                        offset={0}
                        limit={20}
                        query={GET_POLLS_LIST}
                    />
                </Container>
            </Section>
        </Fragment>
    )
}

const GET_POLLS_LIST = gql`
    query PollsList($offset: Int!, $limit: Int!, $subtype: String!) {
        entities(offset: $offset, limit: $limit, subtype: $subtype) {
            total
            edges {
                guid
                ... on Poll {
                    guid
                    title
                    url
                }
            }
        }
    }
`

const GET_POLLS_WRAPPER = gql`
    query PollsWrapper {
        viewer {
            guid
            loggedIn
            canWriteToContainer(subtype: "poll")
        }
    }
`

export default graphql(GET_POLLS_WRAPPER)(List)
