import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'

import Card, { CardContent } from 'js/components/Card/Card'
import { H3 } from 'js/components/Heading'
import Text from 'js/components/Text/Text'

const PollCard = ({ entity, 'data-feed': dataFeed }) => {
    const { t } = useTranslation()

    return (
        <Card
            as="article"
            data-feed={dataFeed}
            style={{ marginBottom: '20px' }}
        >
            <CardContent>
                <H3>
                    <Link to={entity.url}>{entity.title}</Link>
                </H3>
                <Text size="small" variant="grey">
                    {t('entity-poll.content-name')}
                </Text>
            </CardContent>
        </Card>
    )
}

export default PollCard
