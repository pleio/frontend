import React from 'react'
import { useTranslation } from 'react-i18next'

import AddEditForm from './AddEditForm'

const Add = () => {
    const { t } = useTranslation()

    return (
        <AddEditForm
            title={t('entity-poll.create')}
            refetchQueries={['PollsList', 'Poll']}
        />
    )
}

export default Add
