const pollDetailFragment = `
    fragment PollDetailFragment on Poll {
        title
        richDescription
        url
        accessId
        timePublished
        statusPublished
        hasVoted
        canEdit
        choices {
            guid
            text
            votes
        }
    }
`
export default pollDetailFragment
