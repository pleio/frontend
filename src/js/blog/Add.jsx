import React from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'

import EntityAddEditForm from 'js/components/EntityActions/EntityAddEditForm'

import getRefetchQueries from './helpers/getRefetchQueries'

const Add = () => {
    const { t } = useTranslation()
    const { groupGuid } = useParams()
    const refetchQueries = getRefetchQueries(false, !!groupGuid)

    return (
        <EntityAddEditForm
            title={t('entity-blog.create')}
            subtype="blog"
            refetchQueries={refetchQueries}
        />
    )
}

export default Add
