import React from 'react'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { getGroupUrl } from 'helpers'

import { blogViewFragment } from 'js/blog/fragments/entity'
import ItemLayout from 'js/components/Item/ItemLayout'
import NotFound from 'js/core/NotFound'

import getRefetchQueries from './helpers/getRefetchQueries'

const Item = () => {
    const params = useParams()
    const { guid, groupGuid } = params

    const { loading, data } = useQuery(GET_BLOG_ITEM, {
        variables: {
            guid,
            groupGuid,
        },
    })

    if (loading) return null
    if (!data || !data.entity) return <NotFound />

    const { entity, viewer } = data

    const onEdit = `${getGroupUrl(params)}/blog/edit/${entity.guid}`
    const onAfterDelete = `${getGroupUrl(params)}/blog`
    const refetchQueries = getRefetchQueries(true, !!entity.group)

    return (
        <ItemLayout
            viewer={viewer}
            entity={entity}
            refetchQueries={refetchQueries}
            onEdit={onEdit}
            onAfterDelete={onAfterDelete}
        />
    )
}

const GET_BLOG_ITEM = gql`
    query BlogItem($guid: String!, $groupGuid: String) {
        viewer {
            guid
            loggedIn
            user {
                guid
                name
                icon
            }
            requiresCommentModeration(subtype: "blog", groupGuid: $groupGuid)
        }
        entity(guid: $guid, incrementViewCount: true) {
            guid
            ${blogViewFragment}
        }
    }
`

export default Item
