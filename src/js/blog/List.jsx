import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'
import { useWindowScrollPosition } from 'helpers'

import { Container } from 'js/components/Grid/Grid'
import PageHeader from 'js/components/PageHeader/PageHeader'
import Section from 'js/components/Section/Section'
import FeedView from 'js/page/Widget/components/Feed/FeedView'

// First load data, otherwise useWindowScrollPosition and FeedView won't work properly
const ListWrapper = () => {
    const { data, loading } = useQuery(GET_LIST_SETTINGS)

    if (loading) return null
    return <List data={data} />
}

const List = ({ data }) => {
    const { viewer, site } = data

    const { t } = useTranslation()

    const settings = {
        showTagFilter: site?.pageTagFilters.showTagFilter,
        showTagCategories: site?.pageTagFilters.showTagCategories,
        sortingOptions: ['timePublished', 'lastAction'],
        typeFilter: ['blog'],
        sortBy: 'lastAction',
        sortDirection: 'desc',
        itemView: 'list',
        itemCount: 10,
    }

    useWindowScrollPosition('blogs')

    return (
        <>
            <PageHeader
                title={t('entity-blog.title-list')}
                canCreate={viewer && viewer.canWriteToContainer}
                createLink="/blog/add"
                createLabel={t('entity-blog.create')}
            />
            <Section>
                <Container size="tiny">
                    <FeedView guid="blogs" settings={settings} />
                </Container>
            </Section>
        </>
    )
}

const GET_LIST_SETTINGS = gql`
    query Blogs {
        viewer {
            guid
            canWriteToContainer(subtype: "blog")
        }
        site {
            guid
            pageTagFilters(contentType: "blog") {
                showTagFilter
                showTagCategories
            }
        }
    }
`

export default ListWrapper
