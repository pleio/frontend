/**
 * Load the correct query names, depending on if we need
 * to update the detail view and/or group lists.
 *
 * @param {boolean} isDetail
 * @param {boolean} isInGoup
 */
export default (isDetail, isInGoup) => {
    const refetchQueries = ['ActivityList', 'Blogs']
    if (isDetail) {
        refetchQueries.push('BlogItem')
    }
    if (isInGoup) {
        refetchQueries.push('GroupBlogs')
    }
    return refetchQueries
}
