import {
    entityEditFragment,
    entityViewFragment,
} from 'js/components/EntityActions/fragments/entity'

export const blogEditFragment = `
... on Blog {
    ${entityEditFragment}
    backgroundColor
    isRecommended
}
`

export const blogViewFragment = `
... on Blog {
    ${entityViewFragment}
    backgroundColor
    isRecommended
    publishRequest {
        status
    }
}
`
