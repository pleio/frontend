import { featuredViewFragment } from 'js/lib/fragments/featured'
import { ownerFragment } from 'js/lib/fragments/owner'

const blogListFragment = `
    fragment BlogListFragment on Blog {
        guid
        localTitle
        localExcerpt
        title
        url
        excerpt
        description
        richDescription
        isHighlighted
        ${featuredViewFragment}
        subtype
        tagCategories {
            name
            values
        }
        tags
        statusPublished
        timePublished
        isBookmarked
        isPinned
        canBookmark
        canEdit
        canArchiveAndDelete
        commentCount
        canComment
        hasVoted
        votes
        isTranslationEnabled
        ${ownerFragment}
        group {
            guid
            ... on Group {
                isClosed
                name
                url
                membership
                isMembershipOnRequest
                isJoinButtonVisible
            }
        }
        publishRequest {
            status
        }
    }

`
export default blogListFragment
