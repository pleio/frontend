import React from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate, useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { getGroupUrl } from 'helpers'

import { blogEditFragment } from 'js/blog/fragments/entity'
import EntityAddEditForm from 'js/components/EntityActions/EntityAddEditForm'
import NotFound from 'js/core/NotFound'

import getRefetchQueries from './helpers/getRefetchQueries'

const Edit = () => {
    const { t } = useTranslation()
    const navigate = useNavigate()
    const params = useParams()
    const { guid } = params

    const { loading, data } = useQuery(Query, {
        variables: {
            guid,
        },
    })

    if (loading) return null
    if (!data || !data.entity) return <NotFound />

    const { entity } = data

    const afterDelete = () => {
        navigate(`${getGroupUrl(params)}/blog`, {
            replace: true,
        })
    }

    const refetchQueries = getRefetchQueries(true, !!entity.group)

    return (
        <EntityAddEditForm
            title={t('entity-blog.edit-title')}
            deleteTitle={t('entity-blog.delete-title')}
            subtype="blog"
            entity={entity}
            afterDelete={afterDelete}
            refetchQueries={refetchQueries}
        />
    )
}

const Query = gql`
    query EditBlog($guid: String!) {
        entity(guid: $guid) {
            guid
            ${blogEditFragment}
        }
    }
`

export default Edit
