/**
 * Load the correct query names, depending on if we need
 * to update the detail view and/or group lists.
 *
 * @param {boolean} isDetail
 * @param {boolean} isInGoup
 */
export default (isDetail, isInGoup) => {
    const refetchQueries = ['ActivityList', 'Questions']
    if (isDetail) {
        refetchQueries.push('QuestionsItem')
    }
    if (isInGoup) {
        refetchQueries.push('GroupQuestions')
    }
    return refetchQueries
}
