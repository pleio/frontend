import { featuredViewFragment } from 'js/lib/fragments/featured'
import { ownerFragment } from 'js/lib/fragments/owner'

const questionListFragment = `
    fragment QuestionListFragment on Question {
        guid
        localTitle
        localExcerpt
        title
        url
        excerpt
        ${featuredViewFragment}
        canEdit
        canArchiveAndDelete,
        subtype
        votes
        hasVoted
        isClosed
        isBookmarked
        isPinned
        canBookmark
        isTranslationEnabled
        tagCategories {
            name
            values
        }
        tags
        isFeatured
        timePublished
        statusPublished
        views
        commentCount
        canComment
        canChooseBestAnswer
        ${ownerFragment}
        group {
            guid
            ... on Group {
                isClosed
                name
                url
                membership
                isMembershipOnRequest
                isJoinButtonVisible
            }
        }
        publishRequest {
            status
        }
    }
`
export default questionListFragment
