import {
    entityEditFragment,
    entityViewFragment,
} from 'js/components/EntityActions/fragments/entity'

export const questionEditFragment = `
... on Question {
    ${entityEditFragment}
}
`

export const questionViewFragment = `
... on Question {
    ${entityViewFragment}
    isLocked
    canClose
    isClosed
    canChooseBestAnswer
    publishRequest {
        status
    }
}
`
