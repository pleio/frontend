import React from 'react'
import { useParams } from 'react-router-dom'
import { gql, useQuery } from '@apollo/client'
import { getGroupUrl } from 'helpers'

import ItemLayout from 'js/components/Item/ItemLayout'
import NotFound from 'js/core/NotFound'
import { questionViewFragment } from 'js/questions/fragments/entity'

import getRefetchQueries from './helpers/getRefetchQueries'

const Item = () => {
    const params = useParams()
    const { guid, groupGuid } = params

    const { loading, data } = useQuery(GET_QUESTIONS_ITEM, {
        variables: {
            guid,
            groupGuid,
        },
    })

    if (loading) return null
    if (!data || !data.entity) return <NotFound />

    const { entity, viewer } = data
    const onEdit = `${getGroupUrl(params)}/questions/edit/${data.entity.guid}`
    const onAfterDelete = `${getGroupUrl(params)}/questions`
    const refetchQueries = getRefetchQueries(true, !!entity.group)

    return (
        <ItemLayout
            viewer={viewer}
            entity={entity}
            refetchQueries={refetchQueries}
            canUpvote
            onEdit={onEdit}
            onAfterDelete={onAfterDelete}
        />
    )
}

const GET_QUESTIONS_ITEM = gql`
    query QuestionsItem($guid: String!, $groupGuid: String) {
        viewer {
            guid
            loggedIn
            user {
                guid
                name
                icon
                url
            }
            canInsertMedia(subtype: "question")
            requiresCommentModeration(subtype: "question", groupGuid: $groupGuid)
        }
        entity(guid: $guid, incrementViewCount: true) {
            guid
            ${questionViewFragment}
        }
    }
`

export default Item
