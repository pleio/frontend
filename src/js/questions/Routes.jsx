import React from 'react'
import { Route, Routes } from 'react-router-dom'

import NotFound from 'js/core/NotFound'

import Item from './Item'
import List from './List'

const Component = () => (
    <Routes>
        <Route path="/" element={<List />} />
        <Route path="/view/:guid/:slug" element={<Item />} />
        <Route path="*" element={<NotFound />} />
    </Routes>
)

export default Component
