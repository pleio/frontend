import React from 'react'
import { useTranslation } from 'react-i18next'
import { gql, useQuery } from '@apollo/client'
import { useWindowScrollPosition } from 'helpers'

import { Container } from 'js/components/Grid/Grid'
import PageHeader from 'js/components/PageHeader/PageHeader'
import Section from 'js/components/Section/Section'
import FeedView from 'js/page/Widget/components/Feed/FeedView'

// First load data, otherwise useWindowScrollPosition and FeedView won't work properly
const ListWrapper = () => {
    const { data, loading } = useQuery(GET_LIST_SETTINGS)

    if (loading) return null
    return <List data={data} />
}

const List = ({ data }) => {
    const { viewer, site } = data

    const { t } = useTranslation()

    const settings = {
        showTagFilter: site?.pageTagFilters.showTagFilter,
        showTagCategories: site?.pageTagFilters.showTagCategories,
        sortingOptions: ['timePublished', 'lastAction'],
        typeFilter: ['question'],
        sortBy: 'lastAction',
        sortDirection: 'desc',
        itemView: 'list',
        itemCount: 10,
    }

    useWindowScrollPosition('questions')

    return (
        <>
            <PageHeader
                title={t('entity-question.title-list')}
                canCreate={viewer?.canWriteToContainer}
                createLink="/questions/add"
                createLabel={t('entity-question.create')}
            />
            <Section>
                <Container size="tiny">
                    <FeedView guid="questions" settings={settings} />
                </Container>
            </Section>
        </>
    )
}

const GET_LIST_SETTINGS = gql`
    query Questions {
        viewer {
            guid
            canWriteToContainer(subtype: "question")
        }
        site {
            guid
            pageTagFilters(contentType: "question") {
                showTagFilter
                showTagCategories
            }
        }
    }
`

export default ListWrapper
