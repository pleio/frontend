import React from 'react'
import { useArgs } from '@storybook/preview-api'
import type { Meta, StoryObj } from '@storybook/react'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import Modal from 'js/components/Modal/Modal'

const meta = {
    component: Modal,
    argTypes: {
        size: {
            control: {
                type: 'select',
            },
        },
        onClose: {
            description:
                'Will close when pressing ESC or clicking outside of content',
        },
        maxWidth: {
            description:
                'Set a custom max-width for the content (`size` will override this)',
        },
        showCloseButton: {
            description: 'Show close button next to title',
        },
        containHeight: {
            description: 'Maximizes content height to 100% of viewport',
        },
        growHeight: {
            description: 'Grow content height to 100% of viewport',
        },
    },
} satisfies Meta<typeof Modal>

export default meta
type Story = StoryObj<typeof meta>

export const Default: Story = {
    args: {
        children: <div>Hover over me</div>,
        size: 'large',
        isVisible: false,
        showCloseButton: true,
    },
    decorators: [
        () => {
            const [args, updateArgs] = useArgs()
            const handleOnChange = () => {
                updateArgs({ isVisible: !args.isVisible })
            }

            return (
                <>
                    <Button onClick={handleOnChange}>Open modal</Button>
                    <Modal
                        isVisible={args.isVisible}
                        size={args.size}
                        showCloseButton={args.showCloseButton}
                        onClose={handleOnChange}
                        {...args}
                    >
                        <Flexer style={{ height: 300 }}>Modal content</Flexer>
                    </Modal>
                </>
            )
        },
    ],
}
