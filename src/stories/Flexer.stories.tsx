import React from 'react'
import type { Meta, StoryObj } from '@storybook/react'
import { styled } from 'styled-components'

import Flexer from 'js/components/Flexer/Flexer'
import SharedWrapper from 'js/components/Tiptap/BubbleMenu/components/SharedWrapper'

const meta = {
    component: Flexer,
    argTypes: {
        divider: {
            options: ['none', 'small', 'normal', 'large'],
            control: { type: 'select' },
        },
        gutter: {
            options: ['none', 'tiny', 'small', 'normal'],
            control: { type: 'select' },
        },
        alignItems: {
            control: { type: 'text' },
        },
        justifyContent: {
            control: { type: 'text' },
        },
        mt: {
            control: { type: 'boolean' },
        },
        wrap: {
            control: { type: 'boolean' },
        },
    },
    parameters: {
        docs: {
            description: {
                component:
                    "This component wraps it's children with `display: flex`, aligning them horizontally with a gutter by default.",
            },
        },
    },
} satisfies Meta<typeof Flexer>

export default meta
type Story = StoryObj<typeof meta>

const Child = styled.div`
    background: #cfcfcf;
    padding: 10px;
    color: #555;
`

export const Default: Story = {
    args: {
        children: 'Content',
    },
    render: (args) => (
        <Flexer style={{ width: 400, height: 300 }} {...args}>
            <Child>{args.children}</Child>
            <Child>{args.children}</Child>
            <Child>{args.children}</Child>
        </Flexer>
    ),
}

export const WithSharedWrapper: Story = {
    args: {
        children: 'Content',
        alignItems: 'stretch',
        justifyContent: 'space-between',
        divider: 'large',
        gutter: 'none',
    },
    render: (args) => (
        <SharedWrapper
            style={{ width: 400, height: 300 }}
            {...args}
            as="form"
            ref={null}
            onSubmit={() => {}}
        >
            <Child>{args.children}</Child>
            <Child>{args.children}</Child>
            <Child>{args.children}</Child>
        </SharedWrapper>
    ),
}
