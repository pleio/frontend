import type { Meta, StoryObj } from '@storybook/react'
import { fn } from '@storybook/test'

import Button from 'js/components/Button/Button'

const meta = {
    component: Button,
    args: {
        onClick: fn(),
        onHandle: fn(),
    },
    argTypes: {
        size: {
            control: {
                type: 'select',
            },
        },
        variant: {
            control: {
                type: 'select',
            },
        },
        onClick: {
            description: 'Will fall back to `onHandle` if not set',
        },
        onHandle: {
            description: 'Required if `onClick` is not set',
        },
    },
} satisfies Meta<typeof Button>

export default meta
type Story = StoryObj<typeof meta>

export const Primary: Story = {
    args: {
        children: 'Title',
    },
}
