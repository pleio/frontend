import type { Meta, StoryObj } from '@storybook/react'

import Textarea from 'js/components/Textarea/Textarea'

const meta = {
    component: Textarea,
    argTypes: {
        size: {
            control: {
                type: 'select',
            },
        },
    },
} satisfies Meta<typeof Textarea>

export default meta
type Story = StoryObj<typeof meta>

export const Default: Story = {
    args: {},
}
