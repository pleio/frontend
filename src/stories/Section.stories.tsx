import React from 'react'
import type { Meta, StoryObj } from '@storybook/react'
import { styled } from 'styled-components'

import Flexer from 'js/components/Flexer/Flexer'
import Section from 'js/components/Section/Section'

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    height: 400px;
`

const meta = {
    component: Section,
} satisfies Meta<typeof Section>

export default meta
type Story = StoryObj<typeof meta>

export const Default: Story = {
    args: {
        divider: false,
        grow: false,
        noTopPadding: false,
        noBottomPadding: false,
    },
    render: (args) => (
        <Wrapper>
            <Section {...args}>
                <Flexer>Section child</Flexer>
                <Flexer>Section child</Flexer>
                <Flexer>Section child</Flexer>
            </Section>
            <Section {...args}>
                <Flexer>Section child</Flexer>
                <Flexer>Section child</Flexer>
                <Flexer>Section child</Flexer>
            </Section>
        </Wrapper>
    ),
}
