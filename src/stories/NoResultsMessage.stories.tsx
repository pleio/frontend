import type { Meta, StoryObj } from '@storybook/react'
import { t } from 'i18next'

import NoResultsMessage from 'js/components/NoResultsMessage/NoResultsMessage'

const meta = {
    component: NoResultsMessage,
} satisfies Meta<typeof NoResultsMessage>

export default meta
type Story = StoryObj<typeof meta>

export const Default: Story = {
    args: {
        title: t('widget-feed.no-results'),
        subtitle: t('widget-feed.no-results-helper'),
    },
}
