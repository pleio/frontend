import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import type { Meta, StoryObj } from '@storybook/react'
import styled from 'styled-components'

import PageHeader, { Props } from 'js/components/PageHeader/PageHeader'

const Wrapper = styled.div`
    width: 100%;
    max-width: 1024px;
    min-width: 400px;
`

const meta = {
    component: PageHeader,
    decorators: [
        (Story) => (
            <BrowserRouter>
                <Wrapper>
                    <Story />
                </Wrapper>
            </BrowserRouter>
        ),
    ],
    argTypes: {
        createLabel: {
            description: 'Required if `canCreate` is `true`',
        },
        createLink: {
            description: 'Required if `canCreate` is `true`',
        },
    },
} satisfies Meta<typeof PageHeader>

export default meta

// for this story we create a deviant Story type, because the intersection types in PageHeader will not work
// with the stricter StoryObj<typeof meta> we normally use
type Story = StoryObj<Props>

export const Default: Story = {
    args: {
        title: 'Title',
        canCreate: true,
        createLabel: 'Add item',
        createLink: '/add',
    },
}

export const WithoutButton: Story = {
    args: {
        title: 'Title',
        canCreate: false,
    },
}
