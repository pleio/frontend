import type { Meta, StoryObj } from '@storybook/react'

import Text from 'js/components/Text/Text'

const meta = {
    component: Text,
    argTypes: {
        size: {
            control: {
                type: 'select',
            },
        },
        variant: {
            control: {
                type: 'select',
            },
        },
        textAlign: {
            control: {
                type: 'select',
            },
        },
    },
} satisfies Meta<typeof Text>

export default meta
type Story = StoryObj<typeof meta>

export const Default: Story = {
    args: {
        children: 'Text',
    },
}
