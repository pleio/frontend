import React from 'react'
import type { Meta, StoryObj } from '@storybook/react'
import { fn } from '@storybook/test'

import IconButton from 'js/components/IconButton/IconButton'

import HeartIcon from 'icons/heart.svg'

const meta = {
    component: IconButton,
    args: {
        onClick: fn(),
        onHandle: fn(),
    },
    argTypes: {
        onClick: {
            description: 'Will fall back to `onHandle` if not set',
        },
        onHandle: {
            description: 'Required if `onClick` is not set',
        },
        radiusStyle: { control: { type: 'select' } },
        size: { control: { type: 'select' } },
        hoverSize: { control: { type: 'select' } },
        variant: { control: { type: 'select' } },
    },
} satisfies Meta<typeof IconButton>

export default meta
type Story = StoryObj<typeof meta>

export const Default: Story = {
    args: {
        children: <HeartIcon />,
        variant: 'pleio',
    },
}
