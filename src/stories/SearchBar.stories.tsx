import React from 'react'
import type { Meta, StoryObj } from '@storybook/react'
import { fn } from '@storybook/test'
import { useArgs } from 'storybook/internal/preview-api'

import SearchBar from 'js/components/SearchBar/SearchBar'

const meta = {
    component: SearchBar,
    args: {
        onChange: fn(),
        onSearch: fn(),
        onToggleFilters: fn(),
    },
    argTypes: {
        borderStyle: {
            control: {
                type: 'select',
            },
        },
        size: {
            control: {
                type: 'select',
            },
        },
    },
} satisfies Meta<typeof SearchBar>

export default meta
type Story = StoryObj<typeof meta>

export const Primary: Story = {
    decorators: [
        () => {
            const [args, updateArgs] = useArgs()
            const handleOnChange = (
                evt: React.ChangeEvent<HTMLInputElement>,
            ) => {
                const value = evt.target.value
                args.onChange(value) // call the original onChange (Storybook Action fn())
                updateArgs({ value })
            }

            const handleOnToggleFilters = () => {
                args.onToggleFilters(!args.showFilters) // call the original onToggleFilters (Storybook Action fn())
                updateArgs({ showFilters: !args.showFilters })
            }

            return (
                <>
                    <SearchBar
                        {...args}
                        onChange={(evt) => handleOnChange(evt)}
                        onToggleFilters={() => handleOnToggleFilters()}
                    />
                    {args.showFilters && (
                        <div style={{ padding: 30, textAlign: 'center' }}>
                            Filters
                        </div>
                    )}
                </>
            )
        },
    ],
}
