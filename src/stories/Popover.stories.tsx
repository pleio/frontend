import React from 'react'
import type { StoryObj } from '@storybook/react'
import { styled } from 'styled-components'

import Button from 'js/components/Button/Button'
import Popover from 'js/components/Popover/Popover'
import Text from 'js/components/Text/Text'

const meta = {
    component: Popover,
    decorators: [
        (Story: any) => (
            <div style={{ padding: 100 }}>
                <Story />
            </div>
        ),
    ],
    argTypes: {
        children: {
            type: 'ReactElement',
        },
    },
}

export default meta
type Story = StoryObj<typeof meta>

const StyledChild = styled(Text)`
    font-weight: bold;
`

export const Default: Story = {
    args: {
        content: <StyledChild as="ul">content</StyledChild>,
        children: <Button>click me</Button>,
    },
}
