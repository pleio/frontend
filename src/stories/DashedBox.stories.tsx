import type { Meta, StoryObj } from '@storybook/react'

import DashedBox from 'js/components/DashedBox/DashedBox'

const meta = {
    component: DashedBox,
} satisfies Meta<typeof DashedBox>

export default meta
type Story = StoryObj<typeof meta>

export const Default: Story = {
    args: {
        children: 'Content',
    },
}
