import React from 'react'
import type { Meta, StoryObj } from '@storybook/react'

import { H1, H2, H3, H4, H5 } from 'js/components/Heading'

const meta = {
    component: H1,
    args: {
        children: 'The quick brown fox jumps over the lazy dog',
    },
} satisfies Meta<typeof H1>

export default meta
type Story = StoryObj<typeof meta>

export const Default: Story = {
    render: ({ children }) => (
        <>
            <H1>{children}</H1>
            <H2>{children}</H2>
            <H3>{children}</H3>
            <H4>{children}</H4>
            <H5>{children}</H5>
        </>
    ),
}
