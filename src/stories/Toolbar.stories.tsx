import React from 'react'
import type { Meta, StoryObj } from '@storybook/react'
import { fn } from '@storybook/test'

import Button from 'js/components/Button/Button'
import Flexer from 'js/components/Flexer/Flexer'
import Toolbar from 'js/components/Toolbar/Toolbar'

const meta = {
    component: Toolbar,
    args: {
        onClear: fn(),
    },
    argTypes: {
        children: {
            control: false,
            table: {
                type: {
                    summary: 'React.ReactNode',
                },
            },
        },
    },
} satisfies Meta<typeof Toolbar>

export default meta
type Story = StoryObj<typeof meta>

export const Default: Story = {
    args: {
        count: 13,
        children: null,
    },
    render: (args) => (
        <Toolbar {...args}>
            <Flexer divider="normal" gutter="normal">
                <Button
                    variant="warn"
                    onClick={() => alert("'Verwijderen' clicked")}
                >
                    Verwijderen
                </Button>
                <Button onClick={() => alert("'Annuleren' clicked")}>
                    Annuleren
                </Button>
            </Flexer>
        </Toolbar>
    ),
}
