import type { Meta, StoryObj } from '@storybook/react'
import { formatISO, subSeconds } from 'date-fns'

import DisplayDate from 'js/components/DisplayDate'

const meta = {
    component: DisplayDate,
    argTypes: {
        date: {
            control: { type: 'text' },
            table: {
                type: {
                    summary: 'ISO8601 (YYYY-MM-DDTHH:mm:ss.SSSSSS+HH:mm)',
                },
            },
        },
        placement: {
            control: { type: 'select' },
        },
        type: {
            control: { type: 'select' },
        },
    },
    args: {
        date: formatISO(new Date()),
    },
} satisfies Meta<typeof DisplayDate>

export default meta
type Story = StoryObj<typeof meta>

export const ShortDate: Story = { args: { type: 'shortDate' } }
export const DateTime: Story = { args: { type: 'dateTime' } }
export const Time: Story = { args: { type: 'time' } }
export const TimeSince: Story = {
    args: {
        date: formatISO(subSeconds(new Date(), 30)),
        type: 'timeSince',
    },
    parameters: {
        docs: {
            description: {
                story: 'If the selected date is less than 8 days ago from NOW, it will display how much time has elapsed (a few seconds ago, 10 minutes, 12 hours, 1 day, 3 days etc) and will update live. Otherwise, it will display the date. The example below starts with a few seconds ago.',
            },
        },
    },
}
