import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import type { Meta, StoryObj } from '@storybook/react'

import LinkItem from 'js/components/LinkItem/LinkItem'

const meta = {
    component: LinkItem,
    decorators: [
        (Story: any) => (
            <BrowserRouter>
                <Story />
            </BrowserRouter>
        ),
    ],
} satisfies Meta<typeof LinkItem>

export default meta
type Story = StoryObj<typeof meta>

export const ExternalLink: Story = {
    args: {
        url: 'http://www.pleio.nl',
        children: 'Label',
    },
}

export const InternalLink: Story = {
    args: {
        url: '/admin',
        children: 'Label',
    },
}
