import React from 'react'
import type { Meta, StoryObj } from '@storybook/react'

import Card, { CardContent } from 'js/components/Card/Card'

const meta = {
    component: Card,
    argTypes: {
        radiusSize: {
            options: ['small', 'normal', 'large'],
            control: {
                type: 'select',
            },
        },
    },
} satisfies Meta<typeof Card>

export default meta
type Story = StoryObj<typeof meta>

export const Default: Story = {
    args: {
        children: <CardContent>Card</CardContent>,
    },
    render: (args) => (
        <Card style={{ width: 400, height: 300 }} {...args}>
            {args.children}
        </Card>
    ),
}
