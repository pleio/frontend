import type { Meta, StoryObj } from '@storybook/react'

import SocialShare from 'js/components/SocialShare/SocialShare'

const meta = {
    component: SocialShare,
    argTypes: {
        divider: { control: { type: 'select' } },
        gutter: { control: { type: 'select' } },
    },
} satisfies Meta<typeof SocialShare>

export default meta
type Story = StoryObj<typeof meta>

export const Default: Story = {}

export const WithAlternativeLayout: Story = {
    args: {
        divider: 'none',
        gutter: 'normal',
    },
}
