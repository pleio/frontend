import React from 'react'
import type { Meta, StoryObj } from '@storybook/react'
import { styled } from 'styled-components'

import Text from 'js/components/Text/Text'
import Tooltip from 'js/components/Tooltip/Tooltip'

const StyledText = styled(Text)`
    color: #fff;
`

const meta = {
    component: Tooltip,
    decorators: [
        (_, { args }) => (
            <Tooltip content={args.content}>
                <div>{args.children}</div>
            </Tooltip>
        ),
    ],
    argTypes: {
        placement: {
            control: { type: 'select' },
        },
    },
} satisfies Meta<typeof Tooltip>

export default meta
type Story = StoryObj<typeof meta>

export const Default: Story = {
    args: {
        content: <StyledText>Tooltip content</StyledText>,
        children: <Text>Hover over me</Text>,
    },
}
