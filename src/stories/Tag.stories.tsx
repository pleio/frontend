import React from 'react'
import type { Meta, StoryObj } from '@storybook/react'

import Flexer from 'js/components/Flexer/Flexer'
import Tag from 'js/components/Tag/Tag'

const meta = {
    component: Tag,
    args: {
        children: 'Tag content',
    },
} satisfies Meta<typeof Tag>

export default meta
type Story = StoryObj<typeof meta>

export const WithRemoveButton: Story = {
    args: {
        onRemove: () => alert('remove'),
    },
}

export const MultipleTags: Story = {
    args: {
        children: "I'm a tag",
        onRemove: () => alert('remove'),
    },
    render: (args) => (
        <Flexer>
            <Tag>{args.children}</Tag>
            <Tag>{args.children}</Tag>
            <Tag>{args.children}</Tag>
        </Flexer>
    ),
}
