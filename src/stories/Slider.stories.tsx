import React from 'react'
import type { Meta, StoryObj } from '@storybook/react'
import { styled } from 'styled-components'

import Slider from 'js/components/Slider/Slider'

const meta = {
    component: Slider,
    parameters: {
        // the default Storybook layout breaks the Slider
        layout: 'fullscreen',
    },
} satisfies Meta<typeof Slider>

export default meta
type Story = StoryObj<typeof meta>

const Wrapper = styled.div`
    padding: 20px;
`

const Slide = styled.div`
    display: flex;
    flex: 1;
    justify-content: center;
    padding-block: 130px;
`

export const Default: Story = {
    args: {
        slidesPerView: 1,
    },
    render: (args) => (
        <Wrapper>
            <Slider {...args}>
                <Slide>a</Slide>
                <Slide>b</Slide>
                <Slide>c</Slide>
                <Slide>d</Slide>
            </Slider>
        </Wrapper>
    ),
}
