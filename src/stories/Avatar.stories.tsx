import type { Meta, StoryObj } from '@storybook/react'

import Avatar from 'js/components/Avatar/Avatar'

const meta = {
    component: Avatar,
    argTypes: {
        radiusStyle: {
            control: {
                type: 'select',
            },
        },
        status: {
            control: {
                type: 'select',
            },
        },
        image: {
            description: 'use an url to an image',
        },
    },
} satisfies Meta<typeof Avatar>

export default meta
type Story = StoryObj<typeof meta>

export const Default: Story = {
    args: {
        image: 'https://i.pravatar.cc/32',
        showHoverOverlay: true,
    },
}
