import React from 'react'
import type { Meta, StoryObj } from '@storybook/react'

import Img from 'js/components/Img/Img'

const meta = {
    component: Img,
    argTypes: {
        align: { control: { type: 'select' } },
    },
    args: {
        src: 'https://picsum.photos/id/16/2800/1600',
    },
    decorators: [
        (Story) => (
            <div
                style={{ width: 600, height: 340, backgroundColor: '#e0e0e0' }}
            >
                <Story />
            </div>
        ),
    ],
    // Force rerender by putting all args in a key, because this components tends to not always rerender
    render: (args) => <Img {...args} key={JSON.stringify(args)} />,
} satisfies Meta<typeof Img>

export default meta
type Story = StoryObj<typeof meta>

export const Default: Story = {
    args: {
        alt: 'Image description',
    },
}

export const SmallerThanWrapper: Story = {
    args: {
        alt: 'Image description',
        src: 'https://picsum.photos/id/16/500/300',
    },
}

export const ImageNotFound: Story = {
    args: {
        src: 'https://invalid.url',
        alt: 'Image description',
    },
}

export const AlignedRight: Story = {
    args: {
        alt: 'Image description',
        src: 'https://picsum.photos/id/16/500/300',
        align: 'right',
    },
}
