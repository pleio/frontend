FROM node:18-alpine AS build

ARG PUBLIC_PATH=/static/frontend/
ENV PUBLIC_PATH=$PUBLIC_PATH

RUN mkdir /app

# First copy only package.json and yarn.lock to make the dependency fetching step optional.
COPY ./package.json \
     ./yarn.lock \
     /app/

WORKDIR /app
RUN yarn install

# Now copy the whole directory for the build step.
COPY . /app
RUN yarn build

# Copy static files to nginx container.
FROM nginx:1.27.3-alpine-slim

RUN apk update && apk upgrade --available

# Copy nginx configuration
COPY ./docker/default.conf /etc/nginx/conf.d/default.conf

COPY --from=build /app/public /usr/share/nginx/html
COPY --from=build /app/build /usr/share/nginx/html
