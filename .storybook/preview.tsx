import React, { Suspense, useEffect } from 'react'
import { I18nextProvider } from 'react-i18next'
import type { Preview } from '@storybook/react'
import { ThemeProvider } from 'styled-components'

import getTheme from 'js/theme/getTheme'
import GlobalStyle from 'js/theme/globalStyle'

import mocks from './apolloMocks'
import i18n from './i18n'

const preview: Preview = {
    tags: ['autodocs'],
    parameters: {
        apolloClient: {
            mocks,
        },
        controls: {
            matchers: {
                color: /(background|color)$/i,
                date: /Date$/i,
            },
            // Due to an issue in Styled Components (https://github.com/styled-components/styled-components/issues/3813)
            // some props are added automatically to the Stories. We want to hide them for now.
            exclude: ['as', 'theme', 'forwardedAs'],
        },
        layout: 'centered',
    },
    decorators: [
        (Story) => {
            return (
                <ThemeProvider
                    theme={(currentTheme) =>
                        getTheme(currentTheme, 'site', {
                            fontBody: 'Arial',
                            fontHeading: 'Arial',
                            colorPrimary: '#0771f2',
                            colorSecondary: '#0771f2',
                            colorHeader: '#0771f2',
                        })
                    }
                >
                    <GlobalStyle />
                    <Story />
                </ThemeProvider>
            )
        },
        (Story, context) => {
            const { locale = 'nl' } = context.globals

            useEffect(() => {
                i18n.changeLanguage(locale)
            }, [locale])

            return (
                <Suspense fallback={<div>loading translations...</div>}>
                    <I18nextProvider i18n={i18n}>
                        <Story />
                    </I18nextProvider>
                </Suspense>
            )
        },
    ],
}

// Translation picker
export const globalTypes = {
    locale: {
        name: 'Locale',
        description: 'Internationalization locale',
        toolbar: {
            icon: 'globe',
            items: [
                { value: 'en', title: 'Engels' },
                { value: 'de', title: 'Duits' },
                { value: 'nl', title: 'Nederlands' },
                { value: 'fr', title: 'Frans' },
            ],
            showName: true,
        },
    },
}

export default preview
