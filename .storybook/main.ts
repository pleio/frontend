import type { StorybookConfig } from '@storybook/react-webpack5'
import path from 'path'

const config: StorybookConfig = {
    stories: ['../src/**/*.mdx', '../src/**/*.stories.@(js|jsx|mjs|ts|tsx)'],
    addons: [
        '@storybook/addon-webpack5-compiler-swc',
        '@storybook/addon-links',
        '@storybook/addon-essentials',
        '@chromatic-com/storybook',
        '@storybook/addon-interactions',
        '@storybook/addon-a11y',
        'storybook-addon-apollo-client',
    ],
    framework: {
        name: '@storybook/react-webpack5',
        options: {
            strictMode: true,
        },
    },
    webpackFinal: async (config) => {
        config.resolve.alias = {
            ...config.resolve?.alias,
            js: path.join(__dirname, '../src/js'),
            images: path.join(__dirname, '../src/images'),
            icons: path.join(__dirname, '../src/icons'),
            helpers: path.join(__dirname, '../src/js/lib/helpers'),
        }

        config.module.rules = [
            ...config.module.rules.map((rule: any) => {
                if (/svg/.test(rule.test)) {
                    // Silence the Storybook loaders for SVG files
                    return { ...rule, exclude: /\.svg$/i }
                }
                return rule
            }),
            // use @svgr
            {
                test: /\.svg$/i,
                use: ['@svgr/webpack'],
            },
        ]

        return config
    },
    docs: {},
    typescript: {
        reactDocgen: 'react-docgen-typescript',
        check: false,
    },
}
export default config
