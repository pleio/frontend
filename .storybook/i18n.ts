import { initReactI18next } from 'react-i18next'
import i18n from 'i18next'

import { messages } from 'js/i18n/utils'
import { setLocale } from 'js/lib/helpers/date/general'

i18n.use(initReactI18next).init({
    lng: 'nl',
    fallbackLng: 'nl',
    resources: messages,
    defaultNS: 'all',
    react: {
        useSuspense: false,
    },
})

i18n.on('languageChanged', (lang) => {
    const html = document.querySelector('html')
    if (html) {
        html.setAttribute('lang', lang)
    }
    setLocale(lang)
})

export default i18n
