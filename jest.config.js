/** @type {import('ts-jest').JestConfigWithTsJest} **/
module.exports = {
    testEnvironment: 'node',
    transform: {
        '^.+.tsx?$': ['ts-jest', {}],
        '\\.[jt]sx?$': 'babel-jest',
    },
    modulePathIgnorePatterns: ['<rootDir>/.types'],
    moduleNameMapper: {
        '^js/(.*)$': '<rootDir>/src/js/$1',
        '^icons/(.*)$': '<rootDir>/src/icons/$1',
        '^helpers/(.*)$': '<rootDir>/src/js/lib/helpers/$1',
        '^helpers': '<rootDir>/src/js/lib/helpers',
    },
    resetMocks: false,
    setupFiles: ['jest-localstorage-mock'],
    setupFilesAfterEnv: ['./jest.setup.js'],
}
