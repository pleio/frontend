const i18next = {
    t: jest.fn((t: string) => t),
}
export default i18next
