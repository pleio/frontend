// Mock console.error to prevent it from cluttering test output
beforeAll(() => {
    jest.spyOn(console, 'error').mockImplementation(() => { })
})

afterAll(() => {
    console.error.mockRestore()
})
