import babelParser from '@babel/eslint-parser'
import { fixupConfigRules, fixupPluginRules } from '@eslint/compat'
import { FlatCompat } from '@eslint/eslintrc'
import js from '@eslint/js'
import typescriptEslint from '@typescript-eslint/eslint-plugin'
import tsParser from '@typescript-eslint/parser'
import jsxA11Y from 'eslint-plugin-jsx-a11y'
import prettier from 'eslint-plugin-prettier'
import react from 'eslint-plugin-react'
import reactHooks from 'eslint-plugin-react-hooks'
import simpleImportSort from 'eslint-plugin-simple-import-sort'
import globals from 'globals'
import path from 'node:path'
import { fileURLToPath } from 'node:url'

const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)
const compat = new FlatCompat({
    baseDirectory: __dirname,
    recommendedConfig: js.configs.recommended,
    allConfig: js.configs.all,
})

export default [
    {
        ignores: [
            'src/js/i18n/messages/**/*',
            'src/less/**/*',
            'src/possibleTypes.json',
        ],
    },
    ...fixupConfigRules(
        compat.extends(
            'standard',
            'plugin:prettier/recommended',
            'plugin:react/recommended',
            'plugin:jsx-a11y/recommended',
        ),
    ),
    {
        plugins: {
            react: fixupPluginRules(react),
            prettier: fixupPluginRules(prettier),
            'jsx-a11y': fixupPluginRules(jsxA11Y),
            'react-hooks': fixupPluginRules(reactHooks),
            'simple-import-sort': simpleImportSort,
        },

        languageOptions: {
            globals: {
                ...globals.browser,
                ...globals.node,
            },

            parser: babelParser,
            ecmaVersion: 8,
            sourceType: 'module',

            parserOptions: {
                requireConfigFile: false,
            },
        },

        settings: {
            react: {
                version: 'detect',
            },
        },

        rules: {
            'prettier/prettier': [
                'error',
                {
                    tabWidth: 4,
                    printWidth: 80,
                    useTabs: false,
                    semi: false,
                    singleQuote: true,
                    trailingComma: 'all',
                    arrowParens: 'always',
                },
            ],

            'react/prop-types': 'off',

            'react/jsx-pascal-case': [
                'error',
                {
                    allowAllCaps: true,
                },
            ],

            camelcase: [
                'error',
                {
                    allow: ['^UNSAFE_'],
                },
            ],

            'no-console': [
                'error',
                {
                    allow: ['warn', 'error'],
                },
            ],

            'jsx-a11y/accessible-emoji': 'warn',
            'jsx-a11y/alt-text': 'warn',
            'jsx-a11y/anchor-has-content': 'warn',
            'jsx-a11y/anchor-is-valid': 'warn',
            'jsx-a11y/aria-activedescendant-has-tabindex': 'warn',
            'jsx-a11y/aria-props': 'warn',
            'jsx-a11y/aria-proptypes': 'warn',
            'jsx-a11y/aria-role': 'warn',
            'jsx-a11y/aria-unsupported-elements': 'warn',
            'jsx-a11y/click-events-have-key-events': 'warn',

            'jsx-a11y/control-has-associated-label': [
                'off',
                {
                    ignoreElements: [
                        'audio',
                        'canvas',
                        'embed',
                        'input',
                        'textarea',
                        'tr',
                        'video',
                    ],

                    ignoreRoles: [
                        'grid',
                        'listbox',
                        'menu',
                        'menubar',
                        'radiogroup',
                        'row',
                        'tablist',
                        'toolbar',
                        'tree',
                        'treegrid',
                    ],

                    includeRoles: ['alert', 'dialog'],
                },
            ],

            'jsx-a11y/heading-has-content': 'warn',
            'jsx-a11y/html-has-lang': 'warn',
            'jsx-a11y/iframe-has-title': 'warn',
            'jsx-a11y/img-redundant-alt': 'warn',

            'jsx-a11y/interactive-supports-focus': [
                'warn',
                {
                    tabbable: [
                        'button',
                        'checkbox',
                        'link',
                        'searchbox',
                        'spinbutton',
                        'switch',
                        'textbox',
                    ],
                },
            ],

            'jsx-a11y/label-has-associated-control': 'warn',
            'jsx-a11y/label-has-for': 'off',
            'jsx-a11y/media-has-caption': 'warn',
            'jsx-a11y/mouse-events-have-key-events': 'warn',
            'jsx-a11y/no-access-key': 'warn',
            'jsx-a11y/no-autofocus': 'warn',
            'jsx-a11y/no-distracting-elements': 'warn',

            'jsx-a11y/no-interactive-element-to-noninteractive-role': [
                'warn',
                {
                    tr: ['none', 'presentation'],
                },
            ],

            'jsx-a11y/no-noninteractive-element-interactions': [
                'warn',
                {
                    handlers: [
                        'onClick',
                        'onError',
                        'onLoad',
                        'onMouseDown',
                        'onMouseUp',
                        'onKeyPress',
                        'onKeyDown',
                        'onKeyUp',
                    ],

                    alert: ['onKeyUp', 'onKeyDown', 'onKeyPress'],
                    body: ['onError', 'onLoad'],
                    dialog: ['onKeyUp', 'onKeyDown', 'onKeyPress'],
                    iframe: ['onError', 'onLoad'],
                    img: ['onError', 'onLoad'],
                },
            ],

            'jsx-a11y/no-noninteractive-element-to-interactive-role': [
                'warn',
                {
                    ul: [
                        'listbox',
                        'menu',
                        'menubar',
                        'radiogroup',
                        'tablist',
                        'tree',
                        'treegrid',
                    ],
                    ol: [
                        'listbox',
                        'menu',
                        'menubar',
                        'radiogroup',
                        'tablist',
                        'tree',
                        'treegrid',
                    ],
                    li: ['menuitem', 'option', 'row', 'tab', 'treeitem'],
                    table: ['grid'],
                    td: ['gridcell'],
                },
            ],

            'jsx-a11y/no-noninteractive-tabindex': [
                'warn',
                {
                    tags: [],
                    roles: ['tabpanel'],
                    allowExpressionValues: true,
                },
            ],

            'jsx-a11y/no-onchange': 'warn',
            'jsx-a11y/no-redundant-roles': 'warn',

            'jsx-a11y/no-static-element-interactions': [
                'warn',
                {
                    allowExpressionValues: true,

                    handlers: [
                        'onClick',
                        'onMouseDown',
                        'onMouseUp',
                        'onKeyPress',
                        'onKeyDown',
                        'onKeyUp',
                    ],
                },
            ],

            'jsx-a11y/role-has-required-aria-props': 'warn',
            'jsx-a11y/role-supports-aria-props': 'warn',
            'jsx-a11y/scope': 'warn',
            'jsx-a11y/tabindex-no-positive': 'warn',
            'react-hooks/rules-of-hooks': 'warn',
            'react-hooks/exhaustive-deps': 'warn',

            'simple-import-sort/imports': [
                'error',
                {
                    groups: [
                        ['^react', '^@?\\w'],
                        ['^js'],
                        ['^images'],
                        ['^icons'],
                        ['^\\.\\.(?!/?$)', '^\\.\\./?$'],
                    ],
                },
            ],

            'simple-import-sort/exports': 'error',
        },
    },
    ...compat
        .extends(
            'eslint:recommended',
            'plugin:@typescript-eslint/eslint-recommended',
            'plugin:@typescript-eslint/recommended',
        )
        .map((config) => ({
            ...config,
            files: ['**/*.js', '**/*.jsx', '**/*.ts', '**/*.tsx'],
        })),
    {
        files: ['**/*.js', '**/*.jsx', '**/*.ts', '**/*.tsx'],

        plugins: {
            '@typescript-eslint': typescriptEslint,
        },

        languageOptions: {
            globals: {
                ...globals.browser,
                ...globals.node,
                Atomics: 'readonly',
                SharedArrayBuffer: 'readonly',
            },

            parser: tsParser,
            ecmaVersion: 2018,
            sourceType: 'module',

            parserOptions: {
                ecmaFeatures: {
                    jsx: true,
                },

                project: './tsconfig.json',
            },
        },

        rules: {
            '@typescript-eslint/no-explicit-any': 'off',
            '@typescript-eslint/no-empty-function': 'off',
            '@typescript-eslint/no-unused-vars': [
                'warn',
                { argsIgnorePattern: '^_' },
            ],
            'import/export': 'warn',
            'prefer-spread': 'warn',
            'n/no-callback-literal': 'warn',
            'react-hooks/exhaustive-deps': 'warn',
            '@typescript-eslint/ban-ts-comment': 'off',
            'react-hooks/rules-of-hooks': 'warn',
        },
    },
]
